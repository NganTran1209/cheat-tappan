<?php include_once(__DIR__ . '/common/util.php'); ?>
<?php include('header.php'); ?>
<?php
$info = new Information();
if (!empty($_GET['id'])) {
    $info->id = $_GET['id'];
    $info->select();
}

$page = 1;
$page = empty($_POST['page']) ? '1' : $_POST['page'];
if (!empty($_GET['page'])) {
    $page = $_GET['page'];
}
?>
<div class="c-infoDetail">
<form id="fm-param" method="post">
    <input type="hidden" id="id-page" name="page" value="<?= $page ?>"/>
</form>
<div class="container">
    <div class="row">
        <?php if (!empty($_GET['id'])): ?>
            <div class="col-md-9 mainContents  wow animate__animated animate__fadeInUp">
                <div class="bg-inner">
                    <h1><?= $info->title ?></h1>
                    <div class="information_details_contents">
                        <div class="text-left"><?= $info->regist_date ?></div>
                        <div><?php echo nl2br(htmlentities($info->text)); ?></div>
                    </div>
                </div>
            </div>
        <?php else: ?>
            <div class="col-md-9 mainContents  wow animate__animated animate__fadeInUp">
                <div class="bg-inner">
                    <h1>お知らせ</h1>
                    <ul class="list-group">
                        <?php
                        $index = 0;
                        $count = 0;
                        $items = $info->selectAll();
                        foreach ($items as $infoItem) : ?>
                            <?php
                            $count++;
                            if (ceil(++$index / MAX_PAGE_COUNT) != $page) {
                                continue;
                            }
                            ?>
                            <li class="list-group-item">
                                <a href="information_details.php?id=<?= $infoItem->id ?>"><i
                                            class="fa fa-chevron-right"
                                            aria-hidden="true"></i> <?= toDateStringFromMySqlDatetime($infoItem->regist_date) ?> <?= $infoItem->title ?>
                                </a>
                                <?php if (isNew($infoItem->regist_date)): ?>
                                    <span class="newIcon">NEW</span>
                                <?php endif; ?>
                            </li>
                        <?php endforeach; ?>
                    </ul>

                    <div class="text-center margin-20">
                        <?php
                        $prev_page = $page - 1;
                        $max_page = ceil($count / MAX_PAGE_COUNT);
                        $next_page = min($max_page, $page + 1);
                        $view_start_page = max(1, $page - ceil(VIEW_PAGE_SELECT_COUNT / 2));
                        $view_last_page = min($view_start_page + VIEW_PAGE_SELECT_COUNT - 1, $max_page);
                        if ($view_last_page - $view_start_page < VIEW_PAGE_SELECT_COUNT) {
                            $view_start_page = max(1, $view_last_page - VIEW_PAGE_SELECT_COUNT + 1);
                        }
                        ?>
                        <?php if ($prev_page > 0): ?>
                            <a class="sortButton" href=""
                               onclick="submitForm('fm-param', 'page', '<?= $prev_page ?>'); return false;">前へ</a>
                        <?php else: ?>
                            <a class="sortButton" href="" onclick="return false;">前へ</a>
                        <?php endif; ?>

                        <?php for ($i = $view_start_page; $i <= $view_last_page; $i++): ?>
                            <?php if ($page != $i): ?>
                                <a class="sortButton" href=""
                                   onclick="submitForm('fm-param', 'page', '<?= $i ?>'); return false;"><?= $i ?></a>
                            <?php else: ?>
                                <a class="sortButton active" href="" onclick="return false;"><?= $i ?></a>
                            <?php endif; ?>
                        <?php endfor; ?>

                        <?php if ($page < $max_page): ?>
                            <a class="sortButton" href=""
                               onclick="submitForm('fm-param', 'page', '<?= $next_page ?>'); return false;">次へ</a>
                        <?php else: ?>
                            <a class="sortButton" href="" onclick="return false;">次へ</a>
                        <?php endif; ?>

                        <p class="small"><?= number_format(($page - 1) * MAX_PAGE_COUNT + 1) ?>
                            ～<?= number_format(min($count, $page * MAX_PAGE_COUNT)) ?>件/<?= number_format($count) ?>
                            件</p>
                    </div>
                </div>
            </div>
        <?php endif; ?>
        <div class="col-md-3 sideContents  wow animate__animated animate__fadeInUp">
            <?php include('sidebar.php'); ?>
        </div>
    </div>
</div>
</div>
<?php include('footer.php'); ?>
