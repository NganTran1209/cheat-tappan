<?php include_once(__DIR__ . '/../common/util.php'); ?>

<?php
$entity = new User();
if (isset($_POST['action']) && $_POST['action'] == 'regist') {
    $entity->email = $_POST['email'];
    $entity->name = $_POST['name'];
    $entity->name_kana = $_POST['name_kana'];
//     $entity->zip = $_POST['zip'];
//     $entity->addr = $_POST['addr'];
    $entity->password = $_POST['password'];
    $entity->password2 = $_POST['password2'];
//     $entity->company = $_POST['company'];
//     $entity->tel = $_POST['tel'];
//     $entity->pref = $_POST['pref'];
//     $entity->type = $_POST['type'];
    $entity->ipaddr = getRemoteAddress();

    if (empty($_POST['kiyaku'])) {
        setMessage('利用規約に同意されておりません。');
    } elseif (!$entity->validate()) {

    } elseif ($entity->has()) {
        setMessage('既にメールアドレスが登録済みです。');
    } elseif ($_POST['password'] != $_POST['password2']) {
        setMessage('パスワードとパスワード（確認）が一致しません。');
    } else {
        $entity->regist();
        //setMessage('仮登録通知メールを送付いたしました。');
        header('Location: ' . getContextRoot() . '/user/userregist.php');
        exit;
    }
} elseif (isset($_POST['action']) && $_POST['action'] == 'send-mail') {
    $entity->email = $_POST['email'];
    $entity->password = $_POST['password'];
    if (!$entity->selectFromEmail()) {
        setMessage("仮登録しているメールアドレス・パスワードを指定してください。");
    } elseif ($entity->enabled != 0) {
        setMessage("指定されているメールアドレスのアカウントは本登録済みです。");
    } else {
        $entity->sendRegistMail();
        setMessage("仮登録メールを再送しました。");
    }
} elseif (isset($_POST['action']) && $_POST['action'] == 'buy-credit-card') {
    $url = HOME_URL . '/product/by_credit.php?item_id=' . $_POST['item_id'] . '&user_id=' . getUserId();
    header('Location: ' . $url);
}
$title_page = 'レディース新着アイテム';
?>
<style>

</style>
<?php include(__DIR__ . '/../other_header.php'); ?>
<div class="com-header-top">
    <div class="com-header-top__img wow animate__animated animate__fadeInUp">
        <img src="<?php echo HOME_URL; ?>/common/assets/img/common/other-com-header.png" alt="">
    </div>
    <div class="com-header-top__path bg-other-01 wow animate__animated animate__fadeInUp">
        <p><span>トップページ</span><span> > </span><span>レディース新着アイテム</span></p>
    </div>
    <div class="com-header-top__txt wow animate__animated animate__fadeInUp">
        <p>レディース新着アイテム</p>
    </div>
</div>
<div class="customer-container">
    <div class="buy-credit-card wow animate__animated animate__fadeInUp">
        <div class="buy-credit-card__lead">
            <h3><span>お支払い予定金額</span></h3>
        </div>
        <div class="buy-credit-card__price">
            <p><span>¥1,980 円</span>（税込/送料込）</p>
        </div>
        <div class="buy-credit-card__btn">
            <form method="post">
                <input type="hidden" name="item_id" value="1"/>
                <input type="hidden" name="action" value="buy-credit-card"/>
                <button type="submit" id="" name="">今すぐ支払う</button>
            </form>
        </div>
    </div>
</div>
<?php include('../footer.php'); ?>