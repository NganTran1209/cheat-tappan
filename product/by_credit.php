
<?php include_once(__DIR__ . '/../common/util.php'); ?>

<?php $title_page = 'レディース新着アイテム'; ?>
<?php include(__DIR__ . '/../other_header.php'); ?>
<div class="com-header-top">
    <div class="com-header-top__img wow animate__animated animate__fadeInUp">
        <img src="<?php echo HOME_URL; ?>/common/assets/img/common/other-com-header.png" alt="">
    </div>
    <div class="com-header-top__path bg-other-01 wow animate__animated animate__fadeInUp">
        <p><span>トップページ</span><span> > </span><span>レディース新着アイテム</span></p>
    </div>
    <div class="com-header-top__txt wow animate__animated animate__fadeInUp">
        <p>レディース新着アイテム</p>
    </div>
</div>
<div class="customer-container">
    <div class="buy-credit-card wow animate__animated animate__fadeInUp">
        <div class="buy-credit-card__lead">
            <h3><span>振込先の案内をメールしました</span></h3>
        </div>
        <div class="buy-credit">
            <button id="" name="">メールを送信しました</button>
        </div>
        <div class="buy-credit-lead">
            <div class="buy-credit-lead__txt">
                <p>お支払い予定金額</p>
            </div>
            <div class="buy-credit-lead__txt">
                <p><span>¥1,980 円</span>（税込/送料込）</p>
            </div>
        </div>
        <div class="credit-transfer-account">
            <h4>振込先の口座</h4>
        </div>
        <div class="credit-transfer__txt">
            <div class="credit-transfer__txt-item"><p>金融機関名</p></div>
            <div class="credit-transfer__txt-item"><p>○○銀行</p></div>
        </div>
        <div class="credit-transfer__txt">
            <div class="credit-transfer__txt-item"><p>支店名</p></div>
            <div class="credit-transfer__txt-item"><p>○○支店</p></div>
        </div>
        <div class="credit-transfer__txt">
            <div class="credit-transfer__txt-item"><p>預金種別</p></div>
            <div class="credit-transfer__txt-item"><p>普通</p></div>
        </div>
        <div class="credit-transfer__txt">
            <div class="credit-transfer__txt-item"><p>口座番号</p></div>
            <div class="credit-transfer__txt-item"><p>1234567</p></div>
        </div>
        <div class="credit-transfer__txt">
            <div class="credit-transfer__txt-item"><p>講座名義</p></div>
            <div class="credit-transfer__txt-item"><p>あいうえお</p></div>
        </div>
        <div class="credit-transfer__bottom">
            <p>
                <i class="bi bi-exclamation-triangle-fill"></i>メールボックスのゴミ箱・迷惑メールボックスもご確認下さい<br>
                ※5分以内に確認メールを1通送ります。<br>
                <span>※メールが届かない場合は迷惑メールフォルダ、ゴミ箱などをご確認下さい。</span>

            </p>
        </div>
    </div>
</div>
<?php include('../footer.php'); ?>
    