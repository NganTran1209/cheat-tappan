<?php include_once(__DIR__ . '/../common/util.php'); ?>

<?php
$uri = $_SERVER["REQUEST_URI"];
$evaluate = new Evaluate();
$evaluateMap = $evaluate->selectSummaryMap();
$items = array();
$items_tmp = array();
$entity = new Item();
$category_title = 'すべて';
$page = 1;

$search = empty($_POST['search']) ? '' : $_POST['search'];
$category = empty($_POST['category']) ? '' : $_POST['category'];
$sort = empty($_POST['sort']) ? '' : $_POST['sort'];
$owner_id = empty($_POST['owner_id']) ? '' : $_POST['owner_id'];
$page = empty($_POST['page']) ? '1' : $_POST['page'];

if (!empty($_GET['category'])) {
    $category = $_GET['category'];
}
if (!empty($_GET['search'])) {
    $search = $_GET['search'];
}
if (!empty($_GET['owner_id'])) {
    $owner_id = $_GET['owner_id'];
}
if (!empty($_GET['page'])) {
    $page = $_GET['page'];
}

if (!empty($category)) {
    $items_tmp = $entity->selectCategory($category);
    $category_title = selectCodeName('selectCategoryById', $category);
} elseif (!empty($search)) {
    $items_tmp = $entity->selectSearch($search);
    $category_title = '検索文字「' . $search . '」';
} else {
    $items_tmp = $entity->selectAll();
}
foreach ($items_tmp as $item) {
    if ($item->enabled != 1) {
        continue;
    }
    if (!$item->owner_selling) {
        continue;
    }
    if (!empty($owner_id) && $item->owner_id != $owner_id) {
        continue;
    }
    $items[] = $item;
}



// ソート
if (!empty($sort)) {
    if ($sort == '2') {
        usort($items, function ($a, $b) {
            return $b->price - $a->price;
        });
    } elseif ($sort == '3') {
        usort($items, function ($a, $b) {
            return $a->price - $b->price;
        });
    } else {
        usort($items, function ($a, $b) {
            return $a->regist_date >= $b->regist_date ? -1 : 1;
        });
    }
} else {
    usort($items, function ($a, $b) {
        return $a->regist_date >= $b->regist_date ? -1 : 1;
    });
}

if (!empty($owner_id)) {
    $owner = new User();
    $owner->select($owner_id);
    $category_title = $owner->getViewId();
}

function get_url_status($url) 
{
    $curl = curl_init($url);
    $ch = curl_init();
    curl_setopt($ch,CURLOPT_BINARYTRANSFER,1);
    curl_setopt($ch, CURLOPT_VERBOSE, 0);
    curl_setopt($ch, CURLINFO_HEADER_OUT, true);
    curl_setopt($ch, CURLOPT_HEADER, true);
    curl_setopt($ch, CURLOPT_URL, $url);
    echo "<span style='display:none'>";
        $result = curl_exec($curl);
    echo"</span>";
    if ($result !== false)
    {
        $statusCode = curl_getinfo($curl, CURLINFO_HTTP_CODE);  
        if ($statusCode == 404)
        {
            return false;
        }
        else
        {
            return true;
        }
    }
    else
    {
        return false;
    }
}
$title_page = $owner->name;
?>
<?php include(__DIR__ . '/../user_header.php'); ?>
<form id="fm-param" method="post">
    <input type="hidden" id="id-page" name="page" value="<?= $page ?>" />
</form>
<div class="com-header-top">
    <div class="com-header-top__img wow animate__animated animate__fadeInUp">
        <img src="<?php echo HOME_URL; ?>/common/assets/img/common/other-com-header.png" alt="">
    </div>
    <div class="com-header-top__path bg-other-01 wow animate__animated animate__fadeInUp">
        <p><span>出品者商品一覧</span></p>
    </div>
    <div class="com-header-top__txt wow animate__animated animate__fadeInUp">
        <p>出品者：<a href="<?php echo HOME_URL; ?>/user/seller_home.php?owner_id=<?= $owner->id ?>"><?= $owner->name ?></a></p>
    </div>
</div>
<div class="customer-container ">
    <div class="ladies-new-term">
        <div class="row wow animate__animated animate__fadeInUp">
            <?php
               $count = 0;
                $index = 0;
                foreach ($items as $item) {
                    if (User::isInvalid($item->owner_id)) {
                        continue;
                    }
                    if ($item->order_id != null && diffDays(date("Y/m/d"), $item->order_date) > MAX_VIEW_DAYS) {
                        continue;
                    }

                    $count++;
                    if (ceil(++$index / MAX_PAGE_COUNT) != $page) {
                        continue;
                    }
                    ?>
                    <?php
                    $evaluate = new Evaluate();
                    $evaluate->selectSummary($item->owner_id);
                    ?>
                    <a href="<?php echo HOME_URL; ?>/itemdetail.php?id=<?= $item->id ?>" class="ladies-group-column col-md-3 float-left" style="margin-top:2%">
                    <div class="ladies-group-column__img">
                        <?php 
                            // if(@file_get_contents($item->getThumbnailUrl()) ): 
                        ?>
                            <img src="<?= $item->getThumbnailUrl() ?>" alt="">
                        <?php 
                            // else: 
                        ?>
                            <!-- <img src="<?php echo HOME_URL; ?>/common/assets/img/no-image.png" alt="" > -->
                        <?php 
                            // endif; 
                        ?>
                        <div class="ladies-group-column__img-bottom"><?= $item->title ?></div>
                    </div>
                    <div class="ladies-group-column__icon">
                    </div>
                    <div class="ladies-group-column__price" style="right: 15px;">
                        <p><span>¥<?= number_format($item->price) ?></span></p>
                    </div>
                    </a>
            <?php } ?>  
        </div>
        
        <?php
                $prev_page = $page - 1;
                $max_page = ceil($count / MAX_PAGE_COUNT);
                $next_page = min($max_page, $page + 1);
                $view_start_page = max(1, $page - ceil(VIEW_PAGE_SELECT_COUNT / 2));
                $view_last_page = min($view_start_page + VIEW_PAGE_SELECT_COUNT - 1, $max_page);
                if ($view_last_page - $view_start_page < VIEW_PAGE_SELECT_COUNT) {
                    $view_start_page = max(1, $view_last_page - VIEW_PAGE_SELECT_COUNT + 1);
                }
            ?>
        <?php if($max_page >1): ?>
            <div class="content-group-pagination wow animate__animated animate__fadeInUp">
        <?php else: ?>
            <div class="content-group-pagination content-one-page wow animate__animated animate__fadeInUp">
        <?php endif; ?>
            
            <?php if($max_page > 1 ) { ?>
                <?php if ($prev_page > 0): ?>
                    <a id="pagination__preve" class="pagination__preve" href=""
                        onclick="submitForm('fm-param', 'page', '<?= $prev_page ?>'); return false;"><i class="bi bi-chevron-left"></i></a>
                <?php else: ?>
                    <a id="pagination__preve" class="pagination__preve" href="" onclick="return false;"><i class="bi bi-chevron-left"></i></a>
                <?php endif; ?>
            <?php } ?>
            <?php for ($i = $view_start_page; $i <= $view_last_page; $i++): ?>
                <?php if ($page != $i): ?>
                    <a class="pagination__num" href=""
                        onclick="submitForm('fm-param', 'page', '<?= $i ?>'); return false;"><span><?= $i ?></span></a>
                <?php else: ?>
                    <a class="pagination__num pag_active" href="" onclick="return false;"><span><?= $i ?></span></a>
                <?php endif; ?>
            <?php endfor; ?>

            <?php if($max_page > 1 ) { ?>
                <?php if ($page < $max_page): ?>
                    <a id="pagination__next" class="pagination__preve" href="" onclick="submitForm('fm-param', 'page', '<?= $next_page ?>'); return false;">
                        <i class="bi bi-chevron-right"></i>
                    </a>
                <?php else: ?>
                    <a id="pagination__next" class="pagination__preve" href="" onclick="return false;"><i class="bi bi-chevron-right"></i></a>
                <?php endif; ?>
            <?php } ?>
        </div>
    </div>
</div>

<?php include('../footer.php'); ?>
    