<?php include_once(__DIR__ . '/common/util.php'); ?>
<?php
$system_config = SystemConfig::select();
$info = new Information();
$item = new Item();
$request = new Request();
$top_flag = $system_config->enabled_search_bar;
?>
<?php include('header.php'); ?>
<section class="main-top">
    <div class="main-top__bg">
        <!-- <img class="pc-show" src="<?php echo HOME_URL; ?>/common/assets/img/top/top.png" alt="">
        <img class="sp-show" src="<?php echo HOME_URL; ?>/common/assets/img/top/sp-top.png" alt=""> -->
    </div>
    <div class="main-top__txt">
        <h2><span>T</span><span>E</span><span>P</span><span>P</span><span>A</span><span>N</span> <span>P</span><span>R</span><span>E</span><span>M</span><span>I</span><span>U</span><span>M</span> <span>S</span><span>T</span><span>O</span><span>R</span><span>E</span><br>
            <span>N</span><span>E</span><span>W</span><span> </span><span>O</span><span>P</span><span>E</span><span>N</span><span>!</span></h2>
    </div>
</section>
<section class="main-concept">
    <div class="main-concept-item">
        <div class="main-concept-item__img">
            <img class="pc-show" src="<?php echo HOME_URL; ?>/common/assets/img/top/concept-woma.png" alt="">
            <img class="sp-show" src="<?php echo HOME_URL; ?>/common/assets/img/top/sp-concept-woma.png" alt="">
        </div>
        <div class="main-concept-item-txt">
            <div class="main-concept-item-txt__title">
                <h2>てっぱんプレミアムストアとは？</h2>
            </div>
            <div class="main-concept-item-txt__text">
                <p>欲しい商品を画像で確認。届いた商品を画像で照合。</p>
                <p>すり替えをさせない、不正出来無いフリーマーケット。<br>
                只今、技術対応中。近日バージョンアップ致します！</p>
            </div>
        </div>
    </div>
</section>
<section class="popular-category">
    <div class="popular-category__title">
        <h2>人気のカテゴリー</h2>
    </div>
    <div class="popular-category-item">
        <div class="popular-item-group">
            <div class="popular-item-group-column">
                <a href="<?php echo HOME_URL; ?>/itemlist.php?category=1">
                    <div class="popular-group-column__icon">
                        <img src="<?php echo HOME_URL; ?>/common/assets/img/common/icon/dress.png" alt="">
                    </div>
                    <div class="popular-group-column__txt">
                        <p>レディース</p>
                    </div>
                </a>
            </div>
            <div class="popular-item-group-column">
                <a href="<?php echo HOME_URL; ?>/itemlist.php?category=2">
                    <div class="popular-group-column__icon">
                        <img src="<?php echo HOME_URL; ?>/common/assets/img/common/icon/coat.png" alt="">
                    </div>
                    <div class="popular-group-column__txt">
                        <p>メンズ</p>
                    </div>
                </a>
            </div>
        </div>
        <div class="popular-item-group">
            <div class="popular-item-group-column">
                <a href="<?php echo HOME_URL; ?>/itemlist.php?category=4">
                    <div class="popular-group-column__icon">
                        <img src="<?php echo HOME_URL; ?>/common/assets/img/common/icon/lotion.png" alt="">
                    </div>
                    <div class="popular-group-column__txt">
                        <p>コスメ・美容</p>
                    </div>
                </a>
            </div>
            <div class="popular-item-group-column">
                <a href="<?php echo HOME_URL; ?>/itemlist.php?category=3">
                    <div class="popular-group-column__icon">
                        <img src="<?php echo HOME_URL; ?>/common/assets/img/common/icon/baby-feeder.png" alt="">
                    </div>
                    <div class="popular-group-column__txt">
                        <p>キッズ・ベビー・マタニティ</p>
                    </div>
                </a>
            </div>
        </div>
    </div>
    <div class="popular-category-item">
        <div class="popular-item-group">
            <div class="popular-item-group-column">
                <a href="<?php echo HOME_URL; ?>/itemlist.php?category=5">
                    <div class="popular-group-column__icon">
                        <img src="<?php echo HOME_URL; ?>/common/assets/img/common/icon/guitar.png" alt="">
                    </div>
                    <div class="popular-group-column__txt">
                        <p>エンタメ・ホビー</p>
                    </div>
                </a>
            </div>
            <div class="popular-item-group-column">
                <a href="<?php echo HOME_URL; ?>/itemlist.php?category=6">
                    <div class="popular-group-column__icon">
                        <img src="<?php echo HOME_URL; ?>/common/assets/img/common/icon/interior-design.png" alt="">
                    </div>
                    <div class="popular-group-column__txt">
                        <p>インテリア・雑貨</p>
                    </div>
                </a>
            </div>
        </div>
        <div class="popular-item-group">
            <div class="popular-item-group-column">
                <a href="<?php echo HOME_URL; ?>/itemlist.php?category=7">
                    <div class="popular-group-column__icon">
                        <img src="<?php echo HOME_URL; ?>/common/assets/img/common/icon/washing-machine.png" alt="">
                    </div>
                    <div class="popular-group-column__txt">
                        <p>家電</p>
                    </div>
                </a>
            </div>
            <div class="popular-item-group-column">
                <a href="<?php echo HOME_URL; ?>/itemlist.php?category=8">
                    <div class="popular-group-column__icon">
                        <img src="<?php echo HOME_URL; ?>/common/assets/img/common/icon/sewing.png" alt="">
                    </div>
                    <div class="popular-group-column__txt">
                        <p>ハンドメイド</p>
                    </div>
                </a>
            </div>
        </div>
    </div>
</section>
<section class="ladies-item">
    <div class="ladies-item-title">
        <div class="ladies-item-title__txt">
            <h3><span>レディース新着アイテム</span></h3>
        </div>
        <div class="ladies-item-title__txt">
            <a href="<?php echo HOME_URL; ?>/itemlist.php?category=1">更に表示 &nbsp;&nbsp;&nbsp;></a>
        </div>
    </div>
    <div class="ladies-item-row">
        <div class="ladies-item-row-group" style="width: 100%;">
            <?php
            $count = 0;
            foreach ($item->selectCategory(1) as $itemItem): ?>
                <?php if (User::isInvalid($itemItem->owner_id)) {
                    continue;
                } ?>
                <?php if ($itemItem->enabled != 1) {
                    continue;
                } ?>
                <?php if (!$itemItem->owner_selling) {
                    continue;
                } ?>
                <?php
                if ($count++ >= 4) {
                    break;
                }
                ?>
                <a href="<?php echo HOME_URL; ?>/itemdetail.php?id=<?= $itemItem->id ?>" class="ladies-group-column" style="background: url('<?= $itemItem->getThumbnailUrl(); ?>');">
                    <?php if ($itemItem->order_id != null): ?>
                        <div class="soldOutBadgeMini">
                            <div>SOLD</div>
                        </div>
                    <?php endif; ?>
                    <div class="ladies-group-column__img">
                        <!-- <img src="<?= $item->getThumbnailUrl() ?>" alt=""> -->
                        <div class="ladies-group-column__img-bottom"><?= $itemItem->title ?></div>
                    </div><!-- 
                    <div class="ladies-group-column__icon">
                        <img src="<?php echo HOME_URL; ?>/common/assets/img/common/icon/profile.png" alt="">
                    </div> -->
                    <div class="ladies-group-column__price">
                        <p><span>¥<?= number_format($itemItem->price) ?></span></p>
                    </div>
                </a>
            <?php endforeach; ?>
            <?php
                for ($i=1; $i <= (4 - $count) ; $i++) { 
                    echo '<div style="width: 23.5%"></div>';
                }
            ?>
        </div>
    </div>
    <div class="ladies-item-title mt-50">
        <div class="ladies-item-title__txt">
            <h3><span>メンズ新着アイテム</span></h3>
        </div>
        <div class="ladies-item-title__txt">
            <a href="<?php echo HOME_URL; ?>/itemlist.php?category=2">更に表示 &nbsp;&nbsp;&nbsp;></a>
        </div>
    </div>
    <div class="ladies-item-row">
        <div class="ladies-item-row-group" style="width: 100%;">
            <?php
                $count = 0;
                foreach ($item->selectCategory(2) as $itemItem): ?>
                    <?php if (User::isInvalid($itemItem->owner_id)) {
                        continue;
                    } ?>
                    <?php if ($itemItem->enabled != 1) {
                        continue;
                    } ?>
                    <?php if (!$itemItem->owner_selling) {
                        continue;
                    } ?>
                    <?php
                    if ($count++ >= 4) {
                        break;
                    }
                    ?>
                    <a href="<?php echo HOME_URL; ?>/itemdetail.php?id=<?= $itemItem->id ?>" class="ladies-group-column" style="background: url('<?= $itemItem->getThumbnailUrl(); ?>');">
                        <?php if ($itemItem->order_id != null): ?>
                            <div class="soldOutBadgeMini">
                                <div>SOLD</div>
                            </div>
                        <?php endif; ?>
                        <div class="ladies-group-column__img">
                            <!-- <img src="<?= $item->getThumbnailUrl() ?>" alt=""> -->
                            <div class="ladies-group-column__img-bottom"><?= $itemItem->title ?></div>
                        </div><!-- 
                        <div class="ladies-group-column__icon">
                            <img src="<?php echo HOME_URL; ?>/common/assets/img/common/icon/profile.png" alt="">
                        </div> -->
                        <div class="ladies-group-column__price">
                            <p><span>¥<?= number_format($itemItem->price) ?></span></p>
                        </div>
                    </a>
                <?php endforeach; ?>
            <?php
                for ($i=1; $i <= (4 - $count) ; $i++) { 
                    echo '<div style="width: 23.5%"></div>';
                }
            ?>
        </div>
    </div>
</section>
<section class="newsletter">
    <div class="newsletter-content">
        <div class="newsletter-content-item">
            <div class="newsletter-item-title">
                <div class="newsletter-item__txt">
                    <h3><span>最新の購入希望依頼</span></h3>
                </div>
                <div class="newsletter-item__txt">
                    <a href="#">更に表示 &nbsp;&nbsp;&nbsp;></a>
                </div>
            </div>
            <?php
            $count = 0;
            foreach ($request->selectAll() as $requestItem): ?>
                <?php if (User::isInvalid($requestItem->user_id)) {
                    continue;
                } ?>
                <?php
                if ($requestItem->enabled != 1) {
                    continue;
                }
                if ($count++ >= MAX_REQUEST_LIST_COUNT) {
                    break;
                }
                ?>
                    <a class="newsletter-item-content__link" href="<?php echo HOME_URL; ?>/request_details.php?id=<?= $requestItem->id ?>">
                        <div class="newsletter-item-content__link-date"><?= toDateStringFromMySqlDatetime($requestItem->regist_date) ?></div>
                        <div class="newsletter-item-content__link-txt"><?= $requestItem->title ?></div>
                    </a>

                        <!-- <a class="newsletter-item-content__link" href="<?php echo HOME_URL; ?>/information_details.php?id=<?= $infoItem->id ?>">
                            <i class="fa fa-chevron-right" aria-hidden="true"></i>  
                        <div class="newsletter-item-content__link-date"><?= toDateStringFromMySqlDatetime($requestItem->regist_date) ?></div>
                        <div class="newsletter-item-content__link-txt"><?= $requestItem->title ?></div>
                        </a> -->
                    <?php if (isNew($requestItem->regist_date)): ?>
                        <!-- <span class="newIcon">NEW</span> -->
                    <?php endif; ?>
            <?php endforeach; ?><!-- 
            <div class="newsletter-item-content">
                <a href="#" class="newsletter-item-content__link">
                    <div class="newsletter-item-content__link-date">2021/3/11</div>
                    <div class="newsletter-item-content__link-txt">◯◯アイテムの購入希望◯◯アイテムの購入希望</div>
                </a>
                <a href="#" class="newsletter-item-content__link">
                    <div class="newsletter-item-content__link-date">2021/3/11</div>
                    <div class="newsletter-item-content__link-txt">◯◯アイテムの購入希望◯◯アイテムの購入希望</div>
                </a>
                <a href="#" class="newsletter-item-content__link">
                    <div class="newsletter-item-content__link-date">2021/3/11</div>
                    <div class="newsletter-item-content__link-txt">◯◯アイテムの購入希望◯◯アイテムの購入希望</div>
                </a>
                <a href="#" class="newsletter-item-content__link">
                    <div class="newsletter-item-content__link-date">2021/3/11</div>
                    <div class="newsletter-item-content__link-txt">◯◯アイテムの購入希望◯◯アイテムの購入希望</div>
                </a>
                <a href="#" class="newsletter-item-content__link">
                    <div class="newsletter-item-content__link-date">2021/3/11</div>
                    <div class="newsletter-item-content__link-txt">◯◯アイテムの購入希望◯◯アイテムの購入希望</div>
                </a>
            </div> -->
        </div>
        <div class="newsletter-content-item">
            <div class="newsletter-item-title">
                <div class="newsletter-item__txt">
                    <h3><span>お知らせ</span></h3>
                </div>
                <div class="newsletter-item__txt">
                    <a href="#">更に表示 &nbsp;&nbsp;&nbsp;></a>
                </div>
            </div>
            <?php
                $count = 0;
                foreach ($info->selectAll() as $infoItem) : ?>
                    <?php
                    if ($count++ >= MAX_INFOMATION_LIST_COUNT) {
                        break;
                    }
                    ?>
                    <!-- <li> -->
                        <a class="newsletter-item-content__link" href="<?php echo HOME_URL; ?>/information_details.php?id=<?= $infoItem->id ?>">
                            <!-- <i class="fa fa-chevron-right" aria-hidden="true"></i>   -->
                        <div class="newsletter-item-content__link-date"><?= toDateStringFromMySqlDatetime($infoItem->regist_date) ?></div>
                        <div class="newsletter-item-content__link-txt"><?= $infoItem->title ?></div>
                        </a>
                        <?php if (isNew($infoItem->regist_date)): ?>
                            <!-- <span class="newIcon">NEW</span> -->
                        <?php endif; ?>
                    <!-- </li> -->
            <?php endforeach; ?>
<!--             <div class="">
                <a href="#" class="newsletter-item-content__link">
                    <div class="newsletter-item-content__link-date">2021/3/11</div>
                    <div class="newsletter-item-content__link-txt">お知らせあいうえお</div>
                </a>
                <a href="#" class="newsletter-item-content__link">
                    <div class="newsletter-item-content__link-date">2021/3/11</div>
                    <div class="newsletter-item-content__link-txt">お知らせあいうえお</div>
                </a>
                <a href="#" class="newsletter-item-content__link">
                    <div class="newsletter-item-content__link-date">2021/3/11</div>
                    <div class="newsletter-item-content__link-txt">お知らせあいうえお</div>
                </a>
                <a href="#" class="newsletter-item-content__link">
                    <div class="newsletter-item-content__link-date">2021/3/11</div>
                    <div class="newsletter-item-content__link-txt">お知らせあいうえお</div>
                </a>
                <a href="#" class="newsletter-item-content__link">
                    <div class="newsletter-item-content__link-date">2021/3/11</div>
                    <div class="newsletter-item-content__link-txt">お知らせあいうえお</div>
                </a>
            </div> -->
        </div>
    </div>
</section>
<script type="text/javascript">
const CLASSNAME = "-visible";
const TIMEOUT = 1500;
const $target = $(".top__txt");

setInterval(() => {
  $target.addClass(CLASSNAME);
  setTimeout(() => {
    $target.removeClass(CLASSNAME);
  }, TIMEOUT);
}, TIMEOUT * 2);

</script>
<?php include('footer.php'); ?>  