<?php 
$title_page = 'てっぱんプレミアムストア';
$description_page = '欲しい商品を画像で確認・届いた商品を画像で照合！気持ち良い取引が叶うオンラインマーケット。';
 ?>
<?php include_once(__DIR__ . '/common/util.php'); ?>
<?php
$system_config = SystemConfig::select();
$info = new Information();
$item = new Item();
$request = new Request();
$top_flag = $system_config->enabled_search_bar;

function get_url_status($url) 
{
    $curl = curl_init($url);
    $ch = curl_init();
    curl_setopt($ch,CURLOPT_BINARYTRANSFER,1);
    curl_setopt($ch, CURLOPT_VERBOSE, 0);
    curl_setopt($ch, CURLINFO_HEADER_OUT, true);
    curl_setopt($ch, CURLOPT_HEADER, true);
    curl_setopt($ch, CURLOPT_URL, $url);
    echo "<span style='display:none'>";
        $result = curl_exec($curl);
    echo"</span>";
    if ($result !== false)
    {
        $statusCode = curl_getinfo($curl, CURLINFO_HTTP_CODE);  
        if ($statusCode == 404)
        {
            return false;
        }
        else
        {
            return true;
        }
    }
    else
    {
        return false;
    }
}
?>

<?php include('header.php'); ?>
<section class="main-top quynh">
    <div class="main-top__bg">
        <!-- <img class="pc-show" src="<?php echo HOME_URL; ?>/common/assets/img/top/top.png" alt="">
        <img class="sp-show" src="<?php echo HOME_URL; ?>/common/assets/img/top/sp-top.png" alt=""> -->
    </div>
    <div class="main-top__txt wow animate__animated animate__fadeInLeft">
        <h2><span>T</span><span>E</span><span>P</span><span>P</span><span>A</span><span>N</span> <span>P</span><span>R</span><span>E</span><span>M</span><span>I</span><span>U</span><span>M</span> <span>S</span><span>T</span><span>O</span><span>R</span><span>E</span><br>
            <span>N</span><span>E</span><span>W</span><span> </span><span>O</span><span>P</span><span>E</span><span>N</span><span>!</span></h2>
    </div>
</section>
<section class="main-concept">
    <div class="main-concept-item">
        <div class="main-concept-item__img animate__animated animate__fadeInLeft wow">
            <img class="pc-show" src="<?php echo HOME_URL; ?>/common/assets/img/top/concept-woma.png" alt="">
            <img class="sp-show" src="<?php echo HOME_URL; ?>/common/assets/img/top/sp-concept-woma.png" alt="">
        </div>
        <div class="main-concept-item-txt animate__animated animate__fadeInUp wow">
            <div class="main-concept-item-txt__title ">
                <h2>てっぱんプレミアムストアとは？</a></h2>
            </div>
            <div class="main-concept-item-txt__text">
                <p>欲しい商品を画像で確認。届いた商品を画像で照合。</p>
                <p>すり替えをさせない、不正出来ないフリーマーケット。<br>
                只今、技術対応中。近日バージョンアップ致します！</p>
                <p class="top-btn-see-detail"><a href="<?php echo HOME_URL; ?>/pages/teppan.php">→詳しく見る</a></p>
            </div>
        </div>
    </div>
</section>
<section class="popular-category">
    <div class="popular-category__title wow animate__animated animate__fadeInUp">
        <h2>人気のカテゴリー</h2>
    </div>
    <div class="popular-category-item row wow animate__animated animate__fadeInUp">
            <div class="col-md-3 float-left" style="margin-top: 20px;">
                <div class="popular-item-group-column " >
                    <a href="<?php echo HOME_URL; ?>/itemlist.php?category=1">
                        <div class="popular-group-column__icon">
                            <img src="<?php echo HOME_URL; ?>/common/assets/img/common/icon/dress.png" alt="">
                        </div>
                        <div class="popular-group-column__txt">
                            <p>レディース</p>
                        </div>
                    </a>
                </div>
            </div>
            <div class="col-md-3 float-left" style="margin-top: 20px;">
                <div class="popular-item-group-column " >
                    <a href="<?php echo HOME_URL; ?>/itemlist.php?category=2">
                        <div class="popular-group-column__icon">
                            <img src="<?php echo HOME_URL; ?>/common/assets/img/common/icon/coat.png" alt="">
                        </div>
                        <div class="popular-group-column__txt">
                            <p>メンズ</p>
                        </div>
                    </a>
                </div>
            </div>
            <div class="col-md-3 float-left" style="margin-top: 20px;">
                <div class="popular-item-group-column " >
                    <a href="<?php echo HOME_URL; ?>/itemlist.php?category=3">
                        <div class="popular-group-column__icon">
                            <img src="<?php echo HOME_URL; ?>/common/assets/img/common/icon/baby-feeder.png" alt="">
                        </div>
                        <div class="popular-group-column__txt">
                            <p>キッズ・ベビー・マタニティ</p>
                        </div>
                    </a>
                </div>
            </div>
            <div class="col-md-3 float-left" style="margin-top: 20px;">
                <div class="popular-item-group-column " >
                    <a href="<?php echo HOME_URL; ?>/itemlist.php?category=4">
                        <div class="popular-group-column__icon">
                            <img src="<?php echo HOME_URL; ?>/common/assets/img/common/icon/lotion.png" alt="">
                        </div>
                        <div class="popular-group-column__txt">
                            <p>コスメ・美容</p>
                        </div>
                    </a>
                </div>
            </div>
            <div class="col-md-3 float-left" style="margin-top: 20px;">
                <div class="popular-item-group-column " >
                    <a href="<?php echo HOME_URL; ?>/itemlist.php?category=5">
                        <div class="popular-group-column__icon">
                            <img src="<?php echo HOME_URL; ?>/common/assets/img/common/icon/guitar.png" alt="">
                        </div>
                        <div class="popular-group-column__txt">
                            <p>エンタメ・ホビー</p>
                        </div>
                    </a>
                </div>
            </div>
            <div class="col-md-3 float-left" style="margin-top: 20px;">
                <div class="popular-item-group-column " >
                    <a href="<?php echo HOME_URL; ?>/itemlist.php?category=6">
                        <div class="popular-group-column__icon">
                            <img src="<?php echo HOME_URL; ?>/common/assets/img/common/icon/interior-design.png" alt="">
                        </div>
                        <div class="popular-group-column__txt">
                            <p>インテリア・雑貨</p>
                        </div>
                    </a>
                </div>
            </div>
            <div class="col-md-3 float-left" style="margin-top: 20px;">
                <div class="popular-item-group-column " >
                    <a href="<?php echo HOME_URL; ?>/itemlist.php?category=7">
                        <div class="popular-group-column__icon">
                            <img src="<?php echo HOME_URL; ?>/common/assets/img/common/icon/washing-machine.png" alt="">
                        </div>
                        <div class="popular-group-column__txt">
                            <p>家電</p>
                        </div>
                    </a>
                </div>
            </div>
            <div class="col-md-3 float-left" style="margin-top: 20px;">
                <div class="popular-item-group-column " >
                    <a href="<?php echo HOME_URL; ?>/itemlist.php?category=8">
                        <div class="popular-group-column__icon">
                            <img src="<?php echo HOME_URL; ?>/common/assets/img/common/icon/ball.png" alt="">
                        </div>
                        <div class="popular-group-column__txt">
                            <p>スポーツ・アウトドア</p>
                        </div>
                    </a>
                </div>
            </div>
            <div class="col-md-3 float-left" style="margin-top: 20px;">
                <div class="popular-item-group-column " >
                    <a href="<?php echo HOME_URL; ?>/itemlist.php?category=9">
                        <div class="popular-group-column__icon">
                            <img src="<?php echo HOME_URL; ?>/common/assets/img/common/icon/bike.png" alt="">
                        </div>
                        <div class="popular-group-column__txt">
                            <p>自転車・バイク</p>
                        </div>
                    </a>
                </div>
            </div>
            <div class="col-md-3 float-left mr-auto" style="margin-top: 20px;">
                <div class="popular-item-group-column " >
                    <a href="<?php echo HOME_URL; ?>/itemlist.php?category=10">
                        <div class="popular-group-column__icon">
                            <img src="<?php echo HOME_URL; ?>/common/assets/img/common/icon/sewing.png" alt="">
                        </div>
                        <div class="popular-group-column__txt">
                            <p>ハンドメイド</p>
                        </div>
                    </a>
                </div>
            </div>
    </div>
</section>
<section class="ladies-item p-top__ladies-item">
    <?php 
    if (isset($_SESSION['login'])){ ?>
        <div class="ladies-item-title wow animate__animated animate__fadeInUp">
            <div class="ladies-item-title__txt">
                <h3><span>新着アイテム</span></h3>
            </div>
        </div>
        <div class="ladies-item-row wow animate__animated animate__fadeInUp">
            <div id="new_slider" class="ladies-item-row-group">
                <?php
                foreach ($item->selectNewItemsLimited() as $itemItem): ?>
                    <?php if (User::isInvalid($itemItem->owner_id)) {
                        continue;
                    } ?>
                    <?php if ($itemItem->enabled != 1) {
                        continue;
                    } ?>
                    <?php if (!$itemItem->owner_selling) {
                        continue;
                    } ?>
                    <a href="<?php echo HOME_URL; ?>/itemdetail.php?id=<?= $itemItem->id ?>&new_title=レディース新着アイテム" class="ladies-group-column" style="margin-bottom: 50px" >
                        <div class="ladies-group-column__img">
                            <?php 
                                // if(@file_get_contents($itemItem->getThumbnailUrl()) ): 
                            ?>
                                <img src="<?= $itemItem->getThumbnailUrl() ?>">
                            <?php 
                                // else: 
                            ?>
                                <!-- <img src="<?php echo HOME_URL; ?>/common/assets/img/no-image.png" alt="" > -->
                            <?php 
                                // endif; 
                            ?>
                        </div>
                        
                        <div class="ladies-group-column__img-bottom"><?= $itemItem->title ?></div>
                        <div class="ladies-group-column__icon">
                            <!-- <img src="<?php echo HOME_URL; ?>/common/assets/img/common/icon/profile.png" alt=""> -->
                        </div>
                        <div class="ladies-group-column__price">
                            <p><span>¥<?php echo number_format($itemItem->price); ?></span></p>
                        </div>
                    </a>
                <?php endforeach; ?>
            </div>
        </div>
        <div class="ladies-item-title wow animate__animated animate__fadeInUp">
            <div class="ladies-item-title__txt">
                <h3><span>注目アイテム</span></h3>
            </div>
        </div>
        <div class="ladies-item-row wow animate__animated animate__fadeInUp">
            <div id="highlight_slider" class="ladies-item-row-group" style="width: 100%;">
                <?php
                foreach ($item->selectCategory(11) as $itemItem): ?>
                    <?php if (User::isInvalid($itemItem->owner_id)) {
                        continue;
                    } ?>
                    <?php if ($itemItem->enabled != 1) {
                        continue;
                    } ?>
                    <?php if (!$itemItem->owner_selling) {
                        continue;
                    } ?>
                    <a href="<?php echo HOME_URL; ?>/itemdetail.php?id=<?= $itemItem->id ?>&new_title=レディース新着アイテム" class="ladies-group-column" style="margin-bottom: 50px">
                        <div class="ladies-group-column__img">
                            <?php 
                                // if(@file_get_contents($itemItem->getThumbnailUrl()) ): 
                            ?>
                                <img src="<?= $itemItem->getThumbnailUrl() ?>">
                            <?php 
                                // else: 
                            ?>
                                <!-- <img src="<?php echo HOME_URL; ?>/common/assets/img/no-image.png" alt="" > -->
                            <?php 
                                // endif; 
                            ?>
                        </div>
                        
                        <div class="ladies-group-column__img-bottom"><?= $itemItem->title ?></div>
                        <div class="ladies-group-column__icon">
                            <!-- <img src="<?php echo HOME_URL; ?>/common/assets/img/common/icon/profile.png" alt=""> -->
                        </div>
                        <div class="ladies-group-column__price">
                            <p><span>¥<?php echo number_format($itemItem->price); ?></span></p>
                        </div>
                    </a>
                <?php endforeach; ?>
            </div>
        </div>
    <?php } ?>
    <div class="ladies-item-title wow animate__animated animate__fadeInUp">
        <div class="ladies-item-title__txt">
            <h3><span>レディース新着アイテム</span></h3>
        </div>
        <div class="ladies-item-title__txt">
            <a href="<?php echo HOME_URL; ?>/itemlist.php?category=1&new_title=レディース新着アイテム">更に表示 &nbsp;&nbsp;&nbsp;></a>
        </div>
    </div>
    <div class="ladies-item-row wow animate__animated animate__fadeInUp">
        <div class="ladies-item-row-group" style="width: 100%;">
            <?php
            $count = 0;
            foreach ($item->selectCategory(1) as $itemItem): ?>
                <?php if (User::isInvalid($itemItem->owner_id)) {
                    continue;
                } ?>
                <?php if ($itemItem->enabled != 1) {
                    continue;
                } ?>
                <?php if (!$itemItem->owner_selling) {
                    continue;
                } ?>
                <?php
                if ($count++ >= 4) {
                    break;
                }
                ?>
                <a href="<?php echo HOME_URL; ?>/itemdetail.php?id=<?= $itemItem->id ?>&new_title=レディース新着アイテム" class="ladies-group-column" >
                    <div class="ladies-group-column__img">
                        <?php 
                            // if(@file_get_contents($itemItem->getThumbnailUrl()) ): 
                        ?>
                            <img src="<?= $itemItem->getThumbnailUrl() ?>">
                        <?php 
                            // else: 
                        ?>
                            <!-- <img src="<?php echo HOME_URL; ?>/common/assets/img/no-image.png" alt="" > -->
                        <?php 
                            // endif; 
                        ?>
                    </div>
                    
                    <div class="ladies-group-column__img-bottom"><?= $itemItem->title ?></div>
                    <div class="ladies-group-column__icon">
                        <!-- <img src="<?php echo HOME_URL; ?>/common/assets/img/common/icon/profile.png" alt=""> -->
                    </div>
                    <div class="ladies-group-column__price">
                        <p><span>¥<?php echo number_format($itemItem->price); ?></span></p>
                    </div>
                </a>
            <?php endforeach; ?>
            <?php
                for ($i=1; $i <= (4 - $count) ; $i++) { 
                    echo '<div ></div>';
                }
            ?>
        </div>
    </div>
    <div class="ladies-item-title mt-50 wow animate__animated animate__fadeInUp">
        <div class="ladies-item-title__txt">
            <h3><span>メンズ新着アイテム</span></h3>
        </div>
        <div class="ladies-item-title__txt">
            <a href="<?php echo HOME_URL; ?>/itemlist.php?category=2&new_title=メンズ新着アイテム">更に表示 &nbsp;&nbsp;&nbsp;></a>
        </div>
    </div>
    <div class="ladies-item-row wow animate__animated animate__fadeInUp">
        <div class="ladies-item-row-group" style="width: 100%;">
            <?php
                $count = 0;
                foreach ($item->selectCategory(2) as $itemItem): ?>
                    <?php if (User::isInvalid($itemItem->owner_id)) {
                        continue;
                    } ?>
                    <?php if ($itemItem->enabled != 1) {
                        continue;
                    } ?>
                    <?php if (!$itemItem->owner_selling) {
                        continue;
                    } ?>
                    <?php
                    if ($count++ >= 4) {
                        break;
                    }
                    ?>
                    <a href="<?php echo HOME_URL; ?>/itemdetail.php?id=<?= $itemItem->id ?>&new_title=メンズ新着アイテム" class="ladies-group-column">
                        <div class="ladies-group-column__img">
                            <?php 
                                // if(@file_get_contents($itemItem->getThumbnailUrl()) ): 
                            ?>
                                <img src="<?= $itemItem->getThumbnailUrl() ?>">
                            <?php 
                                // else: 
                            ?>
                                <!-- <img src="<?php echo HOME_URL; ?>/common/assets/img/no-image.png" alt="" > -->
                            <?php 
                                // endif; 
                            ?>
                            <div class="ladies-group-column__img-bottom"><?= $itemItem->title ?></div>
                        </div>
                        <div class="ladies-group-column__icon">
                            <!-- <img src="<?php echo HOME_URL; ?>/common/assets/img/common/icon/profile.png" alt=""> -->
                        </div>
                        <div class="ladies-group-column__price">
                            <p><span>¥<?php echo number_format($itemItem->price); ?></span></p>
                        </div>
                    </a>
                <?php endforeach; ?>
            <?php
                for ($i=1; $i <= (4 - $count) ; $i++) { 
                    echo '<div ></div>';
                }
            ?>
        </div>
    </div>

    <div class="ladies-item-title mt-50 wow animate__animated animate__fadeInUp">
        <div class="ladies-item-title__txt">
            <h3><span>キッズ・ベビー・マタニティ新着アイテム</span></h3>
        </div>
        <div class="ladies-item-title__txt">
            <a href="<?php echo HOME_URL; ?>/itemlist.php?category=3&new_title=キッズ・ベビー・マタニティ新着アイテム">更に表示 &nbsp;&nbsp;&nbsp;></a>
        </div>
    </div>
    <div class="ladies-item-row wow animate__animated animate__fadeInUp">
        <div class="ladies-item-row-group" style="width: 100%;">
            <?php
                $count = 0;
                foreach ($item->selectCategory(3) as $itemItem): ?>
                    <?php if (User::isInvalid($itemItem->owner_id)) {
                        continue;
                    } ?>
                    <?php if ($itemItem->enabled != 1) {
                        continue;
                    } ?>
                    <?php if (!$itemItem->owner_selling) {
                        continue;
                    } ?>
                    <?php
                    if ($count++ >= 4) {
                        break;
                    }
                    ?>
                    <a href="<?php echo HOME_URL; ?>/itemdetail.php?id=<?= $itemItem->id ?>&new_title=キッズ・ベビー・マタニティ新着アイテム" class="ladies-group-column">
                        <div class="ladies-group-column__img">
                            <?php 
                                // if(@file_get_contents($itemItem->getThumbnailUrl()) ): 
                            ?>
                                <img src="<?= $itemItem->getThumbnailUrl() ?>">
                            <?php 
                                // else: 
                            ?>
                                <!-- <img src="<?php echo HOME_URL; ?>/common/assets/img/no-image.png" alt="" > -->
                            <?php 
                                // endif; 
                            ?>
                            <div class="ladies-group-column__img-bottom"><?= $itemItem->title ?></div>
                        </div>
                        <div class="ladies-group-column__icon">
                            <!-- <img src="<?php echo HOME_URL; ?>/common/assets/img/common/icon/profile.png" alt=""> -->
                        </div>
                        <div class="ladies-group-column__price">
                            <p><span>¥<?php echo number_format($itemItem->price); ?></span></p>
                        </div>
                    </a>
                <?php endforeach; ?>
            <?php
                for ($i=1; $i <= (4 - $count) ; $i++) { 
                    echo '<div ></div>';
                }
            ?>
        </div>
    </div>

    <div class="ladies-item-title mt-50 wow animate__animated animate__fadeInUp">
        <div class="ladies-item-title__txt">
            <h3><span>コスメ・美容新着アイテム</span></h3>
        </div>
        <div class="ladies-item-title__txt">
            <a href="<?php echo HOME_URL; ?>/itemlist.php?category=4&new_title=コスメ・美容新着アイテム">更に表示 &nbsp;&nbsp;&nbsp;></a>
        </div>
    </div>
    <div class="ladies-item-row wow animate__animated animate__fadeInUp">
        <div class="ladies-item-row-group" style="width: 100%;">
            <?php
                $count = 0;
                foreach ($item->selectCategory(4) as $itemItem): ?>
                    <?php if (User::isInvalid($itemItem->owner_id)) {
                        continue;
                    } ?>
                    <?php if ($itemItem->enabled != 1) {
                        continue;
                    } ?>
                    <?php if (!$itemItem->owner_selling) {
                        continue;
                    } ?>
                    <?php
                    if ($count++ >= 4) {
                        break;
                    }
                    ?>
                    <a href="<?php echo HOME_URL; ?>/itemdetail.php?id=<?= $itemItem->id ?>&new_title=コスメ・美容新着アイテム" class="ladies-group-column">
                        <div class="ladies-group-column__img">
                            <?php 
                                // if(@file_get_contents($itemItem->getThumbnailUrl()) ): 
                            ?>
                                <img src="<?= $itemItem->getThumbnailUrl() ?>">
                            <?php 
                                // else: 
                            ?>
                                <!-- <img src="<?php echo HOME_URL; ?>/common/assets/img/no-image.png" alt="" > -->
                            <?php 
                                // endif; 
                            ?>
                            <div class="ladies-group-column__img-bottom"><?= $itemItem->title ?></div>
                        </div>
                        <div class="ladies-group-column__icon">
                            <!-- <img src="<?php echo HOME_URL; ?>/common/assets/img/common/icon/profile.png" alt=""> -->
                        </div>
                        <div class="ladies-group-column__price">
                            <p><span>¥<?php echo number_format($itemItem->price); ?></span></p>
                        </div>
                    </a>
                <?php endforeach; ?>
            <?php
                for ($i=1; $i <= (4 - $count) ; $i++) { 
                    echo '<div ></div>';
                }
            ?>
        </div>
    </div>



    <div class="ladies-item-title mt-50 wow animate__animated animate__fadeInUp">
        <div class="ladies-item-title__txt">
            <h3><span>エンタメ・ホビー新着アイテム</span></h3>
        </div>
        <div class="ladies-item-title__txt">
            <a href="<?php echo HOME_URL; ?>/itemlist.php?category=5&new_title=エンタメ・ホビー新着アイテム">更に表示 &nbsp;&nbsp;&nbsp;></a>
        </div>
    </div>
    <div class="ladies-item-row wow animate__animated animate__fadeInUp">
        <div class="ladies-item-row-group" style="width: 100%;">
            <?php
                $count = 0;
                foreach ($item->selectCategory(5) as $itemItem): ?>
                    <?php if (User::isInvalid($itemItem->owner_id)) {
                        continue;
                    } ?>
                    <?php if ($itemItem->enabled != 1) {
                        continue;
                    } ?>
                    <?php if (!$itemItem->owner_selling) {
                        continue;
                    } ?>
                    <?php
                    if ($count++ >= 4) {
                        break;
                    }
                    ?>
                    <a href="<?php echo HOME_URL; ?>/itemdetail.php?id=<?= $itemItem->id ?>&new_title=エンタメ・ホビー新着アイテム" class="ladies-group-column">
                        <div class="ladies-group-column__img">
                            <?php 
                                // if(@file_get_contents($itemItem->getThumbnailUrl()) ): 
                            ?>
                                <img src="<?= $itemItem->getThumbnailUrl() ?>">
                            <?php 
                                // else: 
                            ?>
                                <!-- <img src="<?php echo HOME_URL; ?>/common/assets/img/no-image.png" alt="" > -->
                            <?php 
                                // endif; 
                            ?>
                            <div class="ladies-group-column__img-bottom"><?= $itemItem->title ?></div>
                        </div>
                        <div class="ladies-group-column__icon">
                            <!-- <img src="<?php echo HOME_URL; ?>/common/assets/img/common/icon/profile.png" alt=""> -->
                        </div>
                        <div class="ladies-group-column__price">
                            <p><span>¥<?php echo number_format($itemItem->price); ?></span></p>
                        </div>
                    </a>
                <?php endforeach; ?>
            <?php
                for ($i=1; $i <= (4 - $count) ; $i++) { 
                    echo '<div ></div>';
                }
            ?>
        </div>
    </div>

<!--インテリア・雑貨-->
    <div class="ladies-item-title mt-50 wow animate__animated animate__fadeInUp">
        <div class="ladies-item-title__txt">
            <h3><span>インテリア・雑貨新着アイテム</span></h3>
        </div>
        <div class="ladies-item-title__txt">
            <a href="<?php echo HOME_URL; ?>/itemlist.php?category=6&new_title=インテリア・雑貨新着アイテム">更に表示 &nbsp;&nbsp;&nbsp;></a>
        </div>
    </div>
    <div class="ladies-item-row wow animate__animated animate__fadeInUp">
        <div class="ladies-item-row-group" style="width: 100%;">
            <?php
                $count = 0;
                foreach ($item->selectCategory(6) as $itemItem): ?>
                    <?php if (User::isInvalid($itemItem->owner_id)) {
                        continue;
                    } ?>
                    <?php if ($itemItem->enabled != 1) {
                        continue;
                    } ?>
                    <?php if (!$itemItem->owner_selling) {
                        continue;
                    } ?>
                    <?php
                    if ($count++ >= 4) {
                        break;
                    }
                    ?>
                    <a href="<?php echo HOME_URL; ?>/itemdetail.php?id=<?= $itemItem->id ?>&new_title=インテリア・雑貨新着アイテム" class="ladies-group-column">
                        <div class="ladies-group-column__img">
                            <?php 
                                // if(@file_get_contents($itemItem->getThumbnailUrl()) ): 
                            ?>
                                <img src="<?= $itemItem->getThumbnailUrl() ?>">
                            <?php 
                                // else: 
                            ?>
                                <!-- <img src="<?php echo HOME_URL; ?>/common/assets/img/no-image.png" alt="" > -->
                            <?php 
                                // endif; 
                            ?>
                            <div class="ladies-group-column__img-bottom"><?= $itemItem->title ?></div>
                        </div>
                        <div class="ladies-group-column__icon">
                            <!-- <img src="<?php echo HOME_URL; ?>/common/assets/img/common/icon/profile.png" alt=""> -->
                        </div>
                        <div class="ladies-group-column__price">
                            <p><span>¥<?php echo number_format($itemItem->price); ?></span></p>
                        </div>
                    </a>
                <?php endforeach; ?>
            <?php
                for ($i=1; $i <= (4 - $count) ; $i++) { 
                    echo '<div ></div>';
                }
            ?>
        </div>
    </div>

<!--家電-->
    <div class="ladies-item-title mt-50 wow animate__animated animate__fadeInUp">
        <div class="ladies-item-title__txt">
            <h3><span>家電新着アイテム</span></h3>
        </div>
        <div class="ladies-item-title__txt">
            <a href="<?php echo HOME_URL; ?>/itemlist.php?category=7&new_title=家電新着アイテム">更に表示 &nbsp;&nbsp;&nbsp;></a>
        </div>
    </div>
    <div class="ladies-item-row wow animate__animated animate__fadeInUp">
        <div class="ladies-item-row-group" style="width: 100%;">
            <?php
                $count = 0;
                foreach ($item->selectCategory(7) as $itemItem): ?>
                    <?php if (User::isInvalid($itemItem->owner_id)) {
                        continue;
                    } ?>
                    <?php if ($itemItem->enabled != 1) {
                        continue;
                    } ?>
                    <?php if (!$itemItem->owner_selling) {
                        continue;
                    } ?>
                    <?php
                    if ($count++ >= 4) {
                        break;
                    }
                    ?>
                    <a href="<?php echo HOME_URL; ?>/itemdetail.php?id=<?= $itemItem->id ?>&new_title=家電新着アイテム" class="ladies-group-column">
                        <div class="ladies-group-column__img">
                            <?php 
                                // if(@file_get_contents($itemItem->getThumbnailUrl()) ): 
                            ?>
                                <img src="<?= $itemItem->getThumbnailUrl() ?>">
                            <?php 
                                // else: 
                            ?>
                                <!-- <img src="<?php echo HOME_URL; ?>/common/assets/img/no-image.png" alt="" > -->
                            <?php 
                                // endif; 
                            ?>
                            <div class="ladies-group-column__img-bottom"><?= $itemItem->title ?></div>
                        </div>
                        <div class="ladies-group-column__icon">
                            <!-- <img src="<?php echo HOME_URL; ?>/common/assets/img/common/icon/profile.png" alt=""> -->
                        </div>
                        <div class="ladies-group-column__price">
                            <p><span>¥<?php echo number_format($itemItem->price); ?></span></p>
                        </div>
                    </a>
                <?php endforeach; ?>
            <?php
                for ($i=1; $i <= (4 - $count) ; $i++) { 
                    echo '<div ></div>';
                }
            ?>
        </div>
    </div>
<!--スポーツ・アウトドア-->
    <div class="ladies-item-title mt-50 wow animate__animated animate__fadeInUp">
        <div class="ladies-item-title__txt">
            <h3><span>スポーツ・アウトドア新着アイテム</span></h3>
        </div>
        <div class="ladies-item-title__txt">
            <a href="<?php echo HOME_URL; ?>/itemlist.php?category=8&new_title=スポーツ・アウトドア新着アイテム">更に表示 &nbsp;&nbsp;&nbsp;></a>
        </div>
    </div>
    <div class="ladies-item-row wow animate__animated animate__fadeInUp">
        <div class="ladies-item-row-group" style="width: 100%;">
            <?php
                $count = 0;
                foreach ($item->selectCategory(8) as $itemItem): ?>
                    <?php if (User::isInvalid($itemItem->owner_id)) {
                        continue;
                    } ?>
                    <?php if ($itemItem->enabled != 1) {
                        continue;
                    } ?>
                    <?php if (!$itemItem->owner_selling) {
                        continue;
                    } ?>
                    <?php
                    if ($count++ >= 4) {
                        break;
                    }
                    ?>
                    <a href="<?php echo HOME_URL; ?>/itemdetail.php?id=<?= $itemItem->id ?>&new_title=スポーツ・アウトドア新着アイテム" class="ladies-group-column">
                        <div class="ladies-group-column__img">
                            <?php 
                                // if(@file_get_contents($itemItem->getThumbnailUrl()) ): 
                            ?>
                                <img src="<?= $itemItem->getThumbnailUrl() ?>">
                            <?php 
                                // else: 
                            ?>
                                <!-- <img src="<?php echo HOME_URL; ?>/common/assets/img/no-image.png" alt="" > -->
                            <?php 
                                // endif; 
                            ?>
                            <div class="ladies-group-column__img-bottom"><?= $itemItem->title ?></div>
                        </div>
                        <div class="ladies-group-column__icon">
                            <!-- <img src="<?php echo HOME_URL; ?>/common/assets/img/common/icon/profile.png" alt=""> -->
                        </div>
                        <div class="ladies-group-column__price">
                            <p><span>¥<?php echo number_format($itemItem->price); ?></span></p>
                        </div>
                    </a>
                <?php endforeach; ?>
            <?php
                for ($i=1; $i <= (4 - $count) ; $i++) { 
                    echo '<div ></div>';
                }
            ?>
        </div>
    </div>
<!--自転車・バイク-->
    <div class="ladies-item-title mt-50 wow animate__animated animate__fadeInUp">
        <div class="ladies-item-title__txt">
            <h3><span>自転車・バイク新着アイテム</span></h3>
        </div>
        <div class="ladies-item-title__txt">
            <a href="<?php echo HOME_URL; ?>/itemlist.php?category=9&new_title=自転車・バイク新着アイテム">更に表示 &nbsp;&nbsp;&nbsp;></a>
        </div>
    </div>
    <div class="ladies-item-row wow animate__animated animate__fadeInUp">
        <div class="ladies-item-row-group" style="width: 100%;">
            <?php
                $count = 0;
                foreach ($item->selectCategory(9) as $itemItem): ?>
                    <?php if (User::isInvalid($itemItem->owner_id)) {
                        continue;
                    } ?>
                    <?php if ($itemItem->enabled != 1) {
                        continue;
                    } ?>
                    <?php if (!$itemItem->owner_selling) {
                        continue;
                    } ?>
                    <?php
                    if ($count++ >= 4) {
                        break;
                    }
                    ?>
                    <a href="<?php echo HOME_URL; ?>/itemdetail.php?id=<?= $itemItem->id ?>&new_title=自転車・バイク新着アイテム" class="ladies-group-column">
                        <div class="ladies-group-column__img">
                            <?php 
                                // if(@file_get_contents($itemItem->getThumbnailUrl()) ): 
                            ?>
                                <img src="<?= $itemItem->getThumbnailUrl() ?>">
                            <?php 
                                // else: 
                            ?>
                                <!-- <img src="<?php echo HOME_URL; ?>/common/assets/img/no-image.png" alt="" > -->
                            <?php 
                                // endif; 
                            ?>
                            <div class="ladies-group-column__img-bottom"><?= $itemItem->title ?></div>
                        </div>
                        <div class="ladies-group-column__icon">
                            <!-- <img src="<?php echo HOME_URL; ?>/common/assets/img/common/icon/profile.png" alt=""> -->
                        </div>
                        <div class="ladies-group-column__price">
                            <p><span>¥<?php echo number_format($itemItem->price); ?></span></p>
                        </div>
                    </a>
                <?php endforeach; ?>
            <?php
                for ($i=1; $i <= (4 - $count) ; $i++) { 
                    echo '<div ></div>';
                }
            ?>
        </div>
    </div>

<!--ハンドメイド-->
    <div class="ladies-item-title mt-50 wow animate__animated animate__fadeInUp">
        <div class="ladies-item-title__txt">
            <h3><span>ハンドメイド新着アイテム</span></h3>
        </div>
        <div class="ladies-item-title__txt">
            <a href="<?php echo HOME_URL; ?>/itemlist.php?category=10&new_title=ハンドメイド新着アイテム">更に表示 &nbsp;&nbsp;&nbsp;></a>
        </div>
    </div>
    <div class="ladies-item-row wow animate__animated animate__fadeInUp">
        <div class="ladies-item-row-group" style="width: 100%;">
            <?php
                $count = 0;
                foreach ($item->selectCategory(10) as $itemItem): ?>
                    <?php if (User::isInvalid($itemItem->owner_id)) {
                        continue;
                    } ?>
                    <?php if ($itemItem->enabled != 1) {
                        continue;
                    } ?>
                    <?php if (!$itemItem->owner_selling) {
                        continue;
                    } ?>
                    <?php
                    if ($count++ >= 4) {
                        break;
                    }
                    ?>
                    <a href="<?php echo HOME_URL; ?>/itemdetail.php?id=<?= $itemItem->id ?>&new_title=ハンドメイド新着アイテム" class="ladies-group-column">
                        <div class="ladies-group-column__img">
                            <?php 
                                // if(@file_get_contents($itemItem->getThumbnailUrl()) ): 
                            ?>
                                <img src="<?= $itemItem->getThumbnailUrl() ?>">
                            <?php 
                                // else: 
                            ?>
                                <!-- <img src="<?php echo HOME_URL; ?>/common/assets/img/no-image.png" alt="" > -->
                            <?php 
                                // endif; 
                            ?>
                        </div>

                        <div class="ladies-group-column__img-bottom"><?= $itemItem->title ?></div>
                        <div class="ladies-group-column__icon">
                            <!-- <img src="<?php echo HOME_URL; ?>/common/assets/img/common/icon/profile.png" alt=""> -->
                        </div>
                        <div class="ladies-group-column__price">
                            <p><span>¥<?php echo number_format($itemItem->price); ?></span></p>
                        </div>
                    </a>
                <?php endforeach; ?>
            <?php
                for ($i=1; $i <= (4 - $count) ; $i++) { 
                    echo '<div ></div>';
                }
            ?>
        </div>
    </div>




</section>
<section class="newsletter p-top__newsletter">
    <div class="newsletter-content">
        <div class="newsletter-content-item wow animate__animated animate__fadeInLeft">
            <div class="newsletter-item-title">
                <div class="newsletter-item__txt">
                    <h3><span>最新の購入希望依頼</span></h3>
                </div>
                <div class="newsletter-item__txt">
                    <a href="<?php echo HOME_URL; ?>/user/purase_request_info_list.php">更に表示 &nbsp;&nbsp;&nbsp;></a>
                </div>
            </div>
            <?php
            $count = 0;
            foreach ($request->selectAll() as $requestItem): ?>
                <?php if (User::isInvalid($requestItem->user_id)) {
                    continue;
                } ?>
                <?php
                if ($requestItem->enabled != 1) {
                    continue;
                }
                if ($count++ >= MAX_REQUEST_LIST_COUNT) {
                    break;
                }
                ?>
                    <a class="newsletter-item-content__link" href="
                    <?php echo HOME_URL."/user/purase_request_info.php?id=".$requestItem->id ?>">
                        <div class="newsletter-item-content__link-date"><?= toDateStringFromMySqlDatetime($requestItem->regist_date) ?></div>
                        <div class="newsletter-item-content__link-txt" style="line-break: anywhere;"><?= $requestItem->title ?></div>
                    </a>

                        <!-- <a class="newsletter-item-content__link" href="<?php echo HOME_URL; ?>/information_details.php?id=<?= $infoItem->id ?>">
                            <i class="fa fa-chevron-right" aria-hidden="true"></i>  
                        <div class="newsletter-item-content__link-date"><?= toDateStringFromMySqlDatetime($requestItem->regist_date) ?></div>
                        <div class="newsletter-item-content__link-txt"><?= $requestItem->title ?></div>
                        </a> -->
                    <?php if (isNew($requestItem->regist_date)): ?>
                        <!-- <span class="newIcon">NEW</span> -->
                    <?php endif; ?>
            <?php endforeach; ?><!-- 
            <div class="newsletter-item-content">
                <a href="#" class="newsletter-item-content__link">
                    <div class="newsletter-item-content__link-date">2021/3/11</div>
                    <div class="newsletter-item-content__link-txt">◯◯アイテムの購入希望◯◯アイテムの購入希望</div>
                </a>
                <a href="#" class="newsletter-item-content__link">
                    <div class="newsletter-item-content__link-date">2021/3/11</div>
                    <div class="newsletter-item-content__link-txt">◯◯アイテムの購入希望◯◯アイテムの購入希望</div>
                </a>
                <a href="#" class="newsletter-item-content__link">
                    <div class="newsletter-item-content__link-date">2021/3/11</div>
                    <div class="newsletter-item-content__link-txt">◯◯アイテムの購入希望◯◯アイテムの購入希望</div>
                </a>
                <a href="#" class="newsletter-item-content__link">
                    <div class="newsletter-item-content__link-date">2021/3/11</div>
                    <div class="newsletter-item-content__link-txt">◯◯アイテムの購入希望◯◯アイテムの購入希望</div>
                </a>
                <a href="#" class="newsletter-item-content__link">
                    <div class="newsletter-item-content__link-date">2021/3/11</div>
                    <div class="newsletter-item-content__link-txt">◯◯アイテムの購入希望◯◯アイテムの購入希望</div>
                </a>
            </div> -->
        </div>
        <div class="newsletter-content-item wow animate__animated animate__fadeInRight">
            <div class="newsletter-item-title">
                <div class="newsletter-item__txt">
                    <h3><span>お知らせ</span></h3>
                </div>
                <div class="newsletter-item__txt">
                <a href="<?php echo HOME_URL; ?>/user/notice.php">更に表示 &nbsp;&nbsp;&nbsp;></a>
                </div>
            </div>
            <?php
                $count = 0;
                foreach ($info->selectAll() as $infoItem) : ?>
                    <?php
                    if ($count++ >= MAX_INFOMATION_LIST_COUNT) {
                        break;
                    }
                    ?>
                    <!-- <li> -->
                        <a class="newsletter-item-content__link" href="<?php echo HOME_URL; ?>/user/notice_show.php?id=<?= $infoItem->id ?>">
                            <!-- <i class="fa fa-chevron-right" aria-hidden="true"></i>   -->
                        <div class="newsletter-item-content__link-date"><?= toDateStringFromMySqlDatetime($infoItem->regist_date) ?></div>
                        <div class="newsletter-item-content__link-txt" style="line-break: anywhere;"><?= $infoItem->title ?></div>
                        </a>
                        <?php if (isNew($infoItem->regist_date)): ?>
                            <!-- <span class="newIcon">NEW</span> -->
                        <?php endif; ?>
                    <!-- </li> -->
            <?php endforeach; ?>
<!--             <div class="">
                <a href="#" class="newsletter-item-content__link">
                    <div class="newsletter-item-content__link-date">2021/3/11</div>
                    <div class="newsletter-item-content__link-txt">お知らせあいうえお</div>
                </a>
                <a href="#" class="newsletter-item-content__link">
                    <div class="newsletter-item-content__link-date">2021/3/11</div>
                    <div class="newsletter-item-content__link-txt">お知らせあいうえお</div>
                </a>
                <a href="#" class="newsletter-item-content__link">
                    <div class="newsletter-item-content__link-date">2021/3/11</div>
                    <div class="newsletter-item-content__link-txt">お知らせあいうえお</div>
                </a>
                <a href="#" class="newsletter-item-content__link">
                    <div class="newsletter-item-content__link-date">2021/3/11</div>
                    <div class="newsletter-item-content__link-txt">お知らせあいうえお</div>
                </a>
                <a href="#" class="newsletter-item-content__link">
                    <div class="newsletter-item-content__link-date">2021/3/11</div>
                    <div class="newsletter-item-content__link-txt">お知らせあいうえお</div>
                </a>
            </div> -->
        </div>
    </div>
</section>
<script type="text/javascript">
const CLASSNAME = "-visible";
const TIMEOUT = 1500;
const $target = $(".top__txt");

setInterval(() => {
  $target.addClass(CLASSNAME);
  setTimeout(() => {
    $target.removeClass(CLASSNAME);
  }, TIMEOUT);
}, TIMEOUT * 2);

</script>
<?php include('footer.php'); ?> 
<script>
    if ($( window ).width() > 768){
        $('#new_slider').slick({
            slidesToShow: 4,
            slidesToScroll: 1,
            autoplay: false,
            autoplaySpeed: 2000,
        });
        $('#highlight_slider').slick({
            slidesToShow: 4,
            slidesToScroll: 1,
            autoplay: false,
            autoplaySpeed: 2000,
        });
    } else {
        $('#new_slider').slick({
            slidesToShow: 3,
            slidesToScroll: 1,
            autoplay: false,
            autoplaySpeed: 2000,
        });
        $('#highlight_slider').slick({
            slidesToShow: 3,
            slidesToScroll: 1,
            autoplay: false,
            autoplaySpeed: 2000,
        });
    }
</script>