<?php include_once(__DIR__ . '/common/util.php'); ?>
<?php /* サイトタイトル用　パーセント表示など */
$system_config = SystemConfig::select(); ?>
<?php /*スマホ判定処理 これも使ってないので不要 */
if (!isset($item)) {
    $item = new Item();
}
if (isset($_SESSION['login'])) {
    $user = $_SESSION['login'];
}
?>
<!doctype html>
<html lang="ja">

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="keywords" content="" />
    <meta name="description" content="<?php echo $description_page;?>" />
    <link rel="canonical" href="">
    <?php if (isset($title_page)) { ?>
    <title><?= $title_page ?></title>
    <?php } else { ?>
    <title>Index</title>
    <?php } ?>
    <!-- Global site tag (gtag.js) - Google Analytics -->
    <script async src="https://www.googletagmanager.com/gtag/js?id=G-8H0QENYL0L"></script>
    <script>
        window.dataLayer = window.dataLayer || [];
        function gtag(){dataLayer.push(arguments);}
        gtag('js', new Date());

        gtag('config', 'G-8H0QENYL0L');
    </script>
    <link rel="icon" type="image/vnd.microsoft.icon" href="">
    <link href="https://fonts.googleapis.com/css2?family=Castoro&display=swap" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css?family=Noto+Serif+JP:400,700&display=swap&subset=japanese" rel="stylesheet">
    <!-- OGP -->
    <meta property="og:site_name" content="">
    <meta property="og:title" content="">
    <meta property="og:description" content="">
    <meta property="og:type" content="website">
    <meta property="og:url" content="">
    <meta property="og:image" content="">
    <meta name="twitter:card" content="summary">
    <meta name="twitter:site" content="">
    <meta name="twitter:image" content="">

    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
    <link href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css" rel="stylesheet" type="text/css" />
    <link rel="stylesheet" href="<?php echo HOME_URL; ?>/common/assets/icons/icons.css">
    <link rel="stylesheet" href="<?php echo HOME_URL; ?>/common/assets/css/common.css">
    <link rel="stylesheet" href="<?php echo HOME_URL; ?>/common/assets/css/style.css">
    <link rel="stylesheet" href="<?php echo HOME_URL; ?>/common/assets/css/responsive.css">
    <link rel="stylesheet" href="<?php echo HOME_URL; ?>/common/assets/css/add_cheat.css">
    <link rel="stylesheet" href="<?php echo HOME_URL; ?>/common/assets/css/add_cheat2.css">
    <link rel="stylesheet" href="<?php echo HOME_URL; ?>/common/assets/css/add_cheat3.css">
    <link rel="stylesheet" href="<?php echo HOME_URL; ?>/common/assets/css/add_quynh_fix.css?v=<?php echo strtotime('now');?>">
</head>
<body> 
<?php ini_set('display_errors', "On"); ?>
<div class="com-header bg-white">
    <div class="com-header__logo">
    <a href="<?php echo HOME_URL; ?>"><img src="<?php echo HOME_URL; ?>/common/assets/img/common/logo.png" alt=""></a>
    </div>
    <div class="com-header__search sp-hide">
        <form action="<?php echo HOME_URL; ?>/itemlist.php" method="post">
            <input type="text" name="search"
                           value="<?= empty($_POST['search']) ? '' : $_POST['search'] ?>" placeholder="キーワードを入力">
            <button type="submit"><i class="fa fa-search"></i></button>
        </form>
    </div>

        <?php //if(isLogin()): ?>
        <?php $url = $_SERVER["REQUEST_URI"]; ?>
            <div class="main-header-link row mr-0 <?=isLogin() ? 'row-btn-welcom' : '' ?>">
        <?php if(isLogin() && strstr($url, "admin") == false): ?>
            <div class="col-md-12 text-center">
                <h2>ようこそ<?= ($user->nickname != null) ? $user->nickname : $user->name ?>さん！</h2>
            </div>
        <?php endif; ?>
        <div class="col-md-12 row mr-0 <?= isAdminUser() ? '' : (isLogin() ? 'under-welcome' : '') ?> <?=isLogin() ? 'row-btn-welcom' : '' ?>" style="<?=(strstr($url, "admin") == true) ? 'margin: auto;justify-content: center;' : '' ?>">
        <?php
        if (strstr($url, "admin") == true):?>
            <div class="main-header-link__txt <?= (isAdminUser()) ? 'format-col' : '' ?>">
                <div class="main-header-link__txt-item">
                    <span>
                        <svg xmlns="http://www.w3.org/2000/svg" width="20" height="20" viewBox="0 0 8 8">
                            <path d="M5.5 0c-1.38 0-2.5 1.12-2.5 2.5 0 .16 0 .32.03.47l-3.03 3.03v2h3v-2h2v-1l.03-.03c.15.03.31.03.47.03 1.38 0 2.5-1.12 2.5-2.5s-1.12-2.5-2.5-2.5zm.5 1c.55 0 1 .45 1 1s-.45 1-1 1-1-.45-1-1 .45-1 1-1z" fill="white"/>
                        </svg>
                    </span>
                    <span><a href="<?php echo HOME_URL; ?>/admin/">管理画面</a></span>
                </div>
            </div>
        <?php elseif (isLogin()): ?>
            <?php if (isAdminUser()): ?>
                <div class="main-header-link__txt <?= (isAdminUser()) ? 'format-col' : '' ?>">
                    <div class="main-header-link__txt-item">
                        <span>
                            <svg xmlns="http://www.w3.org/2000/svg" width="20" height="20" viewBox="0 0 8 8">
                                <path d="M4 0c-1.1 0-2 1.12-2 2.5s.9 2.5 2 2.5 2-1.12 2-2.5-.9-2.5-2-2.5zm-2.09 5c-1.06.05-1.91.92-1.91 2v1h8v-1c0-1.08-.84-1.95-1.91-2-.54.61-1.28 1-2.09 1-.81 0-1.55-.39-2.09-1z" fill="white"/>
                            </svg>
                        </span>
                        <span><a href="<?php echo HOME_URL; ?>/admin/">管理画面へ</a></span>
                    </div>
                </div>
            <?php endif; ?>
            <div class="main-header-link__txt <?= (isAdminUser()) ? 'format-col' : '' ?>">
                <a href="<?php echo HOME_URL; ?>/login.php">
                    <div class="main-header-link__txt-item">
                        <span>
                            <svg xmlns="http://www.w3.org/2000/svg" width="20" height="20" viewBox="0 0 8 8">
                                <path d="M5.5 0c-1.38 0-2.5 1.12-2.5 2.5 0 .16 0 .32.03.47l-3.03 3.03v2h3v-2h2v-1l.03-.03c.15.03.31.03.47.03 1.38 0 2.5-1.12 2.5-2.5s-1.12-2.5-2.5-2.5zm.5 1c.55 0 1 .45 1 1s-.45 1-1 1-1-.45-1-1 .45-1 1-1z" fill="white"/>
                            </svg>                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                               
                        </span>
                        <span><?= isLogin() ? 'ログアウト' : 'ログイン' ?></span>
                    </div>
                </a>
            </div>
            <div class="main-header-link__txt <?= (isAdminUser()) ? 'format-col' : '' ?>">
                <a href="<?php echo HOME_URL; ?>/user/mypage.php">
                    <div class="main-header-link__txt-item">
                        <span>
                            <svg xmlns="http://www.w3.org/2000/svg" width="20" height="20" viewBox="0 0 8 8">
                                <path d="M4 0c-1.1 0-2 1.12-2 2.5s.9 2.5 2 2.5 2-1.12 2-2.5-.9-2.5-2-2.5zm-2.09 5c-1.06.05-1.91.92-1.91 2v1h8v-1c0-1.08-.84-1.95-1.91-2-.54.61-1.28 1-2.09 1-.81 0-1.55-.39-2.09-1z" fill="white"/>
                            </svg>
                        </span>
                        <span>マイページ</span>
                    </div>
                </a>
            </div>
        <?php elseif (!isLogin()): ?>
            <div class="main-header-link__txt">
                <a href="<?php echo HOME_URL; ?>/login.php">
                    <div class="main-header-link__txt-item">
                        <span>
                            <svg xmlns="http://www.w3.org/2000/svg" width="20" height="20" viewBox="0 0 8 8">
                                <path d="M5.5 0c-1.38 0-2.5 1.12-2.5 2.5 0 .16 0 .32.03.47l-3.03 3.03v2h3v-2h2v-1l.03-.03c.15.03.31.03.47.03 1.38 0 2.5-1.12 2.5-2.5s-1.12-2.5-2.5-2.5zm.5 1c.55 0 1 .45 1 1s-.45 1-1 1-1-.45-1-1 .45-1 1-1z" fill="white"/>
                            </svg>
                        </span>
                        <span><?= isLogin() ? 'ログアウト' : 'ログイン' ?></span>
                    </div>
                </a>
            </div>
            <div class="main-header-link__txt">
                <a href="<?php echo HOME_URL; ?>/user/userentry.php?step=1">
                    <div class="main-header-link__txt-item">
                        <span>
                            <svg xmlns="http://www.w3.org/2000/svg" width="20" height="20" viewBox="0 0 8 8">
                                <path d="M4 0c-1.1 0-2 1.12-2 2.5s.9 2.5 2 2.5 2-1.12 2-2.5-.9-2.5-2-2.5zm-2.09 5c-1.06.05-1.91.92-1.91 2v1h8v-1c0-1.08-.84-1.95-1.91-2-.54.61-1.28 1-2.09 1-.81 0-1.55-.39-2.09-1z" fill="white"/>
                            </svg>
                        </span>
                        <span>新規会員登録</span>
                    </div>
                </a>
            </div>
        <?php endif; ?>
        </div>
    </div>
        <div class="side-bar c-navSP">
                <button class="btn-menu">
                    <span></span>
                    <span></span>
                    <span></span>
                </button>
                <div class="side-bar-content">
                <?php if (!isLogin()){ ?>
                    <div class="side-bar-content__item">
                        <a href="<?php echo HOME_URL; ?>/user/userentry.php?step=1">新規会員登録</a>
                    </div>
                <?php };?>
                <!-- 
                    <div class="side-bar-content__item">
                        <a href="">管理者TOP</a>
                    </div> -->
                    <div class="side-bar-content__item" data-toggle="collapse" data-target="#confirm_list">
                        <p>出品する</p>
                    </div>
                    <div id="confirm_list" class="collapse confirm__list">
                        <a href="<?php echo HOME_URL; ?>/user/sell.php">商品登録</a>出品中のアイテム
                        <a href="<?php echo HOME_URL; ?>/user/sellinglist.php">出品中のアイテム</a>
                        <a href="<?php echo HOME_URL; ?>/user/wait_approval.php">出品許可待ち</a>
                        <!--<a href="<?php echo HOME_URL; ?>/user/untraded_items.php">未取引のアイテム</a>-->
                        <a href="<?php echo HOME_URL; ?>/user/all_item_list.php">全てのアイテム</a>
                        <a href="<?php echo HOME_URL; ?>/user/negotiation_list_sell.php">質問・交渉中</a>
                        <a href="<?php echo HOME_URL; ?>/user/soldout_transaction.php">販売済・取引中</a>
                    </div>
                    <div class="side-bar-content__item" data-toggle="collapse" data-target="#item_list">
                        <p>購入する</p>
                    </div>
                    <div id="item_list" class="collapse confirm__list">
                        <a href="<?php echo HOME_URL; ?>/user/question_negoty.php">購入・交渉中</a>
                        <a href="<?php echo HOME_URL; ?>/user/exhibited_list.php">購入済み</a>
                        <!-- <a href="#">お気に入り一覧</a> -->
                        <a href="<?php echo HOME_URL; ?>/user/request.php">購入希望募集投稿</a>
                        <a href="<?php echo HOME_URL; ?>/user/request_list.php">購入希望募集履歴</a>
                    </div>
                    <div class="side-bar-content__item" data-toggle="collapse" data-target="#other_list">
                        <p>アカウント</p>
                    </div>
                    <div id="other_list" class="collapse confirm__list">
                        <a href="<?php echo HOME_URL; ?>/user/userinfo.php">ユーザー情報変更</a>
                        <a href="<?php echo HOME_URL; ?>/user/account_number_info.php">口座情報変更</a>
                        <a href="<?php echo HOME_URL; ?>/user/cash_flow_history.php">出金申請/履歴</a>
                       <!--  <a href="<?php echo HOME_URL; ?>/user/userinfo.php">評価一覧</a>
                        <a href="<?php echo HOME_URL; ?>/user/userinfo.php">ブラックリスト登録</a> -->
                        <a href="<?php echo HOME_URL; ?>/user/upload_identification.php">本人確認書類の提出</a>
                        <!-- <a href="">退会について</a> -->
                    </div>
                    <div class="side-bar-content__item">
                        <a href="<?php echo HOME_URL; ?>/login.php"><?= isLogin() ? 'ログアウト' : 'ログイン' ?></a>
                    </div>
                </div>
            </div>
        <?php //elseif(!isLogin()): ?>
<!-- 
        <div class="com-header-link">
            <div class="com-header-link__txt">
                <div class="com-header-link__txt-item">
                    <span>
                        <svg xmlns="http://www.w3.org/2000/svg" width="20" height="20" viewBox="0 0 8 8">
                            <path d="M5.5 0c-1.38 0-2.5 1.12-2.5 2.5 0 .16 0 .32.03.47l-3.03 3.03v2h3v-2h2v-1l.03-.03c.15.03.31.03.47.03 1.38 0 2.5-1.12 2.5-2.5s-1.12-2.5-2.5-2.5zm.5 1c.55 0 1 .45 1 1s-.45 1-1 1-1-.45-1-1 .45-1 1-1z" fill="white"/>
                        </svg>
                    </span>
                    <span><a href="<?php echo HOME_URL; ?>/login.php"><?= isLogin() ? 'ログアウト' : 'ログイン' ?></a></span>
                </div>
            </div>
            <div class="com-header-link__txt">
                <div class="com-header-link__txt-item">
                    <span>
                        <svg xmlns="http://www.w3.org/2000/svg" width="20" height="20" viewBox="0 0 8 8">
                            <path d="M4 0c-1.1 0-2 1.12-2 2.5s.9 2.5 2 2.5 2-1.12 2-2.5-.9-2.5-2-2.5zm-2.09 5c-1.06.05-1.91.92-1.91 2v1h8v-1c0-1.08-.84-1.95-1.91-2-.54.61-1.28 1-2.09 1-.81 0-1.55-.39-2.09-1z" fill="white"/>
                        </svg>
                    </span>
                    <span><a href="<?php echo HOME_URL; ?>/user/userentry.php">新規会員登録</a></span>
                </div>
            </div>
        </div> -->
        <?php //endif; ?>
</div>

<?php //保存・エラーメッセージ
$msg = getMessage();
if (count($msg) > 0 && !empty($msg['msg'])) {
    if ($msg['pop']) {
        echo '<script>window.alert("' . $msg['msg'] . '");</script>';
    } else {
        echo "<div class='container'><div class='alert alert-success'><strong>" . $msg['msg'] . "</strong></div></div>";
    }
}
?>