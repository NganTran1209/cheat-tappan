<?php include_once(__DIR__ . '/common/util.php'); ?>
<?php

$group = 0;
$sort = 0;
$user_id = $_GET['user_id'];
if (!empty($_GET['group'])) {
    $group = $_GET['group'];
}
if (!empty($_GET['sort'])) {
    $sort = $_GET['sort'];
}

$order = new Order();
$evoluteList = $order->selectFromUserAll($user_id);

if ($sort != 0) {
    usort($evoluteList, function ($lhs, $rhs) {
        global $sort;
        $lEvolute = 0;
        $rEvolute = 0;
        if ($lhs->user_id == $user_id) {
            $lEvolute = $lhs->buyer_evaluate;
        } else {
            $lEvolute = $lhs->seller_evaluate;
        }
        if ($rhs->user_id == $user_id) {
            $rEvolute = $rhs->buyer_evaluate;
        } else {
            $rEvolute = $rhs->seller_evaluate;
        }
        if ($lEvolute != $rEvolute) {
            if ($sort == $lEvolute) {
                return -1;
            }
            if ($sort == $rEvolute) {
                return 1;
            }
        }
        return 0;
    });
}

$ownerEvaluateList = $order->selectFromOwner($user_id);
$userEvaluateList = $order->selectFromUser($user_id);

$evaluate = new Evaluate();
$evaluate->selectSummary($user_id);

$owner = new User();
$owner->select($user_id);

?>

<?php include(__DIR__ . '/header.php'); ?>
<div class="container mt-20">
    <div class="row">
        <div class="col-md-9 mainContents">
            <div class="bg-inner">
                <h1><?= $owner->getViewNicknameOrId() ?>の評価<?php //echo $owner->hash_id; ?></h1>
                <div class="small text-right"><a href="<?php echo HOME_URL; ?>/itemlist.php?owner_id=<?= $owner->id ?>">出品者の商品一覧はこちら</a>
                </div>
                <div class="row">
                    <div class="col-sm-4">
                        <a class="btn btn-block btn-outline-dark btn-sm"
                           href="<?php echo HOME_URL; ?>/evaluate.php?user_id=<?= $user_id ?>&group=0&sort=0">全ての評価</a>
                    </div>
                    <div class="col-sm-4">
                        <a class="btn btn-block btn-outline-dark btn-sm"
                           href="<?php echo HOME_URL; ?>/evaluate.php?user_id=<?= $user_id ?>&group=1&sort=<?= $sort ?>">購入の評価</a>
                    </div>
                    <div class="col-sm-4">
                        <a class="btn btn-block btn-outline-dark btn-sm"
                           href="<?php echo HOME_URL; ?>/evaluate.php?user_id=<?= $user_id ?>&group=2&sort=<?= $sort ?>">出品の評価</a>
                    </div>
                </div>

                <div class="margin-20">
                    <div class="text-center">
                        総合評価：<?= $evaluate->point ?>(
                        <i class="fas fa-smile mr-1"></i><?= $evaluate->good_count ?>&nbsp;
                        <i class="fas fa-meh mr-1"></i><?= $evaluate->normal_count ?>&nbsp;
                        <i class="fas fa-frown mr-1"></i><?= $evaluate->bad_count ?>
                        )
                    </div>
                    <div class="evaluate-attention">
                        <strong>総合評価の見方</strong><br>
                        ・取引相手から「良い」の評価は＋1、「普通」の評価は＋0、「悪い」の評価は−1となります。<br>
                        ・例えば「良い<i class="fas fa-smile mr-1"></i>」が1、「悪い<i class="fas fa-frown mr-1"></i>」が1の場合、総合評価は±0になります。
                    </div>
                    <hr class="hr">
                </div>
                <div class="text-right mb-2">
                    <i class="fa fa-sort" aria-hidden="true"></i>
                    <a class="sortButton"
                       href="<?php echo HOME_URL; ?>/evaluate.php?user_id=<?= $user_id ?>&group=<?= $group ?>&sort=1">良い</a>
                    <a class="sortButton"
                       href="<?php echo HOME_URL; ?>/evaluate.php?user_id=<?= $user_id ?>&group=<?= $group ?>&sort=2">普通</a>
                    <a class="sortButton"
                       href="<?php echo HOME_URL; ?>/evaluate.php?user_id=<?= $user_id ?>&group=<?= $group ?>&sort=3">悪い</a>
                </div>

                <?php foreach ($evoluteList as $order): ?>
                    <?php
                    $user = new User();
                    $user->select($order->user_id);
                    ?>

                    <?php if ($order->user_id != $user_id): ?>
                        <?php if ($group == 1) {
                            continue;
                        } ?>
                        <?php if ($order->seller_evaluate == 0) {
                            continue;
                        } ?>
                        <?php if ($sort != 0 && $order->seller_evaluate != $sort) {
                            continue;
                        } ?>
                        <div class="row mb-4">
                            <div class="col-md-3">
                                <div class="balloonUser">
                                    <!-- <a href="--><?php //echo HOME_URL; ?><!--/evaluate.php?user_id=-->
                                    <?php //echo $order->user_id; ?><!--">-->
                                    <?php //echo $order->user->hash_id; ?><!--</a> -->
                                    <a href="<?php echo HOME_URL; ?>/user/profile/<?= $order->user->hash_id ?>"> <?= $order->user->getViewNicknameOrId() ?></a>
                                    <div class="commenter">
                                        評価：
                                        <?php if (Evaluate::toEvaluateImageTag($order->seller_evaluate) == Evaluate::toEvaluateImageTag(1)): ?>
                                            <i class="fas fa-smile mr-1"></i>
                                        <?php elseif (Evaluate::toEvaluateImageTag($order->seller_evaluate) == Evaluate::toEvaluateImageTag(2)): ?>
                                            <i class="fas fa-meh mr-1"></i>
                                        <?php elseif (Evaluate::toEvaluateImageTag($order->seller_evaluate) == Evaluate::toEvaluateImageTag(3)): ?>
                                            <i class="fas fa-frown mr-1"></i>
                                        <?php endif; ?>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-9">
                                <div class="bg-comment balloon balloonB">
                                    <div class="balloonComment">
                                        <div class="">
                                            <a href="<?php echo HOME_URL; ?>/itemdetail.php?id=<?= $order->item_id ?>"><?= $order->item_title ?></a>
                                        </div>
                                        <?= empty($order->seller_comment) ? 'コメントはありません' : nl2br($order->seller_comment) ?>
                                    </div>
                                    <div class="balloonTime">
                                        <i class="far fa-clock mr-2"></i><?= $order->regist_date ?>
                                    </div>
                                </div>
                            </div>
                        </div>
                    <?php else: ?>
                        <?php if ($group == 2) {
                            continue;
                        } ?>
                        <?php if ($order->buyer_evaluate == 0) {
                            continue;
                        } ?>
                        <?php if ($sort != 0 && $order->buyer_evaluate != $sort) {
                            continue;
                        } ?>
                        <div class="row mb-4">
                            <div class="col-md-3">
                                <div class="balloonUser">
                                    <!--<a href="--><?php //echo HOME_URL; ?><!--/evaluate.php?user_id=-->
                                    <?php //echo $order->owner_id; ?><!--">-->
                                    <?php //echo $order->owner->hash_id; ?><!--</a>-->
                                    <a href="<?php echo HOME_URL; ?>/user/profile/<?= $order->owner->hash_id ?>"><?= $order->owner->getViewNicknameOrId() ?></a>
                                    <div class="commenter">
                                        評価：
                                        <?php if (Evaluate::toEvaluateImageTag($order->buyer_evaluate) == Evaluate::toEvaluateImageTag(1)): ?>
                                            <i class="fas fa-smile mr-1"></i>
                                        <?php elseif (Evaluate::toEvaluateImageTag($order->buyer_evaluate) == Evaluate::toEvaluateImageTag(2)): ?>
                                            <i class="fas fa-meh mr-1"></i>
                                        <?php elseif (Evaluate::toEvaluateImageTag($order->buyer_evaluate) == Evaluate::toEvaluateImageTag(3)): ?>
                                            <i class="fas fa-frown mr-1"></i>
                                        <?php endif; ?>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-9">
                                <div class="bg-comment balloon balloonA">
                                    <div class="balloonComment">
                                        <div class="">
                                            <a href="<?php echo HOME_URL; ?>/itemdetail.php?id=<?= $order->item_id ?>"><?= $order->item_title ?></a>
                                        </div>
                                        <?= empty($order->buyer_comment) ? 'コメントはありません' : nl2br($order->buyer_comment) ?>
                                    </div>
                                    <div class="balloonTime">
                                        <i class="far fa-clock mr-2"></i><?= $order->regist_date ?>
                                    </div>
                                </div>
                            </div>
                        </div>
                    <?php endif; ?>
                <?php endforeach; ?>
            </div>
        </div>
        <div class="col-md-3 sideContents">
            <?php include('sidebar.php'); ?>
        </div>
    </div>
</div>

<?php include('footer.php'); ?>




