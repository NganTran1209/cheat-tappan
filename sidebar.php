<div class="bg-white">
    <div class="searchWrap">
        <?php if (!isset($top_flag) || empty($top_flag)): ?>
        <form action="<?php echo HOME_URL; ?>/itemlist.php" method="post">
            <div class="input-group">
                <input type="text" class="form-control" name="search"
                       value="<?= empty($_POST['search']) ? '' : $_POST['search'] ?>" placeholder="キーワードを入力">
                <span class="input-group-append"><button class="btn btn-secondary" type="submit"><i class="fa fa-search"></i></button></span>
            </div>
        </form>
        <?php endif; ?>
        <div class="my-3">
            <a class="btn btn-block btn-danger mb-2" href="<?php echo HOME_URL; ?>/pages/sellinfo.php">売りたい方</a>
            <a class="btn btn-block btn-info mb-2" href="<?php echo HOME_URL; ?>/pages/buyinfo.php">買いたい方</a>
        </div>
    </div>
    <div class="categoryWrap">
        <h4>カテゴリー</h4>
        <div class="list-group list-group-flush">
            <?php foreach (selectCodeList('selectCategory') as $category): ?>
                <?php if ($category['enabled']) :?>
                <a class="list-group-item"
                   href="<?php echo HOME_URL; ?>/itemlist.php?category=<?= $category['id'] ?>"><?= $category['name'] ?></a>
                <?php endif;?>
            <?php endforeach; ?>
        </div>
    </div>
</div>
<!--<div class="my-4">-->
<!--    <a href="https://elefantinc.com/ccsystem"><img class="img-fluid" src="--><?php //echo HOME_URL; ?><!--/images/255x150.jpg" alt="フリマ・スキル販売が簡単に運営・構築できる"></a>-->
<!--</div>-->
