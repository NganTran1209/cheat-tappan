<?php 
$title_page = '全ての評価';
$description_page = '欲しい商品を画像で確認・届いた商品を画像で照合！気持ち良い取引が叶うオンラインマーケット。';
 ?>
 <?php include_once(__DIR__ . '/common/util.php'); ?>
<?php

$group = 0;
$sort = 0;
$user_id = $_GET['user_id'];
if (!empty($_GET['group'])) {
    $group = $_GET['group'];
}
if (!empty($_GET['sort'])) {
    $sort = $_GET['sort'];
}

$order = new Order();
$evoluteList = $order->selectFromUserAll($user_id);

if ($sort != 0) {
    usort($evoluteList, function ($lhs, $rhs) {
        global $sort;
        $lEvolute = 0;
        $rEvolute = 0;
        $user_id = $_GET['user_id'];
        
        if ($lhs->user_id == $user_id) {
            $lEvolute = $lhs->buyer_evaluate;
        } else {
            $lEvolute = $lhs->seller_evaluate;
        }
        if ($rhs->user_id == $user_id) {
            $rEvolute = $rhs->buyer_evaluate;
        } else {
            $rEvolute = $rhs->seller_evaluate;
        }
        if ($lEvolute != $rEvolute) {
            if ($sort == $lEvolute) {
                return -1;
            }
            if ($sort == $rEvolute) {
                return 1;
            }
        }
        return 0;
    });
}

$ownerEvaluateList = $order->selectFromOwner($user_id);
$userEvaluateList = $order->selectFromUser($user_id);

$evaluate = new Evaluate();
$evaluate->selectSummary($user_id);

$owner = new User();
$owner->select($user_id);

?>

<?php include('user_header.php'); ?>
<div class="com-header-top">
    <div class="com-header-top__img">
        <img src="<?php echo HOME_URL; ?>/common/assets/img/mypage/top-bg.png" alt="">
    </div>
    <div class="com-header-top__path">
        <p><span><a href="<?php echo HOME_URL; ?>/" class="clr-yel">トップページ</a></span><span> > </span><span><a
                    href="<?php echo HOME_URL; ?>/user/mypage.php" class="clr-yel">マイページ</a></span><span> >
            </span><span><?= $owner->getViewNicknameOrId() ?>の評価</span></p>
    </div>
    <div class="com-header-top__txt">
        <p class="clr-white">マイページ</p>
    </div>
</div>
<div class="com-container bg-yellow">
    <?php include('./user/usersidebar.php'); ?>
    <div class="com-content">
        <div class="content-title">
            <h3><span><?= $owner->getViewNicknameOrId() ?>の評価</span></h3>
        </div>
        <div class="qnego-sm-title">
            <p>※表示期間は最終連絡日から90日間です。</p>
        </div>
        <div class="text-right"><a class="clr-yel"
                href="<?php echo HOME_URL; ?>/itemlist.php?owner_id=<?= $owner->id ?>">出品者の商品一覧はこちら</a>
        </div>
        <section class="evaluation-tab-set">
            <ul class="nav nav-tabs" id="myTab" role="tablist">
                <li class="nav-item waves-effect waves-light">
                    <a class="nav-link <?php if ($group == 0): ?> active <?php endif; ?>" id="evaluation-tab"
                        href="<?php echo HOME_URL; ?>/evaluate.php?user_id=<?= $user_id ?>&group=0&sort=0" role="tab"
                        aria-controls="evaluation" aria-selected="false">全ての評価</a>
                </li>
                <li class="nav-item waves-effect waves-light">
                    <a class="nav-link <?php if ($group == 1): ?> active  <?php endif; ?>" id="evaluation__purchase-tab"
                        href="<?php echo HOME_URL; ?>/evaluate.php?user_id=<?= $user_id ?>&group=1&sort=<?= $sort ?>"
                        role="tab" aria-controls="evaluation__purchase" aria-selected="false">購入の評価</a>
                </li>
                <li class="nav-item waves-effect waves-light">
                    <a class="nav-link <?php if ($group == 2): ?> active  <?php endif; ?>" id="evaluation__listing-tab"
                        href="<?php echo HOME_URL; ?>/evaluate.php?user_id=<?= $user_id ?>&group=2&sort=<?= $sort ?>"
                        role="tab" aria-controls="evaluation__listing" aria-selected="true">出品の評価</a>
                </li>
            </ul>
            <div class="tab-content" id="myTabContent">
                <div class="tab-pane active fade show" id="evaluation" role="tabpanel" aria-labelledby="evaluation-tab">
                    <div class="evaluation-item">
                        <div class="evaluation-content__top">
                            総合評価 : <?= $evaluate->point ?>（<i class="bi bi-emoji-heart-eyes icon__good__eval"></i>
                            <?= $evaluate->good_count ?>　<i class="bi bi-emoji-neutral icon__middle__eval"></i>
                            <?= $evaluate->normal_count ?>　<i class="bi bi-emoji-angry icon__bad__eval"></i>
                            <?= $evaluate->bad_count ?>）
                        </div>
                        <div class="evaluation-content__title">
                            <h3>総合評価の見方</h3>
                        </div>
                        <div class="evaluation-content__exam">
                            <p>
                                取引相手から「良い」の評価は+1、「普通」は+0、「悪い」は-1となります。<br>
                                例えば「良い <i class="bi bi-emoji-heart-eyes icon__good__eval"></i>　」が1、「悪い <i
                                    class="bi bi-emoji-neutral icon__middle__eval"></i>　」が1の場合、総合評価は±0になります。
                            </p>
                        </div>
                    </div>
                    <div class="evaluation-item-show">
                        <div class="evaluation-item-show__btn">
                            <a class="<?php if ($sort == 1): ?>good-active <?php endif; ?>" href="<?php echo HOME_URL; ?>/evaluate.php?user_id=<?= $user_id ?>&group=<?= $group ?>&sort=1">良い</a>
                            <a class="<?php if ($sort == 2): ?>normal-active <?php endif; ?>" href="<?php echo HOME_URL; ?>/evaluate.php?user_id=<?= $user_id ?>&group=<?= $group ?>&sort=2">普通</a>
                            <a class="<?php if ($sort == 3): ?>bad-active <?php endif; ?>" href="<?php echo HOME_URL; ?>/evaluate.php?user_id=<?= $user_id ?>&group=<?= $group ?>&sort=3">悪い</a>
                        </div>
                        <?php foreach ($evoluteList as $order): ?>
                        <?php
                            $user = new User();
                            $user->select($order->user_id);
                        ?>
                        <?php if ($order->user_id != $user_id): ?>
                        <?php if ($group == 1) {
                            continue;
                        } ?>
                        <?php if ($order->seller_evaluate == 0) {
                            continue;
                        } ?>
                        <?php if ($sort != 0 && $order->seller_evaluate != $sort) {
                            continue;
                        } ?>
                        <div class="evaluation-item-show-result">
                            <div class="evaluation-result-person">
                                <div class="evaluation-result-person__img">
                                    <img src="<?php echo HOME_URL; ?>/common/assets/img/common/icon/profile.png" alt="">
                                </div>
                                <div class="evaluation-result-person__name">
                                    <p><a class="clr-yel"
                                            href="<?php echo HOME_URL; ?>/user/profile/<?= $order->user->hash_id ?>">
                                            <?= $order->user->getViewNicknameOrId() ?></a></p>
                                </div>
                            </div>
                            <div class="evaluation-result-content">
                                <div class="evaluation-result-content-head">
                                    <div class="evaluation-result-content-head__level">
                                        評価：
                                        <?php if (Evaluate::toEvaluateImageTag($order->seller_evaluate) == Evaluate::toEvaluateImageTag(1)): ?>
                                        <i class="bi bi-emoji-heart-eyes icon__good__eval"></i>
                                        <?php elseif (Evaluate::toEvaluateImageTag($order->seller_evaluate) == Evaluate::toEvaluateImageTag(2)): ?>
                                        <i class="bi bi-emoji-neutral icon__middle__eval"></i>
                                        <?php elseif (Evaluate::toEvaluateImageTag($order->seller_evaluate) == Evaluate::toEvaluateImageTag(3)): ?>
                                        <i class="bi bi-emoji-angry icon__bad__eval"></i>
                                        <?php endif; ?>
                                    </div>
                                    <div class="evaluation-result-content-head__sex">
                                        <p><a class="clr-yel"
                                                href="<?php echo HOME_URL; ?>/itemdetail.php?id=<?= $order->item_id ?>"><?= $order->item_title ?></a>
                                        </p>
                                    </div>
                                </div>
                                <div class="evaluation-result-content__txt">
                                    <p><?= empty($order->seller_comment) ? 'コメントはありません' : nl2br($order->seller_comment) ?>
                                    </p>
                                    <p><i class="bi bi-alarm mr-1 "></i><?= $order->regist_date ?></p>
                                </div>
                            </div>
                        </div>
                    <?php else: ?>
                        <?php if ($group == 2) {
                            continue;
                        } ?>
                        <?php if ($order->buyer_evaluate == 0) {
                            continue;
                        } ?>
                        <?php if ($sort != 0 && $order->buyer_evaluate != $sort) {
                            continue;
                        } ?>
                        <div class="evaluation-item-show-result">
                            <div class="evaluation-result-person">
                                <div class="evaluation-result-person__img">
                                    <img src="<?php echo HOME_URL; ?>/common/assets/img/common/icon/profile.png" alt="">
                                </div>
                                <div class="evaluation-result-person__name">
                                    <p><a href="<?php echo HOME_URL; ?>/user/profile/<?= $order->user->hash_id ?>">
                                            <?= $order->user->getViewNicknameOrId() ?></a></p>
                                </div>
                            </div>
                            <div class="evaluation-result-content">
                                <div class="evaluation-result-content-head">
                                    <div class="evaluation-result-content-head__level">
                                        評価：
                                        <?php if (Evaluate::toEvaluateImageTag($order->buyer_evaluate) == Evaluate::toEvaluateImageTag(1)): ?>
                                        <i class="bi bi-emoji-heart-eyes icon__good__eval"></i>
                                        <?php elseif (Evaluate::toEvaluateImageTag($order->buyer_evaluate) == Evaluate::toEvaluateImageTag(2)): ?>
                                        <i class="bi bi-emoji-neutral icon__middle__eval"></i>
                                        <?php elseif (Evaluate::toEvaluateImageTag($order->buyer_evaluate) == Evaluate::toEvaluateImageTag(3)): ?>
                                        <i class="bi bi-emoji-angry icon__bad__eval"></i>
                                        <?php endif; ?>
                                    </div>
                                    <div class="evaluation-result-content-head__sex">
                                        <p><a class="clr-yel"
                                                href="<?php echo HOME_URL; ?>/itemdetail.php?id=<?= $order->item_id ?>"><?= $order->item_title ?></a>
                                        </p>
                                    </div>
                                </div>
                                <div class="evaluation-result-content__txt">
                                    <p><?= empty($order->buyer_comment) ? 'コメントはありません' : nl2br($order->buyer_comment) ?>
                                    </p>
                                    <p><i class="bi bi-alarm mr-1 "></i><?= $order->regist_date ?></p>
                                </div>
                            </div>
                        </div>
                        <?php endif; ?>
                        <?php endforeach; ?>
                    </div>
                </div>
                <!-- <div class="tab-pane fade" id="evaluation__purchase" role="tabpanel"
                    aria-labelledby="evaluation__purchase-tab">
                    購入の評価の内容を入力してください。<br>
                    よろしくお願いいたします。
                </div>
                <div class="tab-pane fade" id="evaluation__listing" role="tabpanel"
                    aria-labelledby="evaluation__listing-tab">
                    出品の評価の内容を入力してください。<br>
                    よろしくお願いいたします。
                </div> -->
            </div>
        </section>
    </div>
</div>

<?php include('footer.php'); ?>




