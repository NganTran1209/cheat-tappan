<div class="container mb-5">
    <div class="text-center">
        <h1>管理画面サンプル</h1>
    </div>
    <ul class="list-group list-group-horizontal small">
        <li class="list-group-item flex-fill"><a href="page_01.php">ホーム</a></li>
        <li class="list-group-item flex-fill"><a href="page_02.php">ユーザー一覧</a></li>
        <li class="list-group-item flex-fill"><a href="page_03.php">出品待ち確認</a></li>
        <li class="list-group-item flex-fill"><a href="page_04.php">購入代金入金の確認</a></li>
        <li class="list-group-item flex-fill"><a href="page_05.php">出金申請の確認</a></li>
        <li class="list-group-item flex-fill"><a href="page_06.php">メール配信</a></li>
        <li class="list-group-item flex-fill"><a href="page_07.php">配送目安</a></li>
        <li class="list-group-item flex-fill"><a href="page_08.php">配送方法の追加</a></li>
        <li class="list-group-item flex-fill"><a href="page_09.php">初期設定</a></li>
    </ul>
    <p class="text-center">下記の画面はサンプルですので操作できません。</p>
</div>
