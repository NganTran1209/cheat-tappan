<?php include_once('../common/util.php'); ?>
<?php include_once('./securimage/securimage.php'); ?>
<?php
$value = array(
    'name' => '',
    'add' => '',
    'e_mail' => '',
    'comment' => ''
);
if ($_POST) {
    $value = $_POST;
    $securimage = new Securimage();
    if ($securimage->check($_POST['captcha_code']) == false) {
        setMessage('画像認証が一致しません。');
    } else {
        $_SESSION['mail'] = $value;
        header('Location: ' . getContextRoot() . '/mail/check.php');
    }
}
?>
<?php /*スマホ判定処理*/
function ua_smt()
{
//ユーザーエージェントを取得
    $ua = $_SERVER['HTTP_USER_AGENT'];
//スマホと判定する文字リスト
    $ua_list = array('iPhone', 'iPad', 'iPod', 'Android');
    foreach ($ua_list as $ua_smt) {
//ユーザーエージェントに文字リストの単語を含む場合はTRUE、それ以外はFALSE
        if (strpos($ua, $ua_smt) !== false) {
            return true;
        }
    }
    return false;
} ?>
<?php include('../header.php'); ?>
<script type="text/javascript" src="./js/jQuery.validation.js"></script>
<script type="text/javascript" src="./js/reserve.js"></script>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
<script type="text/javascript">
    $(document).ready(function () {
        $("#contact-form").validate({
            rules: {
                name: {required: true}
            },
            errorClass: "myError"

        });
    });
</script>
<div class="container">
    <div class="row">
        <div class="col-md-9 mainContents">
            <div class="bg-inner">
                <script type="text/javascript">
                    function checkForm() {
                        if (document.formtest.add.value == "" || document.formtest.name.value == "" || document.formtest.e_mail.value == "") {
                            alert("未記入が御座います。全て入力して下さい。");
                            return false;
                        } else {
                            return true;
                        }
                    }
                </script>

                <h1>お問い合わせ</h1>
                <form id="contact-form" name="formtest" method="post" onSubmit="return registerFormCheck(this)">
                    <input type="hidden" name="sub_actions" value="confirm">
                    <table class=" table table-bordered">
                        <tbody>
                        <tr>
                            <th>お名前</th>
                            <td><input class="form-control" type="text" name="name" value="<?= $value['name'] ?>"
                                       required></td>
                        </tr>
                        <tr>
                            <th>住所</th>
                            <td><input class="form-control" type="text" name="add" value="<?= $value['add'] ?>"
                                       required></td>
                        </tr>
                        <tr>
                            <th>メールアドレス</th>
                            <td><input class="form-control email" type="text" name="e_mail"
                                       value="<?= $value['e_mail'] ?>" required></td>
                        </tr>
                        <tr>
                            <th>内容</th>
                            <td><textarea class="form-control" name="comment" rows="4" cols="40"
                                          required><?= $value['comment'] ?></textarea></td>
                        </tr>
                        <tr>
                            <th>画像認証</th>
                            <td>
                                <img id="captcha" src="./securimage/securimage_show.php" alt="CAPTCHA Image"/><br>
                                <input type="text" name="captcha_code" size="10" maxlength="6"/>
                                <a href="#"
                                   onclick="document.getElementById('captcha').src = './securimage/securimage_show.php?' + Math.random(); return false">
                                    [※画像を変更する]</a>
                            </td>
                        </tr>
                        </tbody>
                    </table>
                    <div>
                        <input class="btn btn-block btn-info " type="submit" value="確認画面へ" name="submit"
                               onclick="return checkForm();">
                        <p class="text-center">※未記入がある場合、返信出来ません。</p>
                    </div>
                </form>
            </div>
        </div>
        <div class="col-md-3 sideContents">
            <?php include('../sidebar.php'); ?>
        </div>
    </div>
</div>

<?php include('../footer.php'); ?>
