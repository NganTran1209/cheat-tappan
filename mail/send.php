<?php 
$title_page = 'お問い合わせ内容の送信完了';
$description_page = '欲しい商品を画像で確認・届いた商品を画像で照合！気持ち良い取引が叶うオンラインマーケット。';
 ?>
<?php include_once('../common/util.php'); ?>

<?php
//以下ユーザー確認メール
//info@example.comを、あなたのメールアドレスにすることでこのメールフォームをそのまま使えます。
//session_start();//ページ移動したら再びsession_start
// $system_config = SystemConfig::select();
// $add_header = "From:".$system_config->from_email."\r\n";
// $add_header .= "Reply-to:".$system_config->from_email."\r\n";
// $add_header .= "X-Mailer: PHP/" . phpversion();
//$opt = '-f' . 'info@test.com'; //-fって何か意味あったんだけど忘れました　-fすると迷惑メールになりにくいとか、そんなことだったと思う。
// $opt = '-f' . $system_config->from_email;

//以下ヒアドキュメント<<<●●　HTMLでも、文字列でも、何いれてもOK●●;
//ヒアドキュメントは、メール送信とかの定型文を書いたりするとき、あとはSQLを書くときも使うかな。
//ヒアドキュメント内ではPHPのプログラムは一切かけない。変数だけ。変数は{}で囲ってあげること
//メールの本文をここでひとまとめに。
$message = <<<HTML
お問い合わせ内容の確認です。

お名前
{$_SESSION['mail']['name']}

郵便番号
{$_SESSION['mail']['code']}

住所
{$_SESSION['mail']['add']}

E_mail
{$_SESSION['mail']['e_mail']}

お問い合わせ内容
{$_SESSION['mail']['comment']}

IPアドレス
{$_SERVER["HTTP_X_FORWARDED_FOR"]}

内容確認後、担当者より折り返しご連絡をさせて頂きます。
今しばらくお待ちください。

HTML;

// カレントの言語を日本語に設定する
// mb_language("ja");
// 内部文字エンコードを設定する　このエンコード指定は大昔の携帯だとShift-jisにしないとだめだったとか。
// 今はUTF-8にしておけばだいたいOKだから、楽な時代になったもんだよ。
// mb_internal_encoding("UTF-8");

// mb_send_mail($_SESSION['mail']['e_mail'], "【お問い合わせ】確認メール", $message, $add_header, $opt);
//mb_send_mailは5つの設定項目がある
//mb_send_mail(送信先メールアドレス,"メールのタイトル","メール本文","メールのヘッダーFromとかリプライとか","送信エラーを送るメールアドレス");
//5番目の情報は第5引数と呼ばれるものでして、これがないと迷惑メール扱いになることも。

//マスター管理者にも同じメールを送りつける！！
//mb_send_mail('info@test.com', "お問い合わせがありました", $message, $add_header, $opt);
// mb_send_mail($system_config->from_email, "お問い合わせがありました", $message, $add_header, $opt);

$mail = new PHPMailer\PHPMailer\PHPMailer();
$mail->IsSMTP();
$mail->CharSet = 'UTF-8';

$mail->Host       = "sv12265.xserver.jp"; // SMTP server example
$mail->SMTPDebug  = 0;                     // enables SMTP debug information (for testing)
$mail->SMTPAuth   = true;       
$mail->SMTPSecure = 'TLS';             // enable SMTP authentication
$mail->Port       = 587;                    // set the SMTP port for the GMAIL server
$mail->Username   = 'info@teppan.store'; // SMTP account username example
$mail->Password   = 'cw7MzgwhNChCSWa';  

$mail->SetFrom("info@teppan.store", "TEPPAN STORE");
$mail->AddCC("dev@cheat.co.jp");
$mail->AddAddress($_SESSION['mail']['e_mail']);
$mail->Subject = "【お問い合わせ】確認メール";
$mail->Body    = $message;    
$mail->Send();

$mail->ClearAllRecipients();
$mail->AddAddress($system_config->from_email);
$mail->Subject = "お問い合わせがありました";
$mail->Send();

//session_destroy();  // セッションを破棄
unset($_SESSION['mail']);
?>
<?php /*スマホ判定処理*/
function ua_smt()
{
//ユーザーエージェントを取得
    $ua = $_SERVER['HTTP_USER_AGENT'];
//スマホと判定する文字リスト
    $ua_list = array('iPhone', 'iPad', 'iPod', 'Android');
    foreach ($ua_list as $ua_smt) {
//ユーザーエージェントに文字リストの単語を含む場合はTRUE、それ以外はFALSE
        if (strpos($ua, $ua_smt) !== false) {
            return true;
        }
    }
    return false;
} ?>
<?php $title_page = 'お問い合わせ内容の送信完了' ?>
<?php include('../user_header.php'); ?>
<div class="container">
    <div class="row">
        <div class="col-md-9 mainContents u-mgauto u-sp-px5">
            <div class="bg-inner">
                <h1 class="c-title01"><span>お問い合わせ内容の送信完了</span></h1>
                <div class="my-5">
                    <div class="alert alert-success text-center" style="font-size: 1.5rem">
                        <strong>送信完了しました。</strong>
                    </div>
                </div>
                <div class="w-75 mx-auto">
                    <a class="btn btn-block btn-info c-btn01 c-btn01--danger" href="<?php echo HOME_URL; ?>/" style="font-size: 1.5rem">ホームへ戻る</a>
                </div>
            </div>
        </div>
        <div class="col-md-3 sideContents u-none ">
            <?php include('../sidebar.php'); ?>
        </div>
    </div>
</div>
<?php include('../footer.php'); ?>
