<?php 
$title_page = 'お問い合わせ内容の確認';
$description_page = '欲しい商品を画像で確認・届いた商品を画像で照合！気持ち良い取引が叶うオンラインマーケット。';
 ?>
<?php include_once('../common/util.php'); ?>
<?php
//http://www.kaasan.info/archives/2151
//mail.phpのPostをSessionに代入する。
//Postは次のページまでしか引き継ぐことが出来ません
//確認ページ、送信ページとページをまたぐ際はこのSessionを使うのが普通です。
//そのため、フォームの確認ページなどで一度Sessionに代入する必要があります。

//いきなり確認画面にアクセスがあれば不正アクセス。
//!$_POSTで　$_POSTがSetされているかをチェックし、セットされていなければリダイレクト
if (!isset($_SESSION['mail'])) {
//    header('Location: http://192.168.55.10/ccsystem/');//	headerlocationはPHPのリダイレクト処理でよく使う。
    // TODO Defineの方に変更したので送信の確認しておく
    header('Location: '.HOME_URL.'');//	headerlocationはPHPのリダイレクト処理でよく使う。
}

//Sessionを開始するときの決まり文句、これがないとSessionが開始できない
// /session_start();
//$_SESSION = $_POST;

//無事Sessionに保存できているかチェックする

//echo '<pre>送信チェック内容：デバッグ用<br>';//HTMLのpreタグを使うと、配列が見やすくなる
//print_r($_SESSION);//print_rとは、配列を出力する関数
//echo '</pre>';


?>
<?php /*スマホ判定処理*/
function ua_smt()
{
//ユーザーエージェントを取得
    $ua = $_SERVER['HTTP_USER_AGENT'];
//スマホと判定する文字リスト
    $ua_list = array('iPhone', 'iPad', 'iPod', 'Android');
    foreach ($ua_list as $ua_smt) {
//ユーザーエージェントに文字リストの単語を含む場合はTRUE、それ以外はFALSE
        if (strpos($ua, $ua_smt) !== false) {
            return true;
        }
    }
    return false;
} ?>
<?php $title_page = 'お問い合わせ内容の確認' ?>
<?php include('../user_header.php'); ?>
<style>
    .contact-form td , .contact-form th{
        font-size: 1.5rem;
    }
    .contact-form .btn{
        font-size: 1.6rem;
    }
</style>
<div class="p-check">
<div class="container">
    <div class="row">
        <div class="col-md-9 mainContents u-mgauto u-sp-px5">
            <div class="bg-inner">
                <h1 class="c-title1"><span>お問い合わせ内容の確認</span></h1>
                <form id="contact-form" action="./send.php" method="post" class="contact-form">
                    <input type="hidden" name="sub_actions" value="confirm">
                    <table class=" table table-bordered">
                        <tbody>
                        <tr>
                            <th>お名前</th>
                            <td><?php echo htmlspecialchars($_SESSION['mail']['name']); ?></td>
                        </tr>
                        <tr>
                            <th>郵便番号</th>
                            <td><?php echo htmlspecialchars($_SESSION['mail']['code']); ?></td>
                        </tr>
                        <tr>
                            <th>住所</th>
                            <td><?php echo htmlspecialchars($_SESSION['mail']['add']); ?></td>
                        </tr>
                        <tr>
                            <th>メールアドレス</th>
                            <td><?php echo htmlspecialchars($_SESSION['mail']['e_mail']); ?></td>
                        </tr>
                        <tr>
                            <th>お問い合わせ内容</th>
                            <td><?php echo htmlspecialchars($_SESSION['mail']['comment']); ?></td>
                        </tr>
                        </tbody>
                    </table>
                    <!-- <div class="form-row"> -->
                        <div class="col-md-8 u-mgauto">
                            <input class="btn btn-block btn-info c-btn01 c-btn01--danger" type="submit" value="送信" name="submit">
                        </div>
                        <div class="col-md-3 u-mgauto"><a class="btn btn-block btn-outline-dark c-btn01 c-btn01--sm"
                                                 href="javascript:history.back();">戻る</a></div>
                    <!-- </div> -->
                </form>
            </div>
        </div>
        <div class="col-md-3 sideContents ">
            <?php include('../sidebar.php'); ?>
        </div>
    </div>
</div>
</div>
<?php include('../footer.php'); ?>
