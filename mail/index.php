<?php 
$title_page = 'お問い合わせ';
$description_page = '欲しい商品を画像で確認・届いた商品を画像で照合！気持ち良い取引が叶うオンラインマーケット。';
 ?>
<?php include_once('../common/util.php'); ?>
<?php
$value = array(
    'name' => '',
    'code' => '',
    'add' => '',
    'e_mail' => '',
    'e_mail_confirm' => '',
    'comment' => ''
);
if ($_POST) {
    $value = $_POST;
    $_SESSION['mail'] = $value;
    header('Location: ' . getContextRoot() . '/mail/check.php');
}
?>
<?php /*スマホ判定処理*/
function ua_smt()
{
//ユーザーエージェントを取得
    $ua = $_SERVER['HTTP_USER_AGENT'];
//スマホと判定する文字リスト
    $ua_list = array('iPhone', 'iPad', 'iPod', 'Android');
    foreach ($ua_list as $ua_smt) {
//ユーザーエージェントに文字リストの単語を含む場合はTRUE、それ以外はFALSE
        if (strpos($ua, $ua_smt) !== false) {
            return true;
        }
    }
    return false;
} ?>
<?php $title_page = 'お問い合わせ' ?>
<?php include(__DIR__ . '/../user_header.php'); ?>
<script type="text/javascript" src="./js/jQuery.validation.js"></script>
<script type="text/javascript" src="./js/reserve.js"></script>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
<script type="text/javascript">
    $(document).ready(function () {
        $("#contact-form").validate({
            rules: {
                name: {required: true}
            },
            errorClass: "myError"

        });
    });
</script>

    <div class="com-header-top">
        <div class="com-header-top__img wow animate__animated animate__fadeInUp">
            <img src="<?php echo HOME_URL; ?>/common/assets/img/common/other-com-header.png" alt="">
        </div>
        <div class="com-header-top__path bg-other-01 wow animate__animated animate__fadeInUp">
            <p><span><a href="<?php echo HOME_URL; ?>/" class="clr-yel">トップページ</a></span><span> > </span><span>お問い合わせ</span></p>
        </div>
 <!--       <div class="com-header-top__txt">
            <p>お問い合わせ</p>
        </div>-->
    </div>
    <div class="customer-container">
    <div class="category-title wow animate__animated animate__fadeInUp">
                <h3><span>お問い合わせ</span></h3>
            </div>
        <script type="text/javascript">
            function checkForm() {
                notice = document.querySelector('.c-form01__mess');
                notice.innerHTML="";
                if (document.formtest.code.value == "" || document.formtest.add.value == "" || document.formtest.name.value == "" || document.formtest.e_mail.value == "" 
                        || document.formtest.e_mail_confirm.value == "" || document.formtest.comment.value == "") {
                    //alert("未記入が御座います。全て入力して下さい。");
                    notice.innerHTML = "必須項目に必ずご記入ください";
                    document.querySelector('.c-form01').before(notice);
                    window.scrollTo(0,0);
                    return false;
                } else {
                    var regExp = /(^\d{5}$)|(^\d{3}-\d{4}$)/;
                    var postcode = document.formtest.code.value;

                    if(! regExp.test( postcode ) ){
                        // alert("無効な郵便番号");
                        notice.innerHTML = "無効な郵便番号";
                        document.querySelector('.c-form01').before(notice);
                        window.scrollTo(0,0);
                        return false;
                    } else {
                        if(document.formtest.e_mail.value != document.formtest.e_mail_confirm.value){
                            // alert("メールとメール（確認）が一致しません。");
                            notice.innerHTML = "メールとメール（確認）が一致しません。";
                            document.querySelector('.c-form01').before(notice);
                            window.scrollTo(0,0);
                            return false;
                        }else{
                            return true;
                        }
                    }
                }
            }

        </script>
        <div class="customer-contact-form">
            <div class="customer-contact-form__txt wow animate__animated animate__fadeInUp">
                <p>
                    ご利用のご相談は以下のフォームからお願いします。<br>
                    後日、担当者からご連絡させて頂きます。
                </p>
            </div>
            <div class="c-form01__mess"></div>
            <form class="c-form01" id="contact-form" name="formtest" action="" method="post" onSubmit="return registerFormCheck(this)" class="wow animate__animated animate__fadeInUp">
                <input type="hidden" name="sub_actions" value="confirm">
                <div class="contact-form-content">
                    <div class="contact-form-content__txt">
                        <p>お名前</p>
                        <span class="requried">
                            ※必須
                        </span>
                    </div>
                    <div class="contact-form-content__tag">
                        <input class="width-35 w-sp100" type="text" id="" name="name" value="<?= $value['name'] ?>">
                    </div>
                </div>
                <div class="contact-form-content">
                    <div class="contact-form-content__txt">
                        <p>郵便番号</p>
                        <span class="requried">
                            ※必須
                        </span>                    </div>
                    <div class="contact-form-content__tag">
                        <input class="width-35 w-sp100" type="text" id="" name="code" value="<?= $value['code'] ?>">
                    </div>
                </div>
                <div class="contact-form-content">
                    <div class="contact-form-content__txt">
                        <p>住所</p>
                        <span class="requried">
                            ※必須
                        </span>
                    </div>
                    <div class="contact-form-content__tag">
                        <input class="width-100 w-sp100" type="text" id="" name="add" value="<?= $value['add'] ?>">
                    </div>
                </div>
                <div class="contact-form-content">
                    <div class="contact-form-content__txt">
                    <p>メールアドレス</p>
                        <span class="requried">
                            ※必須
                        </span>
                    </div>
                    <div class="contact-form-content__tag">
                        <input class="width-50 w-sp100" type="text" id="" name="e_mail" value="<?= $value['e_mail'] ?>">
                    </div>
                </div>
                <div class="contact-form-content">
                    <div class="contact-form-content__txt">
                        <p>メールアドレス（確認）</p>
                        <span class="requried">
                            ※必須
                        </span>
                    </div>
                    <div class="contact-form-content__tag">
                        <input class="width-50 w-sp100" type="text" id="" name="e_mail_confirm" value="<?= $value['e_mail_confirm'] ?>">
                    </div>
                </div>
                <div class="contact-form-content">
                    <div class="contact-form-content__txt">
                        <p>内容</p>
                        <span class="requried">
                            ※必須
                        </span>
                    </div>
                    <div class="contact-form-content__tag d-flex align-items-center">
                        <textarea class="width-100" name="comment" id="" cols="30" rows="10" ><?= $value['comment'] ?></textarea>
                    </div>
                </div>
                <div class="contact-form-recapt">
                    <!-- Please here input Recapt Robot -->
                </div>
                <div class="conform-submit__btn wow animate__animated animate__fadeInUp">
                    <button type="submit" onclick="return checkForm();">送信内容を確認</button>
                </div>
            </form>
        </div>
    </div>

<?php include('../footer.php'); ?>
