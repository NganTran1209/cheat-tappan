<?php 
$title_page = '出勤申請／履歴';
$description_page = '欲しい商品を画像で確認・届いた商品を画像で照合！気持ち良い取引が叶うオンラインマーケット。';
 ?>
<?php include_once(__DIR__ . '/../common/util.php'); ?>
<?php include_once(__DIR__ . '/../common/login_check.php'); ?>
<?php

$cash_flow = new CashFlow();
if (isset($_POST['pay'])) {
    $list = $cash_flow->selectByUserIdForPaymentComplete(getUserId());
    $payout_amount = 0;
    foreach ($list as $value) {
        $amount = $value->amount - $value->commission;
        if (($amount < 0 || $value->order->state == 6 ) ) {
            $payout_amount += $amount;
        }
    }

    $payout = new PayoutHistory();
    $payout->amount = $payout_amount;
    $payout->user_id = getUserId();
    $payout->regist();

    $cash_flow->user_id = getUserId();
    $cash_flow->amount = -$payout_amount;
    $cash_flow->regist();
    setMessage('出金依頼をしました。');
}

$cash_flow = new CashFlow();
$list = $cash_flow->selectByUserIdForPaymentComplete(getUserId());

$summary = 0;
$payout_amount = 0;
foreach ($list as $value) {
    $amount = $value->amount - $value->commission;
    $summary += $amount;
    if (($amount < 0 || $value->order->state == 6 )  ) {
        $payout_amount += $amount;
    }
}

$user = new User();
$user->select(getUserId());

$title_page = '出金申請・履歴';
?>

<?php include(__DIR__ . '/../user_header.php'); ?>

<div class="com-container bg-yellow">
    <?php include('usersidebar.php'); ?>
        <div class="com-content">

                <div class="bg-inner">
                    <h1>出金申請・履歴</h1>
                <div class="text-center p-4 mb-4">
                        <div class="fontBold">
                        <p> 合計残高：<span class="font25"><?= number_format($summary) ?></span>円 </p> 
                        </div>
                        <div class="small">
                        <p> （うち出金可能な金額：<span class="fontBold"><?= number_format($payout_amount) ?></span>円） </p> 
                        </div>
                       
                    </div>
                    <div class="mb-5 w-75 mx-auto">
                        <?php if ($payout_amount > 0): ?>
                            <?php if ($user->hasAccountInfo()): ?>
                                <form method="post">
                                    <button class="btn btn-block btn-info" type="submit" name="pay">出金申請する</button>
                                </form>
                            <?php else: ?>
                                <div class="mb-3">
                                    <button class="btn btn-block btn-danger" type="submit" value="" disabled="disabled">
                                        出金出来ません
                                    </button>
                                </div>
                                <p>
                                    ※<a href="account_number_info.php">口座情報設定</a>で口座情報が入力されていない場合、出金ができません。<br>
                                    ※取引状況が「完了」にならない分は出金できません。
                                </p>
                            <?php endif; ?>
                        <?php endif; ?>
                    </div>

                    <h1 class="bg-light p-2 fontBold">入出金 履歴</h1>
                    <div class="table-responsive">
                        <table class="table table-bordered table-hover small text-nowrap">
                            <thead>
                            <tr class="text-center">
                                <th>確定日時</th>
                                <th>入出金</th>
                                <th>金額</th>
                                <th>入金状態</th>
                                <th>取引状況</th>
                                <th>備考</th>
                                <th>スコアフィードバック</th>
                            </tr>
                            </thead>
                            <tbody>
                            <?php foreach ($list as $item): ?>
                                <tr>
                                    <td class="col-sm-2 text-left">
                                        <?= $item->update_date ?>
                                    </td>
                                    <td class="col-sm-2 text-left">
                                        <?= $item->in_out == 1 ? '入金' : '出金' ?>
                                    </td>
                                    <td class="col-sm-2 text-left">
                                        <?= number_format($item->amount - $item->commission) ?>円
                                    </td>
                                    <td class="col-sm-2 text-left">
                                        <?php if ($item->in_out == 1): ?>
                                            <?= $item->order->getCashFlowName() ?>
                                        <?php else: ?>
                                            -
                                        <?php endif; ?>
                                    </td>
                                    <td class="col-sm-2 text-left">
                                        <?php if ($item->in_out == 1): ?>
                                            <?= $item->order->getStateName() ?>
                                        <?php else: ?>
                                            -
                                        <?php endif; ?>
                                    </td>
                                    <td class="col-sm-2 text-left">
                                        <?php if ($item->in_out == 1): ?>
                                            販売「<?= $item->order->item->title ?>」
                                        <?php elseif ($item->in_out == 2): ?>
                                            出金依頼
                                        <?php endif; ?>
                                    </td>
                                    <td class="col-sm-2 text-center">
                                        <?php if ($item->in_out == 1): ?>
                                            <?php 
                                                $feedback1 = new Feedback();
                                                $feedback1->selectForIndex($item->order->item->id,$item->order->user_id,1);
                                                $feedback2 = new Feedback();
                                                $feedback2->selectForIndex($item->order->item->id,$item->order->user_id,2);
                                            ?>
                                            <a href="<?php echo HOME_URL; ?>/user/negotiation.php?item_id=<?=$item->order->item_id ?>&user_id=<?=$item->order->user_id ?>">
                                                <?php if($feedback1->score != null) : ?>
                                                    <p><?=round(($feedback1->score)*100,1) ?>%</p>
                                                <?php endif; ?>
                                                <?php if($feedback2->score != null) : ?>
                                                    <p ><?=round(($feedback2->score)*100,1) ?>%</p>
                                                <?php endif; ?>
                                            </a>
                                        <?php endif; ?>
                                    </td>
                                </tr>
                            <?php endforeach; ?>
                            </tbody>
                        </table>
                    </div>
                    <!--                <div class="text-center">-->
                    <!--                    <nav>-->
                    <!--                        <ul class="pagination justify-content-center">-->
                    <!--                            <li class="page-item"><span id="prev">← 前へ</span></li>-->
                    <!--                            <li class="page-item hidden"><span id="page"> </span></li>-->
                    <!--                            <li class="page-item"><span id="next">次へ →</span></li>-->
                    <!--                        </ul>-->
                    <!--                    </nav>-->
                    <!--                </div>-->
                </div>
            </div>
        </div>
    </div>
<?php include('../footer.php'); ?>