<?php include_once(__DIR__ . '/util.php'); ?>
<?php
if (!isAdminUser()) {
    setMessage('不正なリクエストです。');
    header('Location: '.getContextRoot().'/');
    exit;
}