<?php include_once(__DIR__ . '/../util.php'); ?>
<?php
if (!isset($item)) {
    $item = new Item();
}
if (!isset($view_favorites_button)) {
    $view_favorites_button = false;
}
if (!isset($view_favorites_count)) {
    $view_favorites_count = false;
}
$countFavorites = $item->selectCountFavorites();
?>
<div class="col-sm-6">
    <div id="carouselImage" class="carousel slide" data-ride="carousel" data-interval="false">
        <?php if ($item->order_id != null): ?>
            <div class="soldOutBadge">
                <div>SOLD</div>
            </div>
        <?php endif; ?>
        <ol class="carousel-indicators">
            <?php $url = getImagePath($item->id, 2);
            if (strstr($url, "noimage") == false): ?>
                <li data-target="#carouselImage" data-slide-to="0" class="active"></li>
                <li data-target="#carouselImage" data-slide-to="1"></li>
            <?php endif; ?>
            <?php $url = getImagePath($item->id, 3);
            if (strstr($url, "noimage") == false): ?>
                <li data-target="#carouselImage" data-slide-to="2"></li>
            <?php endif; ?>
            <?php $url = getImagePath($item->id, 4);
            if (strstr($url, "noimage") == false): ?>
                <li data-target="#carouselImage" data-slide-to="3"></li>
            <?php endif; ?>
        </ol>
        <div class="carousel-inner">
            <div class="carousel-item active">
                <img class="img-fluid" src="<?= getImagePath($item->id, 1) ?>" alt="">
            </div>
            <?php $url = getImagePath($item->id, 2);
            if (strstr($url, "noimage") == false): ?>
                <div class="carousel-item">
                    <img class="img-fluid" src="<?= getImagePath($item->id, 2) ?>" alt="">
                </div>
            <?php endif; ?>
            <?php $url = getImagePath($item->id, 3);
            if (strstr($url, "noimage") == false): ?>
                <div class="carousel-item">
                    <img class="img-fluid" src="<?= getImagePath($item->id, 3) ?>" alt="">
                </div>
            <?php endif; ?>
            <?php $url = getImagePath($item->id, 4);
            if (strstr($url, "noimage") == false): ?>
                <div class="carousel-item">
                    <img class="img-fluid" src="<?= getImagePath($item->id, 4) ?>" alt="">
                </div>
            <?php endif; ?>
        </div>
        <?php $url = getImagePath($item->id, 2);
        if (strstr($url, "noimage") == false): ?>
            <a class="carousel-control-prev" href="#carouselImage" role="button" data-slide="prev">
                <i class="fas fa-angle-left"></i>
            </a>
            <a class="carousel-control-next" href="#carouselImage" role="button" data-slide="next">
                <i class="fas fa-angle-right"></i>
            </a>
        <?php endif; ?>
    </div>
    <?php if ($view_favorites_count): ?>
        <div class="text-center mb-4">
            <span class="text-nowrap mr-2">お気に入り数 <?= $countFavorites ?></span>
            <span class="text-nowrap mr-2">閲覧数 <?= $item->access_count ?></span>
            <?php if ($view_favorites_button): ?>
                <span class="text-nowrap mr-2">
                <a type="button" onclick="updateGood()"><i class="fas fa-heart"></i> いいね！<span
                            id="good"><?= Item::selectItemGoodCountSummary($item->id) ?></span></a>
                </span>
            <?php endif; ?>
        </div>
    <?php endif; ?>
</div>
