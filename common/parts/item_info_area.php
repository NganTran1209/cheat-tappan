<?php include_once(__DIR__ . '/../util.php'); ?>
<?php
if (!isset($item)) {
    $item = new Item();
}
if (!isset($view_other_item_link)) {
    $view_other_item_link = false;
}
if (!isset($view_evaluate)) {
    $view_evaluate = false;
}
$owner = new User();
$owner->select($item->owner_id);
$evaluate = new Evaluate();
$evaluate->selectSummary($item->owner_id);
$system_config = SystemConfig::select();
?>
<div class="product-list-content__right">
<div class="product-list-content__right-item">
    <div class="product-list-content__right-item-title">
        <p>出品者</p>
    </div>
    <div class="product-list-content__right-item-txt">
        <a href="<?php echo HOME_URL; ?>/user/seller_home.php?owner_id=<?= $owner->id ?>"><?= $owner->getViewNicknameOrId() ?></a><br>
        <?php if ($view_evaluate): ?>
            <a href="<?php echo HOME_URL; ?>/evaluate.php?user_id=<?= $owner->id ?>"><p class="u-flex"><span>評価</span></a> : 
            <?= $evaluate->point ?>（<i class="bi bi-emoji-heart-eyes icon__good__eval"></i> 
            <?= $evaluate->good_count ?>　<i class="bi bi-emoji-neutral icon__middle__eval"></i> 
            <?= $evaluate->normal_count ?>　<i class="bi bi-emoji-angry icon__bad__eval"></i> 
            <?= $evaluate->bad_count ?>）</p>
        <?php endif; ?>
    </div>
</div>
<?php if ($system_config->enabled_category): ?>
    <div class="product-list-content__right-item">
        <div class="product-list-content__right-item-title">
            <p>カテゴリー</p>
        </div>
        <div class="product-list-content__right-item-txt">
            <p><?= $item->category_name ?></p>
        </div>
    </div>
<?php endif; ?>
<?php if ($system_config->enabled_item_state): ?>
    <div class="product-list-content__right-item">
        <div class="product-list-content__right-item-title">
            <p><?= ITEM_NAME ?>の状態</p>
        </div>
        <div class="product-list-content__right-item-txt">
            <p><?= $item->item_state_name ?></p>
        </div>
    </div>
<?php endif; ?>
<?php foreach (FreeInputItem::selectAll() as $item1): ?>
    <?php if (!$item1->enabled) {
        continue;
    } ?>
    <div class="product-list-content__right-item">
        <div class="product-list-content__right-item-title">
            <p><?= $item1->name ?></p>
        </div>
        <div class="product-list-content__right-item-txt">
            <p> <?= $item->getFreeInfoName($item1->id) ?></p>
        </div>
    </div>
<?php endforeach; ?>
<?php if ($system_config->enabled_pref): ?>
    <div class="product-list-content__right-item">
        <div class="product-list-content__right-item-title">
            <p>地域</p>
        </div>
        <div class="product-list-content__right-item-txt">
            <p><?= $item->pref_name ?></p>
        </div>
    </div>
<?php endif; ?>
<?php if ($item->with_delivery && $system_config->enabled_delivery_type): ?>
    <div class="product-list-content__right-item">
        <div class="product-list-content__right-item-title">
            <p>配送方法</p>
        </div>
        <div class="product-list-content__right-item-txt">
            <p><?= $item->delivery_type_name ?></p>
        </div>
    </div>
<?php endif; ?>
<?php if ($item->with_delivery && $system_config->enabled_postage): ?>
    <div class="product-list-content__right-item">
        <div class="product-list-content__right-item-title">
            <p>配送料の負担</p>
        </div>
        <div class="product-list-content__right-item-txt">
            <p><?= $item->carriage_plan_name ?></p>
        </div>
    </div>
<?php endif; ?>
<?php if ($item->with_delivery && $system_config->enabled_carriage_type): ?>
    <div class="product-list-content__right-item">
        <div class="product-list-content__right-item-title">
            <p>配送の目安</p>
        </div>
        <div class="product-list-content__right-item-txt">
            <p><?= $item->carriage_type_name ?></p>
        </div>
    </div>
<?php endif; ?>
<?php if ($view_other_item_link): ?>
    <div class="product-list-content__right-bottom">
        <a href="<?php echo HOME_URL; ?>/product/list_item.php?owner_id=<?= $item->owner_id ?>">出品者の他の<?= ITEM_NAME ?>を見る</a>
    </div>
<?php endif; ?>
</div>