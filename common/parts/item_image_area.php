<?php include_once(__DIR__ . '/../util.php'); ?>
<?php
if (!isset($item)) {
    $item = new Item();
}
if (!isset($view_favorites_button)) {
    $view_favorites_button = false;
}
if (!isset($view_favorites_count)) {
    $view_favorites_count = false;
}
$countFavorites = $item->selectCountFavorites();
?>
<div class="product-list-content__left h-100">
    <div class="product-list-content__img">
        <?php if ($item->order_id != null): ?>
            <div class="soldOutBadge">
                <div>SOLD</div>
            </div>
        <?php endif; ?>
        <div id="carouselSlider" class="carousel slide" data-ride="carousel" >
            <ol class="carousel-indicators">
                <?php 
                    $count = 0;
                    for($i=2; $i<=10; $i++) {
                        $url = getImagePath($item->id, $i);
                        if (strstr($url, "noimage") == false) {
                            if ($count == 0){
                                echo('
                                    <li data-target="#carouselSlider" data-slide-to="'.$count.'" class="active" style="width: 12px"></li>
                                    <li data-target="#carouselSlider" data-slide-to="'.($count+1).'"  style="width: 12px"></li>
                                ');
                            } else {
                                echo('
                                    <li data-target="#carouselSlider" data-slide-to="'.($count+1).'"  style="width: 12px"></li>
                                ');
                            }
                            $count += 1;
                        }
                    }
                ?>
            </ol>
            <div class="carousel-inner" role="listbox">
                <div class="carousel-item active">
                    <img src="<?= getImagePath($item->id, 1) ?>" alt="">
                </div>
                <?php $url = getImagePath($item->id, 2);
                if (strstr($url, "noimage") == false): ?>
                    <div class="carousel-item">
                            <img src="<?= getImagePath($item->id, 2) ?>" alt="">
                    </div>
                <?php endif; ?>
                <?php $url = getImagePath($item->id, 3);
                if (strstr($url, "noimage") == false): ?>
                    <div class="carousel-item">
                            <img src="<?= getImagePath($item->id, 3) ?>" alt="">
                    </div>
                <?php endif; ?>
                <?php $url = getImagePath($item->id, 4);
                if (strstr($url, "noimage") == false): ?>
                    <div class="carousel-item">
                        <img src="<?= getImagePath($item->id, 4) ?>" alt="">
                    </div>
                <?php endif; ?>
                <?php $url = getImagePath($item->id, 5);
                if (strstr($url, "noimage") == false): ?>
                    <div class="carousel-item">
                        <img src="<?= getImagePath($item->id, 5) ?>" alt="">
                    </div>
                <?php endif; ?>
                <?php $url = getImagePath($item->id, 6);
                if (strstr($url, "noimage") == false): ?>
                    <div class="carousel-item">
                        <img src="<?= getImagePath($item->id, 6) ?>" alt="">
                    </div>
                <?php endif; ?>
                <?php $url = getImagePath($item->id, 7);
                if (strstr($url, "noimage") == false): ?>
                    <div class="carousel-item">
                        <img src="<?= getImagePath($item->id, 7) ?>" alt="">
                    </div>
                <?php endif; ?>
                <?php $url = getImagePath($item->id, 8);
                if (strstr($url, "noimage") == false): ?>
                    <div class="carousel-item">
                        <img src="<?= getImagePath($item->id, 8) ?>" alt="">
                    </div>
                <?php endif; ?>
                <?php $url = getImagePath($item->id, 9);
                if (strstr($url, "noimage") == false): ?>
                    <div class="carousel-item">
                        <img src="<?= getImagePath($item->id, 9) ?>" alt="">
                    </div>
                <?php endif; ?>
                <?php $url = getImagePath($item->id, 10);
                if (strstr($url, "noimage") == false): ?>
                    <div class="carousel-item">
                        <img src="<?= getImagePath($item->id, 10) ?>" alt="">
                    </div>
                <?php endif; ?>
                <a class="carousel-control-prev" href="#carouselSlider" role="button" data-slide="prev">
                        <span class="carousel-control-prev-icon"></span>
                </a>
                <a class="carousel-control-next" href="#carouselSlider" role="button" data-slide="next">
                        <span class="carousel-control-next-icon"></span>
                </a>
            </div>
        </div>
    </div>
    <div class="product-list-content__bottom">
        <?php if ($view_favorites_count): ?>
            <p> 
                <a type="button" onclick="registFavorites()">
                    お気に入り数：<span id="favorite"><?= $countFavorites ?></span>
                </a>　
                観覧数：<?= $item->access_count ?>　
                <a type="button" onclick="updateGood()"><i class="bi bi-heart-fill"></i> いいね！：<span
                    id="good"><?= Item::selectItemGoodCountSummary($item->id) ?></span></a>
                </span>
            </p>
        <?php endif; ?>
    </div>
</div>