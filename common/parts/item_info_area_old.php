<?php include_once(__DIR__ . '/../util.php'); ?>
<?php
if (!isset($item)) {
    $item = new Item();
}
if (!isset($view_other_item_link)) {
    $view_other_item_link = false;
}
if (!isset($view_evaluate)) {
    $view_evaluate = false;
}
$owner = new User();
$owner->select($item->owner_id);
$evaluate = new Evaluate();
$evaluate->selectSummary($item->owner_id);
$system_config = SystemConfig::select();
?>
<div class="col-sm-6">
    <table class="table table-bordered">
        <tbody>
        <tr>
            <th>出品者</th>
            <td>
<!--                --><?php //if (isAdminUser()): ?>
<!--                    <span class="smallEX">(管理者ログインのみ表示)</span><br>-->
<!--                    <a href="--><?php //echo getContextRoot().'/user/profile/'.$owner->hash_id ?><!--">--><?php //echo $owner->name ?><!--</a>-->
<!--                --><?php //endif; ?>

                <a href="<?= getContextRoot().'/user/profile/'.$owner->hash_id ?>"><?= $owner->getViewNicknameOrId() ?></a><br>
                <?php if ($view_evaluate): ?>
                    <a href="<?php echo HOME_URL; ?>/evaluate.php?user_id=<?= $owner->id ?>">評価</a>：
                    <?= $evaluate->point ?>(
                    <i class="fas fa-smile mr-1"></i><?= $evaluate->good_count ?>
                    <i class="fas fa-meh mr-1"></i><?= $evaluate->normal_count ?>
                    <i class="fas fa-frown mr-1"></i><?= $evaluate->bad_count ?>
                    )
                <?php endif; ?>
                <?php /*?>
    <?php if ($item->owner_id != getUserId()): ?>
    <br>
    <form id="block-user" method="post" onsubmit="return window.confirm('出品者をブロックする。'); ">
      <input type="hidden" name="action" value="blockAccess" />
      <input type="hidden" name="item_id" value="<?= $item->id ?>" />
      <a href="#" onclick="submitForm('block-user');">出品者をブロックする。</a>
    </form>
    <?php endif; ?><?php */ ?>
            </td>
        </tr>
        <?php if ($system_config->enabled_category): ?>
            <tr>
                <th>カテゴリー</th>
                <td>
                    <?= $item->category_name ?>
                </td>
            </tr>
        <?php endif; ?>
        <?php if ($system_config->enabled_item_state): ?>
            <tr>
                <th><?= ITEM_NAME ?>の状態</th>
                <td>
                    <?= $item->item_state_name ?>
                </td>
            </tr>
        <?php endif; ?>

        <?php foreach (FreeInputItem::selectAll() as $item1): ?>
            <?php if (!$item1->enabled) {
                continue;
            } ?>
            <tr>
                <th><?= $item1->name ?></th>
                <td>
                    <?= $item->getFreeInfoName($item1->id) ?>
                </td>
            </tr>
        <?php endforeach; ?>

        <?php if ($system_config->enabled_pref): ?>
            <tr>
                <th>地域</th>
                <td>
                    <?= $item->pref_name ?>
                </td>
            </tr>
        <?php endif; ?>

        <?php if ($item->with_delivery && $system_config->enabled_delivery_type): ?>
            <tr>
                <th>配送の方法</th>
                <td>
                    <?= $item->delivery_type_name ?>
                </td>
            </tr>
        <?php endif; ?>
        <?php if ($item->with_delivery && $system_config->enabled_postage): ?>
            <tr>
                <th>配送料の負担</th>
                <td>
                    <?= $item->carriage_plan_name ?>
                </td>
            </tr>
        <?php endif; ?>
        <?php if ($item->with_delivery && $system_config->enabled_carriage_type): ?>
            <tr>
                <th>配送の目安</th>
                <td>
                    <?= $item->carriage_type_name ?>
                </td>
            </tr>
        <?php endif; ?>
        </tbody>
    </table>
    <?php if ($view_favorites_button): ?>
        <?php if (isLogin()): ?>
            <div class="bookmark">
                <?php if (getUserId() != $owner->id) : ?>
                    <?php if (!$item->hasFavorites() && $item->order_id == null) : ?>
                        <form method="post" onsubmit="return confirm('お気に入りに追加します。'); ">
                            <input type="hidden" name="item_id" value="<?= $item->id ?>"/>
                            <input type="hidden" name="action" value="favorites"/>
                            <button class="btn btn-info btn-sm py-0" type="submit">
                                <i class="far fa-bookmark mr-2"></i>お気に入り追加
                            </button>
                        </form>
                    <?php elseif ($item->hasFavorites()) : ?>
                        <form method="post" onsubmit="return confirm('お気に入りを解除します。'); ">
                            <input type="hidden" name="item_id" value="<?= $item->id ?>"/>
                            <input type="hidden" name="action" value="cancelFavorites"/>
                            <!--  <input class="btn-sm" type="submit" value="お気に入り解除"/>-->
                            <button class="btn btn-info btn-sm py-0" type="submit">
                                <i class="fas fa-bookmark mr-2"></i>お気に入り解除
                            </button>
                        </form>
                    <?php endif; ?>
                <?php endif; ?>
            </div>
        <?php endif; ?>
    <?php endif; ?>

    <?php if ($view_other_item_link): ?>
        <div class="text-center">
            <a href="<?php echo HOME_URL; ?>/itemlist.php?owner_id=<?= $item->owner_id ?>">出品者の他の<?= ITEM_NAME ?>を見る</a>
        </div>
    <?php endif; ?>

</div>

