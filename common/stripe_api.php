<?php include_once(__DIR__.'/../common/util.php'); ?>
<?php include_once(__DIR__.'/stripe/init.php'); ?>
<?php
class StripeApi {

    /**
     * 与信用顧客情報登録
     */
    static public function regist_customer() {
        try {
            $system_config = SystemConfig::select();
            \Stripe\Stripe::setApiKey($system_config->stripe_secret_key);

            $card_info = StripeApi::get_card_info();
            if (!empty($card_info) && $card_info->enabled_customer) {
                // 重複顧客削除
                StripeApi::remove_customer($card_info->customer_id);
            }

            // 顧客情報登録
            $customer = StripeApi::create_customer();

        } catch (Exception $e) {
            setMessage($e->getMessage());
            return false;
        }
        return true;
    }

    /**
     * 入金
     * @param unknown $amount 金額
     * @return boolean
     */
    static public function charge($amount, $description) {
        try {
            $system_config = SystemConfig::select();
            \Stripe\Stripe::setApiKey($system_config->stripe_secret_key);

            $card_info = StripeApi::get_card_info();
            \Stripe\Charge::create([
                'customer' => $card_info->customer_id,
                'amount' => $amount,
                'currency' => 'jpy',
                'description' => $description,
            ]);

        } catch (Exception $e) {
            setMessage($e->getMessage());
            return false;
        }
        return true;
    }

    /**
     * トークンの取得
     * @throws Exception
     * @return \Stripe\Token
     */
    static private function create_token() {
        try {
            $token = \Stripe\Token::create([
                'card' => [
                    'number' => $_POST['number'],
                    'exp_month' => $_POST['exp_month'],
                    'exp_year' => $_POST['exp_year'],
                    'cvc' => $_POST['cvc'],
                ],
            ]);

        } catch (\Stripe\Exception\CardException $e) {
            throw new Exception('トークンの取得に失敗しました。', 999, $e);
        }
        return $token;
    }

    /**
     * カード情報の登録
     * @throws Exception
     * @return \Stripe\Customer
     */
    static private function create_customer() {
        try {
            $customer = \Stripe\Customer::create([
                'source' => $_POST['stripeToken'],
                'description' => '['.getHashKey().':'.getUserName().']'.$_POST['stripeEmail'],
                'email' => $_POST['stripeEmail'],
                'name' => '['.getHashKey().':'.getUserName().']',
            ]);

            $card = new CardInfo();
            $card->user_id = getHashKey();
            $card->token = $_POST['stripeToken'];
            $card->customer_id = $customer->id;
            $card->email = $_POST['stripeEmail'];
            $card->regist_key = getHashKey(). md5($customer->id);
            $card->regist();

        } catch (\Stripe\Exception\CardException $e) {
            throw new Exception('カード情報の登録に失敗しました。', 999, $e);
        }
        return $customer;
    }

    /**
     * 顧客を削除
     * @param unknown $customer_id
     * @throws Exception
     * @return \Stripe\Customer
     */
    static public function remove_customer() {
        try {
            $system_config = SystemConfig::select();
            \Stripe\Stripe::setApiKey($system_config->stripe_secret_key);

            $card_info = new CardInfo();
            $card_info->select(getHashKey());

            $customer = \Stripe\Customer::retrieve([
                'id' => $card_info->customer_id,
            ]);
            $customer->delete();
            $card_info->remove();

        } catch (\Stripe\Exception\CardException $e) {
            throw new Exception('カード情報の登録に失敗しました。', 999, $e);
        }
        return $customer;
    }

    /**
     * カード情報が不正な場合、削除して修復する。
     * @return boolean
     */
    static public function repair_costomer() {
        try {
            $system_config = SystemConfig::select();
            \Stripe\Stripe::setApiKey($system_config->stripe_secret_key);

            $card_info = new CardInfo();
            $card_info->select(getHashKey());

            $customer = \Stripe\Customer::retrieve([
                'id' => $card_info->customer_id,
            ]);

        } catch (\Stripe\Exception\InvalidRequestException $e) {
            $card_info->remove();
            return false;
        }
        return true;
    }

    /**
     * カード情報取得
     * @return mixed
     */
    static private function get_card_info() {
        $card = new CardInfo();
        $card->select(getHashKey());
        if (empty($card->customer_id)) {
            return false;
        }
        return $card;
    }

}
