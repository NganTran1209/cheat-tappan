<?php
include_once(__DIR__.'/../common/db.php');

require(__DIR__."/../PHPMailer/src/PHPMailer.php");
require(__DIR__."/../PHPMailer/src/SMTP.php");
require(__DIR__."/../PHPMailer/src/Exception.php");

/**
 * メールオブジェクト
 * @author asagao
 *
 */
class Mail {

	/**
	 * 表題
	 * @var unknown
	 */
	public $subject;
	/**
	 * 本文
	 * @var unknown
	 */
	public $body;
	/**
	 * 送信元アドレス
	 * @var unknown
	 */
	public $from;
	/**
	 * 送信元名
	 * @var unknown
	 */
	public $fromName;
	/**
	 * 送信先アドレス
	 * @var unknown
	 */
	public $to;

	public function sendMail() {
		// mb_language('uni');
		// mb_internal_encoding('UTF-8');

		// $header = "From: ".$this->fromName."<".$this->from.">\r\n";

	// 	$result = mb_send_mail(
	// 			$this->to,
	// 			$this->subject,
	// 			$this->body,
	// 			$header,
	// 			"-f ". $this->from);
	// }
		$mail = new PHPMailer\PHPMailer\PHPMailer();
		$mail->IsSMTP();
		$mail->CharSet = 'UTF-8';

		$mail->Host       = "sv12265.xserver.jp"; // SMTP server example
		$mail->SMTPDebug  = 0;                     // enables SMTP debug information (for testing)
		$mail->SMTPAuth   = true;  
		$mail->SMTPSecure = 'TLS';             // enable SMTP authentication
		$mail->Port       = 587;                    // set the SMTP port for the GMAIL server
		$mail->Username   = 'info@teppan.store'; // SMTP account username example
		$mail->Password   = 'cw7MzgwhNChCSWa';        // SMTP account password example

		// Content
		$mail->Subject = $this->subject;
		$mail->Body    = $this->body;    
		$mail->SetFrom("info@teppan.store", "TEPPAN STORE");
		$mail->AddCC("dev@cheat.co.jp");
		$mail->AddAddress($this->to);

		$mail->Send();
	}
}