<?php
include_once(__DIR__.'/../common/db.php');

define('MAIL_XMLFILE', __DIR__.'/../template/mail.xml');
define('MAIL_INITIAL_XMLFILE', __DIR__.'/../template/mail_init.xml');

/**
 * メールテンプレート
 * @author asagao
 *
 */
class MailTemplate {
	static private $map = null;

	/**
	 * ユーザー名
	 * @var string
	 */
	const REPLACE_USER_NAME = '{USER_NAME}';
	/**
	 * 請求年月
	 * @var string
	 */
	const REPLACE_INVOICE_MONTH = '{INVOICE_MONTH}';
	/**
	 * 金額
	 * @var string
	 */
	const REPLACE_AMOUNT = '{AMOUNT}';
	/**
	 * 月利用料
	 * @var string
	 */
	const REPLACE_MONTHLY_FEE = '{MONTHLY_FEE}';
	/**
	 * 手数料
	 * @var string
	 */
	const REPLACE_COMMISSION_FEE = '{COMMISSION_FEE}';
	/**
	 * 売上
	 * @var string
	 */
	const REPLACE_SALES_AMOUNT = '{SALES_AMOUNT}';
	/**
	 * 手数料率
	 * @var string
	 */
	const REPLACE_COMMISSION_RATE = '{COMMISSION_RATE}';
	/**
	 * 決済件数
	 * @var string
	 */
	const REPLACE_SALES_COUNT = '{SALES_COUNT}';
	/**
	 * ユーザー本登録URL
	 * @var string
	 */
	const REPLACE_REGIST_USER_URL = '{REGIST_USER_URL}';
	/**
	 * 出品者名
	 * @var string
	 */
	const REPLACE_OWNER_NAME = '{OWNER_NAME}';
	/**
	 * タイトル
	 * @var string
	 */
	const REPLACE_TITLE = '{TITLE}';
	/**
	 * カテゴリー名
	 * @var string
	 */
	const REPLACE_CATEGORY_NAME = '{CATEGORY_NAME}';
	/**
	 * 交渉リンク
	 * @var string
	 */
	const REPLACE_NEGOTIATION_URL = '{NEGOTIATION_URL}';
	/**
	 * 新規パスワード
	 * @var string
	 */
	const REPLACE_NEW_PASSWORD= '{NEW_PASSWORD}';
	/**
	 * ハッシュID
	 * @var string
	 */
	const REPLACE_USER_HASH_ID= '{USER_HASH_ID}';
	/**
	 * 出品者ハッシュID
	 * @var string
	 */
	const REPLACE_OWNER_HASH_ID= '{OWNER_HASH_ID}';
	/**
	 * 注文URL
	 * @var string
	 */
	const REPLACE_ORDER_URL= '{ORDER_URL}';
	/**
	 * 評価
	 * @var string
	 */
	const REPLACE_EVALUATE='{EVALUATE}';
	/**
	 * 評価URL
	 * @var string
	 */
	const REPLACE_EVALUATE_URL='{EVALUATE_URL}';
	/**
	 * 出品URL
	 * @var string
	 */
	const REPLACE_ITEM_URL='{ITEM_URL}';
	/**
	 * テキスト
	 * @var string
	 */
	const REPLACE_TEXT = '{TEXT}';
	/**
	 * 価格
	 * @var string
	 */
	const REPLACE_PRICE = '{PRICE}';
	/**
	 * 購入希望URL
	 * @var string
	 */
	const REPLACE_REQUEST_URL='{REQUEST_URL}';
	/**
	 * 会社名
	 * @var string
	 */
	const REPLACE_COMPANY_NAME = '{COMPANY_NAME}';
	/**
	 * 都道府県名
	 * @var string
	 */
	const REPLACE_PREF_NAME = '{PREF_NAME}';
	/**
	 * 金融機関名
	 * @var string
	 */
	const REPLACE_FINANCIAL_INSTITUTION_NAME = '{FINANCIAL_INSTITUTION_NAME}';
	/**
	 * 支店名
	 * @var string
	 */
	const REPLACE_BRANCH_NAME = '{BRANCH_NAME}';
	/**
	 * 預金種別
	 * @var string
	 */
	const REPLACE_DEPOSIT_TYPE = '{DEPOSIT_TYPE}';
	/**
	 * 口座番号
	 * @var string
	 */
	const REPLACE_ACCOUNT_NUMBER = '{ACCOUNT_NUMBER}';
	/**
	 * 口座名義
	 * @var string
	 */
	const REPLACE_ACCOUNT_HOLDER = '{ACCOUNT_HOLDER}';

	public $id;
	public $name;
	public $subject;
	public $body;
	public $target;

	static public function get($id) {
		if (MailTemplate::$map == null) {
			MailTemplate::init();
		}

		return MailTemplate::$map[$id];
	}

	static public function getMap() {
	    if (MailTemplate::$map == null) {
	        MailTemplate::init();
	    }
	    return MailTemplate::$map;
	}

	static public function update($id, $name, $value) {
	    if (MailTemplate::$map == null) {
	        MailTemplate::init();
	    }

	    switch ($name) {
	        case 'subject':
	            MailTemplate::$map[$id]->subject = $value;
	            break;
	        case 'body':
	            MailTemplate::$map[$id]->body = $value;
	            break;
	    }

	    static::save();
	}

	/**
	 * 保存
	 */
	static public function save() {
	    if (MailTemplate::$map == null) {
	        MailTemplate::init();
	    }

	    $doc = new DOMDocument();
	    $doc->encoding = 'utf-8';
	    $doc->formatOutput = true;
	    $root = $doc->createElement('mailMap');
	    foreach (MailTemplate::$map as $key => $value) {
	        $mail = $doc->createElement('mail');
	        $mail->setAttribute('id', $key);
	        $mail->appendChild($doc->createElement('name', $value->name));
	        $mail->appendChild($doc->createElement('target', $value->target));
	        $mail->appendChild($doc->createElement('subject', $value->subject));
	        $body = $doc->createElement('body');
	        $body->appendChild($doc->createCDATASection($value->body));
	        $mail->appendChild($body);

	        $root->appendChild($mail);
	    }
	    $doc->appendChild($root);
	    $doc->save(MAIL_XMLFILE);
	}

	/**
	 * 初期化
	 */
	static private function init() {
	    $xml = simplexml_load_file(MAIL_XMLFILE);

		MailTemplate::$map= array();
		foreach ($xml->mail as $mail) {
			foreach ($mail->attributes() as $name => $value) {
				$item = new MailTemplate();
				$item->id = (string)$value;
				$item->subject = (string)$mail->subject;
				$item->body = (string)$mail->body;
				$item->target = (string)$mail->target;
				$item->name = (string)$mail->name;
				MailTemplate::$map[$item->id] = $item;
			}
		}
	}

	/**
	 * 初期ファイルに置き換える
	 */
	static public function repairInitXml() {
	    copy(MAIL_INITIAL_XMLFILE, MAIL_XMLFILE);
	}

	/**
	 * 本文置換
	 * @param unknown $param
	 */
	public function replaceMessage($param) {
		$result = $this->body;
		if (is_array($param)) {
			foreach ($param as $key => $value) {
				switch (gettype($value)) {
					case 'integer':
						$value = number_format($value);
						break;
				}
				$result = str_replace($key, $value, $result);
			}
		}

		return $result;
	}

	/**
	 * 本文置換
	 * @param unknown $param
	 */
	public function replaceTitle($param) {
		$result = $this->subject;
		if (is_array($param)) {
			foreach ($param as $key => $value) {
				switch (gettype($value)) {
					case 'integer':
						$value = number_format($value);
						break;
				}
				$result = str_replace($key, $value, $result);
			}
		}

		return $result;
	}

}
