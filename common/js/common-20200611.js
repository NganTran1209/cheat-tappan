/**
 * 共通スクリプト
 */

/**
 * submit
 */
function submitForm(frm, name, value) {
	var f = document.getElementById(frm);
	if (name != null) {
		var el = document.getElementById('id-' + name);
		el.value = value;
		f.appendChild(el);
	}
	f.submit();
}

$(function() {
	$('#userIcon').on('change', function (e) {
		$('#clear-userIconPreview').show();
		$('#iconValue').val('true');
	    var reader = new FileReader();
	    reader.onload = function (e) {
	        $("#userIconPreview").attr('src', e.target.result);
	    }
	    reader.readAsDataURL(e.target.files[0]);
	});

	$('#headerImage').on('change', function (e) {
		$('#clear-headerImagePreview').show();
		$('#headerValue').val('true');
	    var reader = new FileReader();
	    reader.onload = function (e) {
	        $("#headerImagePreview").attr('src', e.target.result);
	    }
	    reader.readAsDataURL(e.target.files[0]);
	});

	$('#logoImg').on('change', function (e) {
	    var reader = new FileReader();
	    reader.onload = function (e) {
	        $("#logoImgPreview").attr('src', e.target.result);
	    }
	    reader.readAsDataURL(e.target.files[0]);
	});

	$('#topLargeImg').on('change', function (e) {
	    var reader = new FileReader();
	    reader.onload = function (e) {
	        $("#topLargeImgPreview").attr('src', e.target.result);
	    }
	    reader.readAsDataURL(e.target.files[0]);
	});

});

