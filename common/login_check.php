<?php
if (!isLogin()) {
    setMessage('ログインをしてください。');
    header('Location: ' . getContextRoot() . '/');
    exit;
}