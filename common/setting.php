<?php
// SEO
// head google コンソール
define('GOOGLE_SITE_VERIFICATION', '');
// head GOOGLE_ANALYTICS
define('GOOGLE_ANALYTICS', '');

// TwitterCardCheck
// https://cards-dev.twitter.com/validator
//define ('SITE_NAME', 'スキマー隙間時間活用サイト');
define('DOMAIN', '');
define('KEYWORDS', '');
define('DESCRIPTION', '');
define('TWITTER_NAME', '@');
define('CREATOR', '@');

// law.php 特定商法
define ('COMPANY_NAME', '株式会社○○○○');
define ('COMPANY_MANAGER', '');
define ('COMPANY_ADD', '');
define ('COMPANY_MAIL', '');
define ('COMPANY_MAIL_DOMAIN', '');
define ('COMPANY_CURIO_DEALER', '1234567890');

// terms.php 利用規約制定日時
define ('TERMS_DAY', '2021年 03月05日');