$(".btn-menu").on("click", function() {
	if (!$(".side-bar").hasClass('active')) {
		$(".side-bar").addClass('active')
		$(".side-bar").removeClass('none-active')
	} else {
		$(".side-bar").removeClass('active')
		$(".side-bar").addClass('none-active')
	}
});
$(document).on("click", ".browse", function() {
	var file = $(this).parents().find(".file");
	file.trigger("click");
});
$('input[type="file"]').change(function(e) {
	var fileName = e.target.files[0].name;
	$("#file").val(fileName);
  
	if (e.target.name == 'photo') {
		$('#file-photo').text(fileName)
	} else if (e.target.name == 'pdf') {
		$('#file-pdf').text(fileName);
	}
 
	var reader = new FileReader();
	reader.onload = function(e) {
	  // get loaded data and render thumbnail.
	//   document.getElementById("preview").src = e.target.result;
	};
	// read the image file as a data URL.
	reader.readAsDataURL(this.files[0]);
});

$(document).on("click", ".browse1", function() {
	var file = $(this).parents().find(".file1");
	file.trigger("click");
});
$('input[type="file"]').change(function(e) {
	var fileName = e.target.files[0].name;
	$("#file").val(fileName);
  
	var reader = new FileReader();
	reader.onload = function(e) {
	  // get loaded data and render thumbnail.
	//   document.getElementById("preview1").src = e.target.result;
	};
	// read the image file as a data URL.
	reader.readAsDataURL(this.files[0]);
});

$(document).on("click", ".browse2", function() {
	var file = $(this).parents().find(".file2");
	file.trigger("click");
});
$('input[type="file"]').change(function(e) {
	var fileName = e.target.files[0].name;
	$("#file").val(fileName);
  
	var reader = new FileReader();
	reader.onload = function(e) {
	  // get loaded data and render thumbnail.
	//   document.getElementById("preview2").src = e.target.result;
	};
	// read the image file as a data URL.
	reader.readAsDataURL(this.files[0]);
});

$(document).ready(function() {
	$('#open').click(function(event) {
		opencam();
		$('#control').show();
	});
});

$(window).scroll(function () {
  if($(window).scrollTop() > 20) {
    $('.main-header').addClass('fixed');
    $('.other-main-header').addClass('fixed');
    $('.com-header').addClass('fixed');
  } else {
    $('.main-header').removeClass('fixed');
    $('.other-main-header').removeClass('fixed');
    $('.com-header').removeClass('fixed');
  }
});
$( document ).ready(function(){
	$(".com-sidebar-item__link .add-mapage-link").click(function() {
			   window.location.href = $(this).find('a:first').attr('href');
		   }); 
}); 
$(document).ready(function () {
	new WOW().init();
	// 
	$('.side-bar-content__item').click(function () {
		$(this).toggleClass('active');
		let target = $(this).data('target');

		if (target) {
			$(target).slideToggle();
		}
	});
	// 
	if($('.admin-page').parent('.mainContents').length > 0 ){
		$('.admin-page').parent('.mainContents').addClass('mainContentsAdmin');
	}
});