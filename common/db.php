<?php

include_once(__DIR__.'/define.php');
include_once(__DIR__.'/sqlmap.php');

class DB {
	private $pdo = null;

	/**
	 * コンストラクタ
	 */
	function __construct() {
		$this->pdo = new PDO(
				'mysql:host=127.0.0.1:3307;dbname='.DB_NAME.';charset=utf8mb4',
				DB_USER,
				DB_PASSWORD,
				array(PDO::ATTR_PERSISTENT => true,
					PDO::ATTR_ERRMODE => PDO::ERRMODE_EXCEPTION));
	}

	/**
	 * 更新
	 * @param unknown $sqlid
	 * @param unknown $param
	 */
	public function execute($sqlid, $param = null) {
		$sql = SqlMap::getInstance()->get($sqlid);
		$stmt = $this->pdo->prepare($sql);
		if (is_array($param)) {
			$stmt->execute($param);
		}
		else {
			$stmt->execute();
		}
	}

	/**
	 * 参照
	 * @param unknown $sqlid
	 * @param unknown $param
	 */
	public function query($sqlid, $param = null) {
		$sql = SqlMap::getInstance()->get($sqlid);

		$stmt = $this->pdo->prepare($sql);
		if (is_array($param)) {
			$stmt->execute($param);
		}
		else {
			$stmt->execute();
		}
		return $stmt->fetchAll();
	}

	/**
	 * 最終追加ID取得
	 * @return string
	 */
	public function lastInsertId() {
		return $this->pdo->lastInsertId();
	}

	/**
	 * トランザクション開始
	 */
	public function beginTransaction() {
		$this->pdo->beginTransaction();
	}

	/**
	 * コミット
	 */
	public function commit() {
		$this->pdo->commit();
	}

	/**
	 * ロールバック
	 */
	public function rollback() {
		$this->rollback();
	}
}