<?php

class SqlMap {
	/**
	 * シングルトンインスタンス
	 * @var unknown
	 */
	static private $instance;

	private $map = null;

	/**
	 * コンストラクタ
	 */
	private function __construct() {
		$this->init();
	}

	static public function getInstance() {
		if (static::$instance == null) {
			static::$instance = new SqlMap();
		}

		return static::$instance;
	}

	/**
	 * 初期化
	 */
	private function init() {
	    // ccsystem外に設置する場合はこの書き方
        // $xml = simplexml_load_file(__DIR__.'/../../sql/fream.xml');
        // ccsystem内の設置する時はこの書き方
        // $xml = simplexml_load_file(__DIR__.'/../sql/fream.xml');
        // Common内へ移動した
		$xml = simplexml_load_file(__DIR__.'/../common/sql/fream.xml');

		$this->map = array();
		foreach ($xml as $sql) {
			foreach ($sql->attributes() as $name => $value) {
				$this->map[(string)$value] = (string)$sql;
			}
		}
	}

	/**
	 * SQL取得
	 * @param unknown $sqlid
	 * @return mixed
	 */
	public function get($sqlid) {
		return $this->map[$sqlid];
	}
}