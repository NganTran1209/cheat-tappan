<?php include_once(__DIR__ . '/common/util.php'); ?>
<?php
$system_config = SystemConfig::select();
$info = new Information();
$item = new Item();
$request = new Request();
$top_flag = $system_config->enabled_search_bar;

?>
<?php include('header.php'); ?>
<?php if ($top_flag): ?>
    <style>
        .navbar {
            margin-bottom: 0;
            z-index: 1;
        }
        .searchBox {
            height: 420px;
            width: 100%;
            background: url("<?php echo SystemConfig::get_top_large_image_url(); ?>") no-repeat center center;
            background-size: cover;
        }
        .searchBoxForm {
            max-width: 980px;
            margin: auto;
        }
        .searchBoxForm form {
            width: 90%;
            padding-top: 181px;
            margin: auto;
        }
        @media screen and (max-width: 767px) {
            .searchBox {
                height: 150px;
            }

            .searchBoxForm form {
                padding-top: 60px;
            }
        }
    </style>
    <div class="searchBox">
        <div class="searchBoxForm">
            <form action="<?php echo HOME_URL; ?>/itemlist.php" method="post">
                <div class="input-group">
                    <input type="text" class="form-control" name="search"
                           value="<?= empty($_POST['search']) ? '' : $_POST['search'] ?>" placeholder="キーワードを入力">
                    <span class="input-group-append">
                    <button class="btn btn-secondary" type="submit"><i class="fa fa-search"></i></button></span>
                </div>
            </form>
        </div>
    </div>
<?php else: ?>
<style>
    .test {
        background: url("<?php echo SystemConfig::get_top_large_image_url(); ?>") no-repeat;
        background-size: cover;
    }
</style>

    <div class="homeContents text-center topImg bg-white test">
        <img class="img-fluid" src="<?php echo SystemConfig::get_top_large_image_url(); ?>">
    </div>
<?php endif; ?>

<div class="container homeContents">
    <div class="home-mark-wrap bg-white p-4 border rounded">
        <div class="row">
            <div class="col-md-4">
                <div class="home-mark"><i class="far fa-check-circle"></i></div>
                <h3>出品が簡単</h3>
                <p>最短3分で出品ができます。下書きも保存できるので複数の出品が簡単。</p>
            </div>
            <div class="col-md-4">
                <div class="home-mark"><i class="fas fa-yen-sign"></i></div>
                <h3>料金は成約時のみ</h3>
                <p>出品料や月会費は一切御座いません。売れた時のみ安心して出品いただけます。</p>
            </div>
            <div class="col-md-4">
                <div class="home-mark"><i class="fa fa-heart"></i></div>
                <h3>購入前も安心</h3>
                <p>購入前に質問できるので安心して取引ができます。</p>
            </div>
        </div>
    </div>
</div>

<div class="container">
    <div class="row">
        <div class="col-md-9 mainContents">
            <div class="bg-inner">
                <div class="mb-5">
                    <h2><i class="fas fa-award mr-2"></i>新着！出品情報</h2>
                    <div class="row">
                        <?php
                        $count = 0;
                        foreach ($item->selectAll() as $itemItem): ?>
                            <?php if (User::isInvalid($itemItem->owner_id)) {
                                continue;
                            } ?>
                            <?php if ($itemItem->enabled != 1) {
                                continue;
                            } ?>
                            <?php if (!$itemItem->owner_selling) {
                                continue;
                            } ?>
                            <?php
                            if ($count++ >= MAX_ITEM_LIST_COUNT) {
                                break;
                            }
                            ?>
                            <div class="col-6 col-md-3 mb-3">
                                <div class="border h-100 mb-3">
                                    <a href="<?php echo HOME_URL; ?>/itemdetail.php?id=<?= $itemItem->id ?>">
                                        <div class="priceOnImg">
                                            <div class="topListImg">
                                                <img class="img-fluid" src="<?= $itemItem->getThumbnailUrl() ?>" alt=""/>
                                                <?php //echo $itemItem->getThumbnailUrl(); ?>

                                            </div>
                                            <p><?php echo number_format($itemItem->price); ?><span class="smallEX">円</span></p>
                                        </div>

                                        <div class="small px-1">
                                            <?php echo mb_strimwidth($itemItem->title, 0, 34, '...');?>
                                            <?php if (isNew($itemItem->regist_date)): ?>
                                                <span class="newIcon">NEW</span>
                                            <?php endif; ?>
                                            <?php //echo toDateStringFromMySqlDatetime($itemItem->regist_date) ?>
                                        </div>
                                    </a>
                                    <div class="smallEX px-1">
                                        <?php echo mb_strimwidth($itemItem->owner->getViewNicknameOrId(), 0, 16, ''); ?> <?php //echo $itemItem->price; ?>
                                    </div>
                                </div>
                            </div>
                        <?php endforeach; ?>
                    </div>
                    <?php /*
                <ul class="list-group small">
                    <?php
                    $count = 0;
                    foreach ($item->selectAll() as $itemItem): ?>
                        <?php if (User::isInvalid($itemItem->owner_id)) {
                            continue;
                        } ?>
                        <?php if ($itemItem->enabled != 1) {
                            continue;
                        } ?>
                        <?php if (!$itemItem->owner_selling) {
                            continue;
                        } ?>
                        <?php
                        if ($count++ >= MAX_ITEM_LIST_COUNT) {
                            break;
                        }
                        ?>
                        <li class="list-group-item">
                            <img class="iconimg"
                                 src="<?php echo HOME_URL; ?>/images/item/id<?= $itemItem->id ?>/thumbnail.jpg"
                                 alt=""/>
                            <!--  <img class="iconimg" src="--><?php //echo HOME_URL; ?><!----><?php //echo 'images/lapias/icon_' . sprintf('%02d', $itemItem->category) ?><!--.png" alt=""/>-->
                            <a href="itemdetail.php?id=<?= $itemItem->id ?>">
                                <?= toDateStringFromMySqlDatetime($itemItem->regist_date) ?> <?= $itemItem->title ?></a>
                            <?php if (isNew($itemItem->regist_date)): ?>
                                <span class="newIcon">NEW</span>
                            <?php endif; ?>
                        </li>
                    <?php endforeach; ?>
                    <li class="list-group-item text-right"><a href="<?php echo HOME_URL; ?>/itemlist.php">出品一覧</a></li>
                </ul>
                */ ?>
                </div>
                <div class="mb-5">
                    <h2>購入希望情報</h2>
                    <ul class="list-group">
                        <?php
                        $count = 0;
                        foreach ($request->selectAll() as $requestItem): ?>
                            <?php if (User::isInvalid($requestItem->user_id)) {
                                continue;
                            } ?>
                            <?php
                            if ($requestItem->enabled != 1) {
                                continue;
                            }
                            if ($count++ >= MAX_REQUEST_LIST_COUNT) {
                                break;
                            }
                            ?>
                            <li class="list-group-item">
                                <a href="<?php echo HOME_URL; ?>/request_details.php?id=<?= $requestItem->id ?>"><i class="fa fa-chevron-right"
                                                                                                                    aria-hidden="true"></i> <?= toDateStringFromMySqlDatetime($requestItem->regist_date) ?> <?= $requestItem->title ?>
                                </a>
                                <?php if (isNew($requestItem->regist_date)): ?>
                                    <span class="newIcon">NEW</span>
                                <?php endif; ?>
                            </li>
                        <?php endforeach; ?>
                        <li class="list-group-item text-right"><a href="<?php echo HOME_URL; ?>/request_details.php">購入希望一覧</a></li>
                    </ul>
                </div>
                <div class="mb-5">
                    <h2>お知らせ</h2>
                    <ul class="list-group">
                        <?php
                        $count = 0;
                        foreach ($info->selectAll() as $infoItem) : ?>
                            <?php
                            if ($count++ >= MAX_INFOMATION_LIST_COUNT) {
                                break;
                            }
                            ?>
                            <li class="list-group-item">
                                <a href="<?php echo HOME_URL; ?>/information_details.php?id=<?= $infoItem->id ?>"><i class="fa fa-chevron-right"
                                                                                                                     aria-hidden="true"></i> <?= toDateStringFromMySqlDatetime($infoItem->regist_date) ?> <?= $infoItem->title ?>
                                </a>
                                <?php if (isNew($infoItem->regist_date)): ?>
                                    <span class="newIcon">NEW</span>
                                <?php endif; ?>
                            </li>
                        <?php endforeach; ?>
                        <li class="list-group-item text-right"><a
                                    href="<?php echo HOME_URL; ?>/information_details.php">お知らせ一覧</a>
                        </li>
                    </ul>
                </div>
            </div>
        </div>
        <div class="col-md-3 sideContents">
            <?php include('sidebar.php'); ?>
        </div>
    </div>
</div>

<?php include('footer.php'); ?>
