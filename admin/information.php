<?php include_once(__DIR__ . '/../common/util.php'); ?>
<?php include_once(__DIR__ . '/../common/admin.php'); ?>
<?php
$info = new Information();
$info->select();
if (isset($_POST['action'])) {
    if ($_POST['action'] == 'modify') {
        $info->title = $_POST['title'];
        $info->text = $_POST['text'];
        $info->id = $_POST['id'];
        $info->regist();
    } elseif ($_POST['action'] == 'add') {
        $info->title = $_POST['title'];
        $info->text = $_POST['text'];
        $info->regist();
    }
    setMessage('登録しました。');
}

$items = $info->selectAll();
?>
<?php include('../header.php'); ?>
<style>
    .main-header__search{
        display:none;
    }
    .main-header-link{
        justify-content: flex-end;
        margin-right: 30px;
    }
    @media screen and (max-width: 768px) {   
    .main-header-link{
        justify-content: flex-start !important;
        margin-right:0;
    }}
</style>
    <div class="container">
        <div class="row">
        <div class="col-md-3 sideContents pc-only">
                <?php include('adminsidebar.php'); ?>
            </div>
            <div class="col-md-9 mainContents mb-5">
                <div class="bg-inner  admin-content-title admin-page">
                    <h1>お知らせ投稿</h1>
                    <div class="admin-info-list">
                        <h2 class="bg-light p-2 fontBold my-3">お知らせ新規投稿</h2>
                        <form method="post" onsubmit="return confirm('追加します。')">
                            <div class="row">
                                <div class="col-sm-3 mb-3 font-2rem"> タイトル</div>
                                <div class="col-sm-9 mb-3 font-2rem">
                                    <input type="hidden" name="action" value="add"/>
                                    <input class="form-control" type="text" name="title" placeholder="タイトルを入力してください。"
                                           value=""/>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-sm-3 mb-3 font-2rem"> 本文</div>
                                <div class="col-sm-9 mb-3">
                                <textarea class="form-control" name="text" rows="6"
                                          placeholder="お知らせ本文を入力してください。"></textarea>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-sm-3 mb-3 font-2rem"></div>
                                <div class="col-sm-9">
                                    <input class="btn btn-block btn-sm btn-info" type="submit" value="投稿する"/>
                                </div>
                            </div>
                        </form>
                    </div>

                    <div class="admin-info-list">
                        <h2 class="bg-light p-2 fontBold my-3">お知らせ一覧</h2>
                        <?php foreach ($items as $item): ?>
                            <div class="card card-body mb-4 pb-3">
                                <form method="post" onsubmit="return confirm('登録します。')">
                                    <input type="hidden" name="id" value="<?= $item->id ?>"/>
                                    <input type="hidden" name="action" value="modify"/>
                                    <input class="form-control mb-2" type="text" name="title" value="<?= $item->title ?>"/>
                                    <textarea class="form-control mb-2" name="text"><?= $item->text ?></textarea>
                                    <div class="form-row">
                                        <div class="col-md-8">
                                            No <?= $item->id ?>：<?= $item->regist_date ?>
                                        </div>
                                        <div class="col-md-4">
                                            <input class="btn btn-block btn-sm btn-info" type="submit" value="更新"/>
                                        </div>
                                    </div>
                                </form>
                            </div>
                        <?php endforeach; ?>
                    </div>
                </div>
            </div>
            <div class="col-md-3 sideContents sp-only mt-5">
                <?php include('adminsidebar.php'); ?>
            </div>
        </div>
    </div>
<?php include('../footer.php'); ?>