<?php include_once(__DIR__ . '/../common/util.php'); ?>
<?php include_once(__DIR__ . '/../common/admin.php'); ?>
<?php
if (isset($_POST['action'])) {
    if ($_POST['action'] == 'modify') {
        registCodeList('registCarriagePlan', $_POST['id'], $_POST['name'], isset($_POST['enabled']) && $_POST['enabled']);
    } elseif ($_POST['action'] == 'add') {
         registCodeList('insertCarriagePlan', null, $_POST['name']);
    }
    setMessage('登録しました。');
}

$items = selectCodeList('selectCarriagePlan');
?>

<?php include('../header.php'); ?>

<style>
    .main-header__search{
        display:none;
    }
    .main-header-link{
        justify-content: flex-end;
        margin-right: 30px;
    }
    @media screen and (max-width: 768px) {   
    .main-header-link{
        justify-content: flex-start !important;
        margin-right:0;
    }}
</style>
<div class="container">
    <div class="row">
    <div class="col-md-3 sideContents pc-only">
                <?php include('adminsidebar.php'); ?>
            </div>
        <div class="col-md-9 mainContents">
            <div class="bg-inner admin-content-title admin-page">
                <h1>送料負担の追加</h1>
                <div class="mb-5 mt-5">
                    <form class="form-row" method="post" onsubmit="return confirm('追加します。')">
                        <input type="hidden" name="action" value="add"/>
                        <div class="col-md-8">
                            <input class="form-control" type="text" name="name" value=""/>
                        </div>
                        <div class="col-md-4">
                            <input class="form-control btn-info form-control-register" type="submit" value="追加"/>
                        </div>
                    </form>
                </div>
                <table class="table table-bordered table-striped table-cate">
                    <tr>
                        <th>No</th>
                        <th>項目名</th>
                        <th>有効</th>
                        <th>更新</th>
                    </tr>
                    <?php foreach ($items as $item): ?>
                        <tr>
                            <form name="modify_form" method="post" onsubmit="return confirm('変更します。')">
                                <input type="hidden" name="id" value="<?= $item['id'] ?>"/>
                                <input type="hidden" name="action" value="modify"/>
                                <td class="text-center"><?= $item['id'] ?></td>
                                <td><input class="form-control" type="text" name="name"
                                           value="<?= $item['name'] ?>"/></td>
                                <td class=""><input class="form-control btn-info" type="checkbox" name="enabled"<?= $item['enabled'] ? ' checked="checked"' : '' ?> /></td>
                                <td>
                                    <input class="form-control btn-info btn-sm form-control-register" type="submit" name="submit" value="登録" />
                                    <!--                                <input class="form-control btn-outline-danger btn-sm" type="submit" name="submit" value="削除" />-->
                                </td>
                            </form>
                        </tr>
                    <?php endforeach; ?>
                </table>
            </div>
        </div>
        <div class="col-md-3 sideContents sp-only mt-5">
                <?php include('adminsidebar.php'); ?>
            </div>
    </div>
</div>
<?php include('../footer.php'); ?>
