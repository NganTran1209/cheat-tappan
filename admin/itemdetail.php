<?php include_once(__DIR__ . '/../common/util.php'); ?>
<?php include_once(__DIR__ . '/../common/admin.php'); ?>
<?php
if (isset($_POST['action'])) {
    if ($_POST['action'] == 'approval') {
        $item = new Item();
        $item->select($_POST['item_id']);
        $item->approval();

        setMessage('出品を許可しました。');
    } elseif ($_POST['action'] == 'rejection') {
        $item = new Item();
        $item->id = $_POST['item_id'];
        $item->regection();

        setMessage('出品を却下しました。');
    }
    header('Location: ' . getContextRoot() . '/admin/sellitems.php');
    exit();
}

$item = new Item();
$item->select($_GET['id']);
$view_evaluate = true;
?>
<?php include('../header.php'); ?>
    <div class="container">
        <div class="row">
            <div class="col-md-3 sideContents">
                <?php include('adminsidebar.php'); ?>
            </div>
            <div class="col-md-9 mainContents">
                <div class="bg-inner">
                    <h1><?= $item->title ?></h1>
                    <div class="row">
                        <?php include_once(__DIR__ . '/../common/parts/item_image_area.php'); ?>
                        <?php include_once(__DIR__ . '/../common/parts/item_info_area.php'); ?>
                    </div>

                    <div class="mb-4 mt-5">
                        <div class="text-center mt-5 mb-5">
                            <span class="font40 fontBold fontRed"><?= number_format($item->price) ?></span>
                            <span class="smallEX">円</span>
                        </div>
                        <div class="row">
                            <div class="col-sm-6 pb-5 pt-5">
                                <form method="post" onsubmit="return confirm('出品を許可します');">
                                    <input type="hidden" name="item_id" value="<?= $item->id ?>"/>
                                    <input type="hidden" name="action" value="approval"/>
                                    <input class="btn btn-block btn-info btn-sm btn-info" type="submit"
                                           value="出品を許可する"/>
                                </form>
                            </div>
                            <div class="col-sm-6 pt-5 pb-5">
                                <form method="post" onsubmit="return confirm('出品を却下します');">
                                    <input type="hidden" name="item_id" value="<?= $item->id ?>"/>
                                    <input type="hidden" name="action" value="rejection"/>
                                    <input class="btn btn-block btn-outline-danger btn-sm" type="submit"
                                           value="出品を却下する"/>
                                </form>
                            </div>
                        </div>
                    </div>

                    <div class="itemContentText">
                        <h3>商品説明</h3>
                        <?php echo nl2br(htmlentities($item->remarks)); ?>
                    </div>
                </div>
            </div>
        </div>
    </div>

<?php include('../footer.php'); ?>