<?php include_once(__DIR__ . '/../common/util.php'); ?>
<?php include_once(__DIR__ . '/../common/admin.php'); ?>
<?php
$user = new User();
if (isset($_POST['action'])) {
    if ($_POST['action'] == 'approval') {
        $user->select($_POST['id']);
        $user->selling = true;
        $user->buying = true;
        $user->updateBuying();
        $user->updateSelling();
    } elseif ($_POST['action'] == 'stop') {
        $user->select($_POST['id']);
        $user->selling = false;
        $user->buying = false;
        $user->updateBuying();
        $user->updateSelling();
    } elseif ($_POST['action'] == 'login-ok') {
        $user->select($_POST['id']);
        $user->enabled = true;
        $user->updateEnabled2();
    } elseif ($_POST['action'] == 'login-ng') {
        $user->select($_POST['id']);
        $user->enabled = false;
        $user->updateEnabled2();
    } elseif ($_POST['action'] == 'remove') {
        $user->select($_POST['id']);
        $user->updateInvalid();
        setMessage($user->name . 'さまの情報を削除いたしました。');
    } elseif ($_POST['action'] == 'send-mail') {
        $user->select($_POST['id']);
        $user->sendRegistMail();
        setMessage($user->name . 'さまへ本登録メールを再送しました。');
    } elseif ($_POST['action'] == 'set-enabled') {
        $user->select($_POST['id']);
        $user->updateEnabled();
        setMessage($user->name . 'さまを本登録しました。');
    }
}

$items = $user->selectAll();
$sort = 0;
if (isset($_POST['sort'])) {
    $sort = $_POST['sort'];
    if ($sort == 0) {
        // 0: 五十音順
        usort($items, function ($a, $b) {
            return strcmp($a->name_kana, $b->name_kana);
        });
    } elseif ($sort == 1) {
        // 1: 本人確認証（証明書確認待ち）
        usort($items, function ($a, $b) {
            if ($a->identification == 0 && $b->identification == 0) {
                if ($a->identification_user_id == null && $b->identification_user_id == null) {
                    return 0;
                }
                if ($a->identification_user_id == null) {
                    return 1;
                }
                if ($b->identification_user_id == null) {
                    return -1;
                }
                return 0;
            } elseif ($a->identification != $b->identification) {
                return $a->identification - $b->identification;
            }
            return 0;
        });
    } elseif ($sort == 2) {
        // 2: 本人確認証（未提出）
        usort($items, function ($a, $b) {
            if ($a->identification == 0 && $b->identification == 0) {
                if ($a->identification_user_id == null && $b->identification_user_id == null) {
                    return 0;
                }
                if ($a->identification_user_id == null) {
                    return -1;
                }
                if ($b->identification_user_id == null) {
                    return 1;
                }
                return 0;
            } elseif ($a->identification != $b->identification) {
                return $a->identification - $b->identification;
            }
            return 0;
        });
    } elseif ($sort == 3) {
        // 3: 本人確認証（確認済）
        usort($items, function ($a, $b) {
            if ($a->identification != $b->identification) {
                return $b->identification - $a->identification;
            } elseif ($a->identification == 0 && $b->identification == 0) {
                if ($a->identification_user_id == null && $b->identification_user_id == null) {
                    return 0;
                }
                if ($a->identification_user_id == null) {
                    return 1;
                }
                if ($b->identification_user_id == null) {
                    return -1;
                }
                return 0;
            }
            return 0;
        });
    } elseif ($sort == 4) {
        // 4: 出品可否順
        usort($items, function ($a, $b) {
            if ($a->selling && $b->selling) {
                return 0;
            }

            if ($a->selling) {
                return -1;
            }

            return 1;
        });
    } else {
        $sort = 0;
        usort($items, function ($a, $b) {
            return strcmp($a->name_kana, $b->name_kana);
        });
    }
} else {
    $sort = 0;
    usort($items, function ($a, $b) {
        return strcmp($a->name_kana, $b->name_kana);
    });
}
?>
<?php include('../header.php'); ?>
<style>
    .main-header__search{
        display:none;
    }
    .main-header-link{
        justify-content: flex-end; 
        margin-right: 30px;
    }
    @media screen and (max-width: 768px) {   
    .main-header-link{
        justify-content: flex-start !important;
        margin-right:0;
        /* margin-right: 30px; */
    }}
</style>
    <div class="container">
        <div class="row">
        <div class="col-md-3 sideContents pc-only">
                <?php include('adminsidebar.php'); ?>
            </div>
            <!-- col-sm-3 -->
            <div class="col-md-9 mainContents">
                <div class="bg-inner admin-content-title admin-page">
                    <h1>ユーザー一覧・許可待ち確認</h1>
                    <div class="row mb-3">
                        <!--▼▼▼検索ページビュー▼▼▼ -->
                        <?php /*?>
                      ◆流れ
                      ① 検索条件にマッチする行に目印を設定。
                      ② １ページ目に表示する対象検索で検索条件のマッチングも複合条件として再表示。
                      ③ ページングの方は、検索条件を引き継ぐのでマッチングした行のみを対象としたページング。
                      ※検索条件を変更すると、１ページ目に戻ります。
                      ※検索条件にマッチングしたデータしか表示しなくなる対応なので後続ページに空のページが表示されてしまう。
                      ※五十音順（名前）はひらがな＞カタナカの順に並びます。
                    <?php */ ?>
                        <!--ソート-->
                        <form id="frm" method="post" class="col-md-6 mt-5">
                            <select name="sort" onchange="frm.submit();" class="form-control">
                                <option value="0"<?= $sort == 0 ? ' selected' : '' ?>>五十音順（名前）</option>
                                <option value="1"<?= $sort == 1 ? ' selected' : '' ?>>本人確認書類（証明書確認待ち）</option>
                                <option value="2"<?= $sort == 2 ? ' selected' : '' ?>>本人確認書類（未提出）</option>
                                <option value="3"<?= $sort == 3 ? ' selected' : '' ?>>本人確認書類（確認済）</option>
                                <!--              <option value="4"-->
                                <? //= $sort == 4 ? ' selected' : '' ?><!-- >出品可否順</option> -->
                            </select>
                        </form>
                        <!--全文検索-->
                        <form action="#" class="col-md-6 mt-5">
                            <input type="text" name="search" value="" id="id_search" class="form-control"
                                   placeholder="キーワードを入れてください"/>
                        </form>
                    </div>
                    <!-- 表示数制限 -->
                    <style>
                        .btn {
                            font-size: 1em;
                        }
                    </style>
                    <!-- ▲▲▲検索ページビュー▲▲▲ -->
                    <div class="table-responsive">
                        <table class="table table-bordered table-hover small text-nowrap  table-cate">
                            <thead>
                            <tr class="text-center">
                                <th>出品・購入可否</th>
                                <!--<th>購入可否</th>-->
                                <th>本人確認書類</th>
                                <th>IP</th>
                                <th>メール</th>
                                <th>名前</th>
                                <th>カナ</th>
                                <th>ID</th>
                                <!--<th>会社名</th>-->
                                <!--<th>電話番号</th>-->
                                <!--<th>住所</th>-->
                                <!--<th>属性</th>-->
                                <!--<th>地域</th>-->
                                <th>ログイン許可</th>
                                <th>仮登録メール再送</th>
                                <th>強制本登録設定</th>
                                <th>削除</th>
                            </tr>
                            </thead>
                            <tbody>
                            <?php foreach ($items as $item): ?>
                                <?php if ($item->invalid) {
                                    continue;
                                } ?>
                                <?php if ($sort == 1 && ($item->identification || $item->identification_user_id == null)) {
                                    continue;
                                } ?>
                                <?php if ($sort == 2 && ($item->identification || $item->identification_user_id != null)) {
                                    continue;
                                } ?>
                                <?php if ($sort == 3 && !$item->identification) {
                                    continue;
                                } ?>
                                <tr>
                                    <td class="col-sm-2 text-center">
                                        <?php if ($item->settle_plan > 0 && !$item->selling): ?>
                                            <form method="post"
                                                  onsubmit="return window.confirm('<?= $item->name ?>さまの出品・購入を許可します。')">
                                                <input type="hidden" name="id" value="<?= $item->id ?>"/>
                                                <button class="btn btn-block btn-danger btn-sm" type="submit" name="action"
                                                        value="approval">停止中
                                                </button>
                                            </form>
                                        <?php elseif ($item->settle_plan > 0 && $item->selling): ?>
                                            <form method="post" style="display: inline"
                                                  onsubmit="return window.confirm('<?= $item->name ?>さまの出品・購入を停止します。')">
                                                <input type="hidden" name="id" value="<?= $item->id ?>"/>
                                                <button class="btn btn-block btn-success btn-sm" type="submit" name="action"
                                                        value="stop">許可中
                                                </button>
                                            </form>
                                        <?php else: ?> -
                                        <?php endif; ?>
                                    </td>
                                    <td class="col-sm-2 text-center">
                                        <?php if ($item->identification == 1): ?> 確認済
                                        <?php elseif ($item->identification_user_id != null): ?>
                                            <a href="userinfo.php?id=<?= $item->id ?>">証明書確認待ち</a>
                                        <?php else: ?> 未提出
                                        <?php endif; ?>
                                    </td>
                                    <td class="col-sm-2 text-left">
                                        <?= $item->ipaddr ?>
                                    </td>
                                    <td class="col-sm-2 text-left">
                                        <?= $item->email ?>
                                    </td>
                                    <td class="col-sm-2 text-left">
                                        <a href="userinfo.php?id=<?= $item->id ?>"><?= $item->name ?></a>
                                    <td class="col-sm-2 text-left">
                                        <?= $item->name_kana ?>
                                    </td>
                                    <td class="col-sm-2 text-left">
                                        <?= $item->getViewId() ?>
                                    </td>
                                    <!--<td class="col-sm-2 text-left"><?= $item->company ?></td>-->
                                    <!--<td class="col-sm-2 text-left"><?= $item->tel ?></td>-->
                                    <!--<td class="col-sm-2 text-left"><?= $item->addr ?></td>-->
                                    <!--<td class="col-sm-2 text-center"><?= $item->type_name ?></td>-->
                                    <!--<td class="col-sm-2 text-center"><?= $item->pref_name ?></td>-->
                                    <td class="col-sm-2 text-center">
                                        <?php if ($item->enabled): ?>
                                            <form method="post" style="display: inline"
                                                  onsubmit="return window.confirm('<?= $item->name ?>さまのログインを停止します。')">
                                                <input type="hidden" name="id" value="<?= $item->id ?>"/>
                                                <button class="btn btn-block btn-success btn-sm" type="submit" name="action"
                                                        value="login-ng">許可中
                                                </button>
                                            </form>
                                        <?php else: ?>
                                            <form method="post" style="display: inline"
                                                  onsubmit="return window.confirm('<?= $item->name ?>さまのログインを許可します。')">
                                                <input type="hidden" name="id" value="<?= $item->id ?>"/>
                                                <button class="btn btn-block btn-danger btn-sm" type="submit" name="action"
                                                        value="login-ok">停止中
                                                </button>
                                            </form>
                                        <?php endif; ?>
                                    </td>
                                    <td class="col-sm-2 text-center">
                                        <?php if ($item->enabled == 0): ?>
                                            <form method="post" style="display: inline"
                                                  onsubmit="return window.confirm('<?= $item->name ?>さまへ仮登録メールを再送します。')">
                                                <input type="hidden" name="id" value="<?= $item->id ?>"/>
                                                <button class="btn btn-block btn-outline-dark btn-sm" type="submit"
                                                        name="action" value="send-mail">仮登録メール再送
                                                </button>
                                            </form>
                                        <?php endif; ?>
                                    </td>
                                    <td class="col-sm-2 text-center">
                                        <?php if ($item->enabled == 0): ?>
                                            <form method="post" style="display: inline"
                                                  onsubmit="return window.confirm('<?= $item->name ?>さまを本登録します。')">
                                                <input type="hidden" name="id" value="<?= $item->id ?>"/>
                                                <button class="btn btn-block btn-outline-dark btn-sm" type="submit"
                                                        name="action" value="set-enabled">本登録
                                                </button>
                                            </form>
                                        <?php endif; ?>
                                    </td>
                                    <td class="col-sm-2 text-center">
                                        <?php if (!$item->invalid): ?>
                                            <form method="post" style="display: inline"
                                                  onsubmit="return window.confirm('<?= $item->name ?>さまのユーザー情報を削除します。')">
                                                <input type="hidden" name="id" value="<?= $item->id ?>"/>
                                                <button class="btn btn-block btn-danger btn-sm" type="submit" name="action"
                                                        value="remove">削除(無効)
                                                </button>
                                            </form>
                                        <?php endif; ?>
                                    </td>
                                </tr>
                            <?php endforeach; ?>
                            </tbody>
                        </table>
                    </div>
                    <br>
                    <div class="text-center">
                        <nav>
                            <ul class="pagination justify-content-center">
                                <li class="page-item"><span id="prev">← 前へ</span></li>
                                <li class="page-item hidden"><span id="page"> </span></li>
                                <li class="page-item"><span id="next">次へ →</span></li>
                            </ul>
                        </nav>
                    </div>
                </div>
            </div>
            <div class="col-md-3 sideContents sp-only mt-5">
                <?php include('adminsidebar.php'); ?>
            </div>
        </div>
    </div>
<?php include('../footer.php'); ?>