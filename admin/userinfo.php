<?php include_once(__DIR__ . '/../common/util.php'); ?>
<?php include_once(__DIR__ . '/../common/admin.php'); ?>
<?php include_once(__DIR__ . '/../entity/identification.php'); ?>
<?php
$user_id = 0;
if (isset($_GET['id'])) {
    $user_id = $_GET['id'];
} elseif (isset($_POST['id'])) {
    $user_id = $_POST['id'];
}

if ($user_id == 0) {
    setMessage('不正アクセスです。');
    header('Location: '.getContextRoot().'/admin/userlist.php');
}

if (isset($_POST['regist'])) {
    $entity = new User();
    $entity->approval($_POST['id']);
    setMessage('本人確認を行いました。');
}

if (isset($_POST['action']) && $_POST['action'] == 'upload') {

    if ($_FILES['photo']['error'] == 0) {
        Identification::regist_tmp($user_id);
        $_SESSION['target_user_id'] = $user_id;
        header('Location: '.getContextRoot().'/admin/upload_identification_confirm.php');
        exit();
    } else {
        setMessage('本人確認書類画像が指定されていません。');
    }
}

$entity = new User();
$entity->select($user_id);

$item = new Item();
$items = $item->selectSell($user_id);

$page = empty($_POST['page']) ? '1' : $_POST['page'];
$count = 0;
$index = 0;

?>

<?php include(__DIR__ . '/../header.php'); ?>
<div class="container">
    <div class="row">
        <div class="col-md-3 sideContents">
            <?php include('adminsidebar.php'); ?>
        </div>
        <div class="col-md-9 mainContents">
            <div class="bg-inner">
                <style>
                    table td {
                        word-break: break-all;
                    }
                </style>

                <div class="">
                    <h1>ユーザー情報</h1>
                    <input type="hidden" name="id" value="<?= $entity->id ?>"/>
                    <table class="table table-bordered small">
                        <tr>
                            <th>ユーザーID</th>
                            <td><?= $entity->getViewId() ?></td>
                        </tr>
                        <tr>
                            <th>メール</th>
                            <td><?= $entity->email ?></td>
                        </tr>
                        <tr>
                            <th>名前</th>
                            <td><?= $entity->name ?></td>
                        </tr>
                        <tr>
                            <th>会社名</th>
                            <td><?php echo $entity->company; ?></td>
                        </tr>
                        <tr>
                            <th>電話番号</th>
                            <td><?php echo $entity->tel; ?></td>
                        </tr>

                        <tr>
                            <th>郵便番号</th>
                            <td><?php echo $entity->zip; ?></td>
                        </tr>
                        <tr>
                            <th>住所</th>
                            <td><?php echo $entity->addr; ?></td>
                        </tr>
                        <tr>
                            <th>属性</th>
                            <td><?php echo $entity->type_name; ?></td>
                        </tr>
                        <tr>
                            <th>地域</th>
                            <td><?php echo $entity->pref_name; ?></td>
                        </tr>
                    </table>

                    <div class="mb-3">
                        <h2 class="bg-light p-2 fontBold my-3">本人確認証</h2>
                        <?php if ($entity->identification == 1): ?>
                            <h3 class="fontBold my-2">本人確認済み</h3>
                            <div class="row">
                                <div class="col-md-6">
                                    <a href="identification_photo.php?user_id=<?= $entity->id ?>" target="_blank">
                                        <img class="img-fluid"
                                             src="identification_photo.php?user_id=<?= $entity->id ?>"/></a>
                                </div>
                                <?php if (Identification::hasPhoto2($entity->id)): ?>
                                    <div class="col-md-6">
                                        <a href="identification_photo2.php?user_id=<?= $entity->id ?>" target="_blank">
                                            <img class="img-fluid"
                                                 src="identification_photo2.php?user_id=<?= $entity->id ?>"/></a>
                                    </div>
                                <?php endif; ?>
                            </div>
                        <?php elseif ($entity->identification_user_id != null): ?>
                            <h3 class="fontBold my-2">証明書確認待ち</h3>
                            <div class="row">
                                <div class="col-6">
                                    <h3 class="fontBold my-2">1枚目</h3>
                                    <img class="img-fluid identification_photo"
                                         src="identification_photo.php?user_id=<?= $entity->id ?>"/>
                                </div>
                                <div class="col-6">
                                    <?php if (Identification::hasPhoto2($entity->id)): ?>
                                        <h3 class="fontBold my-2">2枚目</h3>
                                        <img class="img-fluid identification_photo"
                                             src="identification_photo2.php?user_id=<?= $entity->id ?>"/>
                                    <?php endif; ?>
                                </div>
                            </div>
                        <?php else: ?>
                            <h3 class="fontBold my-2">本人確認証が未提出です</h3>
                            <form method="post" enctype="multipart/form-data">
                                <h3 class="fontBold my-2">1枚目</h3>
                                <div class="input-group mb-2">
                                    <div class="custom-file">
                                        <input type="file" name="photo" id="photo" class="custom-file-input"
                                               aria-describedby="photoAdd">
                                        <label class="custom-file-label" for="photo" data-browse="参照">ファイル選択...</label>
                                    </div>
                                </div>

                                <h3 class="fontBold my-2">2枚目</h3>
                                <div class="input-group mb-2">
                                    <div class="custom-file">
                                        <input type="file" name="photo2" id="photo2" class="custom-file-input"
                                               aria-describedby="photo2Add">
                                        <label class="custom-file-label" for="photo2" data-browse="参照">ファイル選択...</label>
                                    </div>
                                </div>
                                <input type="hidden" name="id" value="<?= $entity->id ?>"/>
                                <input type="hidden" name="action" value="upload"/>
                                <input class="btn btn-block btn-info my-4" type="submit" value="アップロード"/>
                            </form>
                        <?php endif; ?>
                    </div>
                    <?php if ($entity->identification != 1 && $entity->identification_user_id != null): ?>
                        <form id="form" method="post" onsubmit="return confirm('承認します。'); ">
                            <div class="my-4">
                                <input type="hidden" name="id" value="<?= $entity->id ?>"/>
                                <input class="btn btn-block btn-info" type="submit" name="regist" value="承認する"/>
                            </div>
                        </form>
                    <?php endif; ?>
                </div>
                <?php if ($entity->identification == 1): ?>
                    <h1>出品中一覧</h1>
                    <table class="table table-bordered table-striped small">
                        <tr>
                            <!--<th>画像</th>-->
                            <th>掲載日</th>
                            <th>タイトル</th>
                            <th>商品価格</th>
                            <!--<th>交渉数</th>-->
                            <th>状態</th>

                        </tr>
                        <?php foreach ($items as $item): ?>
                            <?php $count++;
                            if (ceil(++$index / MAX_PAGE_COUNT) != $page) {
                                continue;
                            }
                            ?>
                            <tr>
                                <!--<td class="col-sm-2 text-center"><img class="img-fluid" src="<?= getContextRoot() ?>/images/item/id<?= $item->id ?>/thumbnail.jpg" alt=""/></td>-->
                                <td class="text-right"><?= $item->regist_date ?></td>
                                <td><a href="transaction.php?item_id=<?= $item->id ?>"><?= $item->title ?></a></td>
                                <td class="text-right"><?= number_format($item->price) ?>円</td>
                                <!--<td class="col-sm-1 text-center"><?= number_format($item->negotiation_count) ?>件</td>-->
                                <td class="text-center"><?= empty($item->buyer_id) ? $item->getStatusText() : '売約済' ?></td>
                            </tr>
                        <?php endforeach; ?>
                    </table>
                    <form id="fm-param" method="post">
                        <input type="hidden" id="id-page" name="page" value="<?= $page ?>"/>
                        <?php
                        $prev_page = $page - 1;
                        $max_page = ceil($count / MAX_PAGE_COUNT);
                        $next_page = min($max_page, $page + 1);
                        $view_start_page = max(1, $page - ceil(VIEW_PAGE_SELECT_COUNT / 2));
                        $view_last_page = min($view_start_page + VIEW_PAGE_SELECT_COUNT - 1, $max_page);
                        if ($view_last_page - $view_start_page < VIEW_PAGE_SELECT_COUNT) {
                            $view_start_page = max(1, $view_last_page - VIEW_PAGE_SELECT_COUNT + 1);
                        }
                        ?>
                        <?php if ($prev_page > 0): ?>
                            <a class="sortButton" href=""
                               onclick="submitForm('fm-param', 'page', '<?= $prev_page ?>'); return false;">前へ</a>
                        <?php else: ?>
                            <a class="sortButton" href="" onclick="return false;">前へ</a>
                        <?php endif; ?>

                        <?php for ($i = $view_start_page; $i <= $view_last_page; $i++): ?>
                            <?php if ($page != $i): ?>
                                <a class="sortButton" href=""
                                   onclick="submitForm('fm-param', 'page', '<?= $i ?>'); return false;"><?= $i ?></a>
                            <?php else: ?>
                                <a class="sortButton active" href="" onclick="return false;"><?= $i ?></a>
                            <?php endif; ?>
                        <?php endfor; ?>

                        <?php if ($page < $max_page): ?>
                            <a class="sortButton" href=""
                               onclick="submitForm('fm-param', 'page', '<?= $next_page ?>'); return false;">次へ</a>
                        <?php else: ?>
                            <a class="sortButton" href="" onclick="return false;">次へ</a>
                        <?php endif; ?>

                        <p class="small"><?= number_format(($page - 1) * MAX_PAGE_COUNT + 1) ?>
                            ～<?= number_format(min($count, $page * MAX_PAGE_COUNT)) ?>件/<?= number_format($count) ?>
                            件</p>
                    </form>
                <?php endif; ?>
            </div>
        </div>
    </div>
</div>

<?php include('../footer.php'); ?>




