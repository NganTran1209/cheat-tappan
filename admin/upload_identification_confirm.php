<?php include_once(__DIR__ . '/../common/util.php'); ?>
<?php include_once(__DIR__ . '/../common/admin.php'); ?>
<?php include_once(__DIR__ . '/../entity/identification.php'); ?>
<?php
if (!isset($_SESSION['target_user_id'])) {
    setMessage('不正アクセスです。');
    header('Location: ' . getContextRoot() . '/admin/userlist.php');
}

$user_id = $_SESSION['target_user_id'];
if (isset($_POST['action']) && $_POST['action'] == 'regist') {
    Identification::registFromAdmin($user_id);
    unset($_SESSION['identification']);
    setMessage('本人証明書を登録しました。');
    header('Location: ' . getContextRoot() . '/admin/userinfo.php?id=' . $user_id);
    exit();
}
?>
<?php include(__DIR__ . '/../header.php'); ?>
    <div class="container">
        <div class="row">
            <div class="col-md-3 sideContents">
                <?php include('adminsidebar.php'); ?>
            </div>
            <div class="col-md-9 mainContents">
                <div class="bg-inner">
                    <h1>本人確認書類アップロード</h1>
                    本人確認書類を提出します。
                    <form method="post" enctype="multipart/form-data">
                        <h2 class="bg-light p-2 fontBold my-3">画像提出</h2>
                        <div class="row">
                            <div class="col-6">
                                <h3 class="fontBold my-2">1枚目</h3>
                                <img class="img-fluid" src="identification_photo.php?user_id=<?= $user_id ?>" alt=""/>
                            </div>
                            <div class="col-6">
                                <?php if (!Identification::hasPhoto2($user_id)) : ?>
                                <?php else : ?>
                                    <h3 class="fontBold my-2">2枚目</h3>
                                    <img class="img-fluid" src="identification_photo2.php?user_id=<?= $user_id ?>" alt=""/>
                                <?php endif ?>
                            </div>
                        </div>
                        <div class="row my-4">
                            <div class="col-md-8">
                                <form method="post">
                                    <input type="hidden" name="action" value="regist"/>
                                    <input class="btn btn-block btn-info" type="submit" value="登録する"/>
                                </form>
                            </div>
                            <div class="col-md-4">
                                <form method="get" action="userinfo.php">
                                    <input type="hidden" name="id" value="<?= $user_id ?>"/>
                                    <input class="btn btn-block btn-outline-dark" type="submit" value="変更する"/>
                                </form>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
<?php include('../footer.php'); ?>