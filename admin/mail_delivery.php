<?php include_once(__DIR__ . '/../common/util.php'); ?>
<?php include_once(__DIR__ . '/../common/admin.php'); ?>
<?php include_once(__DIR__ . '/../common/mail_template.php'); ?>
<?php include_once(__DIR__ . '/../common/mail.php'); ?>
<?php include_once(__DIR__ . '/../entity/free_input_item.php'); ?>
<?php
$user = new User();
$users = $user->selectAll();
$system_config = SystemConfig::select();
$subject = '';
$body = '';
if (isset($_POST['send_mail'])) {
    $target_users = [];
    foreach ($users as $user) {
        if ($_POST['target_type'] == 'select') {
            if (!in_array($user->id, $_POST['targets'])) {
                continue;
            }
        }
        $target_users[] = $user;
    }

    if (!empty($target_users)) {
        $mail_history_id = MailHistory::regist($_POST['subject'], $_POST['body']);
        $mail = new Mail();
        foreach ($target_users as $user) {
            $param = [
                MailTemplate::REPLACE_USER_NAME => $user->name,
                MailTemplate::REPLACE_COMPANY_NAME => $user->company,
                MailTemplate::REPLACE_PREF_NAME => $user->pref_name,
            ];

            $template = new MailTemplate();
            $template->subject = $_POST['subject'];
            $template->body = $_POST['body'];

            $mail->to = $user->email;
            $mail->from = $system_config->from_email;
            $mail->fromName = $system_config->from_email_name;
            $mail->body = $template->replaceMessage($param);
            $mail->subject = $template->replaceTitle($param);

            $mail->sendMail();
            MailHistoryTarget::regist($mail_history_id, $user->id);
        }

        setMessage('メールを送信しました。');
    } else {
        setMessage('送信先を１件以上選択してください。');
        $subject = $_POST['subject'];
        $body = $_POST['body'];
    }
}
?>
<?php include('../header.php'); ?>

<style>
    .main-header__search{
        display:none;
    }
    .main-header-link{
        justify-content: flex-end;
        margin-right: 30px;
    }
    @media screen and (max-width: 768px) {   
    .main-header-link{
        justify-content: flex-start !important;
        margin-right:0;
    }}
</style>
<div class="container">
    <div class="row">
    <div class="col-md-3 sideContents pc-only">
                <?php include('adminsidebar.php'); ?>
            </div>
        <div class="col-md-9 mainContents">
            <div class="bg-inner admin-content-title admin-page">
                <form method="post">
                    <h1>メール配信</h1>
                    <div class="font-2rem">
                    <div class="custom-control custom-radio custom-control-inline mt-5 ">
                        <input type="radio" class="custom-control-input" id="customRadio" name="target_type" value="all" required>
                        <label class="custom-control-label" for="customRadio">全員一括送信</label>
                    </div>
                    <div class="custom-control custom-radio custom-control-inline">
                        <input type="radio" class="custom-control-input" id="customRadio2" name="target_type" value="select" required>
                        <label class="custom-control-label" for="customRadio2">複数送信</label>
                    </div>

                    <div class="form-group mt-5">
                        <label for="exampleFormControlSelect2">複数送信</label>
                        <select multiple class="form-control u-mb01" name="targets[]" id="exampleFormControlSelect2">
                            <?php foreach($users as $user):?>
                                <option value="<?= $user->id ?>"><?= $user->name?>(<?= $user->email?>)</option>
                            <?php endforeach;?>
                        </select>
                       <p> <span class="badge badge-dark">Ctrl</span> 押しながらクリックで複数選択</p>
                    </div>

                    <div class="form-group mt-4">
                        <label class="fontBold" for="ken">件名</label>
                        <input class="form-control" type="text" name="subject" id="subject" placeholder="例）件名を入力してください" value="<?= $subject ?>" required>
                    </div>
                    <div class="form-group mt-4">
                        <label class="fontBold" for="test">本文</label>
                        <textarea class="form-control u-mb01" name="body" id="body" rows="6"
                                  placeholder="例）メール送信内容を記入してください" required><?= $body ?></textarea>
                        <p>名前<?= MailTemplate::REPLACE_USER_NAME ?></p>
                        <p>会社名<?= MailTemplate::REPLACE_COMPANY_NAME ?></p>
                        <p>地域(都道府県？)<?= MailTemplate::REPLACE_PREF_NAME?></p>
                        <p>本文に上記を挿入できるようにしたい</p>
                    </div>
                    <div class="col-md-4 admin-mail-send">
                        <input class="form-control btn-info" type="submit" name="send_mail" value="送信"/>
                    </div>

                    <div class="admin-info-list">
                        <h2 class="bg-light p-2 fontBold my-3 mt-3 mb-5">送信履歴</h2>
                        <p class="u-mb01">最新の送信より20通表示</p>
                  
                        <table class="table table-bordered table-sm">
                            <thead>
                            <tr>
                                <th>送信日時</th>
                                <th>送信件名</th>
                            </tr>
                            </thead>
                            
                            <tbody>
                            <?php foreach(MailHistory::selectView() as $value):?>
                                <tr>
                                    <td><?= $value->regist_date ?></td>
                                    <td><a href="mail_delivery_detail.php?id=<?= $value->id ?>"><?= $value->subject ?></a></td>
                                </tr>
                            <?php endforeach;?>
                            </tbody>
                        </table>
                        </div>
                    </div>
                </form>
            </div>
        </div>
        <div class="col-md-3 sideContents sp-only mt-5">
                <?php include('adminsidebar.php'); ?>
            </div>
    </div>
</div>
<?php include('../footer.php'); ?>

