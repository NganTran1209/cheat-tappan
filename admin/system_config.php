<?php include_once(__DIR__ . '/../common/util.php'); ?>
<?php include_once(__DIR__ . '/../common/admin.php'); ?>
<?php include_once(__DIR__ . '/../entity/free_input_item.php'); ?>
<?php

$system_config = SystemConfig::select();

if (isset($_POST['action']) && $_POST['action'] == 'update_stripe') {
    
    if ($_POST["use_stripe"] == null) {
        $system_config->use_stripe = 0;
        SystemConfig::updateSystemConfigForUseStripe(0);
    } else {
        $system_config->use_stripe = $_POST["use_stripe"];
        SystemConfig::updateSystemConfigForUseStripe($_POST["use_stripe"]);
    }
    $system_config->stripe_access_key = $_POST["stripe_access_key"];
    $system_config->stripe_secret_key = $_POST["stripe_secret_key"];
    $system_config->financial_institution_name = $_POST["financial_institution_name"];
    $system_config->branch_name = $_POST["branch_name"];
    $system_config->deposit_type = $_POST["deposit_type"];
    $system_config->account_number = $_POST["account_number"];
    $system_config->account_holder = $_POST["account_holder"];

    $system_config->updateSystemConfigForStripeAccessKey($system_config->stripe_access_key);
    $system_config->updateSystemConfigForStripeSecretKey($system_config->stripe_secret_key);
    $system_config->updateSystemConfigForFinancialInstitutionName($system_config->financial_institution_name);
    $system_config->updateSystemConfigForBranchName($system_config->branch_name);
    $system_config->updateSystemConfigForDepositType($system_config->deposit_type);
    $system_config->updateSystemConfigForAccountNumber($system_config->account_number);
    $system_config->updateSystemConfigForAccountHolder($system_config->account_holder);

    setMessage('ストライプ情報を更新');    
}

if (isset($_POST['action']) && $_POST['action'] == 'clear_db') {
    SystemConfig::clearDb();
    setMessage('データベースを初期化しました。');
}
if (isset($_POST['registImage'])) {
    SystemConfig::regist_top_large_image();
    SystemConfig::regist_logo_image();
    setMessage('画像を更新しました。');
}

$css = new StyleCss();
if (isset($_POST['update-css'])) {
    $css->css = $_POST['override_css'];
    $css->save();
    setMessage("CSSを更新しました。");
}

$logo_image_url = SystemConfig::get_logo_image_url();
$top_large_image_url = SystemConfig::get_top_large_image_url();

$css->load();
?>

<?php include('../header.php'); ?>
<style>
    .main-header__search{
        display:none;
    }
    .main-header-link{
        justify-content: flex-end;
        margin-right: 30px;
    }
    @media screen and (max-width: 768px) {   
    .main-header-link{
        justify-content: flex-start !important;
        margin-right:0;
    }}
</style>
<div class="container">
    <div class="row">
    <div class="col-md-3 sideContents pc-only">
                <?php include('adminsidebar.php'); ?>
            </div>
            <div class="col-md-9 mainContents mb-5">
                <div class="bg-inner admin-content-title admin-page">
                    <h1>初期設定</h1>

                <ul class="nav nav-pills mb-3 mt-5" id="pills-tab" role="tablist">
                    <li class="nav-item">
                        <a class="nav-link active" id="pills-home-tab" data-toggle="pill" href="#images-setting" role="tab" aria-controls="pills-home" aria-selected="true">画像・CSS</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" id="pills-profile-tab" data-toggle="pill" href="#site-setting" role="tab" aria-controls="pills-profile" aria-selected="false">サイト</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" id="pills-contact-tab" data-toggle="pill" href="#shop-setting" role="tab" aria-controls="pills-contact" aria-selected="false">出品・手数料</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" id="pills-contact-tab" data-toggle="pill" href="#card-setting" role="tab" aria-controls="pills-contact" aria-selected="false">口座・カード</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" id="pills-contact-tab" data-toggle="pill" href="#dell-setting" role="tab" aria-controls="pills-contact" aria-selected="false">初期化</a>
                    </li>
                </ul>
                <div class="tab-content" id="pills-tabContent">
                    <div class="tab-pane fade show active admin-info-list" id="images-setting" role="tabpanel" aria-labelledby="pills-home-tab">
                    
                        <h2 class="bg-light p-2 fontBold my-3">TOP大画像の設定</h2>
                        <p>推奨サイズ1：1920px x 420px(推奨サイズ2：1140px x 420px)</p>
                        <form class="c-form01" id="form" method="post" enctype="multipart/form-data" onsubmit="return confirm('変更します。'); ">
                            <div class="input-group mb-2">
                                <div class="custom-file">
                                    <input type="file" name="topLargeImg" id="topLargeImg" class="custom-file-input"
                                           aria-describedby="topLargeImg">
                                    <label class="custom-file-label" for="topLargeImg" data-browse="参照">ファイル選択...</label>
                                </div>
                            </div>
                            <div class="admin-img">
                            <div class="mb-2">
                                <img class="img-fluid" id="topLargeImgPreview" src="<?= $top_large_image_url ?>">
                            </div>
                            <?php if (!empty($top_large_image_url)): ?>
                                <input class="form-control middle-value" type="text" name="topLargeImg"
                                       value="<?= $top_large_image_url ?>" disabled="disabled">
                            <?php endif; ?>
                            </div>
                            
                            <h2 class="bg-light p-2 fontBold my-3">TOP大画像内に検索バーの表示</h2>
                            
                            <div class="form-group">
                                <div class="custom-control custom-switch">
                                    <input type="checkbox" class="custom-control-input" id="search_bar" name="search_bar"
                                           value="1"<?= $system_config->enabled_search_bar ? ' checked="checked"' : '' ?>>
                                    <label class="custom-control-label" for="search_bar">TOP大画像上に検索バーを表示</label>
                                </div>
                            </div>

                            <div class="form-group">
                            <h2 class="bg-light p-2 fontBold my-3">ロゴ画像の設定</h2>
                            <p>推奨サイズ：150px~200px x 40px</p>
                            <div class="input-group mb-2">
                                <div class="custom-file">
                                    <input type="file" name="logoImg" id="logoImg" class="custom-file-input"
                                           aria-describedby="logoImg">
                                    <label class="custom-file-label" for="logoImg" data-browse="参照">ファイル選択...</label>
                                </div>
                            </div>
                            <div class="admin-img">
                            <div class="mb-2">
                                <img class="img-fluid" id="logoImgPreview" src="<?= $logo_image_url ?>">
                            </div>
                            <?php if (!empty($logo_image_url)): ?>
                                <input class="form-control middle-value" type="text" name="logoImg" value="<?= $logo_image_url ?>"
                                       disabled="disabled">
                            <?php endif; ?>
                            </div>
                            </div>
                            <div class="my-4 w-75 mx-auto">
                                <button class="btn btn-block btn-info" type="submit" name="registImage">画像を保存する</button>
                            </div>
                        </form>

                        <div class="form-group">
                        <h2 class="bg-light p-2 fontBold my-3">専用CSS</h2>
                        <p>
                            CSSを更新できます。
                        </p>
                        <form id="form" method="post" onsubmit="return confirm('CSSを更新します。'); ">
                            <div class="form-group">
                                <label class="fontBold" for="overrideCss">CSS</label>
                                <textarea class="form-control" name="override_css" id="overrideCss" rows="6"><?= $css->css ?></textarea>

                                <div class="my-4 w-75 mx-auto mt-5">
                                    <button class="btn btn-block btn-info" type="submit" name="update-css">CSSを更新</button>
                                </div>
                            </div>
                        </form>
                        </div>
                    </div>

                    <div class="tab-pane fade admin-info-list" id="site-setting" role="tabpanel" aria-labelledby="pills-profile-tab">


                        <h2 class="bg-light p-2 fontBold my-3">サイト設定</h2>
                        <div class="form-group">
                            <label class="fontBold" for="site_name">サイトの名前</label>
                            <input class="form-control" type="text" name="site_name" id="site_name" value="<?= $system_config->site_name ?>"
                                   aria-describedby="basic-site_name">
                        </div>
                        <div class="form-group">
                            <label class="fontBold" for="uneisha">運営者名</label>
                            <input class="form-control" type="text" name="uneisha" id="uneisha" value="<?= $system_config->uneisha ?>"
                                   aria-describedby="basic-uneisha">
                        </div>
                        <div class="form-group">
                            <label class="fontBold" for="shozaichi">所在地</label>
                            <input class="form-control" type="text" name="shozaichi" id="shozaichi" value="<?= $system_config->shozaichi ?>"
                                   aria-describedby="basic-shozaichi">
                        </div>
                        <div class="form-group">
                            <label class="fontBold" for="company_name">会社名・屋号</label>
                            <input class="form-control" type="text" name="company_name" id="company_name" value="<?= $system_config->company_name ?>"
                                   aria-describedby="basic-company_name">
                        </div>

                    </div>
                    <div class="tab-pane fade admin-info-list" id="shop-setting" role="tabpanel" aria-labelledby="pills-contact-tab">



                        <h2 class="bg-light p-2 fontBold my-3">手数料の設定</h2>
                        <div class="form-group">
                            <label class="fontBold" for="commission">手数料率</label>
                            <div class="input-group">
                                <input class="form-control" type="text" name="commission" id="commission"
                                       value="<?= $system_config->commission_rate ?>"
                                       aria-describedby="basic-commission">
                                <div class="input-group-append">
                                    <span class="input-group-text" id="basic-commission">％</span>
                                </div>
                            </div>
                        </div>

                        <h2 class="bg-light p-2 fontBold my-3">品名のタイトル設定</h2>
                        <div class="form-group">
                            <label class="fontBold" for="">商品文字列を変更</label>
                            <div class="input-group">
                                <input class="form-control" type="text" name="item_name" id="item_name"
                                       value="<?= $system_config->item_name ?>"
                                       aria-describedby="">
                            </div>
                        </div>
                        <div class="smallEX">
                            ※初期名（初期化後）の値は「商品」となります。<br>
                            ※設定されている品名は（<?= ITEM_NAME ?>）← この表記でDBの設定値を出力します。
                        </div>

                        <h2 class="bg-light p-2 fontBold my-3">自由項目設定</h2>
                        <?php foreach (FreeInputItem::selectAll() as $item): ?>
                            <div class="form-group">
                                <label class="fontBold" for="free_items<?= $item->id ?>">自由項目名<?= $item->id ?></label>
                                <div class="input-group">
                                    <input class="form-control" type="text" name="free_items<?= $item->id ?>"
                                           id="free_items<?= $item->id ?>" value="<?= $item->name ?>"
                                           aria-describedby="basic-free_items<?= $item->id ?>">
                                </div>
                            </div>
                        <?php endforeach; ?>

                        <h2 class="bg-light p-2 fontBold my-3">自由項目の表示設定</h2>
                        <?php foreach (FreeInputItem::selectAll() as $item): ?>
                            <div class="form-group">
                                <div class="custom-control custom-switch">
                                    <input type="checkbox" class="custom-control-input" id="free_item_enables<?= $item->id ?>"
                                           name="free_item_enables<?= $item->id ?>"
                                           value="<?= $item->id ?>"<?= $item->enabled ? ' checked="checked"' : '' ?>>
                                    <label class="custom-control-label" for="free_item_enables<?= $item->id ?>"><?= $item->name ?>
                                        を表示</label>
                                </div>
                            </div>
                        <?php endforeach; ?>

                        <h2 class="bg-light p-2 fontBold my-3">出品の表示設定</h2>
                        <div class="form-group">
                            <div class="custom-control custom-switch">
                                <input type="checkbox" class="custom-control-input" id="item_state" name="item_state"
                                       value="1"<?= $system_config->enabled_item_state ? ' checked="checked"' : '' ?>>
                                <label class="custom-control-label" for="item_state"><?= ITEM_NAME ?>の状態を表示</label>
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="custom-control custom-switch">
                                <input type="checkbox" class="custom-control-input" id="pref" name="pref"
                                       value="1"<?= $system_config->enabled_pref ? ' checked="checked"' : '' ?>>
                                <label class="custom-control-label" for="pref">地域を表示</label>
                            </div>
                        </div>

                        <h2 class="bg-light p-2 fontBold my-3">出品の配送系の表示設定</h2>
                        <div class="form-group">
                            <div class="custom-control custom-switch">
                                <input type="checkbox" class="custom-control-input" id="delivery_type" name="delivery_type"
                                       value="1"<?= $system_config->enabled_delivery_type ? ' checked="checked"' : '' ?>>
                                <label class="custom-control-label" for="delivery_type">配送方法を表示</label>
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="custom-control custom-switch">
                                <input type="checkbox" class="custom-control-input" id="postage" name="postage"
                                       value="1"<?= $system_config->enabled_postage ? ' checked="checked"' : '' ?>>
                                <label class="custom-control-label" for="postage">配送料負担を表示</label>
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="custom-control custom-switch">
                                <input type="checkbox" class="custom-control-input" id="carriage_type" name="carriage_type"
                                       value="1"<?= $system_config->enabled_carriage_type ? ' checked="checked"' : '' ?>>
                                <label class="custom-control-label" for="carriage_type">配送の目安を表示</label>
                            </div>
                        </div>


                    </div>
                    <div class="tab-pane fade admin-info-list" id="card-setting" role="tabpanel" aria-labelledby="pills-contact-tab">
                        <form method="post">
                            <h2 class="bg-light p-2 fontBold my-3">運営者の入金口座情報の設定</h2>
                            <div class="form-group">
                                <label class="fontBold" for="financial_institution_name">金融機関名</label>
                                <input class="form-control" type="text" name="financial_institution_name" id="financial_institution_name" value="<?= $system_config->financial_institution_name ?>"
                                    aria-describedby="basic-financial_institution_name">
                            </div>
                            <div class="form-group">
                                <label class="fontBold" for="branch_name">支店名</label>
                                <input class="form-control" type="text" name="branch_name" id="branch_name" value="<?= $system_config->branch_name ?>"
                                    aria-describedby="basic-branch_name">
                            </div>
                            <div class="form-group">
                                <label class="fontBold" for="deposit_type">預金種別</label>
                                <div class="form-check-account-add">
                                <input type="radio" name="deposit_type" id="deposit_type_1" value="1"<?= $system_config->deposit_type == 1 ? ' checked="checked"' : '' ?>/><label for="deposit_type_1" class="account-kind">普通預金</label>
                                </div>
                                <div class="form-check-account-add">
                                <input type="radio" name="deposit_type" id="deposit_type_2" value="2"<?= $system_config->deposit_type == 2 ? ' checked="checked"' : '' ?>/><label for="deposit_type_2" class="account-kind">当座預金</label>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="fontBold" for="account_number">口座番号</label>
                                <input class="form-control" type="text" name="account_number" id="account_number" value="<?= $system_config->account_number ?>"
                                    aria-describedby="basic-account_number">
                            </div>
                            <div class="form-group">
                                <label class="fontBold" for="account_holder">口座名義</label>
                                <input class="form-control" type="text" name="account_holder" id="account_holder" value="<?= $system_config->account_holder ?>"
                                    aria-describedby="basic-account_holder">
                            </div>

                            <h2 class="bg-light p-2 fontBold my-3">カード払い(Stripe)</h2>
                            <p>
                                ONの時のみ表示・利用可能
                            </p>
                            <div class="form-group">
                                <div class="custom-control custom-switch">
                                    <input type="checkbox" class="custom-control-input" id="use_stripe" name="use_stripe"
                                        value="1"<?= $system_config->use_stripe ? ' checked="checked"' : '' ?>>
                                    <label class="custom-control-label" for="use_stripe">カード払い利用を許可する</label>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="fontBold" for="stripe_access_key">アクセスキー</label>
                                <input class="form-control" type="text" name="stripe_access_key" id="stripe_access_key" value="<?= $system_config->stripe_access_key ?>"
                                    aria-describedby="basic-stripe_access_key">
                            </div>
                            <div class="form-group">
                                <label class="fontBold" for="stripe_secret_key">シークレットキー</label>
                                <input class="form-control" type="text" name="stripe_secret_key" id="stripe_secret_key" value="<?= $system_config->stripe_secret_key ?>"
                                    aria-describedby="basic-stripe_secret_key">
                            </div>
                            <div class="text-right">
                                <input type="hidden" name="action" value="update_stripe"/>
                                <button type="submit" class="btn btn-outline-success" data-toggel="modal"
                                        data-target="#stripModalCenter">
                                    情報を保存する
                                </button>
                            </div>
                        </form>
                    </div>
                    <div class="tab-pane fade admin-info-list" id="dell-setting" role="tabpanel" aria-labelledby="pills-contact-tab">

                        <h2 class="bg-light p-2 fontBold my-3">データベースの初期化</h2>
                        <div class="text-right">
                            <button type="button" class="btn btn-outline-danger" data-toggle="modal"
                                    data-target="#exampleModalCenter">
                                データベースの初期化
                            </button>
                        </div>
                        <div class="modal fade" id="exampleModalCenter" tabindex="-1" role="dialog"
                             aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
                            <div class="modal-dialog modal-dialog-centered" role="document">
                                <div class="modal-content">
                                    <form class="was-validated" method="post">
                                        <div class="modal-header">
                                            <h5 class="modal-title" id="exampleModalLongTitle">データベースの初期化</h5>
                                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                <span aria-hidden="true">&times;</span>
                                            </button>
                                        </div>
                                        <div class="modal-body">
                                            <div class="custom-control custom-checkbox mb-3">
                                                <input type="checkbox" class="custom-control-input"
                                                       id="customControlValidation1" required>
                                                <label class="custom-control-label"
                                                       for="customControlValidation1">初期化に同意します</label>
                                                <div class="invalid-feedback">削除するにはチェックしてください。</div>
                                            </div>

                                            <div class="custom-control custom-checkbox mb-3">
                                                <input type="checkbox" class="custom-control-input"
                                                       id="customControlValidation2" required>
                                                <label class="custom-control-label"
                                                       for="customControlValidation2">初期化すると出荷状態に戻ります</label>
                                                <div class="invalid-feedback">削除するにはチェックしてください。</div>
                                            </div>

                                            <div class="custom-control custom-checkbox mb-3">
                                                <input type="checkbox" class="custom-control-input"
                                                       id="customControlValidation3" required>
                                                <label class="custom-control-label" for="customControlValidation3">2度と元に戻すことは出来ないことに同意します</label>
                                                <div class="invalid-feedback">削除するにはチェックしてください。</div>
                                            </div>

                                            <div class="custom-control custom-checkbox mb-3">
                                                <input type="checkbox" class="custom-control-input"
                                                       id="customControlValidation4" required>
                                                <label class="custom-control-label"
                                                       for="customControlValidation4">本当に初期化します</label>
                                                <div class="invalid-feedback">削除するにはチェックしてください。</div>
                                            </div>
                                        </div>
                                        <div class="modal-footer">
                                            <input type="hidden" name="action" value="clear_db"/>
                                            <button type="button" class="btn btn-secondary btn-sm" data-dismiss="modal">キャンセル
                                            </button>
                                            <button type="submit" class="btn btn-outline-danger btn-sm">初期化する</button>
                                        </div>
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-md-3 sideContents sp-only mt-5">
                <?php include('adminsidebar.php'); ?>
            </div>
    </div>
</div>
<?php include('../footer.php'); ?>

