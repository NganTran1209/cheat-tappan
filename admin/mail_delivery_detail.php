<?php include_once(__DIR__ . '/../common/util.php'); ?>
<?php include_once(__DIR__ . '/../common/admin.php'); ?>
<?php
$id;
if (!empty($_GET['id'])) {
    $id = $_GET['id'];
} elseif (!empty($_POST['id'])) {
    $id = $_POST['id'];
}
if (empty($id)) {
    setMessage('不正なアクセスです。');
}

$mail_history = MailHistory::select($id);
?>
<?php include('../header.php'); ?>
<div class="container">
    <div class="row">
        <div class="col-md-3 sideContents">
            <?php include('adminsidebar.php'); ?>
        </div>
        <div class="col-md-9 mainContents">
            <div class="bg-inner">
                <h1><?= $mail_history->subject ?></h1>
                <div class="mb-4">
                    <?php $index = 0; ?>
                    <?php foreach($mail_history->targets as $target):?>
                        <?php $user = new User();?>
                        <?php $user->select($target->user_id); ?>
                        <?= ++$index?>: <?= $user->name ?>（<?= $user->email ?>）<br>
                    <?php endforeach; ?>
                </div>
                <div class="mb-4">
                    <pre><?= $mail_history->body ?></pre>
                </div>
            </div>
        </div>
    </div>
</div>
<?php include('../footer.php'); ?>

