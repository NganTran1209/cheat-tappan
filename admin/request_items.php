<?php include_once(__DIR__ . '/../common/util.php'); ?>
<?php include_once(__DIR__ . '/../common/admin.php'); ?>
<?php
$uri = $_SERVER["REQUEST_URI"];
$items = array();
$entity = new Request();
$items = $entity->selectAll();

?>

<?php include('../header.php'); ?>
<style>
    .main-header__search{
        display:none;
    }
    .main-header-link{
        justify-content: flex-end;
        margin-right: 30px;
    }
    @media screen and (max-width: 768px) {   
    .main-header-link{
        justify-content: flex-start !important;
        margin-right:0;
    }}
</style>
    <div class="container">
        <div class="row">
        <div class="col-md-3 sideContents pc-only">
                <?php include('adminsidebar.php'); ?>
            </div>
            <div class="col-md-9 mainContents mb-5">
                <div class="bg-inner admin-content-title admin-page">
                    <h1>購入希望待ち確認</h1>
                    <table class="table table-bordered small mt-5">
                        <tr>
                            <th>日付</th>
                            <th>タイトル</th>
                            <th>予算</th>
                        </tr>
                        <?php foreach ($items as $item) : ?>
                            <?php if ($item->enabled != 0) {
                                continue;
                            } ?>
                            <?php
                            $user = new User();
                            $user->select($item->user_id);
                            ?>

                            <tr>
                                <td class="text-right"><?= $item->regist_date ?></td>
                                <td class="text-left"><a
                                            href="request_detail.php?id=<?= $item->id ?>"><?= $item->title ?></a></td>
                                <td class="text-right"><?= number_format($item->price) ?>円</td>
                            </tr>

                            <!--<?= $user->hash_id ?>-->

                        <?php endforeach; ?>
                    </table>
                </div>
            </div>
            <div class="col-md-3 sideContents sp-only mt-5">
                <?php include('adminsidebar.php'); ?>
            </div>
        </div>
    </div>

<?php include('../footer.php'); ?>