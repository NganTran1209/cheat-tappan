<?php include_once(__DIR__ . '/../common/util.php'); ?>
<?php include_once(__DIR__ . '/../common/admin.php'); ?>
<?php
$item = new Item();
$item->select($_GET['item_id']);
$owner = new User();
$owner->select($item->owner_id);
$negotiation = new Negotiation();
$negotiation->item_id = $_GET['item_id'];
$sells = $negotiation->selectSell();

?>
<?php include('../header.php'); ?>
<div class="container">
    <div class="row">
        <div class="col-md-3 sideContents">
            <?php include('adminsidebar.php'); ?>
        </div>
        <div class="col-md-9 mainContents">
            <div class="bg-inner">
                <h1>問い合わせ一覧</h1>
                <div class="card mb-3">
                    <div class="card-body">
                        <?= $item->title ?>
                    </div>
                </div>
                <div class="row">
                    <?php include_once(__DIR__ . '/../common/parts/item_image_area.php'); ?>
                    <?php include_once(__DIR__ . '/../common/parts/item_info_area.php'); ?>
                </div>
                <div class="text-center mb-4">
                    <span class="font30 fontBold fontRed"><?= number_format($item->price) ?>円</span>送料込(出品者負担)
                </div>
                <div class="itemContentText">
                    <?php echo nl2br(htmlentities($item->remarks)); ?>
                </div>
                <table class="table table-bordered table-striped">
                    <tbody>
                    <tr>
                        <th>問い合わせユーザー</th>
                        <!--    <th>ステータス</th>-->
                        <th>交渉開始日</th>
                        <th>最終送信日</th>
                        <th>交渉回数</th>
                    </tr>
                    <?php foreach ($sells as $sell): ?>
                        <tr>
                            <td class="col-sm-6"><a
                                        href="negotiation.php?item_id=<?= $sell->item_id ?>&user_id=<?= $sell->user_id ?>"><?= $sell->user_name ?></a>
                            </td>
                            <!--    <td>交渉中</td>-->
                            <td><?= date('Y/n/j', strtotime($sell->start_date)) ?></td>
                            <td><?= date('Y/n/j', strtotime($sell->last_date)) ?></td>
                            <td><?= number_format($sell->negotiation_count) ?>回</td>
                        </tr>
                    <?php endforeach; ?>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>
<?php include('../footer.php'); ?>




