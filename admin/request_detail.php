<?php include_once(__DIR__ . '/../common/util.php'); ?>
<?php include_once(__DIR__ . '/../common/admin.php'); ?>
<?php
if (isset($_POST['action'])) {
    if ($_POST['action'] == 'approval') {
        $item = new Request();
        $item->select($_POST['id']);
        $item->approval();

        setMessage('購入希望を許可しました。');
    } elseif ($_POST['action'] == 'rejection') {
        $item = new Request();
        $item->id = $_POST['id'];
        $item->regection();

        setMessage('購入希望を却下しました。');
    }
    header('Location: '.getContextRoot().'/admin/request_items.php');
    exit();
}

$item = new Request();
$item->select($_GET['id']);
$owner = new User();
$owner->select($item->user_id);

$evaluate = new Evaluate();
$evaluate->selectSummary($item->user_id);
?>
<?php include('../header.php'); ?>
        <div class="container">
            <div class="row">
                <div class="col-md-3 sideContents pc-only">
                    <?php include('adminsidebar.php'); ?>
                </div>
                <div class="col-md-9 mainContents">
                    <div class="bg-inner u-sp-mb50">
                        <h1>購入希望待ちの確認の詳細</h1>
                        <div class="card mb-3">
                            <div class="card-body">
                                <div class="card-text">
                                    <strong><?= $item->title ?></strong>
                                </div>
                                <div class="card-text border-bottom pb-2">
                                    <?php echo nl2br(htmlentities($item->text)); ?>
                                </div>
                                <div class="card-text pt-2">
                                    <strong>購入希望者</strong>：<?= $owner->name ?>
                                    <a href="<?php echo HOME_URL; ?>/evaluate.php?user_id=<?= $owner->id ?>">評価</a>：<?= $evaluate->point ?>
                                    (
                                    <i class="fas fa-smile mr-1"></i><?= $evaluate->good_count ?>&nbsp;
                                    <i class="fas fa-meh mr-1"></i><?= $evaluate->normal_count ?>&nbsp;
                                    <i class="fas fa-frown mr-1"></i><?= $evaluate->bad_count ?>
                                    )
                                </div>
                            </div>

                        </div>
                        <div class="margin-20">
                            <div class="text-center">
                                <span class="font30 fontBold fontRed"><?= number_format($item->price) ?></span>
                                <span class="smallEX">円</span>
                            </div>
                            <div class="row">
                                <div class="col-md-6">
                                    <form method="post" onsubmit="return confirm('購入希望を許可します');">
                                        <input type="hidden" name="id" value="<?= $item->id ?>"/>
                                        <input type="hidden" name="action" value="approval"/>
                                        <input class="btn btn-block btn-info" type="submit"
                                               value="掲載を許可する"/>
                                    </form>
                                </div>
                                <div class="col-md-6">
                                    <form method="post" onsubmit="return confirm('購入希望を却下します');">
                                        <input type="hidden" name="id" value="<?= $item->id ?>"/>
                                        <input type="hidden" name="action" value="rejection"/>
                                        <input class="btn btn-block btn-outline-danger" type="submit"
                                               value="掲載を却下する"/>
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-md-3 sideContents sp-only mt-5">
                    <?php include('adminsidebar.php'); ?>
                </div>
            </div>
        </div>

<?php include('../footer.php'); ?>