<?php include_once(__DIR__ . '/../common/util.php'); ?>
<?php include_once(__DIR__ . '/../common/admin.php'); ?>
<?php include_once(__DIR__ . '/../common/mail_template.php'); ?>
<?php

$system_config = SystemConfig::select();

?>
<?php include('../header.php'); ?>
<style>
    .main-header__search{
        display:none;
    }
    .main-header-link{
        justify-content: flex-end;
        margin-right: 30px;
    }
    @media screen and (max-width: 768px) {   
    .main-header-link{
        justify-content: flex-start !important;
        margin-right:0;
    }}
</style>
<div class="container">
    <div class="row">
    <div class="col-md-3 sideContents pc-only">
                <?php include('adminsidebar.php'); ?>
            </div>
        <div class="col-md-9 mainContents mb-5">
            <div class="bg-inner admin-content-title admin-page">
                <h1>自動送信メール編集</h1>
                <div class="mt-5 font-18rem">
                <ul class="nav nav-pills mb-3" id="pills-tab" role="tablist">
                    <li class="nav-item">
                        <a class="nav-link active" id="mail-setting-tab" data-toggle="pill" href="#mail-setting" role="tab"
                           aria-controls="mail-setting" aria-selected="true">メール設定</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" id="mail-admin-tab" data-toggle="pill" href="#mail-admin" role="tab"
                           aria-controls="mail-admin" aria-selected="false">管理者用</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" id="mail-user-tab" data-toggle="pill" href="#mail-user" role="tab"
                           aria-controls="mail-user" aria-selected="false">ユーザー用（共通）</a>
                    </li>

                    <li class="nav-item">
                        <a class="nav-link" id="mail-buyer-tab" data-toggle="pill" href="#mail-buyer" role="tab"
                           aria-controls="mail-buyer" aria-selected="false">ユーザー用（購入者）</a>
                    </li>

                    <li class="nav-item">
                        <a class="nav-link" id="mail-seller-tab" data-toggle="pill" href="#mail-seller" role="tab"
                           aria-controls="mail-seller" aria-selected="false">ユーザー用（出品者）</a>
                    </li>
                </ul>
                </div>
                <div class="tab-content" id="pills-tabContent">
                    <div class="tab-pane fade show active admin-info-list" id="mail-setting" role="tabpanel"
                         aria-labelledby="mail-setting-tab">
                        <h2 class="bg-light p-2 fontBold my-3">送受信用メール設定</h2>
                        <div class="form-group">
                            <label class="fontBold" for="FROM_EMAIL_NAME">送信者名　FROM_EMAIL_NAME</label>
                            <input class="form-control" type="text" name="from_email_name" id="from_email_name" value="<?= $system_config->from_email_name ?>" placeholder="例）送信者名"
                                   value="">
                        </div>
                        <div class="form-group">
                            <label class="fontBold" for="FROM_EMAIL">送信者メールアドレス　FROM_EMAIL</label>
                            <input class="form-control" type="text" name="from_email" id="from_email" value="<?= $system_config->from_email ?>" placeholder="例）送信者メールアドレス"
                                   value="">
                        </div>
                        <div class="form-group">
                            <label class="fontBold" for="TO_EMAIL">返信先　TO_EMAIL</label>
                            <input class="form-control" type="text" name="to_email" id="to_email" value="<?= $system_config->to_email ?>" placeholder="例）返信先メールアドレス"
                                   value="">
                        </div>
                    </div>
                    <!-- mail-setting ここまで-->


                    <!--ここからUl>liタブで指定した内容が表示される-->
                    <div class="tab-pane fade admin-info-list" id="mail-admin" role="tabpanel" aria-labelledby="mail-admin-tab">
                        <h2 class="bg-light p-2 fontBold my-3">管理者用メール</h2>
                        <?php foreach (MailTemplate::getMap() as $key => $value): ?>
                            <?php if ($value->target != 'admin') {continue;} ?>
                            <div class="pb-4 mb-4 form-group">
                                <h3 class="fontBold my-2"><?= $value->name ?>（<?= $key ?>）</h3>
                                <div class="input-group input-group-sm mb-3">
                                    <div class="input-group-prepend">
                                        <span class="input-group-text">件名</span>
                                    </div>
                                    <input type="text" class="form-control" name="<?= $key ?>-subject"
                                           value="<?= $value->subject ?>">
                                </div>

                                <div class="input-group input-group-sm">
                                    <div class="input-group-prepend">
                                        <span class="input-group-text">本文</span>
                                    </div>
                                    <textarea class="form-control" name="<?= $key ?>-body"
                                              rows="6"><?= $value->body ?></textarea>
                                </div>
                            </div>
                        <?php endforeach; ?>
                    </div>
                    <div class="tab-pane fade admin-info-list" id="mail-user" role="tabpanel" aria-labelledby="mail-user-tab">
                        <h2 class="bg-light p-2 fontBold my-3">ユーザー用（共通）</h2>
                        <?php foreach (MailTemplate::getMap() as $key => $value): ?>
                            <?php if ($value->target != 'user') {continue;} ?>
                            <div class="pb-4 mb-4 form-group">
                                <h3 class="fontBold my-2"><?= $value->name ?>（<?= $key ?>）</h3>
                                <div class="input-group input-group-sm mb-3">
                                    <div class="input-group-prepend">
                                        <span class="input-group-text">件名</span>
                                    </div>
                                    <input type="text" class="form-control" name="<?= $key ?>-subject"
                                           value="<?= $value->subject ?>">
                                </div>

                                <div class="input-group input-group-sm">
                                    <div class="input-group-prepend">
                                        <span class="input-group-text">本文</span>
                                    </div>
                                    <textarea class="form-control" name="<?= $key ?>-body"
                                              rows="6"><?= $value->body ?></textarea>
                                </div>
                            </div>
                        <?php endforeach; ?>

                    </div>
                    <div class="tab-pane fade admin-info-list" id="mail-buyer" role="tabpanel" aria-labelledby="mail-buyer-tab">
                        <h2 class="bg-light p-2 fontBold my-3">ユーザー用（購入者）</h2>
                        <?php foreach (MailTemplate::getMap() as $key => $value): ?>
                            <?php if ($value->target != 'buyer') {continue;} ?>
                            <div class="pb-4 mb-4 form-group">
                                <h3 class="fontBold my-2"><?= $value->name ?>（<?= $key ?>）</h3>
                                <div class="input-group input-group-sm mb-3">
                                    <div class="input-group-prepend">
                                        <span class="input-group-text">件名</span>
                                    </div>
                                    <input type="text" class="form-control" name="<?= $key ?>-subject"
                                           value="<?= $value->subject ?>">
                                </div>

                                <div class="input-group input-group-sm">
                                    <div class="input-group-prepend">
                                        <span class="input-group-text">本文</span>
                                    </div>
                                    <textarea class="form-control" name="<?= $key ?>-body"
                                              rows="6"><?= $value->body ?></textarea>
                                </div>
                            </div>
                        <?php endforeach; ?>

                    </div>
                    <div class="tab-pane fade admin-info-list" id="mail-seller" role="tabpanel" aria-labelledby="mail-seller-tab">
                        <h2 class="bg-light p-2 fontBold my-3">ユーザー用（出品者）</h2>
                        <?php foreach (MailTemplate::getMap() as $key => $value): ?>
                            <?php if ($value->target != 'seller') {continue;} ?>
                            <div class="pb-4 mb-4 form-group">
                                <h3 class="fontBold my-2"><?= $value->name ?>（<?= $key ?>）</h3>
                                <div class="input-group input-group-sm mb-3">
                                    <div class="input-group-prepend">
                                        <span class="input-group-text">件名</span>
                                    </div>
                                    <input type="text" class="form-control" name="<?= $key ?>-subject"
                                           value="<?= $value->subject ?>">
                                </div>

                                <div class="input-group input-group-sm">
                                    <div class="input-group-prepend">
                                        <span class="input-group-text">本文</span>
                                    </div>
                                    <textarea class="form-control" name="<?= $key ?>-body"
                                              rows="6"><?= $value->body ?></textarea>
                                </div>
                            </div>
                        <?php endforeach; ?>

                    </div>
                </div>
            </div>
        </div>
        <div class="col-md-3 sideContents sp-only mt-5">
                <?php include('adminsidebar.php'); ?>
            </div>
    </div>
</div>
<?php include('../footer.php'); ?>
<script>
$(function() {
  $('input').change(function() {

    data = {
        target: $(this).attr('name'),
        value: $(this).val()
    };
    if ($(this).attr('type') == 'checkbox') {
    	data.value = $(this).is(':checked');
    }

    $.ajax({
        'dataType': 'json',
        'type': 'PUT',
        'url': '<?= getContextRoot() ?>/api/admin/mail_config.php',
        'data': JSON.stringify(data)
    });

  });

  $('textarea').change(function() {

	    data = {
	        target: $(this).attr('name'),
	        value: $(this).val()
	    };

	    $.ajax({
	        'dataType': 'json',
	        'type': 'PUT',
	        'url': '<?= getContextRoot() ?>/api/admin/mail_config.php',
	        'data': JSON.stringify(data)
	    });

	  });
});
</script>

