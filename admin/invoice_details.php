<?php include_once(__DIR__ . '/../common/util.php'); ?>
<?php include_once(__DIR__ . '/../common/admin.php'); ?>
<?php

if (isset($_POST['action'])) {
    $invoice = new Invoice();
    if ($_POST['action'] == 'send') {
        $invoice->select($_POST['id']);
        $invoice->sendMail();
        setMessage('請求メールを再送しました。');
    } elseif ($_POST['action'] == 'cancel') {
        $invoice->id = $_POST['id'];
        $invoice->status = 1;
        $invoice->updateStatus();
        setMessage('入金登録を取り消しました。');
    }
}
$id = 0;
if (isset($_GET['id'])) {
    $id = $_GET['id'];
} else {
    $id = $_POST['id'];
}
$month = '';
if (isset($_GET['month'])) {
    $month = $_GET['month'];
} else {
    $month = $_POST['month'];
}

$invoiceDetails = new InvoiceDetails();
$items = $invoiceDetails->select($id, $month);
$invoice = new Invoice();
$invoice->select($id);

?>
<?php include('../header.php'); ?>
    <div class="container">
        <div class="row">
            <div class="col-md-3 sideContents">
                <?php include('adminsidebar.php'); ?>
            </div>
            <div class="col-md-9 mainContents">
                <div class="bg-inner">
                    <h1>取引情報詳細</h1>
                    <?php if ($invoice->status == 1): ?>
                        <form method="post" style="display: inline" onsubmit="return window.confirm('請求メールを送再送します。')">
                            <input type="hidden" name="id" value="<?= $id ?>"/>
                            <input type="hidden" name="month" value="<?= $month ?>"/>
                            <button type="submit" name="action" value="send">メールを再送する。</button>
                        </form>
                    <?php endif; ?>
                    <?php if ($invoice->status == 2): ?>
                        <form method="post" style="display: inline" onsubmit="return window.confirm('入金済みを取り消します。')">
                            <input type="hidden" name="id" value="<?= $id ?>"/>
                            <input type="hidden" name="month" value="<?= $month ?>"/>
                            <button type="submit" name="action" value="cancel">入金済みを取り消す。</button>
                        </form>
                    <?php endif; ?>
                    <div class="table-responsive">
                        <table class="table table-bordered">
                            <tr>
                                <th>タイトル</th>
                                <th>購入者</th>
                                <th>価格</th>
                                <th>手数料</th>
                                <th>取引日時</th>
                            </tr>
                            <?php foreach ($items as $item): ?>
                                <tr>
                                    <td class="text-left"><?= $item->title ?></td>
                                    <td class="text-left"><?= $item->buy_user_name ?></td>
                                    <td class="text-right"><?= number_format($item->price) ?>円</td>
                                    <td class="text-right"><?= number_format($item->commission) ?>円</td>
                                    <td class="text-right"><?= $item->transaction_date ?></td>
                                </tr>
                            <?php endforeach; ?>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
<?php include('../footer.php'); ?>