<?php include_once(__DIR__ . '/../common/util.php'); ?>
<?php include_once(__DIR__ . '/../common/admin.php'); ?>

<?php
$invoice = new Invoice();
$monthList = array();
for ($i = 0; $i <= 12; $i++) {
    $monthList[] = strtotime(date('Y-m-1') . '-' . $i . ' month');
}
$month = '';
if (isset($_POST['month'])) {
    $month = $_POST['month'];
} else {
    $month = date('Ym', $monthList[0]);
}

$items = array();
if (isset($_POST['action'])) {
    if ($_POST['action'] == 'summary') {
        $invoice->create($_POST['month']);
    } elseif ($_POST['action'] == 'send') {
        $invoice->sendMailFromMonth($_POST['month']);
        setMessage('請求メールを一括送信しました。');
    } elseif ($_POST['action'] == 'pay') {
        $invoice->id = $_POST['id'];
        $invoice->status = 2;
        $invoice->updateStatus();
        setMessage('入金登録いたしました。');
    } elseif ($_POST['action'] == 'delete') {
        Invoice::delete($_POST['month']);
        setMessage('請求情報を削除いたしました。');
    }
}
$items = $invoice->selectMonth($month);
$isSentMail = false;
foreach ($items as $value) {
    if ($value->status > 0) {
        $isSentMail = true;
        break;
    }
}


?>

<?php include('../header.php'); ?>
    <div class="container">
        <div class="row">
            <div class="col-md-3 sideContents">
                <?php include('adminsidebar.php'); ?>
            </div>
            <div class="col-md-9 mainContents">
                <div class="bg-inner">
                    <h1>手数料の請求</h1>
                    <div class="form-row mb-4">
                        <div class="col-md-4">
                            <form id="frm" method="post">
                                <select class="form-control form-control-sm" name="month" onchange="frm.submit();">
                                    <?php foreach ($monthList as $date): ?>
                                        <?php if ($date < strtotime('2018/10/1')) continue; ?>
                                        <option value="<?= date('Ym', $date) ?>"<?= date('Ym', $date) == $month ? ' selected' : '' ?>><?= date('Y年m月', $date) ?></option>
                                    <?php endforeach; ?>
                                </select>
                            </form>
                        </div>
                        <?php if ($month < date('Ym', $monthList[0])): ?>
                            <div class="col-md-8">
                                <?php if (count($items) == 0): ?>
                                    <form method="post" onsubmit="return window.confirm('請求情報を集計する。')">
                                        <input type="hidden" name="month" value="<?= $month ?>"/>
                                        <button class="btn btn-block btn-info btn-sm" type="submit" name="action"
                                                value="summary">請求情報を集計する
                                        </button>
                                    </form>
                                <?php elseif (!$isSentMail): ?>
                                    <form method="post" onsubmit="return window.confirm('一括で請求メールを送信する。')">
                                        <input type="hidden" name="month" value="<?= $month ?>"/>
                                        <button class="btn btn-block btn-info btn-sm" type="submit" name="action"
                                                value="send">一括請求メールを送信する
                                        </button>
                                    </form>
                                    <form method="post" onsubmit="return window.confirm('請求情報を削除する。')">
                                        <input type="hidden" name="month" value="<?= $month ?>"/>
                                        <button class="btn btn-block btn-info btn-sm" type="submit" name="action"
                                                value="delete">請求情報削除
                                        </button>
                                    </form>
                                <?php endif; ?>
                            </div>
                        <?php else: ?>
                            <!-- 【試験用】 ▼▼▼ ここより下は、当月締めのテスト用に設置【試験用】 -->
                            <div class="col-md-8">
                                <?php if (count($items) == 0): ?>
                                    <form method="post" onsubmit="return window.confirm('請求情報を集計する。')">
                                        <input type="hidden" name="month" value="<?= $month ?>"/>
                                        <button class="btn btn-block btn-info btn-sm" type="submit" name="action"
                                                value="summary">請求情報を集計する（仮確認用）
                                        </button>
                                    </form>
                                <?php elseif (!$isSentMail): ?>
                                    <form method="post" style="display: inline"
                                          onsubmit="return window.confirm('請求情報を削除する。')">
                                        <input type="hidden" name="month" value="<?= $month ?>"/>
                                        <button class="btn btn-block btn-info btn-sm" type="submit" name="action"
                                                value="delete">（仮）請求情報削除
                                        </button>
                                    </form>
                                <?php endif; ?>
                            </div>
                            <div class="col-12">
                                <div class="small">
                                    ※仮確認用ボタンを押して確認したら<span class="fontRed">必ず（仮）請求情報削除ボタンを押してクリアしてください。</span><br>
                                    ※仮確認用ボタンを押して集計中の状態のまま月を越えますと<span class="fontRed">不具合になります</span>ので、月末に仮確認用ボタンを押さない様にしてください。
                                </div>
                            </div>
                            <!-- 【試験用】▲▲▲ ここより上は、当月締めのテスト用に設置【試験用】 -->
                        <?php endif; ?>
                    </div>
                    <div class="table-responsive">
                        <table class="table table-bordered table-striped small table-sm">
                            <tr class="text-center">
                                <th>メールアドレス</th>
                                <th>名前</th>
                                <th>属性</th>
                                <!-- <th>利用料</th>-->
                                <th>取引件数</th>
                                <th>取引金額</th>
                                <th>手数料率</th>
                                <th>手数料</th>
                                <th>請求金額</th>
                                <th>状態</th>
                                <th>入金</th>
                            </tr>
                            <?php foreach ($items as $item): ?>
                                <tr>
                                    <td class="text-left"><?= $item->email ?></td>
                                    <td class="text-right"><?= $item->user_name ?></td>
                                    <td class="text-right"><?= $item->type_name ?></td>
                                    <!-- <td class="text-right">//= number_format($item->price) 円</td>-->
                                    <td class="text-right"><a
                                                href="invoice_details.php?id=<?= $item->id ?>&month=<?= $month ?>"><?= number_format($item->transaction_count) ?>
                                            件</a></td>
                                    <td class="text-right"><a
                                                href="invoice_details.php?id=<?= $item->id ?>&month=<?= $month ?>"><?= number_format($item->transaction_amount) ?>
                                            円</a></td>
                                    <td class="text-right"><?= $item->commission_rate ?>%</td>
                                    <td class="text-right"><?= number_format($item->commission) ?>円</td>
                                    <td class="text-right"><?= number_format($item->amount) ?>円</td>
                                    <td class="text-right"><?= Invoice::toStatusText($item->status) ?></td>
                                    <td class="text-right">
                                        <?php if ($item->status == 1): ?>
                                            <form method="post" style="display: inline"
                                                  onsubmit="return window.confirm('<?= $item->user_name ?>さまを入金済み登録します。')">
                                                <input type="hidden" name="month" value="<?= $month ?>"/>
                                                <input type="hidden" name="id" value="<?= $item->id ?>"/>
                                                <button name="action" type="submit" value="pay">入金</button>
                                            </form>
                                        <?php endif; ?>
                                    </td>
                                </tr>
                            <?php endforeach; ?>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
<?php include('../footer.php'); ?>