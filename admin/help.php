<?php include_once(__DIR__ . '/../common/util.php'); ?>
<?php include_once(__DIR__ . '/../common/admin.php'); ?>
<?php include('../header.php'); ?>
<div class="container">
    <div class="row">
    <div class="col-md-3 sideContents pc-only">
                <?php include('adminsidebar.php'); ?>
            </div>
        <div class="col-md-9 mainContents">
            <div class="bg-inner admin-content-title admin-page">
                <h1>このシステムの使い方(簡易版)</h1>
                <div class="help-content">
                <h2 class="bg-light p-2 fontBold my-3">簡潔の１分説明</h2>
                <ul>
                    <li>初期設定完了後に運営できます。
                        <ol>
                            <li>メールの送信者名、メールアドレスを設定<a href="<?php echo HOME_URL; ?>/admin/mail_auto_delivery.php">自動送信メール編集</a>で行います。</li>
                            <li><a href="<?php echo HOME_URL; ?>/admin/mail_auto_delivery.php">自動送信メール編集</a>の「ユーザー用(共通)」で請求書メールに記載される口座番号を設定します。</li>
                            <li><a href="<?php echo HOME_URL; ?>/admin/system_config.php">初期設定</a>にて手数料や表示項目を決定します。</li>
                            <li><a href="<?php echo HOME_URL; ?>/admin/category.php">カテゴリー追加</a>でカテゴリー名を決めます。</li>
                            <li>これで最低限の動作が行えます。</li>
                        </ol>
                    </li>
                    <li>出品、購入希望、証明書登録、手数料請求は全て管理者の確認と承認が必要な作業です。</li>
                    <li>運営後に管理者の行う作業は上記のみで最低限運営可能です。</li>
                </ul>
            </div>
                <div class="help-content">
                <h2 class="bg-light p-2 fontBold my-3">初期設定の手順</h2>
                <ul>
                    <li>インストールはフォルダ一式と初期用のデータベースをインストールします。<br>同一の１ドメインしか動作しませんのでご注意下さい。</li>
                    <li>管理画面にログイン後＞自動送信メール編集＞メール設定からこのサイトが発信するメールアドレスを設定します。</li>
                    <li>管理画面にログイン後＞自動送信メール編集＞ユーザー用（共通）にある<strong>「請求書送付と口座番号」に口座番号を記入して保存します。</strong></li>
                    <li>管理画面にログイン後＞カテゴリーからカテゴリー名を決めて保存します。表示させる場合は有効にチェックを入れます。表示させたくない場合は有効のチェックを外します。</li>
                    <li>管理画面にログイン後＞配送方法追加からゆうパックやクロネコと言った配送方法に関する設定を行います。</li>
                    <li>管理画面にログイン後＞配送目安の追加から商品到着までの日時を選択できるようにします。</li>
                    <li>管理画面にログイン後＞送料負担の追加から出品者が負担する、購入者が負担するなどの設定を行います。</li>
                    <li>管理画面にログイン後＞設定から表示するものしないものや手数料の設定を行います。</li>
                    <li>管理画面にログイン後＞設定＞自由項目設定は名の通り自由項目名を決められます。3つ設定可能です。自由項目名を変えるには「設定」から変更可能です。</li>
                    <li>管理画面にログイン後＞設定＞品名のタイトル設定から名目が「商品」となっていますが、「アイテム」としても、商品に変わる自由な名目で変更が可能です。</li>
                    <li>管理画面にログイン後＞ユーザー一覧・許可待ち確認からユーザーの承認状態、ユーザーの出品状態、取引中の内容を確認することができます。</li>
                </ul></div>
                <div class="help-content">
                <h2 class="bg-light p-2 fontBold my-3">出品するには</h2>
                <p class="fontBold fontRed">
                    管理画面から「出品待ち」の許可が必要になります。
                </p>
                <p>
                    管理画面にログイン後＞ユーザー一覧・許可待ちから本人確認書類を提出いただき承認してから「出品・購入可否」のボタンを押して頂き「許可」となれば出品可能です。
                </p>
                <p>
                    ユーザーが出品すると管理画面の「出品待ち確認」に表示されます。許可する場合は、「承認」ボタンを押します。
                </p>
            </div>
                <div class="help-content">
                <h2 class="bg-light p-2 fontBold my-3">購入希望を募集するには</h2>
                <p class="fontBold fontRed">
                    管理画面から「購入希望待ち」の許可が必要になります。
                </p>
                <p>
                    ユーザーが欲しい情報をユーザー画面から「購入の希望を募集」から自分の欲しい商品やアイテム・サービスの購入希望の掲載が出来ます。
                </p>
                <p>
                    これも管理画面の「購入希望待ち確認」から確認できます。許可する場合は、「承認」ボタンを押します。
                </p>
            </div>
                <div class="help-content">
                <h2 class="bg-light p-2 fontBold my-3">手数料を請求するには</h2>
                <ol>
                    <li>管理画面にログイン後、「手数料請求」から請求が行えます。</li>
                    <li>月初（月初めや1日）になったら「請求情報を集計する」ボタンを押します。翌月以降であればいつ締めて頂いても問題御座いません。<strong class="fontRed">現状は月末締めの月初めの請求となります。</strong>
                    </li>
                    <li>販売された内容一覧が表示されますので、「請求する」ボタンを押すことで、請求者へ一括メールが配信されます。</li>
                    <li>入金された場合は、「手数料請求」画面にある「入金」ボタンを押して入金確定にしてください。</li>
                </ol></div>

<div class="help-content">
                <h2 class="bg-light p-2 fontBold my-3">お知らせを投稿したい場合は</h2>
                <p>
                    管理画面から「お知らせ投稿」からお知らせの投稿が可能です。
                </p>
                <p>
                    TOPページに表示されるようになっております。
                </p>
                </div>
            </div>
        </div>
        <div class="col-md-3 sideContents sp-only mt-5">
                <?php include('adminsidebar.php'); ?>
            </div>
    </div>
</div>
<?php include('../footer.php'); ?>

