<?php include_once(__DIR__ . '/../common/util.php'); ?>
<?php include_once(__DIR__ . '/../common/admin.php'); ?>
<?php
$negotiation = new Negotiation();
if (isset($_POST['comment'])) {
    $negotiation->comment = $_POST['comment'];
    $negotiation->kbn = $_POST['kbn'];
    $negotiation->item_id = $_POST['item_id'];
    $negotiation->user_id = $_POST['user_id'];

    $negotiation->regist();
    setMessage('コメントを送信しました。');
}

$item = new Item();
$item->select($_GET['item_id']);
$negotiation->item_id = $item->id;
$negotiation->user_id = $_GET['user_id'];
$comments = $negotiation->select();
?>
<?php include('../header.php'); ?>
<div class="container">
    <div class="row">
        <div class="col-md-3 sideContents">
            <?php include('adminsidebar.php'); ?>
        </div>
        <div class="col-md-9 mainContents">
            <div class="bg-inner">
                <h1>出品中の交渉内容を確認</h1>
                <h2><?= $item->title ?></h2>
                <div class="row">
                    <?php include_once(__DIR__ . '/../common/parts/item_image_area.php'); ?>
                    <?php include_once(__DIR__ . '/../common/parts/item_info_area.php'); ?>
                </div>
                <div class="mb-4">
                    <div class="text-center">
                        <span class="font30 fontBold fontRed"><?= number_format($item->price) ?></span>
                        <span class="smallEX">円(税込<?= $item->carriage_plan_name ?>)</span>
                    </div>
                </div>

                <div class="itemContentText">
                    <?php echo nl2br(htmlentities($item->remarks)); ?>
                </div>

                <h2 class="bg-light p-2 fontBold">交渉内容</h2>
                <?php foreach ($comments as $comment): ?>
                    <div class="row mb-4">
                        <div class="col-md-3">
                            <div class="balloonUser">
                                <?php if ($comment->kbn == 1): ?>
                                    <?= $comment->user_name ?>
                                    <!--                            <div class="commenter">-->
                                    <!--                                <a href="--><?php //echo HOME_URL; ?><!--/evaluate.php?user_id=--><?php //echo $comment->user_id; ?><!--">評価</a>-->
                                    <!--                                --><?php //// echo $user_evaluate->point; ?>
                                    <!--                            </div>-->
                                    <div class="commenter">
                                        <span>質問者</span>
                                    </div>
                                <?php else: ?>
                                    <?= $owner->name ?>
                                    <div class="owner">
                                        <span>出品者</span>
                                    </div>
                                <?php endif; ?>
                            </div>
                        </div>
                        <div class="col-md-9">
                            <div class="bg-comment balloon <?php if ($comment->kbn == 1): ?>balloonA<?php else: ?>balloonB<?php endif; ?>">
                                <div class="balloonComment">
                                    <?php echo nl2br(htmlentities($comment->comment)); ?>
                                </div>
                                <div class="">
                                    <?php if ($comment->hasPhoto()): ?>
                                        <a href="<?= $comment->getImageUrl() ?>" target="_blank"><img
                                                    class="img-fluid" width="300"
                                                    src="<?= $comment->getImageUrl() ?>"/></a>
                                    <?php endif; ?>
                                </div>
                                <div class="balloonTime">
                                    <i class="far fa-clock mr-2"></i><?= $comment->regist_date ?>
                                </div>
                            </div>
                        </div>
                    </div>
                <?php endforeach; ?>
            </div>
        </div>
    </div>
</div>

<?php include('../footer.php'); ?>




