<?php include_once(__DIR__ . '/../common/util.php'); ?>
<?php include_once(__DIR__ . '/../common/admin.php'); ?>
<?php include_once(__DIR__ . '/../entity/free_input_item.php'); ?>
<?php include_once(__DIR__ . '/../entity/free_input_item_value.php'); ?>
<?php
if (isset($_GET['id'])) {
    $free_input_item_id = $_GET['id'];
} elseif (isset($_POST['id'])) {
    $free_input_item_id = $_POST['id'];
} else {
    setMessage('不正なアクセスです。');
    header('Location: '.getContextRoot().'/admin/');
    exit;
}

if (isset($_POST['action'])) {
    if ($_POST['action'] == 'modify') {
        FreeInputItemValue::update();
    } elseif ($_POST['action'] == 'add') {
        FreeInputItemValue::regist();
    }
    setMessage('登録しました。');
}

$target = FreeInputItem::select($free_input_item_id);
?>
<?php include('../header.php'); ?>
<style>
    .main-header__search{
        display:none;
    }
    .main-header-link{
        justify-content: flex-end;
        margin-right: 30px;
    }
    @media screen and (max-width: 768px) {   
    .main-header-link{
        justify-content: flex-start !important;
        margin-right:0;
    }}
</style>
<div class="container">
    <div class="row">
    <div class="col-md-3 sideContents pc-only">
                <?php include('adminsidebar.php'); ?>
            </div>
        <div class="col-md-9 mainContents">
            <div class="bg-inner admin-content-title admin-page">
                <h1><?= $target->name ?></h1>
                <div class="mb-4 mt-5">
                    <form class="form-row" method="post" onsubmit="return confirm('追加する。');">
                        <input type="hidden" name="action" value="add"/>
                        <input type="hidden" name="free_input_item_id" value="<?= $free_input_item_id ?>"/>
                        <div class="col-md-8">
                            <input class="form-control" type="text" name="name" value=""/>
                        </div>
                        <div class="col-md-4">
                            <input class="form-control btn-info form-control-register" type="submit" value="追加"/>
                        </div>
                    </form>
                </div>
                <table class="table table-bordered table-striped">
                    <tr>
                        <th>ID</th>
                        <th>項目</th>
                        <th>有効</th>
                        <th>ボタン</th>
                    </tr>
                    <?php $index = 0; ?>
                    <?php foreach (FreeInputItemValue::selectFromFreeInputItemId($free_input_item_id) as $item): ?>
                        <tr>
                            <form method="post" onsubmit="return confirm('登録する。');">
                                <input type="hidden" name="action" value="modify"/>
                                <input type="hidden" name="id" value="<?= $item->id ?>"/>
                                <td class="text-center"><?= ++$index ?></td>
                                <td class=""><input class="form-control" type="text" name="name"
                                                    value="<?= $item->name ?>" /></td>
                                <td class=""><input class="form-control btn-info" type="checkbox" name="enabled"<?= $item->enabled ? ' checked="checked"' : '' ?> /></td>
                                <td class=""><input class="form-control btn-info form-control-register" type="submit" value="登録"/></td>
                            </form>
                        </tr>
                    <?php endforeach; ?>
                </table>
            </div>
        </div>
        <div class="col-md-3 sideContents sp-only mt-5">
                <?php include('adminsidebar.php'); ?>
            </div>
    </div>
</div>
<?php include('../footer.php'); ?>
