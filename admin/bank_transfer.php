<?php include_once(__DIR__ . '/../common/util.php'); ?>
<?php include_once(__DIR__ . '/../common/admin.php'); ?>
<?php

if (!isset($_POST['id']) && !isset($_GET['id'])) {
    setMessage('不正なアクセスです。。');
    header('Location: ' . getContextRoot() . '/');
    exit;
}

$payment = new PayoutHistory();
if (isset($_POST['id'])) {
    $payment->id = $_POST['id'];
} else {
    $payment->id = $_GET['id'];
}
$payment->select();

if (isset($_POST['complete'])) {
    $payment->updateComplete();
    setMessage('振込完了登録しました。');
    header('Location: ' . getContextRoot() . '/admin/request_bank_transfer.php');
    exit;
}

$system_config = SystemConfig::select();
?>
<?php include('../header.php'); ?>

    <div class="container">
        <div class="row">
            <div class="col-md-3 sideContents">
                <?php include('adminsidebar.php'); ?>
            </div>
            <div class="col-md-9 mainContents">
                <div class="bg-inner">
                    <h1 itemprop="headline" class="mb-4 bg-light p-4">振込依頼の確認</h1>
                    <form method="post" onsubmit="return confirm('振込が完了しました');">
                        <h2 class="bg-light p-2 fontBold">振込先</h2>
                        <div class="text-center p-4 mb-4">
                            <div class="fontBold">
                                お振込み金額：<span class="font25"><?= number_format($payment->amount) ?></span>円
                            </div>
                        </div>

                        <h3 class="fontBold">振込先の口座</h3>
                        <div class="mb-5">
                            <div>金融機関名：<?= $payment->user->financial_institution_name ?></div>
                            <div>支店名：<?= $payment->user->branch_name ?></div>
                            <div>預金種別：<?= $payment->user->get_deposit_type_name() ?></div>
                            <div>口座番号：<?= $payment->user->account_number ?></div>
                            <div>口座名義：<?= $payment->user->account_holder ?></div>
                            <input type="hidden" name="id" value="<?= $payment->id ?>"/>
                        </div>

                        <div class="my-4 w-75 mx-auto">
                            <input class="btn btn-block btn-info" type="submit" name="complete" value="振込が完了しました"/>
                            <p class="fontBold">※必ず振込が完了した後にボタンを押して下さい。</p>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>

<?php include('../footer.php'); ?>