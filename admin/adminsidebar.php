<?php include_once(__DIR__ . '/../common/util.php'); ?>
<?php include_once(__DIR__ . '/../entity/free_input_item.php'); ?>
<div class="userSidebar">
    <div class="bg-danger text-center border rounded p-2 mb-3 u-pd1">
        <span class="text-white fontBold">管理画面</span>
    </div>
    <div class="linkList">
        <h4>確認</h4>
        <div class="list-group list-group-flush">
            <a class="list-group-item" href="<?php echo HOME_URL; ?>/admin/">管理者ホーム</a>
            <a class="list-group-item" href="<?php echo HOME_URL; ?>/admin/userlist.php">ユーザー一覧・許可待ち確認</a>
            <a class="list-group-item" href="<?php echo HOME_URL; ?>/admin/sellitems.php">出品待ち確認</a>
            <a class="list-group-item" href="<?php echo HOME_URL; ?>/admin/request_items.php">購入希望待ち確認</a>
            <a class="list-group-item" href="<?php echo HOME_URL; ?>/admin/payment.php">購入代金入金の確認</a>
            <a class="list-group-item" href="<?php echo HOME_URL; ?>/admin/request_bank_transfer.php">出金申請の確認</a>
        </div>
    </div>
    <div class="linkList">
        <h4>投稿</h4>
        <div class="list-group list-group-flush">
            <a class="list-group-item" href="<?php echo HOME_URL; ?>/admin/information.php">お知らせ投稿</a>
        </div>
    </div>
    <div class="linkList">
        <h4>メール配信</h4>
        <div class="list-group list-group-flush">
            <a class="list-group-item" href="<?php echo HOME_URL; ?>/admin/mail_delivery.php">メール配信</a>
            <a class="list-group-item" href="<?php echo HOME_URL; ?>/admin/mail_auto_delivery.php">自動送信メール編集</a>
        </div>
    </div>
    <div class="linkList">
        <h4>追加項目</h4>
        <div class="list-group list-group-flush">
            <a class="list-group-item" href="<?php echo HOME_URL; ?>/admin/category.php">カテゴリー追加</a>
            <a class="list-group-item" href="<?php echo HOME_URL; ?>/admin/delivery_type.php">配送方法の追加</a>
            <a class="list-group-item" href="<?php echo HOME_URL; ?>/admin/carriage_type.php">配送目安の追加</a>
            <a class="list-group-item" href="<?php echo HOME_URL; ?>/admin/postage.php">送料負担の追加</a>
        </div>
        <h4>自由項目の追加</h4>
        <div class="list-group list-group-flush">
            <?php foreach (FreeInputItem::selectAll() as $free_item):?>
            <a class="list-group-item" href="<?php echo HOME_URL; ?>/admin/free_items.php?id=<?= $free_item->id ?>"><?= $free_item->name ?></a>
            <?php endforeach;?>
        </div>
    </div>
    <?php /* カード決済実装時に利用停止
    <div class="linkList">
        <h4>請求状況</h4>
        <div class="list-group list-group-flush">
            <a class="list-group-item" href="<?php echo HOME_URL; ?>/admin/invoice_list.php">手数料の請求</a>
        </div>
    </div>
     */ ?>
    <div class="linkList">
        <h4>設定</h4>
        <div class="list-group list-group-flush">
            <a class="list-group-item" href="<?php echo HOME_URL; ?>/admin/system_config.php">初期設定</a>
            <a class="list-group-item" href="<?php echo HOME_URL; ?>/admin/help.php">使い方(簡易版)</a>
        </div>
    </div>
</div>
