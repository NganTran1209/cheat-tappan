<?php include_once(__DIR__ . '/../common/util.php'); ?>
<?php include_once(__DIR__ . '/../common/admin.php'); ?>
<?php
$uri = $_SERVER["REQUEST_URI"];
$items = array();
$entity = new Item();
$category_title = 'すべて';
if (empty($_POST['search']) && empty($_GET['search'])) {
    if (isset($_GET['category'])) {
        $items = $entity->selectCategory($_GET['category']);
        $category_title = selectCodeName('selectCategoryById', $_GET['category']);
        $uri .= '&';
    } else {
        $items = $entity->selectAll();
        $uri .= '?';
    }
} else {
    if (empty($_POST['search'])) {
        $items = $entity->selectSearch($_GET['search']);
        $category_title = '検索文字「' . $_GET['search'] . '」';
        $uri .= '?search=' . $_GET['search'] . '&';
    } else {
        $items = $entity->selectSearch($_POST['search']);
        $category_title = '検索文字「' . $_POST['search'] . '」';
        $uri .= '?search=' . $_POST['search'] . '&';
    }
}

// ソート
if (!empty($_GET['sort'])) {
    if ($_GET['sort'] == '2') {
        usort($items, function ($a, $b) {
            return $b->price - $a->price;
        });
    } elseif ($_GET['sort'] == '3') {
        usort($items, function ($a, $b) {
            return $a->price - $b->price;
        });
    } else {

    }
    $aa = strpos($uri, 'sort');
    $uri = substr($uri, 0, strpos($uri, 'sort'));
}

?>

<?php include('../header.php'); ?>
<style>
    .main-header__search{
        display:none;
    }
    .main-header-link{
        justify-content: flex-end;
        margin-right: 30px;
    }
    @media screen and (max-width: 768px) {   
    .main-header-link{
        justify-content: flex-start !important;
        margin-right:0;
    }}
</style>
    <div class="container admin-page">
        <div class="row">
        <div class="col-md-3 sideContents pc-only">
                <?php include('adminsidebar.php'); ?>
            </div>
            <div class="col-md-9 mainContents">
                <div class="bg-inner admin-content-title">
                    <h1>出品待ち確認</h1>
                    <div class="item-list-admin">
                    <div class="text-right itemlist-wrap mt-5 mb-5">
                        <!-- おすすめ順 -->
                        <i class="fa fa-sort" aria-hidden="true"></i>
                        <a class="sortButton" href="<?= $uri ?>sort=1">新着順</a>
                        <a class="sortButton" href="<?= $uri ?>sort=2">価格の高い順</a>
                        <a class="sortButton" href="<?= $uri ?>sort=3">価格の安い順</a>
                    </div>

                    <?php foreach ($items as $item) { ?>
                        <?php if ($item->enabled != 0) {
                            continue;
                        } ?>
                        <div class="row mb-2 border-bottom">
                            <div class="col-md-3 pb-3">
                                <a href="itemdetail.php?id=<?= $item->id ?>">
                                    <img class="img-fluid"
                                         src="<?= $item->getThumbnailUrl() ?>"
                                         alt=""/></a>
                            </div>
                            <div class="col-md-9 pb-3 font-2rem">
                                <div>
                                    商品名：<a href="itemdetail.php?id=<?= $item->id ?>"><?= $item->buyer_id == null ? '' : '[売切れ]' ?><?= $item->title ?></a>
                                </div>
                                <!--                            <img src="--><?php //echo HOME_URL; ?><!--/images/lapias/icon_0--><?php //echo $item->category ?><!--.png" width="12" height="12" alt="--><?php //echo $item->category_name ?><!--"/>-->
                                カテゴリー名：<?= $item->category_name ?>
                                <div class="owner">
                                    出品者：<?= $item->owner_name ?>　　
                                    <a href="<?php echo HOME_URL; ?>/evaluate.php?user_id=<?= $item->owner_id ?>">評価：</a>
                                </div>
                                <div class="text-right">
                                    <?= number_format($item->price) ?>円
                                </div>
                            </div>

                        </div>
                    <?php } ?>
                    </div>
                </div>
            </div>
            <div class="col-md-3 sideContents sp-only mt-5">
                <?php include('adminsidebar.php'); ?>
            </div>
        </div>
    </div>

<?php include('../footer.php'); ?>