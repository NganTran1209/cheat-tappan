<?php include_once(__DIR__ . '/../common/util.php'); ?>
<?php include_once(__DIR__ . '/../common/admin.php'); ?>
<?php
$filter = 0;
if (isset($_POST['filter'])) {
    $filter = $_POST['filter'];
}

$payment = new PayoutHistory();
$list = $payment->selectForAll();
$feedback = new Feedback();
$list_feedback = $feedback->selectAll();
$system_config = SystemConfig::select();
if(isset($_POST['search-name'])){
    if($_POST["search-name"] == null){
        $list_feedback = $feedback->selectAll();
    }else{        
        $list_feedback = $feedback->selectSearch($_POST["search-name"]);
    }
}

?>
<?php include('../header.php'); ?>
<style>
    .main-header__search{
        display:none;
    }
    .main-header-link{
        justify-content: flex-end;
        margin-right: 30px;
    }
    @media screen and (max-width: 768px) {   
    .main-header-link{
        justify-content: flex-start !important;
        margin-right:0;
    }}
</style>
    <div class="container">
        <div class="row">
        <div class="col-md-3 sideContents pc-only">
                <?php include('adminsidebar.php'); ?>
            </div>
            <!-- col-sm-3 -->
            <div class="col-md-9 mainContents mb-5">
                <div class="bg-inner admin-content-title admin-page">
                    <h1>出金申請の確認</h1>
                    <div class="row mb-3 mt-5">
                        <!--▼▼▼検索ページビュー▼▼▼ -->
                        <!--ソート-->
                        <form id="frm" method="post" class="col-md-6">
                            <select name="filter" onchange="frm.submit();" class="form-control">
                                <option value="0"<?= $filter == 0 ? ' selected' : '' ?>>すべて</option>
                                <option value="1"<?= $filter == 1 ? ' selected' : '' ?>>未振り込みのみ</option>
                            </select>
                        </form>
                    </div>
                    <!-- 表示数制限 -->
                    <style>
                        .btn {
                            font-size: 1em;
                        }
                    </style>
                    <!-- ▲▲▲検索ページビュー▲▲▲ -->
                    <div class="table-responsive">
                        <table class="table table-bordered table-hover small text-nowrap">
                            <thead>
                            <tr class="text-center">
                                <th>処理状況</th>
                                <th>振込先</th>
                                <th>金額</th>
                                <th>登録時刻</th>
                            </tr>
                            </thead>
                            <tbody>
                            <?php foreach ($list as $item): ?>
                                <?php if ($filter == 1 && $item->status) {
                                    continue;
                                } ?>
                                <tr>
                                    <td class="text-center">
                                        <?php if ($item->status): ?>
                                            振込完了
                                        <?php else: ?>
                                            <a href="bank_transfer.php?id=<?= $item->id ?>">振込待ち</a>
                                        <?php endif; ?>
                                    </td>
                                    <td class="text-left">
                                        <?= $item->user->hash_id . ':' . $item->user->name ?>
                                    </td>
                                    <td class="text-left">
                                        <?= number_format($item->amount) ?>円
                                    </td>
                                    <td class="text-left">
                                        <?= $item->regist_date ?>
                                    </td>
                                </tr>
                            <?php endforeach; ?>
                            </tbody>
                        </table>
                    </div>
                    <br>
                    <div class="text-center">
                        <nav>
                            <ul class="pagination justify-content-center">
                                <li class="page-item"><span id="prev">← 前へ</span></li>
                                <li class="page-item hidden"><span id="page"> </span></li>
                                <li class="page-item"><span id="next">次へ →</span></li>
                            </ul>
                        </nav>
                    </div>
                    <div class="bg-inner admin-content-title">
                        <h1>注文フィードバック</h1>
                        <div class="row mb-3 mt-5">
                            <!--▼▼▼検索ページビュー▼▼▼ -->
                            <!--ソート-->
                            <form id="frm" method="post" class="col-md-6">
                            <input type="text" name="search-name" value="<?= (isset($_POST['search-name'])) ? $_POST["search-name"] : '' ?>" id="id_search" class="form-control"
                                    placeholder="売り手の名前を入力してください"/>
                            </form>
                        </div>
                        <!-- 表示数制限 -->
                        <style>
                            .btn {
                                font-size: 1em;
                            }
                        </style>
                    </div>
                    
                    <div class="table-responsive">
                        <table class="table table-bordered table-hover small text-nowrap">
                            <thead>
                            <tr class="text-center">
                                <th>買い手</th>
                                <th>売り手</th>
                                <th>製品</th>
                                <th>フィードバック</th>
                                <th>スコア</th>
                                <th>金額</th>
                                <th>手数料</th>
                            </tr>
                            </thead>
                            <tbody>
                            <?php foreach ($list_feedback as $fb): ?>
                                <tr>
                                    <td class="text-center">
                                        <?=$fb->user->name ?>
                                    </td>
                                    <td class="text-center">
                                        <?=$fb->item->owner_name ?>
                                    </td>
                                    <td class="text-center">
                                        <a href="<?php echo HOME_URL; ?>/user/negotiation.php?item_id=<?=$fb->item->id ?>&user_id=<?=$fb->user->id ?>"><?=$fb->item->title ?></a>
                                    </td>
                                    <td class="text-center" style="padding-right: 2rem;">
                                        <?php 
                                            $feedback1 = new Feedback();
                                            $feedback1->selectForIndex($fb->item->id,$fb->user->id,1);
                                            $feedback2 = new Feedback();
                                            $feedback2->selectForIndex($fb->item->id,$fb->user->id,2);
                                        ?>
                                        <?php if(@getimagesize(($feedback1->getImageUrl())) != false) : ?>
                                            <a href="<?= $feedback1->getImageUrl() ?>" target="_blank"><img class="img-fluid"
                                                width="300" src="<?= $feedback1->getImageUrl() ?>" /></a><br /><br />
                                        <?php endif; ?>
                                        <?php if(@getimagesize(($feedback2->getImageUrl())) != false) : ?>
                                            <a href="<?= $feedback2->getImageUrl() ?>" target="_blank"><img class="img-fluid"
                                                width="300" src="<?= $feedback2->getImageUrl() ?>" /></a>
                                        <?php endif; ?>
                                    </td>
                                    <td class="text-left">
                                        <?php if($feedback1->score != null) : ?>
                                            <p><?=round(($feedback1->score)*100,1) ?>%</p>
                                        <?php endif; ?>
                                        <?php if($feedback2->score != null) : ?>
                                            <p ><?=round(($feedback2->score)*100,1) ?>%</p>
                                        <?php endif; ?>
                                    </td>
                                    <?php 
                                        $cash_flow = new CashFlow();
                                        $cash_flow->selectByOrderId($fb->order_id);
                                    ?>
                                    <td class="text-left">
                                        <?=$cash_flow->amount ?>
                                    </td>
                                    <td class="text-left">
                                        <?=$cash_flow->commission ?>
                                    </td>
                                </tr>
                            <?php endforeach; ?>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
            <div class="col-md-3 sideContents sp-only mt-5">
                <?php include('adminsidebar.php'); ?>
            </div>
        </div>
    </div>

<?php include('../footer.php'); ?>