<?php include_once(__DIR__.'/../common/util.php'); ?>
<?php include_once(__DIR__ . '/../common/admin.php'); ?>
<?php include_once(__DIR__.'/../entity/identification.php'); ?>
<?php

if (!isset($_GET['user_id'])) {
    exit;
}

if (!Identification::hasPhoto($_GET['user_id'])) {
    exit;
}

$photo = file_get_contents(Identification::getPhoto1Path($_GET['user_id']));
$finfo = finfo_open(FILEINFO_MIME_TYPE);
$mime = finfo_buffer($finfo, $photo);

header("Content-Type: ".$mime);
echo $photo;
exit();
