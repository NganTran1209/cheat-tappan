
<?php include_once(__DIR__ . '/../common/util.php'); ?>
<?php include_once(__DIR__ . '/../common/admin.php'); ?>
<?php
// TODO ベーシック認証でログインできないかも ＠共有サーバー
// 一時的に無効化
//require_once __DIR__ . '/basic.php';
//$username = require_basic_auth();
?>
<?php
//// TODO アドミンログインごとにログアウトをやめる
//$_SESSION['admin'] = true;
//?>
<?php include('../header.php'); ?>

<style>
    .main-header__search{
        display:none;
    }
    .main-header-link{
        justify-content: flex-end;
        margin-right: 30px;
    }
    @media screen and (max-width: 768px) {   
    .main-header-link{
        justify-content: flex-start !important;
        /* margin-right: 30px; */
        margin-right:0;
    }}
</style>
<div class="container admin-container">
    <div class="row">
        <div class="col-sm-9 u-mgauto">
            <?php include('adminsidebar.php'); ?>
        </div>
        <!-- <div class="col-sm-9">

        </div> -->
    </div>
</div>
<?php include('../footer.php'); ?>

