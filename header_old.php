<?php include_once(__DIR__ . '/common/util.php'); ?>
<?php /* サイトタイトル用　パーセント表示など */
$system_config = SystemConfig::select(); ?>
<?php /*スマホ判定処理 これも使ってないので不要 */
//function ua_smt() {
////ユーザーエージェントを取得
//    $ua = $_SERVER['HTTP_USER_AGENT'];
////スマホと判定する文字リスト
//    $ua_list = array('iPhone', 'iPod', 'Android');
////$ua_list = array('iPhone','iPad','iPod','Android');
//    foreach ($ua_list as $ua_smt) {
////ユーザーエージェントに文字リストの単語を含む場合はTRUE、それ以外はFALSE
//        if (strpos($ua, $ua_smt) !== false) {
//            return true;
//        }
//    }
//    return false;
//}

// TODO アドミンログインごとにログアウトをやめる
//if (isAdminPage()) {
//    if (logout()) {
//        setMessage('管理者画面へ移動したため一般ユーザーはログアウトします。');
//    }
//}

if (!isset($item)) {
    $item = new Item();
}

?>
<!DOCTYPE html>
<html lang="ja">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title><?php echo $item->title; ?><?= $system_config->site_name ?></title>
    <meta name="keywords" content="特技売る,スキル販売,技術売る,マーケットプレイス,知識販売,フリマ,スキルマーケット">
    <meta name="description" content="<?php $str = $item->remarks; $str = str_replace(PHP_EOL, '', $str); echo mb_strimwidth(strip_tags($str), 0,120);?>">
    <link rel="apple-touch-icon" href="<?php echo HOME_URL; ?>/images/favicon.ico" type="image/vnd.microsoft.icon">
    <link rel="shortcut icon" href="<?php echo HOME_URL; ?>/images/favicon.ico" type="image/vnd.microsoft.icon">
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css"
          integrity="sha384-Vkoo8x4CGsO3+Hhxv8T/Q5PaXtkKtu6ug5TOeNV6gBiFeWPGFN9MuhOf23Q9Ifjh" crossorigin="anonymous">
    <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Noto+Sans+JP&display=swap">
    <link rel="stylesheet" href="<?php echo HOME_URL; ?>/common/css/style.css?11" type="text/css">
    <link rel="stylesheet" href="<?= StyleCss::getFileName() ?>?24" type="text/css">
    <script src="https://code.jquery.com/jquery-3.4.1.min.js"
            integrity="sha256-CSXorXvZcTkaix6Yvo6HppcZGetbYMGWSFlBw8HfCJo="
            crossorigin="anonymous"></script>
    <!--    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>-->
    <script src="https://kit.fontawesome.com/1e4904d391.js" crossorigin="anonymous"></script>
    <script src="<?php echo HOME_URL; ?>/common/js/common-20200611.js"></script>
</head>
<body>
<?php ini_set('display_errors', "On"); ?>
<nav class="navbar navbar-expand-lg navbar-light">
    <div class="container">
        <a class="navbar-brand" href="<?php echo HOME_URL; ?>"><img src="<?= SystemConfig::get_logo_image_url() ?>" alt="<?= $system_config->site_name ?>" /></a>
<!--        <a class="navbar-brand" href="--><?php //echo HOME_URL; ?><!--">--><?//= SITE_NAME ?><!--</a>-->
        <button type="button" class="navbar-toggler" data-toggle="collapse" data-target="#Navber" aria-controls="Navber"
                aria-expanded="false" aria-label="メニュー">
            <span class="navbar-toggler-icon"></span>
        </button>
        <div class="collapse navbar-collapse" id="Navber">
            <ul class="navbar-nav my-2 my-lg-0 ml-auto small">
                <li class="nav-item"><a class="nav-link" href="<?php echo HOME_URL; ?>">ホーム</a></li>
                <li class="nav-item"><a class="nav-link" href="<?php echo HOME_URL; ?>/pages/buyinfo.php">買う</a></li>
                <li class="nav-item"><a class="nav-link" href="<?php echo HOME_URL; ?>/pages/sellinfo.php">売る</a></li>
                <li class="nav-item"><a class="nav-link" href="<?php echo HOME_URL; ?>/pages/priceinfo.php">ご利用料金</a></li>
                <li class="nav-item"><a class="nav-link" href="<?php echo HOME_URL; ?>/itemlist.php">出品一覧</a></li>
            </ul>
            <?php
            $url = $_SERVER["REQUEST_URI"];
            if (strstr($url, "admin") == true):?>
                <a href="<?php echo HOME_URL; ?>/admin/" class="btn btn-sm ml-2 btn-danger">管理画面</a>
            <?php elseif (isLogin()): ?>
                <?php if (isAdminUser()): ?>
                <a href="<?php echo HOME_URL; ?>/admin/" class="btn btn-sm ml-2 btn-danger">管理画面へ<?php //echo getHashKey(); ?></a>
                <?php endif; ?>
                <a href="<?php echo HOME_URL; ?>/login.php" class="btn btn-sm ml-2 btn-info"><?= isLogin() ? 'ログアウト' : 'ログイン' ?></a>
                <a href="<?php echo HOME_URL; ?>/user/mypage.php" class="btn btn-sm ml-2 btn-danger"><i class="fas fa-user mr-1"></i>マイページ</a>
            <?php elseif (!isLogin()): ?>
                <a href="<?php echo HOME_URL; ?>/login.php" class="btn btn-sm ml-2 btn-info"><?= isLogin() ? 'ログアウト' : 'ログイン' ?></a>
                <a href="<?php echo HOME_URL; ?>/user/userentry.php" class="btn btn-info btn-sm ml-2">新規会員登録</a>
            <?php endif; ?>
        </div>
    </div>
</nav>
<?php //保存・エラーメッセージ
$msg = getMessage();
if (count($msg) > 0 && !empty($msg['msg'])) {
    if ($msg['pop']) {
        echo '<script>window.alert("' . $msg['msg'] . '");</script>';
    } else {
        echo "<div class='container'><div class='alert alert-success'><strong>" . $msg['msg'] . "</strong></div></div>";
    }
}
?>
