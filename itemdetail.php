<?php 
$title_page = '商品の詳細';
$description_page = '欲しい商品を画像で確認・届いた商品を画像で照合！気持ち良い取引が叶うオンラインマーケット。';
 ?>
<?php include_once(__DIR__ . '/common/util.php'); ?>
<?php

$item = new Item();
$negotiation = new Negotiation();
$category_title = 'すべて';
if (!empty($_GET['id'])) {
    $negotiation->item_id = $_GET['id'];
    $item->select($_GET['id']);
} else {
    $item->select($_POST['id']);
    $negotiation->item_id = $_POST['id'];
}
if ($item->enabled == 9) {
    setMessage('出品が取り消されました。');
    header('Location: ' . getContextRoot() . '/');
    exit;
} elseif (User::isInvalid($item->owner_id)) {
    setMessage('出品者が退会しました。');
    header('Location: ' . getContextRoot() . '/');
    exit;
// } elseif ($item->isBlock()) {
// 	setMessage( '閲覧できません。' );
// 	header( 'Location: '.getContextRoot().'/' );
// 	exit;
}

list($before,$after) = $negotiation->select3();
$negotiation_all_item = array_merge($after,$before);

unset($_SESSION['negotiation']);
if (isset($_POST['comment'])) {
    $negotiation->comment = $_POST['comment'];
    $negotiation->kbn = $_POST['kbn'];
    $negotiation->item_id = $_POST['item_id'];
    $negotiation->user_id = getUserId();

    $negotiation->regist();
    setMessage('コメントを送信しました。');
}

if (isset($_POST['action'])) {
    if ($_POST['action'] == 'buy') {
        $order = new Order();
        $order->item_id = $_POST['item_id'];
        $order->user_id = getUserId();
        $order->state = 0;

        $order->regist();
        setMessage('購入しました。<br>送付先やお支払のやり取りを開始してください。 ');
        $url = getContextRoot() . '/user/negotiation.php?item_id=' . $_POST['item_id'] . '&user_id=' . getUserId();
        header('Location: ' . $url);
    } elseif ($_POST['action'] == 'negotiation') {
        setMessage('質問・交渉開始メールを送信しました。');
        $res = $negotiation->sendBeginNegotiationMail($_POST['item_id']);
        if ($res['status']) {
            header('Location: ' . $res['url']);
            exit;
        }
        //    $_SESSION[ 'negotiation' ] = $_GET[ 'id' ];
    } elseif ($_POST['action'] == 'cancel') {
        $item = new Item();
        $item->select($_POST['item_id']);
        $item->cancel();
        setMessage('出品を取り消しました。');
    } elseif ($_POST['action'] == 'favorites') {
        $item = new Item();
        $item->select($_POST['item_id']);
        if($item->hasFavorites()){
            $item->deleteFavorites();
        }else{
            $item->registFavorites();
        }
        setMessage('お気に入り追加しました。');
        echo $item->selectCountFavorites();
        exit();
    } elseif ($_POST['action'] == 'cancelFavorites') {
        $item = new Item();
        $item->select($_POST['item_id']);
        $item->deleteFavorites();
        setMessage('お気に入り解除しました。');
        echo $item->selectCountFavorites();
        exit();
    } elseif ($_POST['action'] == 'blockAccess') {
        $item = new Item();
        $item->select($_POST['item_id']);

        $block = new BlockAccess();
        $block->user_id = getUserId();
        $block->block_user_id = $item->owner_id;
        $block->regist();
        setMessage('対象のユーザーをブロックしました。');
        header('Location: ' . getContextRoot() . '/');
        exit();
    } elseif ($_POST['action'] == 'good') {
        Item::updateItemGoodCountUp($item->id);
        echo Item::selectItemGoodCountSummary($item->id);
        exit();
    }
}

$item->select($_GET['id']);
if ($item->enabled == 9) {
    setMessage('出品が取り消されました。');
    header('Location: ' . getContextRoot() . '/');
    exit;
} elseif (User::isInvalid($item->owner_id)) {
    setMessage('出品者が退会しました。');
    header('Location: ' . getContextRoot() . '/');
    exit;
// } elseif ($item->isBlock()) {
// 	setMessage( '閲覧できません。' );
// 	header( 'Location: '.getContextRoot().'/' );
// 	exit;
}

// アクセス数カウントアップ
Item::updateItemAccessCount($item->id);

$negotiation->item_id = $item->id;
$negotiation->user_id = getUserId();
//$comments = $negotiation->select();
list($before, $after) = $negotiation->select2();
if (count($before) + count($after) > 0) {
    //＠＠＠ここで交渉履歴の有無によるボタン制御を抑止（2018.09.10）
    //$_SESSION[ 'negotiation' ] = $item->id;
}

$user_evaluate = new Evaluate();
$user_evaluate->selectSummary(getUserId());

$view_favorites_button = true;
$view_favorites_count = true;
$view_other_item_link = true;
$view_evaluate = true;
if (!empty($_GET['new_title'])) {
    $category_title = $_GET['new_title'];
}else{
    $category_title = selectCodeName('selectCategoryById', $item->category);
}
$title_page = $category_title;
?>
<style>
    .ladies-product-btn form {
        display: inline-block;
    }
    .ladies-product-btn .negotiation button {
        background-color: #7dc02d;
    }
    .ladies-product-btn .buy-form {
        margin-left: 3rem;
    }
    .ladies-product-btn .buy-form button {
        background-color: #ff7800;
        padding: 10px 8rem;
    }
    .transaction-btn form {
        width: 48%;
    }
    .transaction-btn form button {
        background-color: #ff7800;
    }
}
</style>
<?php include('other_header.php'); ?>
<script>
    function updateGood() {

        var requestData = new FormData();
        requestData.append('id', '<?= $item->id?>');
        requestData.append('action', 'good');
        var result = $.ajax({
            url: './itemdetail.php',
            type: 'post',
            async: false,
            processData: false,
            contentType: false,
            data: requestData
        }).responseText;

        $('#good').text(result);
    }
    function registFavorites() {
        var requestData = new FormData();        
        requestData.append('id', '<?= $item->id?>');
        requestData.append('item_id', '<?= $item->id?>');
        requestData.append('action', 'favorites');
        var result = $.ajax({
            url: './itemdetail.php',
            type: 'post',
            async: false,
            processData: false,
            contentType: false,
            data: requestData
        }).responseText;
        $('#favorite').text(result);
    }
</script>
<div class="com-header-top">
    <div class="com-header-top__img  wow animate__animated animate__fadeInUp">
        <img src="<?php echo HOME_URL; ?>/common/assets/img/common/other-com-header.png" alt="">
    </div>
    <div class="com-header-top__path bg-other-01 wow animate__animated animate__fadeInUp">
        <p><span><a href="<?php echo HOME_URL; ?>/" class="clr-yel">トップページ</a></span><span> > 
            </span><span><a href="<?php echo HOME_URL; ?>/itemlist.php?category=<?= $item->category ?>&new_title=レディース新着アイテム" class="clr-yel"><?= $category_title ?></a></span></p>
    </div>
 <!--   <div class="com-header-top__txt">
        <p>レディース新着アイテム</p>
    </div>-->
</div>
<div class="customer-container">
<div class="category-title  wow animate__animated animate__fadeInUp">
                <h3><span><?= $category_title ?></span></h3>
            </div>
    <div class="customer-contact-form">
        <div class="ladies-product-list  wow animate__animated animate__fadeInUp">
        <div class="content-title">
                <h3><span><?= $item->title ?></span></h3>
            </div>
            <div class="ladies-product-list-content">
                <?php include_once(__DIR__ . '/common/parts/item_image_area.php'); ?>
                <?php include_once(__DIR__ . '/common/parts/item_info_area.php'); ?>
            </div>
            <div class="ladies-product-price">
                <div class="ladies-product-price__txt">
                    <p><span>¥<?= number_format($item->price) ?> 円</span>（税込<?= $item->carriage_plan_name ?>）</p>
                    <?php if ($item->order_id != null): ?>
                        <?php if ($item->buyer_id == getUserId()): ?>
                            <br><span class="small font-vio">この商品は、あなたが購入しました。</span>
                        <?php elseif ($item->owner_id == getUserId()): ?>
                            <br><span class="small font-vio">この商品は、あなたが販売しました。</span>
                        <?php endif; ?>
                    <?php endif; ?>
                </div>
                <?php if ($item->isBlock()): ?>
                    <div class="text-center fontBold fontRed">この出品者とは取引できません。</div>
                <?php elseif ($item->order_id != null): ?>
                    <button type="button" class="btn btn-block btn-danger disabled" aria-disabled="true" disabled>売り切れました</button>
                <?php elseif (!isLogin()): ?>
                    <a class="btn btn-block btn-info"
                    href="<?php echo HOME_URL; ?>/user/userentry.php">無料会員登録</a><br>
                    <div class="text-center fontBold">無料会員登録すると購入できます。</div>
                <?php elseif (!hasBuying()): ?>
                    <div class="text-center">
                        <a class="btn btn-block btn-info fontBold"
                        href="<?php echo HOME_URL; ?>/user/upload_identification.php">本人認証こちら</a><br>
                        本人認証後に購入や交渉が出来ます。
                    </div>
                    
                <?php elseif (User::isInvalid($item->owner_id)): ?>
                <div class="text-center fontBold">出品者は退会しました</div>
                <?php elseif (getUserId() != $owner->id) : ?>
                    <div class="ladies-product-btn">
                        <?php if ($item->order_id == null || $item->buyer_id == getUserId()): ?>
                            <?php if (!isset($_SESSION['negotiation']) || $_SESSION['negotiation'] != $item->id): ?>
                                <form method="post" class="negotiation">
                                    <input type="hidden" name="item_id" value="<?= $item->id ?>"/>
                                    <input type="hidden" name="action" value="negotiation"/>
                                    <?php if ($item->order_id == null): ?>
                                        <button type="submit" id="" name="">質問・交渉する</button>
                                    <?php else: ?>
                                        <button type="submit" id="" name="">取引連絡のコメントする</button>
                                    <?php endif; ?>
                                </form>
                            <?php endif; ?>
                        
                        <!-- <button type="submit" id="" name="">購入する</button> -->
                        <form method="post" class="buy-form" onsubmit="return confirm('購入します。');">
                            <input type="hidden" name="item_id" value="<?= $item->id ?>"/>
                            <input type="hidden" name="action" value="buy"/>
                            <?php if ($item->order_id == null): ?>
                                <button type="submit" class="btn btn-block btn-danger">購入する</button>
                                
                            <?php else: ?>
                                <button type="submit" class="btn btn-block btn-danger" disabled="disabled">購入済</button>
                            <?php endif; ?>
                        </form>
                        <div class="ladies-product-order-warning">
                            <p>※購入後のキャンセルや返品はできません。</p>
                        </div>
                        <?php endif; ?>
                    </div>
                    <?php else: ?>
                        <?php if ($item->order_id == null): ?>
                            <!--出品者自身<br>-->
                            <form method="post" class="buy-form" onsubmit="return confirm('出品を取り消します。'); ">
                                <input type="hidden" name="item_id" value="<?= $item->id ?>"/>
                                <input type="hidden" name="action" value="cancel"/>
                                <button type="submit" >出品を取り消す</button>
                            </form>
                            <div class="ladies-product-order-warning">
                                <p>※購入後のキャンセルや返品はできません。</p>
                            </div>
                        <?php endif; ?>
                    <?php endif; ?>   
                
            </div>
            <div class="mt-5 title-detail">
                <h3 class="font-weight-bold">この商品について</h3>
            </div>
                
            <div class="ladies-product-content mt-0">
                <p style="word-break: break-word;"><?php echo nl2br(htmlentities($item->remarks)); ?></p>
            </div>
            <p class="transaction-comment-head"><span>質問・交渉のコメント履歴</span></p>
            <?php if (count($negotiation_all_item) > 0): ?>
            <div class="transaction-content transaction-content-img" style="margin-top:10">
                <?php foreach ($negotiation_all_item as $comment): ?>
                <?php
                        $commenter = new User();
                        $commenter->select($comment->user_id);
                        ?>
                <div class="evaluation-item-show-result">
                    <div class="evaluation-result-person">
                        <div class="evaluation-result-person__img evaluation-result-person_img_comment">
                            <?php if($commenter->get_icon_url() == ''):?>
                                <img src="<?php echo HOME_URL; ?>/common/assets/img/common/icon/profile.png" alt="">
                            <?php else: ?>
                                <img src="<?= $commenter->get_icon_url();?>" alt="">
                            <?php endif; ?>
                        </div>
                        <div class="evaluation-result-person__name">
                            <?php if ($comment->kbn == 1): ?>
                            <p><a class="clr-yel"
                                    href="<?php echo HOME_URL; ?>/user/profile/<?= $commenter->hash_id ?>">
                                    <?= $comment->user_name ?></a></p>
                            <?php else: ?>
                            <p>
                            <a class="clr-yel" href="<?php echo HOME_URL; ?>/user/profile/<?= $commenter->hash_id ?>">
                                <?= $commenter->name ?></a></p>
                            <?php endif; ?>
                        </div>

                    </div>
                    <div class="evaluation-result-content">
                        <div class="evaluation-result-content-head">
                            <div class="evaluation-result-content-head__level">
                                <?php if ($comment->kbn == 1): ?>
                                <p><a class="clr-yel"
                                        href="<?php echo HOME_URL; ?>/user/seller_home.php?owner_id=<?= $comment->user_id ?>">購入希望者
                                    </a>
                                    <!-- <?= $user_evaluate->point ?> -->
                                </p>
                                <?php else: ?>
                                <p><a class="clr-yel"
                                        href="<?php echo HOME_URL; ?>/user/seller_home.php?owner_id=<?= $comment->user_id ?>">出品者
                                    </a>
                                </p>
                                <?php endif; ?>
                            </div>
                            <div class="evaluation-result-content-head__sex">
                                <!-- <p>レディース</p> -->
                            </div>
                        </div>
                        <div class="evaluation-result-content__txt evaluation-result-content__txt_padding">
                            <p class="size-1_5rem"><?php echo nl2br(htmlentities($comment->comment)); ?></p>
                            <div class="">
                                <?php if ($comment->hasPhoto()): ?>
                                <a href="<?= $comment->getImageUrl() ?>" target="_blank"><img class="img-fluid"
                                        width="300" src="<?= $comment->getImageUrl() ?>" /></a>
                                <?php endif; ?>
                                <?php if ($comment->hasPdf()): ?>
                                <embed src="<?= $comment->getPdfUrl() ?>" type="application/pdf" width="100%"
                                    height="100%">
                                <a class="clr-yel itemContentText" href="<?php echo $comment->getPdfUrl() ?>"
                                    target="_blank">PDFダウンロード</a>
                                <?php endif; ?>
                            </div>
                            <p class="text-right"><i class="bi bi-alarm"></i> <?= $comment->regist_date ?></p>
                        </div>
                    </div>
                </div>
                <?php endforeach; ?>
            </div>
            <?php endif ?>
        </div>
    </div>


<?php include('footer.php'); ?>
<script src="https://code.jquery.com/jquery-3.4.1.min.js"
        integrity="sha256-CSXorXvZcTkaix6Yvo6HppcZGetbYMGWSFlBw8HfCJo="
        crossorigin="anonymous"></script>