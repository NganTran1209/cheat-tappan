<?php include_once(__DIR__ . '/common/util.php'); ?>
<?php
if (isLogin()) {
    setMessage('本登録済みです。');
    header('Location: '.getContextRoot().'/');
    exit;
}
$entity = new User();
if (isset($_POST['action']) && $_POST['action'] == 'send-mail') {
    $entity->email = $_POST['email'];
    if (!$entity->selectFromEmail()) {
        setMessage("ユーザー情報登録で使用したメールアドレスを入力してください。");
    } elseif ($entity->enabled != 0) {
        setMessage("指定されているメールアドレスのアカウントは本登録済みです。");
    } else {
        $entity->sendRegistMail();
        setMessage("仮登録メールを再送しました。");
    }
}
?>
<?php include(__DIR__ . '/header.php'); ?>

<div class="container">
    <div class="row">
        <div class="col-md-9 mainContents">
            <div class="bg-inner">
                <aside id="formlogin">
                    <form id="form" class="form-signin wow animate__animated animate__fadeInUp" method="post" onsubmit="return confirm('再送する');">
                        <h3 class="form-signin-heading"><i class="fa fa-key fa-flip-vertical" aria-hidden="true"></i>仮登録メールを再送する
                        </h3>
                        <hr class="colorgraph">
                        <input type="hidden" name="action" value="send-mail"/>
                        <input class="form-control middle-value mb-2" type="text" name="email" id="email"
                               value="<?= $entity->email ?>" placeholder="メールアドレスを入力してください">
                        <button class="btn btn-sm btn-login-color btn-block" name="Submit" value="reset" type="Submit">
                            仮登録メールを再送する
                        </button>
                    </form>
                </aside>
            </div>
        </div>
        <div class="col-md-3 sideContents wow animate__animated animate__fadeInUp">
            <?php include('sidebar.php'); ?>
        </div>
    </div>

</div>

<?php include('footer.php'); ?>
