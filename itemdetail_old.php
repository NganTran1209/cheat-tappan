<?php include_once(__DIR__ . '/common/util.php'); ?>
<?php

$item = new Item();
if (!empty($_GET['id'])) {
    $item->select($_GET['id']);
} else {
    $item->select($_POST['id']);
}
if ($item->enabled == 9) {
    setMessage('出品が取り消されました。');
    header('Location: ' . getContextRoot() . '/');
    exit;
} elseif (User::isInvalid($item->owner_id)) {
    setMessage('出品者が退会しました。');
    header('Location: ' . getContextRoot() . '/');
    exit;
// } elseif ($item->isBlock()) {
// 	setMessage( '閲覧できません。' );
// 	header( 'Location: '.getContextRoot().'/' );
// 	exit;
}

$negotiation = new Negotiation();
unset($_SESSION['negotiation']);
if (isset($_POST['comment'])) {
    $negotiation->comment = $_POST['comment'];
    $negotiation->kbn = $_POST['kbn'];
    $negotiation->item_id = $_POST['item_id'];
    $negotiation->user_id = getUserId();

    $negotiation->regist();
    setMessage('コメントを送信しました。');
}

if (isset($_POST['action'])) {
    if ($_POST['action'] == 'buy') {
        $order = new Order();
        $order->item_id = $_POST['item_id'];
        $order->user_id = getUserId();
        $order->state = 0;

        $order->regist();
        setMessage('購入しました。<br>送付先やお支払のやり取りを開始してください。 ');
        $url = getContextRoot() . '/user/negotiation.php?item_id=' . $_POST['item_id'] . '&user_id=' . getUserId();
        header('Location: ' . $url);
    } elseif ($_POST['action'] == 'negotiation') {
        setMessage('質問・交渉開始メールを送信しました。');
        $res = $negotiation->sendBeginNegotiationMail($_POST['item_id']);
        if ($res['status']) {
            header('Location: ' . $res['url']);
            exit;
        }
        //    $_SESSION[ 'negotiation' ] = $_GET[ 'id' ];
    } elseif ($_POST['action'] == 'cancel') {
        $item = new Item();
        $item->select($_POST['item_id']);
        $item->cancel();
        setMessage('出品を取り消しました。');
    } elseif ($_POST['action'] == 'favorites') {
        $item = new Item();
        $item->select($_POST['item_id']);
        $item->registFavorites();
        setMessage('お気に入り追加しました。');
    } elseif ($_POST['action'] == 'cancelFavorites') {
        $item = new Item();
        $item->select($_POST['item_id']);
        $item->deleteFavorites();
        setMessage('お気に入り解除しました。');
    } elseif ($_POST['action'] == 'blockAccess') {
        $item = new Item();
        $item->select($_POST['item_id']);

        $block = new BlockAccess();
        $block->user_id = getUserId();
        $block->block_user_id = $item->owner_id;
        $block->regist();
        setMessage('対象のユーザーをブロックしました。');
        header('Location: ' . getContextRoot() . '/');
        exit();
    } elseif ($_POST['action'] == 'good') {
        Item::updateItemGoodCountUp($item->id);
        echo Item::selectItemGoodCountSummary($item->id);
        exit();
    }
}

$item->select($_GET['id']);
if ($item->enabled == 9) {
    setMessage('出品が取り消されました。');
    header('Location: ' . getContextRoot() . '/');
    exit;
} elseif (User::isInvalid($item->owner_id)) {
    setMessage('出品者が退会しました。');
    header('Location: ' . getContextRoot() . '/');
    exit;
// } elseif ($item->isBlock()) {
// 	setMessage( '閲覧できません。' );
// 	header( 'Location: '.getContextRoot().'/' );
// 	exit;
}

// アクセス数カウントアップ
Item::updateItemAccessCount($item->id);

$negotiation->item_id = $item->id;
$negotiation->user_id = getUserId();
//$comments = $negotiation->select();
list($before, $after) = $negotiation->select2();
if (count($before) + count($after) > 0) {
    //＠＠＠ここで交渉履歴の有無によるボタン制御を抑止（2018.09.10）
    //$_SESSION[ 'negotiation' ] = $item->id;
}

$user_evaluate = new Evaluate();
$user_evaluate->selectSummary(getUserId());

$view_favorites_button = true;
$view_favorites_count = true;
$view_other_item_link = true;
$view_evaluate = true;
?>
<?php include('header.php'); ?>
    <script>
        function updateGood() {

            var requestData = new FormData();
            requestData.append('id', '<?= $item->id?>');
            requestData.append('action', 'good');
            var result = $.ajax({
                url: './itemdetail.php',
                type: 'post',
                async: false,
                processData: false,
                contentType: false,
                data: requestData
            }).responseText;

            $('#good').text(result);

        }
    </script>
    <div class="container">
        <div class="row">
            <div class="col-md-9 mainContents">
                <div class="bg-inner">
                    <div class="itemTitle">
                        <h1><?= $item->title ?></h1>
                    </div>
                    <div class="row">
                        <?php include_once(__DIR__ . '/common/parts/item_image_area.php'); ?>
                        <?php include_once(__DIR__ . '/common/parts/item_info_area.php'); ?>
                    </div>

                    <div class="mb-4">
                        <div class="text-center">
                            <span class="font30 fontBold fontRed"><?= number_format($item->price) ?></span>
                            <span class="smallEX">円(税込<?= $item->carriage_plan_name ?>)</span>
                            <?php if ($item->order_id != null): ?>
                                <?php if ($item->buyer_id == getUserId()): ?>
                                    <br><span class="small font-vio">この商品は、あなたが購入しました。</span>
                                <?php elseif ($item->owner_id == getUserId()): ?>
                                    <br><span class="small font-vio">この商品は、あなたが販売しました。</span>
                                <?php endif; ?>
                            <?php endif; ?>
                        </div>
                        <?php if ($item->isBlock()): ?>

                            <div class="text-center fontBold fontRed">この出品者とは取引できません。</div>

                        <?php elseif ($item->order_id != null): ?>

                            <input class="btn btn-block btn-danger disabled" type="submit" value="売り切れました"
                                   aria-disabled="true" disabled/>

                        <?php elseif (!isLogin()): ?>

                            <a class="btn btn-block btn-info"
                               href="<?php echo HOME_URL; ?>/user/userentry.php">無料会員登録</a>
                            <div class="text-center fontBold">無料会員登録すると購入できます。</div>

                        <?php elseif (!hasBuying()): ?>

                            <div class="text-center">
                                <a class="btn btn-block btn-info fontBold"
                                   href="<?php echo HOME_URL; ?>/user/upload_identification.php">本人認証こちら</a>
                                本人認証後に購入や交渉が出来ます。
                            </div>


                        <?php elseif (User::isInvalid($item->owner_id)): ?>

                            <div class="text-center fontBold">出品者は退会しました</div>

                        <?php elseif (getUserId() != $owner->id) : ?>
                            <div class="row">
                                <?php if ($item->order_id == null || $item->buyer_id == getUserId()): ?>
                                    <?php if (!isset($_SESSION['negotiation']) || $_SESSION['negotiation'] != $item->id): ?>
                                        <div class="col-sm-6 mb-4">
                                            <form method="post">
                                                <input type="hidden" name="item_id" value="<?= $item->id ?>"/>
                                                <input type="hidden" name="action" value="negotiation"/>
                                                <?php if ($item->order_id == null): ?>
                                                    <input class="btn btn-block btn-info"
                                                           type="submit" value="質問・交渉する"/>
                                                <?php else: ?>
                                                    <input class="btn btn-block btn-info"
                                                           type="submit" value="取引連絡のコメントする"/>
                                                <?php endif; ?>
                                            </form>
                                        </div>
                                    <?php endif; ?>
                                    <div class="col-sm-6 mb-4">
                                        <form method="post" onsubmit="return confirm('購入します。'); ">
                                            <input type="hidden" name="item_id" value="<?= $item->id ?>"/>
                                            <input type="hidden" name="action" value="buy"/>
                                            <?php if ($item->order_id == null): ?>
                                                <input class="btn btn-block btn-danger" type="submit"
                                                       value="購入する"/>
                                                <div class="font10 text-center">
                                                    ※購入後のキャンセルや返品はできません。
                                                </div>
                                            <?php else: ?>
                                                <input class="btn btn-block btn-danger" type="submit"
                                                       value="購入済" disabled="disabled"/>
                                            <?php endif; ?>
                                        </form>
                                    </div>
                                    <?php /*?>
                <div class="col-sm-12 margin-30">
                  <form method="post" onsubmit="return confirm('評価します。'); ">
                    <input type="hidden" name="action" value="evaluate"/>
                    <?php createRadio('selectEvaluate', 'evaluate', 0); ?>
                    <textarea class="form-control" rows="5" name="evaluate_comment" placeholder="コメントを入力してください"></textarea>
                    <input class="btn btn-block" type="submit" value="評価する"/>
                  </form>
                </div>
                <?php */ ?>
                                <?php endif; ?>

                            </div>


                        <?php else: ?>
                            <?php if ($item->order_id == null): ?>

                                <!--出品者自身<br>-->
                                <form method="post" onsubmit="return confirm('出品を取り消します。'); ">
                                    <input type="hidden" name="item_id" value="<?= $item->id ?>"/>
                                    <input type="hidden" name="action" value="cancel"/>
                                    <input class="btn btn-block btn-outline-danger btn-sm" type="submit" value="出品を取り消す"/>
                                </form>

                                <p>
                                    ※取り消すと、この画面は二度と復活できません。再度、掲載する場合は再出品してください。<br>
                                    重要！出品取り消しを頻繁にされた場合、IDの利用を停止することがあります。
                                </p>

                            <?php endif; ?>
                        <?php endif; ?>

                    </div>


                    <div class="itemContentText">
                        <?php echo nl2br(htmlentities($item->remarks)); ?>
                    </div>

                    <?php /*?>
                <section class="blockAccessWrap">
                <?php if (isLogin() && $item->owner_id != getUserId()): ?>
                <div class="text-right">
                <form id="block-user" method="post" onsubmit="return window.confirm('出品者をブロックする。'); ">
                  <input type="hidden" name="action" value="blockAccess" />
                  <input type="hidden" name="item_id" value="<?= $item->id ?>" />
                  <a href="#" onclick="submitForm('block-user');">
                  <i class="fa fa-times" aria-hidden="true"></i> この出品者をブロックする</a>
                </form>
                </div>
                <?php endif; ?>
                </section>
                <?php */ ?>

                    <?php if ($item->order_id == null): ?>
                        <?php //if ($item->order_id != null && ($item->buyer_id == getUserId() || $item->owner_id == getUserId())): ?>
                        <?php if ($item->order_id != null && ($item->buyer_id == getUserId())): ?>
                            <div class="row">
                                <div class="col-sm-6">
                                    <table class="table table-bordered">
                                        <thead class="thead-light">
                                        <tr>
                                            <th colspan="2" class="bgGray">出品者情報</th>
                                        </tr>
                                        </thead>
                                        <tbody class="userinfo">
                                        <tr>
                                            <th>ID</th>
                                            <td>
                                                <?= $owner->getViewId() ?>
                                            </td>
                                        </tr>
                                        <tr>
                                            <th>氏名</th>
                                            <td>
                                                <?= $owner->name ?>
                                            </td>
                                        </tr>
                                        <!--                                <tr>-->
                                        <!--                                    <th>郵便番号</th>-->
                                        <!--                                    <td>-->
                                        <!--                                        --><?php //echo $owner->zip; ?>
                                        <!--                                    </td>-->
                                        <!--                                </tr>-->
                                        <!--                                <tr>-->
                                        <!--                                    <th>住所</th>-->
                                        <!--                                    <td>-->
                                        <!--                                        --><?php //echo $owner->addr; ?>
                                        <!--                                    </td>-->
                                        <!--                                </tr>-->
                                        <!--                                <tr>-->
                                        <!--                                    <th>電話番号</th>-->
                                        <!--                                    <td>-->
                                        <!--                                        --><?php //echo $owner->tel; ?>
                                        <!--                                    </td>-->
                                        <!--                                </tr>-->
                                        <!--                                <tr>-->
                                        <!--                                    <th>メール</th>-->
                                        <!--                                    <td>-->
                                        <!--                                        --><?php //echo $owner->email; ?>
                                        <!--                                    </td>-->
                                        <!--                                </tr>-->
                                        </tbody>
                                    </table>
                                </div>
                                <div class="col-sm-6">
                                    <?php
                                    $buyer = new User();
                                    $buyer->select($item->buyer_id);
                                    ?>
                                    <table class="table table-bordered">
                                        <thead class="thead-light">
                                        <tr>
                                            <th colspan="2" class="bgGray">購入者情報</th>
                                        </tr>
                                        </thead>
                                        <tbody class="userinfo">
                                        <tr>
                                            <th>ID</th>
                                            <td>
                                                <?= $buyer->getViewId() ?>
                                            </td>
                                        </tr>
                                        <tr>
                                            <th>氏名</th>
                                            <td>
                                                <?= $buyer->name ?>
                                            </td>
                                        </tr>
                                        <!--                                <tr>-->
                                        <!--                                    <th>郵便番号</th>-->
                                        <!--                                    <td>-->
                                        <!--                                        --><?php //echo $buyer->zip; ?>
                                        <!--                                    </td>-->
                                        <!--                                </tr>-->
                                        <!--                                <tr>-->
                                        <!--                                    <th>住所</th>-->
                                        <!--                                    <td>-->
                                        <!--                                        --><?php //echo $buyer->addr; ?>
                                        <!--                                    </td>-->
                                        <!--                                </tr>-->
                                        <!--                                <tr>-->
                                        <!--                                    <th>電話番号</th>-->
                                        <!--                                    <td>-->
                                        <!--                                        --><?php //echo $buyer->tel; ?>
                                        <!--                                    </td>-->
                                        <!--                                </tr>-->
                                        <!--                                <tr>-->
                                        <!--                                    <th>メール</th>-->
                                        <!--                                    <td>-->
                                        <!--                                        --><?php //echo $buyer->email; ?>
                                        <!--                                    </td>-->
                                        <!--                                </tr>-->
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        <?php endif; ?>

                        <?php if (isLogin() && getUserId() != $owner->id && isset($_SESSION['negotiation']) && $_SESSION['negotiation'] == $item->id): ?>

                            <?php if (!User::isInvalid($item->owner_id) && ($item->order_id == null || $item->buyer_id == getUserId())): ?>
                                <form action="./user/negotiation.php?item_id=<?= $item->id ?>&user_id=<?= getUserId() ?>"
                                      method="post">
                                    <input type="hidden" name="item_id" value="<?= $item->id ?>"/>
                                    <input type="hidden" name="user_id" value="<?= getUserId() ?>"/>
                                    <input type="hidden" name="kbn" value="1"/>
                                    <input type="hidden" name="action" value="comment"/>
                                    <textarea class="form-control" rows="5" name="comment"
                                              placeholder="コメントを入力してください"></textarea>
                                    <div class="margin-20 text-center">
                                        <?php if ($item->buyer_id == getUserId()): ?>
                                            <input class="btn btn-block btn-info" type="submit"
                                                   value="取引連絡のコメントする"/>

                                        <?php else: ?>
                                            <input class="btn btn-block btn-info" type="submit"
                                                   value="質問・交渉でコメントする"/>
                                        <?php endif; ?>
                                    </div>
                                </form>
                            <?php endif; ?>

                        <?php endif; ?>
                    <?php endif; ?>
                </div>
            </div>
            <div class="col-md-3 sideContents">
                <?php include('sidebar.php'); ?>
            </div>
        </div>
    </div>


<?php include('footer.php'); ?>