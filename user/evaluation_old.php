<?php include_once(__DIR__ . '/../common/util.php'); ?>
<?php include_once(__DIR__ . '/../common/login_check.php'); ?>
<?php
$order = new Order();
$order->select($_GET['id']);
if ($order->owner_id == getUserId()) {
    if (BlockAccess::isBlock($order->user_id)) {
        setMessage('このユーザーは評価できません。', true);
        header('Location: ' . getContextRoot() . '/');
        exit;
    }
} else {
    if (BlockAccess::isBlock($order->owner_id)) {
        setMessage('このユーザーは評価できません。', true);
        header('Location: ' . getContextRoot() . '/');
        exit;
    }
}

if (isset($_POST['action'])) {
    if ($_POST['action'] == 'evaluate') {
        $order = new Order();
        $order->select($_POST['id']);
        if (isset($_POST['buyer_evaluate'])) {
            $order->buyer_evaluate = $_POST['buyer_evaluate'];
            $order->buyer_comment = $_POST['buyer_comment'];
            $order->registEvaluateByBuyer();
        } elseif (isset($_POST['seller_evaluate'])) {
            $order->seller_evaluate = $_POST['seller_evaluate'];
            $order->seller_comment = $_POST['seller_comment'];
            $order->registEvaluateBySeller();
        } else {
            setMessage('不正なアクションです。');
            header('Location: ' . getContextRoot() . '/user/mypage.php');
            exit;
        }

        setMessage('評価を登録しました。');
    }
    $order->select($_GET['id']);
}

$item = new Item();
$item->select($order->item_id);
$owner = new User();
$owner->select($order->owner_id);
$evaluate = new Evaluate();
$evaluate->selectSummary($item->owner_id);
$evaluateBuyer = new Evaluate();
$evaluateBuyer->selectSummary($order->user_id);

$user = new User();
$user->select($order->user_id);

$countFavorites = $item->selectCountFavorites();
?>

<?php include(__DIR__ . '/../header.php'); ?>
    <div class="container">
        <div class="row">
            <div class="col-md-3 sideContents">
                <?php include('usersidebar.php'); ?>
            </div>
            <div class="col-md-9 mainContents">
                <div class="bg-inner">
                    <h1>評価する</h1>
                    <div class="itemTitle">
                        <h2><?= $item->title ?></h2>
                    </div>
                    <div class="row">
                        <?php include_once('../common/parts/item_image_area.php'); ?>
                        <?php include_once('../common/parts/item_info_area.php'); ?>
                    </div>
                    <div class="mb-4">
                        <div class="text-center">
                            <span class="font30 fontBold fontRed"><?= number_format($item->price) ?></span>
                            <span class="smallEX">円(税込<?= $item->carriage_plan_name ?>)</span>
                        </div>
                    </div>
                    <div class="evaluate">
                        <h2 class="bg-light p-2 fontBold">
                            <?php /*?><?= ($order->user_id == getUserId()) ? '出品者　'.$owner->name : '購入者　'.$order->user_name ?><?php */ ?>
                            <?= ($order->user_id == getUserId()) ? '出品者　' . $owner->hash_id .' '.$owner->getViewNicknameOrId() : '購入者　' . $user->hash_id . ' ' . $user->getViewNicknameOrId() ?>
                            を評価する</h2>
                        <form method="post" onsubmit="return window.confirm('評価します。');">
                            <?php if ($order->user_id != getUserId()): ?>
                                <div class="mb-2">
                                    <?php createRadio('selectEvaluate', 'buyer_evaluate', $order->buyer_evaluate); ?>
                                </div>
                                <textarea class="form-control mb-3" rows="5" name="buyer_comment"
                                          placeholder="コメントを入力してください"><?= $order->buyer_comment ?></textarea>
                                <div>
                                    <input type="hidden" name="id" value="<?= $order->id ?>"/>
                                    <input type="hidden" name="action" value="evaluate"/>
                                    <input class="btn btn-block btn-info" type="submit" name="regist"
                                           value="評価を登録する"/>
                                </div>

                            <?php else: ?>
                                <div class="mb-2">
                                    <?php createRadio('selectEvaluate', 'seller_evaluate', $order->seller_evaluate); ?>
                                </div>
                                <textarea class="form-control mb-3" rows="5" name="seller_comment"
                                          placeholder="コメントを入力してください"><?= $order->seller_comment ?></textarea>
                                <div>
                                    <input type="hidden" name="id" value="<?= $order->id ?>"/>
                                    <input type="hidden" name="action" value="evaluate"/>
                                    <input class="btn btn-block btn-info" type="submit" name="regist"
                                           value="評価を登録する"/>
                                </div>
                            <?php endif; ?>
                        </form>
                    </div>
                    <div class="itemContentText">
                        <?php echo nl2br(htmlentities($item->remarks)); ?>
                    </div>
                </div>
            </div>
        </div>
    </div>
<?php include('../footer.php'); ?>