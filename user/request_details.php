<?php include_once(__DIR__ . '/../common/util.php'); ?>
<?php include_once(__DIR__ . '/../common/login_check.php'); ?>
<?php
$req = new Request();
$req->select($_GET['id']);

$follow = new RequestFollow();
$follows = $follow->selectRequest($req->id);

$user_evaluate = new Evaluate();
$user_evaluate->selectSummary($req->user_id);

$title_page = '購入希望詳細';
?>
<?php include(__DIR__ . '/../user_header.php'); ?>

    <div class="com-container bg-yellow">
        <?php include('usersidebar.php'); ?>

        <div class="com-content">
            <div class="bg-inner">
                <h1>購入希望詳細</h1>
                <div class="information_details_contents">
                    <div class="small"><?= $req->update_date ?>
                        購入希望者ID：<?php echo getHashKey(); ?>　<a
                                href="<?= getContextRoot(); ?>/evaluate.php?user_id=<?= getUserId() ?>">評価</a>：<?= $user_evaluate->point ?>
                    </div>
                    <div class="mb-3">
                        <?php echo nl2br(htmlentities($req->text)); ?>
                    </div>
                    <div class="font20 fontBold text-center">
                        予算：<?= number_format($req->price) ?> 円
                    </div>
                </div>
                <table class="table table-bordered">
                    <tbody>
                    <tr>
                        <th>紹介日</th>
                        <th>紹介された商品</th>
                        <th>紹介者</th>
                    </tr>
                    <?php foreach ($follows as $follow): ?>
                        <?php
                        if (User::isInvalid($follow->user_id)) {
                            continue;
                        }
                        $user = new User();
                        $user->select($follow->user_id);
                        ?>
                        <tr>
                            <td class="text-right"><?= date('Y-m-d H:i:s', strtotime($follow->regist_date)) ?></td>
                            <td>
                                <a href="negotiation.php?item_id=<?= $follow->item_id ?>&user_id=<?= getUserid() ?>"><?= $follow->title ?></a>
                            </td>
                            <td><?= $user->getViewId() ?></td>
                        </tr>
                    <?php endforeach; ?>
                    </tbody>
                </table>
            </div>
        </div>
    </div>

<?php include('../footer.php'); ?>




