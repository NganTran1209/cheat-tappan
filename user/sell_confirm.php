<?php 
$title_page = '登録内容の確認';
$description_page = '欲しい商品を画像で確認・届いた商品を画像で照合！気持ち良い取引が叶うオンラインマーケット。';
 ?>
<?php include_once(__DIR__ . '/../common/util.php'); ?>
<?php include_once(__DIR__.'/../common/login_check.php'); ?>
<?php
//ページタイトルはここで設定させる
$user_page_title = "登録内容の確認";

$isOffer = isset($_SESSION['targetRequest']) && !empty($_SESSION['targetRequest']);

$sell_item = $_SESSION['item'];

if (isset($_POST['action']) && $_POST['action'] == 'regist') {
    if ($isOffer) {
        $req = $_SESSION['targetRequest'];
        $sell_item->target_user_id = $req->user_id;
    }
    $sell_item->regist();
    //$sell_item->removeTmp();
    unset($_SESSION['item']);
    setMessage('商品を登録しました。商品の確認、承認後に出品されます。<br>※出品後メール通知されます。<br>※出品不可の場合、メール通知されません。');
    if ($isOffer) {
        $req = $_SESSION['targetRequest'];
        $req->isNewEntry = true;
        header('Location: ' . getContextRoot() . '/user/offerlist.php');
        exit();
    }
    //header('Location: '.getContextRoot().'/user/sell.php');
    header('Location: ' . getContextRoot() . '/user/mypage.php');
    exit();
}

if($sell_item != null){
    $sell_item->repairName();
    $system_config = SystemConfig::select();
}else{
    setMessage('登録データが足りません。販売用の商品を再登録してください。');
    header('Location: '.getContextRoot().'/user/sell.php');
    exit();
}

$title_page = '出品する';
?>
<?php include(__DIR__ . '/../user_header.php'); ?>


<div class="com-container bg-yellow">
    <?php include('usersidebar.php'); ?>

    <div class="com-content">
            <div class="content-title">
                <h3><span>出品する</span></h3>
            </div>

            <div class="sell-content">

                <div class="bg-inner">
                    <h1>出品する内容の確認</h1>
                    <div class="border-bottom py-4 add-product-contents">
                        <h2 class="bg-light p-2 fontBold my-3"><?= ITEM_NAME ?>の情報</h2>
                        <h3 class="fontBold my-2"><?= ITEM_NAME ?>画像</h3>
                        <div class="row">
                            <div class="col-md-3 mb-2"><img class="img-fluid" src="user_photo.php?id=1" alt=""/></div>
                            <div class="col-md-3 mb-2"><img class="img-fluid" src="user_photo.php?id=2" alt=""/></div>
                            <div class="col-md-3 mb-2"><img class="img-fluid" src="user_photo.php?id=3" alt=""/></div>
                            <div class="col-md-3 mb-2"><img class="img-fluid" src="user_photo.php?id=4" alt=""/></div>
                            <div class="col-md-3 mb-2"><img class="img-fluid" src="user_photo.php?id=5" alt=""/></div>
                            <div class="col-md-3 mb-2"><img class="img-fluid" src="user_photo.php?id=6" alt=""/></div>
                            <div class="col-md-3 mb-2"><img class="img-fluid" src="user_photo.php?id=7" alt=""/></div>
                            <div class="col-md-3 mb-2"><img class="img-fluid" src="user_photo.php?id=8" alt=""/></div>
                            <div class="col-md-3 mb-2"><img class="img-fluid" src="user_photo.php?id=9" alt=""/></div>
                            <div class="col-md-3 mb-2"><img class="img-fluid" src="user_photo.php?id=10" alt=""/></div>
                        </div>
                    </div>
                    <div class="border-bottom py-4 add-product-contents">
                        <h2 class="bg-light p-2 fontBold add-product-detail"><?= ITEM_NAME ?>の詳細</h2>
                        <h3 class="fontBold add-product-detail">タイトル</h3>
                        <p class="c-cofirm"><?= $sell_item->title ?></p>
                        <h3 class="fontBold my-2 add-product-detail"><?= ITEM_NAME ?>の説明文</h3>
                        <?php /*?><pre><?= $sell_item->remarks ?></pre><?php */ ?>
                       <p class="c-cofirm"><?php echo nl2br(htmlentities($sell_item->remarks)); ?></p>

                        <?php if ($system_config->enabled_category):?>
                            <h3 class="fontBold my-2 add-product-detail">カテゴリー</h3>
                           <p class="c-cofirm"> <?= $sell_item->category_name ?></p>
                        <?php endif;?>

                        <?php if ($system_config->enabled_item_state):?>
                            <h3 class="fontBold my-2 add-product-detail"><?= ITEM_NAME ?>の状態</h3>
                          <p class="c-cofirm">  <?= $sell_item->item_state_name ?></p>
                        <?php endif;?>

                        <?php foreach(FreeInputItem::selectAll() as $item1):?>
                            <?php if (!$item1->enabled) {continue;}?>
                            <h3 class="fontBold my-2"><?= $item1->name ?></h3>
                            <p class="c-cofirm"> <?= FreeInputItemValue::select($sell_item->free_input[$item1->id]->free_input_item_value_id)->name ?></p>
                        <?php endforeach; ?>
                    </div>
<div class="border-bottom py-4 add-product-contents">
                        <h2 class="bg-light p-2 fontBold my-3">配送の設定</h2>
                        <?php if ($system_config->enabled_pref):?>
                            <h3 class="fontBold my-2">地域</h3>
                            <p class="c-cofirm">  <?= $sell_item->pref_name ?></p>
                           
                        <?php endif;?>

                        <p class="c-cofirm"><?= $sell_item->with_delivery ? '配送あり' : '配送なし' ?></p>
                        <?php if ($sell_item->with_delivery): ?>
                            <?php if ($system_config->enabled_delivery_type):?>
                                <h3 class="fontBold my-2">配送方法</h3>
                                <p class="c-cofirm">  <?= $sell_item->delivery_type_name ?></p>
                            <?php endif;?>

                            <?php if ($system_config->enabled_postage):?>
                                <h3 class="fontBold my-2">配送料の負担</h3>
                                <p class="c-cofirm">  <?= $sell_item->carriage_plan_name ?></p>
                            <?php endif;?>

                            <?php if ($system_config->enabled_carriage_type):?>
                                <h3 class="fontBold my-2">配送の目安</h3>
                                <p class="c-cofirm"> <?= $sell_item->carriage_type_name ?></p>
                            <?php endif;?>
                        <?php endif;?>
                        <?php /*?>
                       <h3 class="fontBold my-2">配送料金</h3>
                       <?= number_format($sell_item->delivery_price) ?>円
                    <?php */ ?>
                    </div>
                    <div class="border-bottom py-4 add-product-contents">
                        <h2 class="bg-light p-2 fontBold my-3">価格(300円～9,999,999円)</h2>
                        <h3 class="fontBold my-2">価格</h3>
                        <p class="c-cofirm"><?= number_format($sell_item->price) ?>円</p>
                        <!-- ▼ 評価の条件表示 -->
                        <!--                            --><?php //if ($sell_item->show_evaluate): ?>
                        <!--                            <p>評価が0以上のユーザーのみ対象にする</p>-->
                        <!--                            --><?php //else: ?>
                        <!--                            <p>すべてのユーザーを対象にする</p>-->
                        <!--                            --><?php //endif;?>
                        <!-- ▲ 評価の条件表示 -->
                    </div>
                    <div class="row py-4">
                        <div class="col-md-6 mb-3">
                            <form method="post">
                                <?php if ($isOffer): ?>
                                    <input type="hidden" name="offer" value="offer"/>
                                <?php endif; ?>
                                <input type="hidden" name="action" value="regist"/>
                                <input class="btn btn-block btn-info btn-sm product-confirm-add-2" type="submit" value="登録する"/>
                            </form>
                        </div>
                        <div class="col-md-6 mb-3">
                            <form method="post" action="sell.php">
                                <?php if ($isOffer): ?>
                                    <input type="hidden" name="offer" value="offer"/>
                                <?php endif; ?>
                                <input type="hidden" name="action" value="modify"/>
                                <input class="btn btn-block btn-outline-dark btn-sm product-confirm-add" type="submit" value="修正する"/>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<?php include('../footer.php'); ?>
