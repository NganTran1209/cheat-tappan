<?php include_once(__DIR__.'/../common/util.php'); ?>
<?php include_once(__DIR__.'/../common/login_check.php'); ?>
<?php include_once(__DIR__.'/../common/stripe_api.php'); ?>
<?php
if (!isset($_POST['item_id'])) {
    setMessage('不正なアクセスです。');
    header('Location: ' . getContextRoot() . '/');
    exit;

}

$item = new Item();
$item->select($_POST['item_id']);

$order = new Order();
$order->select($item->order_id);

$order->updateOrderForBankTransfer();

$amount = $item->price;

$title_page = '銀行振込';
$system_config = SystemConfig::select();
?>
<?php include(__DIR__.'/../user_header.php'); ?>
<div class="container">
    <div class="row">
        <?php include('usersidebar.php'); ?>
        <div class="col-md-9 mainContents">
            <div class="bg-inner">
                <h1 itemprop="headline" class="mb-4 bg-light p-4">振込先の案内をメールしました</h1>

                <div class="alert alert-success text-center font15 my-5" role="alert">
                    <div class="alert-heading"><i class="fa fa-reply"></i> メールを送信しました</div>
                </div>

                <h2 class="bg-light p-2 fontBold">お支払予定の金額</h2>
                <div class="text-center p-4 mb-4">
                    <div class="fontBold">
                        ご購入金額：<span class="font25"><?= $amount ?></span>円
                    </div>
                </div>

                <h3 class="fontBold">振込先の口座</h3>
                <div class="mb-5">
                    <div>金融機関名：<?= $system_config->financial_institution_name ?></div>
                    <div>支店名：<?= $system_config->branch_name ?></div>
                    <div>預金種別：<?= $system_config->get_deposit_type_name() ?></div>
                    <div>口座番号：<?= $system_config->account_number ?></div>
                    <div>口座名義：<?= $system_config->account_holder ?></div>
                </div>

                <div class="card mb-5">
                    <div class="card-body">
                        <h3 class="card-title"><i class="fa fa-exclamation-triangle"></i> メールボックスのゴミ箱・迷惑メールボックスもご確認下さい</h3>
                        <p class="fontBold">※5分以内に確認メールを1通送ります。</p>
                        <p class="fontRed mb-0">※メールが届かない場合は迷惑メールフォルダ、ゴミ箱などをご確認下さい。</p>
                    </div>
                </div>
                <div>
                    <p class="fontBold">次に<span class="fontRed">「購入済み」</span>の中の商品タイトルを選び、画面下の<span class="fontRed">「お振込完了のご連絡」</span>ボタンを必ず押してください。出品者に通知されます。<span class="fontRed"><a href="negotiation.php?item_id=<?= $item->id ?>&user_id=<?= $item->buyer_id ?>">ココから</a></span></p>
                </div>
            </div>
        </div>
    </div>
</div>
<?php include('../user_footer.php'); ?>








