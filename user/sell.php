<?php 
$title_page = '商品を登録する';
$description_page = '欲しい商品を画像で確認・届いた商品を画像で照合！気持ち良い取引が叶うオンラインマーケット。';
 ?>
<?php include_once(__DIR__ . '/../common/util.php'); ?>
<?php include_once(__DIR__.'/../common/login_check.php'); ?>
<?php
function console_log($output, $with_script_tags = true) {
    $js_code = 'console.log(' . json_encode($output, JSON_HEX_TAG) . 
');';
    if ($with_script_tags) {
        $js_code = '<script>' . $js_code . '</script>';
    }
    echo $js_code;
}
?>
<?php
//ページタイトルはここで設定させる
$user_page_title = "商品を登録する";

//if (!isSelling()) {
//    setMessage('出品が許可されておりません。出品許可を得るためには「本人確認書類」の提出をお願いします。');
//    header('Location: ' . getContextRoot() . '/user/upload_identification.php');
//    exit;
//}
//if (!isIdentification()) {
//    setMessage('本人確認が完了しておりません。');
//    header('Location: ' . getContextRoot() . '/user/upload_identification.php');
//    exit;
//}

$isOffer = false;
$gaziru1Check = false;
$gaziru2Check = false;
$gaziru3Check = false;
$takePhoto1 = false;

if (isset($_POST['offer']) && $_POST['offer'] == 'offer') {
    $isOffer = true;
} else {
    unset($_SESSION['targetRequest']);
}
$sell_item = new Item();
if (isset($_POST['action']) && ($_POST['action'] == 'modify' || $_POST['action'] == 'cancel-load-tmp')) {
    $sell_item = $_SESSION['item'];
} elseif (isset($_POST['action']) && $_POST['action'] == 'load-tmp') {
    $sell_item = new Item();
    $sell_item->selectTmp($_POST['tmp-id']);
    setMessage('下書きを読み込みました。', true);
} elseif (isset($_POST['action']) && $_POST['action'] == 'save-tmp' && empty($_POST['title'])) {
    echo json_encode('下書き登録はタイトルは必須です。');
    exit();
} else if (isset($_POST['title']) || isset($_POST['action'])) {
//  ini_set('display_errors', 0);
    $sell_item->title = trim($_POST['title']);
    $sell_item->category = empty($_POST['category']) ? null : $_POST['category'];
    $sell_item->item_state = isset($_POST['item_state']) ? $_POST['item_state'] : null;
    // $sell_item->item_state = $_POST['item_state'] == "未選択"
    $sell_item->remarks = trim($_POST['remarks']);
    $sell_item->pref = empty($_POST['pref']) ? null : $_POST['pref'];
    $sell_item->carriage_plan = empty($_POST['carriage_plan']) ? null : $_POST['carriage_plan'];
    $sell_item->carriage_type = empty($_POST['carriage_type']) ? null : $_POST['carriage_type'];
    $sell_item->delivery_type = empty($_POST['delivery_type']) ? null : $_POST['delivery_type'];
    $sell_item->show_evaluate = empty($_POST['show_evaluate']) ? null : $_POST['show_evaluate'];
    $sell_item->with_delivery = empty($_POST['with_delivery']) ? null : $_POST['with_delivery'];
    //$sell_item->delivery_price= $_POST['delivery_price'];
    $sell_item->delivery_price = 0;
    $sell_item->price = empty($_POST['price']) ? null : $_POST['price'];
    $sizeLimit = 1024;
    
    if(!empty($_FILES['gaziruPhoto1']) && $_FILES['gaziruPhoto1']['error'] == 0) {
        $_FILES['gaziruPhoto1'] = changeFileType($_FILES['gaziruPhoto1']);
        if (getimagesize($_FILES['gaziruPhoto1']['tmp_name'])[1] > $sizeLimit){
            compress($_FILES['gaziruPhoto1']['tmp_name'],100);
        }
        $registPhoto1 = $sell_item->registerRequestGaziru(curl_file_create($_FILES['gaziruPhoto1']['tmp_name'], $_FILES['gaziruPhoto1']['type'], basename($_FILES['gaziruPhoto1']['name'])));
        $sell_item->photoGaziru1 = json_decode($registPhoto1,true)["objectId"];
        $sell_item->imageAuthen1 = rotateImage(file_get_contents($_FILES['gaziruPhoto1']['tmp_name']));
    }else{
        if(!empty($_POST['check-gaziru-photo1'])){
            $sell_item->photoGaziru1 = $_SESSION['item']->photoGaziru1;
            $sell_item->imageAuthen1 = $_SESSION['item']->imageAuthen1;
        }
    }
    if(!empty($_FILES['gaziruPhoto2']) && $_FILES['gaziruPhoto2']['error'] == 0) {
        $_FILES['gaziruPhoto2'] = changeFileType($_FILES['gaziruPhoto2']);
        if (getimagesize($_FILES['gaziruPhoto2']['tmp_name'])[1] > $sizeLimit){
            compress($_FILES['gaziruPhoto2']['tmp_name'], 100);
        }
        $registPhoto2 = $sell_item->registerRequestGaziru(curl_file_create($_FILES['gaziruPhoto2']['tmp_name'], $_FILES['gaziruPhoto2']['type'], basename($_FILES['gaziruPhoto2']['name'])));
        $sell_item->photoGaziru2 = json_decode($registPhoto2,true)["objectId"];
        $sell_item->imageAuthen2 = rotateImage(file_get_contents($_FILES['gaziruPhoto2']['tmp_name']));
    }else{
        if(!empty($_POST['check-gaziru-photo2'])){
            $sell_item->photoGaziru2 = $_SESSION['item']->photoGaziru2;
            $sell_item->imageAuthen2 = $_SESSION['item']->imageAuthen2;
        }
    }
    
    if(!empty($_FILES['gaziruPhoto3']) && $_FILES['gaziruPhoto3']['error'] == 0) {
        $_FILES['gaziruPhoto3'] = changeFileType($_FILES['gaziruPhoto3']);
        if (getimagesize($_FILES['gaziruPhoto3']['tmp_name'])[1] > $sizeLimit){
            compress($_FILES['gaziruPhoto3']['tmp_name'], 100);
        }
        $registPhoto3 = $sell_item->registerRequestGaziru(curl_file_create($_FILES['gaziruPhoto3']['tmp_name'], $_FILES['gaziruPhoto3']['type'], basename($_FILES['gaziruPhoto3']['name'])));
        $sell_item->photoGaziru3 = json_decode($registPhoto3,true)["objectId"];
        $sell_item->imageAuthen3 = rotateImage(file_get_contents($_FILES['gaziruPhoto3']['tmp_name']));
    }else{
        if(!empty($_POST['check-gaziru-photo3'])){
            $sell_item->photoGaziru3 = $_SESSION['item']->photoGaziru3;
            $sell_item->imageAuthen3 = $_SESSION['item']->imageAuthen3;
        }
    }

    if (!empty($_FILES['photo1']) && $_FILES['photo1']['error'] == 0) {
        $sell_item->photo1 = rotateImage(file_get_contents($_FILES['photo1']['tmp_name']));
        $sell_item->photoCheck1 = $_FILES['photo1'];
    } else {
        if(!empty($_POST['check-photo1'])){
            $sell_item->photo1 = $_SESSION['item']->photo1;
        }else{
            $sell_item->photo1 = null;
        }
    }
    if (!empty($_FILES['photo2']) && $_FILES['photo2']['error'] == 0) {
        $sell_item->photo2 = rotateImage(file_get_contents($_FILES['photo2']['tmp_name']));
        $sell_item->photoCheck2 = $_FILES['photo2'];
    } else {
        if(!empty($_POST['check-photo2'])){
            $sell_item->photo2 = $_SESSION['item']->photo2;
        }else{
            $sell_item->photo2 = null;
        }
    }
    if (!empty($_FILES['photo3']) && $_FILES['photo3']['error'] == 0) {
        $sell_item->photo3 = rotateImage(file_get_contents($_FILES['photo3']['tmp_name']));
        $sell_item->photoCheck3 = $_FILES['photo3'];
    }  else {
        if(!empty($_POST['check-photo3'])){
            $sell_item->photo3 = $_SESSION['item']->photo3;
        }else{
            $sell_item->photo3 = null;
        }
    }
    if (!empty($_FILES['photo4']) && $_FILES['photo4']['error'] == 0) {
        $sell_item->photo4 = rotateImage(file_get_contents($_FILES['photo4']['tmp_name']));
        $sell_item->photoCheck4 = $_FILES['photo4'];
    } else {
        if(!empty($_POST['check-photo4'])){
            $sell_item->photo4 = $_SESSION['item']->photo4;
        }else{
            $sell_item->photo4 = null;
        }
    }
    if (!empty($_FILES['photo5']) && $_FILES['photo5']['error'] == 0) {
        $sell_item->photo5 = rotateImage(file_get_contents($_FILES['photo5']['tmp_name']));
        $sell_item->photoCheck5 = $_FILES['photo5'];
    } else {
        if(!empty($_POST['check-photo5'])){
            $sell_item->photo5 = $_SESSION['item']->photo5;
        }else{
            $sell_item->photo5 = null;
        }
    }
    if (!empty($_FILES['photo6']) && $_FILES['photo6']['error'] == 0) {
        $sell_item->photo6 = rotateImage(file_get_contents($_FILES['photo6']['tmp_name']));
        $sell_item->photoCheck6 = $_FILES['photo6'];
    } else {
        if(!empty($_POST['check-photo6'])){
            $sell_item->photo6 = $_SESSION['item']->photo6;
        }else{
            $sell_item->photo6 = null;
        }
    }
    if (!empty($_FILES['photo7']) && $_FILES['photo7']['error'] == 0) {
        $sell_item->photo7 = rotateImage(file_get_contents($_FILES['photo7']['tmp_name']));
        $sell_item->photoCheck7 = $_FILES['photo7'];
    } else {
        if(!empty($_POST['check-photo7'])){
            $sell_item->photo7 = $_SESSION['item']->photo7;
        }else{
            $sell_item->photo7 = null;
        }
    }
    if (!empty($_FILES['photo8']) && $_FILES['photo8']['error'] == 0) {
        $sell_item->photo8 = rotateImage(file_get_contents($_FILES['photo8']['tmp_name']));
        $sell_item->photoCheck8 = $_FILES['photo8'];
    } else {
        if(!empty($_POST['check-photo8'])){
            $sell_item->photo8 = $_SESSION['item']->photo8;
        }else{
            $sell_item->photo8 = null;
        }
    }
    if (!empty($_FILES['photo9']) && $_FILES['photo9']['error'] == 0) {
        $sell_item->photo9 = rotateImage(file_get_contents($_FILES['photo9']['tmp_name']));
        $sell_item->photoCheck9 = $_FILES['photo9'];
    } else {
        if(!empty($_POST['check-photo9'])){
            $sell_item->photo9 = $_SESSION['item']->photo9;
        }else{
            $sell_item->photo9 = null;
        }
    }
    if (!empty($_FILES['photo10']) && $_FILES['photo10']['error'] == 0) {
        $sell_item->photo10 = rotateImage(file_get_contents($_FILES['photo10']['tmp_name']));
        $sell_item->photoCheck10 = $_FILES['photo10'];
    } else {
        if(!empty($_POST['check-photo10'])){
            $sell_item->photo10 = $_SESSION['item']->photo10;
        }else{
            $sell_item->photo10 = null;
        }
    }

    if (!empty($_POST['free_items'])) {
        foreach ($_POST['free_items'] as $key => $value) {
            $free_info = new ItemFreeInfo();
            $free_info->free_input_item_id = $key;
            $free_info->free_input_item_value_id = $value;
            $sell_item->free_input[$key] = $free_info;
        }
    }

    if (isset($_POST['action']) && $_POST['action'] == 'validation') {
        $sell_item->validate();
        $result = getMessage();
        echo json_encode($result);
        exit();
    }

    if (isset($_POST['action']) && $_POST['action'] == 'move-load-tmp') {
        $_SESSION['item'] = $sell_item;
        header('Location: ' . getContextRoot() . '/user/draft_list.php');
        exit();
    }

    if (isset($_POST['action']) && $_POST['action'] == 'save-tmp') {
        $tmps = $sell_item->selectTmpFromUser(getUserId());
        if (count($tmps) < MAX_TEMP_COUNT) {
            $sell_item->registTmp();
            echo json_encode('下書きを保存しました。');
            exit();
        } else {
            echo json_encode('下書きは[' . MAX_TEMP_COUNT . '件]まで登録できます。');
            exit();
        }
    }

    if ($sell_item->validate()) {
        $_SESSION['item'] = $sell_item;

        header('Location: ' . getContextRoot() . '/user/sell_confirm.php');
        exit();
    }
} else {
    $sell_item = new Item();
    $sell_item->pref = getUserPref();
}
unset($_SESSION['item']);
$system_config = SystemConfig::select();
if (!isIdentification()) {
    header('Location: ' . getContextRoot() . '/user/upload_identification.php');
}

?>
<?php include(__DIR__ . '/../user_header.php'); ?>
<div class="com-header-top">
    <div class="com-header-top__img wow animate__animated animate__fadeInUp">
        <img src="<?php echo HOME_URL; ?>/common/assets/img/mypage/top-bg.png" alt="">
    </div>
    <div class="com-header-top__path wow animate__animated animate__fadeInUp">
        <p><span><a href="<?php echo HOME_URL; ?>/" class="clr-yel">トップページ</a></span><span> > </span><span><a href="<?php echo HOME_URL; ?>/user/mypage.php" class="clr-yel">マイページ</a></span><span> > </span><span>商品を登録する</span></p>
    </div>
<!--     <div class="com-header-top__txt">
        <p class="clr-white">マイページ</p>
    </div>-->
</div>


<script src="https://code.jquery.com/jquery-3.4.1.min.js"
            integrity="sha256-CSXorXvZcTkaix6Yvo6HppcZGetbYMGWSFlBw8HfCJo="
            crossorigin="anonymous"></script>
<!-- <script src="<?php echo HOME_URL; ?>/common/assets/js/jquery.min.js"></script> -->

<script>
    async function confirm(e) {
        
        if(validateRequiredItem().val ==false){
            e.preventDefault();
            window.alert(validateRequiredItem().msg);
            window.scrollTo(0,0);
            return false;
        }
        capture()
        var requestData = new FormData();
        var $f = $('#sell-form');
        var valueData = $f.serializeArray();
        $.each(valueData, function (key, input) {
            requestData.append(input.name, input.value);
        });
        requestData.append('action', 'validation');

        var fileData = $('input[name="photo1"]')[0].files;
        for (var i = 0; i < fileData.length; i++) {
            requestData.append("photo1", fileData[i]);
        }
        $('#mess-modal').modal('show');
        document.getElementById('parent-modal-mess').style.display = "block";
        document.getElementById('parent-modal-mess').style.background = "#0000008c";
        var result = $.ajax({
            url: './sell.php',
            type: 'post',
            async: false,
            processData: false,
            contentType: false,
            data: requestData
        }).responseText;

        var obj = JSON.parse(result);
        
        if (obj.msg.length > 0) {
            window.alert(obj.msg);
            $('#title_id').val("");
            $('#remarks_id').val("");
            $('#category_id').val("");
            $('#item_state_id').val("");
            $('#pref_id').val("");
            $('#delivery_type_id').val("");
            $('#carriage_plan_id').val("");
            $('#carriage_type_id').val("");
            $('#price_id').val("");
            $('#with_delivery').prop('checked', false); // Unchecks it
            $('#delivery_type_id').prop('disabled', true);
            $('#carriage_plan_id').prop('disabled', true);
            $('#carriage_type_id').prop('disabled', true);
            document.querySelectorAll('.custom-file-input').forEach(function(ele) {
                ele.value = "";
                var nextSibling = ele.target.nextElementSibling;
                nextSibling.innerText = "商品画像ファイルの登録";

                var previewDiv = document.getElementById(ele.target.id + '-preview');
                previewDiv.style.display = 'block';
                
            });

            // document.querySelectorAll('.uploadFile').forEach(function(ele) {
            //     ele.value = "";
            //     var previewDiv = document.getElementById(ele.target.id + '-preview');
            //     previewDiv.removeAttribute('src');
            // });

	        
            return false;
        }
        
        return true;
    }

    function saveTmp() {
        var requestData = new FormData();

        var $f = $('#sell-form');
        var valueData = $f.serializeArray();
        $.each(valueData, function (key, input) {
            requestData.append(input.name, input.value);
        });
        requestData.append('action', 'save-tmp');

        var fileData = $('input[name="photo1"]')[0].files;
        for (var i = 0; i < fileData.length; i++) {
            requestData.append("photo1", fileData[i]);
        }
        
        var result = $.ajax({
            url: './sell.php',
            type: 'post',
            async: false,
            processData: false,
            contentType: false,
            data: requestData
        }).responseText;

        var obj = JSON.parse(result);

        if (obj.length > 0) {
            window.alert(obj);
            return false;
        }
        return false;
    }

    function loadTmp() {
        submitForm('sell-form', 'action', 'move-load-tmp');
        var $f = $('#tmp-form').submit();
    }

    $(function() {
        $('#with_delivery').on('change', function() {
            var disabled = !$(this).prop('checked');
            $('#delivery_type_id').prop('disabled', disabled);
            $('#carriage_plan_id').prop('disabled', disabled);
            $('#carriage_type_id').prop('disabled', disabled);
        });
    });

    function validateRequiredItem(){
        var $msg = '';
        if ($('#title_id').val() === ""){
            $msg += '[タイトル]';
        }
        if ($('#check-photo1').val() == "" ){
            if($('#photo1').val() === ""){
                $msg += '[商品画像１]';
            }
        }
        if ($('#remarks_id').val() === ""){
            $msg += '[説明文]';
        }
        if ($('#category_id').val() === ""){
            $msg += '[カテゴリー]';
        }
        if ($('#item_state_id').val() === ""){
            $msg += '[商品の状態]';
        }
        if ($('#price_id').val() === ""){
            $msg += '[商品価格]';
        }
        if ($msg != "") {
            $msg += 'は必須項目です。';
            return {val: false, msg: $msg};
        }else {
            return {val: true, msg: ""};
        }
    }
</script>
<?php 
function compress($source, $quality) {
    $info = getimagesize($source);
    $width = $new_width = $info[0];
    $height = $new_height = $info[1];
    if ($info['mime'] == 'image/jpeg') 
        $image = imagecreatefromjpeg($source);
    elseif ($info['mime'] == 'image/gif') 
        $image = imagecreatefromgif($source);
    elseif ($info['mime'] == 'image/png') 
        $image = imagecreatefrompng($source);
    else 
        $image = imagecreatefromjpeg($source);
    
    $new_width = 1024;
    $new_height = 1024;
    $new_image = imagecreatetruecolor($new_width, $new_height);
    imagecopyresampled($new_image, $image, 0, 0, 0, 0, $new_width, $new_height, $width, $height);  

    $exif = exif_read_data($source);
    if(!empty($exif['Orientation'])) {
        switch($exif['Orientation']) {
            case 8:
                $image = imagerotate($image,90,0);
                break;
            case 3:
                $image = imagerotate($image,180,0);
                break;
            case 6:
                $image = imagerotate($image,-90,0);
                break;
        } 
    }

    imagejpeg($new_image, $source, $quality);
}

function changeFileType($file){
    $info = getimagesize($file['tmp_name']);
    if ($info['mime'] == 'image/jpeg') 
        $image = imagecreatefromjpeg($file['tmp_name']);
    elseif ($info['mime'] == 'image/gif') 
        $image = imagecreatefromgif($file['tmp_name']);
    elseif ($info['mime'] == 'image/png') 
        $image = imagecreatefrompng($file['tmp_name']);
    else 
        $image = imagecreatefromjpeg($file['tmp_name']);
        
    imagejpeg($image,$file['tmp_name'], 100);
    $file['name'] = pathinfo($file['name'])['filename'] . '.jpg';
    $file['type'] = 'image/jpeg';
    return $file;
}
?>
<script>
    $(document).ready(function(){
        var checkMarker = document.getElementById('check-gaziru-photo3').value;
        if(checkMarker != ""){
            var marker = document.createElement("img");
                marker.setAttribute("class", "image-marker overlay");
                marker.setAttribute("id", "image-marker");
                marker.setAttribute("src", "../common/assets/img/marker.png");
                document.getElementById("image-marker-div").appendChild(marker);
                dragElement(marker);
        }

    });
</script>
<div class="com-container bg-yellow">
    <?php include('usersidebar.php'); ?> 
        <div class="com-content">
            <form class="c-form01" id="sell-form" method="post" enctype="multipart/form-data" onsubmit="return confirm(event);">
                <div class="content-title">
                    <h3><span>商品を登録する</span></h3>
                </div>
                <div class="gaziru-content">
                    <div class="gaziru-info">
                        <h3>出品画像、商品指紋認証用の画像は必須登録です。</h3>
                        <div class="gaziru-description">
                            <p>
                                以下の画像のタイプに沿った写真を撮影し、アップロードをしてください。<br />
                                購入後、お客様が同様の画像を撮影し、商品の真贋を比較します。<br />
                                また、画像が適さない場合には、出品が許可されない場合があるためご注意ください。
                            </p>
                        </div>
                        <div class="gaziru-files">
                            <div class="row">
                                <div class="col text-center imgUp">
                                    <div class="imgGaziruPreview">
                                        <?php 
                                            if(!empty($sell_item->imageAuthen1)){
                                        ?>                                                                 
                                            <img class="img img-responsive full-width" id="gaziru-photo1-preview" src="user_photo_gaziru.php?id=1">
                                        <?php } else { ?>
                                            <img class="img img-responsive full-width" id="gaziru-photo1-preview" >
                                        <?php }?>
                                            <input type="text" id="check-gaziru-photo1" name="check-gaziru-photo1"  value="<?= $sell_item->photoGaziru1 ?>" hidden>
                                    </div>
                                    <!-- <label class="btn btn-gaziru-upload"> -->
                                        <!-- 写真を撮る -->
                                        <!-- <input type="file" class="uploadFile img" id="gaziruPhoto1" name="gaziruPhoto1" value="Upload Photo" style="width: 0px;height: 0px;overflow: hidden;"> -->
                                    <!-- </label> -->
                                    <!-- <input type="button" class="btn btn-secondary btn-block" value="写真を撮る" data-toggle="modal" data-target="#gaziru-photo1-modal">
                                    
                                    <input type="button" class="btn btn-secondary btn-block" value="入力をクリア" onclick="removeGaziruImage('gaziru-photo1')"> -->
                                    <div class="m-auto text-center">
                                        <div class="d-flex justify-content-between flex-lg-row flex-column" >
                                            <button class="btn btn-secondary col-lg-6 mr-lg-2 my-2" onclick="return uploadPhoto('gaziru-photo1')" data-toggle="modal" type="button" style="font-size: 1.4rem;">商品画像アップロード</button>
                                            <button class="btn btn-secondary col-lg-5 ml-lg-2 my-2" onclick="return takePhoto('gaziru-photo1')" data-toggle="modal" data-target="#gaziru-photo1-camera-modal" type="button" style="font-size: 1.4rem">写真を撮る</button>
                                        </div>
                                       
                                        <button class="btn btn-secondary btn-block col-md-12 "  onclick="removeGaziruImage('gaziru-photo1')"   type="button" style="font-size: 1.4rem;">入力をクリア</button>
                                    </div>
                                    <h1>全体画像</h1>
                                    <p>商品全体を撮影して画像をアップロードしてください。</p>

                                    <div id="gaziru-photo1-modal" class="modal fade show <?= ($gaziru1Check==true) ? 'md-show' : '' ?>" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-modal="true" style="z-index:999999">
                                        <div class="modal-dialog">
                                            <div class="modal-content p-3">
                                                <div class="modal-header">
                                                    <h2 class="text-center font-weight-bold w-100">認証用画像</h2>
                                                    <button type="button" class="btn-close" data-dismiss="modal" aria-label="Close" onclick="closePhoto('gaziru-photo1')">X</button>
                                                </div>
                                                
                                                <video id="gaziru-photo1-source" muted autoplay playsinline style="display: none"></video>
                                                <div class="imgGaziruPreviewCanvas mt-5">
                                                    <canvas id="gaziru-photo1-canvas" width=1024 height=1024 class="gaziruCanvasUpload img img-responsive full-width"></canvas>
                                                </div>

                                                <div class="mt-5 gaziruOptions">
                                                    <div class="row mt-5 mb-3">
                                                        <div class="col" id="gaziru-photo1-btn-upload">
                                                            <input type="button" id="gaziru-photo1-upload-btn" class="btn btn-secondary btn-block" value="アップロード" onclick="uploadPhoto('gaziru-photo1')">
                                                            <input type="file" class="uploadFile img" id="gaziru-photo1" name="gaziruPhoto1" value="Upload Photo" style="width: 0px;height: 0px;overflow: hidden;">
                                                        </div>
                                                        <div class="col" id="gaziru-photo1-btn-capture">
                                                            <input type="button" class="btn btn-secondary btn-block" value="写真を撮る" onclick="takePhoto('gaziru-photo1')">
                                                        </div>
                                                        <div class="col" id="gaziru-photo1-btn-submit">
                                                            <input type="button" class="btn btn-secondary btn-block" value="追加" onclick="confirmPhoto('gaziru-photo1')">
                                                        </div>        
                                                    </div>                                
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                        <!-- MODAL FOR CAMERA -->
                                    <div id="gaziru-photo1-camera-modal" class="modal fade show <?= ($gaziru1Check==true) ? 'md-show' : '' ?>" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-modal="true" style="z-index:999999">
                                        <div class="modal-dialog">
                                            <div class="modal-content p-3">
                                                <div class="modal-header">
                                                    <h2 class="text-center font-weight-bold w-100">全体画像</h2>
                                                    <button type="button" class="btn-close" data-dismiss="modal" aria-label="Close" onclick="closePhoto('gaziru-photo1-camera')">X</button>
                                                </div>
                                                
                                                <video id="gaziru-photo1-camera-source" muted autoplay playsinline style="display: none"></video>
                                                
                                                <div class="imgGaziruPreviewCanvas mt-5">
                                                    <canvas id="gaziru-photo1-camera-canvas" width=1024 height=1024 class="gaziruCanvasUpload img img-responsive full-width"></canvas>
                                                </div>
                                                <input type="range" class="zoom-range" id="zoom-range-1" hidden>
                                                <div class="mt-5 gaziruOptions">
                                                    <div class="row mt-5 mb-3">
                                                        <div class="col" id="gaziru-photo1-btn-upload" style="display:none">
                                                            <input type="button" id="gaziru-photo1-upload-btn" class="btn btn-secondary btn-block" value="アップロード" onclick="uploadPhoto('gaziru-photo1')">
                                                            <!-- <input type="file" class="uploadFile img" id="gaziru-photo1-camera" name="gaziruPhoto1" value="Upload Photo" style="width: 0px;height: 0px;overflow: hidden;"> -->
                                                        </div>
                                                        <div class="col" id="gaziru-photo1-camera-btn-capture">
                                                            <input type="button" class="btn btn-secondary btn-block" value="写真を撮る" onclick="takePhoto('gaziru-photo1')">
                                                        </div>
                                                        <div class="col" id="gaziru-photo1-camera-btn-submit">
                                                            <input type="button" class="btn btn-secondary btn-block" value="追加" onclick="confirmPhotoCamera('gaziru-photo1')">
                                                        </div>        
                                                    </div>                                
                                                </div>
                                            </div>
                                        </div>
                                    </div>

                                </div>
                                <div class="col text-center imgUp">                        
                                    <div class="imgGaziruPreview">      
                                        <?php 
                                            if(!empty($sell_item->imageAuthen2)){
                                        ?>                                                                 
                                            <img class="img img-responsive full-width" id="gaziru-photo2-preview" src="user_photo_gaziru.php?id=2">
                                        <?php } else { ?>
                                            <img class="img img-responsive full-width" id="gaziru-photo2-preview" >
                                        <?php }?>
                                            <input type="text" id="check-gaziru-photo2" name="check-gaziru-photo2"  value="<?= $sell_item->photoGaziru2 ?>" hidden>                                                           
                                        
                                    </div>    
                                    <!-- <label class="btn btn-gaziru-upload">            
                                        写真を撮る 
                                        <input type="file" class="uploadFile img" id="gaziruPhoto2" name="gaziruPhoto2" value="Upload Photo" style="width: 0px;height: 0px;overflow: hidden;">
                                    </label> -->
                                    <!-- <input type="button" class="btn btn-secondary btn-block" value="写真を撮る" data-toggle="modal" data-target="#gaziru-photo2-modal">
                                    <input type="button" class="btn btn-secondary btn-block" value="入力をクリア" onclick="removeGaziruImage('gaziru-photo2')"> -->
                                    <div class="m-auto text-center ">
                                        <div class="d-flex justify-content-between flex-lg-row flex-column">
                                            <button class="btn btn-secondary col-lg-6 mr-lg-2 my-2"  onclick="return uploadPhoto('gaziru-photo2')" data-toggle="modal"  type="button" style="font-size: 1.4rem;">商品画像アップロード</button>
                                            <button class="btn btn-secondary col-lg-5 ml-lg-2 my-2" onclick="return takePhoto('gaziru-photo2')" data-toggle="modal" data-target="#gaziru-photo2-camera-modal" type="button" style="font-size: 1.4rem;">写真を撮る</button>
                                        </div>
                                        
                                        <button class="btn btn-secondary btn-block col-md-12 "  onclick="removeGaziruImage('gaziru-photo2')"   type="button" style="font-size: 1.4rem;">入力をクリア</button>
                                    </div>
                                    <h1>認証用画像</h1>
                                    <p>商品のボタンや模様、角など、特徴の捉えやすい箇所をマスキングテープで囲み、撮影してください。（※マスキングテープはそのまま剥がさずに梱包してください）。</p>

                                    <div id="gaziru-photo2-modal" class="modal fade show <?= ($gaziru1Check==true) ? 'md-show' : '' ?>" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-modal="true" style="z-index:999999">
                                        <div class="modal-dialog">
                                            <div class="modal-content p-3">
                                                <div class="modal-header">
                                                    <h2 class="text-center font-weight-bold w-100">認証用画像</h2>
                                                    <button type="button" class="btn-close" data-dismiss="modal" aria-label="Close" onclick="closePhoto('gaziru-photo2')">X</button>
                                                </div>
                                                
                                                <video id="gaziru-photo2-source" muted autoplay playsinline style="display: none"></video>
                                                <div class="imgGaziruPreviewCanvas mt-5">
                                                    <canvas id="gaziru-photo2-canvas" width=1024 height=1024 class="gaziruCanvasUpload img img-responsive full-width"></canvas>
                                                </div>

                                                <div class="mt-5 gaziruOptions">
                                                    <div class="row mt-5 mb-3">
                                                        <div class="col" id="gaziru-photo2-btn-upload">
                                                            <input type="button" id="gaziru-photo2-upload-btn" class="btn btn-secondary btn-block" value="アップロード" onclick="uploadPhoto('gaziru-photo2')">
                                                            <input type="file" class="uploadFile img" id="gaziru-photo2" name="gaziruPhoto2" value="Upload Photo" style="width: 0px;height: 0px;overflow: hidden;">
                                                        </div>
                                                        <div class="col" id="gaziru-photo2-btn-capture">
                                                            <input type="button" class="btn btn-secondary btn-block" value="写真を撮る" onclick="takePhoto('gaziru-photo2')">
                                                        </div>
                                                        <div class="col" id="gaziru-photo2-btn-submit">
                                                            <input type="button" class="btn btn-secondary btn-block" value="追加" onclick="confirmPhoto('gaziru-photo2')">
                                                        </div>        
                                                    </div>                                
                                                </div>
                                            </div>
                                        </div>
                                    </div>

                                    <!-- MODAL FOR CAMERA -->
                                    <div id="gaziru-photo2-camera-modal" class="modal fade show <?= ($gaziru1Check==true) ? 'md-show' : '' ?>" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-modal="true" style="z-index:999999">
                                        <div class="modal-dialog">
                                            <div class="modal-content p-3">
                                                <div class="modal-header">
                                                    <h2 class="text-center font-weight-bold w-100">全体画像</h2>
                                                    <button type="button" class="btn-close" data-dismiss="modal" aria-label="Close" onclick="closePhoto('gaziru-photo2-camera')">X</button>
                                                </div>
                                                
                                                <video id="gaziru-photo2-camera-source" muted autoplay playsinline style="display: none"></video>
                                                <div class="imgGaziruPreviewCanvas mt-5">
                                                    <canvas id="gaziru-photo2-camera-canvas" width=1024 height=1024 class="gaziruCanvasUpload img img-responsive full-width"></canvas>
                                                </div>
                                                <input type="range" class="zoom-range" id="zoom-range-2" hidden>
                                                <div class="mt-5 gaziruOptions">
                                                    <div class="row mt-5 mb-3">
                                                        <div class="col" id="gaziru-photo2-btn-upload" style="display:none">
                                                            <input type="button" id="gaziru-photo2-upload-btn" class="btn btn-secondary btn-block" value="アップロード" onclick="uploadPhoto('gaziru-photo2')">
                                                            <!-- <input type="file" class="uploadFile img" id="gaziru-photo2" name="gaziruPhoto2" value="Upload Photo" style="width: 0px;height: 0px;overflow: hidden;"> -->
                                                        </div>
                                                        <div class="col" id="gaziru-photo2-camera-btn-capture">
                                                            <input type="button" class="btn btn-secondary btn-block" value="写真を撮る" onclick="takePhoto('gaziru-photo2')">
                                                        </div>
                                                        <div class="col" id="gaziru-photo2-camera-btn-submit">
                                                            <input type="button" class="btn btn-secondary btn-block" value="追加" onclick="confirmPhotoCamera('gaziru-photo2')">
                                                        </div>        
                                                    </div>                                
                                                </div>
                                            </div>
                                        </div>
                                    </div>

                                </div>
                                <div class="col text-center imgUp">
                                    <div id="capture">
                                        <div class="imgGaziruPreview " id="image-marker-div">  
                                            <?php 
                                                if(!empty($sell_item->imageAuthen3)){
                                            ?>                                                                 
                                                <img class="img img-responsive full-width" id="gaziru-photo3-preview" src="user_photo_gaziru.php?id=3">
                                            <?php } else { ?>
                                                <img class="img img-responsive full-width" id="gaziru-photo3-preview" >
                                            <?php }?>
                                            <input type="text" id="check-gaziru-photo3" name="check-gaziru-photo3"  value="<?= $sell_item->photoGaziru3 ?>" hidden>                                         
                                            
                                        </div>
                                    </div>
                                    <!-- <label class="btn btn-gaziru-upload">
                                        写真を撮る
                                        <input type="file" class="uploadFile img" id="gaziru-photo3" name="gaziru-photo3" value="Upload Photo" style="width: 0px;height: 0px;overflow: hidden;">
                                    </label> -->
                                    <!-- <input type="button" class="btn btn-secondary btn-block" value="写真を撮る" data-toggle="modal" data-target="#gaziru-photo3-modal" id="garizuPhoto3">
                                    <input type="button" class="btn btn-secondary btn-block" value="入力をクリア" onclick="removeGaziruImage('gaziru-photo3')"> -->
                                    <div class="m-auto text-center">
                                        <div class="d-flex justify-content-between flex-lg-row flex-column" >
                                            <button class="btn btn-secondary col-lg-6 mr-lg-2 my-2"  onclick="return uploadPhoto('gaziru-photo3')" data-toggle="modal"  type="button" style="font-size: 1.4rem;">商品画像アップロード</button>
                                            <button class="btn btn-secondary col-lg-5 ml-lg-2 my-2" onclick="return takePhoto('gaziru-photo3')" data-toggle="modal" data-target="#gaziru-photo3-camera-modal" type="button" style="font-size: 1.4rem;">写真を撮る</button>
                                        </div>
                                        
                                        <button class="btn btn-secondary btn-block col-md-12 "  onclick="removeGaziruImage('gaziru-photo3')"   type="button" style="font-size: 1.4rem;">入力をクリア</button>
                                    </div>
                                    <h1>商品マーカー</h1>
                                    <p>どの箇所を認証用画像として撮影したかを、ご購入（検討）者に伝えるための画像を登録してください。</p>
                                    <div id="gaziru-photo3-modal" class="modal fade show <?= ($gaziru1Check==true) ? 'md-show' : '' ?>" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-modal="true" style="z-index:999999">
                                        <div class="modal-dialog">
                                            <div class="modal-content p-3">
                                                <div class="modal-header">
                                                    <h2 class="text-center font-weight-bold w-100">商品マーカー</h2>
                                                    <button type="button" class="btn-close" data-dismiss="modal" aria-label="Close" onclick="closePhoto('gaziru-photo3')">X</button>
                                                </div>
                                                
                                                <video id="gaziru-photo3-source" muted autoplay playsinline style="display: none"></video>
                                                <div class="imgGaziruPreviewCanvas mt-5">
                                                    <canvas id="gaziru-photo3-canvas" width=1024 height=1024 class="gaziruCanvasUpload img img-responsive full-width"></canvas>
                                                </div>

                                                <div class="mt-5 gaziruOptions">
                                                    <div class="row mt-5 mb-3">
                                                        <div class="col" id="gaziru-photo3-btn-upload">
                                                            <input type="button" id="gaziru-photo3-upload-btn" class="btn btn-secondary btn-block" value="アップロード" onclick="uploadPhoto('gaziru-photo3')">
                                                            <input type="file" class="uploadFile img" id="gaziru-photo3" name="gaziruPhoto3" value="Upload Photo" style="width: 0px;height: 0px;overflow: hidden;">
                                                        </div>
                                                        <div class="col" id="gaziru-photo3-btn-capture">
                                                            <input type="button" class="btn btn-secondary btn-block" value="写真を撮る" onclick="takePhoto('gaziru-photo3')">
                                                        </div>
                                                        <div class="col" id="gaziru-photo3-btn-submit">
                                                            <input type="button" class="btn btn-secondary btn-block" value="追加" onclick="confirmPhoto('gaziru-photo3')">
                                                        </div>        
                                                    </div>                                
                                                </div>
                                            </div>
                                        </div>
                                    </div>

                                    <!-- MODAL FOR CAMERA -->
                                    <div id="gaziru-photo3-camera-modal" class="modal fade show <?= ($gaziru1Check==true) ? 'md-show' : '' ?>" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-modal="true" style="z-index:999999">
                                        <div class="modal-dialog">
                                            <div class="modal-content p-3">
                                                <div class="modal-header">
                                                    <h2 class="text-center font-weight-bold w-100">全体画像</h2>
                                                    <button type="button" class="btn-close" data-dismiss="modal" aria-label="Close" onclick="closePhoto('gaziru-photo3-camera')">X</button>
                                                </div>
                                                
                                                <video id="gaziru-photo3-camera-source" muted autoplay playsinline style="display: none"></video>
                                                <div class="imgGaziruPreviewCanvas mt-5">
                                                    <canvas id="gaziru-photo3-camera-canvas" width=1024 height=1024 class="gaziruCanvasUpload img img-responsive full-width"></canvas>
                                                </div>
                                                <input type="range" class="zoom-range" id="zoom-range-3" hidden>
                                                <div class="mt-5 gaziruOptions">
                                                    <div class="row mt-5 mb-3">
                                                        <div class="col" id="gaziru-photo3-btn-upload" style="display:none">
                                                            <input type="button" id="gaziru-photo3-upload-btn" class="btn btn-secondary btn-block" value="アップロード" onclick="uploadPhoto('gaziru-photo3')">
                                                            <!-- <input type="file" class="uploadFile img" id="gaziru-photo3" name="gaziruPhoto3" value="Upload Photo" style="width: 0px;height: 0px;overflow: hidden;"> -->
                                                        </div>
                                                        <div class="col" id="gaziru-photo3-camera-btn-capture">
                                                            <input type="button" class="btn btn-secondary btn-block" value="写真を撮る" onclick="takePhoto('gaziru-photo3')">
                                                        </div>
                                                        <div class="col" id="gaziru-photo3-camera-btn-submit">
                                                            <input type="button" class="btn btn-secondary btn-block" value="追加" onclick="confirmPhotoCamera('gaziru-photo3')">
                                                        </div>        
                                                    </div>                                
                                                </div>
                                            </div>
                                        </div>
                                    </div>

                                </div>
                            </div>
                            <div class="row" id="imgWin-popup">
                            <div class="col">
                                    <div class="text-center imgWin">                                
                                        <a onclick="imgwin('全体画像.PNG')">ガジル社さんの説明画像も分かりやすいのですが、真ん中、右側の参考画像では、最終的にはマスキングテープを貼った画像が分かりやすいと思っております。</a>
                                    </div>
                                </div>
                                <div class="col">
                                    <div class="text-center imgWin">                                
                                        <a onclick="imgwin('特徴画像.PNG')">ガジル社さんの説明画像も分かりやすいのですが、真ん中、右側の参考画像では、最終的にはマスキングテープを貼った画像が分かりやすいと思っております。</a>
                                    </div>
                                </div>
                                <div class="col">
                                    <div class="text-center imgWin">                                
                                        <a onclick="imgwin('商品マーカー.PNG')">ガジル社さんの説明画像も分かりやすいのですが、真ん中、右側の参考画像では、最終的にはマスキングテープを貼った画像が分かりやすいと思っております。</a>
                                    </div>
                                </div>
                            </div>
                            
                            <div class='row'>
                                <img id="result" class="html2cavas-result" width=220 heigh=220>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="sell-content">
                    <div class="product-info">
                        <h3>商品の情報<span style="color: red">「※」</span>は必須入力です。</h3>

                        <div class="purchase-wishes-content-title each-product title-add">
                            <p class='required-field'>タイトル名（出品時に表示されます)</p>
                            <input type="text" name="title" id="title_id" 
                                value="<?= $sell_item->title ?>" placeholder="タイトル名">
                               <?php console_log($sell_item->title); ?>
                        </div>

                        <div class="purchase-wishes-content-title each-product">
                            <p class='required-field'>商品画像</p>
                            <?php 
                                for($i=1; $i <= 10; $i++){
                                    $check = 'photo'.$i;
                                    $namePhotoCheck = 'photoCheck'.$i;
                                    $photoC = $sell_item->$check;
                                    $photoCheck = $sell_item->$namePhotoCheck;
                                    $_SESSION['item'] = $sell_item;
                                    $sell_item->photo1 =  $_SESSION['item']->photo1;
                                    echo ('
                                        <div class="custom-file mb-2">
                                            <input type="file" class="custom-file-input" id="photo'.$i.'" name="photo'.$i.'" aria-describedby="photo'.$i.'Add">
                                            <input type="text" id="check-photo'.$i.'" name="check-photo'.$i.'"  value="'.$photoCheck["name"].'" hidden>
                                            <label class="custom-file-label" id="label-photo'.$i.'" for="photo'.$i.'">'); 
                                           
                                            if(empty($photoCheck["name"])){
                                                echo ('
                                                    商品画像ファイルの登録</label>');
                                            }else{
                                                echo (''.$photoCheck["name"].'');
                                            }
                                        echo ('</div>
                                            
                                        <div class="container">
                                            <div class="row" id="photo'.$i.'-preview" style="'); 
                                                if(empty($photoC)){
                                                    echo ('display: none');
                                                }
                                            echo ('">
                                                <div class="photo-preview-container">
                                                ');
                                                    if(!empty($photoC)){
                                                        echo ('   <img class="preview-image" id="photo'.$i.'-cropper"  width="150" heigth="150" src="user_photo.php?id='.$i.'" alt=""/>');
                                                    }else{
                                                        echo ('   <img class="preview-image" id="photo'.$i.'-cropper" width="150" heigth="150" />');
                                                    }
                                                 echo ('   
                                                </div>
                                                <input type="button" class="btn btn-secondary btn-sm btn-block" value="画像のトリミングをする" onclick="cropInputImage(\'photo'.$i.'\')">
                                                <input type="button" class="btn btn-secondary btn-sm btn-block" value="入力をクリア" onclick="removeInputImage(\'photo'.$i.'\')">
                                            </div>
                                        </div>
                                        ');
                                }
                            ?>

                        </div>

                       <div class="purchase-wishes-content__content each-product">
                            <p class='required-field'>この商品について</p>
                            <p class="product-detail-memo">※最大1.5万文字まで入力できます。</p>
                            <textarea name="remarks" id="remarks_id" cols="30" rows="10" maxlength="20000" placeholder="商品の説明（品番・正確な商品名、色、素材、重さ、保管方法、注意点など）"><?= $sell_item->remarks ?></textarea>
                        </div>
                        <div class="purchase-wishes-content-title title-add">
                        <?php if ($system_config->enabled_category):?>
                        <div class="product-info-select each-product">
                            <p class='required-field'>カテゴリー</p>
                            <select name="category" id="category_id">
                                <option value="">未選択</option>
                                <?php createCombo('selectCategory', $sell_item->category); ?>
                            </select>
                        </div>
                        <?php endif;?>
                        </div>
                        <div class="purchase-wishes-content-title each-product title-add">
                        <?php if ($system_config->enabled_item_state):?>
                        <div class="product-info-select">
                            <p class='required-field'>商品の状態について</p>
                            <select name="item_state" id="item_state_id">
                                <option value="">未選択</option>
                                <?php createCombo('selectItemState', $sell_item->item_state); ?>
                            </select>
                        </div>
                        <?php endif;?>
                        </div>

<!--                         <div class="product-info-select">
                            <p>自由項目</p>
                            <select name="" id="">
                                <option value="0">test</option>
                            </select>
                        </div> -->
                        <div class="purchase-wishes-content-title each-product ">
                        <div class="product-info-select">
                    <?php if ($system_config->enabled_pref):?>
                        <div class="product-info-select each-product">
                            <p>配送地域</p>
                            <select name="pref" id="pref_id">
                                <option value="">都道府県</option>
                                <?php createCombo('selectPref', $sell_item->pref); ?>
                            </select>
                        </div>
                        <?php endif;?>
                        </div>
                        </div>
                    </div>

                    <?php if ($system_config->enabled_delivery_type):?>
                    <div class="product-info">
                        <h3>配送の情報</h3>
                   
                    <div class="product-info-switch">
                        <!-- <div class="product-info-switch__input"> -->
                            <!-- <input type="checkbox" id="switch" />  -->
                            <!-- <input type="checkbox" class="custom-control-input" id="with_delivery" name="with_delivery" id="switch" value="1"<?= $sell_item->with_delivery ? ' checked="checked"' : '' ?>> -->
                            <input type="checkbox" id="with_delivery" name="with_delivery" value="1"<?= $sell_item->with_delivery ? ' checked="checked"' : '' ?>/>
                        <!-- </div> -->
                        <div class="product-info-switch__txt">配送</div>
                    </div>
                    <?php endif;?>
                    
                   
                        <div class="product-info-select each-product">
                            <p>配送方法</p>
                            <select name="delivery_type" id="delivery_type_id"<?= $sell_item->with_delivery ? '' : ' disabled' ?>>
                                <option value="">発送方法</option>
                                <?php createCombo('selectDeliveryType', $sell_item->delivery_type); ?>
                            </select>
                        </div>
                        <div class="product-info-select each-product">
                            <p>配送料の負担</p>
                            <select name="carriage_plan" id="carriage_plan_id"<?= $sell_item->with_delivery ? '' : ' disabled' ?>>
                                <option value="">選択</option>
                                <?php createCombo('selectCarriagePlan', $sell_item->carriage_plan); ?>
                            </select>
                        </div>
                        <div class="purchase-wishes-content-title">
                        <div class="product-info-select each-product">
                            <p>配送の目安</p>
                            <select name="carriage_type" id="carriage_type_id"<?= $sell_item->with_delivery ? '' : ' disabled' ?>>
                                <option value="">選択</option>
                                <?php createCombo('selectCarriageType', $sell_item->carriage_type); ?>
                            </select>
                        </div>
                        </div>
                    </div>
    
                    <div class="product-info">
                    <div class="product-price each-product">
                        <h3>価格の情報<span class="price-memo">(300円〜9,999,999円)</span></h3>
                        <p class='required-field'>価格（税込み価格）</p>
                        <input type="text" type="number" name="price" id = "price_id"
                               value="<?= $sell_item->price ?>"  placeholder="数字のみ（例：10,000円→10000と入力）" max="900000000">
                    </div>
                    </div>
        <!--            <div class="product-check check-margin">
                        <div class="product-check__input">
                            <input type="checkbox" name="show_evaluate" id="show_evaluate" class="add-check"
                                                                      value="1" <?= $sell_item->show_evaluate ? 'checked' : '' ?>>
                        </div>
                   
                        <div class="product-check__txt">
                            <label for="" class="label-add">総合評価の合計が-1以下のユーザーから、質問・交渉、購入をされたくない方はチェックを入れてください。</label>
                        </div>
                        </div>-->
                </div>

                <?php if ($isOffer): ?>
                    <input type="hidden" name="offer" value="offer"/>
                <?php endif; ?>

                <div class="sell-form-btn sell-form-btn-add">
                    <button type="submit" value="確認する" data-toggle="modal">確認する</button>
                <!--    <button>下書きに保存</button>-->
                </div>
<!--                <div class="sell-form-end">
                    <p>※商品画像は保存されません</p>
                </div>-->
            </form>

        </div>
        <!-- MODAL SHOW MESSAGE -->
        <div class="modal-message" id="parent-modal-mess">
            <div id="mess-modal" class="modal fade show" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-modal="true" >
                <div class="modal-dialog" style="background-color: white;text-align: center;">
                    <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" style="margin:auto;background:#fff;display:block;" width="200px" height="200px" viewBox="0 0 100 100" preserveAspectRatio="xMidYMid">
                        <g transform="rotate(0 50 50)">
                        <rect x="47" y="24" rx="3" ry="6" width="6" height="12" fill="#77fe71">
                            <animate attributeName="opacity" values="1;0" keyTimes="0;1" dur="1s" begin="-0.9166666666666666s" repeatCount="indefinite"></animate>
                        </rect>
                        </g><g transform="rotate(30 50 50)">
                        <rect x="47" y="24" rx="3" ry="6" width="6" height="12" fill="#77fe71">
                            <animate attributeName="opacity" values="1;0" keyTimes="0;1" dur="1s" begin="-0.8333333333333334s" repeatCount="indefinite"></animate>
                        </rect>
                        </g><g transform="rotate(60 50 50)">
                        <rect x="47" y="24" rx="3" ry="6" width="6" height="12" fill="#77fe71">
                            <animate attributeName="opacity" values="1;0" keyTimes="0;1" dur="1s" begin="-0.75s" repeatCount="indefinite"></animate>
                        </rect>
                        </g><g transform="rotate(90 50 50)">
                        <rect x="47" y="24" rx="3" ry="6" width="6" height="12" fill="#77fe71">
                            <animate attributeName="opacity" values="1;0" keyTimes="0;1" dur="1s" begin="-0.6666666666666666s" repeatCount="indefinite"></animate>
                        </rect>
                        </g><g transform="rotate(120 50 50)">
                        <rect x="47" y="24" rx="3" ry="6" width="6" height="12" fill="#77fe71">
                            <animate attributeName="opacity" values="1;0" keyTimes="0;1" dur="1s" begin="-0.5833333333333334s" repeatCount="indefinite"></animate>
                        </rect>
                        </g><g transform="rotate(150 50 50)">
                        <rect x="47" y="24" rx="3" ry="6" width="6" height="12" fill="#77fe71">
                            <animate attributeName="opacity" values="1;0" keyTimes="0;1" dur="1s" begin="-0.5s" repeatCount="indefinite"></animate>
                        </rect>
                        </g><g transform="rotate(180 50 50)">
                        <rect x="47" y="24" rx="3" ry="6" width="6" height="12" fill="#77fe71">
                            <animate attributeName="opacity" values="1;0" keyTimes="0;1" dur="1s" begin="-0.4166666666666667s" repeatCount="indefinite"></animate>
                        </rect>
                        </g><g transform="rotate(210 50 50)">
                        <rect x="47" y="24" rx="3" ry="6" width="6" height="12" fill="#77fe71">
                            <animate attributeName="opacity" values="1;0" keyTimes="0;1" dur="1s" begin="-0.3333333333333333s" repeatCount="indefinite"></animate>
                        </rect>
                        </g><g transform="rotate(240 50 50)">
                        <rect x="47" y="24" rx="3" ry="6" width="6" height="12" fill="#77fe71">
                            <animate attributeName="opacity" values="1;0" keyTimes="0;1" dur="1s" begin="-0.25s" repeatCount="indefinite"></animate>
                        </rect>
                        </g><g transform="rotate(270 50 50)">
                        <rect x="47" y="24" rx="3" ry="6" width="6" height="12" fill="#77fe71">
                            <animate attributeName="opacity" values="1;0" keyTimes="0;1" dur="1s" begin="-0.16666666666666666s" repeatCount="indefinite"></animate>
                        </rect>
                        </g><g transform="rotate(300 50 50)">
                        <rect x="47" y="24" rx="3" ry="6" width="6" height="12" fill="#77fe71">
                            <animate attributeName="opacity" values="1;0" keyTimes="0;1" dur="1s" begin="-0.08333333333333333s" repeatCount="indefinite"></animate>
                        </rect>
                        </g><g transform="rotate(330 50 50)">
                        <rect x="47" y="24" rx="3" ry="6" width="6" height="12" fill="#77fe71">
                            <animate attributeName="opacity" values="1;0" keyTimes="0;1" dur="1s" begin="0s" repeatCount="indefinite"></animate>
                        </rect>
                        </g>
                    </svg>
                    <p class="mt-2">登録情報のアップロード</p>
                </div>
            </div>
        </div>
        
    </div>
    
    <script>
        const mediaConstraints = {
            audio: false,
            video: {
                width: 720,
                height: 720,
                facingMode: "environment"   // フロントカメラを利用する
                // facingMode: { exact: "environment" }  // リアカメラを利用する場合
            }
        }

        const croppers = [];
        var cropping = false;
        var takePhoto1 = false;
        var takePhoto2 = false;
        var takePhoto3 = false;
        var gaziruPhoto1 = null;
        var gaziruPhoto2 = null;
        var gaziruPhoto3 = null;

        function takePhoto(id) {
            const video = document.getElementById(id+ '-camera-source')
            takePhoto1 == false;
            takePhoto2 == false;
            takePhoto3 == false;
            if (takePhoto1 == false && id == 'gaziru-photo1') {
                takePhoto1 = true;
                navigator.mediaDevices.getUserMedia(mediaConstraints)
                .then( (stream) => {
                    video.srcObject = stream;
                    const [track] = stream.getVideoTracks();
                    const capabilities = track.getCapabilities();
                    const settings = track.getSettings();
                    const input = document.getElementById('zoom-range-1');
                    
                    if (('zoom' in settings)) {
                        video.setAttribute("zoom", true);
                        // Map zoom to a slider element.
                        input.min = capabilities.zoom.min;
                        input.max = capabilities.zoom.max;
                        input.step = capabilities.zoom.step;
                        input.value = settings.zoom;
                        input.oninput = function(event) {
                            track.applyConstraints({advanced: [ {zoom: event.target.value} ]});
                        }
                        input.hidden = false;
                    }
                    
                    video.onloadedmetadata = async (e) => {
                        await video.play();

                        requestAnimationFrame(updateCanvas1);
                        document.getElementById(id + '-camera-btn-capture').style.display = 'none';
                        document.getElementById(id + '-camera-btn-submit').style.display = 'block';
                    };

                })
                .catch( (err) => {
                    console.log(err.name + ": " + err.message);
                });
            } else {
                takePhoto1 = false;
                document.getElementById(id + '-camera-btn-capture').style.display = 'block';
                document.getElementById(id + '-camera-btn-submit').style.display = 'block';
            }

            if (takePhoto2 == false && id == 'gaziru-photo2') {
                takePhoto2 = true;
                navigator.mediaDevices.getUserMedia(mediaConstraints)
                .then( (stream) => {
                    video.srcObject = stream;
                    const [track] = stream.getVideoTracks();
                    const capabilities = track.getCapabilities();
                    const settings = track.getSettings();

                    const input = document.getElementById('zoom-range-2');

                    if (('zoom' in settings)) {
                        video.setAttribute("zoom", true);
                        // Map zoom to a slider element.
                        input.min = capabilities.zoom.min;
                        input.max = capabilities.zoom.max;
                        input.step = capabilities.zoom.step;
                        input.value = settings.zoom;
                        input.oninput = function(event) {
                            track.applyConstraints({advanced: [ {zoom: event.target.value} ]});
                        }
                        input.hidden = false;
                    }
                    
                    video.onloadedmetadata = async (e) => {
                        await video.play();

                        requestAnimationFrame(updateCanvas2);
                        document.getElementById(id + '-camera-btn-capture').style.display = 'none';
                        document.getElementById(id + '-camera-btn-submit').style.display = 'block';
                    };
                })
                .catch( (err) => {
                    console.log(err.name + ": " + err.message);
                });
            } else {
                takePhoto2 = false;
                document.getElementById(id + '-btn-capture').style.display = 'block';
                document.getElementById(id + '-btn-submit').style.display = 'block';
                
            }

            if (takePhoto3 == false && id == 'gaziru-photo3') {
                takePhoto3 = true;
                navigator.mediaDevices.getUserMedia(mediaConstraints)
                .then( (stream) => {
                    video.srcObject = stream;
                    const [track] = stream.getVideoTracks();
                    const capabilities = track.getCapabilities();
                    const settings = track.getSettings();

                    const input = document.getElementById('zoom-range-3');

                    // Check whether zoom is supported or not.
                    if (('zoom' in settings)) {
                        video.setAttribute("zoom", true);
                        // Map zoom to a slider element.
                        input.min = capabilities.zoom.min;
                        input.max = capabilities.zoom.max;
                        input.step = capabilities.zoom.step;
                        input.value = settings.zoom;
                        input.oninput = function(event) {
                            track.applyConstraints({advanced: [ {zoom: event.target.value} ]});
                        }
                        input.hidden = false;
                    }

                    
                    video.onloadedmetadata = async (e) => {
                        await video.play();

                        requestAnimationFrame(updateCanvas3);
                        document.getElementById(id + '-camera-btn-capture').style.display = 'none';
                        document.getElementById(id + '-camera-btn-submit').style.display = 'block';
                    };
                })
                .catch( (err) => {
                    console.log(err.name + ": " + err.message);
                });
            } else {
                takePhoto3 = false;
                document.getElementById(id + '-btn-capture').style.display = 'block';
                document.getElementById(id + '-btn-submit').style.display = 'block';
            }
        }

        function updateCanvas1() {
            const video = document.getElementById('gaziru-photo1-camera-source')
            const canvas = document.getElementById('gaziru-photo1-camera-canvas')

            var ctx = canvas.getContext('2d')

            ctx.drawImage(video, 0, 0, canvas.width, canvas.height)

            if (takePhoto1 == true) {
            
                ctx.beginPath();
                ctx.lineWidth = "2";
                ctx.strokeStyle = "#77acf1";
                ctx.rect(canvas.width / 2 - 300, canvas.height / 2 - 300, 600, 600);

                ctx.moveTo(canvas.width / 2 - 10, canvas.height / 2);
                ctx.lineTo(canvas.width / 2 + 10, canvas.height / 2);

                ctx.moveTo(canvas.width / 2, canvas.height /2 - 10);
                ctx.lineTo(canvas.width / 2, canvas.height /2 + 10);

                ctx.stroke();
                requestAnimationFrame(updateCanvas1)
            }
        }

        function updateCanvas2() {
            const video = document.getElementById('gaziru-photo2-camera-source')
            const canvas = document.getElementById('gaziru-photo2-camera-canvas')

            var ctx = canvas.getContext('2d')

            ctx.drawImage(video, 0, 0, canvas.width, canvas.height)

            if (takePhoto2 == true) {
                ctx.beginPath();
                ctx.lineWidth = "2";
                ctx.strokeStyle = "#77acf1";
                ctx.rect(canvas.width / 2 - 300, canvas.height / 2 - 300, 600, 600);

                ctx.moveTo(canvas.width / 2 - 10, canvas.height / 2);
                ctx.lineTo(canvas.width / 2 + 10, canvas.height / 2);

                ctx.moveTo(canvas.width / 2, canvas.height /2 - 10);
                ctx.lineTo(canvas.width / 2, canvas.height /2 + 10);

                ctx.stroke();
                requestAnimationFrame(updateCanvas2)
            }
        }

        function updateCanvas3() {
            const video = document.getElementById('gaziru-photo3-camera-source')
            const canvas = document.getElementById('gaziru-photo3-camera-canvas')

            var ctx = canvas.getContext('2d')

            ctx.drawImage(video, 0, 0, canvas.width, canvas.height)

            if (takePhoto3 == true) {
                ctx.beginPath();
                ctx.lineWidth = "2";
                ctx.strokeStyle = "#77acf1";
                ctx.rect(canvas.width / 2 - 300, canvas.height / 2 - 300, 600, 600);

                ctx.moveTo(canvas.width / 2 - 10, canvas.height / 2);
                ctx.lineTo(canvas.width / 2 + 10, canvas.height / 2);

                ctx.moveTo(canvas.width / 2, canvas.height /2 - 10);
                ctx.lineTo(canvas.width / 2, canvas.height /2 + 10);

                ctx.stroke();
                requestAnimationFrame(updateCanvas3)
            }
        }

        function confirmPhoto(id) {
            if (id == 'gaziru-photo3') {
                if(document.getElementById('image-marker') != undefined){
                    document.getElementById('image-marker').remove();
                }
            }
            const fileInput = document.getElementById(id)
            const modal = document.getElementById(id + "-modal")
            const imgResult = document.getElementById(id + '-preview')
            const canvas = document.getElementById(id + '-canvas')
            
            var imgurl =  canvas.toDataURL();
            let blob = fetch(imgurl).then(r => r.blob()).then(
                blobFile => {
                    let newFile = new File([blobFile], 'temp.jpg', { type: "image/jpg" });
                    let container = new DataTransfer();
                    container.items.add(newFile);
                    fileInput.files = container.files;

                    imgResult.src = imgurl;
                    $('#' + id + '-modal').modal('hide');
                    cropping=false;
                }
            );
            
            if (id == 'gaziru-photo3') {
                var marker = document.createElement("img");
                marker.setAttribute("class", "image-marker overlay");
                marker.setAttribute("id", "image-marker");
                marker.setAttribute("src", "../common/assets/img/marker.png");
                document.getElementById("image-marker-div").appendChild(marker);
                dragElement(marker);
            }
        }

        function confirmPhotoCamera(id){
            if (id == 'gaziru-photo3') {
                if(document.getElementById('image-marker') != undefined){
                    document.getElementById('image-marker').remove();
                }
            }
            const fileInput = document.getElementById(id);
            const modal = document.getElementById(id + "-camera-modal");
            const imgResult = document.getElementById(id + '-preview');
            const canvas = document.getElementById(id + '-camera-canvas');
            
            var imgurl =  canvas.toDataURL();
            let blob = fetch(imgurl).then(r => r.blob()).then(
                blobFile => {
                    let newFile = new File([blobFile], 'temp.jpg', { type: "image/jpg" });

                    let container = new DataTransfer();
                    container.items.add(newFile);
                    fileInput.files = container.files;

                    imgResult.src = imgurl;
                    $('#' + id + '-camera-modal').modal('hide');
                    cropping=false;
                }
            );
            
            if (id == 'gaziru-photo3') {
                var marker = document.createElement("img");
                marker.setAttribute("class", "image-marker overlay");
                marker.setAttribute("id", "image-marker");
                marker.setAttribute("src", "../common/assets/img/marker.png");
                document.getElementById("image-marker-div").appendChild(marker);
                dragElement(marker);
            }
        }
        function imgwin(img){
            window.open("../common/assets/img/imgwin/" + img, "imgwindow", "width=866,height=580");
        };

        function capture() {
            console.log("capture")
            html2canvas(document.getElementById('image-marker-div'), {
                useCORS: true,
                scrollY: -window.scrollY,
                scrollX: -window.scrollX,
            }).then(canvas => {
                document.getElementById('gaziru-photo3-preview').src = canvas.toDataURL('image/png');
                document.getElementById('image-marker').remove();
                var imgurl =  canvas.toDataURL();
                let blob = fetch(imgurl).then(r => r.blob()).then(
                    blobFile => {
                        var fileInput = document.getElementById('gaziru-photo3');
                        let newFile = new File([blobFile], fileInput.files[0].name, { type: "image/png" });
                        let container = new DataTransfer();
                        container.items.add(newFile);
                        fileInput.files = container.files;
                    }
                );
            });
        };

        function uploadPhoto(id) {
           
            if(!cropping) {
                document.getElementById(id).click();
                for (const obj in croppers ) {
                    croppers[obj].cropper.destroy();
                    cropping=false;
                
                }
                $('.cropper-container').remove();
                if(document.getElementById(id +'-canvas').getContext('2d').canvas.cropper != undefined){
                    var imgCanvas = document.getElementById(id +'-canvas').getContext('2d').canvas.cropper.destroy();
                }
            } else{
                console.log(croppers);
                if (croppers.find(({ id }) => id === id) != null) {
                    const cropper = croppers.find(({ id }) => id === id);
                    const croppedCanvas =  cropper.cropper.getCroppedCanvas({maxWidth: 1023, maxHeight: 1023})
                    const canvas = document.getElementById(id + '-canvas')
                    canvas.getContext('2d').drawImage(croppedCanvas, 0, 0, canvas.width, canvas.height)
                    croppers.splice(croppers.indexOf(cropper), 1)
                    cropper.cropper.destroy()
                    cropping = false
                    // document.getElementById(id + '-upload-btn').value = 'アップロード';
                }
            } 
        };

        document.querySelectorAll('.uploadFile').forEach(function(ele) {
            ele.addEventListener('change',function(e){
                $('#' + e.target.id + '-modal').modal('show');
                cropping = true;
                var files = !!this.files ? this.files : [];
                if (!files.length || !window.FileReader) return;
        
                if (/^image/.test( files[0].type)){ 
                    var reader = new FileReader(); 
                    reader.readAsDataURL(files[0]);
        
                    reader.onloadend = function() { 
                        const id = e.target.id
                        const canvas = document.getElementById(id + '-canvas');
                        // canvas.width = 1024
                        // canvas.height = 1024
                        const img =  new Image()
                        img.src = window.URL.createObjectURL(files[0]);

                        img.onload = () => {
                            canvas.getContext('2d').drawImage(img, 0, 0, canvas.width, canvas.height);
                            cropping = true;
                            setTimeout(initCropper(id + '-canvas'), 1000);
                            // document.getElementById(id + '-upload-btn').value = 'クロップ画像';
                            document.getElementById(id + '-btn-capture').style.display = 'none';
                            document.getElementById(id + '-btn-upload').style.display = 'block';
                        }
                        
                    }
                            
                }
            });
        });

        document.querySelectorAll('.custom-file-input').forEach(function(ele) {
                ele.addEventListener('change',function(e){
                    if (croppers.find(({ id }) => id === id) != null) {
                        const cropper = croppers.find(({ id }) => id === id);
                        croppers.splice(croppers.indexOf(cropper), 1);
                        cropper.cropper.destroy();
                    }

                    var fileName = document.getElementById(e.target.id).files[0].name;
                    var nextSibling = e.target.nextElementSibling;
                    nextSibling.innerText = fileName;
                    var previewDiv = document.getElementById(e.target.id + '-preview');
                    previewDiv.style.display = 'block';
                    previewDiv.childNodes[1].childNodes[1].src = window.URL.createObjectURL(this.files[0]);
            })
        })

        function removeGaziruImage(name) {
            var preview = document.getElementById(name + '-preview').removeAttribute('src');
            for (const obj in croppers ) {
                croppers[obj].cropper.destroy();
                cropping=false;
                
            }
            
            const canvas = document.getElementById(name + '-canvas');
            var ctx = canvas.getContext('2d')
            ctx.clearRect(0, 0, canvas.width, canvas.height);
            setTimeout(initCropper(name + '-canvas'), 0);
            ctx.clearRect(0, 0, canvas.width, canvas.height);
            $('.cropper-container').remove();

            var photoCheckGaziru = document.getElementById('check-'+ name).value = '';
            document.getElementById('check-gaziru-photo3').value="";
            var imgCanvas = document.getElementById(name +'-canvas').getContext('2d').canvas.cropper.destroy();
            if(document.getElementById(name +'-camera-canvas').getContext('2d').canvas.cropper){
                var imgCanvas = document.getElementById(name +'-camera-canvas').getContext('2d').canvas.cropper.destroy();
            }
            
            var inputFile = document.getElementById(name);
            inputFile.value = "";
            if (name == 'gaziru-photo3') {
                document.getElementById('image-marker').remove();
                document.getElementById('image-marker').style.display="none";
            }

            
        }

        function closePhoto(id){
            for (const obj in croppers ) {
                croppers[obj].cropper.destroy();
                cropping=false;
                croppers[obj].cropper.cropped = false;
                croppers[obj].cropper.crossOriginUrl = "";
            }
            
            const canvas = document.getElementById(id + '-canvas');
            var ctx = canvas.getContext('2d')
            ctx.clearRect(0, 0, canvas.width, canvas.height);
            $('.cropper-container').remove();
            if(document.getElementById(id +'-canvas').getContext('2d').canvas.cropper != undefined){
                    var imgCanvas = document.getElementById(id +'-canvas').getContext('2d').canvas.cropper.destroy();
                }
                console.log(canvas.getContext('2d'));
        }
        function removeInputImage(name) {
            
            if (croppers.find(({ name }) => name === name) != null) {
                const cropper = croppers.find(({ name }) => name === name);
                croppers.splice(croppers.indexOf(cropper), 1);
                cropper.cropper.destroy();
            }

            var previewDiv = document.getElementById(name + '-preview').style.display = 'none';
            var inputFile = document.getElementById(name)
            inputFile.value = "";
            inputFile.nextElementSibling.innerText = "商品画像ファイルの登録";

            var photoCheck = document.getElementById('check-' + name).value = '';
            var labelCheck = document.getElementById('label-' + name).innerHTML = '商品画像ファイルの登録';
        }

        function initCropper(id){
            var image = document.getElementById(id);
            var cropper = new Cropper(image, {
                viewMode: 2,
                center: true,
                responsive: true,
                scalable: false,
                zoomable: false,
                aspectRatio: 1/1,
            })
            
            croppers.push({id: id, cropper: cropper});
        }

        function cropInputImage(id) {
            if (croppers.find(({ id }) => id === id) != null) {
                const cropper = croppers.find(({ id }) => id === id);
                var imgurl =  cropper.cropper.getCroppedCanvas().toDataURL();
                let blob = fetch(imgurl).then(r => r.blob()).then(
                    blobFile => {
                        var fileInput = document.getElementById(id);
                        let newFile = new File([blobFile], fileInput.files[0].name, { type: ["image/jpg", "image/png"] });
                        let container = new DataTransfer();
                        container.items.add(newFile);
                        fileInput.files = container.files;
                        document.getElementById(id + '-cropper').src = imgurl;
                });
                
                croppers.splice(croppers.indexOf(cropper), 1);
                cropper.cropper.destroy();
                cropping = false;
            } else {
                setTimeout(initCropper(id + '-cropper'), 1000);
            }
        }

        function dragElement(elmnt) {
            var pos1 = 0, pos2 = 0, pos3 = 0, pos4 = 0;
            if (document.getElementById('image-marker')) {
                // if present, the header is where you move the DIV from:
                document.getElementById('image-marker').onmousedown = dragMouseDown;
                $('#image-marker').on({ 'touchstart' : dragMouseDown });
            } else {
                // otherwise, move the DIV from anywhere inside the DIV:
                elmnt.onmousedown = dragMouseDown;
            }

            function dragMouseDown(e) {
                e = e || window.event;
                e.preventDefault();
                // get the mouse cursor position at startup:
                if (e.touches!=undefined) {
                    pos3 = e.touches[0].clientX;
                    pos4 = e.touches[0].clientY;
                } else {
                    pos3 = e.clientX;
                    pos4 = e.clientY;
                }
                document.onmouseup = closeDragElement;
                $('#image-marker').on({ 'touchend' : closeDragElement });
                // call a function whenever the cursor moves:
                document.onmousemove = elementDrag;
                $('#image-marker').on({ 'touchmove' : elementDrag });
            }

            function elementDrag(e) {
                var img = document.getElementById('gaziru-photo3-preview');

                e = e || window.event;
                e.preventDefault();
                // calculate the new cursor position:
                if (e.touches!=undefined) {
                    pos1 = pos3 - e.touches[0].clientX;
                    pos2 = pos4 - e.touches[0].clientY;
                    pos3 = e.touches[0].clientX;
                    pos4 = e.touches[0].clientY;
                } else {
                    pos1 = pos3 - e.clientX;
                    pos2 = pos4 - e.clientY;
                    pos3 = e.clientX;
                    pos4 = e.clientY;
                }
                
                // set the element's new position:
                if (0 < (elmnt.offsetTop - pos2) && (elmnt.offsetTop - pos2) < img.height - 10) {
                    elmnt.style.top = (elmnt.offsetTop - pos2) + "px";
                }
                
                if (15 < (elmnt.offsetLeft - pos1) && (elmnt.offsetLeft - pos1) < img.width + 5) {
                    elmnt.style.left = (elmnt.offsetLeft - pos1) + "px";
                }
            }

            function closeDragElement() {
                // stop moving when mouse button is released:
                document.onmouseup = null;
                document.onmousemove = null;
            }
        }
    </script>
    <script src="../common/assets/js/html2canvas.js"></script>
    <script src="../common/assets/js/html2canvas.min.js"></script>
    <link href="../common/assets/js/ImageCropper/cropper.css" rel="stylesheet">
    <script src="../common/assets/js/ImageCropper/cropper.js"></script>
    <script>
        $(".product-info-select select option[value=11]").detach();
    </script>

<?php include('../footer.php'); ?>
