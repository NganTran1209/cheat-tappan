<?php include_once(__DIR__.'/../common/util.php'); ?>
<?php include_once(__DIR__.'/../entity/identification.php'); ?>
<?php

if (!Identification::hasPhoto()) {
    exit;
}

$photo = file_get_contents(Identification::getPhoto1Path());
$finfo = finfo_open(FILEINFO_MIME_TYPE);
$mime = finfo_buffer($finfo, $photo);

header("Content-Type: ".$mime);
echo $photo;
exit();