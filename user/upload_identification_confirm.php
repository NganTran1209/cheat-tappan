<?php 
$title_page = '本人確認書類の提出確認';
$description_page = '欲しい商品を画像で確認・届いた商品を画像で照合！気持ち良い取引が叶うオンラインマーケット。';
 ?>
<?php include_once(__DIR__ . '/../common/util.php'); ?>
<?php include_once(__DIR__ . '/../entity/identification.php'); ?>
<?php include_once(__DIR__ . '/../common/login_check.php'); ?>
<?php
if (isIdentification()) {
    header('Location: ' . getContextRoot() . '/user/mypage.php');
    exit;
}

if (isset($_POST['action']) && $_POST['action'] == 'regist') {
    Identification::regist();
    setMessage('本人証明書を登録しました。出品許可されるまでしばらくお待ち下さい。');
    header('Location: ' . getContextRoot() . '/user/mypage.php');
    exit();
}
$title_page = '本人確認書類の提出確認';
?>
<?php include('../user_header.php'); ?>
    <div class="com-container bg-yellow">
        <?php include('usersidebar.php'); ?>
        <div class="com-content ">
            <div class="bg-inner">
                <h1>本人確認書類アップロードの確認</h1>
                <form method="post" enctype="multipart/form-data">
                    <h2>本人書類確認</h2>
                    <br>
                    <div class="row">
                        <div class="col-4">
                            <img class="img-fluid" src="identification_photo.php" alt=""/>
                        </div>
                        <div class="col-4">
                            <?php if (!Identification::hasPhoto2()) : ?>
                            <?php else : ?>
                                <img class="img-fluid" src="identification_photo2.php" alt=""/>
                            <?php endif ?>
                        </div>
                    </div>

                    <div class="row my-5">
                        <div class="col-md-8">
                            <form method="post">
                                <input type="hidden" name="action" value="regist"/>
                                <input class="btn btn-block btn-info c-btn01 c-btn-01--danger" type="submit" value="登録する"/>
                            </form>
                        </div>
                        <div class="col-md-4">
                            <form method="post" action="upload_identification.php">
                                <input type="hidden" name="action" value="modify"/>
                                <input class="btn btn-block btn-outline-dark c-btn01" type="submit" value="変更する"/>
                            </form>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
<?php include('../user_footer.php'); ?>