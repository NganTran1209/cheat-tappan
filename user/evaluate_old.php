<?php include_once(__DIR__ . '/../common/util.php'); ?>
<?php include_once(__DIR__ . '/../common/login_check.php'); ?>
<?php

$group = 0;
$sort = 0;
if (!empty($_GET['group'])) {
    $group = $_GET['group'];
}
if (!empty($_GET['sort'])) {
    $sort = $_GET['sort'];
}

$order = new Order();
$evoluteList = $order->selectFromUserAll(getUserId());

if ($sort != 0) {
    usort($evoluteList, function ($lhs, $rhs) {
        global $sort;
        $lEvolute = 0;
        $rEvolute = 0;
        if ($lhs->user_id == getUserId()) {
            $lEvolute = $lhs->buyer_evaluate;
        } else {
            $lEvolute = $lhs->seller_evaluate;
        }
        if ($rhs->user_id == getUserId()) {
            $rEvolute = $rhs->buyer_evaluate;
        } else {
            $rEvolute = $rhs->seller_evaluate;
        }
        if ($lEvolute != $rEvolute) {
            if ($sort == $lEvolute) {
                return -1;
            }
            if ($sort == $rEvolute) {
                return 1;
            }
        }
        return 0;
    });
}


$ownerEvaluateList = $order->selectFromOwner(getUserId());
$userEvaluateList = $order->selectFromUser(getUserId());

$evaluate = new Evaluate();
$evaluate->selectSummary(getUserId());

?>

<?php include(__DIR__ . '/../header.php'); ?>
<div class="container">
    <div class="row">
        <div class="col-md-3 sideContents">
            <?php include('usersidebar.php'); ?>
        </div>
        <div class="col-md-9 mainContents">
            <div class="bg-inner">
                <h1>全ての評価</h1>
                <div class="row">
                    <div class="col-sm-4">
                        <a class="btn btn-block btn-outline-dark"
                           href="<?php echo HOME_URL; ?>/user/evaluate.php?group=0&sort=0">全ての評価</a>
                    </div>
                    <div class="col-sm-4">
                        <a class="btn btn-block btn-outline-dark"
                           href="<?php echo HOME_URL; ?>/user/evaluate.php?group=1&sort=<?= $sort ?>">購入の評価</a>
                    </div>
                    <div class="col-sm-4">
                        <a class="btn btn-block btn-outline-dark"
                           href="<?php echo HOME_URL; ?>/user/evaluate.php?group=2&sort=<?= $sort ?>">出品の評価</a>
                    </div>
                </div>
                <div class="margin-20">
                    <div class="text-center">
                        総合評価：<?= $evaluate->point ?>(
                        <i class="fas fa-smile mr-1"></i><?= $evaluate->good_count ?>&nbsp;
                        <i class="fas fa-meh mr-1"></i><?= $evaluate->normal_count ?>&nbsp;
                        <i class="fas fa-frown mr-1"></i><?= $evaluate->bad_count ?>
                        <?php /*?>
                <?= $evaluate->toGoodStringOnImage() ?>
                <?= $evaluate->toNormalStringOnImage() ?>
                <?= $evaluate->toBadStringOnImage() ?>
                <?php */ ?>
                        )
                        <div class="evaluate-attention">
                            <strong>総合評価の見方</strong><br>
                            ・取引相手から「良い」の評価は＋1、「普通」の評価は＋0、「悪い」の評価は−1となります。<br>
                            ・例えば「良い<i class="fas fa-smile mr-1"></i>」が1、「悪い<i class="fas fa-frown mr-1"></i>」が1の場合、総合評価は±0になります。
                        </div>

                    </div>
                    <hr class="hr">
                    <?php /*?>
							<div class="text-center">
								購入者：<?= $evaluate->sellerEvaluate->point ?>(
								<?= $evaluate->sellerEvaluate->toGoodStringOnImage() ?>
								<?= $evaluate->sellerEvaluate->toNormalStringOnImage() ?>
								<?= $evaluate->sellerEvaluate->toBadStringOnImage() ?>
								)(表示仮)
							</div>

							<div class="text-center">
								出品者：<?= $evaluate->buyerEvaluate->point ?>(
								<?= $evaluate->buyerEvaluate->toGoodStringOnImage() ?>
								<?= $evaluate->buyerEvaluate->toNormalStringOnImage() ?>
								<?= $evaluate->buyerEvaluate->toBadStringOnImage() ?>
								)(表示仮)
							</div>
							<?php */ ?>
                </div>
                <div class="text-right mb-3">
                    <i class="fa fa-sort" aria-hidden="true"></i>
                    <a class="sortButton" href="<?php echo HOME_URL; ?>/user/evaluate.php?group=<?= $group ?>&sort=1">良い</a>
                    <a class="sortButton" href="<?php echo HOME_URL; ?>/user/evaluate.php?group=<?= $group ?>&sort=2">普通</a>
                    <a class="sortButton" href="<?php echo HOME_URL; ?>/user/evaluate.php?group=<?= $group ?>&sort=3">悪い</a>
                </div>

                <?php foreach ($evoluteList as $order): ?>
                    <?php if ($order->user_id != getUserId()): ?>
                        <?php if ($group == 1) {
                            continue;
                        } ?>
                        <?php if ($order->seller_evaluate == 0) {
                            continue;
                        } ?>
                        <?php if ($sort != 0 && $order->seller_evaluate != $sort) {
                            continue;
                        } ?>
                        <div class="row mb-4">
                            <div class="col-md-3">
                                <div class="balloonUser">
                                    <!-- <a href="--><?php //echo HOME_URL; ?><!--/evaluate.php?user_id=-->
                                    <?php //echo $order->user_id; ?><!--">--><?php //echo $order->user->hash_id; ?><!--</a>-->
                                    <a href="<?php echo HOME_URL; ?>/profile/<?= $order->user->hash_id ?>"><?= $order->user->getViewNicknameOrId() ?></a>
                                    <div class="commenter">
                                        評価：
                                        <?php if (Evaluate::toEvaluateImageTag($order->seller_evaluate) == Evaluate::toEvaluateImageTag(1)): ?>
                                            <i class="fas fa-smile mr-1"></i>
                                        <?php elseif (Evaluate::toEvaluateImageTag($order->seller_evaluate) == Evaluate::toEvaluateImageTag(2)): ?>
                                            <i class="fas fa-meh mr-1"></i>
                                        <?php elseif (Evaluate::toEvaluateImageTag($order->seller_evaluate) == Evaluate::toEvaluateImageTag(3)): ?>
                                            <i class="fas fa-frown mr-1"></i>
                                        <?php endif; ?>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-9">
                                <div class="bg-comment balloon balloonB">
                                    <div class="balloonComment">
                                        <div class="">
                                            <?php //echo Evaluate::toEvaluateImageTag($order->seller_evaluate); ?>
                                            <a href="<?php echo HOME_URL; ?>/itemdetail.php?id=<?= $order->item_id ?>"><?= $order->item_title ?></a>
                                        </div>
                                        <?= empty($order->seller_comment) ? 'コメントはありません' : nl2br($order->seller_comment) ?>
                                    </div>
                                    <div class="balloonTime">
                                        <i class="far fa-clock mr-2"></i><?= $order->regist_date ?>
                                    </div>
                                </div>
                            </div>
                        </div>
                    <?php else: ?>
                        <?php if ($group == 2) {
                            continue;
                        } ?>
                        <?php if ($order->buyer_evaluate == 0) {
                            continue;
                        } ?>
                        <?php if ($sort != 0 && $order->buyer_evaluate != $sort) {
                            continue;
                        } ?>
                        <div class="row mb-4">
                            <div class="col-md-3">
                                <div class="balloonUser">
                                    <!-- <a href="--><?php //echo HOME_URL; ?><!--/evaluate.php?user_id=-->
                                    <?php //echo $order->owner_id; ?><!--">-->
                                    <?php //echo $order->owner->hash_id; ?><!--</a>-->
                                    <a href="<?php echo HOME_URL; ?>/profile/<?= $order->owner->hash_id ?>"><?= $order->owner->getViewNicknameOrId() ?></a>
                                    <div class="commenter">
                                        評価：
                                        <?php if (Evaluate::toEvaluateImageTag($order->buyer_evaluate) == Evaluate::toEvaluateImageTag(1)): ?>
                                            <i class="fas fa-smile mr-1"></i>
                                        <?php elseif (Evaluate::toEvaluateImageTag($order->buyer_evaluate) == Evaluate::toEvaluateImageTag(2)): ?>
                                            <i class="fas fa-meh mr-1"></i>
                                        <?php elseif (Evaluate::toEvaluateImageTag($order->buyer_evaluate) == Evaluate::toEvaluateImageTag(3)): ?>
                                            <i class="fas fa-frown mr-1"></i>
                                        <?php endif; ?>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-9">
                                <div class="bg-comment balloon balloonA">
                                    <div class="balloonComment">
                                        <div class="">
                                            <a href="<?php echo HOME_URL; ?>/itemdetail.php?id=<?= $order->item_id ?>"><?= $order->item_title ?></a>
                                            <?php //echo Evaluate::toEvaluateImageTag($order->buyer_evaluate); ?>
                                        </div>
                                        <?= empty($order->buyer_comment) ? 'コメントはありません' : nl2br($order->buyer_comment) ?>
                                    </div>
                                    <div class="balloonTime">
                                        <i class="far fa-clock mr-2"></i><?= $order->regist_date ?>
                                    </div>
                                </div>
                            </div>
                        </div>
                    <?php endif; ?>
                <?php endforeach; ?>
            </div>
        </div>
    </div>
</div>

<?php include('../footer.php'); ?>




