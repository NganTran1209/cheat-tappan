<?php include_once(__DIR__ . '/../common/util.php'); ?>
<?php include_once(__DIR__ . '/../common/login_check.php'); ?>
<?php
$item = new Item();
$item->select($_GET['item_id']);
$owner = new User();
$owner->select($item->owner_id);
$negotiation = new Negotiation();
$negotiation->item_id = $_GET['item_id'];
$sells = $negotiation->selectSell();
usort($sells, function ($a, $b) {
    if (strtotime($a->last_date) == strtotime($b->last_date)) {
        return 0;
    } elseif (strtotime($a->last_date) > strtotime($b->last_date)) {
        return -1;
    }
    return 1;
});

$view_other_item_link = true;
$view_evaluate = true;
$title_page = '購入希望の募集履歴';
?>
<?php include('../user_header.php'); ?>
    <div class="com-header-top">
        <div class="com-header-top__img wow animate__animated animate__fadeInUp">
            <img src="<?php echo HOME_URL; ?>/common/assets/img/mypage/top-bg.png" alt="">
        </div>
        <div class="com-header-top__path wow animate__animated animate__fadeInUp">
            <p><span><a href="<?php echo HOME_URL; ?>/" class="clr-yel">トップページ</a></span><span> > </span><span><a href="<?php echo HOME_URL; ?>/user/mypage.php" class="clr-yel">マイページ</a></span>
            <span> > </span><span>購入希望の募集履歴</span></p>
        </div>
        <div class="com-header-top__txt">
            <p class="clr-white">マイページ</p>
        </div>
    </div>
<div class="com-container bg-yellow">
            <?php include('usersidebar.php'); ?>
        <div class="com-content ">
            <div class="bg-inner ">
                <h1 >質問・交渉したユーザー</h1>
                <div class="itemTitle">
                    <h2><?= $item->title ?></h2>
                </div>
                <div class="ladies-product-list-content">
                    <?php include_once(__DIR__ . '/../common/parts/item_image_area.php'); ?>
                    <?php include_once(__DIR__ . '/../common/parts/item_info_area.php'); ?>
                </div>

                <div class="mb-4">
                    <div class="text-center">
                        <span class="font30 fontBold fontRed"><?= number_format($item->price) ?></span>
                        <span class="smallEX">円(税込・<?= $item->carriage_plan_name ?>)</span>
                    </div>
                </div>

                <div class="itemContentText">
                    <?php echo nl2br(htmlentities($item->remarks)); ?>
                </div>
                <table class="table table-bordered my-5">
                    <tbody>
                    <tr>
                        <th>最終連絡日</th>
                        <th>質問・交渉したユーザー</th>
                        <?php /*?><th class="col-sm-2">ステータス</th>
    <th class="col-sm-2">連絡日</th><?php */ ?>

                        <th>連絡数</th>
                    </tr>
                    <?php foreach ($sells as $sell): ?>
                        <?php
                        $seller = new User();
                        $seller->select($sell->user_id);
                        ?>
                        <tr>
                            <td><?php /*?><?= date('Y/n/j', strtotime($sell->last_date)) ?><?php */ ?><?= date($sell->last_date) ?></td>
                            <td><a
                                        href="negotiation.php?item_id=<?= $sell->item_id ?>&user_id=<?= $sell->user_id ?>" <?= $item->createBlockScript($sell->user_id) ?>><?php /*?><?= $sell->user_name ?><?php */ ?><?= $seller->getViewId() ?></a>
                            </td>
                            <?php /*?><td>交渉中</td><?php */ ?>
                            <?php /*?><td><?= date('Y/n/j', strtotime($sell->start_date)) ?></td><?php */ ?>
                            <td><?= number_format($sell->negotiation_count) ?>回</td>
                        </tr>
                    <?php endforeach; ?>
                    </tbody>
                </table>
            </div>
        </div>
    </div>

<?php include('../footer.php'); ?>
