<?php include_once(__DIR__ . '/../common/util.php'); ?>
<?php include_once(__DIR__ . '/../common/login_check.php'); ?>
<?php
$req = null;
if (isset($_SESSION['targetRequest'])) {
    $req = $_SESSION['targetRequest'];
    if ($req->isNewEntry) {
        $req->isNewEntry = false;
    } else {
        $req = null;
    }
}

if ($req == null) {
    if (isset($_POST['reqId'])) {
        $req = new Request();
        $req->select($_POST['reqId']);
    } else {
        setMessage('不正アクセスです。');
        header('Location: '.getContextRoot().'/');
        exit;
    }
}

$item = new Item();
$items = $item->selectSell(getUserId());
$_SESSION['targetRequest'] = $req;

$page = 1;
$page = empty($_POST['page']) ? '1' : $_POST['page'];
if (!empty($_GET['page'])) {
    $page = $_GET['page'];
}
?>
<?php include(__DIR__ . '/../user_header.php'); ?>
<div class="com-header-top">
    <div class="com-header-top__img">
        <img src="<?php echo HOME_URL; ?>/common/assets/img/mypage/top-bg.png" alt="">
    </div>
    <div class="com-header-top__path">
        <p><span><a href="<?php echo HOME_URL; ?>/" class="clr-yel">トップページ</a></span><span> > </span><span><a
                    href="<?php echo HOME_URL; ?>/user/mypage.php" class="clr-yel">マイページ</a></span><span> >
            </span><span>出品商品一覧</span></p>
    </div>
    <!--     <div class="com-header-top__txt">
            <p class="clr-white">マイページ</p>
        </div>-->
</div>
    <div class="com-container bg-yellow">
        <?php include('usersidebar.php'); ?>
        <div class="com-content">
            <div class="bg-inner">
                <h1>既に出品中の商品を紹介する（※商品名をクリックしてください）</h1>
                <table class="table table-bordered">
                    <tr>
                        <th>画像</th>
                        <th>タイトル</th>
                        <th>商品価格</th>
                        <!--<th>交渉数</th>-->
                        <!--                        <th>掲載日</th>-->
                        <!--<th>状態</th>-->

                    </tr>
                    <?php
                    $count = 0;
                    $index = 0;
                    foreach ($items as $item): ?>
                        <?php if ($item->order_id != null) continue; ?>
                        <?php if ($item->enabled != 1) continue; ?>
                        <?php
                        $count++;
                        if (ceil(++$index / MAX_PAGE_COUNT) != $page) {
                            continue;
                        }
                        ?>
                        <tr>
                            <td>
                                <img width="100" height="auto" src="<?php echo HOME_URL;?>/images/item/id<?= $item->id ?>/thumbnail.jpg" alt=""/>
                            </td>
                            <td><a href="offer.php?item_id=<?= $item->id ?>"><?= $item->title ?></a></td>
                            <td><?= number_format($item->price) ?>円</td>
                            <!--<td><?= number_format($item->negotiation_count) ?>件</td>-->
                            <!--  <td>--><?php //echo $item->regist_date ?><!--</td>-->
                            <!--<td><?= empty($item->order_id) ? '販売中' : '売約済み' ?></td>-->
                        </tr>
                    <?php endforeach; ?>
                </table>

                <div class="text-center margin-20">
                    <?php
                    $prev_page = $page - 1;
                    $max_page = ceil($count / MAX_PAGE_COUNT);
                    $next_page = min($max_page, $page + 1);
                    $view_start_page = max(1, $page - ceil(VIEW_PAGE_SELECT_COUNT / 2));
                    $view_last_page = min($view_start_page + VIEW_PAGE_SELECT_COUNT - 1, $max_page);
                    if ($view_last_page - $view_start_page < VIEW_PAGE_SELECT_COUNT) {
                        $view_start_page = max(1, $view_last_page - VIEW_PAGE_SELECT_COUNT + 1);
                    }
                    ?>
                    <?php if ($prev_page > 0): ?>
                        <a class="sortButton" href=""
                            onclick="submitForm('fm-param', 'page', '<?= $prev_page ?>'); return false;">前へ</a>
                    <?php else: ?>
                        <a class="sortButton" href="" onclick="return false;">前へ</a>
                    <?php endif; ?>

                    <?php for ($i = $view_start_page; $i <= $view_last_page; $i++): ?>
                        <?php if ($page != $i): ?>
                            <a class="sortButton" href=""
                                onclick="submitForm('fm-param', 'page', '<?= $i ?>'); return false;"><?= $i ?></a>
                        <?php else: ?>
                            <a class="sortButton active" href="" onclick="return false;"><?= $i ?></a>
                        <?php endif; ?>
                    <?php endfor; ?>

                    <?php if ($page < $max_page): ?>
                        <a class="sortButton" href=""
                            onclick="submitForm('fm-param', 'page', '<?= $next_page ?>'); return false;">次へ</a>
                    <?php else: ?>
                        <a class="sortButton" href="" onclick="return false;">次へ</a>
                    <?php endif; ?>

                    <p class="small"><?= number_format(($page - 1) * MAX_PAGE_COUNT + 1) ?>
                        ～<?= number_format(min($count, $page * MAX_PAGE_COUNT)) ?>件/<?= number_format($count) ?>
                        件</p>
                </div>
                <div class="card card-body">
                    <h2>出品して紹介する</h2>

                    <p>出品して紹介する場合の手順</p>
                    <ol>
                        <li>出品するボタンを押します。</li>
                        <li>出品フォームが表示されますので、商品の情報を登録します。</li>
                        <li>出品が承認されますと、「既に出品中の商品を紹介する」の一覧へ表示されます。</li>
                        <li>既に出品中の商品を紹介するの中から紹介したい商品を選択します。</li>
                        <li>相手に紹介するコメントを入力して「紹介する」ボタンを押すと紹介完了です。</li>
                    </ol>
                    <p>※商品を出品しただけでは相手に案内されませんのでご注意下さい。</p>

                    <form method="post" action="sell.php">
                        <input type="hidden" name="offer" value="offer"/>
                        <input class="btn btn-block btn-info" type="submit" value="出品する"/>
                        <!--<div class="text-center button-attention">
                            出品して紹介すると購入希望者に通知されます。
                        </div>-->
                    </form>
                </div>
            </div>
        </div>
    </div>

<?php include('../footer.php'); ?>