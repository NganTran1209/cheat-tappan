<?php 
$title_page = '仮登録';
$description_page = '欲しい商品を画像で確認・届いた商品を画像で照合！気持ち良い取引が叶うオンラインマーケット。';
 ?>
<?php include_once(__DIR__ . '/../common/util.php'); ?>

<?php include(__DIR__ . '/../other_header.php'); ?>
<div class="container">
    <div class="alert alert-success " style="font-size:1.5rem">
        <strong>ユーザー仮登録を行いました。</strong><br>
        ご登録いただいたメールアドレスへ仮登録通知メールを送付いたしました。<br>
        メール本文のURLをクリックして本登録をしてください。<br>
        <a class="back-login" href="<?php echo HOME_URL; ?>/login.php">ログインの場合はこちら</a>
    </div>
</div>
<?php include('../footer.php'); ?>




