<?php 
$title_page = '全ての評価';
$description_page = '欲しい商品を画像で確認・届いた商品を画像で照合！気持ち良い取引が叶うオンラインマーケット。';
 ?>
<?php include_once(__DIR__ . '/../common/util.php'); ?>

<?php
$user = new User();
$user->select(getUserId());

$title_page = '全ての評価';
?>
<?php include(__DIR__ . '/../user_header.php'); ?>
<div class="com-header-top">
    <div class="com-header-top__img wow animate__animated animate__fadeInUp">
        <img src="<?php echo HOME_URL; ?>/common/assets/img/mypage/top-bg.png" alt="">
    </div>
    <div class="com-header-top__path wow animate__animated animate__fadeInUp">
        <p><span><a href="<?php echo HOME_URL; ?>/index.php" class="clr-yel">トップページ</a></span><span> > </span><span><a href="<?php echo HOME_URL; ?>/user/mypage.php" class="clr-yel">マイページ</a></span><span> > </span><span>全ての評価</span></p>
    </div>
    <!--     <div class="com-header-top__txt">
            <p class="clr-white">マイページ</p>
        </div>-->
</div>

<div class="com-container bg-yellow">
    <?php include('usersidebar.php'); ?>
    <div class="com-content">
        <div class="content-title">
            <h3><span>全ての評価</span></h3>
        </div>
        <div class="qnego-sm-title">
            <p>※表示期間は最終連絡日から90日間です。</p>
        </div>
        <div class="evaluation-content">
            <section class="evaluation-tab-set">
                <ul class="nav nav-tabs" id="myTab" role="tablist">
                    <li class="nav-item waves-effect waves-light">
                    <a class="nav-link active" id="evaluation-tab" data-toggle="tab" href="#evaluation" role="tab" aria-controls="evaluation" aria-selected="false">全ての評価</a>
                    </li>
                    <li class="nav-item waves-effect waves-light">
                    <a class="nav-link" id="evaluation__purchase-tab" data-toggle="tab" href="#evaluation__purchase" role="tab" aria-controls="evaluation__purchase" aria-selected="false">購入の評価</a>
                    </li>
                    <li class="nav-item waves-effect waves-light">
                    <a class="nav-link" id="evaluation__listing-tab" data-toggle="tab" href="#evaluation__listing" role="tab" aria-controls="evaluation__listing" aria-selected="true">出品の評価</a>
                    </li>
                </ul>
                <div class="tab-content" id="myTabContent">
                    <div class="tab-pane active fade show" id="evaluation" role="tabpanel" aria-labelledby="evaluation-tab">
                        <div class="evaluation-item">
                            <div class="evaluation-content__top">
                                総合評価 : 1（<i class="bi bi-emoji-heart-eyes icon__good__eval"></i> 1　<i class="bi bi-emoji-neutral icon__middle__eval"></i> 0　<i class="bi bi-emoji-angry icon__bad__eval"></i> 0）
                            </div>
                            <div class="evaluation-content__title">
                                <h3>総合評価の見方</h3>
                            </div>
                            <div class="evaluation-content__exam">
                                <p>
                                    取引相手から「良い」の評価は+1、「普通」は+0、「悪い」は-1となります。<br>
                                    例えば「良い <i class="bi bi-emoji-heart-eyes icon__good__eval"></i>」が1、「悪い <i class="bi bi-emoji-neutral icon__middle__eval"></i>」が1の場合、総合評価は±0になります。
                                </p>
                            </div>
                        </div>
                        <div class="evaluation-item-show">
                            <div class="evaluation-item-show__btn">
                                <a href="#">良い</a>
                                <a href="#">普通</a>
                                <a href="#">悪い</a>
                            </div>
                            <div class="evaluation-item-show-result">
                                <div class="evaluation-result-person">
                                    <div class="evaluation-result-person__img">
                                        <img src="<?php echo HOME_URL; ?>/common/assets/img/common/icon/profile.png" alt="">
                                    </div>
                                    <div class="evaluation-result-person__name">
                                        <p>匿名希望さん</p>
                                    </div>
                                </div>
                                <div class="evaluation-result-content">
                                    <div class="evaluation-result-content-head">
                                        <div class="evaluation-result-content-head__level">
                                            <p>評価 : <i class="bi bi-emoji-heart-eyes icon__good__eval"></i></p>
                                        </div>
                                        <div class="evaluation-result-content-head__sex">
                                            <p>レディース</p>
                                        </div>
                                    </div>
                                    <div class="evaluation-result-content__txt">
                                        <p>良いと思います。</p>
                                        <p><i class="bi bi-alarm mr-1 "></i>2021-03-06  12：34：56</p>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="tab-pane review-tab" id="evaluation__purchase" role="tabpanel" aria-labelledby="evaluation__purchase-tab">
                        <div class="evaluation-item">
                        購入の評価の内容を入力してください。<br>
                        よろしくお願いいたします。
                        </div>
                        
                    </div>
                    <div class="tab-pane fade review-tab" id="evaluation__listing" role="tabpanel" aria-labelledby="evaluation__listing-tab">
                    <div class="evaluation-item">
                        出品の評価の内容を入力してください。<br>
                        よろしくお願いいたします。
</div>
                    </div>
                </div>
                </section>
        </div>
    </div>
</div>

<?php include('../user_footer.php'); ?>