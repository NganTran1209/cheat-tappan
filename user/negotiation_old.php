<?php include_once(__DIR__ . '/../common/util.php'); ?>
<?php
if (!isLogin()) {
    setMessage('不正なアクセスです。');
    header('Location: ' . getContextRoot() . '/');
    exit;
}

$item = new Item();
$item->select($_GET['item_id']);
if ($item->enabled == 9) {
    setMessage('出品が取り消されました。');
    header('Location: ' . getContextRoot() . '/');
    exit;
} elseif ($item->owner_id == getUserId()) {
    if (BlockAccess::isBlock($_GET['user_id'])) {
        setMessage('このユーザーと取引はできません。', true);
        header('Location: ' . getContextRoot() . '/');
        exit;
    }
} else {
    if ($item->isBlock()) {
        setMessage('このユーザーと取引はできません。', true);
        header('Location: ' . getContextRoot() . '/');
        exit;
    }
}

$negotiation = new Negotiation();
$order = new Order();
$order->select($item->order_id);

$system_config = SystemConfig::select();

if (isset($_POST['action'])) {
    if ($_POST['action'] == 'buy') {
        $order = new Order();
        $order->item_id = $_POST['item_id'];
        $order->user_id = getUserId();
        $order->state = 0;

        $order->regist();
        $item->select($order->item_id);
        setMessage('購入しました。</br>送付先やお支払のやり取りを開始してください。 ');
    } elseif ($_POST['action'] == 'comment') {
        if (!empty($_POST['comment'])
            || (!empty($_FILES['photo']) && $_FILES['photo']['error'] == 0 && isImage($_FILES['photo']['tmp_name']))
            || (!empty($_FILES['pdf']) && $_FILES['pdf']['error'] == 0 && $_FILES["pdf"]["type"] == "application/pdf")) {

            $negotiation->comment = empty($_POST['comment']) ? '' : $_POST['comment'];
            $negotiation->kbn = $_POST['kbn'];
            $negotiation->item_id = $_POST['item_id'];
            $negotiation->user_id = $_POST['user_id'];
            if (!empty($_FILES['photo']) && $_FILES['photo']['error'] == 0) {
                $negotiation->photo = rotateImage(file_get_contents($_FILES['photo']['tmp_name']));
            } else {
                $negotiation->photo = null;
            }

            if (!empty($_FILES['pdf']) && $_FILES['pdf']['error'] == 0) {
                $negotiation->pdf = file_get_contents($_FILES['pdf']['tmp_name']);
            } else {
                $negotiation->pdf = null;
            }

            if (checkToken()) {
                $negotiation->regist();
                setMessage('コメントを送信しました。');
            } else {
                setMessage('重複投稿をブロックしました。');
            }
        } else {
            setMessage('コメントまたは画像( jpg,jpeg,png,gif )、PDFファイルを指定してください。');
        }
    } elseif ($_POST['action'] == 'blockAccess') {
        $block = new BlockAccess();
        $block->user_id = getUserId();
        $block->block_user_id = $_POST['user_id'];
        $block->regist();
        setMessage('対象のユーザーをブロックしました。');
        header('Location: ' . getContextRoot() . '/');
        exit();
    } elseif ($_POST['action'] == 'procedure') {
        $order->id = $_POST['order_id'];
        if ($order->state >= $_POST['state']) {
            setMessage('既にご連絡済みです。');
        } else {
            $order->state = $_POST['state'];
            $order->updateProcedure();
            switch ($order->state) {
                case 1:
                    setMessage('お振込み完了のご連絡をしました。');
                    break;
                case 2:
                    setMessage('納品のご連絡をしました。');
                    break;
                case 3:
                    setMessage('受取のご連絡をしました。');
                    break;
                case 0:
                default:
                    setMessage('不正なアクセスです。', true);
                    header('Location: ' . getContextRoot() . '/');
                    exit;
            }

        }
    } elseif ($_POST['action'] == 'bank-transfer') {
        if ($order->state >= $_POST['state']) {
            setMessage('既にご連絡済みです。');
        } else {
            $order->updateOrderForBankTransferEntry();
            $cash_flow = new CashFlow();
            $cash_flow->user_id = $order->owner->id;
            $cash_flow->amount = $order->item->price;
            $cash_flow->order_id = $order->id;
            $cash_flow->regist();
            setMessage('振込完了を連絡しました。');
        }

    }
}

$item->select($_GET['item_id']);
$order->select($item->order_id);

if ($item->enabled == 9) {
    setMessage('出品が取り消されました。');
    header('Location: ' . getContextRoot() . '/');
    exit;
} elseif ($item->owner_id == getUserId()) {
    if (BlockAccess::isBlock($_GET['user_id'])) {
        setMessage('このユーザーと取引はできません。', true);
        header('Location: ' . getContextRoot() . '/');
        exit;
    }
} else {
    if ($item->isBlock()) {
        setMessage('このユーザーと取引はできません。', true);
        header('Location: ' . getContextRoot() . '/');
        exit;
    }
}


$negotiation->item_id = $item->id;
$negotiation->user_id = $_GET['user_id'];
list($before, $after) = $negotiation->select2();

$user_evaluate = new Evaluate();
$user_evaluate->selectSummary($_GET['user_id']);

$owner = new User();
$owner->select($item->owner_id);
if ($owner->id != getUserId() && $_GET['user_id'] != getUserId()) {
    setMessage('不正なアクセスです。');
    header('Location: ' . getContextRoot() . '/');
    exit;
}
$view_evaluate = true;
//$noimagepng = "http://192.168.55.10/ccsystem/images/noimage.png";
?>
<?php include('../header.php'); ?>
    <div class="container">
        <div class="row">
            <div class="col-md-3 sideContents">
                <?php include('usersidebar.php'); ?>
            </div>
            <div class="col-md-9 mainContents">
                <div class="bg-inner">
                    <h1><?= $item->title ?></h1>
                    <div class="row">
                        <?php include_once(__DIR__ . '/../common/parts/item_image_area.php'); ?>
                        <?php include_once(__DIR__ . '/../common/parts/item_info_area.php'); ?>
                    </div>
                    <div class="mb-5">
                        <div class="text-center">
                            <span class="font30 fontBold fontRed"><?= number_format($item->price) ?></span>
                            <span class="smallEX">円(税込 <?= $item->carriage_plan_name ?>)</span>
                        </div>
                        <?php if (!isLogin()): ?>
                            <div class="row mb-3">
                                <div class="col-md-12">
                                    <a class="btn btn-block btn-sm  btn-info"
                                       href="<?php echo HOME_URL; ?>/user/userentry.php">無料会員登録</a>
                                    <div class="text-center fontBold">無料会員登録すると購入できます。</div>
                                </div>
                            </div>
                        <?php elseif (getUserId() != $owner->id) : ?>
                            <div class="row mb-3">
                                <?php if ($item->order_id == null || $item->buyer_id == getUserId()): ?>
                                    <div class="col-md-12">
                                        <form method="post" onsubmit="return confirm('購入します。'); ">
                                            <input type="hidden" name="item_id" value="<?= $item->id ?>"/>
                                            <input type="hidden" name="action" value="buy"/>
                                            <?php if ($item->order_id == null): ?>
                                                <input class="btn btn-block btn-outline-danger btn-sm" type="submit"
                                                       value="購入する"/>
                                            <?php else: ?>
                                                <input class="btn btn-block btn-danger btn-sm" type="submit"
                                                       value="購入済" disabled="disabled"/>
                                            <?php endif; ?>
                                        </form>
                                    </div>
                                <?php endif; ?>
                            </div>
                        <?php else: ?>

                        <?php endif; ?>
                    </div>
                    <div class="itemContentText">
                        <?php echo nl2br(htmlentities($item->remarks)); ?>
                    </div>
                    <?php /*?>
<section class="blockAccessWrap">
<?php if ($owner->id != getUserId()): ?>
<div class="text-right">
<form id="block-user" method="post" onsubmit="return window.confirm('出品者をブロックする。'); ">
  <input type="hidden" name="action" value="blockAccess" />
  <input type="hidden" name="item_id" value="<?= $item->id ?>"/>
  <input type="hidden" name="user_id" value="<?= $comment->user_id ?>"/>
  <a href="#" onclick="submitForm('block-user');"><i class="fa fa-times" aria-hidden="true"></i> この出品者をブロックする</a>
</form>
</div>
<?php endif; ?>
<?php if ($negotiation->user_id!= getUserId()): ?>
<div class="text-right">
<form id="block-user" method="post" onsubmit="return window.confirm('このユーザーをブロックする。'); ">
  <input type="hidden" name="action" value="blockAccess" />
  <input type="hidden" name="item_id" value="<?= $item->id ?>"/>
  <input type="hidden" name="user_id" value="<?= $negotiation->user_id?>"/>
  <a href="#" onclick="submitForm('block-user');"><i class="fa fa-times" aria-hidden="true"></i> このユーザーをブロックする</a>
</form>
</div>
<?php endif; ?>
</section>
<?php */ ?>
                    <!-- 2018.07.17 ▼ ユーザーの情報をここに移動。 ▼ -->
                    <?php if ($item->order_id != null && ($item->buyer_id == getUserId() || $item->owner_id == getUserId())): ?>
                        <div class="row mb-5">
                            <div class="col-md-6">
                                <table class="table table-bordered">
                                    <thead class="thead-light">
                                    <tr>
                                        <th colspan="2" class="bgGray">出品者情報</th>
                                    </tr>
                                    </thead>
                                    <tbody class="userinfo">
                                    <tr>
                                        <th>ID</th>
                                        <td>
                                            <?= $owner->hash_id ?>
                                        </td>
                                    </tr>
                                    <tr>
                                        <th>ニックネーム</th>
                                        <td>
                                            <?= $owner->nickname ?>
                                        </td>
                                    </tr>
                                    <!--                                <tr>-->
                                    <!--                                    <th>郵便番号</th>-->
                                    <!--                                    <td>-->
                                    <!--                                        --><?php //echo $owner->zip; ?>
                                    <!--                                    </td>-->
                                    <!--                                </tr>-->
                                    <!--                                <tr>-->
                                    <!--                                    <th>住所</th>-->
                                    <!--                                    <td>-->
                                    <!--                                        --><?php //echo $owner->addr; ?>
                                    <!--                                    </td>-->
                                    <!--                                </tr>-->
                                    <!--                                <tr>-->
                                    <!--                                    <th>電話番号</th>-->
                                    <!--                                    <td>-->
                                    <!--                                        --><?php //echo $owner->tel; ?>
                                    <!--                                    </td>-->
                                    <!--                                </tr>-->
                                    <!--                                <tr>-->
                                    <!--                                    <th>メール</th>-->
                                    <!--                                    <td>-->
                                    <!--                                        --><?php //echo $owner->email; ?>
                                    <!--                                    </td>-->
                                    <!--                                </tr>-->
                                    </tbody>
                                </table>
                            </div>
                            <div class="col-md-6">
                                <?php
                                $buyer = new User();
                                $buyer->select($item->buyer_id);
                                ?>
                                <table class="table table-bordered">
                                    <thead class="thead-light">
                                    <tr>
                                        <th colspan="2" class="bgGray">購入者情報</th>
                                    </tr>
                                    </thead>
                                    <tbody class="userinfo">
                                    <tr>
                                        <th>ID</th>
                                        <td>
                                            <?= $buyer->hash_id ?>
                                        </td>
                                    </tr>
                                    <tr>
                                        <th>ニックネーム</th>
                                        <td>
                                            <?= $buyer->nickname ?>
                                        </td>
                                    </tr>
                                    <!--                                <tr>-->
                                    <!--                                    <th>郵便番号</th>-->
                                    <!--                                    <td>-->
                                    <!--                                        --><?php //echo $buyer->zip; ?>
                                    <!--                                    </td>-->
                                    <!--                                </tr>-->
                                    <!--                                <tr>-->
                                    <!--                                    <th>住所</th>-->
                                    <!--                                    <td>-->
                                    <!--                                        --><?php //echo $buyer->addr; ?>
                                    <!--                                    </td>-->
                                    <!--                                </tr>-->
                                    <!--                                <tr>-->
                                    <!--                                    <th>電話番号</th>-->
                                    <!--                                    <td>-->
                                    <!--                                        --><?php //echo $buyer->tel; ?>
                                    <!--                                    </td>-->
                                    <!--                                </tr>-->
                                    <!--                                <tr>-->
                                    <!--                                    <th>メール</th>-->
                                    <!--                                    <td>-->
                                    <!--                                        --><?php //echo $buyer->email; ?>
                                    <!--                                    </td>-->
                                    <!--                                </tr>-->
                                    </tbody>
                                </table>

                                <?php if ($order->cash_flow == 2): ?>
                                    <?php if ($order->user_id == getUserId()): ?>
                                        <div class="alert alert-success text-center font15" role="alert">
                                            <div class="alert-heading">支払済</div>
                                        </div>
                                    <?php else:?>
                                        <div class="alert alert-success text-center font15" role="alert">
                                            <div class="alert-heading">入金済</div>
                                        </div>
                                    <?php endif;?>
                                <?php elseif ($order->cash_flow == 1): ?>
                                    <div class="alert alert-danger text-center font15" role="alert">
                                        <div class="alert-heading">入金処理中</div>
                                    </div>
                                <?php else:?>
                                    <div class="alert alert-danger text-center font15" role="alert">
                                        <div class="alert-heading">未入金</div>
                                    </div>
                                <?php endif; ?>

                            </div>
                        </div>

                        <div class="stepStatus mb-5">
                            <h2 class="bg-light p-2 fontBold">取引ナビ</h2>

                            <ul class="row text-center p-0 my-4">
                                <li class="col-3">取引情報</li>
                                <li class="col-3">お支払い</li>
                                <li class="col-3">納品連絡</li>
                                <li class="col-3">受取連絡</li>
                                <li class="col-3"><span class="btn-circle btn-circle-ok"></span> </li>
                                <li class="col-3"><span class="btn-circle <?= $order->state >= 1 ? 'btn-circle-ok' : '' ?>"></span> </li>
                                <li class="col-3"><span class="btn-circle <?= $order->state >= 2 ? 'btn-circle-ok' : '' ?>"></span> </li>
                                <li class="col-3"><span class="btn-circle <?= $order->state >= 3 ? 'btn-circle-ok' : '' ?>"></span> </li>
                            </ul>
                            <div class="stepStatusLine stepStatusLineStart<?= $order->state >= 1 ? ' stepLineBorderOne' : '' ?><?= $order->state >= 2 ? ' stepLineBorderTwo' : '' ?><?= $order->state >= 3 ? ' stepLineBorderThree' : '' ?>"></div>



                            <?php if ($order->state == 0 && $order->user_id == getUserId()): ?>
                                <p class="fontBold">
                                    代金のお振込完了後、必ず下の<span class="fontRed">「お振込完了のご連絡」ボタンを押して</span>出品者に通知してください。
                                </p>
                                <?php if ($order->cash_flow == 1 && $order->payment == 1): ?>
                                    <form class="text-center m-auto w-75" action="" method="post">
                                        <input type="hidden" name="order_id"
                                               value="<?= $item->order_id ?>"/>
                                        <input type="hidden" name="state" value="1"/>
                                        <input type="hidden" name="action" value="bank-transfer"/>
                                        <input class="btn btn-block btn-info" type="submit" value="お振込完了のご連絡"/>
                                    </form>
                                <?php elseif ($order->cash_flow == 0):?>
                                    <form class="text-center w-75 mx-auto mb-4" action="bank_transfer.php" method="post" onsubmit="return confirm('購入金額を銀行振り込みでお支払いします。');">
                                        <input type="hidden" name="item_id" value="<?= $item->id ?>"/>
                                        <input class="btn btn-block btn-info" type="submit" value="銀行振り込みで購入する"/>
                                    </form>
                                    <?php if ($system_config->use_stripe): ?>
                                        <form class="text-center w-75 mx-auto mb-4" action="card_payment.php" method="post" onsubmit="return confirm('購入金額をクレジットカードでお支払いします。');">
                                            <input type="hidden" name="item_id" value="<?= $item->id ?>"/>
                                            <input class="btn btn-block btn-info" type="submit" value="クレジットカードで購入する"/>
                                        </form>
                                    <?php endif; ?>
                                <?php endif; ?>
                            <?php endif; ?>

                            <?php //if ($item->with_delivery && $order->state == 1 && $item->owner_id == getUserId()): ?>
                            <?php if ($order->state == 1 && $item->owner_id == getUserId()): ?>
                                <?php if ($order->cash_flow == 2): ?>
                                    <p class="fontBold">
                                        納品後、必ず下の<span class="fontRed">「納品連絡」ボタンを押して</span>購入者に通知してください。
                                    </p>
                                    <form class="text-center m-auto w-75" action="" method="post">
                                        <input type="hidden" name="order_id"
                                               value="<?= $item->order_id ?>"/>
                                        <input type="hidden" name="state" value="2"/>
                                        <input type="hidden" name="action" value="procedure"/>
                                        <input class="btn btn-block btn-info" type="submit" value="納品連絡"/>
                                    </form>
                                <?php endif; ?>
                            <?php endif; ?>

                            <?php //if ($item->with_delivery && $order->state == 2 && $order->user_id == getUserId()): ?>
                            <?php if ($order->state == 2 && $order->user_id == getUserId()): ?>
                                <p class="fontBold">
                                    サービスや商品を受け取ったら、必ず下の<span class="fontRed">「受取連絡」ボタンを押して</span>出品者に通知してください。
                                </p>
                                <form class="text-center m-auto w-75" action="" method="post">
                                    <input type="hidden" name="order_id"
                                           value="<?= $item->order_id ?>"/>
                                    <input type="hidden" name="state" value="3"/>
                                    <input type="hidden" name="action" value="procedure"/>
                                    <input class="btn btn-block btn-info" type="submit" value="受取連絡"/>
                                </form>
                            <?php endif; ?>

                        </div>

                    <?php endif; ?>
                    <!-- 2018.07.17 ▲ ユーザーの情報をここに移動。 ▲ -->
                    <div class="wrap-comment">
                        <?php if ($item->order_id != null && ($item->buyer_id == getUserId() || $item->owner_id == getUserId())): ?>
                            <div class="mb-5">
                                <p>
                                    ※購入者は、お早めのお振込。出品者は、ご入金確認後、速やかに納品をお願いいたします。<br>
                                    ※役務の場合、出品者は、ご入金確認後、<span class="fontRed">役務（サービスの提供）を開始前に「納品のご連絡」、
                                            購入者は役務が終了したら「受領のご連絡」ボタンを押してください。</span>
                                </p>
                                <div class="card card-body">
                                    <p class="mb-0">
                                        <strong class="fontRed pr-2">重要!</strong>取引の連絡は、
                                        以下の「取引連絡のコメント」以外は禁止となります。
                                        重大な問題が発生した場合を除いて取引相手に直接電話やメールを送るなどのすることは禁止です。
                                        この場合、利用規約の禁止事項違反とみなしてIDの利用を停止する場合があります。
                                    </p>
                                </div>
                            </div>
                            <!-- 2018.07.17 ここにあったユーザーの情報を上に移動。 -->
                        <?php endif; ?>

                        <?php if (!User::isInvalid($item->owner_id)
                            && ($item->order_id == null || $item->buyer_id == getUserId()
                                || ($item->owner_id == getUserId()
                                    && ($item->buyer_id == null || !User::isInvalid($item->buyer_id))))): ?>
                            <form action="" enctype="multipart/form-data" method="post">
                                <h2 class="bg-light p-2 fontBold">コメント</h2>
                                <input type="hidden" name="item_id" value="<?= $item->id ?>"/>
                                <input type="hidden" name="user_id" value="<?= $_GET['user_id'] ?>"/>
                                <input type="hidden" name="kbn"
                                       value="<?= $owner->id == getUserId() ? '2' : '1' ?>"/>
                                <input type="hidden" name="action" value="comment"/>
                                <input type="hidden" name="token" value="<?= createToken(); ?>"/>

                                <div class="my-4">
                                <textarea class="form-control" rows="5" name="comment"
                                          placeholder="コメントを入力してください"></textarea>
                                </div>

                                <?php if (!empty($item->buyer_id)): ?>
                                    <div class="mb-5">
                                        <div class="row">
                                            <div class="col-md-12">

                                                <div class="input-group mb-2">
                                                    <div class="custom-file">
                                                        <input type="file" name="photo" id="lefile"
                                                               class="custom-file-input">
                                                        <label class="custom-file-label" for="photo" data-browse="参照">画像ファイル選択...</label>
                                                    </div>
                                                    <div class="custom-file">
                                                        <input type="file" name="pdf" id="lefile"
                                                               class="custom-file-input">
                                                        <label class="custom-file-label" for="pdf" data-browse="参照">PDFファイル選択...</label>
                                                    </div>
                                                </div>

                                                <!--                                            <input id="lefile" type="file" name="photo" style="display:none">-->
                                                <!--                                            <div class="input-group">-->
                                                <!--                                                <input type="text" id="photoCover" class="form-control"-->
                                                <!--                                                       placeholder="ファイルを選択">-->
                                                <!--                                                <button type="button"-->
                                                <!--                                                        class="btn btn-lapias-blue"-->
                                                <!--                                                        onclick="$('input[id=lefile]').click();">画像を投稿する-->
                                                <!--                                                </button>-->
                                                <!--                                            </div>-->
                                                <!--                                            <script>-->
                                                <!--                                                $('input[id=lefile]').change(function () {-->
                                                <!--                                                    $('#photoCover').val($(this).val());-->
                                                <!--                                                });-->
                                                <!--                                            </script>-->
                                            </div>
                                            <div class="col-md-12">
                                                <p>
                                                    ※役務やサービスで写真や画像が必要な場合は「画像ファイル選択...」ボタンよりアップロードし
                                                    「取引連絡のコメントする」ボタンで相手に送ることができます。<br>
                                                    ※送信できるのは3MB 以下のjpg,gif,png,jpeg形式ファイルのみです。
                                                </p>
                                            </div>
                                        </div>
                                    </div>
                                <?php endif; ?>


                                <div class="mb-4">
                                    <?php if (empty($item->buyer_id)): ?>
                                        <input class="btn btn-block btn-info" type="submit"
                                               value="質問・交渉でコメントする"/>
                                    <?php else: ?>
                                        <input class="btn btn-block btn-info" type="submit"
                                               value="取引連絡のコメントする"/>
                                    <?php endif; ?>
                                </div>
                            </form>
                        <?php endif; ?>

                        <?php if (count($after) > 0): ?>
                            <?php foreach ($after as $comment): ?>
                                <?php
                                $commenter = new User();
                                $commenter->select($comment->user_id);
                                ?>
                                <div class="row mb-4">
                                    <div class="col-md-3">
                                        <div class="balloonUser">
                                            <?php if ($comment->kbn == 1): ?>
                                                <?php /*?><?= $comment->user_name ?><?php */ ?>
                                                ID:<?= $commenter->hash_id ?>
                                                <div class="commenter">
                                                    <a href="<?php echo HOME_URL; ?>/evaluate.php?user_id=<?= $comment->user_id ?>">評価</a>
                                                    <?= $user_evaluate->point ?>
                                                </div>
                                                <?php /*?>
                                                        <?php if ($commenter->id != getUserId()): ?>
                                                        <form id="block-user" method="post" onsubmit="return window.confirm('このユーザーをブロックする。'); ">
                                                        <input type="hidden" name="action" value="blockAccess" />
                                                        <input type="hidden" name="item_id" value="<?= $item->id ?>"/>
                                                        <input type="hidden" name="user_id" value="<?= $comment->user_id ?>"/>
                                                        <a href="#" onclick="submitForm('block-user');">このユーザーをブロックする</a>
                                                        </form>
                                                        <?php endif; ?>
                                                        <?php */ ?>
                                            <?php else: ?>
                                                <?php /*?><?= $owner->name ?><?php */ ?>
                                                ID:<?= $owner->hash_id ?>
                                                <div class="owner">
                                                    <span>出品者</span>
                                                </div>
                                                <?php /*?>
                                                        <?php if ($owner->id != getUserId()): ?>
                                                        <form id="block-user" method="post" onsubmit="return window.confirm('出品者をブロックする。'); ">
                                                        <input type="hidden" name="action" value="blockAccess" />
                                                        <input type="hidden" name="item_id" value="<?= $item->id ?>"/>
                                                        <input type="hidden" name="user_id" value="<?= $comment->user_id ?>"/>
                                                        <a href="#" onclick="submitForm('block-user');">出品者をブロックする</a>
                                                        </form>
                                                        <?php endif; ?>
                                                        <?php */ ?>
                                            <?php endif; ?>
                                        </div>
                                    </div>
                                    <div class="col-md-9">
                                        <div class="bg-comment balloon <?php if ($comment->kbn == 1): ?>balloonA<?php else: ?>balloonB<?php endif; ?>">
                                            <div class="balloonComment">
                                                <?php echo nl2br(htmlentities($comment->comment)); ?>
                                            </div>
                                            <div class="">
                                                <?php if ($comment->hasPhoto()): ?>
                                                    <a href="<?= $comment->getImageUrl() ?>" target="_blank"><img
                                                                class="img-fluid" width="300"
                                                                src="<?= $comment->getImageUrl() ?>"/></a>
                                                <?php endif; ?>
                                                <?php if ($comment->hasPdf()): ?>
                                                    <embed src="<?= $comment->getPdfUrl() ?>" type="application/pdf" width="100%" height="100%">
                                                    <a href="<?php echo $comment->getPdfUrl() ?>" target="_blank">PDFダウンロード</a>
                                                <?php endif; ?>
                                            </div>

                                            <div class="balloonTime">
                                                <i class="far fa-clock mr-2"></i><?= $comment->regist_date ?>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            <?php endforeach; ?>
                        <?php endif; ?>
                    </div>
                    <?php if (count($before) > 0 && ($item->order_id == null || $item->buyer_id == getUserId() || $item->owner_id == getUserId())): ?>

                        <?php /*?><div class="kugiri"><span class="kugiri-title">購入済</span></div><?php */ ?>
                        <div id="koushou-panel" class="wrap-comment">
                            <?php if (!empty($item->buyer_id) && ($item->buyer_id == getUserId() || $item->owner_id == getUserId())): ?>
                                <div class="koushou-title border-bottom text-center my-4">
                                    質問・交渉のコメント履歴
                                </div>
                            <?php endif; ?>

                            <div class="koushou-content">
                                <?php foreach ($before as $comment): ?>
                                    <?php
                                    $commenter = new User();
                                    $commenter->select($comment->user_id);
                                    ?>
                                    <div class="row mb-4">
                                        <div class="col-md-3">
                                            <div class="balloonUser">
                                                <?php if ($comment->kbn == 1): ?>
                                                    <?php /*?><?= $comment->user_name ?><?php */ ?>
                                                    ID:<?= $commenter->hash_id ?>
                                                    <div class="commenter">
                                                        <a href="<?php echo HOME_URL; ?>/evaluate.php?user_id=<?= $comment->user_id ?>">評価</a>
                                                        <?= $user_evaluate->point ?>
                                                    </div>
                                                    <br>


                                                    <?php /*?>						<?php if ($commenter->id != getUserId()): ?>
                                                            <form id="block-user" method="post" onsubmit="return window.confirm('このユーザーをブロックする。'); ">
                                                              <input type="hidden" name="action" value="blockAccess" />
                                                              <input type="hidden" name="item_id" value="<?= $item->id ?>"/>
                                                              <input type="hidden" name="user_id" value="<?= $comment->user_id ?>"/>
                                                              <a href="#" onclick="submitForm('block-user');">このユーザーをブロックする。</a>
                                                            </form>
                                                            <?php endif; ?><?php */ ?>
                                                <?php else: ?>
                                                    <?php /*?><?= $owner->name ?><?php */ ?>
                                                    ID:<?= $owner->hash_id ?>
                                                    <div class="bg-name">
                                                        <span>出品者</span>
                                                    </div>
                                                    <?php /*?>						<?php if ($owner->id != getUserId()): ?>
                                                            <form id="block-user" method="post" onsubmit="return window.confirm('出品者をブロックする。'); ">
                                                              <input type="hidden" name="action" value="blockAccess" />
                                                              <input type="hidden" name="item_id" value="<?= $item->id ?>"/>
                                                              <input type="hidden" name="user_id" value="<?= $comment->user_id ?>"/>
                                                              <a href="#" onclick="submitForm('block-user');">出品者をブロックする。</a>
                                                            </form>
                                                            <?php endif; ?><?php */ ?>
                                                <?php endif; ?>
                                            </div>
                                        </div>
                                        <div class="col-md-9">
                                            <div class="bg-comment balloon <?php if ($comment->kbn == 1): ?>balloonA<?php else: ?>balloonB<?php endif; ?>">
                                                <div class="balloonComment">
                                                    <?php echo nl2br(htmlentities($comment->comment)); ?>
                                                </div>
                                                <div class="">
                                                    <?php if ($comment->hasPhoto()): ?>
                                                        <a href="<?= $comment->getImageUrl() ?>" target="_blank"><img
                                                                    class="img-fluid" width="300"
                                                                    src="<?= $comment->getImageUrl() ?>"/></a>
                                                    <?php endif; ?>
                                                </div>
                                                <div class="balloonTime">
                                                    <i class="far fa-clock mr-2"></i><?= $comment->regist_date ?>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                <?php endforeach; ?>
                            </div>
                        </div>
                    <?php endif; ?>
                </div>
            </div>
        </div>
    </div>

<?php include('../footer.php'); ?>