<?php include_once(__DIR__.'/../common/util.php'); ?>
<?php include_once(__DIR__ . '/../common/login_check.php'); ?>
<?php
$entity = new User();
if (isset($_POST['action'])) {
	if ($_POST['action'] == 'settlement') {
// 		$xml = settlement();
// 		if ($xml->result[0]['result'] == 1) {
// 			header('Location: ' . urldecode($xml->result[1]['redirect']));
// 			exit;
// 		}
// 		else {
// 			setMessage($xml->result[1]['err_code'] . ':' . urldecode($xml->result[2]['err_detail']));
// 		}
	}
	elseif ($_POST['action'] == 'changePlan') {
		$entity->id = getUserId();
		$entity->settle_plan = $_POST['settle_plan'];
		$entity->updateSettlePlan();
		setMessage("プランを変更しました。");
	}
}
$entity->select(getUserId());
?>
<?php include(__DIR__.'/../header.php'); ?>
<div class="container-fluid margin-contents">
    <div class="container">
        <div class="row">
            <!-- <div class="col-sm-3"> -->
                <?php include('usersidebar.php'); ?>
            <!-- </div> -->
            <!-- col-sm-3 -->
            <div class="col-sm-9 archives">

                <div class="">
                        <h1>プラン選択</h1>
                        <form id="frm" method="post" onsubmit="return confirm('プランを変更する。')">
                        <input type="hidden" name="action" value="changePlan">
                        <table class="table table-bordered table-striped">
                            <tr>
                                <th class="col-sm-4">お申込みプラン</th>
                                <td class="col-sm-8">
                                    <select class="form-control" name="settle_plan" onchange="if (frm.onsubmit()) { frm.submit(); }">
                                        <?php createCombo('selectSettlePlan', $entity->settle_plan); ?>
                                    </select>
                                </td>
                            </tr>

                        </table>
                    	</form>
                        <form method="post" onsubmit="return confirm('お申込み')">
                        <div class="row">
                            <div class="col-xs-8 col-xs-offset-2">
                                <input type="hidden" name="action" value="settlement">
                                <input class="btn btn-success btn-block" type="submit" name="regist" value="お申込み"<?= $entity->settle_plan > 0 ? '' : 'disabled="disabled"'?>/>
                            </div>
                        </div>
                    	</form>
                </div>


            </div>
            <!-- col-sm-9 -->

        </div>
        <!-- row -->

    </div>
    <!-- container -->
</div>
<!-- container-fluid -->

<?php include('../footer.php'); ?>




