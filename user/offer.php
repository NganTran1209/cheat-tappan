<?php include_once(__DIR__ . '/../common/util.php'); ?>
<?php include_once(__DIR__ . '/../common/login_check.php'); ?>
<?php

$req = $_SESSION['targetRequest'];
$negotiation = new Negotiation();

if (isset($_POST['action'])) {
    if ($_POST['action'] == 'comment') {
        if (!empty($_POST['comment'])) {
            unset($_SESSION['targetRequest']);
            $follow = new RequestFollow();
            $follow->user_id = getUserId();
            $follow->item_id = $_POST['item_id'];
            $follow->reques_id = $req->id;
            $follow->regist();

            $negotiation->comment = $_POST['comment'];
            $negotiation->kbn = $_POST['kbn'];
            $negotiation->item_id = $_POST['item_id'];
            $negotiation->user_id = $_POST['user_id'];
            $negotiation->isOffer = true;
            $negotiation->regist();

            setMessage('商品を紹介いたしました。');
            header('Location: ' . getContextRoot() . '/');
            exit;
        } else {
            setMessage('コメントは必須です。');
        }
    }
}

$item = new Item();
$item->select($_GET['item_id']);
$owner = new User();
$owner->select($item->owner_id);
$negotiation->item_id = $item->id;
$negotiation->user_id = $req->user_id;
$comments = $negotiation->select();

//評価制御
$evaluate = new Evaluate();
$evaluate->selectSummary($item->owner_id);
?>
<?php include('../header.php'); ?>
<div class="container">
    <div class="row">
        <div class="col-md-3 sideContents">
            <?php include('../sidebar.php'); ?>
        </div>
        <div class="col-md-9 mainContents">
            <div class="bg-inner">
                <h1>商品を購入希望者に紹介する</h1>
                <div class="itemTitle">
                    <h2><?= $item->title ?></h2>
                </div>
                <div class="row">
                    <?php include_once('../common/parts/item_image_area.php'); ?>
                    <?php include_once('../common/parts/item_info_area.php'); ?>
                </div>
                <div class="mb-4">
                    <div class="text-center">
                        <span class="font30 fontBold fontRed"><?= number_format($item->price) ?></span>
                        <span class="smallEX">円(税込・<?= $item->carriage_plan_name ?>)</span>
                    </div>
                </div>
                <div class="itemContentText">
                    <?php echo nl2br(htmlentities($item->remarks)); ?>
                </div>
                <?php if (isLogin() && getUserId() == $owner->id): ?>
                    <?php foreach ($comments as $comment): ?>
                        <div class="row">
                            <div class="col-md-3">
                                <div class="bg-user">
                                    <?php if ($comment->kbn == 1): ?>
                                        <?php /*?><?= $comment->user_name ?><?php */ ?>
                                        <?= $commenter->getViewId() ?>
                                        <?php /*?><div class="bg-name"><span>購入者</span></div><?php */ ?>
                                    <?php else: ?>
                                        <?php /*?><?= $owner->name ?><?php */ ?>
                                        <?= $owner->getViewId() ?>
                                        <div class="bg-name"><span>出品者</span></div>
                                    <?php endif; ?>
                                </div>
                            </div>
                            <div class="col-md-9">
                                <div class="bg-comment balloon2">
                                    <?php echo nl2br(htmlentities($comment->comment)); ?>
                                    <div class="message-time"><i class="fa fa-clock-o"
                                                                 aria-hidden="true"></i> <?= $comment->regist_date ?></div>
                                </div>
                            </div>

                        </div>
                    <?php endforeach; ?>

                    <form action="" method="post">
                        <input type="hidden" name="item_id" value="<?= $item->id ?>"/>
                        <input type="hidden" name="user_id" value="<?= $req->user_id ?>"/>
                        <input type="hidden" name="kbn" value="2"/>
                        <input type="hidden" name="action" value="comment"/>
                        <textarea class="form-control mb-3" rows="5" name="comment" placeholder="コメントを入力してください"></textarea>
                        <div class="text-center">
                            <input class="btn btn-block btn-info" type="submit" value="紹介する"/>
                            <p class="small">購入希望者に通知されます。</p>
                        </div>
                    </form>

                    <?php /*?>
                <?php foreach ($comments as $comment): ?>
                <div class="card mb-3">
                <?php if ($comment->kbn == 1): ?>
                    <div class="card-header">買い手名：<?= $comment->user_name ?></div>
                <?php else:?>
                    <div class="card-header">出品者：<?= $owner->name ?></div>
                <?php endif;?>
                    <div class="card-body">
                    <pre><?= $comment->comment ?></pre>
                        <div class=" text-right">
                            送信日時：<?= $comment->regist_date ?>
                        </div>
                    </div>
                </div>
                <?php endforeach; ?>
                <?php */ ?>
                <?php endif; ?>
            </div>
        </div>
    </div>
</div>

<?php include('../footer.php'); ?>




