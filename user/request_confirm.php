<?php 
$title_page = '購入希望募集確認';
$description_page = '欲しい商品を画像で確認・届いた商品を画像で照合！気持ち良い取引が叶うオンラインマーケット。';
 ?>
<?php include_once(__DIR__ . '/../common/util.php'); ?>
<?php include_once(__DIR__ . '/../common/login_check.php'); ?>
<?php
$request = $_SESSION['request'];
if (isset($_POST['action']) && $_POST['action'] == 'regist') {
    $request->regist();
    unset($_SESSION['request']);
    setMessage('購入希望情報を登録しました。内容の確認、承認後に掲載されます。<br>※掲載後メール通知されます。<br>※掲載不可の場合、メール通知されません。');
    //header('Location: '.getContextRoot().'/user/request.php');
    header('Location: ' . getContextRoot() . '/user/mypage.php');
    exit();
}

$isOffer = false;
if (isset($_POST['offer']) && $_POST['offer'] == 'offer') {
    $isOffer = true;
} else {
    unset($_SESSION['targetRequest']);
}

$title_page = '購入希望募集確認';
?>
<?php include(__DIR__ . '/../user_header.php'); ?>

    <div class="com-container bg-yellow">
        <?php include('usersidebar.php'); ?>

        <div class="com-content">
            <div class="content-title">
                <h3><span>購入希望募集投稿</span></h3>
            </div>
            <div class="sell-content">
            <div class="bg-inner">
                <h1>購入希望商品の情報</h1>
 

 <!--               <p class="small">購入希望商品の情報をご確認ください。</p>-->
                <div class="border-bottom py-4">
                    <h3 class="fontBold my-2">商品名</h3>
                    <p class="size-1_5rem c-cofirm"><?= $request->title ?></p>
                </div>
                <div class="border-bottom py-4">
                    <h3 class="fontBold my-2">説明文</h3>
                    <p class="size-1_5rem c-cofirm"><?php echo nl2br(htmlentities($request->text)); ?></p>
                </div>
                <div class="border-bottom py-4">
                    <h3 class="fontBold my-2">予算</h3>
                    <p class="size-1_5rem c-cofirm"><?= $request->price ?>円</p>
                </div>

                <div class="row py-4">
                        <div class="col-md-6 mb-3">
                            <form method="post">
                                <?php if ($isOffer): ?>
                                    <input type="hidden" name="offer" value="offer"/>
                                <?php endif; ?>
                                <input type="hidden" name="action" value="regist"/>
                                <input class="btn btn-block btn-info btn-sm product-confirm-add-2" type="submit" value="登録する"/>
                            </form>
                        </div>
                        <div class="col-md-6 mb-3">
                            <form method="post" action="request.php">
                                <?php if ($isOffer): ?>
                                    <input type="hidden" name="offer" value="offer"/>
                                <?php endif; ?>
                                <input type="hidden" name="action" value="modify"/>
                                <input class="btn btn-block btn-outline-dark btn-sm product-confirm-add" type="submit" value="修正する"/>
                            </form>
                        </div>
                    </div>


            </div>
        </div>
    </div>
</div>

<?php include('../footer.php'); ?>
