<?php include_once(__DIR__ . '/../common/util.php'); ?>
<?php include_once(__DIR__ . '/../common/login_check.php'); ?>
<?php
//if (!isIdentification()) {
//    setMessage('購入希望の募集が許可されておりません。許可を得るためには「本人確認書類」の提出をお願いします。');
//    header('Location: ' . getContextRoot() . '/user/upload_identification.php');
//    exit;
//}

if (!hasBuying()) {
    setMessage('購入が許可されておりません。');
    header('Location: ' . getContextRoot() . '/user/mypage.php');
    exit;
}

if (isset($_POST['action'])) {
    if ($_POST['action'] == 'modify') {
        $entity = $_SESSION['request'];
    } elseif ($_POST['action'] == 'request') {
        $entity = new Request();
        $entity->title = $_POST['title'];
        $entity->user_id = getUserId();
        $entity->text = $_POST['text'];
        $entity->price = $_POST['price'];
        if ($entity->validate()) {
            $_SESSION['request'] = $entity;

            header('Location: ' . getContextRoot() . '/user/request_confirm.php');
            exit();
        }
    } else {
        $entity = new Request();
    }
} else {
    $entity = new Request();
}
unset($_SESSION['request']);

?>
<?php include('../header.php'); ?>
<div class="container">
    <div class="row">
        <div class="col-md-3 sideContents">
            <?php include('usersidebar.php'); ?>
        </div>
        <div class="col-md-9 mainContents">
            <div class="bg-inner">
                <h1>購入希望を募集</h1>
                <p class="small">購入希望商品の情報を入力してください。</p>
                <form method="post" enctype="multipart/form-data">
                    <input type="hidden" name="action" value="request"/>

                    <div class="border-bottom py-4">
                        <h3 class="fontBold my-2">タイトル</h3>
                        <input class="form-control middle-value" type="text" name="title" value="<?= $entity->title ?>"
                               placeholder="タイトル">
                    </div>
                    <div class="border-bottom py-4">
                        <h3 class="fontBold my-2">説明文</h3>
                        <textarea class="form-control" rows="5" id="" name="text"><?= $entity->text ?></textarea>
                    </div>
                    <div class="border-bottom py-4">
                        <h3 class="fontBold my-2">予算</h3>
                        <input class="form-control middle-value" type="number" name="price" value="<?= $entity->price ?>"
                               placeholder="数字のみ（例10,000円→10000と入力）">
                    </div>
                    <input class="btn btn-block btn-info my-4" type="submit" value="確認する"/>

                </form>
            </div>
        </div>
    </div>
</div>

<?php include('../footer.php'); ?>
