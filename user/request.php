<?php 
$title_page = '購入希望募集投稿';
$description_page = '欲しい商品を画像で確認・届いた商品を画像で照合！気持ち良い取引が叶うオンラインマーケット。';
 ?>
<?php include_once(__DIR__ . '/../common/util.php'); ?>
<?php include_once(__DIR__ . '/../common/login_check.php'); ?>
<?php
//if (!isIdentification()) {
//    setMessage('購入希望の募集が許可されておりません。許可を得るためには「本人確認書類」の提出をお願いします。');
//    header('Location: ' . getContextRoot() . '/user/upload_identification.php');
//    exit;
//}

if (!hasBuying()) {
    setMessage('購入が許可されておりません。');
    header('Location: ' . getContextRoot() . '/user/mypage.php');
    exit;
}

if (isset($_POST['action'])) {
    if ($_POST['action'] == 'modify') {
        $request = $_SESSION['request'];
    } elseif ($_POST['action'] == 'request') {
        $request = new Request();
        $request->title = $_POST['title'];
        $request->user_id = getUserId();
        $request->text = $_POST['text'];
        $request->price = $_POST['price'];
        if ($request->validate()) {
            $_SESSION['request'] = $request;

            header('Location: ' . getContextRoot() . '/user/request_confirm.php');
            exit();
        }
    } else {
        $request = new Request();
    }
} else {
    $request = new Request();
}
// unset($_SESSION['request']);
$title_page = '購入希望募集投稿';
?>
<?php include(__DIR__ . '/../user_header.php'); ?>
    <div class="com-header-top">
        <div class="com-header-top__img wow animate__animated animate__fadeInUp">
            <img src="<?php echo HOME_URL; ?>/common/assets/img/mypage/top-bg.png" alt="">
        </div>
        <div class="com-header-top__path wow animate__animated animate__fadeInUp">
            <p><span><a href="<?php echo HOME_URL; ?>/" class="clr-yel">トップページ</a></span><span> > </span><span><a href="<?php echo HOME_URL; ?>/user/mypage.php" class="clr-yel">マイページ</a></span><span> > </span><span>購入希望募集投稿</span></p>
        </div>
<!--        <div class="com-header-top__txt">
            <p class="clr-white">マイページ</p>
        </div>-->
    </div>
    <div class="com-container bg-yellow">
        <?php include('usersidebar.php'); ?>
        <div class="com-content">
            <form class="c-form01" method="post" enctype="multipart/form-data">
                <input type="hidden" name="action" value="request"/>
                <div class="content-title">
                    <h3><span>購入希望募集投稿</span></h3>
                </div>
                <div class="qnego-sm-title">
                    <p>購入希望商品の情報を入力してください。</p>
                </div>
                <div class="purchase-wishes-content">
                    <div class="purchase-wishes-content-title">
                        <p class="c-form01__label">商品名</p>
                        <input type="text" name="title" value="<?= $request->title ?>"
                               placeholder="タイトル">
                    </div>
                    <div class="purchase-wishes-content__content">
                    <p class="c-form01__label">商品説明
                    </p>
                        <textarea id="" cols="30" rows="10" name="text"><?= $request->text ?></textarea><br>
                        <p>※最大1.5万文字まで入力できます。</p>
                    </div>
                    <div class="purchase-wishes-content-bottom">
                        <p class="c-form01__label">予算</p>
                        <input type="number" name="price" value="<?= $request->price ?>"
                               placeholder="数字のみ（例10,000円→10000と入力）">
                    </div>
                    <div class="purchase-wishes-content-btn">
                        <button type="submit">確認する</button>
                    </div>
                </div>
            </form>
        </div>
    </div>
<?php include('../footer.php'); ?>
