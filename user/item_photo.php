<?php include_once(__DIR__.'/../entity/item.php'); ?>
<?php include_once(__DIR__.'/../common/util.php'); ?>
<?php
header("Content-Type: image/jpeg");
$entity = new Item();
//echo $entity->getPhoto($_GET['id'], $_GET['number']);

$dirpath = __DIR__.'/../images/item/id' . $_GET['id'];
if (!file_exists($dirpath)) {
	mkdir($dirpath, 0777, true);
}

if ($_GET['number'] == '1') {
	$thumbnail_path = $dirpath . '/thumbnail.jpg';
	if (!file_exists($thumbnail_path)) {
		$handler = fopen($thumbnail_path, 'w');
		$result = fwrite($handler, convertThumbnailImage($entity->getPhoto($_GET['id'], $_GET['number'])));
		fclose($handler);
	}
}

$image_path = $dirpath . '/image' . $_GET['number'] . '.jpg';
if (!file_exists($image_path)) {
	$handler = fopen($image_path, 'w');
	$result = fwrite($handler, convertImage($entity->getPhoto($_GET['id'], $_GET['number'])));
	fclose($handler);
}


