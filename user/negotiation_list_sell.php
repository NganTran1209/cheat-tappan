<?php 
$title_page = '質問・交渉中';
$description_page = '欲しい商品を画像で確認・届いた商品を画像で照合！気持ち良い取引が叶うオンラインマーケット。';
 ?>
<?php include_once(__DIR__ . '/../common/util.php'); ?>
<?php include_once(__DIR__ . '/../common/login_check.php'); ?>
<?php
$item = new Item();
$items = $item->selectSell(getUserId());
usort($items, function ($a, $b) {
    if (strtotime($a->last_date) == strtotime($b->last_date)) {
        return 0;
    } elseif (strtotime($a->last_date) > strtotime($b->last_date)) {
        return -1;
    }
    return 1;
});

$page = 1;
$page = empty($_POST['page']) ? '1' : $_POST['page'];
if (!empty($_GET['page'])) {
    $page = $_GET['page'];
}
$mode = isset($_GET['mode']) ? $_GET['mode'] : '';
$negotiation = false;
if (isset($_POST['mode']) && $_POST['mode'] == 'negotiation') {
    $mode = $_POST['mode'];
}
if (isset($mode) && $mode == 'negotiation') {
    $negotiation = true;
}
$title_page = '質問・交渉中';
?>
<?php include(__DIR__ . '/../user_header.php'); ?>
    <div class="com-header-top">
        <div class="com-header-top__img wow animate__animated animate__fadeInUp">
            <img src="<?php echo HOME_URL; ?>/common/assets/img/mypage/top-bg.png" alt="">
        </div>
        <div class="com-header-top__path wow animate__animated animate__fadeInUp">
            <p><span><a href="<?php echo HOME_URL; ?>/" class="clr-yel">トップページ</a></span><span> > </span><span><a href="<?php echo HOME_URL; ?>/user/mypage.php" class="clr-yel">マイページ</a></span>
            <span> > </span><span>質問・交渉中</span></p>
        </div>
        <!--     <div class="com-header-top__txt">
            <p class="clr-white">マイページ</p>
        </div>-->
    </div>
    <form id="fm-param" method="post">
        <input type="hidden" id="mode" name="mode" value="<?= $mode ?>"/>
        <input type="hidden" id="id-page" name="page" value="<?= $page ?>"/>
    </form>
    <div class="com-container bg-yellow">
        <?php include('usersidebar.php'); ?>
        <div class="com-content">
            <div class="content-title">
                <h3><span>質問・交渉中</span></h3>
            </div>
            <div class="qnego-sm-title">
                <p>※表示期間は最終連絡日から90日間です。</p>
            </div>
            <div class="table-responsive">
                <table class="table table-bordered">
                    <thead>
                      <tr>
                        <th scope="col">掲載日</th>
                        <th scope="col">タイトル</th>
                        <th scope="col">価格</th>
                        <th scope="col">編集</th>
                      </tr>
                    </thead>
                    <tbody>
                    <?php
                      if (empty($items)) { ?>
                    <tr>
                        <td colspan="4">データなし</td>
                    </tr>
                    <?php } ?>
                    <?php
                    $count = 0;
                    $index = 0;
                    foreach ($items as $item):
                        if ($item->enabled == 9) {
                            continue;
                        }
                        if (diffDays(date("Y/m/d"), $item->last_date) > MAX_VIEW_DAYS) {
                            continue;
                        }
                        ?>
                        <?php if ($item->negotiation_count == 0) {
                        continue;
                    } ?>
                        <?php if (empty($item->buyer_id)) :
                        $count++;
                        if (ceil(++$index / MAX_PAGE_COUNT) != $page) {
                            continue;
                        }
                        ?>
                            <tr>
                                <td scope="row"><?= $item->last_date ?></th>
                                <td style="color:#71d100"><?= $item->title ?></td>
                                <td><?= number_format($item->price) ?>円</td>
                                <td><a href="<?php echo HOME_URL; ?>/user/transaction.php?item_id=<?= $item->id ?>" class="btn-edit">編集</a></td>
                            </tr>
                        <?php endif; ?>
                    <?php endforeach; ?>
                    </tbody>
                </table>
            </div>

            <?php
                $prev_page = $page - 1;
                $max_page = ceil($count / MAX_PAGE_COUNT);
                $next_page = min($max_page, $page + 1);
                $view_start_page = max(1, $page - ceil(VIEW_PAGE_SELECT_COUNT / 2));
                $view_last_page = min($view_start_page + VIEW_PAGE_SELECT_COUNT - 1, $max_page);
                if ($view_last_page - $view_start_page < VIEW_PAGE_SELECT_COUNT) {
                    $view_start_page = max(1, $view_last_page - VIEW_PAGE_SELECT_COUNT + 1);
                }
            ?>
                <?php if($max_page >1): ?>
                    <div class="content-group-pagination">
                <?php else: ?>
                    <div class="content-group-pagination content-one-page">
                <?php endif; ?>
                
                <?php if($max_page > 1):  ?>
                    <?php if ($prev_page > 0): ?>
                        <a class="pagination__preve" href=""
                            onclick="submitForm('fm-param', 'page', '<?= $prev_page ?>'); return false;"><i class="bi bi-chevron-left"></i></a>
                    <?php else: ?>
                        <a class="pagination__preve" href="" onclick="return false;"><i class="bi bi-chevron-left"></i></a>
                    <?php endif; ?>
                <?php endif; ?>

                <?php for ($i = $view_start_page; $i <= $view_last_page; $i++): ?>
                    <?php if ($page != $i): ?>
                        <a class="pagination__num" href=""
                               onclick="submitForm('fm-param', 'page', '<?= $i ?>'); return false;"><?= $i ?></a>
                    <?php else: ?>
                        <a class="pagination__num pag_active" href="" onclick="return false;"><span><?= $i ?></span></a>
                    <?php endif; ?>
                <?php endfor; ?>
                <?php if($max_page > 1):  ?>
                    <?php if ($page < $max_page): ?>
                        <a id="pagination__next" class="pagination__preve" href=""
                            onclick="submitForm('fm-param', 'page', '<?= $next_page ?>'); return false;"><i class="bi bi-chevron-right"></i></a>
                    <?php else: ?>
                        <a id="pagination__next" class="pagination__preve" href="" onclick="return false;"><i class="bi bi-chevron-right"></i></a>
                    <?php endif; ?>
                <?php endif; ?>
            </div>
        </div>
    </div>


<?php include('../footer.php'); ?>
