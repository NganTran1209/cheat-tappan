<?php include_once(__DIR__ . '/../common/util.php'); ?>
<?php
$id;
if (!empty($_GET['id'])) {
    $id = $_GET['id'];
} elseif (!empty($_POST['id'])) {
    $id = $_POST['id'];
}

if (empty($id)) {
    setMessage('不正アクセスです。');
    header('Location: ' . getContextRoot() . '/user/mypage.php');
    exit;
}

if (!isLogin()) {
    setMessage('ログインをしてください。');
    header('Location: ' . getContextRoot() . '/');
    exit;
}

if (!isSelling()) {
    setMessage('出品が許可されておりません。');
    header('Location: ' . getContextRoot() . '/user/mypage.php');
    exit;
}

if (!isIdentification()) {
    setMessage('本人確認が完了しておりません。');
    header('Location: ' . getContextRoot() . '/user/mypage.php');
    exit;
}

if (isset($_POST['action'])) {
    if ($_POST['action'] == 'modify') {
        $entity = $_SESSION['item'];
    } else {
        $entity = new Item();
        $entity->select($id);
        $entity->price = empty($_POST['price']) ? null : $_POST['price'];
        if ($_POST['action'] == 'regist') {
            $_SESSION['item'] = $entity;

            header('Location: ' . getContextRoot() . '/user/modify_item_confirm.php');
            exit();
        } elseif ($_POST['action'] == 'validation') {
            $entity->validateModify();
            $result = getMessage();
            echo json_encode($result);
            exit();
        }
    }
} else {
    $entity = new Item();
    $entity->select($id);
    $entity->repairName();
}
unset($_SESSION['item']);

?>
<?php include('../header.php'); ?>
<script>
    function confirm() {
        var requestData = new FormData();

        var $f = $('#sell-form');
        var valueData = $f.serializeArray();
        $.each(valueData, function (key, input) {
            requestData.append(input.name, input.value);
        });
        requestData.append('action', 'validation');

// 	var fileData = $('input[name="photo1"]')[0].files;
// 	for (var i = 0; i < fileData.length; i++) {
// 		requestData.append("photo1", fileData[i]);
// 	}
        var result = $.ajax({
            url: './modify_item.php',
            type: 'post',
            async: false,
            processData: false,
            contentType: false,
            data: requestData
        }).responseText;

        var obj = JSON.parse(result);

        if (obj.msg.length > 0) {
            window.alert(obj.msg);
            return false;
        }
        return true;
    }

</script>

<div class="container">
    <div class="row">
        <div class="col-md-3 sideContents">
            <?php include('usersidebar.php'); ?>
        </div>
        <div class="col-md-9 mainContents">
            <div class="bg-inner">
                <h1>商品情報を編集する</h1>
                <div class="fontRed">※価格のみ編集できます。</div>
                <form id="sell-form" method="post" enctype="multipart/form-data" onsubmit="return confirm();">

                    <div class="border-bottom py-4">
                        <h2 class="bg-light p-2 fontBold my-3">商品の情報</h2>
                        <h3 class="fontBold my-2">商品画像</h3>
                        <div class="row">
                            <div class="col-md-3 mb-2">
                                <img class="img-fluid" src="<?= getImagePath($entity->id, 1) ?>" alt=""/>
                            </div>
                            <?php $url = getImagePath($entity->id, 2);
                            if (strstr($url, "noimage") == false): ?>
                                <div class="col-md-3 mb-2">
                                    <img class="img-fluid" src="<?= getImagePath($entity->id, 2) ?>" alt=""/>
                                </div>
                            <?php endif; ?>
                            <?php $url = getImagePath($entity->id, 3);
                            if (strstr($url, "noimage") == false): ?>
                                <div class="col-md-3 mb-2">
                                    <img class="img-fluid" src="<?= getImagePath($entity->id, 3) ?>" alt=""/>
                                </div>
                            <?php endif; ?>
                            <?php $url = getImagePath($entity->id, 4);
                            if (strstr($url, "noimage") == false): ?>
                                <div class="col-md-3 mb-2">
                                    <img class="img-fluid" src="<?= getImagePath($entity->id, 4) ?>" alt=""/>
                                </div>
                            <?php endif; ?>
                        </div>
                    </div>

                    <div class="border-bottom py-4">
                        <h2 class="bg-light p-2 fontBold">商品の詳細</h2>
                        <h3 class="fontBold">タイトル</h3>
                        <div><?= $entity->title ?></div>
                        <h3 class="fontBold my-2">商品の説明文</h3>
                        <?php echo nl2br(htmlentities($entity->remarks)); ?>
                        <div class="font10">※最大1.5万文字まで説明文を入力できます。</div>
                        <h3 class="fontBold my-2">カテゴリー</h3>
                        <?= $entity->category_name ?>
                        <h3 class="fontBold my-2">商品の状態</h3>
                        <?= $entity->item_state_name ?>
                    </div>

                    <div class="border-bottom py-4">
                        <h2 class="bg-light p-2 fontBold my-3">商品の配送について</h2>
                        <h3 class="fontBold my-2">地域</h3>
                        <?= $entity->pref_name ?>
                        <h3 class="fontBold my-2">配送料の負担</h3>
                        <?= $entity->carriage_plan_name ?>
                        <h3 class="fontBold my-2">配送の目安</h3>
                        <?= $entity->carriage_type_name ?>
                        <h3 class="fontBold my-2">配送方法</h3>
                        <?= $entity->delivery_type_name ?>
                    </div>

                    <div class="border-bottom py-4">
                        <h2 class="bg-light p-2 fontBold my-3">価格(300円～9,999,999円)</h2>
                        <h3 class="fontBold my-2">商品価格</h3>
                        <input class="form-control middle-value" type="number" name="price"
                               value="<?= $entity->price ?>" placeholder="数字のみ（例　10,000円→10000と入力）" max="900000000">

                        <!-- ▼ 評価の条件表示 -->
                        <div class="margin-20 hidden">
                            <label class="font-normal fontRed"><input type="checkbox" name="show_evaluate"
                                                                      id="show_evaluate" disabled="disabled"
                                                                      value="1" <?= $entity->show_evaluate ? 'checked' : '' ?>>
                                総合評価の合計が-1以下のユーザーから、質問•交渉、購入をされたくない方はチェックを入れてください。</label>
                        </div>
                        <!-- ▲ 評価の条件表示 -->
                    </div>

                    <div class="row py-4">
                        <div class="col-md-12 mb-3">
                            <input class="btn btn-success btn-block btn-sm" type="submit" value="確認・更新する"/>
                        </div>
                    </div>
                    <input type="hidden" name="id" value="<?= $entity->id ?>"/>
                    <input type="hidden" name="action" value="regist"/>
                </form>
            </div>
        </div>
    </div>
</div>

<?php include('../footer.php'); ?>
