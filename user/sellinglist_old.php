<?php include_once(__DIR__ . '/../common/util.php'); ?>
<?php include_once(__DIR__ . '/../common/login_check.php'); ?>
<?php
$item = new Item();
$items = $item->selectSell(getUserId());
$negotiation = false;
if (isset($_GET['mode']) && $_GET['mode'] == 'negotiation') {
    $negotiation = true;
}
$page = 1;
$page = empty($_POST['page']) ? '1' : $_POST['page'];
if (!empty($_GET['page'])) {
    $page = $_GET['page'];
}
?>
<?php include('../header.php'); ?>
<form id="fm-param" method="post">
    <input type="hidden" id="id-page" name="page" value="<?= $page ?>"/>
</form>
    <div class="container">
        <div class="row">
            <div class="col-md-3 sideContents">
                <?php include('usersidebar.php'); ?>
            </div>
            <div class="col-md-9 mainContents">
                <div class="bg-inner">
                    <h1>出品した<?= ITEM_NAME ?>-出品中</h1>
                    <div class="small mb-2">※価格を変更する場合、編集をご利用ください。
                        尚、価格変更してから更新すると新着出品情報に表示されます。
                    </div>
                    <table class="table table-bordered table-striped">
                        <tr>
                            <th>掲載日</th>
                            <!--<th class="col-sm-2">画像</th>-->
                            <th>タイトル</th>
                            <th>価格</th>
                            <th>編集</th>
                            <!--<th class="col-sm-2 hidden-xs">連絡数</th>-->
                            <!--<th class="col-sm-1">状態</th>-->
                        </tr>
                        <?php
                        $count = 0;
                        $index = 0;
                        foreach ($items as $item): ?>
                            <?php if ($item->enabled == 9 || $item->enabled == 2 || $item->enabled == 0) continue; ?>
                            <?php if (empty($item->buyer_id)) : ?>
                                <?php
                                $count++;
                                if (ceil(++$index / MAX_PAGE_COUNT) != $page) {
                                    continue;
                                }
                                ?>
                                <tr>
                                    <td class="text-right"><?= $item->regist_date ?></td>
                                    <!--<td class="text-center"><img class="selllist-img" src="<?php echo HOME_URL; ?>/images/item/id<?= $item->id ?>/thumbnail.jpg" width="638" height="704" alt=""/></td>-->
                                    <td class=""><a
                                                href="<?php echo HOME_URL; ?>/itemdetail.php?id=<?= $item->id ?>"><?= $item->title ?></a>
                                        <!--<a href="transaction.php?item_id=<?= $item->id ?>"><?= $item->title ?></a>-->
                                    </td>
                                    <td class="text-right"><?= number_format($item->price) ?>円</td>
                                    <td class="text-center"><a href="modify_item.php?id=<?= $item->id ?>">編集</a></td>
                                    <!--<td class="hidden-xs text-center"><?= number_format($item->negotiation_count) ?>件</td>-->
                                    <!--<td class="text-center"><?= toItemStatusText($item) ?></td>-->
                                </tr>
                            <?php endif; ?>
                        <?php endforeach; ?>
                    </table>
                    <div class="text-center margin-20">
                        <?php
                        $prev_page = $page - 1;
                        $max_page = ceil($count / MAX_PAGE_COUNT);
                        $next_page = min($max_page, $page + 1);
                        $view_start_page = max(1, $page - ceil(VIEW_PAGE_SELECT_COUNT / 2));
                        $view_last_page = min($view_start_page + VIEW_PAGE_SELECT_COUNT - 1, $max_page);
                        if ($view_last_page - $view_start_page < VIEW_PAGE_SELECT_COUNT) {
                            $view_start_page = max(1, $view_last_page - VIEW_PAGE_SELECT_COUNT + 1);
                        }
                        ?>
                        <?php if ($prev_page > 0): ?>
                            <a class="sortButton" href=""
                               onclick="submitForm('fm-param', 'page', '<?= $prev_page ?>'); return false;">前へ</a>
                        <?php else: ?>
                            <a class="sortButton" href="" onclick="return false;">前へ</a>
                        <?php endif; ?>

                        <?php for ($i = $view_start_page; $i <= $view_last_page; $i++): ?>
                            <?php if ($page != $i): ?>
                                <a class="sortButton" href=""
                                   onclick="submitForm('fm-param', 'page', '<?= $i ?>'); return false;"><?= $i ?></a>
                            <?php else: ?>
                                <a class="sortButton active" href="" onclick="return false;"><?= $i ?></a>
                            <?php endif; ?>
                        <?php endfor; ?>

                        <?php if ($page < $max_page): ?>
                            <a class="sortButton" href=""
                               onclick="submitForm('fm-param', 'page', '<?= $next_page ?>'); return false;">次へ</a>
                        <?php else: ?>
                            <a class="sortButton" href="" onclick="return false;">次へ</a>
                        <?php endif; ?>

                        <p class="small"><?= number_format(($page - 1) * MAX_PAGE_COUNT + 1) ?>
                            ～<?= number_format(min($count, $page * MAX_PAGE_COUNT)) ?>件/<?= number_format($count) ?>
                            件</p>
                    </div>
                </div>

            </div>
        </div>
    </div>

<?php include('../footer.php'); ?>
