<?php include_once(__DIR__.'/../common/util.php'); ?>
<?php include_once(__DIR__.'/../common/login_check.php'); ?>
<?php include_once(__DIR__.'/../common/stripe_api.php'); ?>
<?php
if (!isset($_POST['item_id']) && !isset($_POST['stripeToken'])) {
    setMessage('不正なアクセスです。。');
    header('Location: ' . getContextRoot() . '/');
    exit;

}

$item = new Item();
if (isset($_POST['item_id'])) {
    $item->select($_POST['item_id']);
}

if (isset($_SESSION['payment_item'])) {
    $item = $_SESSION['payment_item'];
    $_SESSION['payment_item'] = null;
    unset($_SESSION['payment_item']);
}

$order = new Order();
$order->select($item->order_id);

$amount = $item->price;
$system_config = SystemConfig::select();

$discription = $order->user->hash_id.':'.$order->user->name.'さまは'.$order->owner->hash_id.':'.$order->owner->name.'さまから'.'商品「'.$item->title.'」を購入しました。';

if (isset($_POST['stripeToken'])) {
    if (StripeApi::regist_customer()) {
        if (StripeApi::charge($_POST['amount'], $discription)) {
            $order->updateOrderForCardPayment();
            $cash_flow = new CashFlow();
            $cash_flow->user_id = $order->owner->id;
            $cash_flow->amount = $amount;
            $cash_flow->order_id = $order->id;
            $cash_flow->regist();
            if (!$order->item->with_delivery) {
                $order->updateStateForCompleteWithNonDelivery();
            }
            setMessage('購入しました。');
            header("location: ". getContextRoot().'/user/negotiation.php?item_id='.$item->id.'&user_id='.$order->user->id);
            exit();
        }
    }
}

$card_info = new CardInfo();
$card_info->select(getHashKey());
if (!empty($card_info) && $card_info->enabled_customer && StripeApi::repair_costomer()) {
    if (StripeApi::charge($amount, $discription)) {
        $order->updateOrderForCardPayment();
        $cash_flow = new CashFlow();
        $cash_flow->user_id = $order->owner->id;
        $cash_flow->amount = $amount;
        $cash_flow->order_id = $order->id;
        $cash_flow->regist();
        if (!$order->item->with_delivery) {
            $order->updateStateForCompleteWithNonDelivery();
        }
        setMessage('購入しました。');
        header("location: ". getContextRoot().'/user/negotiation.php?item_id='.$item->id.'&user_id='.$order->user->id);
        exit();
    }
}

$_SESSION['payment_item'] = $item;

?>
<?php include(__DIR__.'/../user_header.php'); ?>
    <div class="container">
        <div class="row">
            <?php include('usersidebar.php'); ?>
            <div class="col-md-9 mainContents">
                <div class="bg-inner">
                    <h1 itemprop="headline" class="mb-4 bg-light p-4">クレジットカードで購入する</h1>

                    <!--                <div class="alert alert-success text-center font15 my-5" role="alert">-->
                    <!--                    <div class="alert-heading"><i class="fas fa-reply"></i> メールを送信しました</div>-->
                    <!--                </div>-->

                    <h2 class="bg-light p-2 fontBold">お支払予定の金額</h2>
                    <div class="text-center p-4">
                        <div class="fontBold">
                            ご購入金額：<span class="font25"><?= $amount ?></span>円
                        </div>
                    </div>

                    <style>
                        /*button.stripe-button-el,*/
                        /*button.stripe-button-el>span {*/
                        /*    background-color: #c50067 !important;*/
                        /*    background-image: none;*/
                        /*}*/
                    </style>
                    <form method="POST">
                        <input type="hidden" name="amount" value="<?= $amount ?>" />
                        <script src="https://checkout.stripe.com/checkout.js" class="stripe-button"
                                data-key='<?= $system_config->stripe_access_key ?>'
                                data-amount="<?= $amount ?>"
                                data-email="<?= getUserEmail() ?>"
                                data-name="ご請求金額は<?= $amount ?>円です。"
                                data-locale="auto"
                                data-allow-remember-me="false"
                                data-label="今すぐ支払う"
                                data-currency="jpy">
                        </script>
                        <script>
                            // Hide default stripe button, be careful there if you
                            // have more than 1 button of that class
                            document.getElementsByClassName("stripe-button-el")[0].style.display = 'none';
                        </script>
                        <div class="w-75 mx-auto">
                            <button type="submit" class="btn btn-block btn-success">今すぐ支払う</button>
                        </div>
                    </form>

                    <!--                <div class="card mb-5">-->
                    <!--                    <div class="card-body">-->
                    <!--                        <h3 class="card-title"><i class="fas fa-exclamation-triangle"></i> メールボックスのゴミ箱・迷惑メールボックスもご確認下さい</h3>-->
                    <!--                        <p class="fontBold">※5分以内に確認メールを1通送ります。</p>-->
                    <!--                        <p class="fontRed mb-0">※メールが届かない場合は迷惑メールフォルダ、ゴミ箱などをご確認下さい。</p>-->
                    <!--                    </div>-->
                    <!--                </div>-->
                </div>
            </div>
        </div>
    </div>

<?php include('../user_footer.php'); ?>