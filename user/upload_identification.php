<?php 
$title_page = '本人確認書類の提出';
$description_page = '欲しい商品を画像で確認・届いた商品を画像で照合！気持ち良い取引が叶うオンラインマーケット。';
 ?>
<?php include_once(__DIR__ . '/../common/util.php'); ?>
<?php include_once(__DIR__ . '/../common/login_check.php'); ?>
<?php
if (isIdentification()) {
    //	header('Location: '.getContextRoot().'/user/mypage.php');
}

if (isset($_POST['action']) && $_POST['action'] == 'upload') {
    if ($_FILES['photo']['error'] == 0) {
        Identification::regist_tmp();
        header('Location: ' . getContextRoot() . '/user/upload_identification_confirm.php');
        exit();
    } else {
        setMessage('本人確認書類画像は必須です。');
    }

}
$system_config = SystemConfig::select();
$title_page = '本人確認書類の提出';
?>
<?php include('../user_header.php'); ?>
<div class="com-header-top">
        <div class="com-header-top__img wow animate__animated animate__fadeInUp">
            <img src="<?php echo HOME_URL; ?>/common/assets/img/mypage/top-bg.png" alt="">
        </div>
        <div class="com-header-top__path wow animate__animated animate__fadeInUp">
            <p><span><a href="<?php echo HOME_URL; ?>/" class="clr-yel">トップページ</a></span><span> > </span><span><a href="<?php echo HOME_URL; ?>/user/mypage.php" class="clr-yel">マイページ</a></span><span> > </span><span>本人確認書類の提出</span></p>
        </div>

    </div>

    <div class="com-container bg-yellow">
        <?php include('usersidebar.php'); ?>
    <!-- <div class="com-header-top">
        <div class="com-header-top__img">
            <img src="<?php echo HOME_URL; ?>/common/assets/img/mypage/top-bg.png" alt="">
        </div>
        <div class="com-header-top__path">
            <p><span><a href="<?php echo HOME_URL; ?>/index.php" class="clr-yel">トップページ</a></span><span> > </span><span><a href="<?php echo HOME_URL; ?>/user/mypage.php" class="clr-yel">マイページ</a></span><span> > </span><span>本人確認書類の提出</span></p>
        </div>
        <div class="com-header-top__txt">
            <p class="clr-white">マイページ</p>
        </div>
    </div> -->
    
    <div class="com-content ">
        <form class="c-form01" method="post" enctype="multipart/form-data">
            <div class="content-title">
                <h3><span>本人確認書類の提出</span></h3>
            </div>
            <div class="verify-sm-txt">
                <h4>デジタルカメラや携帯電話カメラで撮影した画像をアップロードしてください。</h4>
                <p>
                    ※運転免許証、パスポート、公の機関が発行した資格証明書で写真付きの画像を添付して下さい。<br>
                    ※運転免許証など、裏面に現住所の記載がある場合は裏面も撮影して画像を添付して下さい。<br>
                    ※顔写真がついていないものは認められません。<br><br>
                </p>
                <h4>
                    スマホから本人確認書類の提出をされる方へ
                </h4>
                <p>
                    ※特定のブラウザかブラウザアプリを使用している場合、アップロードできないことがあります。
                </p>
            </div>
            <div class="verify-submit-form">
                <div class="verify-submit-form__title">
                    <p>本人確認書類をアップロードする</p>
                </div>
               
                <div class="custom-file custom-file-add h-auto">
                    <input type="file" name="photo" id="photo">
                    <label class="custom-file-label" for="photo">1枚目画像選択...</label>
                    <div class="text-center mt-3"><img id="photo-upload-preview" src="" /></div>
                </div>
                <div class="custom-file custom-file-add h-auto">
                    <input type="file" name="photo2" id="photo2">
                    <label class="custom-file-label" for="photo2">2枚目画像選択...</label>
                    <div class="text-center mt-3"><img id="photo2-upload-preview" src="" /></div>
                </div>
          
                <div class="verify-submit-form__btn">
                    <input type="hidden" name="action" value="upload"/>
                    <button type="submit" id="" name="" class="product-confirm-add-2">確認する</button>
                </div>
            </div>
        </form>
    </div>
</div>
<?php include('../user_footer.php'); ?>

<script>
    $('#photo').on('change', function (e) {
	    var reader = new FileReader();
	    reader.onload = function (e) {
	        $("#photo-upload-preview").attr('src', e.target.result).width(300).height(300);;
	    }
	    reader.readAsDataURL(e.target.files[0]);
	});
    $('#photo2').on('change', function (e) {
	    var reader = new FileReader();
	    reader.onload = function (e) {
	        $("#photo2-upload-preview").attr('src', e.target.result).width(300).height(300);;
	    }
	    reader.readAsDataURL(e.target.files[0]);
	});
</script>