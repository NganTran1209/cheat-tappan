<?php include_once(__DIR__.'/../common/util.php'); ?>
<?php include_once(__DIR__.'/../common/login_check.php'); ?>
<?php include_once(__DIR__.'/../common/stripe_api.php'); ?>
<?php
if (!isset($_POST['item_id']) && !isset($_POST['stripeToken'])) {
    setMessage('不正なアクセスです。');
    header('Location: ' . getContextRoot() . '/');
    exit;

}

$item = new Item();
if (isset($_POST['item_id'])) {
    $item->select($_POST['item_id']);
}

if (isset($_POST['stripeToken']) && isset($_SESSION['payment_item'])) {
    $item = $_SESSION['payment_item'];
    $_SESSION['payment_item'] = null;
    unset($_SESSION['payment_item']);
}

$order = new Order();
$order->select($item->order_id);

$amount = $item->price;
$system_config = SystemConfig::select();

$discription = $order->user->hash_id.':'.$order->user->name.'さまは'.$order->owner->hash_id.':'.$order->owner->name.'さまから'.'商品「'.$item->title.'」を購入しました。';

if (isset($_POST['stripeToken'])) {
    if (StripeApi::regist_customer()) {
        if (StripeApi::charge($_POST['amount'], $discription)) {
            $order->updateOrderForCardPayment();
            $cash_flow = new CashFlow();
            $cash_flow->user_id = $order->owner->id;
            $cash_flow->amount = $amount;
            $cash_flow->order_id = $order->id;
            $cash_flow->regist();
            if (!$order->item->with_delivery) {
                $order->updateStateForCompleteWithNonDelivery();
            }
            setMessage('購入しました。');
            header("location: ". getContextRoot().'/user/negotiation.php?item_id='.$item->id.'&user_id='.$order->user->id);
            exit();
        }
    }
}

$card_info = new CardInfo();
$card_info->select(getHashKey());
if (!empty($card_info) && $card_info->enabled_customer && StripeApi::repair_costomer()) {
    if (StripeApi::charge($amount, $discription)) {
        $order->updateOrderForCardPayment();
        $cash_flow = new CashFlow();
        $cash_flow->user_id = $order->owner->id;
        $cash_flow->amount = $amount;
        $cash_flow->order_id = $order->id;
        $cash_flow->regist();
        if (!$order->item->with_delivery) {
            $order->updateStateForCompleteWithNonDelivery();
        }
        setMessage('購入しました。');
        header("location: ". getContextRoot().'/user/negotiation.php?item_id='.$item->id.'&user_id='.$order->user->id);
        exit();
    }
}

$_SESSION['payment_item'] = $item;

$title_page = 'クレジットカードで購入する';
?>
<?php include(__DIR__.'/../user_header.php'); ?>
<div class="com-header-top">
    <div class="com-header-top__img">
        <img src="<?php echo HOME_URL; ?>/common/assets/img/common/other-com-header.png" alt="">
    </div>
    <div class="com-header-top__path bg-other-01">
        <p><span>トップページ</span><span> > </span><span>レディース新着アイテム</span><span>></span><span>商品タイトル</span><span>></span><span>クレジットカードで購入する</span></p>
    </div>
</div>
<div class="com-container bg-yellow">
    <?php include('usersidebar.php'); ?>
    <div class="com-content">
        <div class="buy-credit-card__lead">
            <h3><span>お支払い予定金額</span></h3>
        </div>
        <div class="buy-credit-card__price">
            <p><span>¥<?= $amount ?> 円</span>（税込/<?= $item->carriage_plan_name ?>)</p>
        </div>
        <div class="buy-credit-card__btn">
            <form method="post">
                <input type="hidden" name="amount" value="<?= $amount ?>" />
                <script src="https://checkout.stripe.com/checkout.js" class="stripe-button"
                        data-key='<?= $system_config->stripe_access_key ?>'
                        data-amount="<?= $amount ?>"
                        data-email="<?= getUserEmail() ?>"
                        data-name="ご請求金額は<?= $amount ?>円です。"
                        data-locale="auto"
                        data-allow-remember-me="false"
                        data-label="今すぐ支払う"
                        data-currency="jpy">
                </script>
                <script>
                    // Hide default stripe button, be careful there if you
                    // have more than 1 button of that class
                    document.getElementsByClassName("stripe-button-el")[0].style.display = 'none';
                </script>
                <button type="submit" id="" name="">今すぐ支払う</button>
            </form>
        </div>
    </div>
</div>
<?php include('../user_footer.php'); ?>