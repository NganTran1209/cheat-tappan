<?php 
$title_page = 'お知らせ';
$description_page = '欲しい商品を画像で確認・届いた商品を画像で照合！気持ち良い取引が叶うオンラインマーケット。';
 ?>
<?php include_once(__DIR__ . '/../common/util.php'); ?>
<?php include_once(__DIR__ . '/../common/login_check.php'); ?>
<?php
$info = new Information();
$items = $info->selectAll();
$page = 1;
$page = empty($_POST['page']) ? '1' : $_POST['page'];
if (!empty($_GET['page'])) {
  $page = $_GET['page'];
}
$mode = isset($_GET['mode']) ? $_GET['mode'] : '';
$negotiation = false;
if (isset($_POST['mode']) && $_POST['mode'] == 'negotiation') {
    $mode = $_POST['mode'];
}
if (isset($mode) && $mode == 'negotiation') {
    $negotiation = true;
}
$title_page = 'お知らせ';
?>
<?php include(__DIR__ . '/../user_header.php'); ?>
<form id="fm-param" method="post">
  <input type="hidden" id="mode" name="mode" value="<?= $mode ?>"/>
  <input type="hidden" id="id-page" name="page" value="<?= $page ?>" />
</form>
<div class="com-header-top">
  <div class="com-header-top__img wow animate__animated animate__fadeInUp">
    <img src="<?php echo HOME_URL; ?>/common/assets/img/common/other-com-header.png" alt="">
  </div>
  <div class="com-header-top__path bg-other-01 wow animate__animated animate__fadeInUp">
    <p><span><a href="<?php echo HOME_URL; ?>/" class="clr-yel">トップページ</a></span><span> > </span><span>お知らせ</span></p>
  </div>
<!--  <div class="com-header-top__txt">
    <p>お知らせ</p>
  </div>-->
</div>
<div class="customer-container">
<div class="category-title wow animate__animated animate__fadeInUp">
                <h3><span>お知らせ</span></h3>
            </div>
  <div class="customer-contact-form">
    <?php
    $count = 0;
    $index = 0;
    foreach ($items as $item) :
    ?>
      <?php if ($item->enabled != 1) {
        continue;
      }
      $count++;
      if (ceil(++$index / MAX_PAGE_COUNT) != $page) {
        continue;
      }
      ?>
      <div class="content-group wow animate__animated animate__fadeInUp">
        <a href="<?php echo HOME_URL; ?>/user/notice_show.php?id=<?= $item->id ?>" class="content-group-item">
          <div class="content-group-item__date"><i class="bi bi-chevron-right"></i><?= date('Y-m-d', strtotime($item->update_date)) ?></div>
          <div class="content-group-item__txt" style="line-break: anywhere;"><?= $item->title ?></div>
          <?php if (date("Y-m-d") == date('Y-m-d', strtotime($item->update_date))) { ?>
            <div class="content-group-item__new"><span>NEW</span></div>
          <?php } ?>
        </a>
      </div>

    <?php endforeach; ?>
    <?php
    $prev_page = $page - 1;
    $max_page = ceil($count / MAX_PAGE_COUNT);
    $next_page = min($max_page, $page + 1);
    $view_start_page = max(1, $page - ceil(VIEW_PAGE_SELECT_COUNT / 2));
    $view_last_page = min($view_start_page + VIEW_PAGE_SELECT_COUNT - 1, $max_page);
    if ($view_last_page - $view_start_page < VIEW_PAGE_SELECT_COUNT) {
      $view_start_page = max(1, $view_last_page - VIEW_PAGE_SELECT_COUNT + 1);
    }
    ?>
    <?php if ($max_page > 1) : ?>
      <div class="content-group-pagination wow animate__animated animate__fadeInUp">
      <?php else : ?>
        <div class="content-group-pagination content-one-page wow animate__animated animate__fadeInUp">
        <?php endif; ?>

        <?php if ($max_page > 1) { ?>
          <?php if ($prev_page > 0) : ?>
            <a id="pagination__preve" class="pagination__preve" href="" onclick="submitForm('fm-param', 'page', '<?= $prev_page ?>'); return false;"><i class="bi bi-chevron-left"></i></a>
          <?php else : ?>
            <a id="pagination__preve" class="pagination__preve" href="" onclick="return false;"><i class="bi bi-chevron-left"></i></a>
          <?php endif; ?>
        <?php } ?>
        <?php for ($i = $view_start_page; $i <= $view_last_page; $i++) : ?>
          <?php if ($page != $i) : ?>
            <a class="pagination__num" href="" onclick="submitForm('fm-param', 'page', '<?= $i ?>'); return false;"><span><?= $i ?></span></a>
          <?php else : ?>
            <a class="pagination__num pag_active" href="" onclick="return false;"><span><?= $i ?></span></a>
          <?php endif; ?>
        <?php endfor; ?>

        <?php if ($max_page > 1) { ?>
          <?php if ($page < $max_page) : ?>
            <a id="pagination__next" class="pagination__preve" href="" onclick="submitForm('fm-param', 'page', '<?= $next_page ?>'); return false;">
              <i class="bi bi-chevron-right"></i>
            </a>
          <?php else : ?>
            <a id="pagination__next" class="pagination__preve" href="" onclick="return false;"><i class="bi bi-chevron-right"></i></a>
          <?php endif; ?>
        <?php } ?>
        </div>
      </div>
  </div>
</div>

<?php include('../footer.php'); ?>