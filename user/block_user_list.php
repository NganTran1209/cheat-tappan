<?php 
$title_page = 'ブラックリスト登録';
$description_page = '欲しい商品を画像で確認・届いた商品を画像で照合！気持ち良い取引が叶うオンラインマーケット。';
 ?>

<?php include_once(__DIR__ . '/../common/util.php'); ?>
<?php include_once(__DIR__.'/../common/login_check.php'); ?>
<?php
if (isset($_POST) && isset($_POST['action'])) {
    if ($_POST['action'] == 'deleteBlock') {
        $block = new BlockAccess();
        $block->id = $_POST['id'];
        $block->select();
        $block->remove();
        $user = new User();
        $user->select($block->block_user_id);
        setMessage('ユーザー「' . $user->hash_id . '」のブラックリストを解除しました。');
    } elseif ($_POST['action'] == 'blockAccess') {
        $user = new User();
        $user->hash_id = $_POST['hash_id'];
        if (!$user->hasHashId()) {
            setMessage('該当するユーザーIDが見つかりません。');
        } elseif ($user->hash_id == getHashKey()) {
            setMessage('自分のIDをブラックリストに登録することはできません。');
        } else {
            $block = new BlockAccess();
            if ($block->isBlockOther($user->id)) {
                setMessage("ユーザー[$user->hash_id]はブラックリストに登録済です。");
            } else {
                $block->user_id = getUserId();
                $block->block_user_id = $user->id;
                $block->regist();
                setMessage("ユーザー[$user->hash_id]をブラックリストに登録しました。");
            }
        }
    }
}

$block = new BlockAccess();
$block->user_id = getUserId();
$items = $block->selectFromUser();
$title_page = 'ブラックリスト登録';
?>
<?php include('../user_header.php'); ?>
<div class="com-container bg-yellow">
            <?php include('usersidebar.php'); ?>
        <div class="com-content">
            <div class="content-title">
                <h3><span>ブラックリスト登録</span></h3>
            </div>
            
                <form class="c-form01 formBlockID" method="post">
                    <div class="row">
                        <div class="col-xl-8 col-lg-6 ">
                            <input class="form-control mb-5 block-p form-control-block" type="text" name="hash_id" placeholder="ユーザーID"/>
                        </div>
                        <div class="col-xl-4 col-lg-6 ">
                            <input class="btn btn-block btn-outline-danger block-p" type="submit" value="ブラックリスト登録"/>
                            <input type="hidden" name="action" value="blockAccess"/>
                        </div>
                    </div>
                </form>
                <br class="sp-only">
                <br class="sp-only">
                <br class="sp-only">
                <div class="fontRed font10 padding-10 block-p mb-5">
                    ※ブラックリストに登録すると、対象ユーザーと取引が一切できなくなります。<br>
                    ※ご自身がブラックリストに登録された場合、相手のユーザーへ嫌がらせ行為等をおこなった場合、ID利用の停止と、警察へ通報されることがあります。
                    <!--                          ※ブラックリストに登録するとログイン時、対象ユーザーの商品は見られなくなります。<br>-->
                    <!--                          ※他のユーザーから、あなたがブラックリストに登録されると、あなたは登録されたユーザーの商品はログイン時、見られなくなります。<br>-->
                    <!--                          ※ログアウトすると商品は見られるようになりますが、取引は一切できません。<br>-->
                    <!--                          <span class="fontBold">ご注意ください！</span>　ブラックリストに登録されたユーザーへ嫌がらせ行為等をおこなった場合、ID利用の停止と、警察へ通報されることがあります。-->
                </div>
              <div class="block_list">
                <h2>ブラックリストに登録されているユーザーID一覧</h2>
              </div>
                <div class="table-responsive">
                <table class="table table-bordered">
                    <thead>
                      <tr>
                        <th scope="col">登録日</th>
                        <th scope="col">ユーザーID</th>
                        <th scope="col">解除</th>
                      </tr>
                    </thead>
                    <tbody>
                    <?php foreach ($items as $item):
                        $user = new User();
                        $user->select($item->block_user_id);
                        ?>
                        <tr>
                            <td class="text-right"><?= $item->regist_date ?></td>
                            <td class="text-left"><?= $user->getViewId() ?></td>
                            <td class="text-center">
                                <form method="post"
                                      onsubmit="return window.confirm('ユーザー「<?= $user->getViewId() ?>」のブラックリストを解除します。');">
                                    <input type="hidden" name="action" value="deleteBlock"/>
                                    <input type="hidden" name="id" value="<?= $item->id ?>"/>
                                    <input class="btn btn-block btn-info" type="submit" value="ブラックリストを解除"/>
                                </form>
                            </td>
                        </tr>
                    <?php endforeach; ?>
                    </tbody>
                </table>
            </div>
        </div>
</div>

<?php include('../footer.php'); ?>
