<?php 
$title_page = '本登録完了';
$description_page = '欲しい商品を画像で確認・届いた商品を画像で照合！気持ち良い取引が叶うオンラインマーケット。';
 ?>
<?php include_once(__DIR__.'/../common/util.php'); ?>
<?php

if (!isset($_GET['id']) || !isset($_GET['check'])) {
	setMessage('不正なアクセスです。');
	header('Location: '.getContextRoot().'/');
	exit;
}

$user = new User();
$user->select($_GET['check']);
if (empty($user->hash_id) || $user->hash_id != $_GET['id']) {
	setMessage('不正なアクセスです。');
	header('Location: '.getContextRoot().'/');
	exit;
}

if ($user->enabled) {
	setMessage('既に本登録済みです。');
	header('Location: '.getContextRoot().'/');
	exit;
}

$user->updateEnabled();
setMessage('本登録を行いました。');
?>

<?php include(__DIR__.'/../header.php'); ?>
<div class="p-uercomplete">
<div class="container-fluid margin-contents">
    <div class="container text-center wow animate__animated animate__fadeInLeft">
        <strong>メールアドレス、パスワードでログインをしてください。</strong>
    </div>
    <!-- container -->
</div>
</div>

<!-- container-fluid -->

<?php include('../footer.php'); ?>




