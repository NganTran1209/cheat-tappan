<?php include_once(__DIR__.'/../../common/util.php'); ?>
<?php

preg_match('|' . dirname($_SERVER['SCRIPT_NAME']) . '/([\w%/]*)|', $_SERVER['REQUEST_URI'], $matches);
$paths = explode('/', $matches[1]);
$id = isset($paths[0]) ? htmlspecialchars($paths[0]) : null;
if (empty($id)) {
    setMessage('不正なアクセスです。');
    header('Location: ' . getContextRoot() . '/');
    exit;
}

$user = new User();
if (!$user->selectFromHashId($id)) {
    setMessage('不正なアクセスです。');
    header('Location: ' . getContextRoot() . '/');
    exit;
}

if (isset($_POST['action'])) {
    if ($_POST['action'] == 'blockAccess') {
        $block = new BlockAccess();
        $block->user_id = getUserId();
        $block->block_user_id = $user->id;
        $block->regist();
        setMessage("ユーザー[$user->hash_id]をブラックリストに登録しました。");
    }
}

$sns = new Sns();
$sns->select($user->id);

$item = new Item();
$items = $item->selectSell($user->id);

$order = new Order();
$evaluates = $order->selectFromUserAll($user->id);

$evaluate = new Evaluate();
$evaluate->selectSummary($user->id);

$icon_url = $user->get_icon_url();
$header_image_url = $user->get_header_image_url();

$soldout_count = 0;
foreach ($items as $item2) {
    if ($item2->enabled == 9) {
        continue;
    }

    if (!empty($item2->buyer_id)) {
        $soldout_count++;
    }
}

?>

<?php include('../../header.php'); ?>
<?php if (!empty($header_image_url)): ?>
<style>
    .navbar {
        margin-bottom: 0;
        z-index: 1;
    }
</style>
<div class="photoWrap">
    <img class="img-fluid" src="<?= $header_image_url ?>">
</div>
<?php endif; ?>

<div class="container">
    <div class="profileContents">
        <div class="row mb-5">
            <div class="col-md-2 profileIconImageWrap">
                <div class="profileIconImage">
                	<?php if (!empty($icon_url)): ?>
                        <img class="" src="<?= $icon_url ?>" height="96" width="96">
                    <?php endif; ?>
                </div>
            </div>
            <div class="col-md-6">
                <div class="profileUserName mb-2">
                    <h1><?= $user->getViewNicknameOrId() ?></h1>
                    <?php //echo $user->name; ?>
                </div>
                <div class="mb-2">
                    <span class="profileUserSlash">ID：<?= $user->hash_id ?></span>
                    <?php if ($user->identification): ?>
                    <span class="profileUserSlash">本人確認済<i class="far fa-check-circle text-success pl-1" aria-hidden="true"></i></span>
                    <?php else:?>
                    <span class="profileUserSlash">本人確認なし<i class="fas fa-times-circle text-danger pl-1"></i></span>
                    <?php endif;?>
                    <?php if (!empty($user->type_name)): ?>
                        <span class="profileUserSlash"><?= $user->type_name ?></span>
                    <?php endif; ?>
                    <?php if (!empty($user->pref_name)): ?>
                        <?= $user->pref_name ?>
                    <?php endif; ?>
                </div>
                <div class="mb-2">
                    <span class="profileUserSlash">販売数 <span class="fontBold"><?= $soldout_count ?></span></span>
                    評価 <span class="fontBold"><?= $evaluate->point ?></span>
                </div>
                <div class="mb-2">
                    <?php if (!empty($sns->twitter_url)):?>
                        <a class="mr-2 text-dark" href="<?= $sns->twitter_url ?>"><i class="fab fa-twitter fa-2x"></a></i>
                    <?php endif;?>
                    <?php if (!empty($sns->facebook_url)):?>
                        <a class="mr-2 text-dark" href="<?= $sns->facebook_url ?>"><i class="fab fa-facebook fa-2x"></i></a></i>
                    <?php endif;?>
                    <?php if (!empty($sns->youtube_url)):?>
                        <a class="mr-2 text-dark" href="<?= $sns->youtube_url ?>"><i class="fab fa-youtube fa-2x"></i></a></i>
                    <?php endif;?>
                    <?php if (!empty($sns->instagram_url)):?>
                        <a class="mr-2 text-dark" href="<?= $sns->instagram_url ?>"><i class="fab fa-instagram fa-2x"></i></a></i>
                    <?php endif;?>
                </div>
                <div class="mb-2">
                    <?php if (!empty($sns->other1_url)):?>
                        <div>
                            <i class="fas fa-link mr-1"></i><a href="<?= $sns->other1_url ?>"><?= $sns->other1_url ?></a>
                        </div>
                    <?php endif;?>
                    <?php if (!empty($sns->other2_url)):?>
                        <div>
                            <i class="fas fa-link mr-1"></i><a href="<?= $sns->other2_url ?>"><?= $sns->other2_url ?></a>
                        </div>
                    <?php endif;?>
                    <?php if (!empty($sns->other3_url)):?>
                        <div>
                            <i class="fas fa-link mr-1"></i><a href="<?= $sns->other3_url ?>"><?= $sns->other3_url ?></a>
                        </div>
                    <?php endif;?>
                </div>

                <div class="my-4">
                    <?php echo nl2br($user->self_pr); ?>
                </div>
            </div>
            <div class="col-md-4">
                <div class="profileUserName text-right">
                    <!-- <i class="fas fa-ellipsis-h"></i>-->
                    <section class="blockAccessWrap">
                        <?php if (isLogin() && $user->id != getUserId()): ?>
                            <div class="text-right">
                            <?php if (!BlockAccess::isBlock($user->id)) :?>
                                <form id="block-user" method="post" onsubmit="return window.confirm('出品者をブロックします。'); ">
                                    <input type="hidden" name="action" value="blockAccess" />
                                    <a href="#" onclick="submitForm('block-user');">
                                        <i class="fa fa-times" aria-hidden="true"></i> この出品者をブロックする</a>
                                </form>
                            <?php endif; ?>
                            </div>
                        <?php endif; ?>
                    </section>
                </div>
            </div>
        </div>

        <div class="mb-4">
            <h2><i class="fas fa-shopping-cart pr-1"></i><?= ITEM_NAME ?>販売一覧</h2>

            <div class="row">
                <?php
                $count = 0;
                $index = 0;
                foreach ($items as $item): ?>
                    <?php if ($item->enabled == 9 || $item->enabled == 2 || $item->enabled == 0) continue; ?>
                    <div class="col-6 col-md-3 mb-3">
                        <div class="border h-100 mb-3">
                            <a href="<?php echo HOME_URL; ?>/itemdetail.php?id=<?= $item->id ?>">
                                <?php if ($item->order_id != null): ?>
                                    <div class="soldOutBadgeMini">
                                        <div>SOLD</div>
                                    </div>
                                <?php endif; ?>
<!--                                <div class="topListImg">-->
<!--                                    <img class="img-fluid" src="--><?php //echo $item->getThumbnailUrl(); ?><!--" alt=""/>-->
<!--                                </div>-->
                                <div class="priceOnImg">
                                    <div class="topListImg">
                                        <img class="img-fluid" src="<?= $item->getThumbnailUrl() ?>" alt=""/>
                                    </div>
                                    <p><?php echo number_format($item->price); ?><span class="smallEX">円</span></p>
                                </div>
                                <div class="small px-1">
                                    <?php echo mb_strimwidth($item->title, 0, 34, '...');?>
                                </div>
<!--                                <div class="small px-1">--><?php //echo $item->price ?><!--円</div>-->
                            </a>
                        </div>
                    </div>
                <?php endforeach; ?>
            </div>

            <div class="my-3 text-right">
                <a href="<?php echo HOME_URL; ?>/itemlist.php?owner_id=<?= $item->owner_id ?>">出品者の他の<?= ITEM_NAME ?>を見る</a>
            </div>
        </div>

        <div class="mb-4">
            <h2><i class="fas fa-smile pr-1"></i>感想/評価</h2>

            <?php foreach ($evaluates as $order): ?>
                <?php if ($order->owner_id != $user->id):?>
                    <?php if (!empty($order->buyer_evaluate)): ?>
                    <div class="row mb-4">
                        <div class="col-md-3">
                            <div class="balloonUser">
                                <a href="<?= $order->owner->hash_id ?>"><?= $order->owner->getViewNicknameOrId() ?></a>
                                <div class="commenter">
                                    評価：
                                    <?php if (Evaluate::toEvaluateImageTag($order->buyer_evaluate) == Evaluate::toEvaluateImageTag(1)): ?>
                                        <i class="fas fa-smile mr-1"></i>
                                    <?php elseif (Evaluate::toEvaluateImageTag($order->buyer_evaluate) == Evaluate::toEvaluateImageTag(2)): ?>
                                        <i class="fas fa-meh mr-1"></i>
                                    <?php elseif (Evaluate::toEvaluateImageTag($order->buyer_evaluate) == Evaluate::toEvaluateImageTag(3)): ?>
                                        <i class="fas fa-frown mr-1"></i>
                                    <?php endif; ?>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-9">
                            <div class="bg-comment balloon balloonB">
                                <div class="balloonComment">
                                    <div class="">
                                        <a href="<?php echo HOME_URL; ?>/itemdetail.php?id=<?= $order->item_id ?>"><?= $order->item_title ?></a>
                                    </div>
                                    <?= empty($order->buyer_comment) ? 'コメントはありません' : nl2br($order->buyer_comment) ?>
                                </div>
                                <div class="balloonTime">
                                    <i class="far fa-clock mr-2"></i><?= $order->regist_date ?>
                                </div>
                            </div>
                        </div>
                    </div>
                    <?php endif; ?>
                <?php else: ?>
                    <?php if (!empty($order->seller_evaluate)): ?>
                    <div class="row mb-4">

                        <div class="col-md-3">
                            <div class="balloonUser">
                                <a href="<?= $order->user->hash_id ?>"><?= $order->user->getViewNicknameOrId() ?></a>
                                <div class="commenter">
                                    評価：
                                    <?php if (Evaluate::toEvaluateImageTag($order->seller_evaluate) == Evaluate::toEvaluateImageTag(1)): ?>
                                        <i class="fas fa-smile mr-1"></i>
                                    <?php elseif (Evaluate::toEvaluateImageTag($order->seller_evaluate) == Evaluate::toEvaluateImageTag(2)): ?>
                                        <i class="fas fa-meh mr-1"></i>
                                    <?php elseif (Evaluate::toEvaluateImageTag($order->seller_evaluate) == Evaluate::toEvaluateImageTag(3)): ?>
                                        <i class="fas fa-frown mr-1"></i>
                                    <?php endif; ?>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-9">
                            <div class="bg-comment balloon balloonA">
                                <div class="balloonComment">
                                    <div class="">
                                        <a href="<?php echo HOME_URL; ?>/itemdetail.php?id=<?= $order->item_id ?>"><?= $order->item_title ?></a>
                                    </div>
                                    <?= empty($order->seller_comment) ? 'コメントはありません' : nl2br($order->seller_comment) ?>
                                </div>
                                <div class="balloonTime">
                                    <i class="far fa-clock mr-2"></i><?= $order->regist_date ?>
                                </div>
                            </div>
                        </div>
                    </div>
                    <?php endif; ?>
                <?php endif; ?>
            <?php endforeach; ?>


            <?php /*?><?= Evaluate::toEvaluateImageTag($order->buyer_evaluate) ?><?php */ ?>　
            <?php /*?><a href="../evaluate.php?user_id=<?= $order->owner_id ?>"><?= $order->owner->hash_id ?></a><?php */ ?>　
            <?php /*?><?= Evaluate::toEvaluateImageTag($order->buyer_evaluate) ?><?php */ ?>　
            <?php /*?><a href="../evaluate.php?user_id=<?= $order->user_id ?>"><?= $order->user->hash_id ?></a><?php */ ?>　

            <?php /*
            <?php foreach ($evaluates as $order): ?>
                <div class="card mb-3">
                    <?php if (!$order->owner_id == $user->id):?>
                        <div class="card-header">
                            評価：
                            <?php if (Evaluate::toEvaluateImageTag($order->buyer_evaluate) == Evaluate::toEvaluateImageTag(1)): ?>
                                <i class="fas fa-smile mr-1"></i>
                            <?php elseif (Evaluate::toEvaluateImageTag($order->buyer_evaluate) == Evaluate::toEvaluateImageTag(2)): ?>
                                <i class="fas fa-meh mr-1"></i>
                            <?php elseif (Evaluate::toEvaluateImageTag($order->buyer_evaluate) == Evaluate::toEvaluateImageTag(3)): ?>
                                <i class="fas fa-frown mr-1"></i>
                            <?php endif; ?>
                            <a href="<?php echo HOME_URL; ?>/itemdetail.php?id=<?= $order->item_id ?>"><?= $order->item_title ?></a>

                        </div>
                        <div class="card-body">
                            <div class="margin-bottom-10">
                                <?= empty($order->buyer_comment) ? 'コメントはありません' : nl2br($order->buyer_comment) ?>
                            </div>
                            <div class="text-right">
                                出品者：<a href="<?= $order->owner->hash_id ?>"><?= $order->owner->getViewNicknameOrId() ?></a>　販売日時：<?= $order->regist_date ?>
                            </div>
                        </div>
                    <?php else: ?>
                        <div class="card-header">
                            評価：
                            <?php if (Evaluate::toEvaluateImageTag($order->seller_evaluate) == Evaluate::toEvaluateImageTag(1)): ?>
                                <i class="fas fa-smile mr-1"></i>
                            <?php elseif (Evaluate::toEvaluateImageTag($order->seller_evaluate) == Evaluate::toEvaluateImageTag(2)): ?>
                                <i class="fas fa-meh mr-1"></i>
                            <?php elseif (Evaluate::toEvaluateImageTag($order->seller_evaluate) == Evaluate::toEvaluateImageTag(3)): ?>
                                <i class="fas fa-frown mr-1"></i>
                            <?php endif; ?>
                            <a href="<?php echo HOME_URL; ?>/itemdetail.php?id=<?= $order->item_id ?>"><?= $order->item_title ?></a>
                        </div>
                        <div class="card-body">
                            <div class="margin-bottom-10">
                                <?= empty($order->seller_comment) ? 'コメントはありません' : nl2br($order->seller_comment) ?>
                            </div>
                            <div class="text-right">
                                購入者：<a href="<?= $order->user->hash_id ?>"><?= $order->user->getViewNicknameOrId() ?></a>　販売日時：<?= $order->regist_date ?>
                            </div>
                        </div>
                    <?php endif; ?>
                </div>
            <?php endforeach; ?>
            */ ?>

            <div class="py-3 text-right">
                <a href="<?php echo HOME_URL; ?>/evaluate.php?user_id=">このユーザーの評価一覧</a>
            </div>
        </div>

    </div>
</div>
<?php include('../../footer.php'); ?>

