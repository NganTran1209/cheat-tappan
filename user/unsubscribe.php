<?php 
$title_page = '退会方法について';
$description_page = '欲しい商品を画像で確認・届いた商品を画像で照合！気持ち良い取引が叶うオンラインマーケット。';
 ?>
<?php include_once(__DIR__ . '/../common/util.php'); ?>
<?php include_once(__DIR__ . '/../common/login_check.php'); ?>
<?php include(__DIR__ . '/../header.php'); ?>
 <!-- <div class="container">
        <div class="row">-->
            <!-- <div class="col-md-3 sideContents"> -->
            <div class="com-container bg-yellow">
            <?php include('usersidebar.php'); ?>
        <div class="com-content">
        <div class="content-title ">
                <h3><span>退会方法について</span></h3>
            </div>
            <div class="mainContents ">
                <div class="bg-inner leave">
                
                    <p>退会したい場合は、お問い合わせフォームからユーザー側よりご連絡いただく必要がございます。<br>
                        お問い合わせフォームより退会のご連絡<span class="fontRed">(退会の旨を明記して下さい)</span></p>

                        <div class="class-about-leave">
                    <h3>退会後に関してまして</h3>
                    <p>販売手数料が発生している場合、翌月または翌々月の１日から３日の間に請求メールが送信されますので、ご精算をお願い致します。<br>
                        <span class="fontRed">ご注意ください！<br>
                            精算が済むまで退会の処理はできません。予めご了承ください。</span></p>
                        </div>
                        <div class="class-about-leave">
                    <h3>退会後の利用期限</h3>
                    <p>
                        退会後、最短即時～7営業日以内に全ての機能が利用不可になりログイン出来なくなります。<br>
                        出品中や購入者とのやり取り中の場合は十分ご注意下さい。
                    </p>
                    </div>
                    <div>
                        <a class="btn btn-block btn-outline-danger btn-sm btn-info"
                           href="<?php echo HOME_URL; ?>/mail/">退会のご連絡</a>
                    </div>
                </div>
            </div>
     </div>
    </div>

<?php include('../footer.php'); ?>