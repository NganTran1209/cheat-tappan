<?php include_once(__DIR__ . '/../common/util.php'); ?>
<?php
$entity = new User();
if (isset($_POST['action']) && $_POST['action'] == 'regist') {
    $entity->email = $_POST['email'];
    $entity->name = $_POST['name'];
    $entity->name_kana = $_POST['name_kana'];
//     $entity->zip = $_POST['zip'];
//     $entity->addr = $_POST['addr'];
    $entity->password = $_POST['password'];
    $entity->password2 = $_POST['password2'];
//     $entity->company = $_POST['company'];
//     $entity->tel = $_POST['tel'];
//     $entity->pref = $_POST['pref'];
//     $entity->type = $_POST['type'];
    $entity->ipaddr = getRemoteAddress();

    if (empty($_POST['kiyaku'])) {
        setMessage('利用規約に同意されておりません。');
    } elseif (!$entity->validate()) {

    } elseif ($entity->has()) {
        setMessage('既にメールアドレスが登録済みです。');
    } elseif ($_POST['password'] != $_POST['password2']) {
        setMessage('パスワードとパスワード（確認）が一致しません。');
    } else {
        $entity->regist();
        //setMessage('仮登録通知メールを送付いたしました。');
        header('Location: ' . getContextRoot() . '/user/userregist.php');
        exit;
    }
} elseif (isset($_POST['action']) && $_POST['action'] == 'send-mail') {
    $entity->email = $_POST['email'];
    $entity->password = $_POST['password'];
    if (!$entity->selectFromEmail()) {
        setMessage("仮登録しているメールアドレス・パスワードを指定してください。");
    } elseif ($entity->enabled != 0) {
        setMessage("指定されているメールアドレスのアカウントは本登録済みです。");
    } else {
        $entity->sendRegistMail();
        setMessage("仮登録メールを再送しました。");
    }
}
?>
<?php include(__DIR__ . '/../other_header.php'); ?>
<script>
    function sendRegistMail() {
        var frm = $('#form-send-mail');
        $('#send-mail-email').val($("#email").val());
        $('#send-mail-password').val($("#password").val());
        frm.submit();
    }
</script>
<form id="form-send-mail" method="post" onsubmit="return confirm('仮登録メールを再送する');">
    <input id="send-mail-email" type="hidden" name="email"/>
    <input id="send-mail-password" type="hidden" name="password"/>
    <input type="hidden" name="action" value="send-mail"/>
</form>
<div class="container">
    <div class="mainContents">
        <div class="bg-inner">
            <form id="form" method="post" onsubmit="return confirm('登録する');">
                <input type="hidden" name="action" value="regist"/>
                <div class="text-center">
                    <h1>会員情報入力</h1>
                </div>

                <div class="form-group">
                    <label class="fontBold" for="name">お名前</label>
                    <input class="form-control middle-value"
                           type="text" name="name" id="name"
                           value="<?= $entity->name ?>" placeholder="例）武田太郎">
                </div>
                <div class="form-group">
                    <label class="fontBold" for="name_kana">お名前(フリガナ)</label>
                    <input class="form-control middle-value"
                           type="text" name="name_kana" id="name_kana" value="<?= $entity->name_kana ?>"
                           placeholder="例）タケダタロウ">
                </div>
                <div class="form-group">
                    <?php /* TODO メールアドレス以外でも登録出来てしまう問題 */ ?>
                    <label class="fontBold" for="email">メールアドレス※1</label>
                    <input class="form-control middle-value" type="text" name="email" id="email"
                           value="<?= $entity->email ?>" placeholder="例）xxx@xxx.co.jp">
                </div>
                <div class="form-group">
                    <label class="fontBold" for="password">パスワード</label>
                    <input class="form-control middle-value" type="password" id="password"
                           name="password" value="<?= $entity->password ?>" placeholder="8文字以上の半角英数字">
                </div>
                <div class="form-group">
                    <label class="fontBold" for="password2">パスワード(確認)</label>
                    <input class="form-control middle-value" type="password"
                           name="password2" id="password2" value="<?= $entity->password2 ?>">
                    <span class="small">
                    ※上記のパスワードと同じパスワードを入力してください。
                </span>
                </div>

                <div class="form-group">
                    <div class="custom-control custom-switch">
                        <input type="checkbox" class="custom-control-input" id="customSwitch1" name="kiyaku" value="1">
                        <label class="custom-control-label" for="customSwitch1"><a
                                    href="<?php echo HOME_URL; ?>/pages/kiyaku.php">利用規約</a>に同意します。</label>
                    </div>
                </div>

                <div class="w-75 my-5 mx-auto">
                    <input class="btn btn-info btn-block" type="submit" value="登録する"/>
                    <div class="small my-3">
                        ※1スマホ・携帯電話アドレスの場合は「ドメイン指定受信」「アドレス指定受信」などを行っていると
                        メールを受信できません。<br>
                        ※受信設定を忘れて登録し、メールが受信できない場合、
                        受信設定後「<a href="<?php echo HOME_URL; ?>/sendregistmail.php">こちら</a>」から仮登録メールを再送信できます。
                    </div>
                </div>
            </form>
        </div>
    </div>
</div>

<?php include('../footer.php'); ?>
