<?php include_once(__DIR__ . '/../common/util.php'); ?>
<?php include_once(__DIR__ . '/../common/login_check.php'); ?>
<?php
$item = new Item();
$items = $item->selectBuy();
usort($items, function ($a, $b) {
    if (strtotime($a->last_date) == strtotime($b->last_date)) {
        return 0;
    } elseif (strtotime($a->last_date) > strtotime($b->last_date)) {
        return -1;
    }
    return 1;
});

$page = 1;
$page = empty($_POST['page']) ? '1' : $_POST['page'];
if (!empty($_GET['page'])) {
    $page = $_GET['page'];
}
?>
<?php include('../header.php'); ?>
<form id="fm-param" method="post">
    <input type="hidden" id="id-page" name="page" value="<?= $page ?>"/>
</form>
<div class="container">
    <div class="row">
        <!-- <div class="col-md-3 sideContents"> -->
            <?php include('usersidebar.php'); ?>
        <!-- </div> -->
        <div class="col-md-9 mainContents">
            <div class="bg-inner">
                <h1>購入した<?= ITEM_NAME ?>-取引中<span class="smallEX">※表示期間は購入日から90日間です。</span></h1>
                <table class="table table-bordered table-striped">
                    <tr class="text-center">

                        <th>最終連絡日</th>
                        <!--<th class="col-sm-2">画像</th>-->
                        <th>タイトル</th>
                        <th>価格</th>
                        <!--<th class="col-sm-1">交渉数</th>-->
                        <!--                    <th>販売者ID</th>-->
                        <th>評価</th>
                    </tr>
                    <?php
                    $count = 0;
                    $index = 0;
                    foreach ($items as $item): ?>
                        <?php
                        if ($item->enabled == 9) {
                            continue;
                        }
                        if (diffDays(date("Y/m/d"), $item->order_date) > MAX_VIEW_DAYS) {
                            continue;
                        }
                        $count++;
                        if (ceil(++$index / MAX_PAGE_COUNT) != $page) {
                            continue;
                        }
                        $owner = new User();
                        $owner->select($item->owner_id);
                        ?>
                        <tr>
                            <td class="text-right"><?= $item->last_date ?><?php /*?><?= date('Y/n/j', strtotime($item->last_date)) ?><?php */ ?></td>
                            <!--                        <td class="col-sm-2 text-center"><img class="selllist-img" src="--><?php //echo getContextRoot()?><!--/images/item/id--><?php //echo $item->id ?><!--/thumbnail.jpg" width="92" height="61" alt=""/></td>-->
                            <td class="">
                                <a href="negotiation.php?item_id=<?= $item->id ?>&user_id=<?= $item->buyer_id ?>" <?= $item->createBlockScript($item->owner_id) ?>><?= $item->title ?></a>
                                <!--                            <a href="../itemdetail.php?id=--><?php //echo $item->id ?><!--">--><?php //echo $item->title ?><!--</a>-->
                            </td>
                            <td class="text-right"><?= number_format($item->price) ?>円
                                <?php if ($item->seller_evaluate == 0): ?>
                            <td class="text-center"><a href="evaluation.php?id=<?= $item->order_id ?>">未評価</a></td>
                            <?php else: ?>
                                <td class="text-center"><a href="evaluation.php?id=<?= $item->order_id ?>">評価済</a></td>
                            <?php endif; ?>

                            </td>
                            <!--                        <td class="col-sm-1 text-center">--><?php //echo number_format($item->negotiation_count) ?><!--件</td>-->
                            <!--                        <td class="">--><?php //echo $owner->name ?><!----><?php //echo $owner->getViewId() ?><!--</td>-->
                        </tr>
                    <?php endforeach; ?>
                </table>

                <div class="text-center margin-20">
                    <?php
                    $prev_page = $page - 1;
                    $max_page = ceil($count / MAX_PAGE_COUNT);
                    $next_page = min($max_page, $page + 1);
                    $view_start_page = max(1, $page - ceil(VIEW_PAGE_SELECT_COUNT / 2));
                    $view_last_page = min($view_start_page + VIEW_PAGE_SELECT_COUNT - 1, $max_page);
                    if ($view_last_page - $view_start_page < VIEW_PAGE_SELECT_COUNT) {
                        $view_start_page = max(1, $view_last_page - VIEW_PAGE_SELECT_COUNT + 1);
                    }
                    ?>
                    <?php if ($prev_page > 0): ?>
                        <a class="sortButton" href=""
                           onclick="submitForm('fm-param', 'page', '<?= $prev_page ?>'); return false;">前へ</a>
                    <?php else: ?>
                        <a class="sortButton" href="" onclick="return false;">前へ</a>
                    <?php endif; ?>

                    <?php for ($i = $view_start_page; $i <= $view_last_page; $i++): ?>
                        <?php if ($page != $i): ?>
                            <a class="sortButton" href=""
                               onclick="submitForm('fm-param', 'page', '<?= $i ?>'); return false;"><?= $i ?></a>
                        <?php else: ?>
                            <a class="sortButton active" href="" onclick="return false;"><?= $i ?></a>
                        <?php endif; ?>
                    <?php endfor; ?>

                    <?php if ($page < $max_page): ?>
                        <a class="sortButton" href=""
                           onclick="submitForm('fm-param', 'page', '<?= $next_page ?>'); return false;">次へ</a>
                    <?php else: ?>
                        <a class="sortButton" href="" onclick="return false;">次へ</a>
                    <?php endif; ?>

                    <p class="small"><?= number_format(($page - 1) * MAX_PAGE_COUNT + 1) ?>
                        ～<?= number_format(min($count, $page * MAX_PAGE_COUNT)) ?>件/<?= number_format($count) ?>
                        件</p>
                </div>
            </div>
        </div>
    </div>
</div>

<?php include('../footer.php'); ?>
