<?php 
$title_page = '購入希望情報';
$description_page = '欲しい商品を画像で確認・届いた商品を画像で照合！気持ち良い取引が叶うオンラインマーケット。';
 ?>
<?php include_once(__DIR__ . '/../common/util.php'); ?>
<?php include_once(__DIR__ . '/../common/login_check.php'); ?>
<?php
$req = new Request();
$user_evaluate = new Evaluate();
if (!empty($_GET['id'])) {
    $req->select($_GET['id']);
    $user = new User();
    $user->select($req->user_id);

    $user_evaluate->selectSummary($req->user_id);
}
if (!empty($_POST['action'])) {
    if ($_POST['action'] == 'cancel') {
        $req = new Request();
        $req->select($_POST['reqId']);
        if ($req->user_id == getUserId()) {
            $req->regection();
            setMessage("購入希望[$req->title]を取り消しました。");
        } else {
            setMessage('不正アクセスです。');
        }
        header('Location: '.getContextRoot().'/');
        exit;
    }
}

$title_page = '購入希望情報';
?>
<?php include(__DIR__ . '/../user_header.php'); ?>

    <div class="com-header-top">
        <div class="com-header-top__img wow animate__animated animate__fadeInUp">
            <img src="<?php echo HOME_URL; ?>/common/assets/img/common/other-com-header.png" alt="">
        </div>
        <div class="com-header-top__path bg-other-01 wow animate__animated animate__fadeInUp">
            <p><span><a href="<?php echo HOME_URL; ?>/" class="clr-yel">トップページ</a></span><span> > </span>
            <span class="clr-yel"><a href="<?php echo HOME_URL; ?>/user/purase_request_info_list.php" class="clr-yel">購入希望情報</a></span><span> > </span><span><?= $req->title ?>の購入希望</span></p>
        </div>
<!--        <div class="com-header-top__txt wow animate__animated animate__fadeInUp">
            <p>購入希望情報</p>
        </div>-->
    </div>
    <div class="customer-container wow animate__animated animate__fadeInUp">
    
        <div class="purase-request-info" style="margin-bottom: 10px;">
        <div class="content-title">
                <h3><span>購入希望詳細</span></h3>
            </div>
            <p>
                <?= $req->title ?>の購入希望<br>
                <?= $req->update_date ?> 購入希望者ID：<?php echo getHashKey(); ?>　評価：<?= $user_evaluate->point ?>
            </p>
            <p>
                <?php echo nl2br(htmlentities($req->text)); ?>
            </p>
            <p>
                予算 : <span><?= number_format($req->price) ?></span> 円
            </p>
        </div>
        <?php if (BlockAccess::isBlock($req->user_id)) : ?>
            <p>
                この購入希望者とは取引できません。
            </p>
        <?php elseif ($req->user_id != getUserId()) : ?>
            <form method="post" action="offerlist.php">
                <input type="hidden" name="reqId" value="<?= $req->id ?>"/>
                <input class="btn btn-block btn-info btn-lg mx-auto btn-req-add" type="submit"
                    value="出品・商品を紹介する"/>
            </form>
            <p class="text-center c-small">
                購入希望者へ自分の商品を紹介することができます。
            </p>
        <?php else: ?>
            <form method="post" action="">
                <input type="hidden" name="action" value="cancel"/>
                <input type="hidden" name="reqId" value="<?= $req->id ?>"/>
                <input class="btn btn-block btn-info btn-lg w-25 mx-auto" type="submit"
                    value="購入希望を取り消す"/>
            </form>
        <?php endif; ?>
    </div>

<?php include('../footer.php'); ?>