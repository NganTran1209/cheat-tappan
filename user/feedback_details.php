<?php include_once(__DIR__ . '/../common/util.php'); ?>

<?php
if (!isLogin()) {
    setMessage('不正なアクセスです。');
    header('Location: ' . getContextRoot() . '/');
    exit;
}

$check = false;
$uploadCheck1 = false;
$uploadCheck2 = false;

$item = new Item();
$item->select($_GET['item_id']);
if ($item->enabled == 9) {
    setMessage('出品が取り消されました。');
    header('Location: ' . getContextRoot() . '/');
    exit;
} elseif ($item->owner_id == getUserId()) {
    if (BlockAccess::isBlock($_GET['user_id'])) {
        setMessage('このユーザーと取引はできません。', true);
        header('Location: ' . getContextRoot() . '/');
        exit;
    }
} else {
    if ($item->isBlock()) {
        setMessage('このユーザーと取引はできません。', true);
        header('Location: ' . getContextRoot() . '/');
        exit;
    }
}

$negotiation = new Negotiation();
$order = new Order();
$order->select($item->order_id);

$system_config = SystemConfig::select();
$feedback = new Feedback();
$sizeLimit = 1024;
if (isset($_POST['action'])) {
    if ($_POST['action'] == 'feedback') {
        if ((!empty($_FILES['photo']) && $_FILES['photo']['error'] == 0) || (!empty($_FILES['photo2']) && $_FILES['photo2']['error'] == 0 && isImage($_FILES['photo2']['tmp_name']))) {
            if($_POST['index'] == 1){
                $feedback->item_id = $_POST['item_id'];
                $feedback->user_id = $_POST['user_id'];
                $feedback->order_id = $_POST['order_id'];
                $feedback->index = $_POST['index'];
                $_FILES['photo'] = changeFileType($_FILES['photo']);
                if (getimagesize($_FILES['photo']['tmp_name'])[1] > $sizeLimit){
                    compress($_FILES['photo']['tmp_name'], 100);
                }
                if (!empty($_FILES['photo']) && $_FILES['photo']['error'] == 0) {
                    $feedback->photo = rotateImage(file_get_contents($_FILES['photo']['tmp_name']));
                } else {
                    $feedback->photo = null;
                }

                $img = curl_file_create($_FILES['photo']['tmp_name'], $_FILES['photo']['type'], basename($_FILES['photo']['name']));
                // $check = $feedback->compareRequestGaziru($_POST['groupCode'], $img);
                $check = $feedback->compareRequestGaziruWithObject($_POST['objectCode'], $img);
                $max_score = json_decode($check,true)["scores"][0]["score"];
                foreach (json_decode($check,true)["scores"] as &$value) {
                    if($value["score"] > $max_score){
                        $max_score = $feedback->score ;
                    }
                }
                $feedback->score =  $max_score;
                $feedback->regist();

                $feedback->state = 1;
                $feedback->updateSate();
            }
            if($_POST['index'] == 2){
                $feedback->item_id = $_POST['item_id'];
                $feedback->user_id = $_POST['user_id'];
                $feedback->order_id = $_POST['order_id'];
                $feedback->index = $_POST['index'];
                $_FILES['photo2'] = changeFileType($_FILES['photo2']);
                if (getimagesize($_FILES['photo2']['tmp_name'])[1] > $sizeLimit){
                    compress($_FILES['photo2']['tmp_name'], 100);
                }
                if (!empty($_FILES['photo2']) && $_FILES['photo2']['error'] == 0) {
                    $feedback->photo = rotateImage(file_get_contents($_FILES['photo2']['tmp_name']));
                } else {
                    $feedback->photo = null;
                }
                $img = curl_file_create($_FILES['photo2']['tmp_name'], $_FILES['photo2']['type'], basename($_FILES['photo2']['name']));
                // $check = $feedback->compareRequestGaziru($_POST['groupCode'], $img);
                $check = $feedback->compareRequestGaziruWithObject($_POST['objectCode'], $img);
                $max_score = json_decode($check,true)["scores"][0]["score"];
                foreach (json_decode($check,true)["scores"] as &$value) {
                    if($value["score"] > $max_score){
                        $max_score = $feedback->score ;
                    }
                }
                $feedback->score =  $max_score;
                $feedback->regist();

                $feedback->state = 1;
                $feedback->updateSate();
            }
            
            $order->state = $_POST['state'];
            $order->updateProcedure();            
            $check = true;

            setMessage('真贋比較画像とアップロードした画像がそれぞれ表示されるようにしてください。');
            echo(201);
            exit;
        } else {
            setMessage('コメントまたは画像( jpg,jpeg,png,gif )。');
            echo(500);
            exit;
            // setMessage($_FILES['photo']['error']);
            // var_dump($_FILES['photo']);
        }
    } 
}
function compress($source, $quality) {
    $info = getimagesize($source);
    $width = $new_width = $info[0];
    $height = $new_height = $info[1];
    if ($info['mime'] == 'image/jpeg') 
        $image = imagecreatefromjpeg($source);
    elseif ($info['mime'] == 'image/gif') 
        $image = imagecreatefromgif($source);
    elseif ($info['mime'] == 'image/png') 
        $image = imagecreatefrompng($source);
    else 
        $image = imagecreatefromjpeg($source);

    $new_width = 1024;
    $new_height = 1024;
    $new_image = imagecreatetruecolor($new_width, $new_height);
    imagecopyresampled($new_image, $image, 0, 0, 0, 0, $new_width, $new_height, $width, $height);  

    $exif = exif_read_data($source);
    if(!empty($exif['Orientation'])) {
        switch($exif['Orientation']) {
            case 8:
                $image = imagerotate($image,90,0);
                break;
            case 3:
                $image = imagerotate($image,180,0);
                break;
            case 6:
                $image = imagerotate($image,-90,0);
                break;
        } 
    }
    imagejpeg($new_image, $source, $quality);
}

function changeFileType($file){
    $info = getimagesize($file['tmp_name']);
    if ($info['mime'] == 'image/jpeg') 
        $image = imagecreatefromjpeg($file['tmp_name']);
    elseif ($info['mime'] == 'image/gif') 
        $image = imagecreatefromgif($file['tmp_name']);
    elseif ($info['mime'] == 'image/png') 
        $image = imagecreatefrompng($file['tmp_name']);
    else 
        $image = imagecreatefromjpeg($file['tmp_name']);
    imagejpeg($image,$file['tmp_name'], 100);
    $file['name'] = pathinfo($file['name'])['filename'] . '.jpg';
    $file['type'] = 'image/jpeg';
    return $file;
}

$item->select($_GET['item_id']);
$order->select($item->order_id);

$owner = new User();
$owner->select($item->owner_id);
if ($owner->id != getUserId() && $_GET['user_id'] != getUserId()) {
    setMessage('不正なアクセスです。');
    header('Location: ' . getContextRoot() . '/');
    exit;
}

//$noimagepng = "http://192.168.55.10/ccsystem/images/noimage.png";
$title_page = 'レディース新着アイテム';

$feedback1 = new Feedback();
$feedback1->selectForIndex($item->id,$_GET['user_id'],1);
$feedback2 = new Feedback();
$feedback2->selectForIndex($item->id,$_GET['user_id'],2);
$user = new User();
$user->select($_GET['user_id']);


if (!empty($_GET['new_title'])) {
    $category_title = $_GET['new_title'];
}else{
    $category_title = selectCodeName('selectCategoryById', $item->category);
}

$title_page = $category_title;
$img_flow_active=getContextRoot().'/common/assets/img/common/flow-active.png';
$img_step1 = getContextRoot().'/common/assets/img/common/flow-next.png';
$img_step2 = getContextRoot().'/common/assets/img/common/flow-next-2.png';
?>

<?php include('../user_header.php'); ?>
<script>

    $(document).ready(function(){
        $('[data-toggle="tooltip"]').tooltip();   
    });
    
    function openForm(id) {
        const canvas = document.getElementById(id + '-canvas');
        const ctx = canvas.getContext('2d');
        if(!ctx.getImageData(0, 0, 1024, 1024).data.some(channel => channel !== 0)) {
            const img = new Image()
            if (id == 'gaziru-photo1') {
                img.src = '<?php echo $feedback1->getImageUrl() ?>'
            } else if (id == 'gaziru-photo2') {
                img.src = '<?php echo $feedback2->getImageUrl() ?>'
            }
            img.onload = () => {
                canvas.getContext('2d').drawImage(img, 0, 0, 1024, 1024);
            }
        }
    }
</script>

<div class="com-header-top">
    <div class="com-header-top__img">
        <img src="<?php echo HOME_URL; ?>/common/assets/img/common/other-com-header.png" alt="">
    </div>
    <div class="com-header-top__path bg-other-01">
        <p><span><a href="<?php echo HOME_URL; ?>/" class="clr-yel">トップページ</a></span><span> > </span><span><?= $category_title ?>新着アイテム</span></p>
    </div>
<!--    <div class="com-header-top__txt">
        <p>レディース新着アイテム</p>
    </div>-->
</div>
<div class="customer-container" id="container">
    <div class="category-title  wow animate__animated animate__fadeInUp">
                    <h3><span><?= $category_title ?>真贋比較画像</span></h3>
                </div>
        <div class="customer-contact-form">
            
            <div class="row mt-5">
                <div class="col-md-6 mb-3">
                    <div class="bg-inner">
                        <div class="content-title">
                            <h3><span><?= $item->title ?></span></h3>
                        </div>
                    </div>
                    <?php 
                        if($item->groupObject != null){
                            $objectCode = json_decode($item->groupObject, true);
                        }
                    ?>
                    <div class="row">
                        <div class="col-6 text-center">
                            <?php if(@getimagesize($item->getImageAuthenPath(1)) != null): ?>
                                <div class="imgGaziruPreview">
                                    <a href="<?= $item->getImageAuthenUrl(1) ?>" target="_blank">
                                        <img class="img img-responsive full-width" src="<?= $item->getImageAuthenUrl(1) ?>"/>
                                    </a>
                                </div>
                                <p class="text-center mt-4">全体画像</p>
                                <?php if(@getimagesize($item->getImageAuthenPath(2)) == false && @getimagesize($item->getImageAuthenPath(3)) != false): ?>
                                    <a href="<?= $item->getImageAuthenUrl(3) ?>" class="link-img-mark" target="_blank" data-toggle="tooltip" data-placement="bottom" title="特徴画像として登録した場所をマーカーで確認することができます">
                                        商品マーカー画像を見る<i class="fa fa-question-circle-o" style="font-size: 18px;padding-left: 5px;"></i>
                                    </a>
                                <?php else: ?>
                                        <a href="<?= $item->getImageAuthenUrl(3) ?>" class="link-img-mark" target="_blank" data-toggle="tooltip" data-placement="bottom" title=""></a>
                                <?php endif; ?>
                                <?php if(@getimagesize(($feedback1->getImagePath())) == false && $order->state != 6 && $order->user_id == getUserId()): ?>
                                    <div class="m-auto text-center">
                                        <div class="d-flex justify-content-between flex-lg-row flex-column">
                                            <button onclick="return uploadPhoto('gaziru-photo1')" data-toggle="modal"  type="button" class="btn-upFb col-lg-6 mr-lg-2 my-2">商品画像アップロード</button>
                                            <button class="btn-upFb col-lg-6 ml-lg-2 my-2" onclick="return takePhoto('gaziru-photo1')" data-toggle="modal" data-target="#gaziru-photo1-modal" type="button">写真を撮る</button>
                                        </div>
                                        
                                    </div>
                                <?php endif; ?>
                            <?php endif; ?>
                        </div>
                        
                        <?php if(@getimagesize($item->getImageAuthenPath(2)) != false): ?>            
                            <div class="col-6 text-center">
                                <div class="imgGaziruPreview">
                                    <a href="<?= $item->getImageAuthenUrl(2) ?>" target="_blank">
                                        <img class="img img-responsive full-width" src="<?= $item->getImageAuthenUrl(2) ?>"/>
                                    </a>
                                </div>
                                <p class="text-center mt-4">認証用画像</p>
                                <?php if(@getimagesize($item->getImageAuthenPath(3)) != false): ?>
                                    <a href="<?= $item->getImageAuthenUrl(3) ?>" class="link-img-mark" target="_blank" data-toggle="tooltip" data-placement="bottom" title="特徴画像として登録した場所をマーカーで確認することができます">
                                        商品マーカー画像を見る<i class="fa fa-question-circle-o" style="font-size: 18px;padding-left: 5px;"></i></a>
                                <?php else: ?>
                                    <a href="<?= $item->getImageAuthenUrl(3) ?>" class="link-img-mark" target="_blank" data-toggle="tooltip" data-placement="bottom" title=""></a>
                                <?php endif; ?>
                                <?php if(@getimagesize(($feedback2->getImagePath())) == false && $order->state != 6 && $order->user_id == getUserId()): ?>
                                    <div class="m-auto text-center">
                                        <div class="d-flex justify-content-between flex-lg-row flex-column">
                                            <button class="btn-upFb col-lg-6 mr-lg-2 my-2"  onclick="return uploadPhoto('gaziru-photo2')" data-toggle="modal"  type="button">商品画像アップロード</button>
                                            <button class="btn-upFb col-lg-6 ml-lg-2 my-2" onclick="return takePhoto('gaziru-photo2')" data-toggle="modal" data-target="#gaziru-photo2-modal" type="button">写真を撮る</button>
                                        </div> 
                                    </div>
                                <?php endif; ?>
                            </div>
                        </div>
                        <?php endif; ?>
                </div>
                <div class="col-md-6 mb-3">
                    <?php if (($order->state == 5 || $order->state == 6 ) && ($order->user_id == getUserId() || $order->owner_id == getUserId() || isAdminUser()) ): ?>
                    <div class="w-100">
                        <div class="bg-inner">
                            <div class="content-title">
                                <h3><span>アップロード画像</span></h3>
                            </div>
                        </div>
                        <div class="show-img-refer row mt-5">
                            <div class="col-6 text-center one-component" >
                                <?php if(@getimagesize(($feedback1->getImagePath())) != false):?>
                                    <div class="imgGaziruPreview">
                                        <a href="<?= $feedback1->getImageUrl() ?>" target="_blank">
                                            <img class="img img-responsive full-width" src="<?= $feedback1->getImageUrl() ?>"/>
                                        </a>
                                    </div>
                                    <p class="text-center mt-4">全体画像</p>
                                    <p class="text-center mt-4"><?= $feedback1->regist_date ?></p>
                                    <?php if($feedback1->state == 1): ?>
                                        <p class="text-center mt-4">真贋判定 
                                            <?php if($feedback1->score <= 1 && $feedback1->score >= 0.5) :?>
                                                <span class="typeScore">正規品</span>
                                            <?php elseif ($feedback1->score <0.5 && $feedback1->score >0.3): ?>
                                                <span class="typeScore">真贋不明</span>
                                            <?php else: ?>
                                                <span class="typeScore">模造品</span>
                                            <?php endif;?>
                                        </p>
                                        <p class="text-center mt-4">画像マッチ度 <?= round(($feedback1->score)*100,1) ?>%</p>
                                    <?php endif;?>

                                    <?php if ($order->state < 6 && $order->user_id == getUserId()): ?>
                                        <div class="m-auto text-center ">
                                            <div class="d-flex justify-content-between flex-lg-row flex-column">
                                                <button onclick="return uploadPhoto('gaziru-photo1')" data-toggle="modal"  type="button" class="btn-upFb col-lg-6 mr-lg-2 my-2">認証に利用する画像を選択する</button>
                                                <button class="btn-upFb col-lg-6 ml-lg-2 my-2" onclick="return takePhoto('gaziru-photo1')" data-toggle="modal" data-target="#gaziru-photo1-modal" type="button">認証に利用する写真を撮る</button>
                                            </div>
                                            
                                        </div>
                                    <?php endif; ?>
                                <?php endif; ?>
                            </div>
                            <div class="col-6 text-center one-component" >
                            <?php if(@getimagesize(($feedback2->getImagePath())) != false):?>
                                    <div class="imgGaziruPreview">
                                        <a href="<?= $feedback2->getImageUrl() ?>" target="_blank">
                                            <img class="img img-responsive full-width" src="<?= $feedback2->getImageUrl() ?>"/>
                                        </a>
                                    </div>
                                    <p class="text-center mt-4">認証用画像</p>
                                    <p class="text-center mt-4"><?= $feedback2->regist_date ?></p>
                                    <?php if($feedback2->state == 1) : ?>
                                        <p class="text-center mt-4">真贋判定 
                                            <?php if($feedback2->score <= 1 && $feedback2->score >= 0.5) :?>
                                                <span class="typeScore">正規品</span>
                                            <?php elseif ($feedback2->score <0.5 && $feedback2->score >0.3): ?>
                                                <span class="typeScore">真贋不明</span>
                                            <?php else: ?>
                                                <span class="typeScore">模造品</span>
                                            <?php endif;?>
                                        </p>
                                        <p class="text-center mt-4">画像マッチ度 <?= round(($feedback2->score)*100,1) ?>%</p>
                                    <?php endif;?>
                                <?php if ($order->state < 6 && $order->user_id == getUserId()): ?>
                                    
                                    <div class="m-auto text-center row">
                                        <div class="d-flex justify-content-between flex-lg-row flex-column">
                                            <button onclick="return uploadPhoto('gaziru-photo2')" data-toggle="modal"  type="button" class="btn-upFb col-lg-6 mr-lg-2 my-2">認証に利用する画像を選択する</button>
                                            <button class="btn-upFb col-lg-6 ml-lg-2 my-2" onclick="return takePhoto('gaziru-photo2')" data-toggle="modal" data-target="#gaziru-photo2-modal" type="button">認証に利用する写真を撮る</button>
                                        </div>
                                    </div>
                                <?php endif; ?>
                                </div>
                            <?php endif; ?>
                        </div>
                    </div>
                <?php endif; ?>
                </div>
            </div>
            
            <div class="text-center mt-5">
                <p>記に登録されている画像と、同じ箇所を撮影してください。<br />
                アップロード後、真贋判定ボタンを押して下さい。<br />
                同じ商品と判断された場合「正規品」と判定され、決済処理が確定いたします。<br />
                「模造品」と判定された場合は、決済処理が落ちません。<br />
                （再度判定処理を行うことは可能です）<br />
                何度も「模造品」判断される場合は、お問い合わせよりご連絡くださいませ。<br />
                </p>
            </div>
        </div>
    </div>
    <div id="gaziru-photo1-modal" class="modal fade show <?= ($uploadCheck1==true) ? 'md-show' : '' ?>" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-modal="true" style="z-index:999999" >
        <div class="modal-dialog">
            <div id="gaziru-photo1-modal-content" class="modal-content p-3">
                <div class="modal-header">
                    <h2 class="modal-title text-center font-weight-bold w-100" id="exampleModalToggleLabel">全体画像</h2>
                    <button type="button" class="btn-close" data-dismiss="modal" aria-label="Close" onclick="closePhoto('gaziru-photo1')">X</button>
                </div>
                <div class="col-8 mx-auto">
                    <div class="row mt-5 gaziruImgs">
                        <div class="col">
                            <p class="text-center font-weight-bold">全体画像</p>
                            <div class="imgGaziruPreview">                    
                                <img class="img img-responsive full-width" src="<?= $item->getImageAuthenUrl(1) ?>"/>
                            </div>
                        </div>
                        <div class="col">
                            <p class="text-center font-weight-bold">商品マーカー</p>
                            <div class="imgGaziruPreview">                    
                                <img class="img img-responsive full-width" src="<?= $item->getImageAuthenUrl(3) ?>"/>
                            </div>
                        </div>
                    </div>
                </div>

                <video id="gaziru-photo1-source" muted autoplay playsinline style="display: none"></video>
                <div class="imgGaziruPreviewCanvas">
                    <canvas id="gaziru-photo1-canvas" width=1024 height=1024 class="gaziruCanvas img img-responsive full-width"></canvas>
                </div>
                
                <div class="gaziruOptions mt-5">
                    <form action="" enctype="multipart/form-data" method="post" id="fr-FeedBack1" class="fr-feedback" >
                        <div class="" style="display:none">
                            <input type="hidden" name="item_id" value="<?= $item->id ?>" />
                            <input type="hidden" name="groupCode" value="<?= $item->groupCode ?>" />
                            <input type="hidden" name="objectCode" value="<?= $objectCode["1"] ?>" />
                            <input type="hidden" name="user_id" value="<?= $_GET['user_id'] ?>" />
                            <input type="hidden" name="order_id" value="<?= $item->order_id ?>"/>
                            <input type="hidden" name="action" value="feedback" />
                            <input type="hidden" name="state" value="5"/>
                            <input type="hidden" name="index" value="1"/>
                            <input type="hidden" name="token" value="<?= createToken(); ?>" />
                        </div>
                        <input type="range" class="zoom-range" id="zoom-range-1" hidden>
                        <div class="row mt-5 mb-3" id="gaziru-options">
                            <div class="col" id="gaziru-photo1-btn-confirm" style="display: none">
                                <input type="button" class="btn btn-secondary btn-block" value="写真を撮る" onclick="confirmPhoto('gaziru-photo1')">
                            </div> 
                            <div class="col" id="gaziru-photo1-btn-upload" >
                                <input type="button" class="btn btn-secondary btn-block" id="gaziru-photo1-upload-btn" value="アップロード" onclick="uploadPhoto('gaziru-photo1')">
                                <input type="file" class="uploadFile img" id="gaziru-photo1" name="photo" value="Upload Photo" style="width: 0px;height: 0px;overflow: hidden;">
                            </div>

                            <div class="col" id="gaziru-photo1-btn-capture" >
                                <input type="button" class="btn btn-secondary btn-block" value="写真を撮る" onclick="takePhoto('gaziru-photo1')">
                            </div>

                            <div class="col" id="gaziru-photo1-btn-submit" >
                                <input type="button" class="btn btn-secondary btn-block" value="判定" onclick="submitPhoto('gaziru-photo1')">
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>

    <div id="gaziru-photo2-modal" class="modal fade show <?= ($uploadCheck2==true) ? 'md-show' : '' ?>" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-modal="true" style="z-index:999999" >
        <div class="modal-dialog">
            <div id="gaziru-photo2-modal-content" class="modal-content p-3">
                <div class="modal-header">
                    <h2 class="modal-title text-center font-weight-bold w-100" id="exampleModalToggleLabel">認証用画像</h2>
                    <button type="button" class="btn-close" data-dismiss="modal" aria-label="Close" onclick="closePhoto('gaziru-photo2')">X</button>
                </div>
                <div class="col-8 mx-auto">
                    <div class="row mt-5 gaziruImgs">
                        <div class="col">
                            <p class="text-center font-weight-bold">認証用画像</p>
                            <div class="imgGaziruPreview">                    
                                <img class="img img-responsive full-width" src="<?= $item->getImageAuthenUrl(2) ?>"/>
                            </div>
                        </div>
                        <div class="col">
                            <p class="text-center font-weight-bold">商品マーカー</p>
                            <div class="imgGaziruPreview">                    
                                <img class="img img-responsive full-width" src="<?= $item->getImageAuthenUrl(3) ?>"/>
                            </div>
                        </div>
                    </div>
                </div>

                <video id="gaziru-photo2-source" muted autoplay playsinline style="display: none"></video>
                <div class="imgGaziruPreviewCanvas">
                    <canvas id="gaziru-photo2-canvas" width=1024 height=1024 class="gaziruCanvas img img-responsive full-width"></canvas>
                </div>
                
                <div class="gaziruOptions mt-5">
                    <form action="" enctype="multipart/form-data" method="post" id="fr-FeedBack2" class="fr-feedback" >
                        <div class="" style="display:none">
                            <input type="hidden" name="item_id" value="<?= $item->id ?>" />
                            <input type="hidden" name="groupCode" value="<?= $item->groupCode ?>" />
                            <input type="hidden" name="objectCode" value="<?= $objectCode["2"] ?>" />
                            <input type="hidden" name="user_id" value="<?= $_GET['user_id'] ?>" />
                            <input type="hidden" name="order_id" value="<?= $item->order_id ?>"/>
                            <input type="hidden" name="action" value="feedback" />
                            <input type="hidden" name="state" value="5"/>
                            <input type="hidden" name="index" value="2"/>
                            <input type="hidden" name="token" value="<?= createToken(); ?>" />
                        </div>
                        <input type="range" class="zoom-range" id="zoom-range-2" hidden>
                        <div class="row mt-5 mb-3">
                            <div class="col" id="gaziru-photo2-btn-confirm" style="display: none">
                                <input type="button" class="btn btn-secondary btn-block" value="写真を撮る" onclick="confirmPhoto('gaziru-photo2')">
                            </div> 
                            <div class="col" id="gaziru-photo2-btn-upload">
                                <input type="button" class="btn btn-secondary btn-block" id="gaziru-photo2-upload-btn" value="アップロード" onclick="uploadPhoto('gaziru-photo2')">
                                <input type="file" class="uploadFile img" id="gaziru-photo2" name="photo2" value="Upload Photo" style="width: 0px;height: 0px;overflow: hidden;">
                            </div>

                            <div class="col" id="gaziru-photo2-btn-capture">
                                <input type="button" class="btn btn-secondary btn-block" value="写真を撮る" onclick="takePhoto('gaziru-photo2')">
                            </div>

                            <div class="col" id="gaziru-photo2-btn-submit">
                                <input type="button" class="btn btn-secondary btn-block" value="判定" onclick="submitPhoto('gaziru-photo2')">
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
<script>

    const mediaConstraints = {
        audio: false,
        video: {
            width: 720,
            height: 720,
            facingMode: "environment"   // フロントカメラを利用する
            // facingMode: { exact: "environment" }  // リアカメラを利用する場合
        }
    }
    var media = null;
    var croppers = [];
    var cropping = false;
    var takePhoto1 = false;
    var takePhoto2 = false;
    var upload = false;

    document.querySelectorAll('.uploadFile').forEach(function(ele) {
            ele.addEventListener('change',function(e){
                $('#' + e.target.id + '-modal').modal('show');
                var files = !!this.files ? this.files : [];
                if (!files.length || !window.FileReader) return;
        
                if (/^image/.test( files[0].type)){ 
                    var reader = new FileReader(); 
                    reader.readAsDataURL(files[0]);
        
                    reader.onloadend = function() { 
                        const id = e.target.id
                        const canvas = document.getElementById(id + '-canvas');
                        const img =  new Image()
                        img.src = window.URL.createObjectURL(files[0]);

                        img.onload = () => {
                            canvas.getContext('2d').drawImage(img, 0, 0, canvas.width, canvas.height);
                            cropping = true;
                            upload = true;
                            setTimeout(initCropper(id + '-canvas'), 1000);
                            
                            document.getElementById(id + '-btn-confirm').style.display = 'block';
                            document.getElementById(id + '-btn-upload').style.display = 'none';
                            document.getElementById(id + '-btn-capture').style.display = 'none';
                            document.getElementById(id + '-btn-submit').style.display = 'none';
                        }
                    }
                }
            });
        });
        
        function takePhoto(id) {
            const video = document.getElementById(id + '-source')
            
            if (takePhoto1 == false && id == 'gaziru-photo1') {
                takePhoto1 = true;
                navigator.mediaDevices.getUserMedia(mediaConstraints)
                .then( (stream) => {
                    media = stream;
                    video.srcObject = stream;
                    const [track] = stream.getVideoTracks();
                    const capabilities = track.getCapabilities();
                    const settings = track.getSettings();

                    const input = document.getElementById('zoom-range-1');

                    // Check whether zoom is supported or not.
                    if (('zoom' in settings)) {
                        video.setAttribute("zoom", true);
                        // Map zoom to a slider element.
                        input.min = capabilities.zoom.min;
                        input.max = capabilities.zoom.max;
                        input.step = capabilities.zoom.step;
                        input.value = settings.zoom;
                        input.oninput = function(event) {
                            track.applyConstraints({advanced: [ {zoom: event.target.value} ]});
                        }
                        input.hidden = false;
                    }

                    
                    
                    video.onloadedmetadata = async (e) => {
                        await video.play();

                        requestAnimationFrame(updateCanvas1);

                        document.getElementById(id + '-btn-confirm').style.display = 'block';
                        document.getElementById(id + '-btn-upload').style.display = 'none';
                        document.getElementById(id + '-btn-capture').style.display = 'none';
                        document.getElementById(id + '-btn-submit').style.display = 'none';
                    };

                })
                .catch( (err) => {
                    console.log(err.name + ": " + err.message);
                });
            } else {
                takePhoto1 = false;
                document.getElementById(id + '-btn-confirm').style.display = 'none';
                document.getElementById(id + '-btn-upload').style.display = 'none';
                document.getElementById(id + '-btn-capture').style.display = 'block';
                document.getElementById(id + '-btn-submit').style.display = 'block';
            }

            if (takePhoto2 == false && id == 'gaziru-photo2') {
                takePhoto2 = true;
                navigator.mediaDevices.getUserMedia(mediaConstraints)
                .then( (stream) => {
                    media = stream
                    video.srcObject = stream;
                    const [track] = stream.getVideoTracks();
                    const capabilities = track.getCapabilities();
                    const settings = track.getSettings();

                    const input = document.getElementById('zoom-range-2');

                    // Check whether zoom is supported or not.
                    if (('zoom' in settings)) {
                        video.setAttribute("zoom", true);
                        // Map zoom to a slider element.
                        input.min = capabilities.zoom.min;
                        input.max = capabilities.zoom.max;
                        input.step = capabilities.zoom.step;
                        input.value = settings.zoom;
                        input.oninput = function(event) {
                            track.applyConstraints({advanced: [ {zoom: event.target.value} ]});
                        }
                        input.hidden = false;
                    }

                    video.onloadedmetadata = async (e) => {
                        await video.play();

                        requestAnimationFrame(updateCanvas2);

                        document.getElementById(id + '-btn-confirm').style.display = 'block';
                        document.getElementById(id + '-btn-upload').style.display = 'none';
                        document.getElementById(id + '-btn-capture').style.display = 'none';
                        document.getElementById(id + '-btn-submit').style.display = 'none';
                    };
                })
                .catch( (err) => {
                    console.log(err.name + ": " + err.message);
                });
            } else {
                takePhoto2 = false;
                document.getElementById(id + '-btn-confirm').style.display = 'none';
                document.getElementById(id + '-btn-upload').style.display = 'none';
                document.getElementById(id + '-btn-capture').style.display = 'block';
                document.getElementById(id + '-btn-submit').style.display = 'block';
            }
        }

        function updateCanvas1() {
            const video = document.getElementById('gaziru-photo1-source')
            const canvas = document.getElementById('gaziru-photo1-canvas')

            var ctx = canvas.getContext('2d')

            ctx.drawImage(video, 0, 0, canvas.width, canvas.height)

            if (takePhoto1 == true) {
            
                ctx.beginPath();
                ctx.lineWidth = "2";
                ctx.strokeStyle = "#77acf1";
                ctx.rect(canvas.width / 2 - 300, canvas.height / 2 - 300, 600, 600);

                ctx.moveTo(canvas.width / 2 - 10, canvas.height / 2);
                ctx.lineTo(canvas.width / 2 + 10, canvas.height / 2);

                ctx.moveTo(canvas.width / 2, canvas.height /2 - 10);
                ctx.lineTo(canvas.width / 2, canvas.height /2 + 10);

                ctx.stroke();
                requestAnimationFrame(updateCanvas1)
            } else if(media) {
                media.getTracks().forEach(function(track) {
                    track.stop();
                });
                media = null;
            }
        }

        function updateCanvas2() {
            const video = document.getElementById('gaziru-photo2-source')
            const canvas = document.getElementById('gaziru-photo2-canvas')

            var ctx = canvas.getContext('2d')

            ctx.drawImage(video, 0, 0, canvas.width, canvas.height)

            if (takePhoto2 == true) {
                ctx.beginPath();
                ctx.lineWidth = "2";
                ctx.strokeStyle = "#77acf1";
                ctx.rect(canvas.width / 2 - 300, canvas.height / 2 - 300, 600, 600);

                ctx.moveTo(canvas.width / 2 - 10, canvas.height / 2);
                ctx.lineTo(canvas.width / 2 + 10, canvas.height / 2);

                ctx.moveTo(canvas.width / 2, canvas.height /2 - 10);
                ctx.lineTo(canvas.width / 2, canvas.height /2 + 10);

                ctx.stroke();
                requestAnimationFrame(updateCanvas2)
            } else if(media) {
                media.getTracks().forEach(function(track) {
                    track.stop();
                });
                media = null;
            }
        }

        function uploadPhoto(id) {
            
            if(!cropping) {
                $('#' + id + '-modal').modal('hide');
                document.getElementById(id).click();
            } else {
                console.log(croppers);
                if (croppers.find(({ id }) => id === id) != null) {
                    const cropper = croppers.find(({ id }) => id === id);
                    const croppedCanvas =  cropper.cropper.getCroppedCanvas({maxWidth: 1023, maxHeight: 1023})
                    const canvas = document.getElementById(id + '-canvas')
                    canvas.getContext('2d').drawImage(croppedCanvas, 0, 0, canvas.width, canvas.height)

                    croppers.splice(croppers.indexOf(cropper), 1)
                    cropper.cropper.destroy()
                    cropping = false
                    document.getElementById(id + '-upload-btn').value = 'アップロード';
                    document.getElementById(id + '-btn-capture').style.display = 'none';
                }
            } 
        };

        function initCropper(name){
            var image = document.getElementById(name);
            var cropper = new Cropper(image, {
                viewMode: 2,
                center: true,
                responsive: true,
                scalable: false,
                zoomable: false,
                aspectRatio: 1/1,
            })
            
            croppers.push({id: name, cropper: cropper});
        }

        function confirmPhoto(id) {
            document.getElementById(id + '-btn-confirm').style.display = 'none';
            document.getElementById(id + '-btn-upload').style.display = 'block';
            document.getElementById(id + '-btn-capture').style.display = 'none';
            document.getElementById(id + '-btn-submit').style.display = 'block';
            
            console.log(upload, takePhoto1, takePhoto2)

            if(upload) {
                uploadPhoto(id)
            } else if (takePhoto1 || takePhoto2) {
                takePhoto(id)
            }
            
            
        }

        
        function submitPhoto(id) {
            takePhoto1 = false;
            takePhoto2 = false;

            const fileInput = document.getElementById(id)
            const modal = document.getElementById(id + '-modal')
            const canvas = document.getElementById(id + '-canvas')
            var formData = null;

            var imgurl =  canvas.toDataURL();
            let blob = fetch(imgurl).then(r => r.blob()).then(
                blobFile => {
                    let newFile = new File([blobFile], id, { type: "image/jpg" });
                    // let container = new DataTransfer();
                    // container.items.add(newFile);
                    // fileInput.files = container.files;
                    // console.log(container.items)

                    if(id == 'gaziru-photo1') {
                        formData = new FormData(document.getElementById('fr-FeedBack1'));
                        formData.append('photo', newFile);
                    } else if (id == 'gaziru-photo2') {
                        formData = new FormData(document.getElementById('fr-FeedBack2'));
                        formData.append('photo2', newFile);
                    }

                    $.ajax({
                        url: 'feedback_details.php?item_id=<?php echo $_GET['item_id']?>&user_id=<?php echo $_GET['user_id']?>',
                        type: 'POST',
                        data: formData,
                        contentType: false,
                        processData: false,
                        success: function(msg) {
                            console.log(msg);
                            $('#' + id + '-modal').modal('hide');
                            if(msg == 201) {
                                $('#container').load(document.URL +  ' #container');
                                // alert('真贋比較画像とアップロードした画像がそれぞれ表示されるようにしてください。')
                            } else {
                                // alert('コメントまたは画像( jpg,jpeg,png,gif )。');
                            }
                        }               
                    }).then(function (data) {
                        cropping = false;
                        document.getElementById(id + '-upload-btn').value = 'アップロード';
                    }).catch(function (err) {
                        console.error(err);
                    });
                }
            );
        }

        function closePhoto(id){
            for (const obj in croppers ) {
                croppers[obj].cropper.destroy();
                cropping=false;
                croppers[obj].cropper.cropped = false;
                croppers[obj].cropper.crossOriginUrl = "";
            }
            
            const canvas = document.getElementById(id + '-canvas');
            var ctx = canvas.getContext('2d')
            ctx.clearRect(0, 0, canvas.width, canvas.height);
            
        }
</script>

<link href="../common/assets/js/ImageCropper/cropper.css" rel="stylesheet">
<script src="../common/assets/js/ImageCropper/cropper.js"></script>

<?php include('../user_footer.php'); ?>