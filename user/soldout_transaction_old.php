<?php include_once(__DIR__ . '/../common/util.php'); ?>
<?php include_once(__DIR__ . '/../common/login_check.php'); ?>
<?php
$item = new Item();
$items = $item->selectSell(getUserId());
usort($items, function ($a, $b) {
    $adate = empty($a->last_date) ? $a->order_date : $a->last_date;
    $bdate = empty($b->last_date) ? $b->order_date : $b->last_date;
    if (strtotime($adate) == strtotime($bdate)) {
        return 0;
    } elseif (strtotime($adate) > strtotime($bdate)) {
        return -1;
    }
    return 1;
});


$page = 1;
$page = empty($_POST['page']) ? '1' : $_POST['page'];
$mode = isset($_GET['mode']) ? $_GET['mode'] : '';
$negotiation = false;
if (isset($_POST['mode']) && $_POST['mode'] == 'negotiation') {
    $mode = $_POST['mode'];
}
if (isset($mode) && $mode == 'negotiation') {
    $negotiation = true;
}
?>
<?php include('../header.php'); ?>
<form id="fm-param" method="post">
    <input type="hidden" id="mode" name="mode" value="<?= $mode ?>"/>
    <input type="hidden" id="id-page" name="page" value="<?= $page ?>"/>
</form>
<div class="container">
    <div class="row">
        <div class="col-md-3 sideContents">
            <?php include('usersidebar.php'); ?>
        </div>
        <div class="col-md-9 mainContents">
            <div class="bg-inner">
                <h1>出品した<?= ITEM_NAME ?>-販売済取引中<span class="smallEX">※表示期間は販売日から90日間です。</span></h1>
                <table class="table table-bordered table-striped">
                    <tr>
                        <!--<th>画像</th>-->
                        <!--<th>販売日</th>-->
                        <th>最終連絡日</th>
                        <th>タイトル</th>
                        <th>価格</th>
                        <!--<th>交渉数</th>-->
                        <!--                    <th>購入者ID</th>-->
                        <!--<th>購入者の評価</th>-->


                    </tr>
                    <?php
                    $count = 0;
                    $index = 0;
                    foreach ($items as $item):
                        if (diffDays(date("Y/m/d"), $item->order_date) > MAX_VIEW_DAYS) {
                            continue;
                        }
                        ?>
                        <?php if (!empty($item->buyer_id)) :
                        $count++;
                        if (ceil(++$index / MAX_PAGE_COUNT) != $page) {
                            continue;
                        }
                        ?>
                        <?php
                        $buyer = new User();
                        $buyer->select($item->buyer_id);
                        ?>
                        <tr>
                            <!--                        <td class="col-sm-2 text-center"><img class="selllist-img" src="-->
                            <?php //echo getContextRoot()
                            ?><!--/images/item/id--><?php //echo $item->id
                            ?><!--/thumbnail.jpg" alt=""/></td>-->
                            <!--                        <td class="col-sm-2 text-right">--><?php //echo $item->regist_date
                            ?><!--</td>-->
                            <td class="text-right"><?= empty($item->last_date) ? $item->order_date : $item->last_date ?></td>
                            <td>
                                <!--                            <a href="transaction.php?item_id=--><?php //echo $item->id
                                ?><!--">--><?php //echo $item->title
                                ?><!--</a>-->
                                <a href="negotiation.php?item_id=<?php echo $item->id ?>&user_id=<?php echo $item->buyer_id ?>" <?php echo $item->createBlockScript($item->buyer_id) ?>><?= $item->title ?></a>
                            </td>
                            <td class="text-right"><?= number_format($item->price) ?>円</td>
                            <!-- <td class="col-sm-1 text-center">--><?php //echo number_format($item->negotiation_count) ?><!--件</td>-->
                            <!--                        <td>-->
                            <!--                            --><?php //echo $buyer->name ?>
                            <!--                            --><?php //echo $buyer->getViewId() ?>
                            <!--                        </td>-->
                            <?php /*?>
                              　 <?php if ($item->seller_evaluate == 0): ?>
                                <td class="col-sm-1 text-center"><a href="evaluation.php?id=<?= $item->order_id ?>">未評価</a></td>
                                <?php else: ?>
                                <td class="col-sm-1 text-center"><a href="evaluation.php?id=<?= $item->order_id ?>"><?= $item->seller_evaluate_name ?></a></td>
                                <?php endif; ?>
                                <?php */
                            ?>
                        </tr>
                    <?php endif; ?>
                    <?php endforeach; ?>
                </table>
                <div class="text-center margin-20">
                    <?php
                    $prev_page = $page - 1;
                    $max_page = ceil($count / MAX_PAGE_COUNT);
                    $next_page = min($max_page, $page + 1);
                    $view_start_page = max(1, $page - ceil(VIEW_PAGE_SELECT_COUNT / 2));
                    $view_last_page = min($view_start_page + VIEW_PAGE_SELECT_COUNT - 1, $max_page);
                    if ($view_last_page - $view_start_page < VIEW_PAGE_SELECT_COUNT) {
                        $view_start_page = max(1, $view_last_page - VIEW_PAGE_SELECT_COUNT + 1);
                    }
                    ?>
                    <?php if ($prev_page > 0): ?>
                        <a class="sortButton" href=""
                           onclick="submitForm('fm-param', 'page', '<?= $prev_page ?>'); return false;">前へ</a>
                    <?php else: ?>
                        <a class="sortButton" href="" onclick="return false;">前へ</a>
                    <?php endif; ?>

                    <?php for ($i = $view_start_page; $i <= $view_last_page; $i++): ?>
                        <?php if ($page != $i): ?>
                            <a class="sortButton" href=""
                               onclick="submitForm('fm-param', 'page', '<?= $i ?>'); return false;"><?= $i ?></a>
                        <?php else: ?>
                            <a class="sortButton active" href="" onclick="return false;"><?= $i ?></a>
                        <?php endif; ?>
                    <?php endfor; ?>

                    <?php if ($page < $max_page): ?>
                        <a class="sortButton" href=""
                           onclick="submitForm('fm-param', 'page', '<?= $next_page ?>'); return false;">次へ</a>
                    <?php else: ?>
                        <a class="sortButton" href="" onclick="return false;">次へ</a>
                    <?php endif; ?>

                    <p class="small"><?= number_format(($page - 1) * MAX_PAGE_COUNT + 1) ?>
                        ～<?= number_format(min($count, $page * MAX_PAGE_COUNT)) ?>
                        件/<?= number_format($count) ?>
                        件</p>
                </div>
            </div>
        </div>
    </div>
</div>
<?php include('../footer.php'); ?>
