<?php include_once(__DIR__ . '/../common/util.php'); ?>
<?php
if($_GET['step'] == 1){
    header('Location: ' . getContextRoot() . '/pages/kiyaku.php?step=1');
    exit;
}
if($_GET['step'] == 2){
    $entity = new User();
    $sell_item = new Item();
    $db = new DB();
	$list = $db->query("selectCategory");
    $sell_item->category = empty($_POST['category']) ? null : $_POST['category'];
    if (isset($_POST['action']) && $_POST['action'] == 'regist') {
        $entity->email = $_POST['email'];
        $entity->name = $_POST['name'];
        $entity->nickname = $_POST['name'];
        // $entity->name_kana = $_POST['name_kana'];
        // $entity->zip = $_POST['zip'];
        // $entity->addr = $_POST['addr'];
        $entity->password = $_POST['password'];
        $entity->age = $_POST['age']; 
        $entity->sex = $_POST['sex'];
        $entity->item_interest = $_POST['items_interest'];
    //     $entity->company = $_POST['company'];
    //     $entity->tel = $_POST['tel'];
    //     $entity->pref = $_POST['pref'];
    //     $entity->type = $_POST['type'];
        $entity->ipaddr = getRemoteAddress();
        if($_POST['length_items_interest'] > 3){
            setMessage('興味のあるアイテムを3つまで登録してください');
        }elseif (!$entity->validate()) {

        } elseif ($entity->has()) {
            setMessage('既にメールアドレスが登録済みです。');
        }else{
            $entity->regist();
            //setMessage('仮登録通知メールを送付いたしました。');
            header('Location: ' . getContextRoot() . '/user/userregist.php');
            exit;
        }
        // if ($_POST['zip'] && !preg_match('/(^\d{5}$)|(^\d{3}-\d{4}$)/', $_POST['zip'])) { 
        //     setMessage('無効な郵便番号');
        // } elseif (empty($_POST['kiyaku'])){
        //     setMessage('利用規約に同意されておりません。');
        // } elseif (!$entity->validate()) {

        // } elseif ($entity->has()) {
        //     setMessage('既にメールアドレスが登録済みです。');
        // } elseif ($_POST['email'] != $_POST['email2']) {
        //     setMessage('パスワードとパスワード（確認）が一致しません。');
        // } else {
        //     $entity->regist();
        //     //setMessage('仮登録通知メールを送付いたしました。');
        //     header('Location: ' . getContextRoot() . '/user/userregist.php');
        //     exit;
        // }
    } elseif (isset($_POST['action']) && $_POST['action'] == 'send-mail') {
        $entity->email = $_POST['email'];
        $entity->password = $_POST['password'];
        if (!$entity->selectFromEmail()) {
            setMessage("仮登録しているメールアドレス・パスワードを指定してください。");
        } elseif ($entity->enabled != 0) {
            setMessage("指定されているメールアドレスのアカウントは本登録済みです。");
        } else {
            $entity->sendRegistMail();
            setMessage("仮登録メールを再送しました。");
        }
    }
    $title_page = '新規会員登録';
    $description_page = '欲しい商品を画像で確認・届いた商品を画像で照合！気持ち良い取引が叶うオンラインマーケット。';

}
?>
<?php include(__DIR__ . '/../other_header.php'); ?>
<style>
    .contact-form-content__tag label{
        display: block;
    }
    .contact-form-content__tag span{
        position: absolute;
        margin-top: 10px;
        margin-left: 15px;
        font-size: 15px;
    }
</style>
<script>
    function sendRegistMail() {
        var frm = $('#form-send-mail');
        $('#send-mail-email').val($("#email").val());
        $('#send-mail-password').val($("#password").val());
        frm.submit();
    }

    function check(){
        var form = $('#form');
        var array = []
        var checkboxes = document.querySelectorAll('input[type=checkbox]:checked')

        for (var i = 0; i < checkboxes.length; i++) {
            array.push(checkboxes[i].value)
        }
        
        document.getElementById("items_interest").value = array;
        console.log(array.length);
        document.getElementById("length_items_interest").value = array.length;
        
    }

    function isNumberKey(evt) {
        var charCode = (evt.which) ? evt.which : evt.keyCode;
        if (charCode > 31 && (charCode < 48 || charCode > 57))
            return false;
        return true;
    }
</script>
    <form id="form-send-mail" method="post" onsubmit="return confirm('仮登録メールを再送する');">
        <input id="send-mail-email" type="hidden" name="email"/>
        <input id="send-mail-password" type="hidden" name="password"/>
        <input type="hidden" name="action" value="send-mail"/>
    </form>

    <div class="com-header-top">
        <div class="com-header-top__img wow animate__animated animate__fadeInUp">
            <img src="<?php echo HOME_URL; ?>/common/assets/img/common/other-com-header.png" alt="">
        </div>
        <div class="com-header-top__path bg-other-01 wow animate__animated animate__fadeInUp">
            <p><span><a href="<?php echo HOME_URL; ?>/" class="clr-yel">トップページ</a></span><span> > </span><span>新規会員登録</span></p>
        </div>
        <div class="com-header-top__txt">
            <p>新規会員登録</p>
        </div>
    </div>
    <div class="customer-container">
        <div class="customer-contact-form">
            <form id="form" method="post" onsubmit="return confirm('登録する');" class="wow animate__animated animate__fadeInUp c-form01">
                <input type="hidden" name="action" value="regist"/>
                <div class="contact-form-content">
                    <div class="contact-form-content__txt">
                        <p>ニックネーム<span class="obligatory ml-2">※必須</span></p>
                    </div>
                    <div class="contact-form-content__tag">
                        <input class="width-35 u-sp-w100" type="text" name="name" id="name" value="<?= $entity->nickname ?>" placeholder="ニックネーム">
                    </div>
                </div>
                
                <div class="contact-form-content">
                    <div class="contact-form-content__txt">
                        <p>メールアドレス<span class="obligatory ml-2">※必須</span></p>
                    </div>
                    <div class="contact-form-content__tag">
                        <input class="width-50 u-sp-w100" type="text" name="email" id="email" value="<?= $entity->email ?>" placeholder="例：teppan@example.com">
                    </div>
                </div>
                <div class="contact-form-content">
                    <div class="contact-form-content__txt">
                        <p>年齢<span class="obligatory ml-2">※必須</span></p>
                    </div>
                    <div class="contact-form-content__tag">
                        <input class="width-50 u-sp-w100" type="number" name="age" id="age" value="<?= $entity->age ?>" placeholder="例：18" min="0" onkeypress="return isNumberKey(event)">
                    </div>
                </div>
                <div class="contact-form-content">
                    <div class="contact-form-content__txt">
                        <p>性別</p>
                    </div>
                    <div class="contact-form-content__tag product-info-select">
                        <div class="radio">
                            <?php if( $entity->sex == 0): ?>
                                <div class="d-flex align-items-center">
                                    <input id="first" type="radio" name="sex" value="0" checked="checked">  
                                    <label for="first">男性</label>  
                                </div><br/>
                                <div class="d-flex align-items-center">
                                    <input id="second" type="radio" name="sex" value="1">  
                                    <label for="second">女性</label>    
                                </div>
                            <?php else: ?>
                                <div class="d-flex align-items-center">
                                    <input id="first" type="radio" name="sex" value="0">  
                                    <label for="first">男性</label>  
                                </div><br/>
                                <div class="d-flex align-items-center">
                                    <input id="second" type="radio" name="sex" value="1" checked="checked">  
                                    <label for="second">女性</label>    
                                </div>
                            <?php endif; ?>
                        </div> 
                    </div>
                </div>
                <div class="contact-form-content">
                    <div class="contact-form-content__txt">
                        <p>興味のあるアイテム (3まで登録)</p>
                    </div>
                    <div class="contact-form-content__tag">
                        <ul class="unstyled centered">
                            <?php createCheckBoxCategories('selectCategory', 'item_interest', $entity->item_interest); ?>
                        </ul>
                    </div>
                </div>
                <input type="hidden" id="items_interest" name="items_interest" value=""/>
                <input type="hidden" id="length_items_interest" name="length_items_interest" />
                <div class="contact-form-content">
                    <div class="contact-form-content__txt">
                        <p>パスワード<span class="obligatory ml-2">※必須</span></p>
                    </div>
                    <div class="contact-form-content__tag">
                        <input minlength="6" maxlength="32" class="width-50 u-sp-w100" type="password" id="password" name="password"  value="<?= $entity->password ?>" placeholder="半角英数記号6〜32文字">
                    </div>
                </div>
               
                <div class="conform-submit__btn">
                    <button onclick="return check()">登録する</button>
                </div>
            </form>
        </div>
    </div>

<?php include('../footer.php'); ?>
<script>
    $( ".footer-login" ).hide();
</script>
