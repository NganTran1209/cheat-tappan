<?php include_once(__DIR__ . '/../common/util.php'); ?>
<?php include_once(__DIR__ . '/../common/login_check.php'); ?>
<?php
/*
2017/11/18 evaluation.php を複製して改変した。
if (!isLogin()) {
	setMessage('ログインをしてください。');
	header('Location: '.getContextRoot().'/');
	exit;
}

if (isset($_POST['action'])) {
	if ($_POST['action'] == 'evaluate') {
		$order = new Order();
		$order->id = $_POST['id'];
		if (isset($_POST['buyer_evaluate'])) {
			$order->buyer_evaluate = $_POST['buyer_evaluate'];
			$order->buyer_comment = $_POST['buyer_comment'];
			$order->registEvaluateByBuyer();
		}
		elseif (isset($_POST['seller_evaluate'])) {
			$order->seller_evaluate = $_POST['seller_evaluate'];
			$order->seller_comment = $_POST['seller_comment'];
			$order->registEvaluateBySeller();
		}
		else {
			setMessage('不正なアクションです。');
			header('Location: '.getContextRoot().'/user/mypage.php');
			exit;
		}

		setMessage('評価を登録しました。');
	}
}
*/
$order = new Order();
$order->select($_GET['id']);
$item = new Item();
$item->select($order->item_id);
$owner = new User();
$owner->select($order->owner_id);
$evaluate = new Evaluate();
$evaluate->selectSummary($item->owner_id);
$evaluateBuyer = new Evaluate();
$evaluateBuyer->selectSummary($order->user_id);

$user = new User();
$user->select($order->user_id);

//$noimagepng = "http://192.168.55.10/ccsystem/images/noimage.png";
$countFavorites = $item->selectCountFavorites();
?>

<?php include(__DIR__ . '/../header.php'); ?>
    <div class="container">
        <div class="row">
            <!-- <div class="col-md-3 sideContents"> -->
                <?php include('usersidebar.php'); ?>
            <!-- </div> -->
            <div class="col-md-9 mainContents">
                <div class="bg-inner">
                    <div class="itemTitle">
                        <h1><?= $item->title ?></h1>
                    </div>
                    <div class="row">
                        <?php include_once('../common/parts/item_image_area.php'); ?>
                        <?php include_once('../common/parts/item_info_area.php'); ?>
                    </div>
                    <div class="mb-4">
                        <div class="text-center">
                            <span class="font30 fontBold fontRed"><?= number_format($item->price) ?></span>
                            <span class="smallEX">円(税込・<?= $item->carriage_plan_name ?>)</span>
                        </div>
                    </div>
                    <div class="itemContentText">
                        <?php echo nl2br(htmlentities($item->remarks)); ?>
                    </div>
                </div>
            </div>
        </div>
    </div>

<?php include('../footer.php'); ?>