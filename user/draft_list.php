<?php include_once(__DIR__ . '/../common/util.php'); ?>
<?php include_once(__DIR__.'/../common/login_check.php'); ?>
<?php
$item = new Item();
if (isset($_POST['action']) && $_POST['action'] == 'remove') {
    $item->id = $_POST['tmp-id'];
    $item->removeTmp();
    setMessage('下書きを削除しました。');
}

$items = $item->selectTmpFromUser(getUserId());
usort($items, function ($a, $b) {
    if (strtotime($a->regist_date) == strtotime($b->regist_date)) {
        return 0;
    } elseif (strtotime($a->regist_date) > strtotime($b->regist_date)) {
        return -1;
    }
    return 1;
});
?>
<?php include('../header.php'); ?>
<form id="frm" action="sell.php" method="post">
    <input type="hidden" id="action" name="action" value="load-tmp"/>
    <input type="hidden" id="id-tmp-id" name="tmp-id"/>
</form>
<div class="container">
    <div class="row">
        <!-- <div class="col-md-3 sideContents"> -->
            <?php include('usersidebar.php'); ?>
        <!-- </div> -->
        <!-- col-sm-3 -->
        <div class="col-md-9 mainContents">
            <div class="bg-inner">
                <h1>下書き保存一覧</h1>
                <div class="mb-3 small">※10件まで下書きが保存できます。<br>
                    タイトルをタップ(クリック)すると、出品画面に下書きした内容が表示されます。
                </div>
                <table class="table table-bordered">
                    <tr>
                        <th>下書き保存日</th>
                        <th>タイトル</th>
                        <th>削除</th>
                    </tr>
                    <?php
                    foreach ($items as $item):
                        ?>
                        <tr>
                            <td class="text-right"><?= $item->regist_date ?></td>
                            <td><a href=""
                                   onclick="submitForm('frm', 'tmp-id', '<?= $item->id ?>'); return false;"><?= $item->title ?></a>
                            </td>
                            <td>
                                <form action="" method="post">
                                    <input type="hidden" name="action" value="remove"/>
                                    <input type="hidden" name="tmp-id" value="<?= $item->id ?>"/>
                                    <input class="btn btn-block btn-outline-danger btn-sm" type="submit" value="削除"/>
                                </form>
                            </td>
                        </tr>
                    <?php endforeach; ?>
                </table>
                <form action="sell.php" method="post">
                    <input type="hidden" name="action" value="cancel-load-tmp"/>
                    <input class="btn btn-block btn-outline-dark" type="submit" value="出品するへ戻る"/>
                </form>
            </div>
        </div>
    </div>
</div>


<?php include('../footer.php'); ?>
