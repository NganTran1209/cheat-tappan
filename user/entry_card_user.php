<?php include_once(__DIR__.'/../common/util.php'); ?>
<?php include_once(__DIR__.'/../common/login_check.php'); ?>
<?php include_once(__DIR__.'/../common/stripe_api.php'); ?>
<?php
$invoice = new Invoice();
$items = $invoice->selectUser(getUserId());
$amount = 0;
$card_info = new CardInfo();
$card_info->select(getHashKey());
foreach ($items as $value) {
    if ($value->status == 1) {
        $amount += $value->amount;
    }
}

if (empty($amount)) {
    setMessage('不正なアクセスです。。');
    header('Location: ' . getContextRoot() . '/');
    exit;
}

if (!empty($card_info) && !empty($card_info->customer_id)) {
    if (StripeApi::charge($amount)) {
        setPayment($items);
        setMessage('入金されました。', '手数料の支払い');
        header("location: ". getContextRoot().'/user/invoice_details.php');
        exit();
    }
}

if (isset($_POST['stripeToken'])) {
    if (StripeApi::regist_customer()) {
        if (StripeApi::charge($amount, '手数料の支払い')) {
            setPayment($items);
            setMessage('入金されました。');
            header("location: ". getContextRoot().'/user/invoice_details.php');
            exit();
        }
    }
}

function setPayment($items) {
    foreach ($items as $value) {
        if ($value->status == 1) {
            $value->status = 2;
            $value->updateStatus();
        }
    }
}

?>
<?php include(__DIR__.'/../header.php'); ?>
    <div id="information" class="container bg-white py-5">
        <div>
            <h2>クレジットカードで入金する。</h2>
            <p>
            ご請求金額は<?= $amount ?>円です。
            </p>
        </div>
      <div>
            <form method="POST">
                <div>
                    <input type="hidden" name="amount" value="<?= $amount ?>" />
                </div>
                <script src="https://checkout.stripe.com/checkout.js" class="stripe-button"
                data-key='<?= STRIPE_API_KEY ?>'
                data-amount="<?= $amount ?>"
                data-email="<?= getUserEmail() ?>"
                data-name="ご請求金額は<?= $amount ?>円です。"
                data-locale="auto"
                data-allow-remember-me="false"
                data-label="クレジットカードで支払う"
                data-currency="jpy">
                </script>
            </form>
      </div>
    </div>
<?php include('../footer.php'); ?>