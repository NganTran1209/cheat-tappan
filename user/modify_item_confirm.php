<?php include_once(__DIR__ . '/../common/util.php'); ?>
<?php
if (!isLogin()) {
    setMessage('ログインをしてください。');
    header('Location: ' . getContextRoot() . '/');
    exit;
}

$isOffer = isset($_SESSION['targetRequest']) && !empty($_SESSION['targetRequest']);

$exhibited_item = $_SESSION['item'];
if (isset($_POST['action']) && $_POST['action'] == 'regist') {
    if ($isOffer) {
        $req = $_SESSION['targetRequest'];
        $exhibited_item->target_user_id = $req->user_id;
    }
    $exhibited_item->modify();
    header('Location: ' . getContextRoot() . '/user/modify_item.php?id=' . $exhibited_item->id);
    exit();
}

if($exhibited_item != null){
    $exhibited_item->repairName();
}else{
    header('Location: '.getContextRoot().'/user/sellinglist.php');
    exit();
}

$title_page = '商品情報を更新する';
?>

<?php include(__DIR__ . '/../user_header.php'); ?>
<div class="com-header-top">
    <div class="com-header-top__img">
        <img src="<?php echo HOME_URL; ?>/common/assets/img/mypage/top-bg.png" alt="">
    </div>
    <!--     <div class="com-header-top__txt">
            <p class="clr-white">マイページ</p>
        </div>-->
</div>
<div class="com-container bg-yellow">
    <?php include('usersidebar.php'); ?>
    <div class="com-content">
            <div class="content-title">
                <h3><span>商品情報を更新する</span></h3>
            </div>
            <div class="border-bottom py-4">
                <h2 class="bg-light p-2 fontBold my-3">商品の情報</h2>
                <h3 class="fontBold my-2">商品画像</h3>
                <div class="row">
                    <div class="col-md-3 mb-2">
                        <img class="img-fluid" src="<?= getImagePath($exhibited_item->id, 1) ?>" alt="">
                    </div>
                    <?php $url = getImagePath($exhibited_item->id, 2);
                    if (strstr($url, "noimage") == false): ?>
                        <div class="col-md-3 mb-2">
                            <img class="img-fluid" src="<?= getImagePath($exhibited_item->id, 2) ?>" alt=""/>
                        </div>
                    <?php endif; ?>
                    <?php $url = getImagePath($exhibited_item->id, 3);
                    if (strstr($url, "noimage") == false): ?>
                        <div class="col-md-3 mb-2">
                            <img class="img-fluid" src="<?= getImagePath($exhibited_item->id, 3) ?>" alt=""/>
                        </div>
                    <?php endif; ?>
                    <?php $url = getImagePath($exhibited_item->id, 4);
                    if (strstr($url, "noimage") == false): ?>
                        <div class="col-md-3 mb-2">
                            <img class="img-fluid" src="<?= getImagePath($exhibited_item->id, 4) ?>" alt=""/>
                        </div>
                    <?php endif; ?>
                </div>
            </div>

            <div class="border-bottom py-4">
                <h2 class="bg-light p-2 fontBold">商品の詳細</h2>
                <h3 class="fontBold">タイトル</h3>
                <div><?= $exhibited_item->title ?></div>
                <h3 class="fontBold my-2">商品の説明文</h3>
                <?php echo nl2br(htmlentities($exhibited_item->remarks)); ?>
                <h3 class="fontBold my-2">カテゴリー</h3>
                <?= $exhibited_item->category_name ?>
                <h3 class="fontBold my-2">商品の状態</h3>
                <?= $exhibited_item->item_state_name ?>
            </div>

            <div class="border-bottom py-4">
                <h2 class="bg-light p-2 fontBold my-3">商品の配送について</h2>
                <h3 class="fontBold my-2">地域</h3>
                <?= $exhibited_item->pref_name ?>
                <h3 class="fontBold my-2">配送料の負担</h3>
                <?= $exhibited_item->carriage_plan_name ?>
                <h3 class="fontBold my-2">配送の目安</h3>
                <?= $exhibited_item->carriage_type_name ?>
                <h3 class="fontBold my-2">配送方法</h3>
                <?= $exhibited_item->delivery_type_name ?>
            </div>

            <div class="border-bottom py-4">
                <h2 class="bg-light p-2 fontBold my-3">価格(300円～9,999,999円)</h2>
                <h3 class="fontBold my-2">商品価格</h3>
                <?= number_format($exhibited_item->price) ?>円
            </div>

            <div class="row py-4">
                <div class="col-md-6 mb-3">
                    <form method="post" onsubmit="return window.confirm('更新する。');">
                        <input type="hidden" name="action" value="regist"/>
                        <input class="btn btn-success btn-block btn-sm" type="submit" value="更新する"/>
                    </form>
                </div>
                <div class="col-md-6 mb-3">
                    <form method="post" action="modify_item.php">
                        <input type="hidden" name="action" value="modify"/>
                        <input type="hidden" name="id" value="<?= $exhibited_item->id ?>"/>
                        <input class="btn btn-light btn-block btn-sm" type="submit" value="修正する"/>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>

<?php include('../footer.php'); ?>
