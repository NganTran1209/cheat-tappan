<?php include_once(__DIR__ . '/../common/util.php'); ?>

<?php
$user = new User();
$user->select(getUserId());
$title_page = '出金申請・履歴';
?>
<?php include(__DIR__ . '/../user_header.php'); ?>
<div class="com-header-top">
    <div class="com-header-top__img wow animate__animated animate__fadeInUp">
        <img src="<?php echo HOME_URL; ?>/common/assets/img/mypage/top-bg.png" alt="">
    </div>
    <div class="com-header-top__path wow animate__animated animate__fadeInUp">
        <p><span><a href="<?php echo HOME_URL; ?>/index.php" class="clr-yel">トップページ</a></span><span> > </span><span><a href="<?php echo HOME_URL; ?>/user/mypage.php" class="clr-yel">マイページ</a></span><span> > </span><span>出金申請・履歴</span></p>
    </div>
    <div class="com-header-top__txt">
        <p class="clr-white">マイページ</p>
    </div>
</div>

<div class="com-container bg-yellow">
    <?php include('usersidebar.php'); ?>
    <div class="com-content">
        <div class="content-title">
            <h3><span>出金申請・履歴</span></h3>
        </div>
        <div class="qnego-sm-title">
            <p>◯円以上にならなければ、出金申請ができませんのでご注意下さいませ。<br>
                また、出金時振込手数料が発生いたします。</p>
        </div>
        <div class="total-cal">
            <div class="total-cal__txt">
                <h2>合計残高：<span>0</span>円</h2>
                <p>（うち出金可能な金額：0円）</p>
            </div>
            <div class="ttal-cal__btn">
                <button type="button">出金申請</button>
            </div>
        </div>
        <div class="table-responsive purchase__wishes">
            <table class="table table-bordered">
                <thead>
                    <tr>
                    <th scope="col">確定日時</th>
                    <th scope="col">状態</th>
                    <th scope="col">タイトル</th>
                    <th scope="col">予算</th>
                    </tr>
                </thead>
                <tbody>
                    <tr>
                    <td scope="row">2021-01-04 17:33:09</td>
                    <td>掲載中</td>
                    <td class="clr-yel">あいうえおあいうえおあいうえおあいうえ...</td>
                    <td>1,980円</td>
                    </tr>
                    <tr>
                    <td scope="row">2021-01-04 17:33:09</td>
                    <td>掲載中</td>
                    <td class="clr-yel">あいうえおあいうえおあいうえおあいうえ...</td>
                    <td>1,980円</td>
                    </tr>
                    <tr>
                    <td scope="row">2021-01-04 17:33:09</td>
                    <td>掲載中</td>
                    <td class="clr-yel">あいうえおあいうえおあいうえおあいうえ...</td>
                    <td>1,980円</td>
                    </tr>
                    <tr>
                    <td scope="row">2021-01-04 17:33:09</td>
                    <td>掲載中</td>
                    <td class="clr-yel">あいうえおあいうえおあいうえおあいうえ...</td>
                    <td>1,980円</td>
                    </tr>
                    <tr>
                    <td scope="row">2021-01-04 17:33:09</td>
                    <td>掲載中</td>
                    <td class="clr-yel">あいうえおあいうえおあいうえおあいうえ...</td>
                    <td>1,980円</td>
                    </tr>
                    <tr>
                    <td scope="row">2021-01-04 17:33:09</td>
                    <td>掲載中</td>
                    <td class="clr-yel">あいうえおあいうえおあいうえおあいうえ...</td>
                    <td>1,980円</td>
                    </tr>
                    <tr>
                    <td scope="row">2021-01-04 17:33:09</td>
                    <td>掲載中</td>
                    <td class="clr-yel">あいうえおあいうえおあいうえおあいうえ...</td>
                    <td>1,980円</td>
                    </tr>
                    <tr>
                    <td scope="row">2021-01-04 17:33:09</td>
                    <td>掲載中</td>
                    <td class="clr-yel">あいうえおあいうえおあいうえおあいうえ...</td>
                    <td>1,980円</td>
                    </tr>
                    <tr>
                    <td scope="row">2021-01-04 17:33:09</td>
                    <td>掲載中</td>
                    <td class="clr-yel">あいうえおあいうえおあいうえおあいうえ...</td>
                    <td>1,980円</td>
                    </tr>
                    <tr>
                    <td scope="row">2021-01-04 17:33:09</td>
                    <td>掲載中</td>
                    <td class="clr-yel">あいうえおあいうえおあいうえおあいうえ...</td>
                    <td>1,980円</td>
                    </tr>
                    <tr>
                    <td scope="row">2021-01-04 17:33:09</td>
                    <td>掲載中</td>
                    <td class="clr-yel">あいうえおあいうえおあいうえおあいうえ...</td>
                    <td>1,980円</td>
                    </tr>
                    <tr>
                    <td scope="row">2021-01-04 17:33:09</td>
                    <td>掲載中</td>
                    <td class="clr-yel">あいうえおあいうえおあいうえおあいうえ...</td>
                    <td>1,980円</td>
                    </tr>
                    <tr>
                    <td scope="row">2021-01-04 17:33:09</td>
                    <td>掲載中</td>
                    <td class="clr-yel">あいうえおあいうえおあいうえおあいうえ...</td>
                    <td>1,980円</td>
                    </tr>
                </tbody>
            </table>
        </div>
        <div class="content-group-pagination">
            <a href="#" id="pagination__preve" class="pagination__preve"><i class="bi bi-chevron-left"></i></a>
            <a href="#" class="pagination__num"><span>1</span></a>
            <a href="#" class="pagination__num"><span>2</span></a>
            <a href="#" class="pagination__num"><span>3</span></a>
            <a href="#" class="pagination__num"><span>4</span></a>
            <a href="#" class="pagination__num"><span>5</span></a>
            <a href="#" class="pagination__num"><span>6</span></a>
            <a href="#" class="pagination__num"><span>7</span></a>
            <!-- <a href="#" class="pagination__num"><span>8</span></a>
            <a href="#" class="pagination__num"><span>9</span></a>
            <a href="#" class="pagination__num"><span>10</span></a>
            <a href="#" class="pagination__num"><span>11</span></a>
            <a href="#" class="pagination__num"><span>12</span></a> -->
            <a href="#" class="pagination__num"><span>...</span></a>
            <a href="#" id="pagination__next" class="pagination__preve"><i class="bi bi-chevron-right"></i></a>
        </div>
    </div>
</div>
<?php include('../user_footer.php'); ?>