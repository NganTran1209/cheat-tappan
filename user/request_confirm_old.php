<?php include_once(__DIR__ . '/../common/util.php'); ?>
<?php include_once(__DIR__ . '/../common/login_check.php'); ?>
<?php
$request = $_SESSION['request'];
if (isset($_POST['action']) && $_POST['action'] == 'regist') {
    $request->regist();
    unset($_SESSION['request']);
    setMessage('購入希望情報を登録しました。内容の確認、承認後に掲載されます。<br>※掲載後メール通知されます。<br>※掲載不可の場合、メール通知されません。');
    //header('Location: '.getContextRoot().'/user/request.php');
    header('Location: ' . getContextRoot() . '/user/mypage.php');
    exit();
}

?>
<?php include(__DIR__ . '/../user_header.php'); ?>

    <div class="com-container bg-yellow">
        <?php include('usersidebar.php'); ?>

        <div class="com-content">
            <div class="bg-inner">
                <h1>購入希望するー確認画面</h1>
                <p class="small">購入希望商品の情報をご確認ください。</p>
                <div class="border-bottom py-4">
                    <h3 class="fontBold my-2">商品名</h3>
                    <?= $request->title ?>
                </div>
                <div class="border-bottom py-4">
                    <h3 class="fontBold my-2">説明文</h3>
                    <?php echo nl2br(htmlentities($request->text)); ?>
                </div>
                <div class="border-bottom py-4">
                    <h3 class="fontBold my-2">予算</h3>
                    <?= $request->price ?>円
                </div>

                <div class="row my-5">
                    <div class="col-md-8">
                        <form method="post">
                            <input type="hidden" name="action" value="regist"/>
                            <input class="btn btn-block btn-info" type="submit" value="登録する"/>
                        </form>
                    </div>
                    <div class="col-md-4">
                        <form method="post" action="request.php">
                            <input type="hidden" name="action" value="modify"/>
                            <input class="btn btn-block btn-outline-dark" type="submit" value="修正する"/>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<?php include('../footer.php'); ?>
