<?php $title_page = '出品者ホーム'; ?>
<?php include(__DIR__ . '/../user_header.php'); ?>
<?php
    $uri = $_SERVER["REQUEST_URI"];
    $owner_id = empty($_GET['owner_id']) ? '' : $_GET['owner_id'];
    if (!empty($owner_id)) {
        $item = new Item();
        $items = $item->selectSell($owner_id);

        $soldout_count = 0;
        foreach ($items as $item2) {
            if ($item2->enabled == 9) {
                continue;
            }

            if (!empty($item2->buyer_id)) {
                $soldout_count++;
            }
        }
        $evaluate = new Evaluate();
        $evaluate->selectSummary($owner_id);

        $sns = new Sns();
        $sns->select($owner_id);

        $item = new Item();
        $items = $item->selectSell($owner_id);

        $order = new Order();
        $evaluates = $order->selectFromUserAll($owner_id);

        $owner = new User();
        $owner->select($owner_id);
        $category_title = $owner->getViewId();
        $page = 1;
        $page = empty($_POST['page']) ? '1' : $_POST['page'];
        if (!empty($_GET['page'])) {
            $page = $_GET['page'];
        }
    }
    
    function get_url_status($url) 
    {
        $curl = curl_init($url);
        $ch = curl_init();
        curl_setopt($ch,CURLOPT_BINARYTRANSFER,1);
        curl_setopt($ch, CURLOPT_VERBOSE, 0);
        curl_setopt($ch, CURLINFO_HEADER_OUT, true);
        curl_setopt($ch, CURLOPT_HEADER, true);
        curl_setopt($ch, CURLOPT_URL, $url);
        echo "<span style='display:none'>";
            $result = curl_exec($curl);
        echo"</span>";
        if ($result !== false)
        {
            $statusCode = curl_getinfo($curl, CURLINFO_HTTP_CODE);  
            if ($statusCode == 404)
            {
                return false;
            }
            else
            {
                return true;
            }
        }
        else
        {
            return false;
        }
    }
?>
<form id="fm-param" method="post">
    <input type="hidden" id="id-page" name="page" value="<?= $page ?>" />
</form>
<div class="com-header-top">
        <?php
            $entity = new User();
            $sns = new Sns();
            if (isLogin()) {
                $entity->select($owner->id);
                $sns->select($owner->id);
            }

                $icon_url = $entity->get_icon_url();
                $header_image_url = $entity->get_header_image_url();
        ?>
        <div class="com-header-top__img">
            <?php if (!isset($_GET["preview"])): ?>
                <?php if(!empty($header_image_url)): ?>
                    <div style="background: url('<?= $header_image_url ?>'); height: 200px;" alt=""></div>
                <?php else: ?>
                    <img src="<?php echo HOME_URL; ?>/common/assets/img/common/seller-home.png" alt="">
                <?php endif ?>
            <?php else: ?>
                <img src="" height="250px" alt="">
            <?php endif ?>
        </div>
</div>
<div class="customer-container">
        <div class="customer-contact-form">
            <div style="display: flex;" class="box-user-info">
                <div class="seller-home__img">
                    <?php if (!isset($_GET["preview"])): ?>
                        <?php if(!empty($icon_url)):?>
                            <img style="border-radius:50%" src="<?= $icon_url ?>" alt="">
                        <?php else: ?>
                            <img src="<?php echo HOME_URL; ?>/common/assets/img/common/seller-img.png" alt="">
                        <?php endif; ?>
                    <?php else: ?>
                        <img style="border-radius:50%" src="" alt="">
                    <?php endif ?>
                </div>
                <div class="seller-home-profile">
                    <div class="seller-home-profile__title">
                        <h3><?= $owner->name ?></h3>
                    </div>
                    <div class="seller-home-profile__txt">
                        <p>
                            ID : <?= $owner->hash_id ?> / 
                            <?php if($owner->identification) : ?> 
                                <span>本人確認済</span> 
                            <?php else: ?> 
                                <span>未確認の身元</span>
                            <?php endif; ?> / 
                            <?php if(!empty($owner->company)): ?> <span><?=$owner->company ?></span><?php else: ?><span>個人</span> <?php endif; ?>/ <?= $owner->addr ?><br>
                            販売数 <?= $soldout_count ?> / 評価 <?= $evaluate->point ?>
                        </p>
                    </div>
                    <div class="social-icon">
                        <?php if (!empty($sns->twitter_url)):?>
                            <a href="<?= $sns->twitter_url ?>"><i class="bi bi-twitter"></i></a>
                        <?php endif;?>
                        <?php if (!empty($sns->facebook_url)):?>
                            <a href="<?= $sns->facebook_url ?>"><i class="bi bi-facebook"></i></a>
                        <?php endif;?>
                        <?php if (!empty($sns->instagram_url)):?>
                            <a href="<?= $sns->instagram_url ?>"><i class="bi bi-instagram"></i></a>
                        <?php endif;?>
                        <?php if (!empty($sns->youtube_url)):?>
                            <a href="<?= $sns->youtube_url ?>"><i class="bi bi-youtube"></i></a>
                        <?php endif;?>
                    </div>
                    <div class="seller-home-profile__txt">
                        <p>
                        <?php echo nl2br($owner->self_pr); ?>
                        </p>
                    </div>
                </div>
            </div>
            <div class="seller-home">
                <div class="ladies-item-title">
                    <div class="ladies-item-title__txt">
                        <h3><span>レディース新着アイテム</span></h3>
                    </div>
                    <div class="ladies-item-title__txt">
                       
                    </div>
                </div>
                <div class="row" style="margin-top:15px">
                    <?php
                    $count = 0;
                    $index = 0;
                    foreach ($items as $item): ?>
                        
                        <?php 
                            if ($item->enabled == 9 || $item->enabled == 2 || $item->enabled == 0) continue; 
                            $count++;
                            if (ceil(++$index / MAX_PAGE_COUNT) != $page) {
                                continue;
                            }
                        ?>
                            <a href="<?php echo HOME_URL; ?>/itemdetail.php?id=<?= $item->id ?>" class="ladies-group-column col-md-3 float-left" style="margin-top:2%">
                                <div class="ladies-group-column__img">
                                    <?php if(@file_get_contents($item->getThumnailPath())): ?>
                                        <img src="<?= $item->getThumbnailUrl() ?>" alt="">
                                    <?php else: ?>
                                        <img src="<?php echo HOME_URL; ?>/common/assets/img/no-image.png" alt="" >
                                    <?php endif; ?>
                                    <div class="ladies-group-column__img-bottom"><?= $item->title ?></div>
                                </div>
                                <div class="ladies-group-column__icon">
                                </div>
                                <div class="ladies-group-column__price" style="right: 15px;">
                                    <p><span>¥<?= number_format($item->price) ?></span></p>
                                </div>
                            </a>
                    <?php endforeach; ?> 
                </div>
                <?php
                $prev_page = $page - 1;
                $max_page = ceil($count / MAX_PAGE_COUNT);
                $next_page = min($max_page, $page + 1);
                $view_start_page = max(1, $page - ceil(VIEW_PAGE_SELECT_COUNT / 2));
                $view_last_page = min($view_start_page + VIEW_PAGE_SELECT_COUNT - 1, $max_page);
                if ($view_last_page - $view_start_page < VIEW_PAGE_SELECT_COUNT) {
                    $view_start_page = max(1, $view_last_page - VIEW_PAGE_SELECT_COUNT + 1);
                }
                ?>
                <?php if($max_page >1): ?>
                    <div class="content-group-pagination">
                <?php else: ?>
                    <div class="content-group-pagination content-one-page">
                <?php endif; ?>
                    
                    <?php if($max_page > 1 ) { ?>
                        <?php if ($prev_page > 0): ?>
                            <a id="pagination__preve" class="pagination__preve" href=""
                                onclick="submitForm('fm-param', 'page', '<?= $prev_page ?>'); return false;"><i class="bi bi-chevron-left"></i></a>
                        <?php else: ?>
                            <a id="pagination__preve" class="pagination__preve" href="" onclick="return false;"><i class="bi bi-chevron-left"></i></a>
                        <?php endif; ?>
                    <?php } ?>
                    <?php for ($i = $view_start_page; $i <= $view_last_page; $i++): ?>
                        <?php if ($page != $i): ?>
                            <a class="pagination__num" href=""
                                onclick="submitForm('fm-param', 'page', '<?= $i ?>'); return false;"><span><?= $i ?></span></a>
                        <?php else: ?>
                            <a class="pagination__num pag_active" href="" onclick="return false;"><span><?= $i ?></span></a>
                        <?php endif; ?>
                    <?php endfor; ?>

                    <?php if($max_page > 1 ) { ?>
                        <?php if ($page < $max_page): ?>
                            <a id="pagination__next" class="pagination__preve" href="" onclick="submitForm('fm-param', 'page', '<?= $next_page ?>'); return false;">
                                <i class="bi bi-chevron-right"></i>
                            </a>
                        <?php else: ?>
                            <a id="pagination__next" class="pagination__preve" href="" onclick="return false;"><i class="bi bi-chevron-right"></i></a>
                        <?php endif; ?>
                    <?php } ?>
                </div>
                <div class="seller-bottom">
                    <a href="<?php echo HOME_URL; ?>/product/list_item.php?owner_id=<?= $owner->id ?>">出品者の他の商品を見る</a>
                </div>
                <div class="seller-evaluation__title">
                    <p><span>感想・評価</span></p>
                </div>
                <div class="seller-evaluation">
                    <?php foreach ($evaluates as $order): ?>
                        <?php if ($order->owner_id != $owner->id):?>
                            <?php if (!empty($order->buyer_evaluate)): ?>
                                <div class="evaluation-item-show-result">
                                    <div class="evaluation-result-person">
                                        <div class="evaluation-result-person__img">
                                            <?php
                                                $entity = new User();
                                                $sns = new Sns();
                                                if (isLogin()) {
                                                    $entity->select($order->user->id);
                                                    $sns->select($order->user->id);
                                                }

                                                $icon_url = $entity->get_icon_url();
                                                $header_image_url = $entity->get_header_image_url();
                                            ?>
                                            <?php if(get_url_status($header_image_url) ):?>
                                                <img src="<?= $header_image_url ?>" alt="">
                                            <?php else: ?>
                                                <img src="<?php echo HOME_URL; ?>/common/assets/img/common/icon/profile.png" alt="">
                                            <?php endif; ?>
                                        </div>
                                        <div class="evaluation-result-person__name">
                                            <p><?= $order->owner->getViewNicknameOrId() ?></p>
                                        </div>
                                    </div>
                                    <div class="evaluation-result-content">
                                        <div class="evaluation-result-content-head">
                                            <div class="evaluation-result-content-head__level">
                                                <p>評価 : <i class="bi bi-emoji-heart-eyes icon__good__eval"></i></p>
                                            </div>
                                            <div class="evaluation-result-content-head__sex">
                                            </div>
                                        </div>
                                        <div class="evaluation-result-content__txt">
                                            <p><?= empty($order->buyer_comment) ? 'コメントはありません' : nl2br($order->buyer_comment) ?></p>
                                            <p><i class="bi bi-alarm"></i><?= $order->regist_date ?></p>
                                        </div>
                                    </div>
                                </div>
                            <?php endif; ?>
                            <?php else: ?>
                                <?php if (!empty($order->seller_evaluate)): ?>
                                    <div class="evaluation-item-show-result">
                                        <div class="evaluation-result-person">
                                            <div class="evaluation-result-person__img">
                                               
                                                <?php
                                                    $entity = new User();
                                                    $sns = new Sns();
                                                    if (isLogin()) {
                                                        $entity->select($order->user->id);
                                                        $sns->select($order->user->id);
                                                    }

                                                    $icon_url = $entity->get_icon_url();
                                                    $header_image_url = $entity->get_header_image_url();
                                                ?>
                                                <?php if(get_url_status($header_image_url) ):?>
                                                    <img src="<?= $header_image_url ?>" alt="">
                                                <?php else: ?>
                                                    <img src="<?php echo HOME_URL; ?>/common/assets/img/common/icon/profile.png" alt="">
                                                <?php endif; ?>
                                            </div>
                                            <div class="evaluation-result-person__name">
                                                <p><?= $order->user->getViewNicknameOrId() ?></p>
                                            </div>
                                        </div>
                                        <div class="evaluation-result-content">
                                            <div class="evaluation-result-content-head">
                                                <div class="evaluation-result-content-head__level">
                                                    <p>評価 : 
                                                    <?php if (Evaluate::toEvaluateImageTag($order->seller_evaluate) == Evaluate::toEvaluateImageTag(1)): ?>
                                                        <i class="bi bi-emoji-heart-eyes icon__good__eval"></i></p>
                                                    <?php elseif (Evaluate::toEvaluateImageTag($order->seller_evaluate) == Evaluate::toEvaluateImageTag(2)): ?>
                                                        <i class="bi bi-emoji-neutral icon__middle__eval"></i> 
                                                    <?php elseif (Evaluate::toEvaluateImageTag($order->seller_evaluate) == Evaluate::toEvaluateImageTag(3)): ?>
                                                        <i class="bi bi-emoji-angry icon__bad__eval"></i>
                                                    <?php endif; ?>
                                                </div>
                                                <div class="evaluation-result-content-head__sex">
                                                </div>
                                            </div>
                                            <div class="evaluation-result-content__txt">
                                                <p><?= empty($order->seller_comment) ? 'コメントはありません' : nl2br($order->seller_comment) ?></p>
                                                <p><i class="bi bi-alarm"></i><?= $order->regist_date ?></p>
                                            </div>
                                        </div>
                                    </div>
                            <?php endif; ?>
                        <?php endif; ?>
                    <?php endforeach; ?>
                </div>
            </div>
        </div>
    </div>
<?php include('../footer.php'); ?>
<script>
    window.onload = (event) => {
        const queryString = window.location.search;
        const urlParams = new URLSearchParams(queryString);
        const param = urlParams.get('preview');
        if (param == 'true'){
            headerUrl = localStorage.getItem("headerPreview")
            iconUrl = localStorage.getItem("iconPreview")
            $(".com-header-top__img img").attr("src", headerUrl);
            $(".seller-home__img img").attr("src", iconUrl);
        }
    };  
</script>