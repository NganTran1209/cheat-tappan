<?php 
$title_page = '出品中のアイテム';
$description_page = '欲しい商品を画像で確認・届いた商品を画像で照合！気持ち良い取引が叶うオンラインマーケット。';
 ?>
<?php include_once(__DIR__ . '/../common/util.php'); ?>
<?php include_once(__DIR__ . '/../common/login_check.php'); ?>
<?php
  $item = new Item();
  $items = $item->selectSell(getUserId());
  $negotiation = false;
  if (isset($_GET['mode']) && $_GET['mode'] == 'negotiation') {
      $negotiation = true;
  }
  $page = 1;
  $page = empty($_POST['page']) ? '1' : $_POST['page'];
  if (!empty($_GET['page'])) {
      $page = $_GET['page'];
  }
  $title_page = '出品中のアイテム';
?>
<?php include(__DIR__ . '/../user_header.php'); ?>
    <div class="com-header-top">
        <div class="com-header-top__img wow animate__animated animate__fadeInUp">
            <img src="<?php echo HOME_URL; ?>/common/assets/img/mypage/top-bg.png" alt="">
        </div>
        <div class="com-header-top__path wow animate__animated animate__fadeInUp">
            <p><span><a href="<?php echo HOME_URL; ?>/" class="clr-yel">トップページ</a></span><span> > </span><span><a href="<?php echo HOME_URL; ?>/user/mypage.php" class="clr-yel">マイページ</a></span><span> > </span><span>出品中のアイテム</span></p>        </div>
   <!--     <div class="com-header-top__txt">
            <p class="clr-white">マイページ</p>
        </div>-->
    </div>
    <div class="com-container bg-yellow">
        <?php include('usersidebar.php'); ?>
        <div class="com-content ">
            <div class="content-title">
                <h3><span>出品中のアイテム</span></h3>
            </div>
            <div class="qnego-sm-title">
                <p>※表示期間は最終連絡日から90日間です。</p>
            </div>
            <div class="table-responsive">
                <table class="table table-bordered">
                    <thead>
                      <tr>
                        <th scope="col">掲載日</th>
                        <th scope="col">タイトル</th>
                        <th scope="col">価格</th>
                        <th scope="col">編集</th>
                      </tr>
                    </thead>
                    <tbody>
                      <?php
                      $count = 0;
                      $index = 0;
                      foreach ($items as $item): ?>
                        <?php if ($item->enabled == 9 || $item->enabled == 2 || $item->enabled == 0) continue; ?>
                        <?php if (empty($item->buyer_id)) : ?>
                          <?php
                          $count++;
                          if (ceil(++$index / MAX_PAGE_COUNT) != $page) {
                              continue;
                          }
                          ?>
                          <tr>
                            <td scope="row"><?=$item->regist_date?></td>
                            <td class="clr-yel"> <a href="<?php echo HOME_URL; ?>/itemdetail.php?id=<?= $item->id ?>"><?=$item->title?></td>
                            <td><?=number_format($item->price)?>円</td>
                            <td class="text-center"><a href="modify_item.php?id=<?= $item->id ?>">編集</a></td>
                          </tr>
                          <?php endif; ?>
                        <?php endforeach; ?>  
                    </tbody>
                </table>

            </div>
            <?php
                $prev_page = $page - 1;
                
                $max_page = ceil($count / MAX_PAGE_COUNT);
                $next_page = min($max_page, $page + 1);
                $view_start_page = max(1, $page - ceil(VIEW_PAGE_SELECT_COUNT / 2));
                $view_last_page = min($view_start_page + VIEW_PAGE_SELECT_COUNT - 1, $max_page);
                if ($view_last_page - $view_start_page < VIEW_PAGE_SELECT_COUNT) {
                    $view_start_page = max(1, $view_last_page - VIEW_PAGE_SELECT_COUNT + 1);
                }
                ?>
            <?php if($max_page >1): ?>
                <div class="content-group-pagination">
            <?php else: ?>
                <div class="content-group-pagination content-one-page">
            <?php endif; ?>
            <?php if($max_page > 1):  ?>   
                <?php if ($prev_page > 0): ?>
                    <a href="" onclick="submitForm('fm-param', 'page', '<?= $prev_page ?>'); return false;" id="pagination__preve" class="pagination__preve"><i class="bi bi-chevron-left"></i></a>
                <?php else: ?>
                    <a href="" onclick="return false;" id="pagination__next" class="pagination__preve"><i class="bi bi-chevron-left"></i></a>
                <?php endif; ?>
            <?php endif; ?>
                <?php for ($i = $view_start_page; $i <= $view_last_page; $i++): ?>
                    <?php if ($page != $i): ?>
                        <a href="" class="pagination__num" onclick="submitForm('fm-param', 'page', '<?= $i ?>'); return false;"><span><?= $i ?></span></a>
                    <?php else: ?>
                        <a href="" class="pagination__num pag_active" onclick="return false;"><span><?= $i ?></span></a>
                    <?php endif; ?>
                <?php endfor; ?>
            <?php if($max_page > 1):  ?>
                <?php if ($page < $max_page): ?>
                    <a href="" id="pagination__next" class="pagination__preve" onclick="submitForm('fm-param', 'page', '<?= $next_page ?>'); return false;"><i class="bi bi-chevron-right"></i></a>
                <?php else: ?>
                    <a href="" id="pagination__next" class="pagination__preve" onclick="return false;"><i class="bi bi-chevron-right"></i></a>
                <?php endif; ?>
            <?php endif; ?>
            </div>
        </div>
    </div>
<?php include('../footer.php'); ?>
