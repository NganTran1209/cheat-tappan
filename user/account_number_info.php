<?php 
$title_page = '口座情報変更';
$description_page = '欲しい商品を画像で確認・届いた商品を画像で照合！気持ち良い取引が叶うオンラインマーケット。';
 ?>
<?php include_once(__DIR__ . '/../common/util.php'); ?>
<?php include_once(__DIR__ . '/../common/login_check.php'); ?>
<?php include_once(__DIR__ . '/../common/stripe_api.php'); ?>
<?php

$entity = new User();
$entity->select(getUserId());
if (isset($_POST['regist'])) {
    $entity->financial_institution_name = $_POST['financial_institution_name'];
    $entity->branch_name = $_POST['branch_name'];
    $entity->deposit_type = $_POST['deposit_type'];
    $entity->account_number = $_POST['account_number'];
    $entity->account_holder = $_POST['account_holder'];
    $entity->updateAccountInfo();
    setMessage('口座情報を更新しました。');
} elseif (isset($_POST['remove-card'])) {
    StripeApi::remove_customer();
    setMessage('カード情報を削除しました。');
}

$card_info = new CardInfo();
$card_info->select(getHashKey());

$title_page = '口座情報変更';
?>

<?php include(__DIR__ . '/../user_header.php'); ?>
<div class="com-header-top">
        <div class="com-header-top__img wow animate__animated animate__fadeInUp">
            <img src="<?php echo HOME_URL; ?>/common/assets/img/mypage/top-bg.png" alt="">
        </div>
        <div class="com-header-top__path wow animate__animated animate__fadeInUp">
            <p><span><a href="<?php echo HOME_URL; ?>/" class="clr-yel">トップページ</a></span><span> > </span><span><a href="<?php echo HOME_URL; ?>/user/mypage.php" class="clr-yel">マイページ</a></span><span> > </span><span>口座情報変更</span></p>
        </div>

    </div>
<div class="com-container bg-yellow">
    <?php include('usersidebar.php'); ?>
        <div class="com-content">
        <div class="content-title">
                <h3><span>口座情報変更</span></h3>
            </div>
                <div class="bg-inner">
                    <form class="c-form01" id="form" method="post" enctype="multipart/form-data" onsubmit="return confirm('変更します'); ">
                        <h1>口座情報変更</h1>
                        <table class="table table-bordered mb-5 table-bordered-add">
                            <tr>
                                <th>金融機関名</th>
                                <td>
                                    <input class="form-control middle-value" type="text" name="financial_institution_name"
                                           placeholder="" value="<?= $entity->financial_institution_name ?>">
                                </td>
                            </tr>
                            <tr>
                                <th>支店名</th>
                                <td>
                                    <input class="form-control middle-value" type="text" name="branch_name" placeholder=""
                                           value="<?= $entity->branch_name ?>">
                                </td>
                            </tr>
                            <tr>
                                <th>預金種別</th>
                                <td>
                                    <div class="form-check form-check-account-add">
                                        <input class="form-check-input" type="radio" name="deposit_type" id="deposit_type_1"
                                               value="1"<?= $entity->deposit_type == 1 ? ' checked="checked"' : '' ?>/>
                                        <label class="form-check-label form-check-label-add" for="deposit_type_1">普通預金</label>
                                    </div>
                                    <div class="form-check form-check-account-add">
                                        <input class="form-check-input" type="radio" name="deposit_type" id="deposit_type_2"
                                               value="2"<?= $entity->deposit_type == 2 ? ' checked="checked"' : '' ?>/>
                                        <label class="form-check-label form-check-label-add" for="deposit_type_2">当座預金</label>
                                    </div>
                                </td>
                            </tr>
                            <tr>
                                <th>口座番号</th>
                                <td>
                                    <input class="form-control middle-value" type="text" name="account_number"
                                           placeholder="" value="<?= $entity->account_number ?>">
                                </td>
                            </tr>
                            <tr>
                                <th>口座名義</th>
                                <td>
                                    <input class="form-control middle-value" type="text" name="account_holder"
                                           placeholder="" value="<?= $entity->account_holder ?>">
                                </td>
                            </tr>
                        </table>

                        <div class="mb-5 w-75 mx-auto">
                            <input class="btn btn-block btn-info product-confirm-add-2" type="submit" name="regist" value="変更を保存する"/>
                        </div>

                        <h2 class="bg-light p-2 fontBold">※ カード情報について</h2>
                        <?php if ($card_info->enabled_customer): ?>
                            <p>
                                カード情報が保存されておりますが、<br>
                                <span class="text-success">このサイトに番号などは一切保存されておりません</span> のでご安心ください。
                            </p>
                            <p>
                                カード番号の変更や削除は下記のボタンから削除頂けます。
                            </p>
                            <div class="my-4 w-75 mx-auto">
                                <input class="btn btn-block btn-info product-confirm-add-2" type="submit" name="remove-card"
                                       value="クレジットカード情報を削除する"/>
                            </div>
                        <?php else: ?>
                            <p class="c-small">
                                カード情報は保存されておりません。<br>
                                また購入時に登録しても
                                <span class="text-success">このサイトに番号などは一切保存されておりません</span> のでご安心ください。
                            </p>
                        <?php endif; ?>
                    </form>
                </div>
            </div>
        </div>
    </div>
<?php include('../footer.php'); ?>