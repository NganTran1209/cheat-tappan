<?php include_once(__DIR__ . '/../common/util.php'); ?>
<?php
if (!isLogin()) {
    setMessage('ログインをしてください。');
    header('Location: ' . getContextRoot() . '/');
    exit;
}

$isOffer = isset($_SESSION['targetRequest']) && !empty($_SESSION['targetRequest']);

$entity = $_SESSION['item'];
if (isset($_POST['action']) && $_POST['action'] == 'regist') {
    if ($isOffer) {
        $req = $_SESSION['targetRequest'];
        $entity->target_user_id = $req->user_id;
    }
    $entity->modify();
    header('Location: ' . getContextRoot() . '/user/modify_item.php?id=' . $entity->id);
    exit();
}

$entity->repairName();

?>
<?php include('../header.php'); ?>
<div class="container">
    <div class="row">
        <div class="col-md-3 sideContents">
            <?php include('usersidebar.php'); ?>
        </div>
        <div class="col-md-9 mainContents">
            <div class="bg-inner">
                <h1>商品情報を更新する</h1>

                <div class="border-bottom py-4">
                    <h2 class="bg-light p-2 fontBold my-3">商品の情報</h2>
                    <h3 class="fontBold my-2">商品画像</h3>
                    <div class="row">
                        <div class="col-md-3 mb-2">
                            <img class="img-fluid" src="<?= getImagePath($entity->id, 1) ?>" alt="">
                        </div>
                        <?php $url = getImagePath($entity->id, 2);
                        if (strstr($url, "noimage") == false): ?>
                            <div class="col-md-3 mb-2">
                                <img class="img-fluid" src="<?= getImagePath($entity->id, 2) ?>" alt=""/>
                            </div>
                        <?php endif; ?>
                        <?php $url = getImagePath($entity->id, 3);
                        if (strstr($url, "noimage") == false): ?>
                            <div class="col-md-3 mb-2">
                                <img class="img-fluid" src="<?= getImagePath($entity->id, 3) ?>" alt=""/>
                            </div>
                        <?php endif; ?>
                        <?php $url = getImagePath($entity->id, 4);
                        if (strstr($url, "noimage") == false): ?>
                            <div class="col-md-3 mb-2">
                                <img class="img-fluid" src="<?= getImagePath($entity->id, 4) ?>" alt=""/>
                            </div>
                        <?php endif; ?>
                    </div>
                </div>

                <div class="border-bottom py-4">
                    <h2 class="bg-light p-2 fontBold">商品の詳細</h2>
                    <h3 class="fontBold">タイトル</h3>
                    <div><?= $entity->title ?></div>
                    <h3 class="fontBold my-2">商品の説明文</h3>
                    <?php echo nl2br(htmlentities($entity->remarks)); ?>
                    <h3 class="fontBold my-2">カテゴリー</h3>
                    <?= $entity->category_name ?>
                    <h3 class="fontBold my-2">商品の状態</h3>
                    <?= $entity->item_state_name ?>
                </div>

                <div class="border-bottom py-4">
                    <h2 class="bg-light p-2 fontBold my-3">商品の配送について</h2>
                    <h3 class="fontBold my-2">地域</h3>
                    <?= $entity->pref_name ?>
                    <h3 class="fontBold my-2">配送料の負担</h3>
                    <?= $entity->carriage_plan_name ?>
                    <h3 class="fontBold my-2">配送の目安</h3>
                    <?= $entity->carriage_type_name ?>
                    <h3 class="fontBold my-2">配送方法</h3>
                    <?= $entity->delivery_type_name ?>
                </div>

                <div class="border-bottom py-4">
                    <h2 class="bg-light p-2 fontBold my-3">価格(300円～9,999,999円)</h2>
                    <h3 class="fontBold my-2">商品価格</h3>
                    <?= number_format($entity->price) ?>円

                </div>

                <div class="row py-4">
                    <div class="col-md-6 mb-3">
                        <form method="post" onsubmit="return window.confirm('更新する。');">
                            <input type="hidden" name="action" value="regist"/>
                            <input class="btn btn-success btn-block btn-sm" type="submit" value="更新する"/>
                        </form>
                    </div>
                    <div class="col-md-6 mb-3">
                        <form method="post" action="modify_item.php">
                            <input type="hidden" name="action" value="modify"/>
                            <input type="hidden" name="id" value="<?= $entity->id ?>"/>
                            <input class="btn btn-light btn-block btn-sm" type="submit" value="修正する"/>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<?php include('../footer.php'); ?>
