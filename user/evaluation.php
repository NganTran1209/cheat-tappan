
<?php include_once(__DIR__ . '/../common/util.php'); ?>
<?php include_once(__DIR__ . '/../common/login_check.php'); ?>
<?php
$order = new Order();
$order->select($_GET['id']);
if ($order->owner_id == getUserId()) {
    if (BlockAccess::isBlock($order->user_id)) {
        setMessage('このユーザーは評価できません。', true);
        header('Location: ' . getContextRoot() . '/');
        exit;
    }
} else {
    if (BlockAccess::isBlock($order->owner_id)) {
        setMessage('このユーザーは評価できません。', true);
        header('Location: ' . getContextRoot() . '/');
        exit;
    }
}

if (isset($_POST['action'])) {
    if ($_POST['action'] == 'evaluate') {
        $order = new Order();
        $order->select($_POST['id']);
        if (isset($_POST['buyer_evaluate'])) {
            $order->buyer_evaluate = $_POST['buyer_evaluate'];
            $order->buyer_comment = $_POST['buyer_comment'];
            $order->registEvaluateByBuyer();
        } elseif (isset($_POST['seller_evaluate'])) {
            $order->seller_evaluate = $_POST['seller_evaluate'];
            $order->seller_comment = $_POST['seller_comment'];
            $order->registEvaluateBySeller();
        } else {
            setMessage('不正なアクションです。');
            header('Location: ' . getContextRoot() . '/user/mypage.php');
            exit;
        }

        setMessage('評価を登録しました。');
        header('Location: ' . getContextRoot() . '/user/exhibited_list.php');
        exit;
    }
    // $order->select($_GET['id']);
}

$item = new Item();
$item->select($order->item_id);
$owner = new User();
$owner->select($order->owner_id);
$evaluate = new Evaluate();
$evaluate->selectSummary($item->owner_id);
$evaluateBuyer = new Evaluate();
$evaluateBuyer->selectSummary($order->user_id);

$user = new User();
$user->select($order->user_id);

$countFavorites = $item->selectCountFavorites();

$title_page = '評価する';
?>
<?php include(__DIR__ . '/../user_header.php'); ?>
<div class="com-header-top">
    <div class="com-header-top__img wow animate__animated animate__fadeInUp">
        <img src="<?php echo HOME_URL; ?>/common/assets/img/mypage/top-bg.png" alt="">
    </div>
    <div class="com-header-top__path wow animate__animated animate__fadeInUp">
        <p><span><a href="<?php echo HOME_URL; ?>/" class="clr-yel">トップページ</a></span><span> > </span><span><a
                    href="<?php echo HOME_URL; ?>/user/mypage.php" class="clr-yel">マイページ</a></span><span> >
            </span><span>評価する</span></p>
    </div>
<!--     <div class="com-header-top__txt">
            <p class="clr-white">マイページ</p>
        </div>-->

</div>
<div class="com-container bg-yellow">
    <?php include('usersidebar.php'); ?>
    <div class="com-content">
        <div class="bg-inner">
            <div class="content-title">
                <h3><span>評価する</span></h3>
            </div>
            <div class="qnego-sm-title">
                <p>※表示期間は最終連絡日から90日間です。</p>
            </div>
            <h1><span><?= $item->title ?></span></h1>
            <div class="ladies-product-list-content">
                <?php include_once(__DIR__ . '/../common/parts/item_image_area.php'); ?>
                <?php include_once(__DIR__ . '/../common/parts/item_info_area.php'); ?>
            </div>
            <div class="mb-4 ladies-product-price">
                <div class="text-center">
                      <div class="ladies-product-price__txt">
                        <p><span>¥<?= number_format($item->price) ?> 円</span>（税込/<?= $item->carriage_plan_name ?>）</p>
                    </div>
                </div>
            </div>
            <div class="evaluate">
                <h2 class="bg-light p-2 fontBold">
                    <?php /*?><?= ($order->user_id == getUserId()) ? '出品者　'.$owner->name : '購入者　'.$order->user_name ?><?php */ ?>
                    <?= ($order->user_id == getUserId()) ? '出品者　' . $owner->hash_id .' '.$owner->getViewNicknameOrId() : '購入者　' . $user->hash_id . ' ' . $user->getViewNicknameOrId() ?>
                    を評価する</h2>
                <form method="post" onsubmit="return window.confirm('評価します。');">
                    <?php if ($order->user_id != getUserId()): ?>
                    <div class="mb-2">
                        <?php createRadio('selectEvaluate', 'buyer_evaluate', $order->buyer_evaluate); ?>
                    </div>
                    <textarea class="form-control mb-3" rows="5" name="buyer_comment"
                        placeholder="コメントを入力してください"><?= $order->buyer_comment ?></textarea>
                    <div>
                        <input type="hidden" name="id" value="<?= $order->id ?>" />
                        <input type="hidden" name="action" value="evaluate" />
                        <input class="btn btn-block btn-info" type="submit" name="regist" value="評価を登録する" />
                    </div>

                    <?php else: ?>
                    <div class="mb-2">
                        <?php createRadio('selectEvaluate', 'seller_evaluate', $order->seller_evaluate); ?>
                    </div>
                    <textarea class="form-control mb-3" rows="5" name="seller_comment"
                        placeholder="コメントを入力してください"><?= $order->seller_comment ?></textarea>
                    <div>
                        <input type="hidden" name="id" value="<?= $order->id ?>" />
                        <input type="hidden" name="action" value="evaluate" />
                        <input class="btn btn-block btn-info btn-custom" type="submit" name="regist" value="評価を登録する" />
                    </div>
                    <?php endif; ?>
                </form>
            </div>
            <div class="itemContentText">
                <?php echo nl2br(htmlentities($item->remarks)); ?>
            </div>
        </div>
    </div>
</div>
</div>
<?php include('../footer.php'); ?>