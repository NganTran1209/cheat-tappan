<?php include_once(__DIR__.'/../common/util.php'); ?>
<?php include_once(__DIR__ . '/../common/login_check.php'); ?>
<?php
if (isset($_POST['action']) && $_POST['action'] == 'settlement') {
	$price = Invoice::selectAmount(getUserId());
	if ($price > 0) {
		$order_number = Invoice::createOrderNumber();
		$xml = settlement($order_number, $price);
		if ($xml->result[0]['result'] == 1) {
			Invoice::registOrderNumber($order_number);
			header('Location: ' . urldecode($xml->result[1]['redirect']));
			exit;
		}
		else {
			setMessage($xml->result[1]['err_code'] . ':' . urldecode($xml->result[2]['err_detail']));
		}
	}
	else {
		setMessage('決済金額は0円です。');
	}
}

?>
<?php include('../header.php'); ?>
<div class="container-fluid margin-contents">
    <div class="container">
        <div class="row">
            <!-- <div class="col-sm-3"> -->
                <?php include('usersidebar.php'); ?>
            <!-- </div> -->
            <!-- col-sm-3 -->
            <div class="col-sm-9 archives  ">
                <article>
                    <header>
                        <h1>決済</h1>
                    </header>
                    <section>
                    決済します。
                        <form method="post" enctype="multipart/form-data" onsubmit="return confirm('決済をする。')">
                            <h3>決済金額</h3>
                            <input class="form-control middle-value" type="text" name="title" value="<?= Invoice::selectAmount(getUserId()) ?>円" placeholder="金額" readonly="readonly">
                            <div class="row margin-20">
                                <div class="col-xs-8">
                                    <input type="hidden" name="action" value="settlement">
                                    <input class="btn btn-success btn-block" type="submit" value="決済する" />
                                </div>
                            </div>
                        </form>
                    </section>
                </article>
            </div>
            <!-- col-sm-9 -->

        </div>
        <!-- row -->

    </div>
    <!-- container -->
</div>
<!-- container-fluid -->

<?php include('../footer.php'); ?>
