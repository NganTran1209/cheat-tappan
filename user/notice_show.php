<?php 
$title_page = 'お知らせ';
$description_page = '欲しい商品を画像で確認・届いた商品を画像で照合！気持ち良い取引が叶うオンラインマーケット。';
 ?>
<?php include_once(__DIR__ . '/../common/util.php'); ?>
<?php include_once(__DIR__ . '/../common/login_check.php'); ?>
<?php
$info = new Information();
if (!empty($_GET['id'])) {
  $info->id = $_GET['id'];
  $info->select();
}

$title_page = 'お知らせ';
?>
<?php include(__DIR__ . '/../user_header.php'); ?>
<div class="com-header-top">
  <div class="com-header-top__img wow animate__animated animate__fadeInUp">
    <img src="<?php echo HOME_URL; ?>/common/assets/img/common/other-com-header.png" alt="">
  </div>
  <div class="com-header-top__path bg-other-01 wow animate__animated animate__fadeInUp">
    <p><span><a href="<?php echo HOME_URL; ?>/" class="clr-yel">トップページ</a></span><span> > </span>
      <span class="clr-yel"><a href="<?php echo HOME_URL; ?>/user/notice.php" class="clr-yel">お知らせ</a></span><span>></span><span><?= $info->title ?></span>
    </p>
  </div>
  <div class="com-header-top__txt wow animate__animated animate__fadeInUp">
    <p>お知らせ</p>
  </div>
</div>
<div class="customer-container wow animate__animated animate__fadeInUp">
  <div class="customer-contact-form">
    <div class="notice-show">
      <div class="notice-show__top">
        <p><?= $info->title ?></p>
        <p><i class="bi bi-alarm"></i> <?= $info->regist_date ?></p>
      </div>
      <div class="notice-show__content">
        <p>
          <?php echo nl2br(htmlentities($info->text)); ?>
        </p>
      </div>
      <div class="notice-show__btn">
        <button onclick="location.href= '<?php echo HOME_URL; ?>/user/notice.php'">お知らせ一覧に戻る</button>
      </div>
    </div>
  </div>
</div>

<?php include('../footer.php'); ?>