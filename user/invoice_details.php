<?php include_once(__DIR__ . '/../common/util.php'); ?>
<?php include_once(__DIR__ . '/../common/login_check.php'); ?>
<?php
$invoice = new Invoice();
$items = $invoice->selectUser(getUserId());
usort($items, function ($a, $b) {
    if ($a->month > $b->month) {
        return -1;
    } elseif ($a->month < $b->month) {
        return 1;
    }
    return 0;
});
?>
<?php include('../header.php'); ?>
    <div class="container">
        <div class="row">
            <!-- <div class="col-sm-3 sideContents"> -->
                <?php include('usersidebar.php'); ?>
            <!-- </div> -->
            <?php /* カード決済実装時に利用停止
            <div class="col-sm-9 mainContents">
                <div class="bg-inner">
                <h1>請求詳細情報　<span class="smallEX">※取引があった月のみ表示します。</span></h1>
                <div class="table-responsive">
                    <table class="table table-bordered">
                        <tr>
                            <th>請求月</th>
                            <!-- <th>利用料</th>-->
                            <th>取引件数</th>
                            <th>取引金額</th>
                            <th>手数料率</th>
                            <th>手数料</th>
                            <th>請求金額</th>
                            <th>支払状況</th>
                        </tr>
                        <?php foreach ($items as $item): ?>
                            <?php if ($item->status > 0) : ?>
                                <tr>
                                    <td class="text-right">
                                        <?= toMonthJp($item->month) ?>
                                    </td>
                                    <!--                  <td class="text-right">-->
                                    <!-- ? //= number_format($item->price) ? -->
                                    <!--円</td>-->
                                    <td class="text-right">
                                        <?= number_format($item->transaction_count) ?>件
                                    </td>
                                    <td class="text-right">
                                        <?= number_format($item->transaction_amount) ?>円
                                    </td>
                                    <td class="text-right">
                                        <?= number_format($item->commission_rate) ?>%
                                    </td>
                                    <td class="text-right">
                                        <?= number_format($item->commission) ?>円
                                    </td>
                                    <td class="text-right">
                                        <?= number_format($item->amount) ?>円
                                    </td>
                                    <td class="text-right">
                                        <?= Invoice::toStatusUserText($item->status) ?>
                                        <?php if ($item->status == 1): ?>
                                        <div><a href="entry_card_user.php">クレジットで支払う</a></div>
                                        <?php endif; ?>
                                    </td>
                                </tr>
                            <?php endif; ?>
                        <?php endforeach; ?>
                    </table>
                </div>
                </div>
            </div>
            */ ?>
        </div>
    </div>

<?php include('../footer.php'); ?>