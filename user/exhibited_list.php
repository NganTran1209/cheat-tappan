<?php 
$title_page = '購入済み';
$description_page = '欲しい商品を画像で確認・届いた商品を画像で照合！気持ち良い取引が叶うオンラインマーケット。';
 ?>
<?php include_once(__DIR__ . '/../common/util.php'); ?>
<?php include_once(__DIR__ . '/../common/login_check.php'); ?>
<?php
$item = new Item();
$items = $item->selectBuy();
usort($items, function ($a, $b) {
    if (strtotime($a->last_date) == strtotime($b->last_date)) {
        return 0;
    } elseif (strtotime($a->last_date) > strtotime($b->last_date)) {
        return -1;
    }
    return 1;
});

$page = 1;
$page = empty($_POST['page']) ? '1' : $_POST['page'];
if (!empty($_GET['page'])) {
    $page = $_GET['page'];
}

$title_page = '購入済み';
?>
<?php include(__DIR__ . '/../user_header.php'); ?>
<form id="fm-param" method="post">
    <input type="hidden" id="id-page" name="page" value="<?= $page ?>" />
</form>
<div class="com-header-top">
    <div class="com-header-top__img wow animate__animated animate__fadeInUp">
        <img src="<?php echo HOME_URL; ?>/common/assets/img/mypage/top-bg.png" alt="">
    </div>
    <div class="com-header-top__path wow animate__animated animate__fadeInUp">
        <p><span><a href="<?php echo HOME_URL; ?>/" class="clr-yel">トップページ</a></span><span> > </span><span><a
                    href="<?php echo HOME_URL; ?>/user/mypage.php" class="clr-yel">マイページ</a></span><span> >
            </span><span>購入済み</span></p>
    </div>
    <!--     <div class="com-header-top__txt">
            <p class="clr-white">マイページ</p>
        </div>-->
</div>
<div class="com-container bg-yellow">
    <?php include('usersidebar.php'); ?>
    <div class="com-content">
        <div class="content-title">
            <h3><span>購入済み</span></h3>
        </div>
        <div class="qnego-sm-title">
            <p>※表示期間は最終連絡日から90日間です。</p>
        </div>
        <div class="table-responsive">
            <table class="table table-bordered">
                <thead>
                    <tr>
                        <th scope="col">掲載日</th>
                        <th scope="col">タイトル</th>
                        <th scope="col">価格</th>
                        <th scope="col">評価</th>
                    </tr>
                </thead>
                <tbody>
                    <?php
                      if (empty($items)) { ?>
                    <tr>
                        <td colspan="4">データなし</td>
                    </tr>
                    <?php } ?>
                    <!-- <tr>
                        <td scope="row">2021-01-04 17:33:09</th>
                        <td class="clr-yel">あいうえおあいうえおあいうえおあいうえ...</td>
                        <td>1,000円</td>
                        <td class="clr-yel">編集</td>
                    </tr> -->
                    <?php
                    $count = 0;
                    $index = 0;
                    foreach ($items as $item): ?>
                    <?php
                        if ($item->enabled == 9) {
                            continue;
                        }
                        if (diffDays(date("Y/m/d"), $item->order_date) > MAX_VIEW_DAYS) {
                            continue;
                        }
                        $count++;
                        if (ceil(++$index / MAX_PAGE_COUNT) != $page) {
                            continue;
                        }
                        $owner = new User();
                        $owner->select($item->owner_id);
                        ?>
                    <tr>
                        <td scope="row">
                            <?= $item->last_date ?><?php /*?><?= date('Y/n/j', strtotime($item->last_date)) ?><?php */ ?>
                        </td>
                        <!--                        <td class="col-sm-2 text-center"><img class="selllist-img" src="--><?php //echo getContextRoot()?>
                        <!--/images/item/id--><?php //echo $item->id ?>
                        <!--/thumbnail.jpg" width="92" height="61" alt=""/></td>-->
                        <td class="">
                            <a class="clr-yel"
                                href="negotiation.php?item_id=<?= $item->id ?>&user_id=<?= $item->buyer_id ?>"
                                <?= $item->createBlockScript($item->owner_id) ?>><?= $item->title ?></a>
                            <!--                            <a href="../itemdetail.php?id=--><?php //echo $item->id ?>
                            <!--">--><?php //echo $item->title ?>
                            <!--</a>-->
                        </td>
                        <td class="text-right"><?= number_format($item->price) ?>円</td>
                        <?php if ($item->seller_evaluate == 0): ?>
                        <td class="text-center"><a class="clr-yel" href="evaluation.php?id=<?= $item->order_id ?>">未評価</a></td>
                        <?php else: ?>
                        <td class="text-center"><a class="clr-yel"
                                href="evaluation.php?id=<?= $item->order_id ?>">評価済</a></td>
                        <?php endif; ?>

                        </td>
                        <!--                        <td class="col-sm-1 text-center">--><?php //echo number_format($item->negotiation_count) ?>
                        <!--件</td>-->
                        <!--                        <td class="">--><?php //echo $owner->name ?>
                        <!----><?php //echo $owner->getViewId() ?>
                        <!--</td>-->
                    </tr>
                    <?php endforeach; ?>
                </tbody>
            </table>
        </div>

        <?php
          $prev_page = $page - 1;
          $max_page = ceil($count / MAX_PAGE_COUNT);
          $next_page = min($max_page, $page + 1);
          $view_start_page = max(1, $page - ceil(VIEW_PAGE_SELECT_COUNT / 2));
          $view_last_page = min($view_start_page + VIEW_PAGE_SELECT_COUNT - 1, $max_page);
          if ($view_last_page - $view_start_page < VIEW_PAGE_SELECT_COUNT) {
              $view_start_page = max(1, $view_last_page - VIEW_PAGE_SELECT_COUNT + 1);
          }
        ?>
        <?php if($max_page >1): ?>
        <div class="content-group-pagination">
            <?php else: ?>
            <div class="content-group-pagination content-one-page">
                <?php endif; ?>

                <?php if($max_page > 1 ) { ?>
                <?php if ($prev_page > 0): ?>
                <a id="pagination__preve" class="pagination__preve" href=""
                    onclick="submitForm('fm-param', 'page', '<?= $prev_page ?>'); return false;"><i
                        class="bi bi-chevron-left"></i></a>
                <?php else: ?>
                <a id="pagination__preve" class="pagination__preve" href="" onclick="return false;"><i
                        class="bi bi-chevron-left"></i></a>
                <?php endif; ?>
                <?php } ?>
                <?php for ($i = $view_start_page; $i <= $view_last_page; $i++): ?>
                <?php if ($page != $i): ?>
                <a class="pagination__num" href=""
                    onclick="submitForm('fm-param', 'page', '<?= $i ?>'); return false;"><span><?= $i ?></span></a>
                <?php else: ?>
                <a class="pagination__num pag_active" href="" onclick="return false;"><span><?= $i ?></span></a>
                <?php endif; ?>
                <?php endfor; ?>

                <?php if($max_page > 1 ) { ?>
                <?php if ($page < $max_page): ?>
                <a id="pagination__next" class="pagination__preve" href=""
                    onclick="submitForm('fm-param', 'page', '<?= $next_page ?>'); return false;">
                    <i class="bi bi-chevron-right"></i>
                </a>
                <?php else: ?>
                <a id="pagination__next" class="pagination__preve" href="" onclick="return false;"><i
                        class="bi bi-chevron-right"></i></a>
                <?php endif; ?>
                <?php } ?>
            </div>
        </div>
    </div>
    
<?php include('../footer.php'); ?>