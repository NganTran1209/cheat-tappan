<?php 
$title_page = '購入希望の募集履歴';
$description_page = '欲しい商品を画像で確認・届いた商品を画像で照合！気持ち良い取引が叶うオンラインマーケット。';
 ?>
<?php include_once(__DIR__ . '/../common/util.php'); ?>
<?php include_once(__DIR__ . '/../common/login_check.php'); ?>
<?php
$req = new Request();
$items = $req->selectUser(getUserId());
$page = 1;
$page = empty($_POST['page']) ? '1' : $_POST['page'];
if (!empty($_GET['page'])) {
    $page = $_GET['page'];
}
$mode = isset($_GET['mode']) ? $_GET['mode'] : '';
$negotiation = false;
if (isset($_POST['mode']) && $_POST['mode'] == 'negotiation') {
    $mode = $_POST['mode'];
}
if (isset($mode) && $mode == 'negotiation') {
    $negotiation = true;
}
$title_page = '購入希望の募集履歴';
?>
<?php include(__DIR__ . '/../user_header.php'); ?>
<!--     <div class="com-header-top">
        <div class="com-header-top__img">
            <img src="<?php echo HOME_URL; ?>/common/assets/img/mypage/top-bg.png" alt="">
        </div>
        <div class="com-header-top__path">
            <p><span><a href="<?php echo HOME_URL; ?>/" class="clr-yel">トップページ</a></span><span> > </span><span><a href="<?php echo HOME_URL; ?>/user/mypage.php" class="clr-yel">マイページ</a></span><span> > </span><span>購入希望の募集履歴</span></p>
        </div>
        <div class="com-header-top__txt">
            <p class="clr-white">マイページ</p>
        </div>
    </div> -->
    <div class="com-container bg-yellow">
        <?php include('usersidebar.php'); ?>
        <div class="com-content">
            <div class="content-title">
                <h3><span>購入希望の募集履歴</span></h3>
            </div>
            <div class="table-responsive purchase__wishes">
                <table class="table table-bordered">
                    <thead>
                      <tr>
                        <th scope="col">掲載日</th>
                        <th scope="col">状態</th>
                        <th scope="col">商品名</th>
                        <th scope="col">予算</th>
                      </tr>
                    </thead>
                    <tbody>
                    <?php
                        $count = 0;
                        $index = 0;
                        foreach ($items as $item):
                            //if (diffDays(date("Y/m/d"), $item->update_date) > MAX_VIEW_DAYS) {continue;}
                            ?>
                            <?php if ($item->enabled != 1) {
                            continue;
                        }
                            $count++;
                            if (ceil(++$index / MAX_PAGE_COUNT) != $page) {
                                continue;
                            }
                    ?>

                      <tr>
                        <td class="text-right" scope="row">
                            <?= $item->update_date ?>
                        </td>
                        <td class="text-center">
                            <?= Request::toStatusUserText($item->enabled) ?>
                        </td>
                        <td class="clr-yel">
                            <a href="request_details.php?id=<?= $item->id ?>"><?= $item->title ?></a>
                        </td>
                        <td class="text-right">
                            <?= number_format($item->price) ?>円
                        </td>
                        <!-- <td scope="row">2021-01-04 17:33:09</th>
                        <td>掲載中</td>
                        <td class="">あいうえおあいうえおあいうえおあいうえ...</td>
                        <td>1,980円</td> -->
                      </tr>
                    <?php endforeach; ?>

                    </tbody>
                </table>
            </div>

            <div class="content-group-pagination">
                <?php
                $prev_page = $page - 1;
                $max_page = ceil($count / MAX_PAGE_COUNT);
                $next_page = min($max_page, $page + 1);
                $view_start_page = max(1, $page - ceil(VIEW_PAGE_SELECT_COUNT / 2));
                $view_last_page = min($view_start_page + VIEW_PAGE_SELECT_COUNT - 1, $max_page);
                if ($view_last_page - $view_start_page < VIEW_PAGE_SELECT_COUNT) {
                    $view_start_page = max(1, $view_last_page - VIEW_PAGE_SELECT_COUNT + 1);
                }
                ?>
                <?php if ($prev_page > 0): ?>
                    <a id="pagination__preve" class="pagination__preve" href=""
                       onclick="submitForm('fm-param', 'page', '<?= $prev_page ?>'); return false;">前へ</a>
                <?php else: ?>
                    <a class="pagination__preve" href="" onclick="return false;">前へ</a>
                <?php endif; ?>

                <?php for ($i = $view_start_page; $i <= $view_last_page; $i++): ?>
                    <?php if ($page != $i): ?>
                        <a class="pagination__num" href=""
                           onclick="submitForm('fm-param', 'page', '<?= $i ?>'); return false;"><?= $i ?></a>
                    <?php else: ?>
                        <a class="pagination__num active" href="" onclick="return false;"><?= $i ?></a>
                    <?php endif; ?>
                <?php endfor; ?>

                <?php if ($page < $max_page): ?>
                    <a class="pagination__next" class="pagination__next" href=""
                       onclick="submitForm('fm-param', 'page', '<?= $next_page ?>'); return false;">次へ</a>
                <?php else: ?>
                    <a id="pagination__next" class="pagination__next" href="" onclick="return false;">次へ</a>
                <?php endif; ?>

                <p class="small"><?= number_format(($page - 1) * MAX_PAGE_COUNT + 1) ?>
                    ～<?= number_format(min($count, $page * MAX_PAGE_COUNT)) ?>件/<?= number_format($count) ?>
                    件</p>
            </div>

        </div>
    </div>
<?php include('../footer.php'); ?>