<?php 
$title_page = 'お気に入り一覧';
$description_page = '欲しい商品を画像で確認・届いた商品を画像で照合！気持ち良い取引が叶うオンラインマーケット。';
 ?>
<?php include_once(__DIR__ . '/../common/util.php'); ?>
<?php include_once(__DIR__.'/../common/login_check.php'); ?>
<?php
$item = new Item();
$items = $item->selectFavorites();
usort($items, function ($a, $b) {
    if (strtotime($a->favorites_regist_date) == strtotime($b->favorites_regist_date)) {
        return 0;
    } elseif (strtotime($a->favorites_regist_date) > strtotime($b->favorites_regist_date)) {
        return -1;
    }
    return 1;
});
$page = 1;
$page = empty($_POST['page']) ? '1' : $_POST['page'];
if (!empty($_GET['page'])) {
    $page = $_GET['page'];
}
$mode = isset($_GET['mode']) ? $_GET['mode'] : '';
$negotiation = false;
if (isset($_POST['mode']) && $_POST['mode'] == 'negotiation') {
    $mode = $_POST['mode'];
}
if (isset($mode) && $mode == 'negotiation') {
    $negotiation = true;
}
$title_page = 'お気に入り一覧';
?>
<?php include('../user_header.php'); ?>
<form id="fm-param" method="post">
    <input type="hidden" id="mode" name="mode" value="<?= $mode ?>"/>
    <input type="hidden" id="id-page" name="page" value="<?= $page ?>"/>
</form>
<div class="com-container bg-yellow">
        <?php include('usersidebar.php'); ?>
        <div class="com-content">
            <div class="content-title">
                <h3><span>お気に入り一覧</span></h3>
            </div>
            <div class="qnego-sm-title">
                <p>※表示期間はお気に入り追加日から90日間です。</p>
            </div>
            <div class="table-responsive">
                <table class="table table-bordered">
                    <thead>
                      <tr>
                        <th scope="col">お気に入り追加日</th>
                        <th scope="col">タイトル</th>
                        <th scope="col">価格</th>
                      </tr>
                    </thead>
                    <tbody>
                    <?php
                    $count = 0;
                    $index = 0;
                    foreach ($items as $item):
                        if ($item->enabled == 9) {
                            continue;
                        }
                        if ($item->isBlock()) {
                            continue;
                        }
                        if (diffDays(date("Y/m/d"), $item->favorites_regist_date) > MAX_VIEW_DAYS) {
                            continue;
                        }
                        if (User::isInvalid($item->owner_id)) {
                            continue;
                        }
                        if (empty($item->buyer_id) || $item->buyer_id = getUserId()):
                            $count++;
                            if (ceil(++$index / MAX_PAGE_COUNT) != $page) {
                                continue;
                            }
                            ?>
                            <tr>
                                <td class="text-right"><?= $item->favorites_regist_date ?></td>
                                <td>
                                    <a href="<?php echo HOME_URL; ?>/itemdetail.php?id=<?= $item->id ?>" <?= $item->createBlockScript() ?>><?= $item->title ?></a>
                                </td>
                                <td class="text-right"><?= number_format($item->price) ?>円</td>
                            </tr>
                        <?php endif; ?>
                    <?php endforeach; ?>
                    </tbody>
                </table>
            </div>
                <div class="text-center margin-20">
                    <?php
                    $prev_page = $page - 1;
                    $max_page = ceil($count / MAX_PAGE_COUNT);
                    $next_page = min($max_page, $page + 1);
                    $view_start_page = max(1, $page - ceil(VIEW_PAGE_SELECT_COUNT / 2));
                    $view_last_page = min($view_start_page + VIEW_PAGE_SELECT_COUNT - 1, $max_page);
                    if ($view_last_page - $view_start_page < VIEW_PAGE_SELECT_COUNT) {
                        $view_start_page = max(1, $view_last_page - VIEW_PAGE_SELECT_COUNT + 1);
                    }
                    ?>
                    <?php if($max_page > 1):  ?>  
                        <?php if ($prev_page > 0): ?>
                            <a class="sortButton" href=""
                            onclick="submitForm('fm-param', 'page', '<?= $prev_page ?>'); return false;">前へ</a>
                        <?php else: ?>
                            <a class="sortButton" href="" onclick="return false;">前へ</a>
                        <?php endif; ?>
                    <?php endif; ?>
                    <?php for ($i = $view_start_page; $i <= $view_last_page; $i++): ?>
                        <?php if ($page != $i): ?>
                            <a class="sortButton" href=""
                               onclick="submitForm('fm-param', 'page', '<?= $i ?>'); return false;"><?= $i ?></a>
                        <?php else: ?>
                            <a class="sortButton active" href="" onclick="return false;"><?= $i ?></a>
                        <?php endif; ?>
                    <?php endfor; ?>
                    <?php if($max_page > 1):  ?> 
                        <?php if ($page < $max_page): ?>
                            <a class="sortButton" href=""
                            onclick="submitForm('fm-param', 'page', '<?= $next_page ?>'); return false;">次へ</a>
                        <?php else: ?>
                            <a class="sortButton" href="" onclick="return false;">次へ</a>
                        <?php endif; ?>
                    <?php endif; ?>
                    
                </div>
        </div>
</div>

<?php include('../footer.php'); ?>
