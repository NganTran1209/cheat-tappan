<?php include_once(__DIR__ . '/../common/util.php'); ?>
<?php include_once(__DIR__ . '/../common/login_check.php'); ?>
<?php
if (isIdentification()) {
    //	header('Location: '.getContextRoot().'/user/mypage.php');
}

if (isset($_POST['action']) && $_POST['action'] == 'upload') {
    if ($_FILES['photo']['error'] == 0) {
        Identification::regist_tmp();
        header('Location: ' . getContextRoot() . '/user/upload_identification_confirm.php');
        exit();
    } else {
        setMessage('本人確認書類画像は必須です。');
    }

}
$system_config = SystemConfig::select();
?>
<?php include('../header.php'); ?>
    <div class="container">
        <div class="row">
            <div class="col-md-3 sideContents">
                <?php include('usersidebar.php'); ?>
            </div>
            <div class="col-md-9 mainContents">
                <div class="bg-inner">
                    <h1>本人確認書類の提出</h1>
                    <p>
                        <strong>デジタルカメラや携帯電話カメラで撮影した画像をアップロードしてください。</strong>
                        <br> ※運転免許証、パスポート、公の機関が発行した資格証明書で写真付きの画像を添付して下さい。
                        <br> ※運転免許証など、裏面に現住所の記載がある場合は裏面も撮影して画像を添付して下さい。
                        <br> ※顔写真がついていないものは認められません。
                    </p>
                    <p class="fontRed">
                        <strong class="font-black">スマホから本人確認書類の提出をされる方へ</strong>
                        <br> ※特定のブラウザかブラウザアプリを使用している場合、アップロードできないことがあります。
                        <br> その場合、PCからおこなってください。PCをお持ちでない方は、当社アドレスへ添付してお送りください。
                        <br> 当社アドレス「<?= $system_config->from_email ?>」
                    </p>
                    <form method="post" enctype="multipart/form-data">
                        <h2>本人確認書類をアップロードする</h2>
                        <div class="input-group mb-2">
                            <div class="custom-file">
                                <input type="file" name="photo" id="photo" class="custom-file-input"
                                       aria-describedby="photoAdd">
                                <label class="custom-file-label" for="photo" data-browse="参照">1枚目画像選択...</label>
                            </div>
                        </div>
                        <div class="input-group mb-2">
                            <div class="custom-file">
                                <input type="file" name="photo2" id="photo2" class="custom-file-input"
                                       aria-describedby="photo2Add">
                                <label class="custom-file-label" for="photo2" data-browse="参照">2枚目画像選択...</label>
                            </div>
                        </div>
                        <!--                    <p>◆一枚目<br>-->
                        <!--                        <input class="" type="file" name="photo" placeholder="画像">-->
                        <!--                    </p>-->
                        <!--                    <p>◆二枚目<br>-->
                        <!--                        <input class="" type="file" name="photo2" placeholder="画像2">-->
                        <!--                    </p>-->
                        <div class="my-3">
                            <input type="hidden" name="action" value="upload"/>
                            <input class="btn btn-block btn-info" type="submit" value="確認する"/>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>

<?php include('../footer.php'); ?>