<?php include_once(__DIR__ . '/../common/util.php'); ?>
<?php include_once(__DIR__.'/../common/login_check.php'); ?>
<?php
//if (!isSelling()) {
//    setMessage('出品が許可されておりません。出品許可を得るためには「本人確認書類」の提出をお願いします。');
//    header('Location: ' . getContextRoot() . '/user/upload_identification.php');
//    exit;
//}
//if (!isIdentification()) {
//    setMessage('本人確認が完了しておりません。');
//    header('Location: ' . getContextRoot() . '/user/upload_identification.php');
//    exit;
//}

$isOffer = false;
if (isset($_POST['offer']) && $_POST['offer'] == 'offer') {
    $isOffer = true;
} else {
    unset($_SESSION['targetRequest']);
}

if (isset($_POST['action']) && ($_POST['action'] == 'modify' || $_POST['action'] == 'cancel-load-tmp')) {
    $entity = $_SESSION['item'];
} elseif (isset($_POST['action']) && $_POST['action'] == 'load-tmp') {
    $entity = new Item();
    $entity->selectTmp($_POST['tmp-id']);
    setMessage('下書きを読み込みました。', true);
} elseif (isset($_POST['action']) && $_POST['action'] == 'save-tmp' && empty($_POST['title'])) {
    echo json_encode('下書き登録はタイトルは必須です。');
    exit();
} else if (isset($_POST['title']) || isset($_POST['action'])) {
//	ini_set('display_errors', 0);
    $entity = new Item();
    $entity->title = $_POST['title'];
    $entity->category = empty($_POST['category']) ? null : $_POST['category'];
    $entity->item_state = isset($_POST['item_state']) ? $_POST['item_state'] : null;
    $entity->remarks = $_POST['remarks'];
    $entity->pref = empty($_POST['pref']) ? null : $_POST['pref'];
    $entity->carriage_plan = empty($_POST['carriage_plan']) ? null : $_POST['carriage_plan'];
    $entity->carriage_type = empty($_POST['carriage_type']) ? null : $_POST['carriage_type'];
    $entity->delivery_type = empty($_POST['delivery_type']) ? null : $_POST['delivery_type'];
    $entity->show_evaluate = empty($_POST['show_evaluate']) ? null : $_POST['show_evaluate'];
    $entity->with_delivery = empty($_POST['with_delivery']) ? null : $_POST['with_delivery'];
    //$entity->delivery_price= $_POST['delivery_price'];
    $entity->delivery_price = 0;
    $entity->price = empty($_POST['price']) ? null : $_POST['price'];
    if (!empty($_FILES['photo1']) && $_FILES['photo1']['error'] == 0) {
        $entity->photo1 = rotateImage(file_get_contents($_FILES['photo1']['tmp_name']));
    } else {
        $entity->photo1 = null;
    }
    if (!empty($_FILES['photo2']) && $_FILES['photo2']['error'] == 0) {
        $entity->photo2 = rotateImage(file_get_contents($_FILES['photo2']['tmp_name']));
    } else {
        $entity->photo2 = null;
    }
    if (!empty($_FILES['photo3']) && $_FILES['photo3']['error'] == 0) {
        $entity->photo3 = rotateImage(file_get_contents($_FILES['photo3']['tmp_name']));
    } else {
        $entity->photo3 = null;
    }
    if (!empty($_FILES['photo4']) && $_FILES['photo4']['error'] == 0) {
        $entity->photo4 = rotateImage(file_get_contents($_FILES['photo4']['tmp_name']));
    } else {
        $entity->photo4 = null;
    }

    if (!empty($_POST['free_items'])) {
        foreach ($_POST['free_items'] as $key => $value) {
            $free_info = new ItemFreeInfo();
            $free_info->free_input_item_id = $key;
            $free_info->free_input_item_value_id = $value;
            $entity->free_input[$key] = $free_info;
        }
    }

    if (isset($_POST['action']) && $_POST['action'] == 'validation') {
        $entity->validate();
        $result = getMessage();
        echo json_encode($result);
        exit();
    }

    if (isset($_POST['action']) && $_POST['action'] == 'move-load-tmp') {
        $_SESSION['item'] = $entity;
        header('Location: ' . getContextRoot() . '/user/draft_list.php');
        exit();
    }

    if (isset($_POST['action']) && $_POST['action'] == 'save-tmp') {
        $tmps = $entity->selectTmpFromUser(getUserId());
        if (count($tmps) < MAX_TEMP_COUNT) {
            $entity->registTmp();
            echo json_encode('下書きを保存しました。');
            exit();
        } else {
            echo json_encode('下書きは[' . MAX_TEMP_COUNT . '件]まで登録できます。');
            exit();
        }
    }

    if ($entity->validate()) {
        $_SESSION['item'] = $entity;

        header('Location: ' . getContextRoot() . '/user/sell_confirm.php');
        exit();
    }
} else {
    $entity = new Item();
    $entity->pref = getUserPref();
}
unset($_SESSION['item']);
$system_config = SystemConfig::select();

?>
<?php include('../header.php'); ?>
<script>
    function confirm() {
        var requestData = new FormData();

        var $f = $('#sell-form');
        var valueData = $f.serializeArray();
        $.each(valueData, function (key, input) {
            requestData.append(input.name, input.value);
        });
        requestData.append('action', 'validation');

        var fileData = $('input[name="photo1"]')[0].files;
        for (var i = 0; i < fileData.length; i++) {
            requestData.append("photo1", fileData[i]);
        }
        var result = $.ajax({
            url: './sell.php',
            type: 'post',
            async: false,
            processData: false,
            contentType: false,
            data: requestData
        }).responseText;

        var obj = JSON.parse(result);

        if (obj.msg.length > 0) {
            window.alert(obj.msg);
            return false;
        }
        return true;
    }

    function saveTmp() {
        var requestData = new FormData();

        var $f = $('#sell-form');
        var valueData = $f.serializeArray();
        $.each(valueData, function (key, input) {
            requestData.append(input.name, input.value);
        });
        requestData.append('action', 'save-tmp');

        var fileData = $('input[name="photo1"]')[0].files;
        for (var i = 0; i < fileData.length; i++) {
            requestData.append("photo1", fileData[i]);
        }
        var result = $.ajax({
            url: './sell.php',
            type: 'post',
            async: false,
            processData: false,
            contentType: false,
            data: requestData
        }).responseText;

        var obj = JSON.parse(result);

        if (obj.length > 0) {
            window.alert(obj);
            return false;
        }
        return false;
    }

    function loadTmp() {
        submitForm('sell-form', 'action', 'move-load-tmp');
        var $f = $('#tmp-form').submit();
    }

    $(function() {
        $('#with_delivery').on('change', function() {
            var disabled = !$(this).prop('checked');
            $('#delivery_type').prop('disabled', disabled);
            $('#carriage_plan').prop('disabled', disabled);
            $('#carriage_type').prop('disabled', disabled);
        });
    });

</script>

<div class="container">
    <div class="row">
        <!-- <div class="col-md-3 sideContents"> -->
            <?php include('usersidebar.php'); ?>
        <!-- </div> -->
        <div class="col-md-9 mainContents ">
            <div class="bg-inner">
                <h1>出品する</h1>
                <?= ITEM_NAME ?>・出品情報を入力します。
                <form id="sell-form" method="post" enctype="multipart/form-data" onsubmit="return confirm();">
                    <div class="row mb-3">
                        <div class="col-sm-6">
                        </div>
                        <div class="col-sm-6">
                            <input class="btn btn-block btn-info btn-sm" type="button" value="保存した下書き一覧へ"
                                   onclick="loadTmp()"/>
                        </div>
                    </div>

                    <div class="border-bottom py-4">
                        <h2 class="bg-light p-2 fontBold my-3"><?= ITEM_NAME ?>の情報</h2>
                        <h3 class="fontBold my-2"><?= ITEM_NAME ?>画像</h3>
                        <div class="input-group mb-2">
                            <div class="custom-file">
                                <input type="file" name="photo1" id="photo1" class="custom-file-input"
                                       aria-describedby="photo1Add">
                                <label class="custom-file-label" for="photo1" data-browse="参照">ファイル選択...</label>
                            </div>
                        </div>
                        <div class="input-group mb-2">
                            <div class="custom-file">
                                <input type="file" name="photo2" id="photo2" class="custom-file-input"
                                       aria-describedby="photo2Add">
                                <label class="custom-file-label" for="photo2" data-browse="参照">ファイル選択...</label>
                            </div>
                        </div>
                        <div class="input-group mb-2">
                            <div class="custom-file">
                                <input type="file" name="photo3" id="photo3" class="custom-file-input"
                                       aria-describedby="photo3Add">
                                <label class="custom-file-label" for="photo3" data-browse="参照">ファイル選択...</label>
                            </div>
                        </div>
                        <div class="input-group mb-2">
                            <div class="custom-file">
                                <input type="file" name="photo4" id="photo4" class="custom-file-input"
                                       aria-describedby="photo4Add">
                                <label class="custom-file-label" for="photo4" data-browse="参照">ファイル選択...</label>
                            </div>
                        </div>
                    </div>

                    <div class="border-bottom py-4">
                        <h2 class="bg-light p-2 fontBold"><?= ITEM_NAME ?>の詳細</h2>

                        <h3 class="fontBold">タイトル</h3>
                        <input class="form-control middle-value form-control-sm" type="text" name="title"
                               value="<?= $entity->title ?>" placeholder="タイトル">
                        <h3 class="fontBold mt-4"><?= ITEM_NAME ?>の説明文</h3>
                        <textarea class="form-control" rows="8" id="" maxlength="20000"
                                  name="remarks"><?= $entity->remarks ?></textarea>
                        <span class="font10">※最大1.5万文字まで説明文を入力できます。</span>


                        <?php if ($system_config->enabled_category):?>
                            <h3 class="fontBold mt-4">カテゴリー</h3>
                            <select class="form-control form-control-sm" name="category" id="">
                                <option value="">未選択</option>
                                <?php createCombo('selectCategory', $entity->category); ?>
                            </select>
                        <?php endif;?>

                        <?php if ($system_config->enabled_item_state):?>
                            <h3 class="fontBold mt-4"><?= ITEM_NAME ?>の状態</h3>
                            <?php //createRadio('selectItemState', 'item_state', $entity->item_state); ?>
                            <select class="form-control form-control-sm" name="item_state" id="">
                                <option value="">未選択</option>
                                <?php createCombo('selectItemState', $entity->item_state); ?>
                            </select>
                        <?php endif;?>

                        <?php foreach(FreeInputItem::selectAll() as $item1):?>
                            <?php if (!$item1->enabled) {continue;}?>
                            <h3 class="fontBold mt-4"><?= $item1->name ?></h3>
                            <select class="form-control form-control-sm" name="free_items[<?= $item1->id ?>]" id="">
                                <option value="0">未選択</option>
                                <?php foreach(FreeInputItemValue::selectFromFreeInputItemId($item1->id) as $item2):?>
                                    <?php if (!$item2->enabled) {continue;}?>
                                    <option value="<?= $item2->id ?>"><?= $item2->name ?></option>
                                <?php endforeach;?>
                            </select>
                        <?php endforeach;?>

                        <?php if ($system_config->enabled_pref):?>
                            <h3 class="fontBold">地域</h3>
                            <select class="form-control form-control-sm" name="pref" id="pref">
                                <option value="">都道府県</option>
                                <?php createCombo('selectPref', $entity->pref); ?>
                            </select>
                        <?php endif;?>


                        <?php if ($system_config->enabled_delivery_type):?>
                            <h2 class="bg-light p-2 fontBold my-3">配送の設定</h2>
                            <div class="custom-control custom-switch">
                                <input type="checkbox" class="custom-control-input" id="with_delivery" name="with_delivery"
                                       value="1"<?= $entity->with_delivery ? ' checked="checked"' : '' ?>>
                                <label class="custom-control-label" for="with_delivery">配送</label>
                            </div>

                            <h3 class="fontBold mt-4">配送方法</h3>
                            <select class="form-control form-control-sm" name="delivery_type" id="delivery_type"<?= $entity->with_delivery ? '' : ' disabled' ?>>
                                <option value="">発送方法</option>
                                <?php createCombo('selectDeliveryType', $entity->delivery_type); ?>
                            </select>
                        <?php endif;?>

                        <?php if ($system_config->enabled_postage):?>
                            <h3 class="fontBold mt-4">配送料の負担</h3>
                            <?php //createRadio('selectCarriagePlan', 'carriage_plan', $entity->carriage_plan); ?>
                            <select class="form-control form-control-sm" name="carriage_plan" id="carriage_plan"<?= $entity->with_delivery ? '' : ' disabled' ?>>
                                <option value="">選択</option>
                                <?php createCombo('selectCarriagePlan', $entity->carriage_plan); ?>
                            </select>
                        <?php endif;?>

                        <?php if ($system_config->enabled_carriage_type):?>
                            <h3 class="fontBold mt-4">配送の目安</h3>
                            <?php //createRadio('selectCarriageType', 'carriage_type', $entity->carriage_type); ?>
                            <select class="form-control form-control-sm" name="carriage_type" id="carriage_type"<?= $entity->with_delivery ? '' : ' disabled' ?>>
                                <option value="">選択</option>
                                <?php createCombo('selectCarriageType', $entity->carriage_type); ?>
                            </select>
                        <?php endif;?>



                        <?php /*?>
                            <h3>配送料金</h3>
                            <input class="form-control middle-value" type="number" name="delivery_price" value="<?= $entity->delivery_price ?>" >
                            <?php */ ?>

                    </div>

                    <div class="border-bottom py-4">
                        <h2 class="bg-light p-2 fontBold my-3">価格(300円～9,999,999円)</h2>
                        <h3 class="fontBold">価格(※税込価格で入力してください)</h3>
                        <input class="form-control middle-value" type="number" name="price"
                               value="<?= $entity->price ?>" placeholder="数字のみ（例10,000円→10000と入力）" max="900000000">
                        <!-- ▼ 評価の条件表示 -->
                        <div class="py-3">
                            <label class="font-normal fontRed"><input type="checkbox" name="show_evaluate" id="show_evaluate"
                                                                      value="1" <?= $entity->show_evaluate ? 'checked' : '' ?>>
                                総合評価の合計が-1以下のユーザーから、質問•交渉、購入をされたくない方はチェックを入れてください。</label>
                        </div>
                        <!-- ▲ 評価の条件表示 -->
                    </div>


                    <div class="row py-4">
                        <div class="col-md-8 mb-3">
                            <?php if ($isOffer): ?>
                                <input type="hidden" name="offer" value="offer"/>
                            <?php endif; ?>
                            <input class="btn btn-block btn-info btn-sm" type="submit" value="確認する"/>
                        </div>
                        <div class="col-md-4 mb-3">
                            <input class="btn btn-block btn-info btn-sm" type="button" value="下書きに保存"
                                   onclick="saveTmp()"/>
                            <div class="text-center font10">※<?= ITEM_NAME ?>の画像は保存されません</div>
                        </div>
                    </div>
                    <input type="hidden" id="id-action" name="action"/>
                </form>
            </div>
        </div>
    </div>
</div>
<?php include('../footer.php'); ?>
