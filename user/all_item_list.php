<?php 
$title_page = '全てのアイテム';
$description_page = '欲しい商品を画像で確認・届いた商品を画像で照合！気持ち良い取引が叶うオンラインマーケット。';
 ?>
<?php include_once(__DIR__ . '/../common/util.php'); ?>
<?php include_once(__DIR__ . '/../common/login_check.php'); ?>
<?php
$item = new Item();
$items = $item->selectAllSellItemByUserID(getUserId());
usort($items, function ($a, $b) {
    $adate = empty($a->last_date) ? $a->order_date : $a->last_date;
    $bdate = empty($b->last_date) ? $b->order_date : $b->last_date;
    if (strtotime($adate) == strtotime($bdate)) {
        return 0;
    } elseif (strtotime($adate) > strtotime($bdate)) {
        return -1;
    }
    return 1;
});

$page = 1;
$page = empty($_POST['page']) ? '1' : $_POST['page'];
$mode = isset($_GET['mode']) ? $_GET['mode'] : '';
$negotiation = false;
if (isset($_POST['mode']) && $_POST['mode'] == 'negotiation') {
    $mode = $_POST['mode'];
}
if (isset($mode) && $mode == 'negotiation') {
    $negotiation = true;
}
$title_page = '全てのアイテム';
$negotiation = new Negotiation();
$order = new Order();
$order->select($item->order_id);
$system_config = SystemConfig::select();
$feedback = new Feedback();
$sizeLimit = 1024;
if (isset($_POST['action'])) {
    if ($_POST['action'] == 'feedback') {
        if ((!empty($_FILES['photo']) && $_FILES['photo']['error'] == 0 && isImage($_FILES['photo']['tmp_name'])) || (!empty($_FILES['photo2']) && $_FILES['photo2']['error'] == 0 && isImage($_FILES['photo2']['tmp_name']))) {
            if($_POST['index'] == 1){
                $feedback->item_id = $_POST['item_id'];
                $feedback->user_id = $_POST['user_id'];
                $feedback->order_id = $_POST['order_id'];
                $feedback->index = $_POST['index'];
                $_FILES['photo'] = changeFileType($_FILES['photo']);
                if (getimagesize($_FILES['photo']['tmp_name'])[1] > $sizeLimit){
                    compress($_FILES['photo']['tmp_name'], 100);
                }
                if (!empty($_FILES['photo']) && $_FILES['photo']['error'] == 0) {
                    $feedback->photo = rotateImage(file_get_contents($_FILES['photo']['tmp_name']));
                } else {
                    $feedback->photo = null;
                }

                $img = curl_file_create($_FILES['photo']['tmp_name'], $_FILES['photo']['type'], basename($_FILES['photo']['name']));
                // $check = $feedback->compareRequestGaziru($_POST['groupCode'], $img);
                $check = $feedback->compareRequestGaziruWithObject($_POST['objectCode'], $img);
                $max_score = json_decode($check,true)["scores"][0]["score"];
                foreach (json_decode($check,true)["scores"] as &$value) {
                    if($value["score"] > $max_score){
                        $max_score = $feedback->score ;
                    }
                }
                $feedback->score =  $max_score;
                $feedback->regist();
               
            }
            if($_POST['index'] == 2){
                $feedback->item_id = $_POST['item_id'];
                $feedback->user_id = $_POST['user_id'];
                $feedback->order_id = $_POST['order_id'];
                $feedback->index = $_POST['index'];
                $_FILES['photo2'] = changeFileType($_FILES['photo2']);
                if (getimagesize($_FILES['photo2']['tmp_name'])[1] > $sizeLimit){
                    compress($_FILES['photo2']['tmp_name'], 100);
                }
                if (!empty($_FILES['photo2']) && $_FILES['photo2']['error'] == 0) {
                    $feedback->photo = rotateImage(file_get_contents($_FILES['photo2']['tmp_name']));
                } else {
                    $feedback->photo = null;
                }
                $img = curl_file_create($_FILES['photo2']['tmp_name'], $_FILES['photo2']['type'], basename($_FILES['photo2']['name']));
                // $check = $feedback->compareRequestGaziru($_POST['groupCode'], $img);
                $check = $feedback->compareRequestGaziruWithObject($_POST['objectCode'], $img);
                $max_score = json_decode($check,true)["scores"][0]["score"];
                foreach (json_decode($check,true)["scores"] as &$value) {
                    if($value["score"] > $max_score){
                        $max_score = $feedback->score ;
                    }
                }
                $feedback->score =  $max_score;
                $feedback->regist();
            }
            
            setMessage('真贋比較画像とアップロードした画像がそれぞれ表示されるようにしてください。');
            
            $order->state = $_POST['state'];
            $order->updateProcedure();            
            $check = true;
            echo '<script type="text/javascript">';
            echo '$(document).ready(function() {';
            echo '$("#modalFeedback").modal("show");';
            echo '});';
            echo '</script>';
        } else {
            setMessage('コメントまたは画像( jpg,jpeg,png,gif )。');
        }
    } elseif ($_POST['action'] == 're-feedback') {
        if((!empty($_FILES['photoRe1']) && $_FILES['photoRe1']['error'] == 0 && isImage($_FILES['photoRe1']['tmp_name'])) || (!empty($_FILES['photoRe2']) && $_FILES['photoRe2']['error'] == 0 && isImage($_FILES['photoRe2']['tmp_name']))){

            if ((!empty($_FILES['photoRe1']) && $_FILES['photoRe1']['error'] == 0 && isImage($_FILES['photoRe1']['tmp_name']))) {

                $feedback->item_id = $_POST['item_id'];
                $feedback->user_id = $_POST['user_id'];
                $feedback->order_id = $_POST['order_id'];
                $feedback->index = $_POST['index'];
                $_FILES['photoRe1'] = changeFileType($_FILES['photoRe1']);
                if (getimagesize($_FILES['photoRe1']['tmp_name'])[1] > $sizeLimit){
                    compress($_FILES['photoRe1']['tmp_name'], 100);
                }
                if (!empty($_FILES['photoRe1']) && $_FILES['photoRe1']['error'] == 0) {
                    $feedback->photo = rotateImage(file_get_contents($_FILES['photoRe1']['tmp_name']));
                } else {
                    $feedback->photo = null;
                }
                $img = curl_file_create($_FILES['photoRe1']['tmp_name'], $_FILES['photoRe1']['type'], basename($_FILES['photoRe1']['name']));
                // $check = $feedback->compareRequestGaziru($_POST['groupCode'], $img);
                $check = $feedback->compareRequestGaziruWithObject($_POST['objectCode'], $img);
                $max_score = json_decode($check,true)["scores"][0]["score"];
                foreach (json_decode($check,true)["scores"] as &$value) {
                    if($value["score"] > $max_score){
                        $max_score = $feedback->score ;
                    }
                }
                $feedback->score =  $max_score;
                $feedback->regist();
                
                setMessage('真贋比較画像とアップロードした画像がそれぞれ表示されるようにしてください。');
                $check = true;
            } 
            if ((!empty($_FILES['photoRe2']) && $_FILES['photoRe2']['error'] == 0 && isImage($_FILES['photoRe2']['tmp_name']))) {

                $feedback->item_id = $_POST['item_id'];
                $feedback->user_id = $_POST['user_id'];
                $feedback->order_id = $_POST['order_id'];
                $feedback->index = $_POST['index'];
                $_FILES['photoRe2'] = changeFileType($_FILES['photoRe2']);
                if (getimagesize($_FILES['photoRe2']['tmp_name'])[1] > $sizeLimit){
                    compress($_FILES['photoRe2']['tmp_name'], 100);
                }
                if (!empty($_FILES['photoRe2']) && $_FILES['photoRe2']['error'] == 0) {
                    $feedback->photo = rotateImage(file_get_contents($_FILES['photoRe2']['tmp_name']));
                } else {
                    $feedback->photo = null;
                }
                $img = curl_file_create($_FILES['photoRe2']['tmp_name'], $_FILES['photoRe2']['type'], basename($_FILES['photoRe2']['name']));
                // $check = $feedback->compareRequestGaziru($_POST['groupCode'], $img);
                $check = $feedback->compareRequestGaziruWithObject($_POST['objectCode'], $img);
                $max_score = json_decode($check,true)["scores"][0]["score"];
                foreach (json_decode($check,true)["scores"] as &$value) {
                    if($value["score"] > $max_score){
                        $max_score = $feedback->score ;
                    }
                }
                $feedback->score =  $max_score;
                $feedback->regist();
                
                setMessage('真贋比較画像とアップロードした画像がそれぞれ表示されるようにしてください。');
                $check = true;
            } 
        } else {
            setMessage('コメントまたは画像( jpg,jpeg,png,gif )。');
        } 
    } elseif($_POST['action'] == 'upStateFb'){
        $feedback->id = $_POST['feedback_id'];
        $feedback->state = $_POST['state'];
        $feedback->updateSate();
        $check = true;
    }
}
function compress($source, $quality) {
    $info = getimagesize($source);
    $width = $new_width = $info[0];
    $height = $new_height = $info[1];
    if ($info['mime'] == 'image/jpeg') 
        $image = imagecreatefromjpeg($source);
    elseif ($info['mime'] == 'image/gif') 
        $image = imagecreatefromgif($source);
    elseif ($info['mime'] == 'image/png') 
        $image = imagecreatefrompng($source);
    else 
        $image = imagecreatefromjpeg($source);
    
    $new_width = 1024;
    $new_height = 1024;
    $new_image = imagecreatetruecolor($new_width, $new_height);
    imagecopyresampled($new_image, $image, 0, 0, 0, 0, $new_width, $new_height, $width, $height);  

    $exif = exif_read_data($source);
    if(!empty($exif['Orientation'])) {
        switch($exif['Orientation']) {
            case 8:
                $image = imagerotate($image,90,0);
                break;
            case 3:
                $image = imagerotate($image,180,0);
                break;
            case 6:
                $image = imagerotate($image,-90,0);
                break;
        } 
    }
    imagejpeg($new_image, $source, $quality);
}

function changeFileType($file){
    $info = getimagesize($file['tmp_name']);
    if ($info['mime'] == 'image/jpeg') 
        $image = imagecreatefromjpeg($file['tmp_name']);
    elseif ($info['mime'] == 'image/gif') 
        $image = imagecreatefromgif($file['tmp_name']);
    elseif ($info['mime'] == 'image/png') 
        $image = imagecreatefrompng($file['tmp_name']);
    else 
        $image = imagecreatefromjpeg($file['tmp_name']);
    imagejpeg($image,$file['tmp_name'], 100);
    $file['name'] = pathinfo($file['name'])['filename'] . '.jpg';
    $file['type'] = 'image/jpeg';
    return $file;
}
?>
<script>
    $(document).ready(function(){
        $('[data-toggle="tooltip"]').tooltip();
    });

    function changeFeedback(){
        $('#photo1').on('change', function () {
            if(this.files.length > 0){
                $('#fr-reUpFeedback').submit();
            }
        }).click();
    }
        function openForm(id) {
            var modal = document.getElementById(id);
            modal.style.display = "block";
            sessionStorage.setItem("modalId",id);
            sessionStorage.setItem("showModal", true);
            if ($('#' + sessionStorage.getItem("modalId")).find('img.img-preview1').length != 0){
                $('#' + sessionStorage.getItem("modalId")).find(".btn-upFb1").hide();
            }
            if ($('#' + sessionStorage.getItem("modalId")).find('img.img-preview2').length != 0){
                $('#' + sessionStorage.getItem("modalId")).find(".btn-upFb2").hide();
            }
        }

        function closeForm(id) {
            var modal = document.getElementById(id);
            modal.style.display = "none";
            sessionStorage.setItem("showModal", false);
        }

        function Feedback1(id){
            $('#photo-'+id).on('change', function () {
                console.log(this.files);
                if(this.files.length > 0){
                    $('#fr-Feedback1-'+id).submit();
                }
            }).click();
            
        }
        function Feedback2(id){
        
            $('#photo2-'+id).on('change', function () {
                console.log(this.files);
                if(this.files.length > 0){
                    $('#fr-Feedback2-'+id).submit();
                    $('#fr-Feedback2-'+id).hide();
                }
            }).click();
        }
        function reUpFeedback1(id){
            $('#photoRe1-'+id).on('change', function () {
                console.log(this.files);
                if(this.files.length > 0){
                    $('#fr-reUpFeedback1-'+id).submit();
                }
            }).click();
        
        }
        function reUpFeedback2(id){
            $('#photoRe2-'+id).on('change', function () {
                console.log(this.files);
                if(this.files.length > 0){
                    $('#fr-reUpFeedback2-'+id).submit();
                }
            }).click();
        
        }
</script>
<?php include(__DIR__ . '/../user_header.php'); ?>
    <div class="com-header-top">
        <div class="com-header-top__img wow animate__animated animate__fadeInUp">
            <img src="<?php echo HOME_URL; ?>/common/assets/img/mypage/top-bg.png" alt="">
        </div>
        <div class="com-header-top__path wow animate__animated animate__fadeInUp">
            <p><span><a href="<?php echo HOME_URL; ?>/" class="clr-yel">トップページ</a></span><span> > </span><span><a href="<?php echo HOME_URL; ?>/user/mypage.php" class="clr-yel">マイページ</a></span>
            <span> > </span><span>全てのアイテム</span></p>
        </div>
<!--        <div class="com-header-top__txt">
            <p class="clr-white">マイページ</p>
        </div>-->
    </div>
<form id="fm-param" method="post">
    <input type="hidden" id="mode" name="mode" value="<?= $mode ?>"/>
    <input type="hidden" id="id-page" name="page" value="<?= $page ?>"/>
</form>
    <div class="com-container bg-yellow">
        <?php include('usersidebar.php'); ?>
        <div class="com-content ">
            <div class="content-title">
                <h3><span>全てのアイテム</span></h3>
            </div>
            <div class="table-responsive">
                <table class="table table-bordered">
                    <thead>
                      <tr>
                        <th scope="col">登録時刻</th>
                        <th scope="col">タイトル</th>
                        <th scope="col">商品説明</th>
                        <th scope="col">価格</th>             
                        <th scope="col"></th>           
                      </tr>
                    </thead>
                    <tbody>
                    <?php
                      if (empty($items)) { ?>
                    <tr>
                        <td colspan="4">データなし</td>
                    </tr>
                    <?php } ?>
                    <?php
                    $count = 0;
                    $index = 0;
                    foreach ($items as $item):?>
                    <?php 
                        $count++;
                        if (ceil(++$index / MAX_PAGE_COUNT) != $page) {
                            continue;
                        }
                        ?>
                        <tr>
                            <td scope="row"><?= $item->regist_date ?></th>
                            <td style="color:#71d100">
                                <a class="btn-edit" href="<?php echo HOME_URL; ?>/user/negotiation.php?item_id=<?php echo $item->id ?>&user_id=<?php echo $item->owner_id ?>"><?= $item->title ?>
                                </a>
                            </td>
                            <td><?= $item->remarks ?></td>
                            <td><?= number_format($item->price) ?>円</td>
                            <td>
                                <div class="transaction-info">
                                    <div>
                                        <form class="text-center m-auto" action="" method="post">
                                            <input class="btn btn-block btn-info" value="照合テスト" onclick="return openForm('modalFeedback-<?= $item->id?>')" type="button"/>
                                        </form>
                                    </div>
                                </div>
                                <?php
                                    $feedback1 = new Feedback();
                                    $feedback1->selectForIndex($item->id,getUserId(),1);
                                    $feedback2 = new Feedback();
                                    $feedback2->selectForIndex($item->id,getUserId(),2);
                                    $user = new User();
                                    $user->select(getUserId());
                                ?>
                                <div id="modalFeedback-<?= $item->id?>" class="modal fade show modalFeedback" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-modal="true" >            <div class="modal-dialog modal-lg">
                                <!-- Modal content-->
                                    <div class="modal-content  container" >
                                        <h3 class="font-weight-bold">真贋比較画像</h3>
                                        <button type="button" class="close" data-dismiss="modal" onclick="return closeForm('modalFeedback-<?= $item->id?>')" style="font-size: 25px;">×</button>
                                        <div class="show-img-refer row mt-5">
                                            <?php
                                                $item->select($item->id);
                                                if($item->groupObject != null){
                                                    $objectCode = json_decode($item->groupObject, true);
                                                }
                                            ?>
                                                <div class="col-md-6 text-center">
                                                    <?php if(@getimagesize($item->getImageAuthenPath(1)) != null): ?>
                                                        <a href="<?= $item->getImageAuthenUrl(1) ?>" target="_blank">
                                                            <img class="imagePreview" width="300" src="<?= $item->getImageAuthenUrl(1) ?>"/>
                                                        </a>
                                                        <p class="text-center mt-4">全体画像</p>
                                                        <?php if(@getimagesize($item->getImageAuthenPath(2)) == false && @getimagesize($item->getImageAuthenPath(3)) != false): ?>
                                                            <a href="<?= $item->getImageAuthenUrl(3) ?>" class="link-img-mark" target="_blank" data-toggle="tooltip" data-placement="bottom" title="特徴画像として登録した場所をマーカーで確認することができます">
                                                                商品マーカー画像を見<i class="fa fa-question-circle-o" style="font-size: 18px;padding-left: 5px;"></i>
                                                            </a>
                                                        <?php else: ?>
                                                            <a href="<?= $item->getImageAuthenUrl(3) ?>" class="link-img-mark" target="_blank" data-toggle="tooltip" data-placement="bottom" title=""></a>
                                                        <?php endif; ?>
                                                            <form action="" enctype="multipart/form-data" method="post" id="fr-Feedback1-<?= $item->id ?>" class="fr-feedback" >
                                                                <div class="" style="display:none">
                                                                    <input type="hidden" name="item_id" value="<?= $item->id ?>" />
                                                                    <input type="hidden" name="groupCode" value="<?= $item->groupCode ?>" />
                                                                    <input type="hidden" name="objectCode" value="<?= $objectCode["1"] ?>" />
                                                                    <input type="hidden" name="user_id" value="<?= getUserId() ?>" />
                                                                    <input type="hidden" name="order_id" value="<?= $item->order_id ?>"/>
                                                                    <input type="hidden" name="action" value="feedback" />
                                                                    <input type="hidden" name="state" value="5"/>
                                                                    <input type="hidden" name="index" value="1"/>
                                                                    <input type="hidden" name="token" value="<?= createToken(); ?>" />
                                                                </div>
                                                                <div class="transaction-file-upload" style="visibility:hidden">
                                                                    <div class="transaction-file-upload-item" style="width:80%; margin:auto">
                                                                        <div class="custom-file mb-3">
                                                                            <input type="file" onchange="test();" class="custom-file-input" id="photo-<?= $item->id ?>" name="photo" >
                                                                            <label class="custom-file-label" for="photo" id="file-photo" data-browse="参照">画像ファイル選択...</label>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                                <div class="w-75 m-auto text-center">
                                                                <button onclick="return Feedback1(<?= $item->id ?>)" type="button" class="btn-upFb btn-upFb1">商品画像アップロード</button>
                                                                </div>
                                                            </form>
                                                    <?php endif; ?>
                                                </div>
                                                
                                                <?php if(@getimagesize($item->getImageAuthenPath(2)) != false): ?>            
                                                    <div class="col-md-6 text-center">
                                                        <a href="<?= $item->getImageAuthenUrl(2) ?>" target="_blank">
                                                            <img class="imagePreview" width="300" src="<?= $item->getImageAuthenUrl(2) ?>"/>
                                                        </a>
                                                        <p class="text-center mt-4">全体画像</p>
                                                        <?php if(@getimagesize($item->getImageAuthenPath(3)) != false): ?>
                                                            <a href="<?= $item->getImageAuthenUrl(3) ?>" class="link-img-mark" target="_blank" data-toggle="tooltip" data-placement="bottom" title="特徴画像として登録した場所をマーカーで確認することができます">
                                                                商品マーカー画像を見<i class="fa fa-question-circle-o" style="font-size: 18px;padding-left: 5px;"></i></a>
                                                        <?php else: ?>
                                                            <a href="<?= $item->getImageAuthenUrl(3) ?>" class="link-img-mark" target="_blank" data-toggle="tooltip" data-placement="bottom" title=""></a>
                                                        <?php endif; ?>
                                                            <form action="" enctype="multipart/form-data" method="post" id="fr-Feedback2-<?= $item->id ?>" class="fr-feedback" >
                                                                <div class="" style="display:none">
                                                                    <input type="hidden" name="item_id" value="<?= $item->id ?>" />
                                                                    <input type="hidden" name="groupCode" value="<?= $item->groupCode ?>" />
                                                                    <input type="hidden" name="objectCode" value="<?= $objectCode["2"] ?>" />
                                                                    <input type="hidden" name="user_id" value="<?= getUserId() ?>" />
                                                                    <input type="hidden" name="order_id" value="<?= $item->order_id ?>"/>
                                                                    <input type="hidden" name="action" value="feedback" />
                                                                    <input type="hidden" name="state" value="5"/>
                                                                    <input type="hidden" name="index" value="2"/>
                                                                    <input type="hidden" name="token" value="<?= createToken(); ?>" />
                                                                </div>
                                                                <div class="transaction-file-upload" style="visibility:hidden">
                                                                    <div class="transaction-file-upload-item" style="width:80%; margin:auto">
                                                                        <div class="custom-file mb-3">
                                                                            <input type="file" class="custom-file-input" id="photo2-<?= $item->id ?>" name="photo2" >
                                                                            <label class="custom-file-label" for="photo" id="file-photo2" data-browse="参照">画像ファイル選択...</label>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                                <div class="w-75 m-auto text-center">
                                                                <button class="btn-upFb btn-upFb2"  onclick="return Feedback2(<?= $item->id ?>)" type="button">商品画像アップロード</button>
                                                                </div> 
                                                            </form>
                                                    </div>
                                                <?php endif; ?>
                                        </div>
                                            
                                            <div class="mt-5">
                                                    <h3 class="font-weight-bold">アップロード画像</h3>
                                                    <div class="show-img-refer row mt-5">
                                                        <div class="col-md-6 text-center one-component" >
                                                        <?php if(@getimagesize(($feedback1->getImagePath())) != false):?>
                                                            <a href="<?= $feedback1->getImageUrl() ?>" target="_blank">
                                                                <img class="imagePreview img-preview1" width="300" src="<?= $feedback1->getImageUrl() ?>"/>
                                                            </a>
                                                            <p class="text-center mt-4">全体画像</p>
                                                            <p class="text-center mt-4"><?= $feedback1->regist_date ?></p>
                                                            <?php if($feedback1->state == 1): ?>
                                                                <p class="text-center mt-4">真贋判定 
                                                                    <?php if($feedback1->score <= 1 && $feedback1->score >= 0.5) :?>
                                                                        <span class="typeScore">正規品</span>
                                                                    <?php elseif ($feedback1->score <0.5 && $feedback1->score >0.3): ?>
                                                                        <span class="typeScore">真贋不明</span>
                                                                    <?php else: ?>
                                                                        <span class="typeScore">模造品</span>
                                                                    <?php endif;?>
                                                                </p>
                                                                <p class="text-center mt-4">画像マッチ度 <?= round(($feedback1->score)*100,1) ?>%</p>
                                                            <?php endif;?>

                                                                <?php if($feedback1->state == 0) : ?>
                                                                    <form action=""  method="post" class="">
                                                                        <div class="" style="display:none">
                                                                            <input type="hidden" name="user_id" value="<?= getUserId() ?>" />
                                                                            <input type="hidden" name="feedback_id" value="<?= $feedback1->id ?>"/>
                                                                            <input type="hidden" name="action" value="upStateFb" />
                                                                            <input type="hidden" name="state" value="1"/>
                                                                            <input type="hidden" name="token" value="<?= createToken(); ?>" />

                                                                        </div>
                                                                        <div class="w-75 m-auto text-center">
                                                                            <button class="btn-upFb"  type="submit">真贋判定</button>
                                                                        </div>
                                                                    </form>
                                                                <?php endif; ?>
                                                                <form action="" enctype="multipart/form-data" method="post" id="fr-reUpFeedback1-<?= $item->id ?>" class="fr-feedback">
                                                                    <div class="" style="display:none">
                                                                        <input type="hidden" name="item_id" value="<?= $item->id ?>" />
                                                                        <input type="hidden" name="groupCode" value="<?= $item->groupCode ?>" />
                                                                        <input type="hidden" name="objectCode" value="<?= $objectCode["1"] ?>" />
                                                                        <input type="hidden" name="user_id" value="<?= getUserId() ?>" />
                                                                        <input type="hidden" name="order_id" value="<?= $item->order_id ?>"/>
                                                                        <input type="hidden" name="action" value="re-feedback" />
                                                                        <input type="hidden" name="state" value="5"/>
                                                                        <input type="hidden" name="index" value="1"/>
                                                                        <input type="hidden" name="token" value="<?= createToken(); ?>" />
                                                                    </div>
                                                                    <div class="transaction-file-upload" style="visibility:hidden">
                                                                        <div class="transaction-file-upload-item" style="width:80%; margin:auto">
                                                                            <div class="custom-file mb-3">
                                                                                <input type="file" class="custom-file-input" id="photoRe1-<?= $item->id ?>" name="photoRe1" >
                                                                                <label class="custom-file-label" for="photoRe1" id="photoRe1" data-browse="参照">画像ファイル選択...</label>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                    <div class="w-75 m-auto text-center">
                                                                        <button class="btn-upFb" onclick="return reUpFeedback1(<?= $item->id ?>)" type="button">認証に利用する画像を変更する</button>
                                                                    </div>
                                                                </form>
                                                            <?php endif; ?>
                                                        </div>
                                                        <?php if(@getimagesize(($feedback2->getImagePath())) != false):?>
                                                            <div class="col-md-6 text-center">
                                                                <a href="<?= $feedback2->getImageUrl() ?>" target="_blank">
                                                                    <img class="imagePreview img-preview2" width="300" src="<?= $feedback2->getImageUrl() ?>"/>
                                                                </a>
                                                                <p class="text-center mt-4">全体画像</p>
                                                                <p class="text-center mt-4"><?= $feedback2->regist_date ?></p>
                                                                <?php if($feedback2->state == 1) : ?>
                                                                    <p class="text-center mt-4">真贋判定 
                                                                        <?php if($feedback2->score <= 1 && $feedback2->score >= 0.5) :?>
                                                                            <span class="typeScore">正規品</span>
                                                                        <?php elseif ($feedback2->score <0.5 && $feedback2->score >0.3): ?>
                                                                            <span class="typeScore">真贋不明</span>
                                                                        <?php else: ?>
                                                                            <span class="typeScore">模造品</span>
                                                                        <?php endif;?>
                                                                    </p>
                                                                    <p class="text-center mt-4">画像マッチ度 <?= round(($feedback2->score)*100,1) ?>%</p>
                                                                <?php endif;?>
                                                                <?php if($feedback2->state == 0) : ?>
                                                                        <form action=""  method="post" >
                                                                            <div class="" style="display:none">
                                                                                <input type="hidden" name="user_id" value="<?= getUserId() ?>" />
                                                                                <input type="hidden" name="feedback_id" value="<?= $feedback2->id ?>"/>
                                                                                <input type="hidden" name="action" value="upStateFb" />
                                                                                <input type="hidden" name="state" value="1"/>
                                                                                <input type="hidden" name="token" value="<?= createToken(); ?>" />
                                                                            </div>
                                                                            
                                                                            <div class="w-75 m-auto text-center">
                                                                                <button class="btn-upFb"  type="submit">真贋判定</button>
                                                                            </div>
                                                                        </form>
                                                                    <?php endif; ?>
                                                                <form action="" enctype="multipart/form-data" method="post" id="fr-reUpFeedback2-<?= $item->id ?>" class="fr-feedback">
                                                                    <div class="" style="display:none">
                                                                        <input type="hidden" name="item_id" value="<?= $item->id ?>" />
                                                                        <input type="hidden" name="groupCode" value="<?= $item->groupCode ?>" />
                                                                        <input type="hidden" name="objectCode" value="<?= $objectCode["2"] ?>" />
                                                                        <input type="hidden" name="user_id" value="<?= getUserId() ?>" />
                                                                        <input type="hidden" name="order_id" value="<?= $item->order_id ?>"/>
                                                                        <input type="hidden" name="action" value="re-feedback" />
                                                                        <input type="hidden" name="state" value="5"/>
                                                                        <input type="hidden" name="index" value="2"/>
                                                                        <input type="hidden" name="token" value="<?= createToken(); ?>" />
                                                                    </div>
                                                                    <div class="transaction-file-upload" style="visibility:hidden">
                                                                        <div class="transaction-file-upload-item" style="width:80%; margin:auto">
                                                                            <div class="custom-file mb-3">
                                                                                <input type="file" class="custom-file-input" id="photoRe2-<?= $item->id ?>" name="photoRe2" >
                                                                                <label class="custom-file-label" for="photoRe2" id="photoRe2" data-browse="参照">画像ファイル選択...</label>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                    <div class="w-75 m-auto text-center">
                                                                        <button class="btn-upFb" onclick="return reUpFeedback2(<?= $item->id ?>)" type="button">認証に利用する画像を変更する</button>
                                                                    </div>
                                                                </form>
                                                            </div>
                                                        <?php endif; ?>
                                                    </div>
                                            </div>
                                        <div class="mt-5">
                                            <p>記に登録されている画像と、同じ箇所を撮影してください。<br />
                                            アップロード後、真贋判定ボタンを押して下さい。<br />
                                            同じ商品と判断された場合「正規品」と判定され、決済処理が確定いたします。<br />
                                            「模造品」と判定された場合は、決済処理が落ちません。<br />
                                            （再度判定処理を行うことは可能です）<br />
                                            何度も「模造品」判断される場合は、お問い合わせよりご連絡くださいませ。<br />
                                            </p>
                                        </div>
                                    </div>
                                </div>
                            </div> 
                        </td>
                    </tr>
                <?php endforeach; ?>
                </tbody>
            </table>
        </div>

                <?php
                    $prev_page = $page - 1;
                    $max_page = ceil($count / MAX_PAGE_COUNT);
                    $next_page = min($max_page, $page + 1);
                    $view_start_page = max(1, $page - ceil(VIEW_PAGE_SELECT_COUNT / 2));
                    $view_last_page = min($view_start_page + VIEW_PAGE_SELECT_COUNT - 1, $max_page);
                    if ($view_last_page - $view_start_page < VIEW_PAGE_SELECT_COUNT) {
                        $view_start_page = max(1, $view_last_page - VIEW_PAGE_SELECT_COUNT + 1);
                    }
                    ?>
                <?php if($max_page >1): ?>
                    <div class="content-group-pagination">
                <?php else: ?>
                    <div class="content-group-pagination content-one-page">
                <?php endif; ?>
                
                <?php if($max_page > 1):  ?>
                    <?php if ($prev_page > 0): ?>
                        <a class="pagination__preve" href=""
                            onclick="submitForm('fm-param', 'page', '<?= $prev_page ?>'); return false;"><i class="bi bi-chevron-left"></i></a>
                    <?php else: ?>
                        <a class="pagination__preve" href="" onclick="return false;"><i class="bi bi-chevron-left"></i></a>
                    <?php endif; ?>
                <?php endif; ?>

                <?php for ($i = $view_start_page; $i <= $view_last_page; $i++): ?>
                    <?php if ($page != $i): ?>
                        <a class="pagination__num" href=""
                               onclick="submitForm('fm-param', 'page', '<?= $i ?>'); return false;"><?= $i ?></a>
                    <?php else: ?>
                        <a class="pagination__num pag_active" href="" onclick="return false;"><span><?= $i ?></span></a>
                    <?php endif; ?>
                <?php endfor; ?>
                <?php if($max_page > 1):  ?>
                    <?php if ($page < $max_page): ?>
                        <a id="pagination__next" class="pagination__preve" href=""
                            onclick="submitForm('fm-param', 'page', '<?= $next_page ?>'); return false;"><i class="bi bi-chevron-right"></i></a>
                    <?php else: ?>
                        <a id="pagination__next" class="pagination__preve" href="" onclick="return false;"><i class="bi bi-chevron-right"></i></a>
                    <?php endif; ?>
                <?php endif; ?>
            </div>
        </div>
    </div>

<?php include('../footer.php'); ?>
<script>
    window.onload = function() {
        if (sessionStorage.getItem("showModal") == 'true'){
            openForm(sessionStorage.getItem("modalId"));
        }
    };
</script>
