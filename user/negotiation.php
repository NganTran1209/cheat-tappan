<?php include_once(__DIR__ . '/../common/util.php'); ?>
 <?php date_default_timezone_set('Asia/Tokyo'); ?>
<?php
if (!isLogin()) {
    setMessage('不正なアクセスです。');
    header('Location: ' . getContextRoot() . '/');
    exit;
}

$check = false;
$uploadCheck1 = false;
$uploadCheck2 = false;

$item = new Item();
$item->select($_GET['item_id']);
if ($item->enabled == 9) {
    setMessage('出品が取り消されました。');
    header('Location: ' . getContextRoot() . '/');
    exit;
} elseif ($item->owner_id == getUserId()) {
    if (BlockAccess::isBlock($_GET['user_id'])) {
        setMessage('このユーザーと取引はできません。', true);
        header('Location: ' . getContextRoot() . '/');
        exit;
    }
} else {
    if ($item->isBlock()) {
        setMessage('このユーザーと取引はできません。', true);
        header('Location: ' . getContextRoot() . '/');
        exit;
    }
}

$negotiation = new Negotiation();
$order = new Order();
$order->select($item->order_id);

$system_config = SystemConfig::select();
$feedback = new Feedback();
$sizeLimit = 1024;
if (isset($_POST['action'])) {
    if ($_POST['action'] == 'buy') {
        $order = new Order();
        $order->item_id = $_POST['item_id'];
        $order->user_id = getUserId();
        $order->state = 0;

        $order->regist();
        $item->select($order->item_id);
        setMessage('購入しました。</br>送付先やお支払のやり取りを開始してください。 ');
    } elseif ($_POST['action'] == 'comment') {
        if (!empty($_POST['comment'])
            || (!empty($_FILES['photo']) && $_FILES['photo']['error'] == 0 && isImage($_FILES['photo']['tmp_name']))
            || (!empty($_FILES['pdf']) && $_FILES['pdf']['error'] == 0 && $_FILES["pdf"]["type"] == "application/pdf")) {

            $negotiation->comment = empty($_POST['comment']) ? '' : $_POST['comment'];
            $negotiation->kbn = $_POST['kbn'];
            $negotiation->item_id = $_POST['item_id'];
            $negotiation->user_id = $_POST['user_id'];
            $negotiation->regist_date = date_format(date_create('',timezone_open('Asia/Tokyo')),'Y-m-d H:i:s');
            if (!empty($_FILES['photo']) && $_FILES['photo']['error'] == 0) {
                $negotiation->photo = rotateImage(file_get_contents($_FILES['photo']['tmp_name']));
            } else {
                $negotiation->photo = null;
            }

            if (!empty($_FILES['pdf']) && $_FILES['pdf']['error'] == 0) {
                $negotiation->pdf = file_get_contents($_FILES['pdf']['tmp_name']);
            } else {
                $negotiation->pdf = null;
            }

            if (checkToken()) {
                $negotiation->regist();
                setMessage('コメントを送信しました。');
            } else {
                setMessage('重複投稿をブロックしました。');
            }
        } else {
            setMessage('コメントまたは画像( jpg,jpeg,png,gif )、PDFファイルを指定してください。');
        }
    } elseif ($_POST['action'] == 'blockAccess') {
        $block = new BlockAccess();
        $block->user_id = getUserId();
        $block->block_user_id = $_POST['user_id'];
        $block->regist();
        setMessage('対象のユーザーをブロックしました。');
        header('Location: ' . getContextRoot() . '/');
        exit();
    } elseif ($_POST['action'] == 'procedure') {
        $order->id = $_POST['order_id'];
        if ($order->state >= $_POST['state']) {
            setMessage('既にご連絡済みです。');
        } else {
            $order->state = $_POST['state'];
            $order->updateProcedure();
            switch ($order->state) {
                case 1:
                    setMessage('お振込み完了のご連絡をしました。');
                    break;
                case 2:
                    setMessage('納品のご連絡をしました。');
                    break;
                case 3:
                    setMessage('受取のご連絡をしました。');
                    break;
                case 4:
                    setMessage('フィードバック画像をアップロードする.');    
                    break;
                case 6:
                    setMessage('受取のご連絡をしました。');
                    // $order->updateCashFlowReceipt();
                    break;
                default:
                    setMessage('不正なアクセスです。', true);
                    header('Location: ' . getContextRoot() . '/');
                    exit;
            }

        }
    } elseif ($_POST['action'] == 'bank-transfer') {
        if ($order->state >= $_POST['state']) {
            setMessage('既にご連絡済みです。');
        } else {
            $order->updateOrderForBankTransferEntry();
            $cash_flow = new CashFlow();
            $cash_flow->user_id = $order->owner->id;
            $cash_flow->amount = $order->item->price;
            $cash_flow->order_id = $order->id;
            $cash_flow->regist();
            setMessage('振込完了を連絡しました。');
        } 

    } elseif ($_POST['action'] == 'feedback') {
        if ((!empty($_FILES['photo']) && $_FILES['photo']['error'] == 0 && isImage($_FILES['photo']['tmp_name'])) || (!empty($_FILES['photo2']) && $_FILES['photo2']['error'] == 0 && isImage($_FILES['photo2']['tmp_name']))) {
            if($_POST['index'] == 1){
                $feedback->item_id = $_POST['item_id'];
                $feedback->user_id = $_POST['user_id'];
                $feedback->order_id = $_POST['order_id'];
                $feedback->index = $_POST['index'];
                $_FILES['photo'] = changeFileType($_FILES['photo']);
                if (getimagesize($_FILES['photo']['tmp_name'])[1] > $sizeLimit){
                    compress($_FILES['photo']['tmp_name'], 100);
                }
                if (!empty($_FILES['photo']) && $_FILES['photo']['error'] == 0) {
                    $feedback->photo = rotateImage(file_get_contents($_FILES['photo']['tmp_name']));
                } else {
                    $feedback->photo = null;
                }

                $img = curl_file_create($_FILES['photo']['tmp_name'], $_FILES['photo']['type'], basename($_FILES['photo']['name']));
                // $check = $feedback->compareRequestGaziru($_POST['groupCode'], $img);
                $check = $feedback->compareRequestGaziruWithObject($_POST['objectCode'], $img);
                $max_score = json_decode($check,true)["scores"][0]["score"];
                foreach (json_decode($check,true)["scores"] as &$value) {
                    if($value["score"] > $max_score){
                        $max_score = $feedback->score ;
                    }
                }
                $feedback->score =  $max_score;
                $feedback->regist();

                $feedback->state = 1;
                $feedback->updateSate();
               
            }
            if($_POST['index'] == 2){
                $feedback->item_id = $_POST['item_id'];
                $feedback->user_id = $_POST['user_id'];
                $feedback->order_id = $_POST['order_id'];
                $feedback->index = $_POST['index'];
                $_FILES['photo2'] = changeFileType($_FILES['photo2']);
                if (getimagesize($_FILES['photo2']['tmp_name'])[1] > $sizeLimit){
                    compress($_FILES['photo2']['tmp_name'], 100);
                }
                if (!empty($_FILES['photo2']) && $_FILES['photo2']['error'] == 0) {
                    $feedback->photo = rotateImage(file_get_contents($_FILES['photo2']['tmp_name']));
                } else {
                    $feedback->photo = null;
                }
                $img = curl_file_create($_FILES['photo2']['tmp_name'], $_FILES['photo2']['type'], basename($_FILES['photo2']['name']));
                // $check = $feedback->compareRequestGaziru($_POST['groupCode'], $img);
                $check = $feedback->compareRequestGaziruWithObject($_POST['objectCode'], $img);
                $max_score = json_decode($check,true)["scores"][0]["score"];
                foreach (json_decode($check,true)["scores"] as &$value) {
                    if($value["score"] > $max_score){
                        $max_score = $feedback->score ;
                    }
                }
                $feedback->score =  $max_score;
                $feedback->regist();

                $feedback->state = 1;
                $feedback->updateSate();
            }
            
            setMessage('真贋比較画像とアップロードした画像がそれぞれ表示されるようにしてください。');
            
            $order->state = $_POST['state'];
            $order->updateProcedure();            
            $check = true;
        } else {
            setMessage('コメントまたは画像( jpg,jpeg,png,gif )。');
        }
    } elseif ($_POST['action'] == 're-feedback') {
        if((!empty($_FILES['photoRe1']) && $_FILES['photoRe1']['error'] == 0 && isImage($_FILES['photoRe1']['tmp_name'])) || (!empty($_FILES['photoRe2']) && $_FILES['photoRe2']['error'] == 0 && isImage($_FILES['photoRe2']['tmp_name']))){

            if ((!empty($_FILES['photoRe1']) && $_FILES['photoRe1']['error'] == 0 && isImage($_FILES['photoRe1']['tmp_name']))) {

                $feedback->item_id = $_POST['item_id'];
                $feedback->user_id = $_POST['user_id'];
                $feedback->order_id = $_POST['order_id'];
                $feedback->index = $_POST['index'];
                $_FILES['photoRe1'] = changeFileType($_FILES['photoRe1']);
                if (getimagesize($_FILES['photoRe1']['tmp_name'])[1] > $sizeLimit){
                    compress($_FILES['photoRe1']['tmp_name'], 100);
                }
                if (!empty($_FILES['photoRe1']) && $_FILES['photoRe1']['error'] == 0) {
                    $feedback->photo = rotateImage(file_get_contents($_FILES['photoRe1']['tmp_name']));
                } else {
                    $feedback->photo = null;
                }
                $img = curl_file_create($_FILES['photoRe1']['tmp_name'], $_FILES['photoRe1']['type'], basename($_FILES['photoRe1']['name']));
                // $check = $feedback->compareRequestGaziru($_POST['groupCode'], $img);
                $check = $feedback->compareRequestGaziruWithObject($_POST['objectCode'], $img);
                $max_score = json_decode($check,true)["scores"][0]["score"];
                foreach (json_decode($check,true)["scores"] as &$value) {
                    if($value["score"] > $max_score){
                        $max_score = $feedback->score ;
                    }
                }
                $feedback->score =  $max_score;
                $feedback->regist();
                
                setMessage('真贋比較画像とアップロードした画像がそれぞれ表示されるようにしてください。');
                $check = true;
            } 
            if ((!empty($_FILES['photoRe2']) && $_FILES['photoRe2']['error'] == 0 && isImage($_FILES['photoRe2']['tmp_name']))) {

                $feedback->item_id = $_POST['item_id'];
                $feedback->user_id = $_POST['user_id'];
                $feedback->order_id = $_POST['order_id'];
                $feedback->index = $_POST['index'];
                $_FILES['photoRe2'] = changeFileType($_FILES['photoRe2']);
                if (getimagesize($_FILES['photoRe2']['tmp_name'])[1] > $sizeLimit){
                    compress($_FILES['photoRe2']['tmp_name'], 100);
                }
                if (!empty($_FILES['photoRe2']) && $_FILES['photoRe2']['error'] == 0) {
                    $feedback->photo = rotateImage(file_get_contents($_FILES['photoRe2']['tmp_name']));
                } else {
                    $feedback->photo = null;
                }
                $img = curl_file_create($_FILES['photoRe2']['tmp_name'], $_FILES['photoRe2']['type'], basename($_FILES['photoRe2']['name']));
                // $check = $feedback->compareRequestGaziru($_POST['groupCode'], $img);
                $check = $feedback->compareRequestGaziruWithObject($_POST['objectCode'], $img);
                $max_score = json_decode($check,true)["scores"][0]["score"];
                foreach (json_decode($check,true)["scores"] as &$value) {
                    if($value["score"] > $max_score){
                        $max_score = $feedback->score ;
                    }
                }
                $feedback->score =  $max_score;
                $feedback->regist();
                
                setMessage('真贋比較画像とアップロードした画像がそれぞれ表示されるようにしてください。');
                $check = true;
            } 
        } else {
            setMessage('コメントまたは画像( jpg,jpeg,png,gif )。');
        } 
    } elseif($_POST['action'] == 'upStateFb'){
        $feedback->id = $_POST['feedback_id'];
        $feedback->state = $_POST['state'];
        $feedback->updateSate();
        $check = true;
    }
}
function compress($source, $quality) {
    $info = getimagesize($source);
    $width = $new_width = $info[0];
    $height = $new_height = $info[1];
    if ($info['mime'] == 'image/jpeg') 
        $image = imagecreatefromjpeg($source);
    elseif ($info['mime'] == 'image/gif') 
        $image = imagecreatefromgif($source);
    elseif ($info['mime'] == 'image/png') 
        $image = imagecreatefrompng($source);
    else 
        $image = imagecreatefromjpeg($source);
    
    // $new_width = 1024;
    // $new_height = 1024;
    // $new_image = imagecreatetruecolor($new_width, $new_height);
    // imagecopyresampled($new_image, $image, 0, 0, 0, 0, $new_width, $new_height, $width, $height);  

    $exif = exif_read_data($source);
    if(!empty($exif['Orientation'])) {
        switch($exif['Orientation']) {
            case 8:
                $image = imagerotate($image,90,0);
                break;
            case 3:
                $image = imagerotate($image,180,0);
                break;
            case 6:
                $image = imagerotate($image,-90,0);
                break;
        } 
    }
    imagejpeg($image, $source, $quality);
}

function changeFileType($file){
    $info = getimagesize($file['tmp_name']);
    if ($info['mime'] == 'image/jpeg') 
        $image = imagecreatefromjpeg($file['tmp_name']);
    elseif ($info['mime'] == 'image/gif') 
        $image = imagecreatefromgif($file['tmp_name']);
    elseif ($info['mime'] == 'image/png') 
        $image = imagecreatefrompng($file['tmp_name']);
    else 
        $image = imagecreatefromjpeg($file['tmp_name']);
    imagejpeg($image,$file['tmp_name'], 100);
    $file['name'] = pathinfo($file['name'])['filename'] . '.jpg';
    $file['type'] = 'image/jpeg';
    return $file;
}

$item->select($_GET['item_id']);
$order->select($item->order_id);

if ($item->enabled == 9) {
    setMessage('出品が取り消されました。');
    header('Location: ' . getContextRoot() . '/');
    exit;
} elseif ($item->owner_id == getUserId()) {
    if (BlockAccess::isBlock($_GET['user_id'])) {
        setMessage('このユーザーと取引はできません。', true);
        header('Location: ' . getContextRoot() . '/');
        exit;
    }
} else {
    if ($item->isBlock()) {
        setMessage('このユーザーと取引はできません。', true);
        header('Location: ' . getContextRoot() . '/');
        exit;
    }
}


$negotiation->item_id = $item->id;
$negotiation->user_id = $_GET['user_id'];
list($before, $after) = $negotiation->select3();

$user_evaluate = new Evaluate();
$user_evaluate->selectSummary($_GET['user_id']);

$owner = new User();
$owner->select($item->owner_id);
if ($owner->id != getUserId() && $_GET['user_id'] != getUserId()) {
    setMessage('不正なアクセスです。');
    header('Location: ' . getContextRoot() . '/');
    exit;
}
$view_evaluate = true;
//$noimagepng = "http://192.168.55.10/ccsystem/images/noimage.png";
$title_page = 'レディース新着アイテム';
$user_evaluate = new Evaluate();
$user_evaluate->selectSummary(getUserId());

$view_favorites_button = true;
$view_favorites_count = true;
$view_other_item_link = true;
$view_evaluate = true;
if (!empty($_GET['new_title'])) {
    $category_title = $_GET['new_title'];
}else{
    $category_title = selectCodeName('selectCategoryById', $item->category);
}
$title_page = $category_title;
$img_flow_active=getContextRoot().'/common/assets/img/common/flow-active.png';
$img_step1 = getContextRoot().'/common/assets/img/common/flow-next.png';
$img_step2 = getContextRoot().'/common/assets/img/common/flow-next-2.png';
?>
<?php include('../user_header.php'); ?>
<style>
    .ladies-product-btn form {
        display: inline-block;
    }

    .ladies-product-btn .negotiation button {
        background-color: #7dc02d;
    }

    .ladies-product-btn .buy-form {
        margin-left: 3rem;
    }

    .ladies-product-btn .buy-form button {
        background-color: #ff7800;
        padding: 10px 8rem;
    }

    .transaction-btn form {
        width: 48%;
    }

    .transaction-btn form button {
        background-color: #ff7800;
    }

    .transaction-btn form button.buy_by_bank_transfer {
        background-color: #72d102 !important;
    }
    .btn-feedback{
        background-color: #ff7800;
        border-color: #ff7800;
    }
    .btn-feedback:hover {
        background-color: initial;
        border-color: #ff7800;
        color: #ff7800;
    }
    .form-popup {
        display: none;
        position: fixed;
        bottom: 0;
        right: 15px;
        border: 3px solid #f1f1f1;
        z-index: 9;
    }
    .md-show{
        display: block;
    }
}
</style>
<script>

    $(document).ready(function(){
        $('[data-toggle="tooltip"]').tooltip();   
    });

    function changeFeedback(){
        $('#photo1').on('change', function () {
            console.log(this.files);
            if(this.files.length > 0){
                $('#fr-reUpFeedback').submit();
            }
        }).click();
    }
    function openForm(id) {
        document.getElementById(id).style.display = "block";
    }

    function closeForm(id) {
        document.getElementById(id).style.display = "none";
    }

    function Feedback1(){
        // $('#photo').on('change', function () {
        //     console.log(this.files);
        //     if(this.files.length > 0){
            $('#fr-Feedback1').submit();
        //     }
        // }).click();
        
    }
    function Feedback2(){
    
        $('#photo2').on('change', function () {
            console.log(this.files);
            if(this.files.length > 0){
                $('#fr-Feedback2').submit();
            }
        }).click();
    }
    function reUpFeedback1(){
        $('#photoRe1').on('change', function () {
            console.log(this.files);
            if(this.files.length > 0){
                $('#fr-reUpFeedback1').submit();
            }
        }).click();
    
    }
    function reUpFeedback2(){
        $('#photoRe2').on('change', function () {
            console.log(this.files);
            if(this.files.length > 0){
                $('#fr-reUpFeedback2').submit();
            }
        }).click();
    }
</script>
<div class="com-header-top">
    <div class="com-header-top__img">
        <img src="<?php echo HOME_URL; ?>/common/assets/img/common/other-com-header.png" alt="">
    </div>
    <div class="com-header-top__path bg-other-01">
        <p><span><a href="<?php echo HOME_URL; ?>/" class="clr-yel">トップページ</a></span><span> > </span><span><?= $category_title ?>新着アイテム</span></p>
    </div>
<!--    <div class="com-header-top__txt">
        <p>レディース新着アイテム</p>
    </div>-->
</div>
<div class="customer-container">
<div class="category-title  wow animate__animated animate__fadeInUp">
                <h3><span><?= $category_title ?>新着アイテム</span></h3>
            </div>
    <div class="customer-contact-form">
        <div class="bg-inner">
            <div class="content-title">
                <h3><span><?= $item->title ?></span></h3>
            </div>
            <div class="ladies-product-list-content">
                <?php include_once(__DIR__ . '/../common/parts/item_image_area.php'); ?>
                <?php include_once(__DIR__ . '/../common/parts/item_info_area.php'); ?>
            </div>
            <div class="ladies-product-price">
                <div class="ladies-product-price__txt">
                    <p><span>¥<?= number_format($item->price) ?> 円</span>（税込/<?= $item->carriage_plan_name ?>）</p>
                </div>
                <?php if (!isLogin()): ?>
                <div class="ladies-product-btn">
                    <a class="btn btn-block btn-sm  btn-info"
                        href="<?php echo HOME_URL; ?>/user/userentry.php?step=1">無料会員登録</a>
                    <div class="text-center fontBold">無料会員登録すると購入できます。</div>
                </div>
                <?php elseif (getUserId() != $owner->id) : ?>
                <?php if ($item->order_id == null || $item->buyer_id == getUserId()): ?>
                <form method="post" class="buy-form" onsubmit="return confirm('購入します。'); ">
                    <input type="hidden" name="item_id" value="<?= $item->id ?>" />
                    <input type="hidden" name="action" value="buy" />
                    <?php if ($item->order_id == null): ?>
                    <button class="btn btn-block btn-outline-danger btn-sm btn-custom" type="submit">購入する</button>
                    <?php else: ?>
                    <button class="btn btn-block btn-danger btn-sm btn-custom" type="submit"
                        disabled="disabled">購入済</button>
                    <?php endif; ?>
                </form>
                <div class="ladies-product-order-warning ">
                    <p class="c-small">※購入後のキャンセルや返品はできません。</p>
                </div>
                <?php endif; ?>
                <?php else: ?><?php endif; ?>
            </div>
            <div class="mt-5">
                <h3 class="font-weight-bold">この商品について</h3>
            </div>
            <div class="ladies-product-content mt-0">
                <p style="word-break: break-word;"><?php echo nl2br(htmlentities($item->remarks)); ?></p>
            </div>
            <?php if ($item->order_id != null && ($item->buyer_id == getUserId() || $item->owner_id == getUserId())): ?>
            <div class="seller-info">
                <div class="seller-info-item">
                    <div class="seller-info-item__lead">
                        <p>出品者情報</p>
                    </div>
                    <div class="product-list-content__right-item">
                        <div class="product-list-content__right-item-title">
                            <p>ID</p>
                        </div>
                        <div class="product-list-content__right-item-txt">
                            <p><?= $owner->hash_id ?></p>
                        </div>
                    </div>
                    <div class="product-list-content__right-item">
                        <div class="product-list-content__right-item-title">
                            <p>ニックネーム</p>
                        </div>
                        <div class="product-list-content__right-item-txt">
                            <p><?= $owner->nickname ?></p>
                        </div>
                    </div>
                </div>
                <div class="seller-info-item">
                    <?php
                                $buyer = new User();
                                $buyer->select($item->buyer_id);
                            ?>
                    <div class="seller-info-item__lead">
                        <p>購入者情報</p>
                    </div>
                    <div class="product-list-content__right-item">
                        <div class="product-list-content__right-item-title">
                            <p>ID</p>
                        </div>
                        <div class="product-list-content__right-item-txt">
                            <p><?= $buyer->hash_id ?></p>
                        </div>
                    </div>
                    <div class="product-list-content__right-item">
                        <div class="product-list-content__right-item-title">
                            <p>ニックネーム</p>
                        </div>
                        <div class="product-list-content__right-item-txt">
                            <p><?= $buyer->nickname ?></p>
                        </div>
                    </div>

                    <div class="not-payment-btn">
                        <?php if ($order->cash_flow == 2): ?>
                        <?php if ($order->user_id == getUserId()): ?>
                        <button type="button">支払済</button>
                        <?php else:?>
                        <button type="button">入金済</button>
                        <?php endif;?>
                        <?php elseif ($order->cash_flow == 1): ?>
                        <button type="button">入金処理中</button>
                        <?php else:?>
                        <button type="button">未入金</button>
                        <?php endif; ?>
                    </div>
                </div>
            </div>
            <div class="transaction-info">
                <div class="transaction-info__lead">
                    <p><span>取引の流れ</span></p>
                </div>
                <div class="transaction-info-flow">
                    <div  class="transaction-flow <?= ($order->state == 2 || $order->state == 6 ) ? 'change-background' : '' ?>">
                        <div class="flow-step step-1 <?= ($order->state == 0 && $order->regist_date == $order->update_date) ? 'active' : '' ?>">
                        <img src="<?= ($order->state == 0 && $order->regist_date == $order->update_date) ? $img_flow_active : $img_step1 ?>" />
                        <p>取引情報</p>
                        </div>
                        <div class="flow-step step-2 <?= (($order->state == 0 && $order->regist_date != $order->update_date) || ($order->state == 1 && $order->payment == 0)) ? 'active' : '' ?>">
                        <img src="<?= (($order->state == 0 && $order->regist_date != $order->update_date) || ($order->state == 1 && $order->payment == 0)) ? $img_flow_active : (($order->state == 0 && $order->regist_date == $order->update_date) ? $img_step1 : $img_step2) ?>" />
                        <p>お支払い</p>
                        </div>
                        <div class="flow-step step-3 <?= ($order->state == 1 && $order->payment == 1) ? 'active' : '' ?>">
                        <img src="<?= ($order->state == 1 && $order->payment == 1) ? $img_flow_active : (($order->state == 2 || $order->state == 6) ? $img_step1 : $img_step2) ?>" /> <p>納品連絡</p>
                        </div>
                        <div class="flow-step flow-step4 <?= ($order->state == 2 || $order->state == 6 ) ? 'active' : '' ?>">
                        <p>受け取り連絡</p>
                        <div>
                    </div>
                </div>
            </div>
            <?php if ($order->state == 0 && $order->user_id == getUserId()): ?>
                <div class="transaction-info-flow__bottom">
                    <p>代金のお振込完了後、必ず下の<span>「お振込完了のご連絡」</span>ボタンを押して出品者に通知してください。</p>
                </div>
            <?php endif; ?>
            <div class="transaction-btn">
                <?php if ($order->state == 0 && $order->user_id == getUserId()): ?>
                <?php if ($order->cash_flow == 1 && $order->payment == 1): ?>
                <form method="post" class="mb-0">
                    <input type="hidden" name="order_id" value="<?= $item->order_id ?>" />
                    <input type="hidden" name="state" value="1" />
                    <input type="hidden" name="action" value="bank-transfer" />
                    <button type="submit" class="w-100">お振込完了のご連絡</button>
                </form>
                <?php elseif ($order->cash_flow == 0):?>
                <form action="bank_transfer.php" method="post" onsubmit="return confirm('購入金額を銀行振り込みでお支払いします。');"
                    class="mb-0">
                    <input type="hidden" name="item_id" value="<?= $item->id ?>" />
                    <button class="buy_by_bank_transfer w-100" type="submit" class="w-100">銀行振り込みで購入する</button>
                </form>
                <?php if ($system_config->use_stripe): ?>
                <form action="card_payment.php" method="post" onsubmit="return confirm('購入金額をクレジットカードでお支払いします。');"
                    class="mb-0">
                    <input type="hidden" name="item_id" value="<?= $item->id ?>" />
                    <button type="submit" class="w-100">クレジットカードで購入する</button>
                </form>
                <?php endif; ?>
                <?php endif; ?>
                <?php endif; ?>
            </div>
            <?php endif; ?>
            <?php if ($order->state == 1 && $item->owner_id == getUserId()): ?>
                <?php if ($order->cash_flow == 2): ?>
                <div class="transaction-info">
                    <div class="transaction-info-flow__bottom">
                        <p>配送手配後、必ず下の<span>「納品連絡」ボタンを押して</span>購入者に通知してください。</p>
                    </div>
                    <div class="transaction-btn">
                        <form method="post" class="mb-0">
                            <input type="hidden" name="order_id" value="<?= $item->order_id ?>" />
                            <input type="hidden" name="state" value="2" />
                            <input type="hidden" name="action" value="procedure" />
                            <button type="submit" class="w-100">納品連絡</button>
                        </form>
                    </div>
                </div>
                <?php endif; ?>
            <?php endif; ?>

            <?php if (($order->state == 2 || $order->state == 5 || $order->state == 6) ): ?>
                <?php if($order->user_id == getUserId() || $order->owner_id == getUserId() || isAdminUser()): ?>
                    <?php
                            $feedback1 = new Feedback();
                            $feedback1->selectForIndex($item->id,$_GET['user_id'],1);
                            $feedback2 = new Feedback();
                            $feedback2->selectForIndex($item->id,$_GET['user_id'],2);
                            $user = new User();
                            $user->select($_GET['user_id']);
                        ?>
                    <?php if(($feedback1->score!= null || $feedback2->score != null) && ($order->state == 6 || $order->state == 5)): ?>
                    <div class="transaction-content mt-0">
                        <div class="transaction-info__lead transaction-info__lead_feedback mt-0"><p><span>フィードバック画像</span></p></div>
                        <div class="evaluation-item-show-result mt-0">
                        <div class="evaluation-result-person">
                                <div class="evaluation-result-person__img evaluation-result-person_img_comment">
                                    <?php if($user->get_icon_url() == ''):?>
                                        <img src="<?php echo HOME_URL; ?>/common/assets/img/common/icon/profile.png" alt="">
                                    <?php else: ?>
                                        <img src="<?= $user->get_icon_url();?>" alt="">
                                    <?php endif; ?>
                                </div>
                                <div class="evaluation-result-person__name">
                                    <p><a class="clr-yel"
                                            href="<?php echo HOME_URL; ?>/user/profile/<?= $user->hash_id ?>">
                                            <?= $user->name ?></a></p>
                                    
                                </div>
                            </div>
                            <div class="evaluation-result-content">
                                
                                <div class="evaluation-result-content__txt row">
                                    <?php if(@getimagesize(($feedback1->getImageUrl())) != false ): ?>
                                        <div class="col-md-6" >
                                            <a href="<?= $feedback1->getImageUrl() ?>" target="_blank"><img class="img-fluid"
                                                    width="300" src="<?= $feedback1->getImageUrl() ?>" /></a>
                                            <?php if($feedback1->state == 1): ?>
                                                <div>
                                                    <p><span class="fontRed" style="color: #FF0004">得点:</span> <?=($feedback1->state == 1) ? round(($feedback1->score)*100,1) : '' ?></p>
                                                </div>
                                            <?php endif; ?>
                                        </div>
                                    <?php endif; ?>
                                    <?php if(@getimagesize(($feedback2->getImageUrl())) != false ): ?>
                                        <div class="col-md-6" >
                                            <a href="<?= $feedback2->getImageUrl() ?>" target="_blank"><img class="img-fluid"
                                                    width="300" src="<?= $feedback2->getImageUrl() ?>" /></a>
                                            <?php if($feedback2->state == 1): ?>
                                                <div><p><span class="fontRed" style="color: #FF0004">得点:</span> <?=round(($feedback2->score)*100,1) ?></p></div>                                            
                                            <?php endif; ?>
                                        </div>
                                    <?php endif; ?>
                                    <p class="text-right col-md-12"><i class="bi bi-alarm"></i> <?= ($feedback1->regist_date != null ) ? $feedback1->regist_date : $feedback2->regist_date ?></p>
                                </div>
                            </div>
                        </div>    
                    </div>
                    <?php endif; ?>
                <div class="transaction-info">
                    <div class="transaction-info-flow__bottom">
                        <?php if($order->state != 6): ?>
                            <p class="fontBold">
                                サービスや商品を受け取ったら、必
                                ず下の<span class="fontRed">「受取連絡」ボタンを押して</span>出品者に通知してください。
                                もし、受け取った商品と出品画像を画像認証する場合、下の<span class="fontRed">「商品画像の認証を行う」</span>ボタンを押してください。
                            </p>
                        <?php endif; ?>
                        <?php if ($item->owner_id != getUserId()): ?>
                            <form class="text-center m-auto w-75" action="" method="post">
                                <input type="hidden" name="order_id"
                                    value="<?= $item->order_id ?>"/>
                                <input type="hidden" name="state" value="4"/>
                                <input type="hidden" name="action" value="procedure"/>
                                <a class="btn btn-block btn-info"  href="<?php echo HOME_URL; ?>/user/feedback_details.php?item_id=<?php echo $item->id?>&user_id=<?php echo $user->id?>">商品画像の認証を行う</a>
                            </form>
                        <?php endif; ?>
                    </div>
                </div>
                <?php endif; ?>
                <?php if ($order->state != 6 && $order->user_id == getUserId()): ?>
                    <div class="transaction-info">
                        <div class="transaction-info-flow__bottom" >
                            
                            <form class="text-center m-auto w-75" action="" method="post">
                                <input type="hidden" name="order_id"
                                    value="<?= $item->order_id ?>"/>
                                <input type="hidden" name="state" value="6"/>
                                <input type="hidden" name="action" value="procedure"/>
                                <input class="btn btn-block btn-info  btn-feedback"  value="受取連絡" type="submit" />
                            </form>
                        </div>
                    </div>
                <?php endif; ?>
            <?php endif; ?>
            
            <?php if ($order->state == 4 && $order->user_id == getUserId()): ?>
                <?php if ($order->state != 6 ): ?>
                    <div class="transaction-info">
                        <div class="transaction-info-flow__bottom" >
                            <form class="text-center m-auto w-75" action="" method="post">
                                <input type="hidden" name="order_id"
                                    value="<?= $item->order_id ?>"/>
                                <input type="hidden" name="state" value="6"/>
                                <input type="hidden" name="action" value="procedure"/>
                                <input class="btn btn-block btn-info  btn-feedback"  value="受取連絡" type="submit" />
                            </form>
                            
                        </div>
                    </div>
                <?php endif; ?>
            <?php endif; ?>

            <?php if ($item->order_id != null && ($item->buyer_id == getUserId() || $item->owner_id == getUserId())): ?>
            <div class="transaction-refer">
                <p>
                    ※購入者は、お早めのお振込。出品者は、ご入金確認後、速やかに納品をお願いいたします。 <br>
                    ※役務の場合、出品者は、ご入金確認後、<span>役務(サービスの提供を開始前に「納品のご連絡」、購入者は役務 が終了したら「受領のご連絡」ボタンを押してください。</span>
                </p>
            </div>
            <div class="transaction-bottom">
                <p><span>！重要！</span><br>取引の連絡は、以下の「取引連絡のコメント」以外は禁止となります。 重大な問題が発生した場合 を除いて取引相手に直接電話やメールを送るなどのすることは禁止です。
                    この場合、利用規約の禁止事項 違反とみなしてIDの利用を停止する場合があります。</p>
            </div>
            <?php endif; ?>

            <?php if (!User::isInvalid($item->owner_id)
                            && ($item->order_id == null || $item->buyer_id == getUserId()
                                || ($item->owner_id == getUserId()
                                    && ($item->buyer_id == null || !User::isInvalid($item->buyer_id))))): ?>
            <form action="" enctype="multipart/form-data" method="post" class="c-form01">
                <div class="transaction-info__lead">
                    <p><span>コメント</span></p>
                    <input type="hidden" name="item_id" value="<?= $item->id ?>" />
                    <input type="hidden" name="user_id" value="<?= getUserId() ?>" />
                    <input type="hidden" name="kbn" value="<?= $owner->id == getUserId() ? '2' : '1' ?>" />
                    <input type="hidden" name="action" value="comment" />
                    <input type="hidden" name="token" value="<?= createToken(); ?>" />
                </div>

                <div class="my-4 ">
                    <textarea class="form-control" rows="3" name="comment" placeholder="コメントを入力してください"></textarea>
                </div>

                <?php if (!empty($item->buyer_id)): ?>
                <div class="transaction-file-upload">
                    <div class="transaction-file-upload-item">
                        <div class="custom-file mb-3">
                            <input type="file" class="custom-file-input" id="lefile" name="photo">
                            <label class="custom-file-label" for="photo" id="file-photo" data-browse="参照">画像ファイル選択...</label>
                        </div>
                    </div>
                    <div class="transaction-file-upload-item">
                        <div class="custom-file mb-3">
                            <input type="file" class="custom-file-input" id="lefile" name="pdf">
                            <label class="custom-file-label" for="pdf" id="file-pdf"
                                data-browse="参照">PDFファイル選択...</label>
                        </div>
                    </div>
                </div>
                <div class="transaction-file-down">
                    <p>
                        ※役務やサービスで写真や画像が必要な場合は「画像ファイル選択...」ボタンよりアップロードし 「取引連絡の コメントする」ボタンで相手に送ることができます。 <br>
                        ※送信できるのは3MB 以下のjpg,gif.png.jpeg形式ファイルのみです。
                    </p>
                </div>
                <?php endif; ?>

                <div class="transaction-file-btn">
                    <?php if (empty($item->buyer_id)): ?>
                    <button type="submit">質問・交渉でコメントする</button>
                    <?php else: ?>
                    <button type="submit">取引連絡のコメントをする</button>
                    <?php endif; ?>
                </div>
            </form>
            <?php endif; ?>
            
            <?php if (count($after) > 0): ?>
            <div class="transaction-content transaction-content_img">
                <?php foreach ($after as $comment): ?>
                <?php
                        $commenter = new User();
                        $commenter->select($comment->user_id);
                    ?>
                <div class="evaluation-item-show-result">
                    <div class="evaluation-result-person">
                        <div class="evaluation-result-person__img evaluation-result-person_img_comment">
                            <?php if($commenter->get_icon_url() == ''):?>
                                <img src="<?php echo HOME_URL; ?>/common/assets/img/common/icon/profile.png" alt="">
                            <?php else: ?>
                                <img src="<?= $commenter->get_icon_url();?>" alt="">
                            <?php endif; ?>
                        </div>
                        <div class="evaluation-result-person__name">
                            <?php if ($comment->kbn == 1): ?>
                            <p><a class="clr-yel"
                                    href="<?php echo HOME_URL; ?>/user/profile/<?= $commenter->hash_id ?>">
                                    <?= $comment->user_name ?></a></p>
                            <?php else: ?>
                            <p>
                            <a class="clr-yel" href="<?php echo HOME_URL; ?>/user/profile/<?= $commenter->hash_id ?>">
                                <?= $commenter->name ?></a></p>
                            <?php endif; ?>
                        </div>
                    </div>
                    <div class="evaluation-result-content">
                        <div class="evaluation-result-content-head">
                            <div class="evaluation-result-content-head__level">
                                <?php if ($comment->kbn == 1): ?>
                                <p><a class="clr-yel"
                                        href="<?php echo HOME_URL; ?>/user/seller_home.php?owner_id=<?= $comment->user_id ?>">購入希望者
                                    </a>
                                    <!-- <?= $user_evaluate->point ?> -->
                                </p>
                                <?php else: ?>
                                    <p><a class="clr-yel"
                                        href="<?php echo HOME_URL; ?>/user/seller_home.php?owner_id=<?= $comment->user_id ?>">出品者
                                    </a>
                                </p>
                                <?php endif; ?>
                            </div>
                            <div class="evaluation-result-content-head__sex">
                                <!-- <p>レディース</p> -->
                            </div>
                        </div>
                        <div class="evaluation-result-content__txt">
                            <p class="size-1_5rem"><?php echo nl2br(htmlentities($comment->comment));?>
                            </p>
                            <div class="">
                                <?php if ($comment->hasPhoto()): ?>
                                <a href="<?= $comment->getImageUrl() ?>" target="_blank"><img class="img-fluid"
                                        width="300" src="<?= $comment->getImageUrl() ?>" /></a>
                                <?php endif; ?>
                                <?php if ($comment->hasPdf()): ?>
                                <embed src="<?= $comment->getPdfUrl() ?>" type="application/pdf" width="100%"
                                    height="100%">
                                <a class="clr-yel itemContentText" href="<?php echo $comment->getPdfUrl() ?>"
                                    target="_blank">PDFダウンロード</a>
                                <?php endif; ?>
                            </div>
                            <p class="text-right"><i class="bi bi-alarm"></i> <?= $comment->regist_date ?></p>
                            
                        </div>
                    </div>
                </div>
                <?php endforeach; ?>
            </div>
            <?php endif; ?>
            <div class="seller-evaluation__title seller-evaluation__title_margin" style="margin:7% 0 10% 0">
                <p><span>質問・交渉のコメント履歴</span></p>
            </div>
            <?php if (count($before) > 0 && ($item->order_id == null || $item->buyer_id == getUserId() || $item->owner_id == getUserId())): ?>
            <div class="transaction-content" style="margin-top:0">
                <?php foreach ($before as $comment): ?>
                <?php
                        $commenter = new User();
                        $commenter->select($comment->user_id);
                        ?>
                <div class="evaluation-item-show-result" style="margin-top:1rem">
                    <div class="evaluation-result-person">
                        <div class="evaluation-result-person__img evaluation-result-person_img_comment">
                            <?php if($commenter->get_icon_url() == ''):?>
                                <img src="<?php echo HOME_URL; ?>/common/assets/img/common/icon/profile.png" alt="">
                            <?php else: ?>
                                <img src="<?= $commenter->get_icon_url();?>" alt="">
                            <?php endif; ?>
                        </div>
                        <div class="evaluation-result-person__name">
                            <?php if ($comment->kbn == 1): ?>
                            <p><a class="clr-yel"
                                    href="<?php echo HOME_URL; ?>/user/profile/<?= $commenter->hash_id ?>">
                                    <?= $comment->user_name ?></a></p>
                            <?php else: ?>
                            <a class="clr-yel" href="<?php echo HOME_URL; ?>/user/profile/<?= $owner->hash_id ?>">
                                <?= $owner->name ?></a></p>
                            <?php endif; ?>
                        </div>

                    </div>
                    <div class="evaluation-result-content">
                        <div class="evaluation-result-content-head">
                            <div class="evaluation-result-content-head__level">
                                <?php if ($comment->kbn == 1): ?>
                                <p><a class="clr-yel"
                                        href="<?php echo HOME_URL; ?>/evaluate.php?user_id=<?= $comment->user_id ?>">購入希望者
                                    </a>
                                    <!-- <?= $user_evaluate->point ?> -->
                                </p>
                                <?php else: ?>
                                <p>出品者</p>
                                <?php endif; ?>
                            </div>
                            <div class="evaluation-result-content-head__sex">
                                <!-- <p>レディース</p> -->
                            </div>
                        </div>
                        <p class="size-1_5rem"><?php echo nl2br(htmlentities($comment->comment)); ?></p>
                        <div class="">
                            <?php if ($comment->hasPhoto()): ?>
                            <a href="<?= $comment->getImageUrl() ?>" target="_blank"><img class="img-fluid" width="300"
                                    src="<?= $comment->getImageUrl() ?>" /></a>
                            <?php endif; ?>
                            <?php if ($comment->hasPdf()): ?>
                            <embed src="<?= $comment->getPdfUrl() ?>" type="application/pdf" width="100%" height="100%">
                            <a class="clr-yel itemContentText" href="<?php echo $comment->getPdfUrl() ?>"
                                target="_blank">PDFダウンロード</a>
                            <?php endif; ?>
                        </div>
                        <p class="text-right"><i class="bi bi-alarm"></i> <?= $comment->regist_date ?></p>
                    </div>
                </div>
                <?php endforeach; ?>
            </div>
            <?php endif; ?>
            </div>
    </div>
</div>
</div>
</div>
</div>
</div>

<?php include('../user_footer.php'); ?>