<?php include_once(__DIR__ . '/../common/util.php'); ?>
<div class="userSidebar">
    <?php if (isLogin()): ?>
        <?php if (getSettlePlan() > 0): ?>
            <div class="linkList">
                <h4>出品</h4>
                <div class="list-group list-group-flush">
                    <a class="list-group-item" href="<?php echo HOME_URL; ?>/user/sell.php">出品する</a>
                    <a class="list-group-item" href="<?php echo HOME_URL; ?>/user/sellinglist.php">出品した<?= ITEM_NAME ?>
                        -出品中</a>
                </div>
            </div>
            <div class="linkList">
                <h4>売る</h4>
                <div class="list-group list-group-flush">
                    <a class="list-group-item"
                       href="<?php echo HOME_URL; ?>/user/negotiation_list_sell.php">出品した<?= ITEM_NAME ?>-質問・交渉中</a>
                    <a class="list-group-item"
                       href="<?php echo HOME_URL; ?>/user/soldout_transaction.php">出品した<?= ITEM_NAME ?>-販売済・取引中</a>
<!--                    <a class="list-group-item" href="--><?php //echo HOME_URL; ?><!--/user/soldout_list.php">出品した--><?php //echo ITEM_NAME; ?><!---評価</a>-->
                </div>
            </div>
        <?php endif; ?>

        <div class="linkList">
            <h4>買う</h4>
            <div class="list-group list-group-flush">
                <a class="list-group-item" href="<?php echo HOME_URL; ?>/user/negotiation_list.php">購入前の<?= ITEM_NAME ?>
                    -質問・交渉中</a>
                <a class="list-group-item"
                   href="<?php echo HOME_URL; ?>/user/purchased_transaction.php">購入した<?= ITEM_NAME ?>-購入済・取引中</a>
<!--                <a class="list-group-item" href="--><?php //echo HOME_URL; ?><!--/user/purchased.php">購入した--><?php //echo ITEM_NAME; ?><!---評価</a>-->
            </div>
        </div>

        <div class="linkList">
            <h4>購入希望</h4>
            <div class="list-group list-group-flush">
                <a class="list-group-item" href="<?php echo HOME_URL; ?>/user/request.php">購入希望を募集</a>
                <a class="list-group-item" href="<?php echo HOME_URL; ?>/user/request_list.php">購入希望の募集履歴</a>
            </div>
        </div>

        <div class="linkList">
            <h4>アカウント</h4>
            <div class="list-group list-group-flush">
                <a class="list-group-item" href="<?php echo HOME_URL; ?>/user/userinfo.php">ユーザー情報変更</a>
                <a class="list-group-item" href="<?php echo HOME_URL; ?>/user/account_number_info.php">口座情報変更</a>
                <a class="list-group-item" href="<?php echo HOME_URL; ?>/user/favorites_list.php">お気に入り一覧</a>
                <a class="list-group-item" href="<?php echo HOME_URL; ?>/user/evaluate.php">全ての評価</a>
                <a class="list-group-item" href="<?php echo HOME_URL; ?>/user/block_user_list.php">ブラックリスト登録</a>
                <a class="list-group-item" href="<?php echo HOME_URL; ?>/user/unsubscribe.php">退会方法について</a>
                <?php if (isIdentification()): ?>
                    <a class="list-group-item text-dark text-decoration-none"
                       href="<?php echo HOME_URL; ?>/user/upload_identification.php">本人確認書類の提出済<i class="far fa-check-circle text-success pl-1"></i></a>
                <?php else: ?>
                    <a class="list-group-item"
                       href="<?php echo HOME_URL; ?>/user/upload_identification.php">本人確認書類の未提出<i class="fas fa-times-circle text-danger pl-1"></i></a>
                <?php endif; ?>
            </div>
        </div>

        <div class="linkList">
            <h4>残高</h4>
            <div class="list-group list-group-flush">
                <a class="list-group-item" href="<?php echo HOME_URL; ?>/user/cash_flow_history.php">出金申請・履歴</a>
            </div>
        </div>

    <?php /* カード決済実装時に利用停止
        <div class="linkList">
            <h4><?php echo date('n'); ?>月のご請求予定額</h4>
            <div class="list-group list-group-flush text-right">
                <a class="list-group-item"
                   href="<?php echo HOME_URL; ?>/user/invoice_details.php"><?= number_format(Invoice::selectAmount(getUserId())) ?>
                    円<br>請求詳細情報</a>
            </div>
        </div>
    */ ?>
    <?php endif; ?>
</div>
