<?php include_once(__DIR__ . '/../common/util.php'); ?>
<?php
$id;
if (!empty($_GET['id'])) {
    $id = $_GET['id'];
} elseif (!empty($_POST['id'])) {
    $id = $_POST['id'];
}

if (empty($id)) {
    setMessage('不正アクセスです。');
    header('Location: ' . getContextRoot() . '/user/mypage.php');
    exit;
}

if (!isLogin()) {
    setMessage('ログインをしてください。');
    header('Location: ' . getContextRoot() . '/');
    exit;
}

if (!isSelling()) {
    setMessage('出品が許可されておりません。');
    header('Location: ' . getContextRoot() . '/user/mypage.php');
    exit;
}

if (!isIdentification()) {
    setMessage('本人確認が完了しておりません。');
    header('Location: ' . getContextRoot() . '/user/mypage.php');
    exit;
}

if (isset($_POST['action'])) {
    if ($_POST['action'] == 'modify') {
        $exhibited_item = $_SESSION['item'];
    } else {
        $exhibited_item = new Item();
        $exhibited_item->select($id);
        $exhibited_item->price = empty($_POST['price']) ? null : $_POST['price'];
        if ($_POST['action'] == 'regist') {
            $_SESSION['item'] = $exhibited_item;

            header('Location: ' . getContextRoot() . '/user/modify_item_confirm.php');
            exit();
        } elseif ($_POST['action'] == 'validation') {
            $exhibited_item->validateModify();
            $result = getMessage();
            echo json_encode($result);
            exit();
        }
    }
} else {
    $exhibited_item = new Item();
    $exhibited_item->select($id);
    $exhibited_item->repairName();
}
unset($_SESSION['item']);
?>

<script>
    function confirm() {
        var requestData = new FormData();

        var $f = $('#sell-form');
        var valueData = $f.serializeArray();
        $.each(valueData, function (key, input) {
            requestData.append(input.name, input.value);
        });
        requestData.append('action', 'validation');

// 	var fileData = $('input[name="photo1"]')[0].files;
// 	for (var i = 0; i < fileData.length; i++) {
// 		requestData.append("photo1", fileData[i]);
// 	}
        var result = $.ajax({
            url: './modify_item.php',
            type: 'post',
            async: false,
            processData: false,
            contentType: false,
            data: requestData
        }).responseText;

        var obj = JSON.parse(result);

        if (obj.msg.length > 0) {
            window.alert(obj.msg);
            return false;
        }
        return true;
    }
</script>

<?php
function console_log($output, $with_script_tags = true) {
    $js_code = 'console.log(' . json_encode($output, JSON_HEX_TAG) . 
');';
    if ($with_script_tags) {
        $js_code = '<script>' . $js_code . '</script>';
    }
    echo $js_code;
}

$title_page = '商品情報を編集する';
?>

<?php include(__DIR__ . '/../user_header.php'); ?>
<div class="com-header-top">
    <div class="com-header-top__img">
        <img src="<?php echo HOME_URL; ?>/common/assets/img/mypage/top-bg.png" alt="">
    </div>
    <div class="com-header-top__txt">
        <p class="clr-white">マイページ</p>
    </div>
</div>
<div class="com-container bg-yellow">
    <?php include('usersidebar.php'); ?>
    <?php console_log($exhibited_item->title); ?>    
    <div class="com-content">
        <div class="content-title">
            <h3><span>商品情報を編集する</span></h3>
        </div>
        <div class="qnego-sm-title" >
            <p style="color:red;">※価格のみ編集できます。</p>
        </div>
        <form id="sell-form" method="post" enctype="multipart/form-data" onsubmit="return confirm();">
            <div class="border-bottom py-4">
                <h2 class="bg-light p-2 fontBold my-3">商品の情報</h2>
                <h3 class="fontBold my-2">商品画像</h3>
                <div class="row">
                    <div class="col-md-3 mb-2">
                        <img class="img-fluid" src="<?= getImagePath($exhibited_item->id, 1) ?>" alt=""/>
                    </div>
                    <?php $url = getImagePath($exhibited_item->id, 2);
                    if (strstr($url, "noimage") == false): ?>
                        <div class="col-md-3 mb-2">
                            <img class="img-fluid" src="<?= getImagePath($exhibited_item->id, 2) ?>" alt=""/>
                        </div>
                    <?php endif; ?>
                    <?php $url = getImagePath($exhibited_item->id, 3);
                    if (strstr($url, "noimage") == false): ?>
                        <div class="col-md-3 mb-2">
                            <img class="img-fluid" src="<?= getImagePath($exhibited_item->id, 3) ?>" alt=""/>
                        </div>
                    <?php endif; ?>
                    <?php $url = getImagePath($exhibited_item->id, 4);
                    if (strstr($url, "noimage") == false): ?>
                        <div class="col-md-3 mb-2">
                            <img class="img-fluid" src="<?= getImagePath($exhibited_item->id, 4) ?>" alt=""/>
                        </div>
                    <?php endif; ?>
                </div>
            </div>

            <div class="border-bottom py-4">
                <h2 class="bg-light p-2 fontBold">商品の詳細</h2>
                <h3 class="fontBold">タイトル</h3>
                <h5><?= $exhibited_item->title ?></h5>
                <h3 class="fontBold my-2">商品の説明文</h3>
                <!-- <?php echo nl2br(htmlentities($exhibited_item->remarks)); ?> -->
                <h5><?= $exhibited_item->remarks ?></h5>
                <div class="font10">※最大1.5万文字まで説明文を入力できます。</div>
                <h3 class="fontBold my-2">カテゴリー</h3>
                <h5><?= $exhibited_item->category_name ?></h5>
                <h3 class="fontBold my-2">商品の状態</h3>
                <h5><?= $exhibited_item->item_state_name ?></h5>
            </div>

            <div class="border-bottom py-4">
                <h2 class="bg-light p-2 fontBold my-3">商品の配送について</h2>
                <h3 class="fontBold my-2">地域</h3>
                <h5><?= $exhibited_item->pref_name ?></h5>
                <h3 class="fontBold my-2">配送料の負担</h3>
                <h5><?= $exhibited_item->carriage_plan_name ?></h5>
                <h3 class="fontBold my-2">配送の目安</h3>
                <h5><?= $exhibited_item->carriage_type_name ?></h5>
                <h3 class="fontBold my-2">配送方法</h3>
                <h5><?= $exhibited_item->delivery_type_name ?></h5>
            </div>

            <div class="border-bottom py-4">
                <h2 class="bg-light p-2 fontBold my-3">価格(300円～9,999,999円)</h2>
                <h3 class="fontBold my-2">商品価格</h3>
                <input class="form-control middle-value" style="font-size:20px;" type="number" name="price"
                        value="<?= $exhibited_item->price ?>" placeholder="数字のみ（例　10,000円→10000と入力）" max="900000000">

                <!-- ▼ 評価の条件表示 -->
                <div class="margin-20 hidden">
                    <label style="font-size:15px; color:red;" class="font-normal fontRed"><input type="checkbox" name="show_evaluate"
                                                                id="show_evaluate" disabled="disabled"
                                                                value="1" <?= $exhibited_item->show_evaluate ? 'checked' : '' ?>>
                        総合評価の合計が-1以下のユーザーから、質問•交渉、購入をされたくない方はチェックを入れてください。</label>
                </div>
                <!-- ▲ 評価の条件表示 -->
            </div>
            <div class="row py-4">
                <div class="col-md-12 mb-3">
                    <input class="btn btn-success btn-block btn-sm" type="submit" value="確認・更新する"/>
                </div>
            </div>
            <input type="hidden" name="id" value="<?= $exhibited_item->id ?>"/>
            <input type="hidden" name="action" value="regist"/>
        </form>

    </div>
</div>

<?php include('../footer.php'); ?>
