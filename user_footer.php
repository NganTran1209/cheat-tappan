
<section class="footer">
    <div class="footer-content  wow animate__animated animate__fadeInUp">
        <div class="footer-content-item">
            <div class="footer-content-item__title"><p>カテゴリー一覧</p></div>
            <div class="footer-content-item-link">
                <div class="footer-content-item-link__group">
                    <p><a href="#">レディース</a></p>
                    <p><a href="#">メンズ</a></p>
                    <p><a href="#">キッズ・ベビー・マタニティ</a></p>
                    <p><a href="#">コスメ・美容</a></p>
                    <p><a href="#">エンタメ・ホビー</a></p>
                </div>
                <div class="footer-content-item-link__group">
                    <p><a href="#">インテリア・雑貨</a></p>
                    <p><a href="#">家電</a></p>
                    <p><a href="#">スポーツ・アウトドア </a></p>
                    <p><a href="#">自転車・バイク</a></p>
                    <p><a href="#">ハンドメイド</a></p>
                </div>
            </div>
        </div>
        <div class="footer-content-item">
            <div class="footer-content-item-group">
                <div class="footer-content-item__group">
                    <div class="footer-content-item__title"><p>ご利用情報</p></div>
                    <p><a href="<?php echo HOME_URL; ?>/login.php" class="footer-content-item__txt">ログイン</a></p>
                    <p><a href="<?php echo HOME_URL; ?>/user/userentry.php?step=1" class="footer-content-item__txt">新規会員登録</a></p>
                    <p><a href="#" class="footer-content-item__txt">料金について</a></p>
                    <p><a href="<?php echo HOME_URL; ?>/user/purase_request_info_list.php" class="footer-content-item__txt">購入希望情報</a></p>
                    <p><a href="<?php echo HOME_URL; ?>/user/notice.php" class="footer-content-item__txt">お知らせ </a></p>
                    <p><a href="<?php echo HOME_URL; ?>/pages/buyinfo.php" class="footer-content-item__txt">ご利用の流れ「買う」</a></p>
                    <p><a href="<?php echo HOME_URL; ?>/pages/sellinfo.php" class="footer-content-item__txt">ご利用の流れ「売る」</a></p>
                </div>
                <div class="footer-content-item__group">
                    <div class="footer-content-item__title"><p>TEPPANについて</p></div>
                    <p><a href="<?php echo HOME_URL; ?>/pages/teppan.php" class="footer-content-item__txt">てっぱんプレミアストアとは</a></p>
                    <p><a href="#" class="footer-content-item__txt">会社概要</a></p>
                    <p><a href="<?php echo HOME_URL; ?>/pages/tradelaw.php" class="footer-content-item__txt">特定商取引法に基づく表記</a></p>
                    <p><a href="<?php echo HOME_URL; ?>/pages/kiyaku.php" class="footer-content-item__txt">利用規約</a></p>
                    <p><a href="<?php echo HOME_URL; ?>/pages/privacypolicy.php" class="footer-content-item__txt">プライバシーポリシー </a></p>
                    <p><a href="<?php echo HOME_URL; ?>/pages/faq.php" class="footer-content-item__txt">よくあるご質問</a></p>
                    <p><a href="<?php echo HOME_URL; ?>/mail" class="footer-content-item__txt">お問い合わせ</a></p>
                </div>
            </div>
        </div>
    </div>
    <div id="footerlogo" class="footer-logo  wow animate__animated animate__fadeInUp">
        <a href="<?php echo HOME_URL; ?>/user/sell.php">
            <img src="<?php echo HOME_URL; ?>/common/assets/img/common/footer01.png" alt="">
        </a>
    </div>
    <div class="footer-end  wow animate__animated animate__fadeInUp">
        <p>© Teppan Co.,Ltd. All Rights Reserved.</p>
    </div>
</section>
    <!-- <script src="<?php //echo HOME_URL; ?>/common/assets/js/jquery.min.js"></script> -->
    <script src="https://code.jquery.com/jquery-3.6.0.min.js"></script>
    <script src="<?php echo HOME_URL; ?>/common/assets/js/bootstrap.min.js"></script>
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/animate.css/4.1.1/animate.min.css" />
    <script src="https://cdnjs.cloudflare.com/ajax/libs/wow/1.1.2/wow.min.js" ></script>
    <script src="<?php echo HOME_URL; ?>/common/assets/js/main.js"></script>
    <script>
        $( document ).ready(function() {
            if ($( window ).width()>=1024){
                $('.side-bar.c-navSP').addClass('none-active');
            }
        });

        var mybutton = document.getElementById("footerlogo");

        // When the user scrolls down 20px from the top of the document, show the button
        window.onscroll = function() {scrollFunction()};

        function scrollFunction() {
        if (document.body.scrollTop > 40 || document.documentElement.scrollTop > 40) {
            mybutton.style.display = "block";
        } else {
            mybutton.style.display = "none";
        }
        }
        $(".com-sidebar-item__link-txt").click(function() {
            window.location.href = $(this).find('a:first').attr('href');
        });
    </script>
</body>

</html>
