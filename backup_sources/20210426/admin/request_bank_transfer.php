<?php include_once(__DIR__ . '/../common/util.php'); ?>
<?php include_once(__DIR__ . '/../common/admin.php'); ?>
<?php
$filter = 0;
if (isset($_POST['filter'])) {
    $filter = $_POST['filter'];
}

$payment = new PayoutHistory();
$list = $payment->selectForAll();

$system_config = SystemConfig::select();
?>
<?php include('../header.php'); ?>
<style>
    .main-header__search{
        display:none;
    }
    .main-header-link{
        justify-content: flex-end;
        margin-right: 30px;
    }
    @media screen and (max-width: 768px) {   
    .main-header-link{
        justify-content: flex-start !important;
        margin-right:0;
    }}
</style>
    <div class="container">
        <div class="row">
        <div class="col-md-3 sideContents pc-only">
                <?php include('adminsidebar.php'); ?>
            </div>
            <!-- col-sm-3 -->
            <div class="col-md-9 mainContents mb-5">
                <div class="bg-inner admin-content-title admin-page">
                    <h1>出金申請の確認</h1>
                    <div class="row mb-3 mt-5">
                        <!--▼▼▼検索ページビュー▼▼▼ -->
                        <!--ソート-->
                        <form id="frm" method="post" class="col-md-6">
                            <select name="filter" onchange="frm.submit();" class="form-control">
                                <option value="0"<?= $filter == 0 ? ' selected' : '' ?>>すべて</option>
                                <option value="1"<?= $filter == 1 ? ' selected' : '' ?>>未振り込みのみ</option>
                            </select>
                        </form>
                    </div>
                    <!-- 表示数制限 -->
                    <style>
                        .btn {
                            font-size: 1em;
                        }
                    </style>
                    <!-- ▲▲▲検索ページビュー▲▲▲ -->
                    <div class="table-responsive">
                        <table class="table table-bordered table-hover small text-nowrap">
                            <thead>
                            <tr class="text-center">
                                <th>処理状況</th>
                                <th>振込先</th>
                                <th>金額</th>
                                <th>登録時刻</th>
                            </tr>
                            </thead>
                            <tbody>
                            <?php foreach ($list as $item): ?>
                                <?php if ($filter == 1 && $item->status) {
                                    continue;
                                } ?>
                                <tr>
                                    <td class="text-center">
                                        <?php if ($item->status): ?>
                                            振込完了
                                        <?php else: ?>
                                            <a href="bank_transfer.php?id=<?= $item->id ?>">振込待ち</a>
                                        <?php endif; ?>
                                    </td>
                                    <td class="text-left">
                                        <?= $item->user->hash_id . ':' . $item->user->name ?>
                                    </td>
                                    <td class="text-left">
                                        <?= number_format($item->amount) ?>円
                                    </td>
                                    <td class="text-left">
                                        <?= $item->regist_date ?>
                                    </td>
                                </tr>
                            <?php endforeach; ?>
                            </tbody>
                        </table>
                    </div>
                    <br>
                    <div class="text-center">
                        <nav>
                            <ul class="pagination justify-content-center">
                                <li class="page-item"><span id="prev">← 前へ</span></li>
                                <li class="page-item hidden"><span id="page"> </span></li>
                                <li class="page-item"><span id="next">次へ →</span></li>
                            </ul>
                        </nav>
                    </div>
                </div>
            </div>
            <div class="col-md-3 sideContents sp-only mt-5">
                <?php include('adminsidebar.php'); ?>
            </div>
        </div>
    </div>

<?php include('../footer.php'); ?>