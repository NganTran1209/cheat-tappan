<?php include_once(__DIR__ . '/../common/util.php'); ?>
<?php include_once(__DIR__ . '/../common/admin.php'); ?>
<?php
$filter = 0;
if (isset($_POST['filter'])) {
    $filter = $_POST['filter'];
}

if (isset($_POST['action'])) {
    if ($_POST['action'] = 'complete') {
        $order = new Order();
        $order->select($_POST['order_id']);
        $order->updateOrderForBankTransferComplete();

//         if (!$order->item->with_delivery) {
//             $order->updateStateForCompleteWithNonDelivery();
//         }

        setMessage('入金を確定しました。');
    }
}

$cash_flow = new CashFlow();
$list = $cash_flow->selectByIn();

$system_config = SystemConfig::select();
?>
<?php include('../header.php'); ?>

<style>
    .main-header__search{
        display:none;
    }
    .main-header-link{
        justify-content: flex-end;
        margin-right: 30px;
    }
    @media screen and (max-width: 768px) {   
    .main-header-link{
        justify-content: flex-start !important;
        margin-right:0;
    }}
</style>
    <div class="container">
        <div class="row">
        <div class="col-md-3 sideContents pc-only">
                <?php include('adminsidebar.php'); ?>
            </div>
            <div class="col-sm-9 mainContents mb-5">
                <div class="bg-inner admin-content-title admin-page">
                    <h1>購入代金入金の確認</h1>
                    <div class="row mb-3 mt-5">
                        <!--▼▼▼検索ページビュー▼▼▼ -->
                        <!--ソート-->
                        <form id="frm" method="post" class="col-md-6">
                            <select name="filter" onchange="frm.submit();" class="form-control">
                                <option value="0"<?= $filter == 0 ? ' selected' : '' ?>>すべて</option>
                                <option value="1"<?= $filter == 1 ? ' selected' : '' ?>>入金未確定のみ</option>
                            </select>
                        </form>
                    </div>
                    <!-- 表示数制限 -->
                    <!-- ▲▲▲検索ページビュー▲▲▲ -->
                    <div class="table-responsive">
                        <table class="table table-bordered table-hover small text-nowrap">
                            <thead>
                            <tr class="text-center">
                                <th>入金状況</th>
                                <th>決済方法</th>
                                <th>購入者</th>
                                <th><?= $system_config->item_name ?></th>
                                <th>金額</th>
                                <th>手数料</th>
                                <th>登録時刻</th>
                            </tr>
                            </thead>
                            <tbody>
                            <?php foreach ($list as $item): ?>
                                <?php if ($filter == 1 && $item->order->cash_flow >= 2) {
                                    continue;
                                } ?>
                                <tr>
                                    <td class="text-center">
                                        <?php if ($item->order->cash_flow == 2): ?>
                                            入金確認済み
                                        <?php elseif ($item->order->cash_flow == 1): ?>
                                            <form method="post" style="display: inline"
                                                  onsubmit="return window.confirm('入金を確定します。')">
                                                <input type="hidden" name="order_id" value="<?= $item->order->id ?>"/>
                                                <button class="btn btn-block btn-success btn-sm" type="submit" name="action"
                                                        value="complete">入金確定
                                                </button>
                                            </form>
                                        <?php else: ?> -
                                            振込準備中
                                        <?php endif; ?>
                                    </td>
                                    <td class="text-left">
                                        <?= $item->order->payment == 1 ? '銀行振込' : 'クレジット決済' ?>
                                    </td>
                                    <td class="text-left">
                                        <?= $item->order->user->hash_id . ':' . $item->order->user->name ?>
                                    </td>
                                    <td class="text-left">
                                        <?= $item->order->item->title ?>
                                    </td>
                                    <td class="text-left">
                                        <?= $item->amount ?>
                                    </td>
                                    <td class="text-left">
                                        <?= $item->commission ?>
                                    </td>
                                    <td class="text-left">
                                        <?= $item->regist_date ?>
                                    </td>
                                </tr>
                            <?php endforeach; ?>
                            </tbody>
                        </table>
                    </div>
                    <br>
                    <div class="text-center">
                        <nav>
                            <ul class="pagination justify-content-center">
                                <li class="page-item"><span id="prev">← 前へ</span></li>
                                <li class="page-item hidden"><span id="page"> </span></li>
                                <li class="page-item"><span id="next">次へ →</span></li>
                            </ul>
                        </nav>
                    </div>
                </div>
                </div>
                <div class="col-md-3 sideContents sp-only mt-5">
                <?php include('adminsidebar.php'); ?>
            </div>
        
    </div>
<?php include('../footer.php'); ?>