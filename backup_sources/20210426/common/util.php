<?php
include_once(__DIR__.'/db.php');
include_once(__DIR__.'/mail_template.php');
include_once(__DIR__.'/../entity/user.php');
include_once(__DIR__.'/../entity/item.php');
include_once(__DIR__.'/../entity/negotiation.php');
include_once(__DIR__.'/../entity/order.php');
include_once(__DIR__.'/../entity/identification.php');
include_once(__DIR__.'/../entity/information.php');
include_once(__DIR__.'/../entity/request.php');
include_once(__DIR__.'/../entity/request_follow.php');
include_once(__DIR__.'/../entity/order_evaluate.php');
include_once(__DIR__.'/../entity/invoice.php');
include_once(__DIR__.'/../entity/invoice_details.php');
include_once(__DIR__.'/../entity/evaluate.php');
include_once(__DIR__.'/../entity/block_access.php');
include_once(__DIR__.'/../entity/free_input_item.php');
include_once(__DIR__.'/../entity/free_input_item_value.php');
include_once(__DIR__.'/../entity/item_free_info.php');
include_once(__DIR__.'/../entity/system_config.php');
include_once(__DIR__.'/../entity/mail_history.php');
include_once(__DIR__.'/../entity/mail_history_target.php');
include_once(__DIR__.'/../entity/sns.php');
include_once(__DIR__.'/../entity/card_info.php');
include_once(__DIR__.'/../entity/cash_flow.php');
include_once(__DIR__.'/../entity/payout_history.php');
include_once(__DIR__.'/../entity/style_css.php');

include_once(__DIR__.'/../common/setting.php');

session_start();

/**
 * コンテキストルート取得
 * @return string
 */
function getContextRoot() {
    $contextroot = empty(CONTEXT_ROOT) ? '' : '/' . CONTEXT_ROOT;
    if (isset($_SERVER['HTTP_X_FORWARDED_PROTO'])) {
        return $_SERVER['HTTP_X_FORWARDED_PROTO'].'://'.$_SERVER['HTTP_HOST'].$contextroot;
    } else {
        return (empty($_SERVER["HTTPS"]) ? "http://" : "https://").$_SERVER['HTTP_HOST'].$contextroot;
    }
}

/**
 * リクエストURL取得
 */
function getRequestUri() {
    if (isset($_SERVER['HTTP_X_FORWARDED_PROTO'])) {
        return $_SERVER['HTTP_X_FORWARDED_PROTO'].'://'.$_SERVER['HTTP_HOST']. $_SERVER["REQUEST_URI"];
    } else {
        return (empty($_SERVER["HTTPS"]) ? "http://" : "https://").$_SERVER['HTTP_HOST']. $_SERVER["REQUEST_URI"];
    }
}

/**
 * リモートIPアドレス取得
 */
function getRemoteAddress() {
    if (isset($_SERVER['HTTP_X_FORWARDED_FOR'])) {
        return $_SERVER['HTTP_X_FORWARDED_FOR'];
    } else {
        return $_SERVER["REMOTE_ADDR"];
    }
}

/**
 * 管理ページチェック
 * @return mixed
 */
function isAdminPage() {
	return strpos(getRequestUri(), getContextRoot().'/admin') === 0;
}

/**
 * ログアウト
 */
function logout() {
	if (!isset($_SESSION['login'])) {
		return false;
	}

	User::removeLoginSession();
	unset( $_SESSION[ 'login' ] );
	session_destroy();
	return true;
}

/**
 * マスター登録
 * @param unknown $sqlid
 * @param unknown $id
 * @param unknown $name
 * @param unknown $enabled
 */
function registCodeList($sqlid, $id, $name, $enabled = 0) {
	$db = new DB();
	if ($id == null) {
		$param = array(
		    ':name' => $name
		);
	}
	else {
		$param = array(
			':id' => $id,
		    ':name' => $name,
		    ':enabled' => !empty($enabled),
		);
	}
	$db->execute($sqlid, $param);
}

/**
 * マスター一覧取得
 * @param unknown $sqlid
 * @return array
 */
function selectCodeList($sqlid) {
	$db = new DB();
	return $db->query($sqlid);
}

/**
 * マスター検索
 * @param unknown $sqlid
 * @param unknown $id
 * @return array
 */
function selectCodeName($sqlid, $id) {
	$db = new DB();
	$param = array(
			':id' => $id
	);
	$list = $db->query($sqlid, $param);
	if (is_array($list) && count($list) > 0) {
		foreach ($list as $item) {
			return $item['name'];
		}
	}
	return '';
}

/**
 * プルダウン生成
 * @param unknown $sqlid
 */
function createCombo($sqlid, $selected = null) {
	$db = new DB();
	$list = $db->query($sqlid);
	foreach ($list as $item) {
	    if (!$item['enabled']) {
	        continue;
	    }
		if ($selected != null && $item['id'] == $selected) {
			echo '<option value="'.$item['id'].'" selected="selected" >'.$item['name'].'</option>'."\n";
		}
		else {
			echo '<option value="'.$item['id'].'">'.$item['name'].'</option>'."\n";
		}
	}
}

/**
 * ラジオボタン生成
 * @param unknown $sqlid
 * @param unknown $name
 * @param unknown $selected
 */
function createRadio($sqlid, $name, $selected = null) {
	$db = new DB();
	$list = $db->query($sqlid);
	foreach ($list as $item) {
		if ($selected != null && $item['id'] == $selected) {
			echo '<label class="radio-inline"><input type="radio" name="'.$name.'" value="'.$item['id'].'" checked="checked">'.$item['name'].'</label>'."\n";
		}
		else {
			echo '<label class="radio-inline"><input type="radio" name="'.$name.'" value="'.$item['id'].'">'.$item['name'].'</label>'."\n";
		}
	}
}

/**
 * ラジオボタン生成
 * @param unknown $sqlid
 * @param unknown $name
 * @param unknown $selected
 */
function createCheckBox($sqlid, $name, $selected = null) {
	$array = explode(',', $selected);
	$db = new DB();
	$list = $db->query($sqlid);
	foreach ($list as $item) {
		if ($selected != null ) {
			$check=0;
			foreach($array as $arrayI){
				if($item['id'] == $arrayI){
					$check = 1;
				}
			}
			if($check == 1){
				echo '<label class="radio-inline"><input type="checkbox" name="'.$name.'" value="'.$item['id'].'" checked="checked"><span>'.$item['name'].'</span></label>'."\n";
			}else{
				echo '<label class="radio-inline"><input type="checkbox" name="'.$name.'" value="'.$item['id'].'"><span>'.$item['name'].'</span></label>'."\n";
			}
			$check=0;
		}
		else {
			
			echo '<label class="radio-inline"><input type="checkbox" name="'.$name.'" value="'.$item['id'].'"><span>'.$item['name'].'</span></label>'."\n";
		}
	}
}
/**
 * ログイン状況チェック
 * @return boolean
 */
function isLogin() {
	resetLogin();
	return isset($_SESSION['login']) &&  $_SESSION['login'] != null;
}

/**
 * ログイン情報の更新
 */
function resetLogin() {
	if (!isset($_SESSION['login'])) {
		return;
	}

	$login = $_SESSION['login'];
	$user = new User();
	$user->select(getUserId());
	if ($user->invalid) {
		logout();
		return;
	}

	if ($user->update_date > $login->update_date) {
		$_SESSION[ 'login' ] = $user;
	}
}


/**
 * ログイン情報取得
 * @return string|unknown
 */
function getEmail() {
	if (isset($_SESSION['login'])) {
		$user = $_SESSION['login'];
		return $user->email;
	}
	return '';
}

/**
 * 本人確認済み状況
 * @return boolean
 */
function isIdentification() {
	if (isset($_SESSION['login'])) {
		$user = $_SESSION['login'];
		return $user->identification == 1;
	}
	return false;
}

/**
 * ユーザID取得
 * @return string
 */
function getUserId() {
	if (isset($_SESSION['login'])) {
		$user = $_SESSION['login'];
		return $user->id;
	}
	return 0;
}


/**
 * ユーザーEメール取得
 * @return unknown|string
 */
function getUserEmail() {
    if (isset($_SESSION['login'])) {
        $user = $_SESSION['login'];
        return $user->email;
    }
    return '';
}

/**
 * ユーザ名取得
 * @return string
 */
function getUserName() {
	if (isset($_SESSION['login'])) {
		$user = $_SESSION['login'];
		return $user->name;
	}
	return '';
}

/**
 * 購入許可取得
 * @return string
 */
function hasBuying() {
	if (isset($_SESSION['login'])) {
		$user = $_SESSION['login'];
		return $user->buying;
	}
	return 0;
}

/**
 * ハッシュキー取得
 * @return string
 */
function getHashKey() {
	if (isset($_SESSION['login'])) {
		$user = $_SESSION['login'];
		return $user->hash_id;
	}
	return '';
}

/**
 * 地域取得
 * @return string
 */
function getUserPref() {
	if (isset($_SESSION['login'])) {
		$user = $_SESSION['login'];
		return $user->pref;
	}
	return '';
}

/**
 * 販売許可取得
 * @return string
 */
function isSelling() {
	if (isset($_SESSION['login'])) {
		$user = $_SESSION['login'];
		return $user->selling;
	}
	return false;
}

/**
 * 決済プラン取得
 * @return unknown|number
 */
function getSettlePlan() {
	if (isset($_SESSION['login'])) {
		$user = $_SESSION['login'];
		return $user->settle_plan;
	}
	return 0;
}

/**
 * メッセージセット
 * @param unknown $msg
 */
function setMessage($msg, $isPop = false) {
	if ($isPop) {
		$_SESSION['message-pop'] = $msg;
	} else {
		$_SESSION['message'] = $msg;
	}
}

/**
 * メッセージ取得
 * @return unknown
 */
function getMessage() {
	$msg = array();
	if (isset($_SESSION['message'])) {
		$msg['msg'] = $_SESSION['message'];
		$msg['pop'] = false;
		unset($_SESSION['message']);
	} elseif(isset($_SESSION['message-pop'])) {
		$msg['msg'] = $_SESSION['message-pop'];
		$msg['pop'] = true;
		unset($_SESSION['message-pop']);
	}
	return $msg;
}

/**
 * エンプティを空欄に変換
 * @param unknown $value
 * @return string|unknown
 */
function toEmpty($value) {
	if (empty($value)) {
		return '';
	}
	return $value;
}

/**
 * MySQLのTimestamp（文字列）を日付のみにする
 * @param unknown $value
 */
function toDateStringFromMySqlDatetime($value) {
	return str_replace('-', '/', substr($value, 0, 10));
}

/**
 * 新着チェック
 * @return boolean
 */
function isNew($value) {
	if (empty($value)) {
		return false;
	}

	return date('Y/m/d', strtotime('-'.NEW_DAYS.' days')) <= toDateStringFromMySqlDatetime($value);
}

/**
 * 開始文字列比較
 * @param unknown $haystack
 * @param unknown $needle
 * @return boolean
 */
function startsWith($haystack, $needle) {
	return (strpos($haystack, $needle) === 0);
}

/**
 * 終了文字列比較
 * @param unknown $haystack
 * @param unknown $needle
 * @return boolean
 */
function endsWith($haystack, $needle) {
	return (strrpos($haystack, $needle) === strlen($haystack) - strlen($needle));
}

/**
 * 決済
 */
function settlement($order_number, $price) {
	if ($price <= 0) {
		return false;
	}

	$params = [
			'version' => IPSILON_VERSION,
			'contract_code' => IPSILON_CONTRACT_CODE,
			'user_id' => IPSILON_USER_ID,
			'user_name' => getUserName(),
			'user_mail_add' => getEmail(),
			'item_code' => IPSILON_ITEM_CODE,
			'item_name' => mb_convert_encoding(IPSILON_ITEM_NAME, 'UTF-8', 'auto'),
			'order_number' => $order_number,
			'st_code' => '',
			'mission_code' => IPSILON_MISSION_CODE,
			'item_price' => $price,
			'process_code' => IPSILON_PROCESS_CODE,
			'memo1' => IPSILON_MEMO1,
			'memo2' => IPSILON_MEMO2,
			'xml' => IPSILON_XML,
			'character_code' => IPSILON_CHARACTER_CODE,
	];
	// mb_convert_encoding($item_name, "UTF-8", "auto"));

	$curl = curl_init(IPSILON_SETTLEMENT_URL);
	curl_setopt($curl, CURLOPT_POST, TRUE);
	curl_setopt($curl, CURLOPT_POSTFIELDS, $params); // パラメータをセット
	curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);
	curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
	$xml = curl_exec($curl);
	curl_close($curl);
	$result = new SimpleXMLElement($xml);
	return $result;
}

/**
 * 商品ステータス変換
 * @param unknown $item
 * @return string
 */
function toItemStatusText($item) {
	if ($item->enabled != 1) {
		return '承認待ち';
	}
	return empty($item->buyer_id) ? '販売中' : '売約済み';
}

/**
 * YYYYMM → YYYY年MM月
 * @param unknown $value
 * @return string
 */
function toMonthJp($value) {
	$year = substr($value, 0, 4);
	$month = (int)substr($value, 4, 2);

	return $year.'年'.$month.'月';
}

/**
 * クッキー情報からの自動ログイン
 * @return boolean
 */
function autoLogin() {
	if (!isset($_COOKIE['SESSION_KEY'])) {
		return false;
	}
	$hash_key = $_COOKIE['SESSION_KEY'];
	if (empty($hash_key)) {
		return false;
	}
	$user = User::selectLoginSession($hash_key);
	if (empty($user->email) || empty($user->password)) {
		User::removeLoginSession();
		return false;
	}
	$user->login(true);
	$_SESSION[ 'login' ] = $user;
}

/**
 * サムネイル変換
 * @param unknown $image
 */
function convertThumbnailImage($image) {
	return convertImage($image, MAX_THUMBNAIL_WIDTH, MAX_THUMBNAIL_HEIGHT);
}

/**
 * 画像変換
 * @param unknown $image 変換前バイナリ
 * @param unknown $max_width 最大幅
 * @param unknown $max_height 最大高さ
 */
function convertImage($image, $max_width = MAX_IMAGE_WIDTH, $max_height = MAX_IMAGE_HEIGHT) {
	list($width, $height, $type) = getimagesizefromstring($image);
	if ($width <= $max_width&& $height <= $max_height) {
		return $image;
	}

	$width_rate =  $max_width/ $width;
	$height_rate = $max_height/ $height;

	$rate = $width_rate;
	if ($width_rate < $height_rate) {
		$rate = $height_rate;
	}

	$width2 = $width * $rate;
	$height2 = $height * $rate;

	$src_image = imagecreatefromstring($image);
	$dst_image = imagecreatetruecolor($width2, $height2);
	imagefill($dst_image, 0, 0, imagecolorallocate($dst_image, 255, 255, 255));
	imagecopyresampled($dst_image, $src_image, 0, 0, 0, 0, $width2, $height2, $width, $height);

	ob_start();
	imagejpeg($dst_image, null, 100);
	//imagepng($dst_image, null, 1);
	$result = ob_get_clean();

	imagedestroy($src_image);
	imagedestroy($dst_image);

	return $result;
}

/**
 * 画像のパス取得
 * @param unknown $id
 * @param unknown $number
 */
function getImagePath($id, $number) {
    $item = new Item();
    $item->select($id);
    $path = $item->getImagePath($number);
	if (file_exists($item->getImagePath($number))) {
	    return $item->getImageUrl($number);
	}

	return getContextRoot() . '/' . NOIMAGE_PATH;
}

/**
 * イメージ傾き調整
 * @param unknown $image イメージ
 * @return unknown|string
 */
function rotateImage($image) {

	$temp = tmpfile();
	fwrite($temp, $image);
	fseek($temp, 0);
	$meta = stream_get_meta_data($temp);
	$exif = @exif_read_data($meta['uri']);
	fclose($temp);

	if (empty($exif['Orientation']) || in_array($exif['Orientation'], [1, 2])) {
		return $image;
	}

	$src = imagecreatefromstring($image);
	$angle= 0;
	$mode = 0;

	switch ($exif['Orientation']) {
		case 2:
			$mode = IMG_FLIP_HORIZONTAL;
			break;
		case 3:
			$angle= 180;
			break;
		case 4:
			$mode = IMG_FLIP_VERTICAL;
			break;
		case 5:
			$angle= 90;
			$mode    = IMG_FLIP_VERTICAL;
			break;
		case 6:
			$angle= 270;
			break;
		case 7:
			$angle= 270;
			$mode    = IMG_FLIP_VERTICAL;
			break;
		case 8:
			$angle= 90;
			break;
	}

	if (!empty($mode)) {
		imageflip($src, $mode);
	}

	if (!empty($angle)) {
		$src= imagerotate($src, $angle, 0);
	}

	ob_start();
	if (empty($exif['MimeType']) || $exif['MimeType'] == 'image/jpeg') {
		imagejpeg($src);
	} elseif ($exif['MimeType'] == 'image/png') {
		imagepng($src);
	} elseif ($exif['MimeType'] == 'image/gif') {
		imagegif($src);
	}

	$result = ob_get_clean();
	imagedestroy($src);
	return $result;
}

/**
 * 画像ファイル判別
 * @param unknown $path
 * @return boolean
 */
function isImage($path) {
	return file_exists($path) && exif_imagetype($path);
}

/**
 * 日付差異取得
 * @param unknown $lhs
 * @param unknown $rhs
 */
function diffDays($lhs, $rhs) {
	$l_timestamp= strtotime($lhs);
	$r_timestamp = strtotime(date('Y/m/d', strtotime($rhs)));

	$seconddiff = abs($r_timestamp- $l_timestamp);
	$daydiff = $seconddiff / (60 * 60 * 24);

	return $daydiff;
}

/**
 * トークン生成
 * @return string
 */
function createToken() {
    $token = hash('crc32', 'TOKEN:' . date('YmdHis'));
    $_SESSION['token'] = $token;
    return hash('crc32', 'TOKEN:' . date('YmdHis'));
}

/**
 * トークンチェック
 * @return boolean
 */
function checkToken() {
    if (!isset($_POST['token'])) {
        return false;
    }
    $ret = $_SESSION['token'] == $_POST['token'];
    createToken();
    return $ret;
}

/**
 * 管理者ログインチェック
 * @return boolean
 */
function isAdminUser() {
    return isset($_SESSION['login']) && $_SESSION['login']->admin;
}

// 自動ログイン
if (!isLogin()) {
	autoLogin();
}


// IPアドレス更新
User::updateIp();

// 商品の別名
define('ITEM_NAME', SystemConfig::getItemName());