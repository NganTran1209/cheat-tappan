<?php include_once(__DIR__ . '/common/util.php'); ?>
<?php /* サイトタイトル用　パーセント表示など */
$system_config = SystemConfig::select(); ?>
<?php /*スマホ判定処理 これも使ってないので不要 */
if (!isset($item)) {
    $item = new Item();
}

//ページタイトル
$meta_title = "てっぱんプレミアムストア";
//ページの説明
$meta_description = "欲しい商品を画像で確認・届いた商品を画像で照合！気持ち良い取引が叶うオンラインマーケット。";
?>
<!doctype html>
<html lang="ja">

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="keywords" content="" />
    <meta name="description" content="<?php echo $meta_description; ?>" />
    <link rel="canonical" href="">
    <title><?php echo $meta_title; ?></title>
    <link rel="icon" type="image/vnd.microsoft.icon" href="">
    
    <!-- Global site tag (gtag.js) - Google Analytics -->
    <script async src="https://www.googletagmanager.com/gtag/js?id=G-8H0QENYL0L"></script>
    <script>
        window.dataLayer = window.dataLayer || [];
        function gtag(){dataLayer.push(arguments);}
        gtag('js', new Date());

        gtag('config', 'G-8H0QENYL0L');
    </script>

    <link href="https://fonts.googleapis.com/css2?family=Castoro&display=swap" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css?family=Noto+Serif+JP:400,700&display=swap&subset=japanese" rel="stylesheet">
    <!-- OGP -->
    <meta property="og:site_name" content="">
    <meta property="og:title" content="">
    <meta property="og:description" content="">
    <meta property="og:type" content="website">
    <meta property="og:url" content="">
    <meta property="og:image" content="">
    <meta name="twitter:card" content="summary">
    <meta name="twitter:site" content="">
    <meta name="twitter:image" content="">

    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
    <link href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css" rel="stylesheet" type="text/css" />
    <script
      src="https://code.jquery.com/jquery-2.2.4.min.js"
      integrity="sha256-BbhdlvQf/xTY9gja0Dq3HiwQF8LaCRTXxZKRutelT44="
      crossorigin="anonymous"></script>
    <link rel="stylesheet" href="<?php echo HOME_URL; ?>/common/assets/css/common.css">
    <link rel="stylesheet" href="<?php echo HOME_URL; ?>/common/assets/css/style.css">
    <link rel="stylesheet" href="<?php echo HOME_URL; ?>/common/assets/css/sub_page.css">
    <link rel="stylesheet" href="<?php echo HOME_URL; ?>/common/assets/css/responsive.css">
    <link rel="stylesheet" href="<?php echo HOME_URL; ?>/common/assets/css/add_cheat.css">
    <link rel="stylesheet" href="<?php echo HOME_URL; ?>/common/assets/css/add_cheat2.css">
    <link rel="stylesheet" href="<?php echo HOME_URL; ?>/common/assets/css/add_cheat3.css">
    
    <?php //admin管理画面だけのcss

$url = $_SERVER['REQUEST_URI'];
$pattern = '/admin/';
$result = preg_match( $pattern, $url );

if( $result ){
  /** URLが"正しい"場合の処理 */
  ?>
    <link rel="stylesheet" href="<?php echo HOME_URL; ?>/common/assets/css/admin_css.css">
  <?php } ?>
  <link rel="stylesheet" href="<?php echo HOME_URL; ?>/common/assets/css/add_quynh_fix.css?v=<?php echo strtotime('now');?>">
</head>
<body> 
<?php ini_set('display_errors', "On"); ?>
<div class="main-header">
    <div class="main-header__logo">
        <a href="<?php echo HOME_URL; ?>"><img src="<?php echo HOME_URL; ?>/common/assets/img/common/logo.png" alt=""></a>
    </div>
    <div class="main-header__search">

        <form action="<?php echo HOME_URL; ?>/itemlist.php" method="post">
            <input type="text" name="search"
                           value="<?= empty($_POST['search']) ? '' : $_POST['search'] ?>" placeholder="キーワードを入力">
            <button type="submit"><i class="fa fa-search"></i></button>
        </form>
    </div>
    <div class="main-header-link">
        <?php
        $url = $_SERVER["REQUEST_URI"];
        if (strstr($url, "admin") == true):?>
            <div class="main-header-link__txt">
                <div class="main-header-link__txt-item">
                    <span>
                        <svg xmlns="http://www.w3.org/2000/svg" width="20" height="20" viewBox="0 0 8 8">
                            <path d="M5.5 0c-1.38 0-2.5 1.12-2.5 2.5 0 .16 0 .32.03.47l-3.03 3.03v2h3v-2h2v-1l.03-.03c.15.03.31.03.47.03 1.38 0 2.5-1.12 2.5-2.5s-1.12-2.5-2.5-2.5zm.5 1c.55 0 1 .45 1 1s-.45 1-1 1-1-.45-1-1 .45-1 1-1z" fill="white"/>
                        </svg>
                    </span>
                    <span><a href="<?php echo HOME_URL; ?>/admin/">管理画面</a></span>
                </div>
            </div>
        <?php elseif (isLogin()): ?>
            <?php if (isAdminUser()): ?>
                <div class="main-header-link__txt">
                    <div class="main-header-link__txt-item">
                        <span>
                            <svg xmlns="http://www.w3.org/2000/svg" width="20" height="20" viewBox="0 0 8 8">
                                <path d="M4 0c-1.1 0-2 1.12-2 2.5s.9 2.5 2 2.5 2-1.12 2-2.5-.9-2.5-2-2.5zm-2.09 5c-1.06.05-1.91.92-1.91 2v1h8v-1c0-1.08-.84-1.95-1.91-2-.54.61-1.28 1-2.09 1-.81 0-1.55-.39-2.09-1z" fill="white"/>
                            </svg>
                        </span>
                        <span><a href="<?php echo HOME_URL; ?>/admin/">管理画面へ</a></span>
                    </div>
                </div>
            <?php endif; ?>
            <div class="main-header-link__txt">
                <div class="main-header-link__txt-item">
                    <span>
                        <svg xmlns="http://www.w3.org/2000/svg" width="20" height="20" viewBox="0 0 8 8">
                            <path d="M5.5 0c-1.38 0-2.5 1.12-2.5 2.5 0 .16 0 .32.03.47l-3.03 3.03v2h3v-2h2v-1l.03-.03c.15.03.31.03.47.03 1.38 0 2.5-1.12 2.5-2.5s-1.12-2.5-2.5-2.5zm.5 1c.55 0 1 .45 1 1s-.45 1-1 1-1-.45-1-1 .45-1 1-1z" fill="white"/>
                        </svg>
                    </span>
                    <span><a href="<?php echo HOME_URL; ?>/login.php"><?= isLogin() ? 'ログアウト' : 'ログイン' ?></a></span>
                </div>
            </div>
            <div class="main-header-link__txt">
                <div class="main-header-link__txt-item">
                    <span>
                        <svg xmlns="http://www.w3.org/2000/svg" width="20" height="20" viewBox="0 0 8 8">
                            <path d="M4 0c-1.1 0-2 1.12-2 2.5s.9 2.5 2 2.5 2-1.12 2-2.5-.9-2.5-2-2.5zm-2.09 5c-1.06.05-1.91.92-1.91 2v1h8v-1c0-1.08-.84-1.95-1.91-2-.54.61-1.28 1-2.09 1-.81 0-1.55-.39-2.09-1z" fill="white"/>
                        </svg>
                    </span>
                    <span><a href="<?php echo HOME_URL; ?>/user/mypage.php">マイページ</a></span>
                </div>
            </div>
        <?php elseif (!isLogin()): ?>
            <div class="main-header-link__txt">
                <div class="main-header-link__txt-item">
                    <span>
                        <svg xmlns="http://www.w3.org/2000/svg" width="20" height="20" viewBox="0 0 8 8">
                            <path d="M5.5 0c-1.38 0-2.5 1.12-2.5 2.5 0 .16 0 .32.03.47l-3.03 3.03v2h3v-2h2v-1l.03-.03c.15.03.31.03.47.03 1.38 0 2.5-1.12 2.5-2.5s-1.12-2.5-2.5-2.5zm.5 1c.55 0 1 .45 1 1s-.45 1-1 1-1-.45-1-1 .45-1 1-1z" fill="white"/>
                        </svg>
                    </span>
                    <span><a href="<?php echo HOME_URL; ?>/login.php"><?= isLogin() ? 'ログアウト' : 'ログイン' ?></a></span>
                </div>
            </div>
            <div class="main-header-link__txt">
                <div class="main-header-link__txt-item">
                    <span>
                        <svg xmlns="http://www.w3.org/2000/svg" width="20" height="20" viewBox="0 0 8 8">
                            <path d="M4 0c-1.1 0-2 1.12-2 2.5s.9 2.5 2 2.5 2-1.12 2-2.5-.9-2.5-2-2.5zm-2.09 5c-1.06.05-1.91.92-1.91 2v1h8v-1c0-1.08-.84-1.95-1.91-2-.54.61-1.28 1-2.09 1-.81 0-1.55-.39-2.09-1z" fill="white"/>
                        </svg>
                    </span>
                    <span><a href="<?php echo HOME_URL; ?>/user/userentry.php?step=1">新規会員登録</a></span>
                </div>
            </div>
        <?php endif; ?>
    </div>
</div>

<?php //保存・エラーメッセージ
$msg = getMessage();
if (count($msg) > 0 && !empty($msg['msg'])) {
    if ($msg['pop']) {
        echo '<script>window.alert("' . $msg['msg'] . '");</script>';
    } else {
        echo "<div class='container'><div class='alert alert-success'><strong>" . $msg['msg'] . "</strong></div></div>";
    }
}
?>