<?php
include_once(__DIR__.'/../common/db.php');

class Order {

	public $id;
	public $item_id;
	public $user_id;
	public $state;
	public $state_name;
	public $cash_flow;
	public $cash_flow_name;
	public $payment;
	public $payment_name;
	public $seller_evaluate;
	public $buyer_evaluate;
	public $seller_comment;
	public $buyer_comment;

	public $user_name;
	public $owner_id;
	public $owner_name;
	public $user_hash_id;
	public $owner_hash_id;
	public $item_title;

	public $regist_date;
	public $update_date;

	public $owner;
	public $user;

	public $item;

	function __construct() {
	    $this->owner = new User();
	    $this->user = new User();
	    $this->item = new Item();
	}

	/**
	 * 検索
	 * @param unknown $id
	 */
	public function select($id) {
		$db = new DB();
		$param = array(
				':id' => $id
		);

		$list = $db->query('selectOrder', $param);
		if (is_array($list) && count($list) > 0) {
			foreach ($list as $item) {
				$this->id = $item['id'];
				$this->user_id= $item['user_id'];
				$this->item_id= $item['item_id'];
				$this->state= $item['state'];
				$this->cash_flow= $item['cash_flow'];
				$this->payment= $item['payment'];
				$this->seller_evaluate= $item['seller_evaluate'];
				$this->buyer_evaluate= $item['buyer_evaluate'];
				$this->seller_comment= $item['seller_comment'];
				$this->buyer_comment= $item['buyer_comment'];
				$this->owner_id= $item['owner_id'];
				$this->user_name= $item['user_name'];
				$this->owner_name= $item['owner_name'];
				$this->user_hash_id= $item['user_hash_id'];
				$this->owner_hash_id= $item['owner_hash_id'];

				$this->regist_date= $item['regist_date'];
				$this->update_date= $item['update_date'];

				$this->owner->select($this->owner_id);
				$this->user->select($this->user_id);
				$this->item->select($this->item_id);

			}
		}
	}

	/**
	 * 購入注文検索（出品者）
	 * @param unknown $owner_id
	 * @return Order[]
	 */
	public function selectFromOwner($owner_id) {
		$db = new DB();
		$param = array(
				':owner_id' => $owner_id
		);

		$result = array();
		$list = $db->query('selectOrderFromOwner', $param);
		if (is_array($list) && count($list) > 0) {
			foreach ($list as $item) {
				$entity = new Order();
				$entity->id = $item['id'];
				$entity->user_id= $item['user_id'];
				$entity->item_id= $item['item_id'];
				$entity->state= $item['state'];
				$entity->cash_flow= $item['cash_flow'];
				$entity->payment= $item['payment'];
				$entity->item_title= $item['item_title'];
				$entity->seller_evaluate= $item['seller_evaluate'];
				$entity->buyer_evaluate= $item['buyer_evaluate'];
				$entity->seller_comment= $item['seller_comment'];
				$entity->buyer_comment= $item['buyer_comment'];
				$entity->owner_id= $item['owner_id'];
				$entity->user_name= $item['user_name'];
				$entity->owner_name= $item['owner_name'];
				$entity->user_hash_id= $item['user_hash_id'];
				$entity->owner_hash_id= $item['owner_hash_id'];

				$entity->regist_date= $item['regist_date'];
				$entity->update_date= $item['update_date'];

				$entity->owner->select($entity->owner_id);
				$entity->user->select($entity->user_id);
				$entity->item->select($entity->item_id);

				$result[] = $entity;
			}
		}
		return $result;
	}

	/**
	 * 購入注文検索（全部）
	 * @param unknown $owner_id
	 * @return Order[]
	 */
	public function selectFromUserAll($user_id) {
		$db = new DB();
		$param = array(
				':user_id' => $user_id
		);

		$result = array();
		$list = $db->query('selectOrderFromUserAll', $param);
		if (is_array($list) && count($list) > 0) {
			foreach ($list as $item) {
				$entity = new Order();
				$entity->id = $item['id'];
				$entity->user_id= $item['user_id'];
				$entity->item_id= $item['item_id'];
				$entity->state= $item['state'];
				$entity->cash_flow= $item['cash_flow'];
				$entity->payment= $item['payment'];
				$entity->item_title= $item['item_title'];
				$entity->seller_evaluate= $item['seller_evaluate'];
				$entity->buyer_evaluate= $item['buyer_evaluate'];
				$entity->seller_comment= $item['seller_comment'];
				$entity->buyer_comment= $item['buyer_comment'];
				$entity->owner_id= $item['owner_id'];
				$entity->user_name= $item['user_name'];
				$entity->owner_name= $item['owner_name'];
				$entity->user_hash_id= $item['user_hash_id'];
				$entity->owner_hash_id= $item['owner_hash_id'];

				$entity->regist_date= $item['regist_date'];
				$entity->update_date= $item['update_date'];

				$entity->owner->select($entity->owner_id);
				$entity->user->select($entity->user_id);
				$entity->item->select($entity->item_id);

				$result[] = $entity;
			}
		}
		return $result;
	}

	public function selectFromUser($user_id) {
		$db = new DB();
		$param = array(
				':user_id' => $user_id
		);

		$result = array();
		$list = $db->query('selectOrderFromUser', $param);
		if (is_array($list) && count($list) > 0) {
			foreach ($list as $item) {
				$entity = new Order();
				$entity->id = $item['id'];
				$entity->user_id= $item['user_id'];
				$entity->item_id= $item['item_id'];
				$entity->state= $item['state'];
				$entity->cash_flow= $item['cash_flow'];
				$entity->payment= $item['payment'];
				$entity->item_title= $item['item_title'];
				$entity->seller_evaluate= $item['seller_evaluate'];
				$entity->buyer_evaluate= $item['buyer_evaluate'];
				$entity->seller_comment= $item['seller_comment'];
				$entity->buyer_comment= $item['buyer_comment'];
				$entity->owner_id= $item['owner_id'];
				$entity->user_name= $item['user_name'];
				$entity->owner_name= $item['owner_name'];
				$entity->user_hash_id= $item['user_hash_id'];
				$entity->owner_hash_id= $item['owner_hash_id'];

				$entity->regist_date= $item['regist_date'];
				$entity->update_date= $item['update_date'];

				$entity->owner->select($entity->owner_id);
				$entity->user->select($entity->user_id);
				$entity->item->select($entity->item_id);

				$result[] = $entity;
			}
		}
		return $result;
	}

	static public function selectFromItem($item_id) {
		$db = new DB();
		$param = array(
				':item_id' => $item_id
		);

		$result = array();
		$list = $db->query('selectOrderFromItem', $param);
		if (is_array($list) && count($list) > 0) {
			return true;
		}
		return false;
	}

	/**
	 * 登録
	 */
	public function regist() {
		$db = new DB();
		$param = array(
				':item_id' => $this->item_id,
				':user_id' => $this->user_id,
				':state' => $this->state
		);

		//$db->beginTransaction();
		try {
			$db->execute('insertOrder', $param);
			$this->sendRegistMail();
		} catch (Exception $e) {

		}
		//$db->commit();
	}

	/**
	 * 登録
	 */
	public function updateProcedure() {
		$db = new DB();
		$param = array(
				':id' => $this->id,
				':state' => $this->state
		);

		$db->execute('updateOrderState', $param);
		$this->sendProcedure();
	}

	/**
	 * 登録（カード決済）
	 */
	public function updateOrderForCardPayment() {
	    $db = new DB();

	    $param = array(
	        ':id' => $this->id,
	        ':cash_flow' => 2,
	    );
	    $db->execute('updateOrderForCashFlow', $param);

	    $param = array(
	        ':id' => $this->id,
	        ':payment' => 2,
	    );
	    $db->execute('updateOrderForPayment', $param);

	    $param = array(
	        ':id' => $this->id,
	        ':state' => 1
	    );
	    $db->execute('updateOrderState', $param);

	    $this->sendMailCardPayment();
	    $this->sendMailCardPaymentAdmin();
	    $this->sendMailPaymentComplete();
	}

	/**
	 * 登録（銀行振り込み）
	 */
	public function updateOrderForBankTransfer() {
	    $db = new DB();

	    $param = array(
	        ':id' => $this->id,
	        ':cash_flow' => 1,
	    );
	    $db->execute('updateOrderForCashFlow', $param);

	    $param = array(
	        ':id' => $this->id,
	        ':payment' => 1,
	    );
	    $db->execute('updateOrderForPayment', $param);
	    $this->sendMailBankTransfer();
	}

	/**
	 * 銀行振り込み実施
	 */
	public function updateOrderForBankTransferEntry() {
	    $db = new DB();
	    $param = array(
	        ':id' => $this->id,
	        ':state' => 1
	    );
	    $db->execute('updateOrderState', $param);
	    $this->sendMailBankTransferAdmin();
	}

	/**
	 * 銀行振り込み入金確定
	 */
	public function updateOrderForBankTransferComplete() {
	    $db = new DB();
	    $param = array(
	        ':id' => $this->id,
	        ':cash_flow' => 2,
	    );
	    $db->execute('updateOrderForCashFlow', $param);
	    $this->sendMailPaymentComplete();
	}

	/**
	 * 配送なし取引の完了
	 */
	public function updateStateForCompleteWithNonDelivery() {
	    $db = new DB();
	    $param = array(
	        ':id' => $this->id,
	        ':state' => 1
	    );
	    $db->execute('updateOrderState', $param);
	    $this->sendMailPaymentCompleteWithNonDelivery();
	}

	/**
	 * 販売者評価登録
	 */
	public function registEvaluateByBuyer() {
		$db = new DB();
		$param = array(
				':id' => $this->id,
				':buyer_evaluate' => $this->buyer_evaluate,
				':buyer_comment' => $this->buyer_comment
		);

		$db->execute('registOrderEvaluateByBuyer', $param);
		$this->sendMailEvaluateByBuyer();
	}

	/**
	 * 購入者評価登録
	 */
	public function registEvaluateBySeller() {
		$db = new DB();
		$param = array(
				':id' => $this->id,
				':seller_evaluate' => $this->seller_evaluate,
				':seller_comment' => $this->seller_comment,
		);

		$db->execute('registOrderEvaluateBySeller', $param);
		$this->sendMailEvaluateBySeller();
	}

	/**
	 * メール送信
	 */
	public function sendRegistMail() {
		$item = new Item();
		$item->select($this->item_id);
		$owner = new User();
		$owner->select($item->owner_id);

		if (RequestFollow::hasRequest(getUserId(), $this->item_id)) {
			$this->sendRequestOrderMail($item, $owner);
			$this->sendRequestOrderConpleteMail($item, $owner);
		} else {
			$this->sendOrderMail($item, $owner);
			$this->sendOrderConpleteMail($item, $owner);
		}
	}

	private function sendOrderMail($item, $owner) {

	    $system_config = SystemConfig::select();
	    $mail = new Mail();
		$mail->to = $owner->email;
		$mail->from = $system_config->from_email;
		$mail->fromName = $system_config->from_email_name;

		$template = MailTemplate::get('sendOrder');
		$mail->subject = $template->replaceTitle(
				array(
						MailTemplate::REPLACE_TITLE=> $item->title,
				)
				);
		$mail->body = $template->replaceMessage(
				array(
						MailTemplate::REPLACE_USER_NAME => getUserName(),
						MailTemplate::REPLACE_TITLE=> $item->title,
						MailTemplate::REPLACE_OWNER_NAME=> $owner->name,
						MailTemplate::REPLACE_CATEGORY_NAME=> $item->category_name,
						MailTemplate::REPLACE_USER_HASH_ID=> getHashKey(),
						MailTemplate::REPLACE_ORDER_URL=> getContextRoot().'/user/negotiation.php?item_id='.$this->item_id.'&user_id='.getUserId(),
				)
				);
		$mail->sendMail();
	}

	private function sendProcedure() {

	    $system_config = SystemConfig::select();
	    $item = new Item();
		$item->select($this->item_id);
		$owner = new User();
		$owner->select($item->owner_id);
		$user = new User();
		$user->select($this->user_id);

		$mail = new Mail();
		$mail->from = $system_config->from_email;
		$mail->fromName = $system_config->from_email_name;

		switch ($this->state) {
			case 1:
				$mail->to = $owner->email;
				$template = MailTemplate::get('sendProcedure1');
				break;
			case 2:
				$mail->to = $user->email;
				$template = MailTemplate::get('sendProcedure2');
				break;
			case 3:
				$mail->to = $owner->email;
				$template = MailTemplate::get('sendProcedure3');
				break;
			case 0:
			default:
				return;
		}
		$mail->subject = $template->replaceTitle(
				array(
						MailTemplate::REPLACE_TITLE=> $item->title,
				)
				);
		$mail->body = $template->replaceMessage(
				array(
						MailTemplate::REPLACE_USER_NAME => $user->name,
						MailTemplate::REPLACE_TITLE=> $item->title,
						MailTemplate::REPLACE_OWNER_NAME=> $owner->name,
						MailTemplate::REPLACE_CATEGORY_NAME=> $item->category_name,
						MailTemplate::REPLACE_USER_HASH_ID=> getHashKey(),
						MailTemplate::REPLACE_NEGOTIATION_URL=> getContextRoot().'/user/negotiation.php?item_id='.$this->item_id.'&user_id='.$this->user_id
				)
				);
		$mail->sendMail();
	}

	private function sendOrderConpleteMail($item, $owner) {

	    $system_config = SystemConfig::select();
	    $mail = new Mail();
		$mail->to = getEmail();
		$mail->from = $system_config->from_email;
		$mail->fromName = $system_config->from_email_name;

		$template = MailTemplate::get('sendOrderComplete');
		$mail->subject = $template->replaceTitle(
				array(
						MailTemplate::REPLACE_TITLE=> $item->title,
				)
				);
		$mail->body = $template->replaceMessage(
				array(
						MailTemplate::REPLACE_USER_NAME => getUserName(),
						MailTemplate::REPLACE_TITLE=> $item->title,
						MailTemplate::REPLACE_OWNER_NAME=> $owner->name,
						MailTemplate::REPLACE_CATEGORY_NAME=> $item->category_name,
						MailTemplate::REPLACE_USER_HASH_ID=> getHashKey(),
						MailTemplate::REPLACE_ORDER_URL=> getContextRoot().'/user/negotiation.php?item_id='.$this->item_id.'&user_id='.getUserId(),
				)
				);
		$mail->sendMail();
	}

	private function sendRequestOrderMail($item, $owner) {

	    $system_config = SystemConfig::select();
	    $mail = new Mail();
		$mail->to = $owner->email;
		$mail->from = $system_config->from_email;
		$mail->fromName = $system_config->from_email_name;

		$template = MailTemplate::get('sendRequestOrder');
		$mail->subject = $template->replaceTitle(
				array(
						MailTemplate::REPLACE_TITLE=> $item->title,
				)
				);
		$mail->body = $template->replaceMessage(
				array(
						MailTemplate::REPLACE_USER_NAME => getUserName(),
						MailTemplate::REPLACE_TITLE=> $item->title,
						MailTemplate::REPLACE_OWNER_NAME=> $owner->name,
						MailTemplate::REPLACE_CATEGORY_NAME=> $item->category_name,
						MailTemplate::REPLACE_USER_HASH_ID=> getHashKey(),
						MailTemplate::REPLACE_ORDER_URL=> getContextRoot().'/user/negotiation.php?item_id='.$this->item_id.'&user_id='.getUserId(),
				)
				);
		$mail->sendMail();
	}

	private function sendRequestOrderConpleteMail($item, $owner) {

	    $system_config = SystemConfig::select();
	    $mail = new Mail();
		$mail->to = getEmail();
		$mail->from = $system_config->from_email;
		$mail->fromName = $system_config->from_email_name;

		$template = MailTemplate::get('sendRequestOrderComplete');
		$mail->subject = $template->replaceTitle(
				array(
						MailTemplate::REPLACE_TITLE=> $item->title,
				)
				);
		$mail->body = $template->replaceMessage(
				array(
						MailTemplate::REPLACE_USER_NAME => getUserName(),
						MailTemplate::REPLACE_TITLE=> $item->title,
						MailTemplate::REPLACE_OWNER_NAME=> $owner->name,
						MailTemplate::REPLACE_CATEGORY_NAME=> $item->category_name,
						MailTemplate::REPLACE_USER_HASH_ID=> getHashKey(),
						MailTemplate::REPLACE_ORDER_URL=> getContextRoot().'/user/negotiation.php?item_id='.$this->item_id.'&user_id='.getUserId(),
				)
				);
		$mail->sendMail();
	}

	private function sendMailEvaluateByBuyer() {

	    $system_config = SystemConfig::select();
	    $owner = new User();
		$owner->select($this->owner_id);
		$user = new User();
		$user->select($this->user_id);
		$item = new Item();
		$item->select($this->item_id);

		$mail = new Mail();
		$mail->to = $user->email;
		$mail->from = $system_config->from_email;
		$mail->fromName = $system_config->from_email_name;

		$template = MailTemplate::get('sendEvaluateByBuyer');
		$mail->subject = $template->replaceTitle(
				array(
						MailTemplate::REPLACE_TITLE=> $item->title,
						MailTemplate::REPLACE_EVALUATE=> Evaluate::toEvaluateText($this->buyer_evaluate),
				)
				);
		$mail->body = $template->replaceMessage(
				array(
						MailTemplate::REPLACE_USER_NAME => $user->name,
						MailTemplate::REPLACE_TITLE=> $item->title,
						MailTemplate::REPLACE_OWNER_NAME=> $owner->name,
						MailTemplate::REPLACE_OWNER_HASH_ID=> $owner->hash_id,
						MailTemplate::REPLACE_CATEGORY_NAME=> $item->category_name,
						MailTemplate::REPLACE_USER_HASH_ID=> $user->hash_id,
						MailTemplate::REPLACE_EVALUATE_URL=> getContextRoot().'/evaluate.php?user_id='.$user->id,
				)
				);
		$mail->sendMail();
	}

	private function sendMailEvaluateBySeller() {

	    $system_config = SystemConfig::select();
	    $owner = new User();
		$owner->select($this->owner_id);
		$user = new User();
		$user->select($this->user_id);
		$item = new Item();
		$item->select($this->item_id);

		$mail = new Mail();
		$mail->to = $owner->email;
		$mail->from = $system_config->from_email;
		$mail->fromName = $system_config->from_email_name;

		$template = MailTemplate::get('sendEvaluateBySeller');
		$mail->subject = $template->replaceTitle(
				array(
						MailTemplate::REPLACE_TITLE=> $item->title,
						MailTemplate::REPLACE_EVALUATE=> Evaluate::toEvaluateText($this->seller_evaluate),
				)
				);
		$mail->body = $template->replaceMessage(
				array(
						MailTemplate::REPLACE_USER_NAME => $user->name,
						MailTemplate::REPLACE_TITLE=> $item->title,
						MailTemplate::REPLACE_OWNER_NAME=> $owner->name,
						MailTemplate::REPLACE_OWNER_HASH_ID=> $owner->hash_id,
						MailTemplate::REPLACE_CATEGORY_NAME=> $item->category_name,
						MailTemplate::REPLACE_USER_HASH_ID=> $user->hash_id,
						MailTemplate::REPLACE_EVALUATE_URL=> getContextRoot().'/evaluate.php?user_id='.$owner->id,
				)
				);
		$mail->sendMail();
	}

	/**
	 * 振込先通知（ユーザーへ）
	 */
	private function sendMailBankTransfer() {
	    $system_config = SystemConfig::select();
	    $owner = new User();
	    $owner->select($this->owner_id);
	    $user = new User();
	    $user->select($this->user_id);
	    $item = new Item();
	    $item->select($this->item_id);

	    $mail = new Mail();
	    $mail->to = $user->email;
	    $mail->from = $system_config->from_email;
	    $mail->fromName = $system_config->from_email_name;

	    $template = MailTemplate::get('sendBankTransfer');
	    $mail->subject = $template->replaceTitle(
	        array(
	            MailTemplate::REPLACE_TITLE=> $item->title,
	            MailTemplate::REPLACE_EVALUATE=> Evaluate::toEvaluateText($this->seller_evaluate),
	        )
	        );
	    $mail->body = $template->replaceMessage(
	        array(
	            MailTemplate::REPLACE_USER_NAME => $user->name,
	            MailTemplate::REPLACE_TITLE=> $item->title,
	            MailTemplate::REPLACE_OWNER_NAME=> $owner->name,
	            MailTemplate::REPLACE_OWNER_HASH_ID=> $owner->hash_id,
	            MailTemplate::REPLACE_CATEGORY_NAME=> $item->category_name,
	            MailTemplate::REPLACE_USER_HASH_ID=> $user->hash_id,
	            MailTemplate::REPLACE_EVALUATE_URL=> getContextRoot().'/evaluate.php?user_id='.$owner->id,

	            MailTemplate::REPLACE_FINANCIAL_INSTITUTION_NAME=> $system_config->financial_institution_name,
	            MailTemplate::REPLACE_BRANCH_NAME=> $system_config->branch_name,
	            MailTemplate::REPLACE_DEPOSIT_TYPE=> $system_config->get_deposit_type_name(),
	            MailTemplate::REPLACE_ACCOUNT_NUMBER=> $system_config->account_number,
	            MailTemplate::REPLACE_ACCOUNT_HOLDER=> $system_config->account_holder,

	            MailTemplate::REPLACE_AMOUNT=>number_format($this->item->price),
	            MailTemplate::REPLACE_ORDER_URL=> getContextRoot().'/user/negotiation.php?item_id='.$this->item_id.'&user_id='.$this->user_id,
	        )
	        );
	    $mail->sendMail();

	}

	/**
	 * カード決済（ユーザーへ）
	 */
	private function sendMailCardPayment() {
	    $system_config = SystemConfig::select();
	    $owner = new User();
	    $owner->select($this->owner_id);
	    $user = new User();
	    $user->select($this->user_id);
	    $item = new Item();
	    $item->select($this->item_id);

	    $mail = new Mail();
	    $mail->to = $user->email;
	    $mail->from = $system_config->from_email;
	    $mail->fromName = $system_config->from_email_name;

	    $template = MailTemplate::get('sendCardPayment');
	    $mail->subject = $template->replaceTitle(
	        array(
	            MailTemplate::REPLACE_TITLE=> $item->title,
	            MailTemplate::REPLACE_EVALUATE=> Evaluate::toEvaluateText($this->seller_evaluate),
	        )
	        );
	    $mail->body = $template->replaceMessage(
	        array(
	            MailTemplate::REPLACE_USER_NAME => $user->name,
	            MailTemplate::REPLACE_TITLE=> $item->title,
	            MailTemplate::REPLACE_OWNER_NAME=> $owner->name,
	            MailTemplate::REPLACE_OWNER_HASH_ID=> $owner->hash_id,
	            MailTemplate::REPLACE_CATEGORY_NAME=> $item->category_name,
	            MailTemplate::REPLACE_USER_HASH_ID=> $user->hash_id,
	            MailTemplate::REPLACE_EVALUATE_URL=> getContextRoot().'/evaluate.php?user_id='.$owner->id,

	            MailTemplate::REPLACE_FINANCIAL_INSTITUTION_NAME=> $system_config->financial_institution_name,
	            MailTemplate::REPLACE_BRANCH_NAME=> $system_config->branch_name,
	            MailTemplate::REPLACE_DEPOSIT_TYPE=> $system_config->get_deposit_type_name(),
	            MailTemplate::REPLACE_ACCOUNT_NUMBER=> $system_config->account_number,
	            MailTemplate::REPLACE_ACCOUNT_HOLDER=> $system_config->account_holder,

	            MailTemplate::REPLACE_AMOUNT=>number_format($this->item->price),
	            MailTemplate::REPLACE_ORDER_URL=> getContextRoot().'/user/negotiation.php?item_id='.$this->item_id.'&user_id='.$this->user_id,
	        )
	        );
	    $mail->sendMail();

	}

	/**
	 * カード決済（管理者へ）
	 */
	private function sendMailCardPaymentAdmin() {
	    $system_config = SystemConfig::select();
	    $owner = new User();
	    $owner->select($this->owner_id);
	    $user = new User();
	    $user->select($this->user_id);
	    $item = new Item();
	    $item->select($this->item_id);

	    $mail = new Mail();
	    $mail->to = $system_config->to_email;
	    $mail->from = $system_config->from_email;
	    $mail->fromName = $system_config->from_email_name;

	    $template = MailTemplate::get('sendCardPaymentAdmin');
	    $mail->subject = $template->replaceTitle(
	        array(
	            MailTemplate::REPLACE_TITLE=> $item->title,
	            MailTemplate::REPLACE_EVALUATE=> Evaluate::toEvaluateText($this->seller_evaluate),
	        )
	        );
	    $mail->body = $template->replaceMessage(
	        array(
	            MailTemplate::REPLACE_USER_NAME => $user->name,
	            MailTemplate::REPLACE_TITLE=> $item->title,
	            MailTemplate::REPLACE_OWNER_NAME=> $owner->name,
	            MailTemplate::REPLACE_OWNER_HASH_ID=> $owner->hash_id,
	            MailTemplate::REPLACE_CATEGORY_NAME=> $item->category_name,
	            MailTemplate::REPLACE_USER_HASH_ID=> $user->hash_id,
	            MailTemplate::REPLACE_EVALUATE_URL=> getContextRoot().'/evaluate.php?user_id='.$owner->id,

	            MailTemplate::REPLACE_FINANCIAL_INSTITUTION_NAME=> $system_config->financial_institution_name,
	            MailTemplate::REPLACE_BRANCH_NAME=> $system_config->branch_name,
	            MailTemplate::REPLACE_DEPOSIT_TYPE=> $system_config->get_deposit_type_name(),
	            MailTemplate::REPLACE_ACCOUNT_NUMBER=> $system_config->account_number,
	            MailTemplate::REPLACE_ACCOUNT_HOLDER=> $system_config->account_holder,

	            MailTemplate::REPLACE_AMOUNT=>number_format($this->item->price),
	            MailTemplate::REPLACE_ORDER_URL=> getContextRoot().'/user/negotiation.php?item_id='.$this->item_id.'&user_id='.$this->user_id,
	        )
	        );
	    $mail->sendMail();

	}

	/**
	 * 振込実施（管理者へ）
	 */
	private function sendMailBankTransferAdmin() {
	    $system_config = SystemConfig::select();
	    $owner = new User();
	    $owner->select($this->owner_id);
	    $user = new User();
	    $user->select($this->user_id);
	    $item = new Item();
	    $item->select($this->item_id);

	    $mail = new Mail();
	    $mail->to = $system_config->to_email;
	    $mail->from = $system_config->from_email;
	    $mail->fromName = $system_config->from_email_name;

	    $template = MailTemplate::get('sendBankTransferAdmin');
	    $mail->subject = $template->replaceTitle(
	        array(
	            MailTemplate::REPLACE_TITLE=> $item->title,
	            MailTemplate::REPLACE_EVALUATE=> Evaluate::toEvaluateText($this->seller_evaluate),
	        )
	        );
	    $mail->body = $template->replaceMessage(
	        array(
	            MailTemplate::REPLACE_USER_NAME => $user->name,
	            MailTemplate::REPLACE_TITLE=> $item->title,
	            MailTemplate::REPLACE_OWNER_NAME=> $owner->name,
	            MailTemplate::REPLACE_OWNER_HASH_ID=> $owner->hash_id,
	            MailTemplate::REPLACE_CATEGORY_NAME=> $item->category_name,
	            MailTemplate::REPLACE_USER_HASH_ID=> $user->hash_id,
	            MailTemplate::REPLACE_EVALUATE_URL=> getContextRoot().'/evaluate.php?user_id='.$owner->id,

	            MailTemplate::REPLACE_FINANCIAL_INSTITUTION_NAME=> $system_config->financial_institution_name,
	            MailTemplate::REPLACE_BRANCH_NAME=> $system_config->branch_name,
	            MailTemplate::REPLACE_DEPOSIT_TYPE=> $system_config->get_deposit_type_name(),
	            MailTemplate::REPLACE_ACCOUNT_NUMBER=> $system_config->account_number,
	            MailTemplate::REPLACE_ACCOUNT_HOLDER=> $system_config->account_holder,

	            MailTemplate::REPLACE_AMOUNT=>number_format($this->item->price),
	            MailTemplate::REPLACE_ORDER_URL=> getContextRoot().'/user/negotiation.php?item_id='.$this->item_id.'&user_id='.$this->user_id,
	        )
	        );
	    $mail->sendMail();

	}

	/**
	 * 入金完了（出品者へ）
	 */
	private function sendMailPaymentComplete() {
	    $system_config = SystemConfig::select();
	    $owner = new User();
	    $owner->select($this->owner_id);
	    $user = new User();
	    $user->select($this->user_id);
	    $item = new Item();
	    $item->select($this->item_id);

	    $mail = new Mail();
	    $mail->to = $owner->email;
	    $mail->from = $system_config->from_email;
	    $mail->fromName = $system_config->from_email_name;

	    $template = MailTemplate::get('sendPaymentComplete');
	    $mail->subject = $template->replaceTitle(
	        array(
	            MailTemplate::REPLACE_TITLE=> $item->title,
	            MailTemplate::REPLACE_EVALUATE=> Evaluate::toEvaluateText($this->seller_evaluate),
	        )
	        );
	    $mail->body = $template->replaceMessage(
	        array(
	            MailTemplate::REPLACE_USER_NAME => $user->name,
	            MailTemplate::REPLACE_TITLE=> $item->title,
	            MailTemplate::REPLACE_OWNER_NAME=> $owner->name,
	            MailTemplate::REPLACE_OWNER_HASH_ID=> $owner->hash_id,
	            MailTemplate::REPLACE_CATEGORY_NAME=> $item->category_name,
	            MailTemplate::REPLACE_USER_HASH_ID=> $user->hash_id,
	            MailTemplate::REPLACE_EVALUATE_URL=> getContextRoot().'/evaluate.php?user_id='.$owner->id,

	            MailTemplate::REPLACE_FINANCIAL_INSTITUTION_NAME=> $system_config->financial_institution_name,
	            MailTemplate::REPLACE_BRANCH_NAME=> $system_config->branch_name,
	            MailTemplate::REPLACE_DEPOSIT_TYPE=> $system_config->get_deposit_type_name(),
	            MailTemplate::REPLACE_ACCOUNT_NUMBER=> $system_config->account_number,
	            MailTemplate::REPLACE_ACCOUNT_HOLDER=> $system_config->account_holder,

	            MailTemplate::REPLACE_AMOUNT=>number_format($this->item->price),
	            MailTemplate::REPLACE_ORDER_URL=> getContextRoot().'/user/negotiation.php?item_id='.$this->item_id.'&user_id='.$this->user_id,
	        )
	        );
	    $mail->sendMail();
	}

	/**
	 * 入金完了（配送なし）（出品者へ）
	 */
	private function sendMailPaymentCompleteWithNonDelivery() {
	    $system_config = SystemConfig::select();
	    $owner = new User();
	    $owner->select($this->owner_id);
	    $user = new User();
	    $user->select($this->user_id);
	    $item = new Item();
	    $item->select($this->item_id);

	    $mail = new Mail();
	    $mail->to = $owner->email;
	    $mail->from = $system_config->from_email;
	    $mail->fromName = $system_config->from_email_name;

	    $template = MailTemplate::get('sendPaymentCompleteWithNonDelivery');
	    $mail->subject = $template->replaceTitle(
	        array(
	            MailTemplate::REPLACE_TITLE=> $item->title,
	            MailTemplate::REPLACE_EVALUATE=> Evaluate::toEvaluateText($this->seller_evaluate),
	        )
	        );
	    $mail->body = $template->replaceMessage(
	        array(
	            MailTemplate::REPLACE_USER_NAME => $user->name,
	            MailTemplate::REPLACE_TITLE=> $item->title,
	            MailTemplate::REPLACE_OWNER_NAME=> $owner->name,
	            MailTemplate::REPLACE_OWNER_HASH_ID=> $owner->hash_id,
	            MailTemplate::REPLACE_CATEGORY_NAME=> $item->category_name,
	            MailTemplate::REPLACE_USER_HASH_ID=> $user->hash_id,
	            MailTemplate::REPLACE_EVALUATE_URL=> getContextRoot().'/evaluate.php?user_id='.$owner->id,

	            MailTemplate::REPLACE_FINANCIAL_INSTITUTION_NAME=> $system_config->financial_institution_name,
	            MailTemplate::REPLACE_BRANCH_NAME=> $system_config->branch_name,
	            MailTemplate::REPLACE_DEPOSIT_TYPE=> $system_config->get_deposit_type_name(),
	            MailTemplate::REPLACE_ACCOUNT_NUMBER=> $system_config->account_number,
	            MailTemplate::REPLACE_ACCOUNT_HOLDER=> $system_config->account_holder,

	            MailTemplate::REPLACE_AMOUNT=>number_format($this->item->price),
	            MailTemplate::REPLACE_ORDER_URL=> getContextRoot().'/user/negotiation.php?item_id='.$this->item_id.'&user_id='.$this->user_id,
	        )
	        );
	    $mail->sendMail();
	}



	/**
	 * 入金状態
	 * @return string
	 */
	public function getCashFlowName() {
	    switch ($this->cash_flow) {
	        case 0:
	            return '未入金';
	        case 1:
	            return '入金処理中';
	        case 2:
	            return '入金済';
	        default:
	            break;
	    }
	    return '';
	}

	public function getStateName() {
	    switch ($this->state) {
	        case 0:
	            return '取引開始';
	        case 1:
	            return 'お支払い';
	        case 2:
	            return '納品発送';
	        case 3:
	            return '完了';
	        default:
	            break;
	    }
	    return '';
	}

}
