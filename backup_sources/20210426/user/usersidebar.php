<?php include_once(__DIR__ . '/../common/util.php'); ?>
<?php
    $entity = new User();
    $sns = new Sns();
    if (isLogin()) {
        $entity->select(getUserId());
        $sns->select(getUserId());
    }

    $icon_url = $entity->get_icon_url();
    $header_image_url = $entity->get_header_image_url();
?>

<div class="com-sidebar ">

    <?php if (isLogin()): ?>
        <?php if (getSettlePlan() > 0): ?>

        <div class="com-sidebar-profile">

            <?php if (!empty($header_image_url)): ?>
                <div class="prof" style="background: url('<?= $header_image_url ?>');" alt=""></div>
            <?php else:?>
                <img src="<?php echo HOME_URL; ?>/common/assets/img/mypage/profile-bg.png" alt="">
            <?php endif;?>

            <div class="com-sidebar-profile-content__img">

                <?php if (!empty($icon_url)): ?>
                    <img src="<?= $icon_url ?>">
                <?php else:?>
                    <img src="<?php echo HOME_URL; ?>/common/assets/img/mypage/profile.png" alt="">
                <?php endif;?>
            </div>
            <div class="com-sidebar-profile-content__name">
                <p><?= getUserName(); ?>さんの<br>
                    マイページ</p>
            </div>
        </div>
        <div class="com-sidebar__item">
            <h4>出品する</h4>
        </div>
        <div class="com-sidebar-item__link">
            <div class="com-sidebar-item__link-txt">
                <a href="<?php echo HOME_URL; ?>/user/sell.php">商品登録</a>
            </div>
            <div class="com-sidebar-item__link-txt">
                <a href="<?php echo HOME_URL; ?>/user/sellinglist.php">出品アイテム</a>
            </div>
            <div class="com-sidebar-item__link-txt">
                <a href="<?php echo HOME_URL; ?>/user/negotiation_list_sell.php">質問・交渉中</a>
            </div>
            <div class="com-sidebar-item__link-txt">
                <a href="<?php echo HOME_URL; ?>/user/soldout_transaction.php">販売済・取引中</a>
            </div>
        </div>

        <?php endif; ?>

    <div class="com-sidebar__item">
        <h4>購入する</h4>
    </div>
    <div class="com-sidebar-item__link">
        <div class="com-sidebar-item__link-txt">
            <a href="<?php echo HOME_URL; ?>/user/question_negoty.php">購入・交渉中</a>
        </div>
        <div class="com-sidebar-item__link-txt">
            <a href="<?php echo HOME_URL; ?>/user/exhibited_list.php">購入済み</a>
        </div>
        <div class="com-sidebar-item__link-txt">
            <a href="<?php echo HOME_URL; ?>/user/favorites_list.php">お気に入り一覧</a>
        </div>
        <div class="com-sidebar-item__link-txt">
            <a href="<?php echo HOME_URL; ?>/user/request.php">購入希望募集投稿</a>
        </div>
        <div class="com-sidebar-item__link-txt">
            <a href="<?php echo HOME_URL; ?>/user/request_list.php">購入希望募集履歴</a>
        </div>
    </div>
    <div class="com-sidebar__item">
        <h4>アカウント</h4>
    </div>
    <div class="com-sidebar-item__link">
        <div class="com-sidebar-item__link-txt">
            <a href="<?php echo HOME_URL; ?>/user/userinfo.php">ユーザー情報変更</a>
        </div>
        <div class="com-sidebar-item__link-txt">
            <a href="<?php echo HOME_URL; ?>/user/account_number_info.php">口座情報変更</a>
        </div>
        <div class="com-sidebar-item__link-txt">
            <a href="<?php echo HOME_URL; ?>/user/cash_flow_history.php">出金申請/履歴</a>
        </div>
        <div class="com-sidebar-item__link-txt">
            <a href="<?php echo HOME_URL; ?>/user/evaluate.php">評価一覧</a>
        </div>
        <div class="com-sidebar-item__link-txt">
            <a href="<?php echo HOME_URL; ?>/user/block_user_list.php">ブラックリスト登録</a>
        </div>
        <div class="com-sidebar-item__link-txt">

            <?php if (isIdentification()): ?>
                <a class=""
                   href="<?php echo HOME_URL; ?>/user/upload_identification.php">本人確認済<i class="far fa-check-circle text-success pl-1"></i></a>
            <?php else: ?>
                <a href="<?php echo HOME_URL; ?>/user/upload_identification.php">本人確認書類の提出</a>
            <?php endif; ?>

        </div>
        <!-- <div class="com-sidebar-item__link-txt">
            <a href="#">退会について</a>
        </div> -->
    </div>

    <?php endif; ?>
</div>
