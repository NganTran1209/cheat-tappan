<?php 
$title_page = '出品する';
$description_page = '欲しい商品を画像で確認・届いた商品を画像で照合！気持ち良い取引が叶うオンラインマーケット。';
 ?>
<?php include_once(__DIR__ . '/../common/util.php'); ?>
<?php include_once(__DIR__.'/../common/login_check.php'); ?>
<?php
function console_log($output, $with_script_tags = true) {
    $js_code = 'console.log(' . json_encode($output, JSON_HEX_TAG) . 
');';
    if ($with_script_tags) {
        $js_code = '<script>' . $js_code . '</script>';
    }
    echo $js_code;
}
?>
<?php
//ページタイトルはここで設定させる
$user_page_title = "出品する";

//if (!isSelling()) {
//    setMessage('出品が許可されておりません。出品許可を得るためには「本人確認書類」の提出をお願いします。');
//    header('Location: ' . getContextRoot() . '/user/upload_identification.php');
//    exit;
//}
//if (!isIdentification()) {
//    setMessage('本人確認が完了しておりません。');
//    header('Location: ' . getContextRoot() . '/user/upload_identification.php');
//    exit;
//}

$isOffer = false;
if (isset($_POST['offer']) && $_POST['offer'] == 'offer') {
    $isOffer = true;
} else {
    unset($_SESSION['targetRequest']);
}

if (isset($_POST['action']) && ($_POST['action'] == 'modify' || $_POST['action'] == 'cancel-load-tmp')) {
    $sell_item = $_SESSION['item'];
} elseif (isset($_POST['action']) && $_POST['action'] == 'load-tmp') {
    $sell_item = new Item();
    $sell_item->selectTmp($_POST['tmp-id']);
    setMessage('下書きを読み込みました。', true);
} elseif (isset($_POST['action']) && $_POST['action'] == 'save-tmp' && empty($_POST['title'])) {
    echo json_encode('下書き登録はタイトルは必須です。');
    exit();
} else if (isset($_POST['title']) || isset($_POST['action'])) {
//  ini_set('display_errors', 0);
    $sell_item = new Item();
    $sell_item->title = trim($_POST['title']);
    $sell_item->category = empty($_POST['category']) ? null : $_POST['category'];
    $sell_item->item_state = isset($_POST['item_state']) ? $_POST['item_state'] : null;
    // $sell_item->item_state = $_POST['item_state'] == "未選択"
    $sell_item->remarks = trim($_POST['remarks']);
    $sell_item->pref = empty($_POST['pref']) ? null : $_POST['pref'];
    $sell_item->carriage_plan = empty($_POST['carriage_plan']) ? null : $_POST['carriage_plan'];
    $sell_item->carriage_type = empty($_POST['carriage_type']) ? null : $_POST['carriage_type'];
    $sell_item->delivery_type = empty($_POST['delivery_type']) ? null : $_POST['delivery_type'];
    $sell_item->show_evaluate = empty($_POST['show_evaluate']) ? null : $_POST['show_evaluate'];
    $sell_item->with_delivery = empty($_POST['with_delivery']) ? null : $_POST['with_delivery'];
    //$sell_item->delivery_price= $_POST['delivery_price'];
    $sell_item->delivery_price = 0;
    $sell_item->price = empty($_POST['price']) ? null : $_POST['price'];
    if (!empty($_FILES['photo1']) && $_FILES['photo1']['error'] == 0) {
        $sell_item->photo1 = rotateImage(file_get_contents($_FILES['photo1']['tmp_name']));
    } else {
        $sell_item->photo1 = null;
    }
    if (!empty($_FILES['photo2']) && $_FILES['photo2']['error'] == 0) {
        $sell_item->photo2 = rotateImage(file_get_contents($_FILES['photo2']['tmp_name']));
    } else {
        $sell_item->photo2 = null;
    }
    if (!empty($_FILES['photo3']) && $_FILES['photo3']['error'] == 0) {
        $sell_item->photo3 = rotateImage(file_get_contents($_FILES['photo3']['tmp_name']));
    } else {
        $sell_item->photo3 = null;
    }
    if (!empty($_FILES['photo4']) && $_FILES['photo4']['error'] == 0) {
        $sell_item->photo4 = rotateImage(file_get_contents($_FILES['photo4']['tmp_name']));
    } else {
        $sell_item->photo4 = null;
    }
    if (!empty($_FILES['photo5']) && $_FILES['photo5']['error'] == 0) {
        $sell_item->photo5 = rotateImage(file_get_contents($_FILES['photo5']['tmp_name']));
    } else {
        $sell_item->photo5 = null;
    }
    if (!empty($_FILES['photo6']) && $_FILES['photo6']['error'] == 0) {
        $sell_item->photo6 = rotateImage(file_get_contents($_FILES['photo6']['tmp_name']));
    } else {
        $sell_item->photo6 = null;
    }
    if (!empty($_FILES['photo7']) && $_FILES['photo7']['error'] == 0) {
        $sell_item->photo7 = rotateImage(file_get_contents($_FILES['photo7']['tmp_name']));
    } else {
        $sell_item->photo7 = null;
    }
    if (!empty($_FILES['photo8']) && $_FILES['photo8']['error'] == 0) {
        $sell_item->photo8 = rotateImage(file_get_contents($_FILES['photo8']['tmp_name']));
    } else {
        $sell_item->photo8 = null;
    }
    if (!empty($_FILES['photo9']) && $_FILES['photo9']['error'] == 0) {
        $sell_item->photo9 = rotateImage(file_get_contents($_FILES['photo9']['tmp_name']));
    } else {
        $sell_item->photo9 = null;
    }
    if (!empty($_FILES['photo10']) && $_FILES['photo10']['error'] == 0) {
        $sell_item->photo10 = rotateImage(file_get_contents($_FILES['photo10']['tmp_name']));
    } else {
        $sell_item->photo10 = null;
    }

    if (!empty($_POST['free_items'])) {
        foreach ($_POST['free_items'] as $key => $value) {
            $free_info = new ItemFreeInfo();
            $free_info->free_input_item_id = $key;
            $free_info->free_input_item_value_id = $value;
            $sell_item->free_input[$key] = $free_info;
        }
    }

    if (isset($_POST['action']) && $_POST['action'] == 'validation') {
        $sell_item->validate();
        $result = getMessage();
        echo json_encode($result);
        exit();
    }

    if (isset($_POST['action']) && $_POST['action'] == 'move-load-tmp') {
        $_SESSION['item'] = $sell_item;
        header('Location: ' . getContextRoot() . '/user/draft_list.php');
        exit();
    }

    if (isset($_POST['action']) && $_POST['action'] == 'save-tmp') {
        $tmps = $sell_item->selectTmpFromUser(getUserId());
        if (count($tmps) < MAX_TEMP_COUNT) {
            $sell_item->registTmp();
            echo json_encode('下書きを保存しました。');
            exit();
        } else {
            echo json_encode('下書きは[' . MAX_TEMP_COUNT . '件]まで登録できます。');
            exit();
        }
    }

    if ($sell_item->validate()) {
        $_SESSION['item'] = $sell_item;

        header('Location: ' . getContextRoot() . '/user/sell_confirm.php');
        exit();
    }
} else {
    $sell_item = new Item();
    $sell_item->pref = getUserPref();
}
unset($_SESSION['item']);
$system_config = SystemConfig::select();
$title_page = '出品する';
?>
<?php include(__DIR__ . '/../user_header.php'); ?>


<script src="https://code.jquery.com/jquery-3.4.1.min.js"
            integrity="sha256-CSXorXvZcTkaix6Yvo6HppcZGetbYMGWSFlBw8HfCJo="
            crossorigin="anonymous"></script>
<!-- <script src="<?php echo HOME_URL; ?>/common/assets/js/jquery.min.js"></script> -->

<script>
    function confirm() {
        var requestData = new FormData();

        var $f = $('#sell-form');
        var valueData = $f.serializeArray();
        $.each(valueData, function (key, input) {
            requestData.append(input.name, input.value);
        });
        requestData.append('action', 'validation');

        var fileData = $('input[name="photo1"]')[0].files;
        for (var i = 0; i < fileData.length; i++) {
            requestData.append("photo1", fileData[i]);
        }
        var result = $.ajax({
            url: './sell.php',
            type: 'post',
            async: false,
            processData: false,
            contentType: false,
            data: requestData
        }).responseText;

        var obj = JSON.parse(result);
        
        if (obj.msg.length > 0) {
            window.alert(obj.msg);
            $('#title_id').val("");
            $('#remarks_id').val("");
            $('#category_id').val("");
            $('#item_state_id').val("");
            $('#pref_id').val("");
            $('#delivery_type_id').val("");
            $('#carriage_plan_id').val("");
            $('#carriage_type_id').val("");
            $('#price_id').val("");
            $('#with_delivery').prop('checked', false); // Unchecks it
            $('#delivery_type_id').prop('disabled', true);
            $('#carriage_plan_id').prop('disabled', true);
            $('#carriage_type_id').prop('disabled', true);
            document.querySelectorAll('.custom-file-input').forEach(function(ele) {
                ele.value = "";
                var nextSibling = ele.target.nextElementSibling;
                nextSibling.innerText = "ファイル選択";
              
            });
            // $('#photo').val("");
            // $('#photo').next('label').html("ファイル選択");
	        
            return false;
        }
        return true;
    }

    function saveTmp() {
        var requestData = new FormData();

        var $f = $('#sell-form');
        var valueData = $f.serializeArray();
        $.each(valueData, function (key, input) {
            requestData.append(input.name, input.value);
        });
        requestData.append('action', 'save-tmp');

        var fileData = $('input[name="photo1"]')[0].files;
        for (var i = 0; i < fileData.length; i++) {
            requestData.append("photo1", fileData[i]);
        }
        var result = $.ajax({
            url: './sell.php',
            type: 'post',
            async: false,
            processData: false,
            contentType: false,
            data: requestData
        }).responseText;

        var obj = JSON.parse(result);

        if (obj.length > 0) {
            window.alert(obj);
            return false;
        }
        return false;
    }

    function loadTmp() {
        submitForm('sell-form', 'action', 'move-load-tmp');
        var $f = $('#tmp-form').submit();
    }

    $(function() {
        $('#with_delivery').on('change', function() {
            var disabled = !$(this).prop('checked');
            $('#delivery_type_id').prop('disabled', disabled);
            $('#carriage_plan_id').prop('disabled', disabled);
            $('#carriage_type_id').prop('disabled', disabled);
        });
    });
</script>


<div class="com-container bg-yellow">
    <?php include('usersidebar.php'); ?>
        <div class="com-content">
            <form class="c-form01" id="sell-form" method="post" enctype="multipart/form-data" onsubmit="return confirm();">
                <div class="content-title">
                    <h3><span>出品する</span></h3>
                </div>
                <div class="sell-content">
                    <div class="product-info">
                        <h3>商品の情報</h3>

                        <div class="purchase-wishes-content-title each-product title-add">
                            <p class='required-field'>タイトル</p>
                            <input type="text" name="title" id="title_id" 
                                value="<?= $sell_item->title ?>" placeholder="タイトル">
                               <?php console_log($sell_item->title); ?>
                        </div>

                        <div class="purchase-wishes-content-title each-product">
                            <p class='required-field'>商品画像</p>
                            <div class="custom-file mb-2">
                                <input type="file" class="custom-file-input" id="photo1" name="photo1" aria-describedby="photo1Add">
                                <label class="custom-file-label" for="photo1">ファイル選択</label>
                            </div>
                            <img id="photo1-preview" width="150" heigth="150"/>

                            <div class="custom-file mb-2">
                                <input type="file" class="custom-file-input" id="photo2" name="photo2" aria-describedby="photo2Add">
                                <label class="custom-file-label" for="photo2">ファイル選択</label>
                            </div>
                            <img id="photo2-preview" width="150" heigth="150"/>

                            <div class="custom-file mb-2">
                                <input type="file" class="custom-file-input" id="photo3" name="photo3" aria-describedby="photo3Add">
                                <label class="custom-file-label" for="photo3">ファイル選択</label>
                            </div>
                            <img id="photo3-preview" width="150" heigth="150"/>

                            <div class="custom-file mb-2">
                                <input type="file" class="custom-file-input" id="photo4" name="photo4" aria-describedby="photo4Add">
                                <label class="custom-file-label" for="photo4">ファイル選択</label>
                            </div>
                            <img id="photo4-preview" width="150" heigth="150"/>

                            <div class="custom-file mb-2">
                                <input type="file" class="custom-file-input" id="photo5" name="photo5" aria-describedby="photo5Add">
                                <label class="custom-file-label" for="photo5">ファイル選択</label>
                            </div>
                            <img id="photo5-preview" width="150" heigth="150"/>

                            <div class="custom-file mb-2">
                                <input type="file" class="custom-file-input" id="photo6" name="photo6" aria-describedby="photo6Add">
                                <label class="custom-file-label" for="photo6">ファイル選択</label>
                            </div>
                            <img id="photo6-preview" width="150" heigth="150"/>

                            <div class="custom-file mb-2">
                                <input type="file" class="custom-file-input" id="photo7" name="photo7" aria-describedby="photo7Add">
                                <label class="custom-file-label" for="photo7">ファイル選択</label>
                            </div>
                            <img id="photo7-preview" width="150" heigth="150"/>

                            <div class="custom-file mb-2">
                                <input type="file" class="custom-file-input" id="photo8" name="photo8" aria-describedby="photo8Add">
                                <label class="custom-file-label" for="photo8">ファイル選択</label>
                            </div>
                            <img id="photo8-preview" width="150" heigth="150"/>

                            <div class="custom-file mb-2">
                                <input type="file" class="custom-file-input" id="photo9" name="photo9" aria-describedby="photo9Add">
                                <label class="custom-file-label" for="photo9">ファイル選択</label>
                            </div>
                            <img id="photo9-preview" width="150" heigth="150"/>
                            
                            <div class="custom-file mb-2">
                                <input type="file" class="custom-file-input" id="photo10" name="photo10" aria-describedby="photo10Add">
                                <label class="custom-file-label" for="photo10">ファイル選択</label>
                            </div>
                            <img id="photo10-preview" width="150" heigth="150"/>
                            <!-- <div class="row">
                                <img id="photo1-preview" width="100" heigth="100"/>
                                <img id="photo2-preview" width="100" heigth="100"/>
                                <img id="photo3-preview" width="100" heigth="100"/>
                                <img id="photo4-preview" width="100" heigth="100"/>
                                <img id="photo5-preview" width="100" heigth="100"/>
                                <img id="photo6-preview" width="100" heigth="100"/>
                                <img id="photo7-preview" width="100" heigth="100"/>
                                <img id="photo8-preview" width="100" heigth="100"/>
                                <img id="photo9-preview" width="100" heigth="100"/>
                            </div> -->
                        </div>

                       <div class="purchase-wishes-content__content each-product">
                            <p class='required-field'>説明文</p>
                            <p class="product-detail-memo">※最大1.5万文字まで入力できます。</p>
                            <textarea name="remarks" id="remarks_id" cols="30" rows="10" maxlength="20000"><?= $sell_item->remarks ?></textarea>
                        </div>
                        <div class="purchase-wishes-content-title title-add">
                        <?php if ($system_config->enabled_category):?>
                        <div class="product-info-select each-product">
                            <p class='required-field'>カテゴリー</p>
                            <select name="category" id="category_id">
                                <option value="">未選択</option>
                                <?php createCombo('selectCategory', $sell_item->category); ?>
                            </select>
                        </div>
                        <?php endif;?>
                        </div>
                        <div class="purchase-wishes-content-title each-product title-add">
                        <?php if ($system_config->enabled_item_state):?>
                        <div class="product-info-select">
                            <p class='required-field'>商品の状態</p>
                            <select name="item_state" id="item_state_id">
                                <option value="">未選択</option>
                                <?php createCombo('selectItemState', $sell_item->item_state); ?>
                            </select>
                        </div>
                        <?php endif;?>
                        </div>

<!--                         <div class="product-info-select">
                            <p>自由項目</p>
                            <select name="" id="">
                                <option value="0">test</option>
                            </select>
                        </div> -->
                        <div class="purchase-wishes-content-title each-product ">
                        <div class="product-info-select">
                    <?php if ($system_config->enabled_pref):?>
                        <div class="product-info-select each-product">
                            <p>配送地域</p>
                            <select name="pref" id="pref_id">
                                <option value="">都道府県</option>
                                <?php createCombo('selectPref', $sell_item->pref); ?>
                            </select>
                        </div>
                        <?php endif;?>
                        </div>
                        </div>
                    </div>

                    <?php if ($system_config->enabled_delivery_type):?>
                    <div class="product-info">
                        <h3>配送の情報</h3>
                   
                    <div class="product-info-switch">
                        <!-- <div class="product-info-switch__input"> -->
                            <!-- <input type="checkbox" id="switch" />  -->
                            <!-- <input type="checkbox" class="custom-control-input" id="with_delivery" name="with_delivery" id="switch" value="1"<?= $sell_item->with_delivery ? ' checked="checked"' : '' ?>> -->
                            <input type="checkbox" id="with_delivery" name="with_delivery" value="1"<?= $sell_item->with_delivery ? ' checked="checked"' : '' ?>/>
                        <!-- </div> -->
                        <div class="product-info-switch__txt">配送</div>
                    </div>
                    <?php endif;?>
                    
                   
                        <div class="product-info-select each-product">
                            <p>配送方法</p>
                            <select name="delivery_type" id="delivery_type_id"<?= $sell_item->with_delivery ? '' : ' disabled' ?>>
                                <option value="">発送方法</option>
                                <?php createCombo('selectDeliveryType', $sell_item->delivery_type); ?>
                            </select>
                        </div>
                        <div class="product-info-select each-product">
                            <p>配送料の負担</p>
                            <select name="carriage_plan" id="carriage_plan_id"<?= $sell_item->with_delivery ? '' : ' disabled' ?>>
                                <option value="">選択</option>
                                <?php createCombo('selectCarriagePlan', $sell_item->carriage_plan); ?>
                            </select>
                        </div>
                        <div class="purchase-wishes-content-title">
                        <div class="product-info-select each-product">
                            <p>配送の目安</p>
                            <select name="carriage_type" id="carriage_type_id"<?= $sell_item->with_delivery ? '' : ' disabled' ?>>
                                <option value="">選択</option>
                                <?php createCombo('selectCarriageType', $sell_item->carriage_type); ?>
                            </select>
                        </div>
                        </div>
                    </div>
    
                    <div class="product-info">
                    <div class="product-price each-product">
                        <h3>価格の情報<span class="price-memo">(300円〜9,999,999円)</span></h3>
                        <p class='required-field'>価格（税込み価格）</p>
                        <input type="text" type="number" name="price" id = "price_id"
                               value="<?= $sell_item->price ?>"  placeholder="数字のみ（例：10,000円→10000と入力）" max="900000000">
                    </div>
                    </div>
        <!--            <div class="product-check check-margin">
                        <div class="product-check__input">
                            <input type="checkbox" name="show_evaluate" id="show_evaluate" class="add-check"
                                                                      value="1" <?= $sell_item->show_evaluate ? 'checked' : '' ?>>
                        </div>
                   
                        <div class="product-check__txt">
                            <label for="" class="label-add">総合評価の合計が-1以下のユーザーから、質問・交渉、購入をされたくない方はチェックを入れてください。</label>
                        </div>
                        </div>-->
                </div>

                <?php if ($isOffer): ?>
                    <input type="hidden" name="offer" value="offer"/>
                <?php endif; ?>

                <div class="sell-form-btn sell-form-btn-add">
                    <button type="submit" value="確認する">確認する</button>
                <!--    <button>下書きに保存</button>-->
                </div>
<!--                <div class="sell-form-end">
                    <p>※商品画像は保存されません</p>
                </div>-->
            </form>
        </div>
    </div>
    <script>
        document.querySelectorAll('.custom-file-input').forEach(function(ele) {
                ele.addEventListener('change',function(e){
                    var fileName = document.getElementById(e.target.id).files[0].name;
                    var nextSibling = e.target.nextElementSibling
                    nextSibling.innerText = fileName
                    document.getElementById(e.target.id + '-preview').src = window.URL.createObjectURL(this.files[0])
            })
        })
    </script>
<?php include('../footer.php'); ?>
