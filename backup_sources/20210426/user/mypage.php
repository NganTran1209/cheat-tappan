<?php 
$title_page = 'マイページ';
$description_page = '欲しい商品を画像で確認・届いた商品を画像で照合！気持ち良い取引が叶うオンラインマーケット。';
 ?>

<?php include_once(__DIR__ . '/../common/util.php'); ?>
<?php include_once(__DIR__ . '/../common/login_check.php'); ?>
<?php
$user = new User();
$user->select(getUserId());

$title_page = 'マイページ';
?>
<?php include(__DIR__ . '/../user_header.php'); ?>
<!--     <div class="com-header-top">
        <div class="com-header-top__img">
            <img src="<?php echo HOME_URL; ?>/common/assets/img/mypage/top-bg.png" alt="">
        </div>
        <div class="com-header-top__path">
            <p><span><a href="<?php echo HOME_URL; ?>/" class="clr-yel">トップページ</a></span><span> > </span><span>マイページ</span></p>
        </div>
        <div class="com-header-top__txt">
            <p class="clr-white">マイページ</p>
        </div>
    </div> -->
    <div class="com-container bg-yellow">
        <?php include('usersidebar.php'); ?>
        <div class="com-content">
            <div class="content-title">
                <h3><span>メニューからコンテンツをお選びくださいませ。</span></h3>
                <!-- <h3><span>お知らせ</span></h3> -->
            </div>
            <div class="mypage-selection-sp">
            <?php include_once(__DIR__ . '/../common/util.php'); ?>
<?php
    $entity = new User();
    $sns = new Sns();
    if (isLogin()) {
        $entity->select(getUserId());
        $sns->select(getUserId());
    }

    $icon_url = $entity->get_icon_url();
    $header_image_url = $entity->get_header_image_url();
?>

<div class="com-sidebar mypage-selection-sp sp-side-add">

    <?php if (isLogin()): ?>
        <?php if (getSettlePlan() > 0): ?>

      <div class="com-sidebar-profile">
   <!--     <?php if (!empty($header_image_url)): ?>
                <div class="prof" style="background: url('<?= $header_image_url ?>');" alt=""></div>
            <?php else:?>
                <img src="<?php echo HOME_URL; ?>/common/assets/img/mypage/profile-bg.png" alt="">
            <?php endif;?>

            <div class="com-sidebar-profile-content__img mypage-selection-sp">

                <?php if (!empty($icon_url)): ?>
                    <img src="<?= $icon_url ?>">
                <?php else:?>
                    <img src="<?php echo HOME_URL; ?>/common/assets/img/mypage/profile.png" alt="">
                <?php endif;?>
            </div>-->
            <div class="com-sidebar-profile-content__name">
                <p><?= getUserName(); ?>さんの<br>
                    マイページ</p>
            </div>
        </div>
        <div class="com-sidebar__item com-sidebar__item-sp-add">
            <h4>出品する</h4>
        </div>
        <div class="com-sidebar-item__link">
            <div class="add-mapage-link">
                <a href="<?php echo HOME_URL; ?>/user/sell.php">商品登録</a>
            </div>
            <div class="add-mapage-link">
                <a href="<?php echo HOME_URL; ?>/user/sellinglist.php">出品アイテム</a>
            </div>
            <div class="add-mapage-link">
                <a href="<?php echo HOME_URL; ?>/user/negotiation_list_sell.php">質問・交渉中</a>
            </div>
            <div class="add-mapage-link">
                <a href="<?php echo HOME_URL; ?>/user/soldout_transaction.php">販売済・取引中</a>
            </div>
        </div>

        <?php endif; ?>

    <div class="com-sidebar__item com-sidebar__item-sp-add">
        <h4>購入する</h4>
    </div>
    <div class="com-sidebar-item__link">
        <div class="add-mapage-link">
            <a href="<?php echo HOME_URL; ?>/user/question_negoty.php">購入・交渉中</a>
        </div>
        <div class="add-mapage-link">
            <a href="<?php echo HOME_URL; ?>/user/exhibited_list.php">購入済み</a>
        </div>
<!--         <div class="com-sidebar-item__link-txt">
            <a href="<?php echo HOME_URL; ?>/user/favorites_list.php">お気に入り一覧</a>
        </div> -->
        <div class="add-mapage-link">
            <a href="<?php echo HOME_URL; ?>/user/request.php">購入希望募集投稿</a>
        </div>
        <div class="add-mapage-link">
            <a href="<?php echo HOME_URL; ?>/user/request_list.php">購入希望募集履歴</a>
        </div>
    </div>
    <div class="com-sidebar__item com-sidebar__item-sp-add">
        <h4>アカウント</h4>
    </div>
    <div class="com-sidebar-item__link">
        <div class="add-mapage-link">
            <a href="<?php echo HOME_URL; ?>/user/userinfo.php">ユーザー情報変更</a>
        </div>
        <div class="add-mapage-link">
            <a href="<?php echo HOME_URL; ?>/user/account_number_info.php">口座情報変更</a>
        </div>
        <div class="add-mapage-link">
            <a href="<?php echo HOME_URL; ?>/user/cash_flow_history.php">出金申請/履歴</a>
        </div>
<!--         <div class="com-sidebar-item__link-txt">
            <a href="<?php echo HOME_URL; ?>/user/evaluate.php">評価一覧</a>
        </div>
        <div class="com-sidebar-item__link-txt">
            <a href="<?php echo HOME_URL; ?>/user/block_user_list.php">ブラックリスト登録</a>
        </div>
 -->        <div class="add-mapage-link">

            <?php if (isIdentification()): ?>
                <a class=""
                   href="<?php echo HOME_URL; ?>/user/upload_identification.php">本人確認済<i class="far fa-check-circle text-success pl-1"></i></a>
            <?php else: ?>
                <a href="<?php echo HOME_URL; ?>/user/upload_identification.php">本人確認書類の提出</a>
            <?php endif; ?>

        </div>
        <!-- <div class="com-sidebar-item__link-txt">
            <a href="#">退会について</a>
        </div> -->
    </div>

    <?php endif; ?>
</div>
            </div>
            <!-- 
            <div class="content-group">
                <a href="#" class="content-group-item">
                    <div class="content-group-item__date"><i class="bi bi-chevron-right"></i>2021/03/06</div>
                    <div class="content-group-item__txt">お知らせあいうえおお知らせあいうえおお知らせあいうえおお知らせ...</div>
                    <div class="content-group-item__new"><span>NEW</span></div>
                </a>
            </div>
            <div class="content-group">
                <a href="#" class="content-group-item">
                    <div class="content-group-item__date"><i class="bi bi-chevron-right"></i>2021/03/06</div>
                    <div class="content-group-item__txt">お知らせあいうえおお知らせあいうえおお知らせあいうえおお知らせ...</div>
                    <div class="content-group-item__new"><span>NEW</span></div>
                </a>
            </div>
            <div class="content-group">
                <a href="#" class="content-group-item">
                    <div class="content-group-item__date"><i class="bi bi-chevron-right"></i>2021/03/06</div>
                    <div class="content-group-item__txt">お知らせあいうえおお知らせあいうえおお知らせあいうえおお知らせ...</div>
                    <div class="content-group-item__new"><span>NEW</span></div>
                </a>
            </div>
            <div class="content-group">
                <a href="#" class="content-group-item">
                    <div class="content-group-item__date"><i class="bi bi-chevron-right"></i>2021/03/06</div>
                    <div class="content-group-item__txt">お知らせあいうえおお知らせあいうえおお知らせあいうえおお知らせ...</div>
                </a>
            </div>
            <div class="content-group">
                <a href="#" class="content-group-item">
                    <div class="content-group-item__date"><i class="bi bi-chevron-right"></i>2021/03/06</div>
                    <div class="content-group-item__txt">お知らせあいうえおお知らせあいうえおお知らせあいうえおお知らせ...</div>
                </a>
            </div>
            <div class="content-group">
                <a href="#" class="content-group-item">
                    <div class="content-group-item__date"><i class="bi bi-chevron-right"></i>2021/03/06</div>
                    <div class="content-group-item__txt">お知らせあいうえおお知らせあいうえおお知らせあいうえおお知らせ...</div>
                </a>
            </div>
            <div class="content-group">
                <a href="#" class="content-group-item">
                    <div class="content-group-item__date"><i class="bi bi-chevron-right"></i>2021/03/06</div>
                    <div class="content-group-item__txt">お知らせあいうえおお知らせあいうえおお知らせあいうえおお知らせ...</div>
                </a>
            </div>
            <div class="content-group">
                <a href="#" class="content-group-item">
                    <div class="content-group-item__date"><i class="bi bi-chevron-right"></i>2021/03/06</div>
                    <div class="content-group-item__txt">お知らせあいうえおお知らせあいうえおお知らせあいうえおお知らせ...</div>
                </a>
            </div>
            <div class="content-group">
                <a href="#" class="content-group-item">
                    <div class="content-group-item__date"><i class="bi bi-chevron-right"></i>2021/03/06</div>
                    <div class="content-group-item__txt">お知らせあいうえおお知らせあいうえおお知らせあいうえおお知らせ...</div>
                </a>
            </div>
            <div class="content-group">
                <a href="#" class="content-group-item">
                    <div class="content-group-item__date"><i class="bi bi-chevron-right"></i>2021/03/06</div>
                    <div class="content-group-item__txt">お知らせあいうえおお知らせあいうえおお知らせあいうえおお知らせ...</div>
                </a>
            </div>
            <div class="content-group">
                <a href="#" class="content-group-item">
                    <div class="content-group-item__date"><i class="bi bi-chevron-right"></i>2021/03/06</div>
                    <div class="content-group-item__txt">お知らせあいうえおお知らせあいうえおお知らせあいうえおお知らせ...</div>
                </a>
            </div>
            <div class="content-group">
                <a href="#" class="content-group-item">
                    <div class="content-group-item__date"><i class="bi bi-chevron-right"></i>2021/03/06</div>
                    <div class="content-group-item__txt">お知らせあいうえおお知らせあいうえおお知らせあいうえおお知らせ...</div>
                </a>
            </div>
            <div class="content-group">
                <a href="#" class="content-group-item">
                    <div class="content-group-item__date"><i class="bi bi-chevron-right"></i>2021/03/06</div>
                    <div class="content-group-item__txt">お知らせあいうえおお知らせあいうえおお知らせあいうえおお知らせ...</div>
                </a>
            </div>
            <div class="content-group">
                <a href="#" class="content-group-item">
                    <div class="content-group-item__date"><i class="bi bi-chevron-right"></i>2021/03/06</div>
                    <div class="content-group-item__txt">お知らせあいうえおお知らせあいうえおお知らせあいうえおお知らせ...</div>
                </a>
            </div>
            <div class="content-group">
                <a href="#" class="content-group-item">
                    <div class="content-group-item__date"><i class="bi bi-chevron-right"></i>2021/03/06</div>
                    <div class="content-group-item__txt">お知らせあいうえおお知らせあいうえおお知らせあいうえおお知らせ...</div>
                </a>
            </div>
            <div class="content-group">
                <a href="#" class="content-group-item">
                    <div class="content-group-item__date"><i class="bi bi-chevron-right"></i>2021/03/06</div>
                    <div class="content-group-item__txt">お知らせあいうえおお知らせあいうえおお知らせあいうえおお知らせ...</div>
                </a>
            </div>
            <div class="content-group">
                <a href="#" class="content-group-item">
                    <div class="content-group-item__date"><i class="bi bi-chevron-right"></i>2021/03/06</div>
                    <div class="content-group-item__txt">お知らせあいうえおお知らせあいうえおお知らせあいうえおお知らせ...</div>
                </a>
            </div>
            <div class="content-group">
                <a href="#" class="content-group-item">
                    <div class="content-group-item__date"><i class="bi bi-chevron-right"></i>2021/03/06</div>
                    <div class="content-group-item__txt">お知らせあいうえおお知らせあいうえおお知らせあいうえおお知らせ...</div>
                </a>
            </div>
            <div class="content-group">
                <a href="#" class="content-group-item">
                    <div class="content-group-item__date"><i class="bi bi-chevron-right"></i>2021/03/06</div>
                    <div class="content-group-item__txt">お知らせあいうえおお知らせあいうえおお知らせあいうえおお知らせ...</div>
                </a>
            </div>
            <div class="content-group">
                <a href="#" class="content-group-item">
                    <div class="content-group-item__date"><i class="bi bi-chevron-right"></i>2021/03/06</div>
                    <div class="content-group-item__txt">お知らせあいうえおお知らせあいうえおお知らせあいうえおお知らせ...</div>
                </a>
            </div>
            <div class="content-group-pagination">
                <a href="#" id="pagination__preve" class="pagination__preve"><i class="bi bi-chevron-left"></i></a>
                <a href="#" class="pagination__num"><span>1</span></a>
                <a href="#" class="pagination__num"><span>2</span></a>
                <a href="#" class="pagination__num"><span>3</span></a>
                <a href="#" class="pagination__num"><span>4</span></a>
                <a href="#" class="pagination__num"><span>5</span></a>
                <a href="#" class="pagination__num"><span>6</span></a>
                <a href="#" class="pagination__num"><span>7</span></a>
                <a href="#" class="pagination__num"><span>8</span></a>
                <a href="#" class="pagination__num"><span>9</span></a>
                <a href="#" class="pagination__num"><span>10</span></a>
                <a href="#" class="pagination__num"><span>11</span></a>
                <a href="#" class="pagination__num"><span>12</span></a>
                <a href="#" class="pagination__num"><span>...</span></a>
                <a href="#" id="pagination__next" class="pagination__preve"><i class="bi bi-chevron-right"></i></a>
            </div> -->
        </div>
    </div>

<?php include('../user_footer.php'); ?>




