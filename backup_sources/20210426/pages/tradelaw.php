<?php 
$title_page = '特定商取引法に基づく表示';
$description_page = '欲しい商品を画像で確認・届いた商品を画像で照合！気持ち良い取引が叶うオンラインマーケット。';
 ?>
<?php include('../user_header.php'); ?>
    <div class="com-header-top">
        <div class="com-header-top__img wow animate__animated animate__fadeInUp">
            <img src="<?php echo HOME_URL; ?>/common/assets/img/common/other-com-header.png" alt="">
        </div>
        <div class="com-header-top__path bg-other-01 wow animate__animated animate__fadeInUp">
            <p><span><a href="<?php echo HOME_URL; ?>/" class="clr-yel">トップページ</a></span><span> > </span><span>特定商取引法に基づく表記</span></p>
        </div>
 <!--       <div class="com-header-top__txt">
            <p>特定商取引法に基づく表記</p>
        </div>-->
    </div>
    <div class="customer-container">
    <div class="category-title wow animate__animated animate__fadeInUp">
                <h3 ><span>特定商取引法に基づく表記</span></h3>
            </div>
        <div class="customer-contact-form ">
                <div class="priceinfo-inner">
                    <div class="contents">
                        <table class="priceinfo-table wow animate__animated animate__fadeInUp">
                            <tr>
                                <th>事業者</th>
                                <td>株式会社てっぱん</td>
                            </tr>
                            <tr>
                                <th>代表者</th>
                                <td>楠原 英明</td>
                            </tr>
                            <tr>
                                <th>所在地</th>
                                <td>東京都中央区日本橋2－1－3アーバンネット日本橋10Ｆ</td>
                            </tr>
                            <tr>
                                <th>メールアドレス</th>
                                <td>info@teppan.store</td>
                            </tr>
                            <tr>
                                <th>電話番号</th>
                                <td>050-5434-8102<br>
                                ※お取引やサービスについてのお問い合わせは電話では受け付けておりません<br>
                                お問い合わせについては個人情報保護などのため、アプリまたはWebにログイン後「マイページ＞お問い合わせ」からご連絡ください。<br>
                                ご不便をおかけいたしますが、ご理解ご協力をよろしくお願いいたします。</td>
                            </tr>
                            <tr>
                                <th>販売価格帯</th>
                                <td>各商品に表記された価格に準じます。</td>
                            </tr>
                            <tr>
                                <th>商品等の引き渡し時期</th>
                                <td>入金確認後、1～7日程度で発送</td>
                            </tr>
                            <tr>
                                <th>代金の支払方法</th>
                                <td>-</td>
                            </tr>
                            <tr>
                                <th>代金の支払時期</th>
                                <td>-</td>
                            </tr>
                            <tr>
                                <th>商品代金以外に必要な費用</th>
                                <td>商品により配送費用<br>
                                    支払い方法により所定の手数料</td>
                            </tr>
                            <tr>
                                <th>返品・交換について</th>
                                <td>お客様都合による返品・交換は受け付けておりません<br>
                                不良品については販売者への連絡対応をおこなってください</td>
                            </tr>
                        </table>
                    </div>
                </div>
        </div>
    </div>
<?php include('../footer.php'); ?>