-- phpMyAdmin SQL Dump
-- version 4.4.15.10
-- https://www.phpmyadmin.net
--
-- Host: mysql12027.xserver.jp
-- Generation Time: Apr 26, 2021 at 05:58 PM
-- Server version: 5.7.31
-- PHP Version: 5.4.16

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `xs480077_fleamarket`
--

-- --------------------------------------------------------

--
-- Table structure for table `frema_block_access`
--

CREATE TABLE IF NOT EXISTS `frema_block_access` (
  `id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `block_user_id` int(11) NOT NULL,
  `regist_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `update_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Table structure for table `frema_card_info`
--

CREATE TABLE IF NOT EXISTS `frema_card_info` (
  `user_id` varchar(250) CHARACTER SET utf8 NOT NULL,
  `token` varchar(250) CHARACTER SET utf8 DEFAULT NULL,
  `customer_id` varchar(250) CHARACTER SET utf8 DEFAULT NULL,
  `last_receipt_url` varchar(250) CHARACTER SET utf8 DEFAULT NULL,
  `email` varchar(250) CHARACTER SET utf8 NOT NULL DEFAULT '',
  `enabled_customer` tinyint(1) NOT NULL DEFAULT '0',
  `regist_key` varchar(250) CHARACTER SET utf8 NOT NULL DEFAULT '',
  `subscription_id` varchar(250) CHARACTER SET utf8 NOT NULL DEFAULT '',
  `canceled_subscription` tinyint(1) NOT NULL DEFAULT '0',
  `regist_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `update_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Table structure for table `frema_carriage_plan`
--

CREATE TABLE IF NOT EXISTS `frema_carriage_plan` (
  `id` int(11) NOT NULL,
  `name` varchar(100) NOT NULL,
  `enabled` tinyint(1) NOT NULL DEFAULT '0',
  `regist_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `update_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `frema_carriage_plan`
--

INSERT INTO `frema_carriage_plan` (`id`, `name`, `enabled`, `regist_date`, `update_date`) VALUES
(1, '送料込', 1, '2017-05-22 00:57:43', '2017-05-22 00:57:43'),
(2, '送料別', 1, '2017-05-22 00:57:43', '2017-05-22 00:57:43');

-- --------------------------------------------------------

--
-- Table structure for table `frema_carriage_type`
--

CREATE TABLE IF NOT EXISTS `frema_carriage_type` (
  `id` int(11) NOT NULL,
  `name` varchar(100) NOT NULL,
  `enabled` tinyint(1) NOT NULL DEFAULT '0',
  `regist_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `update_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `frema_carriage_type`
--

INSERT INTO `frema_carriage_type` (`id`, `name`, `enabled`, `regist_date`, `update_date`) VALUES
(1, '1～2日で発送', 1, '2017-05-22 01:06:38', '2017-05-22 01:06:38'),
(2, '3～5日で発送', 1, '2017-09-15 00:35:28', '2017-09-15 00:35:28'),
(3, '7日以内に発送', 1, '2017-09-15 00:35:42', '2017-09-15 00:35:42');

-- --------------------------------------------------------

--
-- Table structure for table `frema_cash_flow`
--

CREATE TABLE IF NOT EXISTS `frema_cash_flow` (
  `id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `order_id` int(11) NOT NULL,
  `in_out` tinyint(4) NOT NULL COMMENT '1:????,2:?o??',
  `amount` int(11) NOT NULL,
  `commission` int(11) NOT NULL,
  `update_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `regist_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `frema_cash_flow`
--

INSERT INTO `frema_cash_flow` (`id`, `user_id`, `order_id`, `in_out`, `amount`, `commission`, `update_date`, `regist_date`) VALUES
(1, 15, 9, 1, 2000, 200, '2021-04-23 04:17:38', '2021-04-23 04:17:38');

-- --------------------------------------------------------

--
-- Table structure for table `frema_category`
--

CREATE TABLE IF NOT EXISTS `frema_category` (
  `id` int(11) NOT NULL,
  `name` varchar(100) NOT NULL,
  `enabled` tinyint(1) NOT NULL DEFAULT '0',
  `regist_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `update_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `frema_category`
--

INSERT INTO `frema_category` (`id`, `name`, `enabled`, `regist_date`, `update_date`) VALUES
(1, 'レディース', 1, '2021-03-31 08:14:18', '2021-03-31 08:14:18'),
(2, 'メンズ', 1, '2021-04-02 16:59:32', '2021-04-02 16:59:32'),
(3, 'キッズ・ベビー・マタニティ', 1, '2021-04-02 16:59:45', '2021-04-02 16:59:45'),
(4, 'コスメ・美容', 1, '2021-04-02 16:59:57', '2021-04-02 16:59:57'),
(5, 'エンタメ・ホビー', 1, '2021-04-02 17:00:09', '2021-04-02 17:00:09'),
(6, 'インテリア・雑貨', 1, '2021-04-02 17:00:26', '2021-04-02 17:00:26'),
(7, '家電', 1, '2021-04-02 17:00:41', '2021-04-02 17:00:41'),
(8, 'スポーツ・アウトドア', 1, '2021-04-02 17:00:55', '2021-04-02 17:00:55'),
(9, '自転車・バイク', 1, '2021-04-02 17:01:07', '2021-04-02 17:01:07'),
(10, 'ハンドメイド', 1, '2021-04-02 17:01:19', '2021-04-02 17:01:19');

-- --------------------------------------------------------

--
-- Table structure for table `frema_delivery_type`
--

CREATE TABLE IF NOT EXISTS `frema_delivery_type` (
  `id` int(11) NOT NULL,
  `name` varchar(100) NOT NULL,
  `enabled` tinyint(1) NOT NULL DEFAULT '0',
  `regist_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `update_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB AUTO_INCREMENT=12 DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `frema_delivery_type`
--

INSERT INTO `frema_delivery_type` (`id`, `name`, `enabled`, `regist_date`, `update_date`) VALUES
(1, '普通郵便（定形・定形外）', 1, '2017-05-22 01:05:52', '2017-05-22 01:05:52'),
(2, 'ゆうメール', 1, '2017-06-16 00:28:48', '2017-06-16 00:28:48'),
(3, 'スマートレター', 1, '2017-09-13 00:40:21', '2017-09-13 00:40:21'),
(4, 'レターパック（ライト・プラス）', 1, '2017-09-15 01:54:32', '2017-09-15 01:54:32'),
(5, 'ゆうパック', 1, '2018-07-23 13:07:52', '2018-07-23 13:07:52'),
(6, 'ヤマト宅急便', 1, '2018-07-23 13:13:49', '2018-07-23 13:13:49'),
(7, '宅急便コンパクト', 1, '2018-07-23 13:14:13', '2018-07-23 13:14:13'),
(8, '佐川急便', 1, '2018-07-23 13:14:24', '2018-07-23 13:14:24'),
(9, '代引き（手数料は出品者負担）', 1, '2018-10-15 11:05:47', '2018-10-15 11:05:47'),
(10, '代引き（手数料は購入者負担）', 1, '2018-10-17 12:50:47', '2018-10-17 12:50:47'),
(11, 'その他', 1, '2018-10-17 12:50:58', '2018-10-17 12:50:58');

-- --------------------------------------------------------

--
-- Table structure for table `frema_evaluate`
--

CREATE TABLE IF NOT EXISTS `frema_evaluate` (
  `id` int(11) NOT NULL,
  `name` varchar(50) NOT NULL,
  `point` int(11) NOT NULL,
  `regist_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `update_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `frema_evaluate`
--

INSERT INTO `frema_evaluate` (`id`, `name`, `point`, `regist_date`, `update_date`) VALUES
(1, '良い', 1, '2017-06-26 21:53:36', '2017-06-26 21:53:36'),
(2, '普通', 0, '2017-06-26 21:53:36', '2017-06-26 21:53:36'),
(3, '悪い', -1, '2017-06-26 21:53:55', '2017-06-26 21:53:55');

-- --------------------------------------------------------

--
-- Table structure for table `frema_favorites`
--

CREATE TABLE IF NOT EXISTS `frema_favorites` (
  `user_id` int(11) NOT NULL,
  `item_id` int(11) NOT NULL,
  `regist_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `update_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Table structure for table `frema_free_input_item`
--

CREATE TABLE IF NOT EXISTS `frema_free_input_item` (
  `id` int(11) NOT NULL,
  `name` varchar(100) NOT NULL,
  `field_type` int(11) NOT NULL COMMENT 'フィールドのタイプ（1:プルダウン、2:チェックボックス、3:ラジオボタン）',
  `enabled` tinyint(1) NOT NULL DEFAULT '0',
  `updata_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `regist_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `frema_free_input_item`
--

INSERT INTO `frema_free_input_item` (`id`, `name`, `field_type`, `enabled`, `updata_date`, `regist_date`) VALUES
(1, '自由項目1', 1, 0, '2021-03-31 08:14:18', '2021-03-31 08:14:18'),
(2, '自由項目2', 1, 0, '2021-03-31 08:14:18', '2021-03-31 08:14:18'),
(3, '自由項目3', 1, 0, '2021-03-31 08:14:18', '2021-03-31 08:14:18');

-- --------------------------------------------------------

--
-- Table structure for table `frema_free_input_item_value`
--

CREATE TABLE IF NOT EXISTS `frema_free_input_item_value` (
  `id` int(11) NOT NULL,
  `free_input_item_id` int(11) NOT NULL,
  `name` varchar(100) NOT NULL,
  `order_number` int(11) NOT NULL DEFAULT '0',
  `enabled` tinyint(1) NOT NULL DEFAULT '0',
  `update_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `regist_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Table structure for table `frema_good`
--

CREATE TABLE IF NOT EXISTS `frema_good` (
  `item_id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `good_count` int(11) NOT NULL DEFAULT '1',
  `regist_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `update_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Table structure for table `frema_identification`
--

CREATE TABLE IF NOT EXISTS `frema_identification` (
  `user_id` int(11) NOT NULL,
  `photo` mediumblob,
  `photo2` mediumblob,
  `regist_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `update_date` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `frema_identification`
--

INSERT INTO `frema_identification` (`user_id`, `photo`, `photo2`, `regist_date`, `update_date`) VALUES
(4, NULL, NULL, '2021-03-31 11:26:50', '0000-00-00 00:00:00'),
(15, NULL, NULL, '2021-04-23 01:15:52', '0000-00-00 00:00:00'),
(21, NULL, NULL, '2021-04-23 04:07:25', '0000-00-00 00:00:00');

-- --------------------------------------------------------

--
-- Table structure for table `frema_information`
--

CREATE TABLE IF NOT EXISTS `frema_information` (
  `id` int(11) NOT NULL,
  `title` varchar(100) NOT NULL,
  `text` varchar(1000) NOT NULL,
  `enabled` tinyint(1) NOT NULL DEFAULT '1',
  `regist_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `update_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Table structure for table `frema_invoice`
--

CREATE TABLE IF NOT EXISTS `frema_invoice` (
  `id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `month` varchar(8) NOT NULL,
  `status` tinyint(1) NOT NULL DEFAULT '0',
  `price` int(11) NOT NULL DEFAULT '0',
  `transaction_count` int(11) NOT NULL DEFAULT '0',
  `transaction_amount` int(11) unsigned NOT NULL DEFAULT '0',
  `commission_rate` int(11) NOT NULL DEFAULT '0',
  `commission` int(11) NOT NULL DEFAULT '0',
  `amount` int(11) NOT NULL DEFAULT '0',
  `regist_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `update_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Table structure for table `frema_invoice_details`
--

CREATE TABLE IF NOT EXISTS `frema_invoice_details` (
  `invoice_id` int(11) NOT NULL,
  `order_id` int(11) NOT NULL,
  `regist_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `update_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Table structure for table `frema_item`
--

CREATE TABLE IF NOT EXISTS `frema_item` (
  `id` int(11) NOT NULL,
  `access_count` int(11) DEFAULT '0',
  `owner_id` int(11) NOT NULL,
  `title` varchar(250) NOT NULL,
  `category` int(11) DEFAULT NULL,
  `item_state` int(11) DEFAULT NULL,
  `remarks` varchar(16000) NOT NULL,
  `pref` int(11) DEFAULT '0',
  `carriage_plan` int(11) DEFAULT NULL,
  `carriage_type` int(11) DEFAULT NULL,
  `delivery_type` int(11) DEFAULT NULL,
  `price` int(11) NOT NULL DEFAULT '0',
  `delivery_price` int(11) NOT NULL DEFAULT '0',
  `stock` int(11) DEFAULT NULL,
  `photo` mediumblob,
  `target_user_id` int(11) DEFAULT NULL,
  `show_evaluate` tinyint(1) DEFAULT '0',
  `with_delivery` tinyint(1) NOT NULL DEFAULT '1',
  `enabled` tinyint(1) NOT NULL DEFAULT '0',
  `regist_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `update_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `frema_item`
--

INSERT INTO `frema_item` (`id`, `access_count`, `owner_id`, `title`, `category`, `item_state`, `remarks`, `pref`, `carriage_plan`, `carriage_type`, `delivery_type`, `price`, `delivery_price`, `stock`, `photo`, `target_user_id`, `show_evaluate`, `with_delivery`, `enabled`, `regist_date`, `update_date`) VALUES
(1, 0, 1, 'test', 1, 2, 'テスト', 2, NULL, NULL, NULL, 9999, 0, 0, NULL, NULL, 0, 0, 0, '2021-04-01 04:06:20', '2021-04-01 04:06:20'),
(2, 0, 5, '箱', 10, 7, '箱', 13, 1, 3, 2, 1000, 0, 0, NULL, NULL, 0, 1, 0, '2021-04-05 05:06:03', '2021-04-05 05:06:03'),
(3, 1, 15, 'テスト', 2, 5, 'テスト', 13, 1, 3, 11, 5000, 0, 0, NULL, NULL, 0, 1, 1, '2021-04-23 04:10:50', '2021-04-05 05:08:06'),
(4, 0, 20, 'お手製キルト', 10, 7, 'この文章はダミーです。文字の大きさ、量、字間、行間等を確認するために入れています。この文章はダミーです。文字の大きさ、量、字間、行間等を確認するために入れています。この文章はダミーです。文字の大きさ、量、字間、行間等を確認するために入れています。この文章はダミーです。文字の大きさ、量、字間、行間等を確認するために入れています。この文章はダミーです。文字の大きさ、量、字間、行間等を確認するために入れています。この文章はダミーです。文字の大きさ、量、字間、行間等を確認するために入れています。この文章はダミーです。文字の大きさ、量、字間、行間等を確認するために入れています。この文章はダミーです。文字の大きさ、量、字間、行間等を確認するために入れています。この文章はダミーです。文字の大きさ、量、字間、行間等を確認するために入れています。この文章はダミーです。文字の大きさ、量、字間、行間等を確認するために入れています。この文章はダミーです。文字の大きさ、量、字間、行間等を確認するために入れています。この文章はダミーです。文字の大きさ、量、字間、行間等を確認するために入れています。この文章はダミーです。文字の大きさ、量、字間、行間等を確認するために入れています。この文章はダミーです。文字の大きさ、量、字間、行間等を確認するために入れています。この文章はダミーです。文字の大きさ、量、字間、行間等を確認するために入れています。この文章はダミーです。文字の大きさ、量、字間、行間等を確認するために入れています。この文章はダミーです。文字の大きさ、量、字間、行間等を確認するために入れています。この文章はダミーです。文字の大きさ、量、字間、行間等を確認するために入れています。この文章はダミーです。文字の大きさ、量、字間、行間等を確認するために入れています。この文章はダミーです。文字の大きさ、量、字間、行間等を確認するために入れています。この文章はダミーです。文字の大きさ、量、字間、行間等を確認するために入れています。この文章はダミーです。文字の大きさ、量、字間、行間等を確認するために入れています。この文章はダミーです。文字の大きさ、量、字間、行間等を確認するために入れています。この文章はダミーです。文字の大きさ、量、字間、行間等を確認するために入れています。この文章はダミーです。文字の大きさ、量、字間、行間等を確認するために入れています。この文章はダミーです。文字の大きさ、量、字間、行間等を確認するために入れています。この文章はダミーです。文字の大きさ、量、字間、行間等を確認するために入れています。この文章はダミーです。文字の大きさ、量、字間、行間等を確認するために入れています。この文章はダミーです。文字の大きさ、量、字間、行間等を確認するために入れています。この文章はダミーです。文字の大きさ、量、字間、行間等を確認するために入れています。この文章はダミーです。文字の大きさ、量、字間、行間等を確認するために入れています。この文章はダミーです。文字の大きさ、量、字間、行間等を確認するために入れています。この文章はダミーです。文字の大きさ、量、字間、行間等を確認するために入れています。この文章はダミーです。文字の大きさ、量、字間、行間等を確認するために入れています。この文章はダミーです。文字の大きさ、量、字間、行間等を確認するために入れています。この文章はダミーです。文字の大きさ、量、字間、行間等を確認するために入れています。この文章はダミーです。文字の大きさ、量、字間、行間等を確認するために入れています。この文章はダミーです。文字の大きさ、量、字間、行間等を確認するために入れています。この文章はダミーです。文字の大きさ、量、字間、行間等を確認するために入れています。この文章はダミーです。文字の大きさ、量、字間、行間等を確認するために入れています。この文章はダミーです。文字の大きさ、量、字間、行間等を確認するために入れています。この文章はダミーです。文字の大きさ、量、字間、行間等を確認するために入れています。この文章はダミーです。文字の大きさ、量、字間、行間等を確認するために入れています。この文章はダミーです。文字の大きさ、量、字間、行間等を確認するために入れています。この文章はダミーです。文字の大きさ、量、字間、行間等を確認するために入れています。この文章はダミーです。文字の大きさ、量、字間、行間等を確認するために入れています。この文章はダミーです。文字の大きさ、量、字間、行間等を確認するために入れています。この文章はダミーです。文字の大きさ、量、字間、行間等を確認するために入れています。この文章はダミーです。文字の大きさ、量、字間、行間等を確認するために入れています。この文章はダミーです。文字の大きさ、量、字間、行間等を確認するために入れています。この文章はダミーです。文字の大きさ、量、字間、行間等を確認するために入れています。この文章はダミーです。文字の大きさ、量、字間、行間等を確認するために入れています。この文章はダミーです。文字の大きさ、量、字間、行間等を確認するために入れています。この文章はダミーです。文字の大きさ、量、字間、行間等を確認するために入れています。この文章はダミーです。文字の大きさ、量、字間、行間等を確認するために入れています。この文章はダミーです。文字の大きさ、量、字間、行間等を確認するために入れています。この文章はダミーです。文字の大きさ、量、字間、行間等を確認するために入れています。この文章はダミーです。文字の大きさ、量、字間、行間等を確認するために入れています。この文章はダミーです。文字の大きさ、量、字間、行間等を確認するために入れています。この文章はダミーです。文字の大きさ、量、字間、行間等を確認するために入れています。この文章はダミーです。文字の大きさ、量、字間、行間等を確認するために入れています。この文章はダミーです。文字の大きさ、量、字間、行間等を確認するために入れています。この文章はダミーです。文字の大きさ、量、字間、行間等を確認するために入れています。この文章はダミーです。文字の大きさ、量、字間、行間等を確認するために入れています。この文章はダミーです。文字の大きさ、量、字間、行間等を確認するために入れています。この文章はダミーです。文字の大きさ、量、字間、行間等を確認するために入れています。この文章はダミーです。文字の大きさ、量、字間、行間等を確認するために入れています。この文章はダミーです。文字の大きさ、量、字間、行間等を確認するために入れています。この文章はダミーです。文字の大きさ、量、字間、行間等を確認するために入れています。この文章はダミーです。文字の大きさ、量、字間、行間等を確認するために入れています。この文章はダミーです。文字の大きさ、量、字間、行間等を確認するために入れています。この文章はダミーです。文字の大きさ、量、字間、行間等を確認するために入れています。この文章はダミーです。文字の大きさ、量、字間、行間等を確認するために入れています。この文章はダミーです。文字の大きさ、量、字間、行間等を確認するために入れています。この文章はダミーです。文字の大きさ、量、字間、行間等を確認するために入れています。この文章はダミーです。文字の大きさ、量、字間、行間等を確認するために入れています。この文章はダミーです。文字の大きさ、量、字間、行間等を確認するために入れています。この文章はダミーです。文字の大きさ、量、字間、行間等を確認するために入れています。この文章はダミーです。文字の大きさ、量、字間、行間等を確認するために入れています。この文章はダミーです。文字の大きさ、量、字間、行間等を確認するために入れています。この文章はダミーです。文字の大きさ、量、字間、行間等を確認するために入れています。この文章はダミーです。文字の大きさ、量、字間、行間等を確認するために入れています。この文章はダミーです。文字の大きさ、量、字間、行間等を確認するために入れています。この文章はダミーです。文字の大きさ、量、字間、行間等を確認するために入れています。この文章はダミーです。文字の大きさ、量、字間、行間等を確認するために入れています。この文章はダミーです。文字の大きさ、量、字間、行間等を確認するために入れています。この文章はダミーです。文字の大きさ、量、字間、行間等を確認するために入れています。この文章はダミーです。文字の大きさ、量、字間、行間等を確認するために入れています。この文章はダミーです。文字の大きさ、量、字間、行間等を確認するために入れています。この文章はダミーです。文字の大きさ、量、字間、行間等を確認するために入れています。この文章はダミーです。文字の大きさ、量、字間、行間等を確認するために入れています。この文章はダミーです。文字の大きさ、量、字間、行間等を確認するために入れています。この文章はダミーです。文字の大きさ、量、字間、行間等を確認するために入れています。この文章はダミーです。文字の大きさ、量、字間、行間等を確認するために入れています。この文章はダミーです。文字の大きさ、量、字間、行間等を確認するために入れています。この文章はダミーです。文字の大きさ、量、字間、行間等を確認するために入れています。この文章はダミーです。文字の大きさ、量、字間、行間等を確認するために入れています。この文章はダミーです。文字の大きさ、量、字間、行間等を確認するために入れています。この文章はダミーです。文字の大きさ、量、字間、行間等を確認するために入れています。この文章はダミーです。文字の大きさ、量、字間、行間等を確認するために入れています。この文章はダミーです。文字の大きさ、量、字間、行間等を確認するために入れています。この文章はダミーです。文字の大きさ、量、字間、行間等を確認するために入れています。この文章はダミーです。文字の大きさ、量、字間、行間等を確認するために入れています。この文章はダミーです。文字の大きさ、量、字間、行間等を確認するために入れています。この文章はダミーです。文字の大きさ、量、字間、行間等を確認するために入れています。この文章はダミーです。文字の大きさ、量、字間、行間等を確認するために入れています。この文章はダミーです。文字の大きさ、量、字間、行間等を確認するために入れています。この文章はダミーです。文字の大きさ、量、字間、行間等を確認するために入れています。この文章はダミーです。文字の大きさ、量、字間、行間等を確認するために入れています。この文章はダミーです。文字の大きさ、量、字間、行間等を確認するために入れています。この文章はダミーです。文字の大きさ、量、字間、行間等を確認するために入れています。この文章はダミーです。文字の大きさ、量、字間、行間等を確認するために入れています。この文章はダミーです。文字の大きさ、量、字間、行間等を確認するために入れています。この文章はダミーです。文字の大きさ、量、字間、行間等を確認するために入れています。この文章はダミーです。文字の大きさ、量、字間、行間等を確認するために入れています。この文章はダミーです。文字の大きさ、量、字間、行間等を確認するために入れています。この文章はダミーです。文字の大きさ、量、字間、行間等を確認するために入れています。この文章はダミーです。文字の大きさ、量、字間、行間等を確認するために入れています。この文章はダミーです。文字の大きさ、量、字間、行間等を確認するために入れています。この文章はダミーです。文字の大きさ、量、字間、行間等を確認するために入れています。この文章はダミーです。文字の大きさ、量、字間、行間等を確認するために入れています。この文章はダミーです。文字の大きさ、量、字間、行間等を確認するために入れています。この文章はダミーです。文字の大きさ、量、字間、行間等を確認するために入れています。この文章はダミーです。文字の大きさ、量、字間、行間等を確認するために入れています。この文章はダミーです。文字の大きさ、量、字間、行間等を確認するために入れています。この文章はダミーです。文字の大きさ、量、字間、行間等を確認するために入れています。この文章はダミーです。文字の大きさ、量、字間、行間等を確認するために入れています。この文章はダミーです。文字の大きさ、量、字間、行間等を確認するために入れています。この文章はダミーです。文字の大きさ、量、字間、行間等を確認するために入れています。この文章はダミーです。文字の大きさ、量、字間、行間等を確認するために入れています。この文章はダミーです。文字の大きさ、量、字間、行間等を確認するために入れています。この文章はダミーです。文字の大きさ、量、字間、行間等を確認するために入れています。この文章はダミーです。文字の大きさ、量、字間、行間等を確認するために入れています。この文章はダミーです。文字の大きさ、量、字間、行間等を確認するために入れています。この文章はダミーです。文字の大きさ、量、字間、行間等を確認するために入れています。この文章はダミーです。文字の大きさ、量、字間、行間等を確認するために入れています。この文章はダミーです。文字の大きさ、量、字間、行間等を確認するために入れています。この文章はダミーです。文字の大きさ、量、字間、行間等を確認するために入れています。この文章はダミーです。文字の大きさ、量、字間、行間等を確認するために入れています。この文章はダミーです。文字の大きさ、量、字間、行間等を確認するために入れています。この文章はダミーです。文字の大きさ、量、字間、行間等を確認するために入れています。この文章はダミーです。文字の大きさ、量、字間、行間等を確認するために入れています。この文章はダミーです。文字の大きさ、量、字間、行間等を確認するために入れています。この文章はダミーです。文字の大きさ、量、字間、行間等を確認するために入れています。この文章はダミーです。文字の大きさ、量、字間、行間等を確認するために入れています。この文章はダミーです。文字の大きさ、量、字間、行間等を確認するために入れています。この文章はダミーです。文字の大きさ、量、字間、行間等を確認するために入れています。この文章はダミーです。文字の大きさ、量、字間、行間等を確認するために入れています。この文章はダミーです。文字の大きさ、量、字間、行間等を確認するために入れています。この文章はダミーです。文字の大きさ、量、字間、行間等を確認するために入れています。この文章はダミーです。文字の大きさ、量、字間、行間等を確認するために入れています。この文章はダミーです。文字の大きさ、量、字間、行間等を確認するために入れています。この文章はダミーです。文字の大きさ、量、字間、行間等を確認するために入れています。この文章はダミーです。文字の大きさ、量、字間、行間等を確認するために入れています。この文章はダミーです。文字の大きさ、量、字間、行間等を確認するために入れています。この文章はダミーです。文字の大きさ、量、字間、行間等を確認するために入れています。この文章はダミーです。文字の大きさ、量、字間、行間等を確認するために入れています。この文章はダミーです。文字の大きさ、量、字間、行間等を確認するために入れています。この文章はダミーです。文字の大きさ、量、字間、行間等を確認するために入れています。この文章はダミーです。文字の大きさ、量、字間、行間等を確認するために入れています。この文章はダミーです。文字の大きさ、量、字間、行間等を確認するために入れています。この文章はダミーです。文字の大きさ、量、字間、行間等を確認するために入れています。この文章はダミーです。文字の大きさ、量、字間、行間等を確認するために入れています。この文章はダミーです。文字の大きさ、量、字間、行間等を確認するために入れています。この文章はダミーです。文字の大きさ、量、字間、行間等を確認するために入れています。この文章はダミーです。文字の大きさ、量、字間、行間等を確認するために入れています。この文章はダミーです。文字の大きさ、量、字間、行間等を確認するために入れています。この文章はダミーです。文字の大きさ、量、字間、行間等を確認するために入れています。この文章はダミーです。文字の大きさ、量、字間、行間等を確認するために入れています。この文章はダミーです。文字の大きさ、量、字間、行間等を確認するために入れています。この文章はダミーです。文字の大きさ、量、字間、行間等を確認するために入れています。この文章はダミーです。文字の大きさ、量、字間、行間等を確認するために入れています。この文章はダミーです。文字の大きさ、量、字間、行間等を確認するために入れています。この文章はダミーです。文字の大きさ、量、字間、行間等を確認するために入れています。この文章はダミーです。文字の大きさ、量、字間、行間等を確認するために入れています。この文章はダミーです。文字の大きさ、量、字間、行間等を確認するために入れています。この文章はダミーです。文字の大きさ、量、字間、行間等を確認するために入れています。この文章はダミーです。文字の大きさ、量、字間、行間等を確認するために入れています。この文章はダミーです。文字の大きさ、量、字間、行間等を確認するために入れています。この文章はダミーです。文字の大きさ、量、字間、行間等を確認するために入れています。この文章はダミーです。文字の大きさ、量、字間、行間等を確認するために入れています。この文章はダミーです。文字の大きさ、量、字間、行間等を確認するために入れています。この文章はダミーです。文字の大きさ、量、字間、行間等を確認するために入れています。この文章はダミーです。文字の大きさ、量、字間、行間等を確認するために入れています。この文章はダミーです。文字の大きさ、量、字間、行間等を確認するために入れています。この文章はダミーです。文字の大きさ、量、字間、行間等を確認するために入れています。この文章はダミーです。文字の大きさ、量、字間、行間等を確認するために入れています。この文章はダミーです。文字の大きさ、量、字間、行間等を確認するために入れています。この文章はダミーです。文字の大きさ、量、字間、行間等を確認するために入れています。この文章はダミーです。文字の大きさ、量、字間、行間等を確認するために入れています。この文章はダミーです。文字の大きさ、量、字間、行間等を確認するために入れています。この文章はダミーです。文字の大きさ、量、字間、行間等を確認するために入れています。この文章はダミーです。文字の大きさ、量、字間、行間等を確認するために入れています。この文章はダミーです。文字の大きさ、量、字間、行間等を確認するために入れています。この文章はダミーです。文字の大きさ、量、字間、行間等を確認するために入れています。この文章はダミーです。文字の大きさ、量、字間、行間等を確認するために入れています。この文章はダミーです。文字の大きさ、量、字間、行間等を確認するために入れています。この文章はダミーです。文字の大きさ、量、字間、行間等を確認するために入れています。この文章はダミーです。文字の大きさ、量、字間、行間等を確認するために入れています。この文章はダミーです。文字の大きさ、量、字間、行間等を確認するために入れています。この文章はダミーです。文字の大きさ、量、字間、行間等を確認するために入れています。この文章はダミーです。文字の大きさ、量、字間、行間等を確認するために入れています。この文章はダミーです。文字の大きさ、量、字間、行間等を確認するために入れています。この文章はダミーです。文字の大きさ、量、字間、行間等を確認するために入れています。この文章はダミーです。文字の大きさ、量、字間、行間等を確認するために入れています。この文章はダミーです。文字の大きさ、量、字間、行間等を確認するために入れています。この文章はダミーです。文字の大きさ、量、字間、行間等を確認するために入れています。この文章はダミーです。文字の大きさ、量、字間、行間等を確認するために入れています。この文章はダミーです。文字の大きさ、量、字間、行間等を確認するために入れています。この文章はダミーです。文字の大きさ、量、字間、行間等を確認するために入れています。この文章はダミーです。文字の大きさ、量、字間、行間等を確認するために入れています。この文章はダミーです。文字の大きさ、量、字間、行間等を確認するために入れています。この文章はダミーです。文字の大きさ、量、字間、行間等を確認するために入れています。この文章はダミーです。文字の大きさ、量、字間、行間等を確認するために入れています。この文章はダミーです。文字の大きさ、量、字間、行間等を確認するために入れています。この文章はダミーです。文字の大きさ、量、字間、行間等を確認するために入れています。この文章はダミーです。文字の大きさ、量、字間、行間等を確認するために入れています。この文章はダミーです。文字の大きさ、量、字間、行間等を確認するために入れています。この文章はダミーです。文字の大きさ、量、字間、行間等を確認するために入れています。この文章はダミーです。文字の大きさ、量、字間、行間等を確認するために入れています。この文章はダミーです。文字の大きさ、量、字間、行間等を確認するために入れています。この文章はダミーです。文字の大きさ、量、字間、行間等を確認するために入れています。この文章はダミーです。文字の大きさ、量、字間、行間等を確認するために入れています。この文章はダミーです。文字の大きさ、量、字間、行間等を確認するために入れています。この文章はダミーです。文字の大きさ、量、字間、行間等を確認するために入れています。この文章はダミーです。文字の大きさ、量、字間、行間等を確認するために入れています。この文章はダミーです。文字の大きさ、量、字間、行間等を確認するために入れています。この文章はダミーです。文字の大きさ、量、字間、行間等を確認するために入れています。この文章はダミーです。文字の大きさ、量、字間、行間等を確認するために入れています。この文章はダミーです。文字の大きさ、量、字間、行間等を確認するために入れています。この文章はダミーです。文字の大きさ、量、字間、行間等を確認するために入れています。この文章はダミーです。文字の大きさ、量、字間、行間等を確認するために入れています。この文章はダミーです。文字の大きさ、量、字間、行間等を確認するために入れています。この文章はダミーです。文字の大きさ、量、字間、行間等を確認するために入れています。この文章はダミーです。文字の大きさ、量、字間、行間等を確認するために入れています。この文章はダミーです。文字の大きさ、量、字間、行間等を確認するために入れています。この文章はダミーです。文字の大きさ、量、字間、行間等を確認するために入れています。この文章はダミーです。文字の大きさ、量、字間、行間等を確認するために入れています。この文章はダミーです。文字の大きさ、量、字間、行間等を確認するために入れています。この文章はダミーです。文字の大きさ、量、字間、行間等を確認するために入れています。この文章はダミーです。文字の大きさ、量、字間、行間等を確認するために入れています。この文章はダミーです。文字の大きさ、量、字間、行間等を確認するために入れています。この文章はダミーです。文字の大きさ、量、字間、行間等を確認するために入れています。この文章はダミーです。文字の大きさ、量、字間、行間等を確認するために入れています。この文章はダミーです。文字の大きさ、量、字間、行間等を確認するために入れています。この文章はダミーです。文字の大きさ、量、字間、行間等を確認するために入れています。この文章はダミーです。文字の大きさ、量、字間、行間等を確認するために入れています。この文章はダミーです。文字の大きさ、量、字間、行間等を確認するために入れています。この文章はダミーです。文字の大きさ、量、字間、行間等を確認するために入れています。この文章はダミーです。文字の大きさ、量、字間、行間等を確認するために入れています。この文章はダミーです。文字の大きさ、量、字間、行間等を確認するために入れています。この文章はダミーです。文字の大きさ、量、字間、行間等を確認するために入れています。この文章はダミーです。文字の大きさ、量、字間、行間等を確認するために入れています。この文章はダミーです。文字の大きさ、量、字間、行間等を確認するために入れています。この文章はダミーです。文字の大きさ、量、字間、行間等を確認するために入れています。この文章はダミーです。文字の大きさ、量、字間、行間等を確認するために入れています。この文章はダミーです。文字の大きさ、量、字間、行間等を確認するために入れています。この文章はダミーです。文字の大きさ、量、字間、行間等を確認するために入れています。この文章はダミーです。文字の大きさ、量、字間、行間等を確認するために入れています。この文章はダミーです。文字の大きさ、量、字間、行間等を確認するために入れています。この文章はダミーです。文字の大きさ、量、字間、行間等を確認するために入れています。この文章はダミーです。文字の大きさ、量、字間、行間等を確認するために入れています。この文章はダミーです。文字の大きさ、量、字間、行間等を確認するために入れています。この文章はダミーです。文字の大きさ、量、字間、行間等を確認するために入れています。この文章はダミーです。文字の大きさ、量、字間、行間等を確認するために入れています。この文章はダミーです。文字の大きさ、量、字間、行間等を確認するために入れています。この文章はダミーです。文字の大きさ、量、字間、行間等を確認するために入れています。この文章はダミーです。文字の大きさ、量、字間、行間等を確認するために入れています。この文章はダミーです。文字の大きさ、量、字間、行間等を確認するために入れています。この文章はダミーです。文字の大きさ、量、字間、行間等を確認するために入れています。この文章はダミーです。文字の大きさ、量、字間、行間等を確認するために入れています。この文章はダミーです。文字の大きさ、量、字間、行間等を確認するために入れています。この文章はダミーです。文字の大きさ、量、字間、行間等を確認するために入れています。この文章はダミーです。文字の大きさ、量、字間、行間等を確認するために入れています。この文章はダミーです。文字の大きさ、量、字間、行間等を確認するために入れています。この文章はダミーです。文字の大きさ、量、字間、行間等を確認するために入れています。この文章はダミーです。文字の大きさ、量、字間、行間等を確認するために入れています。この文章はダミーです。文字の大きさ、量、字間、行間等を確認するために入れています。この文章はダミーです。文字の大きさ、量、字間、行間等を確認するために入れています。この文章はダミーです。文字の大きさ、量、字間、行間等を確認するために入れています。この文章はダミーです。文字の大きさ、量、字間、行間等を確認するために入れています。この文章はダミーです。文字の大きさ、量、字間、行間等を確認するために入れています。この文章はダミーです。文字の大きさ、量、字間、行間等を確認するために入れています。この文章はダミーです。文字の大きさ、量、字間、行間等を確認するために入れています。この文章はダミーです。文字の大きさ、量、字間、行間等を確認するために入れています。この文章はダミーです。文字の大きさ、量、字間、行間等を確認するために入れています。この文章はダミーです。文字の大きさ、量、字間、行間等を確認するために入れています。この文章はダミーです。文字の大きさ、量、字間、行間等を確認するために入れています。この文章はダミーです。文字の大きさ、量、字間、行間等を確認するために入れています。この文章はダミーです。文字の大きさ、量、字間、行間等を確認するために入れています。この文章はダミーです。文字の大きさ、量、字間、行間等を確認するために入れています。この文章はダミーです。文字の大きさ、量、字間、行間等を確認するために入れています。この文章はダミーです。文字の大きさ、量、字間、行間等を確認するために入れています。この文章はダミーです。文字の大きさ、量、字間、行間等を確認するために入れています。この文章はダミーです。文字の大きさ、量、字間、行間等を確認するために入れています。この文章はダミーです。文字の大きさ、量、字間、行間等を確認するために入れています。この文章はダミーです。文字の大きさ、量、字間、行間等を確認するために入れています。この文章はダミーです。文字の大きさ、量、字間、行間等を確認するために入れています。この文章はダミーです。文字の大きさ、量、字間、行間等を確認するために入れています。この文章はダミーです。文字の大きさ、量、字間、行間等を確認するために入れています。この文章はダミーです。文字の大きさ、量、字間、行間等を確認するために入れています。この文章はダミーです。文字の大きさ、量、字間、行間等を確認するために入れています。この文章はダミーです。文字の大きさ、量、字間、行間等を確認するために入れています。この文章はダミーです。文字の大きさ、量、字間、行間等を確認するために入れています。この文章はダミーです。文字の大きさ、量、字間、行間等を確認するために入れています。この文章はダミーです。文字の大きさ、量、字間、行間等を確認するために入れています。この文章はダミーです。文字の大きさ、量、字間、行間等を確認するために入れています。この文章はダミーです。文字の大きさ、量、字間、行間等を確認するために入れています。この文章はダミーです。文字の大きさ、量、字間、行間等を確認するために入れています。この文章はダミーです。文字の大きさ、量、字間、行間等を確認するために入れています。この文章はダミーです。文字の大きさ、量、字間、行間等を確認するために入れています。この文章はダミーです。文字の大きさ、量、字間、行間等を確認するために入れています。この文章はダミーです。文字の大きさ、量、字間、行間等を確認するために入れています。この文章はダミーです。文字の大きさ、量、字間、行間等を確認するために入れています。この文章はダミーです。文字の大きさ、量、字間、行間等を確認するために入れています。この文章はダミーです。文字の大きさ、量、字間、行間等を確認するために入れています。この文章はダミーです。文字の大きさ、量、字間、行間等を確認するために入れています。この文章はダミーです。文字の大きさ、量、字間、行間等を確認するために入れています。この文章はダミーです。文字の大きさ、量、字間、行間等を確認するために入れています。この文章はダミーです。文字の大きさ、量、字間、行間等を確認するために入れています。この文章はダミーです。文字の大きさ、量、字間、行間等を確認するために入れています。この文章はダミーです。文字の大きさ、量、字間、行間等を確認するために入れています。この文章はダミーです。文字の大きさ、量、字間、行間等を確認するために入れています。この文章はダミーです。文字の大きさ、量、字間、行間等を確認するために入れています。この文章はダミーです。文字の大きさ、量、字間、行間等を確認するために入れています。この文章はダミーです。文字の大きさ、量、字間、行間等を確認するために入れています。この文章はダミーです。文字の大きさ、量、字間、行間等を確認するために入れています。この文章はダミーです。文字の大きさ、量、字間、行間等を確認するために入れています。この文章はダミーです。文字の大きさ、量、字間、行間等を確認するために入れています。この文章はダミーです。文字の大きさ、量、字間、行間等を確認するために入れています。この文章はダミーです。文字の大きさ、量、字間、行間等を確認するために入れています。この文章はダミーです。文字の大きさ、量、字間、行間等を確認するために入れています。この文章はダミーです。文字の大きさ、量、字間、行間等を確認するために入れています。この文章はダミーです。文字の大きさ、量、字間、行間等を確認するために入れています。この文章はダミーです。文字の大きさ、量、字間、行間等を確認するために入れています。この文章はダミーです。文字の大きさ、量、字間、行間等を確認するために入れています。この文章はダミーです。文字の大きさ、量、字間、行間等を確認するために入れています。この文章はダミーです。文字の大きさ、量、字間、行間等を確認するために入れています。この文章はダミーです。文字の大きさ、量、字間、行間等を確認するために入れています。この文章はダミーです。文字の大きさ、量、字間、行間等を確認するために入れています。この文章はダミーです。文字の大きさ、量、字間、行間等を確認するために入れています。この文章はダミーです。文字の大きさ、量、字間、行間等を確認するために入れています。この文章はダミーです。文字の大きさ、量、字間、行間等を確認するために入れています。この文章はダミーです。文字の大きさ、量、字間、行間等を確認するために入れています。この文章はダミーです。文字の大きさ、量、字間、行間等を確認するために入れています。この文章はダミーです。文字の大きさ、量、字間、行間等を確認するために入れています。この文章はダミーです。文字の大きさ、量、字間、行間等を確認するために入れています。この文章はダミーです。文字の大きさ、量、字間、行間等を確認するために入れています。この文章はダミーです。文字の大きさ、量、字間、行間等を確認するために入れています。この文章はダミーです。文字の大きさ、量、字間、行間等を確認するために入れています。この文章はダミーです。文字の大きさ、量、字間、行間等を確認するために入れています。この文章はダミーです。文字の大きさ、量、字間、行間等を確認するために入れています。この文章はダミーです。文字の大きさ、量、字間、行間等を確認するために入れています。この文章はダミーです。文字の大きさ、量、字間、行間等を確認するために入れています。この文章はダミーです。文字の大きさ、量、字間、行間等を確認するために入れています。この文章はダミーです。文字の大きさ、量、字間、行間等を確認するために入れています。この文章はダミーです。文字の大きさ、量、字間、行間等を確認するために入れています。この文章はダミーです。文字の大きさ、量、字間、行間等を確認するために入れています。この文章はダミーです。文字の大きさ、量、字間、行間等を確認するために入れています。この文章はダミーです。文字の大きさ、量、字間、行間等を確認するために入れています。この文章はダミーです。文字の大きさ、量、字間、行間等を確認するために入れています。この文章はダミーです。文字の大きさ、量、字間、行間等を確認するために入れています。この文章はダミーです。文字の大きさ、量、字間、行間等を確認するために入れています。この文章はダミーです。文字の大きさ、量、字間、行間等を確認するために入れています。この文章はダミーです。文字の大きさ、量、字間、行間等を確認するために入れています。この文章はダミーです。文字の大きさ、量、字間、行間等を確認するために入れています。この文章はダミーです。文字の大きさ、量、字間、行間等を確認するために入れています。この文章はダミーです。文字の大きさ、量、字間、行間等を確認するために入れています。', 12, NULL, NULL, NULL, 500, 0, 0, NULL, NULL, 0, 0, 0, '2021-04-15 07:59:40', '2021-04-15 07:59:40'),
(5, 0, 20, 'イヤリング', 1, 2, '椿の形の和風イヤリングです。', 12, NULL, NULL, NULL, 1000, 0, 0, NULL, NULL, 0, 0, 0, '2021-04-15 08:10:10', '2021-04-15 08:10:10'),
(6, 0, 20, 'テスト', 10, 1, 'テストです。', 6, 2, 2, 7, 2000, 0, 0, NULL, NULL, 0, 1, 0, '2021-04-15 09:24:02', '2021-04-15 09:24:02'),
(7, 3, 15, '423test111', 2, 5, '423テスト1', 13, NULL, NULL, NULL, 2000, 0, 0, NULL, NULL, 0, 0, 1, '2021-04-23 04:10:30', '2021-04-23 01:03:21'),
(8, 0, 15, '423test222', 7, 4, '423テスト2', NULL, NULL, NULL, NULL, 1500, 0, 0, NULL, NULL, 0, 0, 1, '2021-04-23 04:10:40', '2021-04-23 01:07:51');

-- --------------------------------------------------------

--
-- Table structure for table `frema_item_free_input`
--

CREATE TABLE IF NOT EXISTS `frema_item_free_input` (
  `item_id` int(11) NOT NULL,
  `free_input_item_id` int(11) NOT NULL,
  `free_input_item_value_id` int(11) NOT NULL,
  `free_input_item_value_text` varchar(250) DEFAULT '',
  `update_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `regist_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Table structure for table `frema_item_photo`
--

CREATE TABLE IF NOT EXISTS `frema_item_photo` (
  `item_id` int(11) NOT NULL,
  `number` int(11) NOT NULL,
  `photo` mediumblob NOT NULL,
  `regist_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `update_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Table structure for table `frema_item_state`
--

CREATE TABLE IF NOT EXISTS `frema_item_state` (
  `id` int(11) NOT NULL,
  `name` varchar(100) NOT NULL,
  `enabled` tinyint(1) NOT NULL DEFAULT '0',
  `regist_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `update_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `frema_item_state`
--

INSERT INTO `frema_item_state` (`id`, `name`, `enabled`, `regist_date`, `update_date`) VALUES
(1, '新品（未使用）', 1, '2017-05-22 00:46:09', '2017-05-22 00:46:09'),
(2, '未使用に近い\r\n', 1, '2017-05-22 00:46:09', '2017-05-22 00:46:09'),
(3, '目立った傷や汚れなし ', 1, '2017-05-22 00:46:20', '2017-05-22 00:46:20'),
(4, 'やや傷や汚れあり', 1, '2018-11-28 22:37:46', '2018-11-28 22:37:46'),
(5, '傷や汚れあり', 1, '2018-11-28 22:37:46', '2018-11-28 22:37:46'),
(6, '役務（サービスの提供）', 1, '2018-11-28 22:38:16', '2018-11-28 22:38:16'),
(7, 'その他', 1, '2018-11-28 22:38:16', '2018-11-28 22:38:16');

-- --------------------------------------------------------

--
-- Table structure for table `frema_item_tmp`
--

CREATE TABLE IF NOT EXISTS `frema_item_tmp` (
  `id` int(11) NOT NULL,
  `owner_id` int(11) NOT NULL,
  `title` varchar(250) DEFAULT NULL,
  `category` int(11) DEFAULT NULL,
  `item_state` int(11) DEFAULT NULL,
  `remarks` varchar(16000) NOT NULL,
  `pref` int(11) DEFAULT '0',
  `carriage_plan` int(11) DEFAULT NULL,
  `carriage_type` int(11) DEFAULT NULL,
  `delivery_type` int(11) DEFAULT NULL,
  `price` int(11) DEFAULT '0',
  `delivery_price` int(11) DEFAULT '0',
  `stock` int(11) DEFAULT NULL,
  `photo` mediumblob,
  `target_user_id` int(11) DEFAULT NULL,
  `show_evaluate` tinyint(1) DEFAULT '0',
  `with_delivery` tinyint(1) NOT NULL DEFAULT '1',
  `enabled` tinyint(1) NOT NULL DEFAULT '0',
  `regist_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `update_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Table structure for table `frema_login_session`
--

CREATE TABLE IF NOT EXISTS `frema_login_session` (
  `hash_key` varchar(100) NOT NULL,
  `user_id` int(11) NOT NULL,
  `regist_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `update_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `frema_login_session`
--

INSERT INTO `frema_login_session` (`hash_key`, `user_id`, `regist_date`, `update_date`) VALUES
('09b9e586b929936346ca123c1bf98c5a02c2beb3', 4, '2021-04-19 15:14:55', '2021-04-19 15:14:55'),
('1f5291eb3429ac2ff4790f6fcadc3ca9812d74e8', 6, '2021-04-05 07:41:12', '2021-04-05 07:41:12'),
('2a5eee3b2c9ee370cf0e2c112db2f093b0f444ba', 6, '2021-04-06 07:04:48', '2021-04-06 07:04:48'),
('344ebb59bd500a63b5cb8cc5b8f492776c8c291c', 6, '2021-04-05 05:05:39', '2021-04-05 05:05:39'),
('3b955db61786189e7a7541fb8a7823394ad6d65c', 5, '2021-04-04 11:29:09', '2021-04-04 11:29:09'),
('4821a21d1a3aba1e5023c468feb2539997769898', 5, '2021-04-23 13:21:41', '2021-04-23 13:21:41'),
('488906548a83ebc0405a629dd4e76c38fa01d6ca', 5, '2021-04-02 22:51:18', '2021-04-02 22:51:18'),
('48e1e81c25fd62d1953444ad5915b11c30ff2f5a', 2, '2021-04-07 03:50:47', '2021-04-07 03:50:47'),
('5df477a4b87f9221c07e8e471bf82832ce433ac5', 5, '2021-04-05 22:07:03', '2021-04-05 22:07:03'),
('64cd2a348b68ef9e360f47f42f783045b0805158', 5, '2021-04-05 12:12:55', '2021-04-05 12:12:55'),
('8cca02b1e9c85eea40c452e161d030a7fcf48758', 6, '2021-04-01 01:57:42', '2021-04-01 01:57:42'),
('a3df492e4451bc5aee356ff626964e4d9e8ee572', 5, '2021-04-10 03:28:56', '2021-04-10 03:28:56'),
('c92e28aa45261f99c8f88ec3be20a74f81b2bc62', 6, '2021-04-01 23:59:11', '2021-04-01 23:59:11'),
('c9451961e1c230fa5c6c514e1f160c0d7eadefe4', 5, '2021-04-05 03:36:50', '2021-04-05 03:36:50'),
('d2042582a5d1821ee3102ff2c8d98b60896926d8', 5, '2021-04-06 00:27:13', '2021-04-06 00:27:13'),
('d7f9542fb9c3f53752128060c47e3e017b03299c', 1, '2021-04-26 07:19:50', '2021-04-26 07:19:50'),
('da4dd3c477f5d559537981fb106bd5caae5f6b41', 10, '2021-04-02 23:17:56', '2021-04-02 23:17:56'),
('e19a9d7f82c9fd82e336ec108101b6d4e4b11b5e', 5, '2021-04-05 05:01:56', '2021-04-05 05:01:56'),
('e2c046afd1f3506953c45d1a7b3c92dec235ff1f', 5, '2021-04-05 02:41:30', '2021-04-05 02:41:30'),
('e312028584f6a1c0b59e0d742ab0ea58c1fcf071', 5, '2021-04-06 04:41:19', '2021-04-06 04:41:19'),
('ec2d017888499f8bef9c9694743fcc8bcd92f757', 11, '2021-04-02 23:55:29', '2021-04-02 23:55:29'),
('f400d1a93f959e820165a6a0b29a2abd042f0c34', 5, '2021-04-07 02:31:24', '2021-04-07 02:31:24'),
('fd835f8e9945b444d4dd989c698e824a097e6c64', 5, '2021-04-05 11:52:38', '2021-04-05 11:52:38');

-- --------------------------------------------------------

--
-- Table structure for table `frema_mail_history`
--

CREATE TABLE IF NOT EXISTS `frema_mail_history` (
  `id` int(11) NOT NULL,
  `subject` varchar(200) CHARACTER SET utf8 NOT NULL,
  `body` varchar(4000) CHARACTER SET utf8 NOT NULL,
  `update_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `regist_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `frema_mail_history_target`
--

CREATE TABLE IF NOT EXISTS `frema_mail_history_target` (
  `mail_history_id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `update_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `regist_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `frema_negotiation`
--

CREATE TABLE IF NOT EXISTS `frema_negotiation` (
  `id` int(11) NOT NULL,
  `item_id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `comment` varchar(1000) DEFAULT NULL,
  `kbn` int(11) NOT NULL,
  `regist_date` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `update_date` timestamp NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `frema_negotiation`
--

INSERT INTO `frema_negotiation` (`id`, `item_id`, `user_id`, `comment`, `kbn`, `regist_date`, `update_date`) VALUES
(1, 7, 21, '', 1, '2021-04-23 04:52:13', '2021-04-23 04:52:13');

-- --------------------------------------------------------

--
-- Table structure for table `frema_order`
--

CREATE TABLE IF NOT EXISTS `frema_order` (
  `id` int(11) NOT NULL,
  `item_id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `buy_count` int(11) DEFAULT NULL,
  `price` int(11) DEFAULT NULL,
  `delivery_price` int(11) NOT NULL DEFAULT '0',
  `state` int(11) NOT NULL,
  `cash_flow` tinyint(4) NOT NULL DEFAULT '0' COMMENT '0:???????A1:???????A2:?????ς?',
  `payment` tinyint(4) NOT NULL DEFAULT '0' COMMENT '1:?U?荞??,2:?N???W?b?g',
  `seller_evaluate` int(11) NOT NULL DEFAULT '0',
  `buyer_evaluate` int(11) NOT NULL DEFAULT '0',
  `seller_comment` varchar(1000) NOT NULL DEFAULT '',
  `buyer_comment` varchar(1000) NOT NULL DEFAULT '',
  `regist_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `update_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB AUTO_INCREMENT=10 DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `frema_order`
--

INSERT INTO `frema_order` (`id`, `item_id`, `user_id`, `buy_count`, `price`, `delivery_price`, `state`, `cash_flow`, `payment`, `seller_evaluate`, `buyer_evaluate`, `seller_comment`, `buyer_comment`, `regist_date`, `update_date`) VALUES
(1, 0, 2, NULL, NULL, 0, 0, 0, 0, 0, 0, '', '', '2021-04-06 04:17:10', '2021-04-06 04:17:10'),
(9, 7, 21, NULL, NULL, 0, 3, 2, 1, 0, 0, '', '', '2021-04-23 04:13:06', '2021-04-23 04:22:55');

-- --------------------------------------------------------

--
-- Table structure for table `frema_order_number`
--

CREATE TABLE IF NOT EXISTS `frema_order_number` (
  `order_number` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `regist_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `update_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Table structure for table `frema_payout_history`
--

CREATE TABLE IF NOT EXISTS `frema_payout_history` (
  `id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `amount` int(11) NOT NULL,
  `status` tinyint(4) NOT NULL DEFAULT '0' COMMENT '0:???????A1:?U???ς?',
  `regist_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `update_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Table structure for table `frema_pr`
--

CREATE TABLE IF NOT EXISTS `frema_pr` (
  `id` int(11) NOT NULL,
  `title` varchar(100) NOT NULL DEFAULT '',
  `description` varchar(400) NOT NULL DEFAULT '',
  `icon` varchar(50) NOT NULL DEFAULT '',
  `update_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `regist_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `frema_pr`
--

INSERT INTO `frema_pr` (`id`, `title`, `description`, `icon`, `update_date`, `regist_date`) VALUES
(1, 'テスト', 'テスト', 'far fa-check-circle', '2021-03-19 08:06:15', '2021-03-19 00:36:59'),
(2, 'テストテスト', 'テストテスト', 'fas fa-yen-sign', '2021-03-19 08:06:05', '2021-03-19 00:36:59'),
(3, 'テストテストテスト', 'テストテストテスト', 'fa fa-heart', '2021-03-19 08:06:11', '2021-03-19 00:36:59');

-- --------------------------------------------------------

--
-- Table structure for table `frema_pref`
--

CREATE TABLE IF NOT EXISTS `frema_pref` (
  `id` int(11) NOT NULL,
  `name` varchar(50) NOT NULL,
  `enabled` tinyint(1) NOT NULL DEFAULT '0',
  `regist_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `update_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `frema_pref`
--

INSERT INTO `frema_pref` (`id`, `name`, `enabled`, `regist_date`, `update_date`) VALUES
(1, '北海道', 1, '2017-04-17 22:29:37', '2017-04-17 22:29:37'),
(2, '青森県', 1, '2017-04-17 22:30:32', '2017-04-17 22:30:32'),
(3, '岩手県', 1, '2017-04-17 22:30:33', '2017-04-17 22:30:33'),
(4, '宮城県', 1, '2017-04-17 22:30:34', '2017-04-17 22:30:34'),
(5, '秋田県', 1, '2017-04-17 22:30:35', '2017-04-17 22:30:35'),
(6, '山形県', 1, '2017-04-17 22:30:36', '2017-04-17 22:30:36'),
(7, '福島県', 1, '2017-04-17 22:30:37', '2017-04-17 22:30:37'),
(8, '茨城県', 1, '2017-04-17 22:30:38', '2017-04-17 22:30:38'),
(9, '栃木県', 1, '2017-04-17 22:30:39', '2017-04-17 22:30:39'),
(10, '群馬県', 1, '2017-04-17 22:30:40', '2017-04-17 22:30:40'),
(11, '埼玉県', 1, '2017-04-17 22:30:40', '2017-04-17 22:30:40'),
(12, '千葉県', 1, '2017-04-17 22:30:41', '2017-04-17 22:30:41'),
(13, '東京都', 1, '2017-04-17 22:30:42', '2017-04-17 22:30:42'),
(14, '神奈川県', 1, '2017-04-17 22:30:43', '2017-04-17 22:30:43'),
(15, '新潟県', 1, '2017-04-17 22:32:42', '2017-04-17 22:32:42'),
(16, '富山県', 1, '2017-04-17 22:32:43', '2017-04-17 22:32:43'),
(17, '石川県', 1, '2017-04-17 22:32:44', '2017-04-17 22:32:44'),
(18, '福井県', 1, '2017-04-17 22:32:45', '2017-04-17 22:32:45'),
(19, '山梨県', 1, '2017-04-17 22:32:46', '2017-04-17 22:32:46'),
(20, '長野県', 1, '2017-04-17 22:32:47', '2017-04-17 22:32:47'),
(21, '岐阜県', 1, '2017-04-17 22:32:48', '2017-04-17 22:32:48'),
(22, '静岡県', 1, '2017-04-17 22:33:08', '2017-04-17 22:33:08'),
(23, '愛知県', 1, '2017-04-17 22:33:15', '2017-04-17 22:33:15'),
(24, '三重県', 1, '2017-04-17 22:33:16', '2017-04-17 22:33:16'),
(25, '滋賀県', 1, '2017-04-17 22:33:17', '2017-04-17 22:33:17'),
(26, '京都府', 1, '2017-04-17 22:33:18', '2017-04-17 22:33:18'),
(27, '大阪府', 1, '2017-04-17 22:33:18', '2017-04-17 22:33:18'),
(28, '兵庫県', 1, '2017-04-17 22:33:19', '2017-04-17 22:33:19'),
(29, '奈良県', 1, '2017-04-17 22:33:21', '2017-04-17 22:33:21'),
(30, '和歌山県', 1, '2017-04-17 22:33:22', '2017-04-17 22:33:22'),
(31, '鳥取県', 1, '2017-04-17 22:33:24', '2017-04-17 22:33:24'),
(32, '島根県', 1, '2017-04-17 22:33:25', '2017-04-17 22:33:25'),
(33, '岡山県', 1, '2017-04-17 22:33:26', '2017-04-17 22:33:26'),
(34, '広島県', 1, '2017-04-17 22:33:27', '2017-04-17 22:33:27'),
(35, '山口県', 1, '2017-04-17 22:33:28', '2017-04-17 22:33:28'),
(36, '徳島県', 1, '2017-04-17 22:33:29', '2017-04-17 22:33:29'),
(37, '香川県', 1, '2017-04-17 22:33:30', '2017-04-17 22:33:30'),
(38, '愛媛県', 1, '2017-04-17 22:33:31', '2017-04-17 22:33:31'),
(39, '高知県', 1, '2017-04-17 22:33:32', '2017-04-17 22:33:32'),
(40, '福岡県', 1, '2017-04-17 22:33:33', '2017-04-17 22:33:33'),
(41, '佐賀県', 1, '2017-04-17 22:33:33', '2017-04-17 22:33:33'),
(42, '長崎県', 1, '2017-04-17 22:33:34', '2017-04-17 22:33:34'),
(43, '熊本県', 1, '2017-04-17 22:33:36', '2017-04-17 22:33:36'),
(44, '大分県', 1, '2017-04-17 22:33:37', '2017-04-17 22:33:37'),
(45, '宮崎県', 1, '2017-04-17 22:33:38', '2017-04-17 22:33:38'),
(46, '鹿児島県', 1, '2017-04-17 22:33:39', '2017-04-17 22:33:39'),
(47, '沖縄県', 1, '2017-04-17 22:36:12', '2017-04-17 22:36:12');

-- --------------------------------------------------------

--
-- Table structure for table `frema_request`
--

CREATE TABLE IF NOT EXISTS `frema_request` (
  `id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `title` varchar(100) NOT NULL,
  `text` varchar(1000) NOT NULL,
  `price` int(11) NOT NULL,
  `enabled` tinyint(1) NOT NULL DEFAULT '0',
  `regist_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `update_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `frema_request`
--

INSERT INTO `frema_request` (`id`, `user_id`, `title`, `text`, `price`, `enabled`, `regist_date`, `update_date`) VALUES
(1, 20, 'ワンピース', '青いワンピースが欲しいです。', 7000, 0, '2021-04-15 08:41:07', '2021-04-15 08:41:07');

-- --------------------------------------------------------

--
-- Table structure for table `frema_request_follow`
--

CREATE TABLE IF NOT EXISTS `frema_request_follow` (
  `id` int(11) NOT NULL,
  `request_id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `item_id` int(11) NOT NULL,
  `regist_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `update_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Table structure for table `frema_setting`
--

CREATE TABLE IF NOT EXISTS `frema_setting` (
  `commission_rate` int(11) NOT NULL,
  `update_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `regist_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Table structure for table `frema_settle_plan`
--

CREATE TABLE IF NOT EXISTS `frema_settle_plan` (
  `id` int(11) NOT NULL,
  `name` varchar(50) NOT NULL,
  `enabled` tinyint(1) NOT NULL DEFAULT '0',
  `number` int(11) NOT NULL,
  `regist_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `update_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `frema_settle_plan`
--

INSERT INTO `frema_settle_plan` (`id`, `name`, `enabled`, `number`, `regist_date`, `update_date`) VALUES
(1, '販売可能なプラン（仮名）', 1, 1, '2017-08-30 22:05:51', '2020-04-11 18:28:09');

-- --------------------------------------------------------

--
-- Table structure for table `frema_sns`
--

CREATE TABLE IF NOT EXISTS `frema_sns` (
  `user_id` int(11) NOT NULL,
  `twitter_url` varchar(400) NOT NULL,
  `facebook_url` varchar(400) NOT NULL,
  `youtube_url` varchar(400) NOT NULL,
  `instagram_url` varchar(400) NOT NULL,
  `other1_url` varchar(400) NOT NULL,
  `other2_url` varchar(400) NOT NULL,
  `other3_url` varchar(400) NOT NULL,
  `update_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `regist_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `frema_sns`
--

INSERT INTO `frema_sns` (`user_id`, `twitter_url`, `facebook_url`, `youtube_url`, `instagram_url`, `other1_url`, `other2_url`, `other3_url`, `update_date`, `regist_date`) VALUES
(1, 'https://twitter.com/orange__hah', '', '', '', '', '', '', '2021-03-29 19:06:11', '2020-06-08 23:51:32'),
(3, '', '', '', '', '', '', '', '2020-06-24 12:14:01', '2020-06-24 12:14:01'),
(5, '', '', '', '', '', '', '', '2021-04-02 22:14:16', '2021-04-02 22:14:16'),
(8, '', '', '', '', '', '', '', '2021-03-30 16:01:07', '2021-03-30 16:01:07'),
(12, 'https://twitter.com/orange__hah', '', '', '', '', '', '', '2021-03-29 19:05:30', '2021-03-29 19:05:30'),
(17, '', '', '', '', '', '', '', '2021-03-30 04:07:25', '2021-03-30 04:07:25'),
(20, '', '', '', '', '', '', '', '2021-03-30 04:49:08', '2021-03-30 04:49:08'),
(24, '', '', '', '', '', '', '', '2021-03-30 09:40:25', '2021-03-30 09:40:25'),
(26, '', '', '', '', '', '', '', '2021-03-31 01:08:04', '2021-03-31 01:08:04'),
(30, '', '', '', '', '', '', '', '2021-03-30 16:30:31', '2021-03-30 16:30:31');

-- --------------------------------------------------------

--
-- Table structure for table `frema_system`
--

CREATE TABLE IF NOT EXISTS `frema_system` (
  `order_number` int(11) NOT NULL DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `frema_system`
--

INSERT INTO `frema_system` (`order_number`) VALUES
(3);

-- --------------------------------------------------------

--
-- Table structure for table `frema_system_config`
--

CREATE TABLE IF NOT EXISTS `frema_system_config` (
  `item_name` varchar(50) NOT NULL DEFAULT '商品',
  `commission_rate` decimal(10,2) NOT NULL,
  `from_email` varchar(100) NOT NULL DEFAULT '',
  `from_email_name` varchar(100) NOT NULL DEFAULT '',
  `to_email` varchar(100) NOT NULL DEFAULT '',
  `site_name` varchar(250) NOT NULL DEFAULT '',
  `financial_institution_name` varchar(50) NOT NULL DEFAULT '' COMMENT '???Z?@?֖?',
  `branch_name` varchar(50) NOT NULL DEFAULT '' COMMENT '?x?X??',
  `deposit_type` tinyint(4) NOT NULL DEFAULT '0' COMMENT '?a??????',
  `account_number` varchar(10) NOT NULL DEFAULT '' COMMENT '?????ԍ?',
  `account_holder` varchar(50) NOT NULL DEFAULT '' COMMENT '???????`',
  `uneisha` varchar(250) NOT NULL DEFAULT '',
  `shozaichi` varchar(250) NOT NULL DEFAULT '',
  `company_name` varchar(250) NOT NULL DEFAULT '',
  `kiyaku_date` varchar(100) DEFAULT NULL,
  `keyword` varchar(100) DEFAULT NULL,
  `banner_url` varchar(250) DEFAULT '1',
  `description` text,
  `use_stripe` tinyint(1) NOT NULL DEFAULT '0',
  `stripe_access_key` varchar(200) NOT NULL DEFAULT '',
  `stripe_secret_key` varchar(200) NOT NULL DEFAULT '',
  `enabled_category` tinyint(1) NOT NULL,
  `enabled_delivery_type` tinyint(1) NOT NULL,
  `enabled_carriage_type` tinyint(1) NOT NULL,
  `enabled_postage` tinyint(1) NOT NULL,
  `enabled_pref` tinyint(1) NOT NULL,
  `enabled_item_state` tinyint(1) NOT NULL,
  `enabled_search_bar` tinyint(1) NOT NULL DEFAULT '0',
  `enabled_disp_pr` tinyint(1) DEFAULT '1',
  `enabled_image` tinyint(1) DEFAULT '1',
  `update_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `regist_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `frema_system_config`
--

INSERT INTO `frema_system_config` (`item_name`, `commission_rate`, `from_email`, `from_email_name`, `to_email`, `site_name`, `financial_institution_name`, `branch_name`, `deposit_type`, `account_number`, `account_holder`, `uneisha`, `shozaichi`, `company_name`, `kiyaku_date`, `keyword`, `banner_url`, `description`, `use_stripe`, `stripe_access_key`, `stripe_secret_key`, `enabled_category`, `enabled_delivery_type`, `enabled_carriage_type`, `enabled_postage`, `enabled_pref`, `enabled_item_state`, `enabled_search_bar`, `enabled_disp_pr`, `enabled_image`, `update_date`, `regist_date`) VALUES
('商品', 10.00, '', '', '', 'てっぱんプレミアムストア', 'MUFJ', 'Kamata', 1, '123456', 'Cheat', '', '', '', '20xx年xx月xx日', '', '', '', 1, 'pk_live_51IW7mnIwM6hBQwMQ5TZpegbwVz8MXhP6wpdUyL7ookNyfheUkyrzJI5iWr6uhrVAwzFk06f5GjWBLhKJOjp1pKs200Pej96zQp', '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', 1, 1, 1, 1, 1, 1, 0, 1, 1, '2021-03-31 09:34:26', '2020-04-07 05:20:26');

-- --------------------------------------------------------

--
-- Table structure for table `frema_type`
--

CREATE TABLE IF NOT EXISTS `frema_type` (
  `id` int(11) NOT NULL,
  `name` varchar(50) NOT NULL,
  `enabled` tinyint(1) NOT NULL DEFAULT '0',
  `price` int(11) NOT NULL,
  `commission_rate` int(11) NOT NULL,
  `number` int(11) NOT NULL,
  `regist_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `update_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `frema_type`
--

INSERT INTO `frema_type` (`id`, `name`, `enabled`, `price`, `commission_rate`, `number`, `regist_date`, `update_date`) VALUES
(1, '法人', 1, 0, 8, 2, '2017-04-17 22:18:58', '2017-04-17 22:18:58'),
(2, '個人', 1, 0, 8, 1, '2017-04-17 22:18:58', '2017-04-17 22:18:58');

-- --------------------------------------------------------

--
-- Table structure for table `frema_user`
--

CREATE TABLE IF NOT EXISTS `frema_user` (
  `id` int(11) NOT NULL,
  `hash_id` varchar(20) NOT NULL,
  `password` varchar(150) NOT NULL,
  `email` varchar(190) NOT NULL,
  `ipaddr` varchar(16) DEFAULT NULL,
  `nickname` varchar(250) NOT NULL,
  `name` varchar(50) NOT NULL,
  `name_kana` varchar(100) NOT NULL,
  `company` varchar(200) DEFAULT NULL,
  `tel` varchar(50) DEFAULT NULL,
  `addr` varchar(200) DEFAULT NULL,
  `zip` varchar(10) DEFAULT NULL,
  `pref` int(11) DEFAULT NULL,
  `financial_institution_name` varchar(50) NOT NULL DEFAULT '' COMMENT '???Z?@?֖?',
  `branch_name` varchar(50) NOT NULL DEFAULT '' COMMENT '?x?X??',
  `deposit_type` tinyint(4) NOT NULL DEFAULT '0' COMMENT '?a??????',
  `account_number` varchar(10) NOT NULL DEFAULT '' COMMENT '?????ԍ?',
  `account_holder` varchar(50) NOT NULL DEFAULT '' COMMENT '???????`',
  `self_pr` text,
  `type` int(11) DEFAULT NULL,
  `buying` tinyint(1) DEFAULT '1',
  `selling` tinyint(1) DEFAULT '1',
  `send_use` tinyint(1) DEFAULT NULL,
  `settle_plan` int(11) NOT NULL DEFAULT '1',
  `identification` tinyint(4) NOT NULL DEFAULT '0',
  `admin` tinyint(1) NOT NULL DEFAULT '0',
  `enabled` tinyint(1) NOT NULL DEFAULT '0',
  `invalid` tinyint(1) NOT NULL DEFAULT '0',
  `regist_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `update_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `age` int(11) DEFAULT NULL,
  `sex` tinyint(4) DEFAULT NULL,
  `item_interest` varchar(255) DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=22 DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `frema_user`
--

INSERT INTO `frema_user` (`id`, `hash_id`, `password`, `email`, `ipaddr`, `nickname`, `name`, `name_kana`, `company`, `tel`, `addr`, `zip`, `pref`, `financial_institution_name`, `branch_name`, `deposit_type`, `account_number`, `account_holder`, `self_pr`, `type`, `buying`, `selling`, `send_use`, `settle_plan`, `identification`, `admin`, `enabled`, `invalid`, `regist_date`, `update_date`, `age`, `sex`, `item_interest`) VALUES
(1, '9b59a991', '$2y$10$KF53FXSqO2ookC0gnZ1QOOjgJZhUD2o6x4Jb3zHhybgsh3nlq0rVm', 'teppan.system@gmail.com', '203.205.51.20', 'System admin', 'システム管理者', 'システムカンリシャ', '', '123456789', 'Tokyo', '123-0056', 1, '', '', 0, '', '', '', 2, 1, 1, NULL, 1, 1, 1, 1, 0, '2021-03-31 08:14:18', '2021-04-26 07:19:51', NULL, NULL, NULL),
(2, '07a2c711', '$2y$10$XPXPMZDqV/P3oPYwxfV.MeEPwtV0pnKH.e8sSh8/rOnuJw1u6/7lO', 'qdang828@gmail.com', '118.69.225.49', '', 'test', 'カナカナ', NULL, NULL, 'test', '000-0000', NULL, '金融機関名', '1', 1, '1', '1', NULL, NULL, 1, 1, NULL, 1, 0, 0, 1, 0, '2021-03-31 08:55:50', '2021-04-06 02:09:27', NULL, NULL, NULL),
(3, 'f0cea1e6', '$2y$10$LqZartR05r4Ll/Dhl0.M1uLQImeTdZcGmkgAVipAaOR6UGFQZf8H.', 'test@gmail.com', '118.69.225.49', '', 'test', 'カナカナ', NULL, NULL, 'test', '000-0000', NULL, '', '', 0, '', '', NULL, NULL, 1, 1, NULL, 1, 0, 0, 0, 0, '2021-03-31 09:52:14', '2021-03-31 09:52:14', NULL, NULL, NULL),
(4, 'b3e6a2eb', '$2y$10$PhCW6bYMGdidk12Rj1tLKufh6dDZSa1hcSa92cISPS1dJurgxQ466', 'akiodayo4@gmail.com', '211.135.138.54', '', '岡崎幸太', 'おかざきこうた', NULL, NULL, '東京都渋谷区南平台町', '150-0036', NULL, '', '', 0, '', '', NULL, NULL, 1, 1, NULL, 1, 0, 0, 1, 0, '2021-03-31 10:51:22', '2021-04-19 14:54:14', NULL, NULL, NULL),
(5, '5e97bddc', '$2y$10$TTAsn0xtyHUMa5r.Y9wPNuyf9RoGBhCD6jgnIxLhnHLtA8qr8npFm', 'kimiyo1923@au.com', '106.133.50.6', 'きんちゃん', '楠原　公代', 'なんばら　きみよ', '', '', '東京都台東区谷中2-1-11', '110-0001', 0, '', '', 0, '', '', '', 0, 1, 1, NULL, 1, 0, 0, 1, 0, '2021-03-31 11:45:48', '2021-04-23 13:21:41', 0, 0, ''),
(6, '274216be', '$2y$10$LlFcU8gErlt8W/Dsy8Y6lOx5E1HyCcTmM6Dp557eLdlp7fAbHKgy.', 'fukuzawa@gaziru.co.jp', '183.180.169.62', '', '福澤茂和', '福澤茂和', NULL, NULL, '川崎市中原区上丸子山王町1-1465', '211-0002', NULL, '', '', 0, '', '', NULL, NULL, 1, 1, NULL, 1, 0, 0, 1, 0, '2021-04-01 01:08:19', '2021-04-06 07:04:48', NULL, NULL, NULL),
(7, 'c423298b', '$2y$10$GjcIOcIumx4V4iGPgBB0ge2gFWSNehqJ2gKHM8yIWcYWoFza7963q', 'ando@cheat.co.jp', '60.134.36.211', '', 'テスト', 'てすと', NULL, NULL, '4丁目1-10', '651-0087', NULL, '', '', 0, '', '', NULL, NULL, 1, 1, NULL, 1, 0, 0, 1, 0, '2021-04-01 04:29:28', '2021-04-01 04:30:37', NULL, NULL, NULL),
(9, '5439552e', '$2y$10$g75Lre8D3oDK8fOIdhv6Jez1O3jU/3rDyZdFW56LIbf7ZXUugJIBm', 'lovelovephuket0801+mailtest1@gmail.com', '122.212.131.170', 'okazaki4', 'okazaki4', '', NULL, NULL, '', '', NULL, '', '', 0, '', '', NULL, NULL, 1, 1, NULL, 1, 0, 0, 1, 0, '2021-04-02 17:09:58', '2021-04-02 17:30:39', 35, 0, '1,2,8'),
(10, '1eef7ea7', '$2y$10$qo.F64IK2X0/VG9Eo2L1KeGBuh2IGIcSv3QkkkSYqV3bDlrE6wpGe', 'et3yfhgegf@i.SoftBank.jp', '114.158.248.56', 'もーヤン', 'もーヤン', '', NULL, NULL, '', '', NULL, '', '', 0, '', '', NULL, NULL, 1, 1, NULL, 1, 0, 0, 1, 0, '2021-04-02 23:11:41', '2021-04-02 23:12:09', 50, 0, ''),
(11, '997e8056', '$2y$10$fXhHf0MFS3JAZm9ddMKUd.5ai9lmsgqO.6LrtXCV3GN8l/PE4ecvG', 'katsuyuki471208@gmail.com', '133.175.219.188', 'かっつ', 'かっつ', '', NULL, NULL, '', '', NULL, '', '', 0, '', '', NULL, NULL, 1, 1, NULL, 1, 0, 0, 1, 0, '2021-04-02 23:23:34', '2021-04-02 23:23:52', 48, 0, '2,6,9'),
(12, '78c612b2', '$2y$10$.JN7bzd6NRUQa7lQCFGl2.josKuwtt4iuhajszKs79q8R315zGMRi', 'y2km@docomo.ne.jp', '49.98.147.4', 'ガルミ', 'ガルミ', '', NULL, NULL, '', '', NULL, '', '', 0, '', '', NULL, NULL, 1, 1, NULL, 1, 0, 0, 0, 0, '2021-04-03 00:47:30', '2021-04-03 00:47:30', 48, 1, ''),
(13, '51e11e1d', '$2y$10$.mO74D66qoNKGEJt6twdyeiNP.1QzxFY63hO1Co0YNQGiPbzxbYSO', 'keicakutekini', '60.150.193.127', 'deburuneko', 'deburuneko', '', NULL, NULL, '', '', NULL, '', '', 0, '', '', NULL, NULL, 1, 1, NULL, 1, 0, 0, 0, 0, '2021-04-03 05:00:14', '2021-04-03 05:00:14', 44, 1, '1'),
(14, 'e82c9b73', '$2y$10$QuZZ1gTJCc/8HdPlyv7qEO20rGzfsy3DpsY7tebGsjeUPflGasjeS', 'keicakutekini@gmail.com', '60.150.193.127', 'deburuneko', 'deburuneko', '', NULL, NULL, '', '', NULL, '', '', 0, '', '', NULL, NULL, 1, 1, NULL, 1, 0, 0, 1, 0, '2021-04-03 05:32:17', '2021-04-04 02:58:59', 44, 1, '1'),
(15, '5acd9485', '$2y$10$OWvUcboKGwiiqAkzBbqt7.k2h6mfQK60J0bqSpOqM8.O36LWXXZ/y', 'hide3.hirano@gmail.com', '221.112.63.18', '平野　秀実', '平野　秀実', '', NULL, NULL, '', '', NULL, '', '', 0, '', '', NULL, NULL, 1, 1, NULL, 1, 1, 0, 1, 0, '2021-04-05 03:25:32', '2021-04-23 04:09:34', 50, 0, '2,8,9'),
(18, '10dbf026', '$2y$10$b4G.yTJ8TLqB699dMCSLHe/ydw0KEFEZyQN/PLb0xhvoCpTp7pVUu', 'ngan12091998t3@gmail.com', '203.205.51.20', 'Ngan Tran', 'Ngan Tran', '', NULL, NULL, '', '', NULL, '', '', 0, '', '', NULL, NULL, 1, 1, NULL, 1, 0, 0, 0, 0, '2021-04-12 09:28:35', '2021-04-12 09:28:35', 23, 1, '1,4,10'),
(19, 'c6683cdd', '$2y$10$4262k4.DA2EVJJ0.xtX/O.3H70wPY5mcGDxf467XnwqYsZ/hm4B6G', 'h.hirano@nonno-teppan.jp', '1.75.3.8', 'ヒデ', 'ヒデ', '', NULL, NULL, '', '', NULL, '', '', 0, '', '', NULL, NULL, 1, 1, NULL, 1, 0, 0, 1, 0, '2021-04-15 07:18:59', '2021-04-15 07:19:57', 0, 0, '8,9,10'),
(20, '32500387', '$2y$10$NJVZxIiERPzR/7uNW2kVveCEz5LQbVE2f.CzrDchoj8zb8yXNtXWS', 'y.akiyama@nonno-teppan.jp', '153.242.178.12', '秋山', '秋山', 'アキヤマ', '', '', '', '', 0, '', '', 0, '', '', '', 0, 1, 1, NULL, 1, 0, 0, 1, 0, '2021-04-15 07:43:57', '2021-04-23 09:22:13', 0, 1, ''),
(21, 'e9175afb', '$2y$10$DFBnnwYHBExypMmEa8fiwu9lAOz5xx9C59G65bGT8HeKmAhWfRvhC', 'rukirukiclan@gmail.com', '221.112.63.18', '平野２', '平野２', '', NULL, NULL, '', '', NULL, '', '', 0, '', '', NULL, NULL, 1, 1, NULL, 1, 1, 0, 1, 0, '2021-04-23 03:56:40', '2021-04-23 04:08:58', 50, 0, '2');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `frema_block_access`
--
ALTER TABLE `frema_block_access`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `frema_block_access_idx1` (`user_id`,`block_user_id`),
  ADD KEY `frema_block_access_idx2` (`block_user_id`);

--
-- Indexes for table `frema_card_info`
--
ALTER TABLE `frema_card_info`
  ADD PRIMARY KEY (`user_id`);

--
-- Indexes for table `frema_carriage_plan`
--
ALTER TABLE `frema_carriage_plan`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `frema_carriage_type`
--
ALTER TABLE `frema_carriage_type`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `frema_cash_flow`
--
ALTER TABLE `frema_cash_flow`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `frema_category`
--
ALTER TABLE `frema_category`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `frema_delivery_type`
--
ALTER TABLE `frema_delivery_type`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `frema_evaluate`
--
ALTER TABLE `frema_evaluate`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `frema_favorites`
--
ALTER TABLE `frema_favorites`
  ADD PRIMARY KEY (`user_id`,`item_id`);

--
-- Indexes for table `frema_free_input_item`
--
ALTER TABLE `frema_free_input_item`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `frema_free_input_item_value`
--
ALTER TABLE `frema_free_input_item_value`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `frema_good`
--
ALTER TABLE `frema_good`
  ADD PRIMARY KEY (`item_id`,`user_id`);

--
-- Indexes for table `frema_information`
--
ALTER TABLE `frema_information`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `frema_invoice`
--
ALTER TABLE `frema_invoice`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `frema_invoice_idx1` (`user_id`,`month`);

--
-- Indexes for table `frema_invoice_details`
--
ALTER TABLE `frema_invoice_details`
  ADD PRIMARY KEY (`order_id`);

--
-- Indexes for table `frema_item`
--
ALTER TABLE `frema_item`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `frema_item_free_input`
--
ALTER TABLE `frema_item_free_input`
  ADD PRIMARY KEY (`item_id`,`free_input_item_id`);

--
-- Indexes for table `frema_item_photo`
--
ALTER TABLE `frema_item_photo`
  ADD PRIMARY KEY (`item_id`,`number`);

--
-- Indexes for table `frema_item_state`
--
ALTER TABLE `frema_item_state`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `frema_item_tmp`
--
ALTER TABLE `frema_item_tmp`
  ADD PRIMARY KEY (`id`),
  ADD KEY `frema_item_tmp_idx01` (`owner_id`) USING BTREE;

--
-- Indexes for table `frema_login_session`
--
ALTER TABLE `frema_login_session`
  ADD PRIMARY KEY (`hash_key`);

--
-- Indexes for table `frema_mail_history`
--
ALTER TABLE `frema_mail_history`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `frema_mail_history_target`
--
ALTER TABLE `frema_mail_history_target`
  ADD PRIMARY KEY (`mail_history_id`,`user_id`);

--
-- Indexes for table `frema_negotiation`
--
ALTER TABLE `frema_negotiation`
  ADD PRIMARY KEY (`id`),
  ADD KEY `item_id` (`item_id`,`user_id`);

--
-- Indexes for table `frema_order`
--
ALTER TABLE `frema_order`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `frema_order_idx1` (`item_id`);

--
-- Indexes for table `frema_order_number`
--
ALTER TABLE `frema_order_number`
  ADD PRIMARY KEY (`order_number`);

--
-- Indexes for table `frema_payout_history`
--
ALTER TABLE `frema_payout_history`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `frema_pr`
--
ALTER TABLE `frema_pr`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `frema_pref`
--
ALTER TABLE `frema_pref`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `frema_request`
--
ALTER TABLE `frema_request`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `frema_request_follow`
--
ALTER TABLE `frema_request_follow`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `frema_settle_plan`
--
ALTER TABLE `frema_settle_plan`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `frema_sns`
--
ALTER TABLE `frema_sns`
  ADD PRIMARY KEY (`user_id`);

--
-- Indexes for table `frema_type`
--
ALTER TABLE `frema_type`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `frema_user`
--
ALTER TABLE `frema_user`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `frema_user_idx1` (`email`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `frema_block_access`
--
ALTER TABLE `frema_block_access`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `frema_carriage_plan`
--
ALTER TABLE `frema_carriage_plan`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `frema_carriage_type`
--
ALTER TABLE `frema_carriage_type`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT for table `frema_cash_flow`
--
ALTER TABLE `frema_cash_flow`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `frema_category`
--
ALTER TABLE `frema_category`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=11;
--
-- AUTO_INCREMENT for table `frema_delivery_type`
--
ALTER TABLE `frema_delivery_type`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=12;
--
-- AUTO_INCREMENT for table `frema_free_input_item_value`
--
ALTER TABLE `frema_free_input_item_value`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `frema_information`
--
ALTER TABLE `frema_information`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `frema_invoice`
--
ALTER TABLE `frema_invoice`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `frema_item`
--
ALTER TABLE `frema_item`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=9;
--
-- AUTO_INCREMENT for table `frema_item_state`
--
ALTER TABLE `frema_item_state`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=8;
--
-- AUTO_INCREMENT for table `frema_item_tmp`
--
ALTER TABLE `frema_item_tmp`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `frema_mail_history`
--
ALTER TABLE `frema_mail_history`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `frema_negotiation`
--
ALTER TABLE `frema_negotiation`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `frema_order`
--
ALTER TABLE `frema_order`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=10;
--
-- AUTO_INCREMENT for table `frema_payout_history`
--
ALTER TABLE `frema_payout_history`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `frema_request`
--
ALTER TABLE `frema_request`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `frema_request_follow`
--
ALTER TABLE `frema_request_follow`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `frema_user`
--
ALTER TABLE `frema_user`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=22;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
