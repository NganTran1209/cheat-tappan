<?php 
$title_page = 'パスワード再発行';
$description_page = '欲しい商品を画像で確認・届いた商品を画像で照合！気持ち良い取引が叶うオンラインマーケット。';
 ?>
<?php include_once(__DIR__ . '/common/util.php'); ?>
<?php include_once(__DIR__ . '/entity/user.php'); ?>
<?php
if (isset($_POST['email'])) {
    $entity = new User();
    $entity->email = $_POST['email'];
    if ($entity->has()) {
        $entity->password = hash('crc32', $entity->email . date('YmdHis'));
        setMessage($entity->password);
        $entity->resetPassword();
        setMessage('パスワード再発行メールを送信しました。');
    } else {
        setMessage('入力されたメールアドレスは登録されておりません。');
    }

}
?>
<?php include('other_header.php'); ?>
    <div class="container">
        <div class="row">
            <div class="col-md-9 mainContents p-rp__form u-mgauto">
                <div class="bg-inner">
                    <?php if (!isLogin()): ?>
                        <aside id="formlogin">
                            <form action="" method="post" name="Login_Form" class="form-signin wow animate__animated animate__fadeInUp">
                                <h3 class="form-signin-heading"><i class="fas fa-key mr-2"></i>パスワードの再発行</h3>
                                <hr class="colorgraph">
                                <input type="text" class="form-control mb-2" name="email" placeholder="ログインIDに使用しているメールアドレス"/>
                                <button class="btn btn-sm btn-login-color btn-block  btn-danger" name="Submit" value="reset"
                                        type="Submit">メール送信
                                </button>
                            </form>
                        </aside>
                    <?php endif; ?>
                </div>
            </div>
            <div class="col-md-3 sideContents u-none">
                <?php include('sidebar.php'); ?>
            </div>
        </div>
    </div>

<?php include('footer.php'); ?>