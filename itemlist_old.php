<?php include_once(__DIR__ . '/common/util.php'); ?>
<?php include(__DIR__ . '/user_header.php'); ?>
<?php
$uri = $_SERVER["REQUEST_URI"];
$evaluate = new Evaluate();
$evaluateMap = $evaluate->selectSummaryMap();
$items = array();
$items_tmp = array();
$entity = new Item();
$category_title = 'すべて';
$page = 1;

$search = empty($_POST['search']) ? '' : $_POST['search'];
$category = empty($_POST['category']) ? '' : $_POST['category'];
$sort = empty($_POST['sort']) ? '' : $_POST['sort'];
$owner_id = empty($_POST['owner_id']) ? '' : $_POST['owner_id'];
$page = empty($_POST['page']) ? '1' : $_POST['page'];

if (!empty($_GET['category'])) {
    $category = $_GET['category'];
}
if (!empty($_GET['search'])) {
    $search = $_GET['search'];
}
if (!empty($_GET['owner_id'])) {
    $owner_id = $_GET['owner_id'];
}
if (!empty($_GET['page'])) {
    $page = $_GET['page'];
}

if (!empty($category)) {
    $items_tmp = $entity->selectCategory($category);
    $category_title = selectCodeName('selectCategoryById', $category);
} elseif (!empty($search)) {
    $items_tmp = $entity->selectSearch($search);
    $category_title = '検索文字「' . $search . '」';
} else {
    $items_tmp = $entity->selectAll();
}
foreach ($items_tmp as $item) {
    if ($item->enabled != 1) {
        continue;
    }
    if (!$item->owner_selling) {
        continue;
    }
    if (!empty($owner_id) && $item->owner_id != $owner_id) {
        continue;
    }
    //if ($item->order_id != null) { continue; }
    $items[] = $item;
}

// ページ数制御 商品増幅＠テスト用
//{
//	for ($i = 0; $i < 500; $i++) {
//		$items[] = $items[0];
//	}
//}


// ソート
if (!empty($sort)) {
    if ($sort == '2') {
        usort($items, function ($a, $b) {
// 			if (empty($a->buyer_id) && !empty($b->buyer_id)) {
// 				return -1;
// 			} elseif (!empty($a->buyer_id) && empty($b->buyer_id)) {
// 				return 1;
// 			}
            return $b->price - $a->price;
        });
    } elseif ($sort == '3') {
        usort($items, function ($a, $b) {
// 			if (empty($a->buyer_id) && !empty($b->buyer_id)) {
// 				return -1;
// 			} elseif (!empty($a->buyer_id) && empty($b->buyer_id)) {
// 				return 1;
// 			}
            return $a->price - $b->price;
        });
    } else {
        usort($items, function ($a, $b) {
// 			if (empty($a->buyer_id) && !empty($b->buyer_id)) {
// 				return -1;
// 			} elseif (!empty($a->buyer_id) && empty($b->buyer_id)) {
// 				return 1;
// 			} elseif (!empty($a->buyer_id) && !empty($b->buyer_id)) {
// 				return $a->order_date >= $b->order_date ? -1 : 1 ;
// 			}
            return $a->regist_date >= $b->regist_date ? -1 : 1;
        });
    }
} else {
    usort($items, function ($a, $b) {
// 		if (empty($a->buyer_id) && !empty($b->buyer_id)) {
// 			return -1;
// 		} elseif (!empty($a->buyer_id) && empty($b->buyer_id)) {
// 			return 1;
// 		} elseif (!empty($a->buyer_id) && !empty($b->buyer_id)) {
// 			return $a->regist_date >= $b->regist_date ? -1 : 1;
// 		}
        return $a->regist_date >= $b->regist_date ? -1 : 1;
    });
}

if (!empty($owner_id)) {
    $owner = new User();
    $owner->select($owner_id);
    $category_title = $owner->getViewId();
}

?>
    <form id="fm-param" method="post">
        <input type="hidden" id="id-search" name="search" value="<?= $search ?>"/>
        <input type="hidden" id="id-category" name="category" value="<?= $category ?>"/>
        <input type="hidden" id="id-sort" name="sort" value="<?= $sort ?>"/>
        <input type="hidden" id="id-owner_id" name="owner_id" value="<?= $owner_id ?>"/>
        <input type="hidden" id="id-page" name="page" value="<?= $page ?>"/>
    </form>
    <div class="com-header-top">
        <div class="com-header-top__img">
            <img src="<?php echo HOME_URL; ?>/common/assets/img/common/other-com-header.png" alt="">
        </div>
        <div class="com-header-top__path bg-other-01">
            <p><span><?= $category_title ?>の商品一覧</span></p>
        </div>
        <div class="com-header-top__txt">
            <p><?= $category_title ?>の商品一覧</p>
        </div>
       <!--  <div class="com-header-top__txt">
            <p>出品者：<a href="<?php echo HOME_URL; ?>/user/seller_home.php">匿名希望さん</a></p>
        </div> -->
    </div>
<!--     <div>
        <div class="text-right itemlist-wrap">
            
            <i class="fa fa-sort" aria-hidden="true"></i>
            <a class="sortButton" href="" onclick="submitForm('fm-param', 'sort', '1'); return false;">新着順</a>
            <a class="sortButton" href=""
               onclick="submitForm('fm-param', 'sort', '2'); return false;">価格の高い順</a>
            <a class="sortButton" href=""
               onclick="submitForm('fm-param', 'sort', '3'); return false;">価格の安い順</a>
        </div>
    </div> -->
    <div class="customer-container">
        <div class="ladies-new-term">
            <div class="ladies-item-row">
                <div class="ladies-item-row-group">
                <!-- <div class="bg-inner"> -->

                    <?php
                    $count = 0;
                    $index = 0;
                    foreach ($items as $item) {
                        if (User::isInvalid($item->owner_id)) {
                            continue;
                        }
                        if ($item->order_id != null && diffDays(date("Y/m/d"), $item->order_date) > MAX_VIEW_DAYS) {
                            continue;
                        }
//                             if ($item->isBlock()) {
//                                 continue;
//                             }
                        $count++;
                        if (ceil(++$index / MAX_PAGE_COUNT) != $page) {
                            continue;
                        }
                        ?>
                        <?php
                        $evaluate = new Evaluate();
                        $evaluate->selectSummary($item->owner_id);
                        ?>

                        <a href="<?php echo HOME_URL; ?>/itemdetail.php?id=<?= $item->id ?>" class="ladies-group-column" style="background: url('<?= $item->getThumbnailUrl() ?>');">
                            <?php if ($item->order_id != null): ?>
                                <div class="soldOutBadgeMini">
                                    <div>SOLD</div>
                                </div>
                            <?php endif; ?>
                            <div class="ladies-group-column__img">
                                <!-- <img src="<?= $item->getThumbnailUrl() ?>" alt=""> -->
                                <div class="ladies-group-column__img-bottom"><?= $item->title ?></div>
                            </div><!-- 
                            <div class="ladies-group-column__icon">
                                <img src="<?php echo HOME_URL; ?>/common/assets/img/common/icon/profile.png" alt="">
                            </div> -->
                            <div class="ladies-group-column__price">
                                <p><span>¥<?= number_format($item->price) ?></span></p>
                            </div>
                        </a>

                    <?php } ?>


                    <?php /*?>
					<?php foreach ($items as $item) { ?>
					<section class="col-sm-4">
						<a href="itemdetail.php?id=<?= $item->id ?>">
							<div class="">
								<img class="itemlist-img" src="<?= 'user/item_photo.php?id='.$item->id ?>&number=1" width="638" height="704" alt=""/>
							</div>
							<div class="">
								<div class=""><img class="iconimg" src="<?php echo HOME_URL; ?>/images/lapias/icon_0<?= $item->category ?>.png" width="20" height="20" alt="<?= $item->category_name ?>" title="<?= $item->category_name ?>"/>
									<?= $item->title ?>
									<?= $item->order_id == null ? '' : '[売切れ]'?>
								</div>
								<div class="fontBold">
									<?= number_format($item->price) ?>円</div>
							</div>
						</a>
					</section>
					<?php } ?>
					<?php */ ?>

                </div>

            </div>
            
                    <?php if ($count == 0) : ?>
                        <div class="card mb-3">
                            <div class="card-body">
                                <div class="card-text font20 fontBold">
                                    検索結果 0件
                                </div>
                                <p>
                                    該当する商品が見つかりませんでした。商品は増えていますので、これからの出品にご期待ください。
                                    また、購入希望情報への投稿もご利用ください。
                                </p>
                            </div>
                        </div>
                    <?php else: ?>
                        <div class="text-center my-2">
                            <?php
                            $prev_page = $page - 1;
                            $max_page = ceil($count / MAX_PAGE_COUNT);
                            $next_page = min($max_page, $page + 1);
                            $view_start_page = max(1, $page - ceil(VIEW_PAGE_SELECT_COUNT / 2));
                            $view_last_page = min($view_start_page + VIEW_PAGE_SELECT_COUNT - 1, $max_page);
                            if ($view_last_page - $view_start_page < VIEW_PAGE_SELECT_COUNT) {
                                $view_start_page = max(1, $view_last_page - VIEW_PAGE_SELECT_COUNT + 1);
                            }
                            ?>
                            <?php if ($prev_page > 0): ?>
                                <a class="sortButton" href=""
                                   onclick="submitForm('fm-param', 'page', '<?= $prev_page ?>'); return false;">前へ</a>
                            <?php else: ?>
                                <a class="sortButton" href="" onclick="return false;">前へ</a>
                            <?php endif; ?>

                            <?php for ($i = $view_start_page; $i <= $view_last_page; $i++): ?>
                                <?php if ($page != $i): ?>
                                    <a class="sortButton" href=""
                                       onclick="submitForm('fm-param', 'page', '<?= $i ?>'); return false;"><?= $i ?></a>
                                <?php else: ?>
                                    <a class="sortButton active" href="" onclick="return false;"><?= $i ?></a>
                                <?php endif; ?>
                            <?php endfor; ?>

                            <?php if ($page < $max_page): ?>
                                <a class="sortButton" href=""
                                   onclick="submitForm('fm-param', 'page', '<?= $next_page ?>'); return false;">次へ</a>
                            <?php else: ?>
                                <a class="sortButton" href="" onclick="return false;">次へ</a>
                            <?php endif; ?>

                            <p class="small"><?= number_format(($page - 1) * MAX_PAGE_COUNT + 1) ?>
                                ～<?= number_format(min($count, $page * MAX_PAGE_COUNT)) ?>件/<?= number_format($count) ?>
                                件</p>
                        </div>
                    <?php endif; ?>
        </div>
    </div>


<?php include('footer.php'); ?>