<div id="footer" class="small text-center mt-4">
    <div class="container">
        <ul class="list-inline">
            <li class="list-inline-item"><a href="<?php echo HOME_URL; ?>/">ホーム</a></li>
            <li class="list-inline-item"><a href="<?php echo HOME_URL; ?>/pages/kiyaku.php">利用規約</a></li>
            <li class="list-inline-item"><a href="<?php echo HOME_URL; ?>/pages/tokusho.php">特定商取引法に基づく表示</a></li>
            <li class="list-inline-item"><a href="<?php echo HOME_URL; ?>/pages/privacypolicy.php">プライバシーポリシー</a></li>
            <li class="list-inline-item"><a href="<?php echo HOME_URL; ?>/pages/faq.php">よくあるご質問</a></li>
            <li class="list-inline-item"><a href="<?php echo HOME_URL; ?>/mail">お問い合わせ</a></li>
        </ul>
    </div>
    <div class="copyright p-2">
        <div class="container">
            &copy; 2021 <?= $system_config->site_name ?>
        </div>
    </div>
</div>
<script src="https://code.jquery.com/jquery-3.4.1.min.js"
        integrity="sha256-CSXorXvZcTkaix6Yvo6HppcZGetbYMGWSFlBw8HfCJo="
        crossorigin="anonymous"></script>
<script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.0/dist/umd/popper.min.js"
        integrity="sha384-Q6E9RHvbIyZFJoft+2mJbHaEWldlvI9IOYy5n3zV9zzTtmI3UksdQRVvoxMfooAo"
        crossorigin="anonymous"></script>
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.js"
        integrity="sha384-wfSDF2E50Y2D1uUdj0O3uMBJnjuUD4Ih7YwaYd1iqfktj0Uod8GCExl3Og8ifwB6"
        crossorigin="anonymous"></script>
<script>
    // input Form File に アップロードする文字列を挿入する
    !function(e,t){"object"==typeof exports&&"undefined"!=typeof module?module.exports=t():"function"==typeof define&&define.amd?define(t):(e=e||self).bsCustomFileInput=t()}(this,function(){"use strict";var s={CUSTOMFILE:'.custom-file input[type="file"]',CUSTOMFILELABEL:".custom-file-label",FORM:"form",INPUT:"input"},l=function(e){if(0<e.childNodes.length)for(var t=[].slice.call(e.childNodes),n=0;n<t.length;n++){var l=t[n];if(3!==l.nodeType)return l}return e},u=function(e){var t=e.bsCustomFileInput.defaultText,n=e.parentNode.querySelector(s.CUSTOMFILELABEL);n&&(l(n).textContent=t)},n=!!window.File,r=function(e){if(e.hasAttribute("multiple")&&n)return[].slice.call(e.files).map(function(e){return e.name}).join(", ");if(-1===e.value.indexOf("fakepath"))return e.value;var t=e.value.split("\\");return t[t.length-1]};function d(){var e=this.parentNode.querySelector(s.CUSTOMFILELABEL);if(e){var t=l(e),n=r(this);n.length?t.textContent=n:u(this)}}function v(){for(var e=[].slice.call(this.querySelectorAll(s.INPUT)).filter(function(e){return!!e.bsCustomFileInput}),t=0,n=e.length;t<n;t++)u(e[t])}var p="bsCustomFileInput",m="reset",h="change";return{init:function(e,t){void 0===e&&(e=s.CUSTOMFILE),void 0===t&&(t=s.FORM);for(var n,l,r=[].slice.call(document.querySelectorAll(e)),i=[].slice.call(document.querySelectorAll(t)),o=0,u=r.length;o<u;o++){var c=r[o];Object.defineProperty(c,p,{value:{defaultText:(n=void 0,n="",(l=c.parentNode.querySelector(s.CUSTOMFILELABEL))&&(n=l.textContent),n)},writable:!0}),d.call(c),c.addEventListener(h,d)}for(var f=0,a=i.length;f<a;f++)i[f].addEventListener(m,v),Object.defineProperty(i[f],p,{value:!0,writable:!0})},destroy:function(){for(var e=[].slice.call(document.querySelectorAll(s.FORM)).filter(function(e){return!!e.bsCustomFileInput}),t=[].slice.call(document.querySelectorAll(s.INPUT)).filter(function(e){return!!e.bsCustomFileInput}),n=0,l=t.length;n<l;n++){var r=t[n];u(r),r[p]=void 0,r.removeEventListener(h,d)}for(var i=0,o=e.length;i<o;i++)e[i].removeEventListener(m,v),e[i][p]=void 0}}});
    // 追加
    bsCustomFileInput.init();
</script>
<?php $url = $_SERVER["REQUEST_URI"]; if (strstr($url, "admin/system_config.php") == true):?>
    <!--<script>-->
    <!--    // 取引コメントのパネル開閉用-->
    <!--    $(function () {-->
    <!--        //最初は全てのパネルを非表示に-->
    <!--        $('#koushou-panel > .koushou-content').hide();-->
    <!--        $('#koushou-panel > .koushou-title')-->
    <!--            .click(function (e) {-->
    <!--                //選択したパネルを開く-->
    <!--                $('+.koushou-content', this).slideToggle(300);-->
    <!--            })-->
    <!--    });-->
    <!--</script>-->
    <script>
        // 管理画面のON/OFF動作
        $(function() {
            $('input').change(function() {

                data = {
                    target: $(this).attr('name'),
                    value: $(this).val()
                };
                if ($(this).attr('type') == 'checkbox') {
                    data.value = $(this).is(':checked');
                }

                $.ajax({
                    'dataType': 'json',
                    'type': 'PUT',
                    'url': '<?php echo HOME_URL; ?>/api/admin/system_config.php',
                    'data': JSON.stringify(data)
                });

            });
        });
    </script>
<?php endif;?>
<?php $url = $_SERVER["REQUEST_URI"]; if (strstr($url, "admin/userlist.php") == true):?>
    <script src="<?php echo HOME_URL; ?>/common/js/quicksearch/src/jquery.quicksearch.js"></script>
    <script>
        // 管理画面ユーザー絞込
        $(function () {
            var page = 0;
            //表示行数設定
            var showLength = 30;
            $('input#id_search').quicksearch('table tbody tr', {
                'show': function () {
                    $(this).show();
                    $(this).removeClass('hide-search');
                },
                'hide': function () {
                    $(this).hide();
                    $(this).addClass('hide-search');
                },
                'onAfter': function () {
                    page = 0;
                    draw();
                }
            });

            function draw() {
                $('#page').html(page + 1);
                $('table tbody tr').hide();
                var length = $('table tbody').children().length;
                var count = 0;
                for (var i = 1; i <= length; i++) {
                    if ($('tr:eq(' + i + ')').hasClass('hide-search')) {
                        continue;
                    }
                    if (count >= page * showLength) {
                        $('tr:eq(' + i + ')').show();
                    }
                    if (++count >= (page + 1) * showLength) {
                        break;
                    }
                }
            }

            $('#prev').click(function () {
                if (page > 0) {
                    page--;
                    draw();
                }
            });
            $('#next').click(function () {
                if (page < ($('tr').size() - 1) / showLength - 1) {
                    page++;
                    draw();
                }
            });
            draw();
        });
    </script>
<?php endif;?>
</body>
</html>