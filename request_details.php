<?php include_once(__DIR__ . '/common/util.php'); ?>
<?php
if (!empty($_POST['action'])) {
    if ($_POST['action'] == 'cancel') {
        $req = new Request();
        $req->select($_POST['reqId']);
        if ($req->user_id == getUserId()) {
            $req->regection();
            setMessage("購入希望[$req->title]を取り消しました。");
        } else {
            setMessage('不正アクセスです。');
        }
        header('Location: '.getContextRoot().'/');
        exit;
    }
}


$req = new Request();
if (!empty($_GET['id'])) {
    $req->select($_GET['id']);
    $user = new User();
    $user->select($req->user_id);

    $user_evaluate = new Evaluate();
    $user_evaluate->selectSummary($req->user_id);
}

$page = 1;
$page = empty($_POST['page']) ? '1' : $_POST['page'];
if (!empty($_GET['page'])) {
    $page = $_GET['page'];
}
?>
<?php include('header.php'); ?>
<div class="p-requestDetail">
    <form id="fm-param" method="post">
        <input type="hidden" id="id-page" name="page" value="<?= $page ?>"/>
    </form>
    <div class="container">
        <div class="row">
            <?php if (!empty($_GET['id'])): ?>
                <div class="col-md-9 mainContents">
                    <div class="bg-inner">
                        <h1 class="wow animate__animated animate__fadeInUp"><?= $req->title ?></h1>
                        <div class="information_details_contents wow animate__animated animate__fadeInUp">
                            <div class="dateName">
                                <?= $req->update_date ?>
                                購入希望者ID：<?= $user->getViewId() ?>　<a
                                        href="<?= getContextRoot(); ?>/evaluate.php?user_id=<?= $user->id ?>">評価</a>：<?= $user_evaluate->point ?>
                            </div>
                            <div class="itemContentText">
                                <?php echo nl2br(htmlentities($req->text)); ?>
                            </div>
                            <div class="font20 fontBold text-center">
                                予算：<?= number_format($req->price) ?> 円
                            </div>
                        </div>
                        <?php if (BlockAccess::isBlock($req->user_id)) : ?>
                            <p class="wow animate__animated animate__fadeInUp">
                                この購入希望者とは取引できません。
                            </p>
                        <?php elseif ($req->user_id != getUserId()) : ?>
                            <form method="post" action="user/offerlist.php" class="wow animate__animated animate__fadeInUp">
                                <input type="hidden" name="reqId" value="<?= $req->id ?>"/>
                                <input class="btn btn-block btn-info btn-sm w-75 mx-auto" type="submit"
                                       value="出品・商品を紹介する"/>
                            </form>
                            <p class="text-center wow animate__animated animate__fadeInUp">
                                購入希望者へ自分の商品を紹介することができます。
                            </p>
                        <?php else: ?>
                            <form method="post" action="" class="wow animate__animated animate__fadeInUp">
                                <input type="hidden" name="action" value="cancel"/>
                                <input type="hidden" name="reqId" value="<?= $req->id ?>"/>
                                <input class="btn btn-block btn-info btn-sm w-75 mx-auto" type="submit"
                                       value="購入希望を取り消す"/>
                            </form>
                        <?php endif; ?>
                    </div>
                </div>
            <?php else: ?>
                <div class="col-md-9 mainContents">
                    <div class="bg-inner wow animate__animated animate__fadeInUp">
                        <h1 >購入希望情報</h1>
                        <ul class="list-group ">
                            <?php
                            $count = 0;
                            $index = 0;
                            $items = $req->selectAll();
                            foreach ($items as $requestItem):
                                //if (diffDays(date("Y/m/d"), $requestItem->update_date) > MAX_VIEW_DAYS) {continue;}
                                if (User::isInvalid($requestItem->user_id)) {
                                    continue;
                                }
                                ?>
                                <?php if ($requestItem->enabled != 1) {
                                continue;
                            } ?>
                                <?php
                                $count++;
                                if (ceil(++$index / MAX_PAGE_COUNT) != $page) {
                                    continue;
                                } ?>
                                <li class="list-group-item">
                                    <a href="request_details.php?id=<?= $requestItem->id ?>"><i
                                                class="fa fa-chevron-right"
                                                aria-hidden="true"></i> <?= toDateStringFromMySqlDatetime($requestItem->regist_date) ?> <?= $requestItem->title ?>
                                    </a>
                                    <?php if (isNew($requestItem->update_date)): ?>
                                        <span class="newIcon">NEW</span>
                                    <?php endif; ?>
                                </li>
                            <?php endforeach; ?>
                        </ul>
                        <div class="text-center margin-20 ">
                            <?php
                            $prev_page = $page - 1;
                            $max_page = ceil($count / MAX_PAGE_COUNT);
                            $next_page = min($max_page, $page + 1);
                            $view_start_page = max(1, $page - ceil(VIEW_PAGE_SELECT_COUNT / 2));
                            $view_last_page = min($view_start_page + VIEW_PAGE_SELECT_COUNT - 1, $max_page);
                            if ($view_last_page - $view_start_page < VIEW_PAGE_SELECT_COUNT) {
                                $view_start_page = max(1, $view_last_page - VIEW_PAGE_SELECT_COUNT + 1);
                            }
                            ?>
                            <?php if ($prev_page > 0): ?>
                                <a class="sortButton" href=""
                                   onclick="submitForm('fm-param', 'page', '<?= $prev_page ?>'); return false;">前へ</a>
                            <?php else: ?>
                                <a class="sortButton" href="" onclick="return false;">前へ</a>
                            <?php endif; ?>

                            <?php for ($i = $view_start_page; $i <= $view_last_page; $i++): ?>
                                <?php if ($page != $i): ?>
                                    <a class="sortButton" href=""
                                       onclick="submitForm('fm-param', 'page', '<?= $i ?>'); return false;"><?= $i ?></a>
                                <?php else: ?>
                                    <a class="sortButton active" href="" onclick="return false;"><?= $i ?></a>
                                <?php endif; ?>
                            <?php endfor; ?>

                            <?php if ($page < $max_page): ?>
                                <a class="sortButton" href=""
                                   onclick="submitForm('fm-param', 'page', '<?= $next_page ?>'); return false;">次へ</a>
                            <?php else: ?>
                                <a class="sortButton" href="" onclick="return false;">次へ</a>
                            <?php endif; ?>

                            <p class="small"><?= number_format(($page - 1) * MAX_PAGE_COUNT + 1) ?>
                                ～<?= number_format(min($count, $page * MAX_PAGE_COUNT)) ?>
                                件/<?= number_format($count) ?>
                                件</p>
                        </div>
                    </div>
                </div>
            <?php endif; ?>
            <div class="col-md-3 sideContents wow animate__animated animate__fadeInUp">
                <?php include('sidebar.php'); ?>
            </div>
        </div>
    </div>
    </div>
<?php include('footer.php'); ?>