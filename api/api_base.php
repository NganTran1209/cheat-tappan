<?php
include_once(__DIR__.'/../common/util.php');

class ApiBase {

    protected $params;

    public function execute() {

        $result = [];
        try {
            switch($_SERVER['REQUEST_METHOD']) {
                case 'GET':
                    $this->params = json_decode($_GET);
                    $result = $this->get();
                    break;
                case 'POST':
                    $this->params = json_decode($_POST);
                    $result = $this->post();
                    break;
                case 'DELETE':
                    $this->params = json_decode(file_get_contents('php://input'));
                    $result = $this->delete();
                    break;
                case 'PUT':
                    $param = file_get_contents('php://input');
                    $this->params = json_decode(file_get_contents('php://input'));
                    $result = $this->put();
                    break;
                default:
                    break;
            }
            http_response_code(200);
        } catch (Exception $ex){
            $result = ['message', $ex->getMessage()];
            http_response_code(404);
        }
        header("Content-Type: application/json; charset=UTF-8");
        header("X-Content-Type-Option: nosniff");
        echo json_encode($result);
        exit;
    }

    protected function get($param) {
        return $this->error();
    }

    protected function post($param) {
        return $this->error();
    }

    protected function delete($param) {
        return $this->error();
    }

    protected function put($param) {
        return $this->error();
    }
}

