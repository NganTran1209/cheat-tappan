<?php
include_once(__DIR__.'/../api_base.php');

class AdminApiBase extends ApiBase {

    public function execute() {
        if (!isAdminUser()) {
            throw new Exception();
        }
        return parent::execute();
    }
}
