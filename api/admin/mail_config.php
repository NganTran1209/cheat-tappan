<?php
include_once(__DIR__.'/admin_api_base.php');
include_once(__DIR__.'/../../common/mail_template.php');

class MailConfigApi extends AdminApiBase {

    protected function put() {
        switch ($this->params->target) {
            case 'from_email_name':
                $this->updateSystemConfigForFromEmailName();
                break;
            case 'from_email':
                $this->updateSystemConfigForFromEmail();
                break;
            case 'to_email':
                $this->updateSystemConfigForToEmail();
                break;
            default:
                if (endsWith($this->params->target, 'body')
                || endsWith($this->params->target, 'subject')) {
                    $this->updateMailTemplate();
                }
                break;
        }

    }

    protected function updateSystemConfigForFromEmailName() {
        SystemConfig::updateForFromEmailName($this->params->value);
    }

    protected function updateSystemConfigForFromEmail() {
        SystemConfig::updateFromEmail($this->params->value);
    }

    protected function updateSystemConfigForToEmail() {
        SystemConfig::updateToEmail($this->params->value);
    }

    protected function updateMailTemplate() {
        $keys = explode('-', $this->params->target);
        MailTemplate::update($keys[0], $keys[1], $this->params->value);
    }

}

$req = new MailConfigApi();
$req->execute();
