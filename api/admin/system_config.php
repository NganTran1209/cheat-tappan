<?php
include_once(__DIR__.'/admin_api_base.php');
include_once(__DIR__.'/../../entity/free_input_item.php');

class SystemConfigApi extends AdminApiBase {

    protected function put() {
        switch ($this->params->target) {
            case 'free_items1':
            case 'free_items2':
            case 'free_items3':
                $this->updateFreeInputItemForName();
                break;
            case 'free_item_enables1':
            case 'free_item_enables2':
            case 'free_item_enables3':
                $this->updateFreeInputItemForEnabled();
                break;
            case 'commission':
                $this->updateSystemConfigForCommission();
                break;
            case 'delivery_type':
                $this->updateSystemConfigForEnabledDeliveryType();
                break;
            case 'carriage_type':
                $this->updateSystemConfigForEnabledCarriageType();
                break;
            case 'postage':
                $this->updateSystemConfigForEnabledPostage();
                break;
            case 'pref':
                $this->updateSystemConfigForEnabledPref();
                break;
            case 'item_state':
                $this->updateSystemConfigForEnabledItemState();
                break;
            case 'item_name':
                $this->updateSystemConfigForItemName();
                break;
            case 'financial_institution_name':
                $this->updateSystemConfigForFinancialInstitutionName();
                break;
            case 'branch_name':
                $this->updateSystemConfigForBranchName();
                break;
            case 'deposit_type':
                $this->updateSystemConfigForDepositType();
                break;
            case 'account_number':
                $this->updateSystemConfigForAccountNumber();
                break;
            case 'account_holder':
                $this->updateSystemConfigForAccountHolder();
                break;
            case 'stripe_access_key':
                $this->updateSystemConfigForStripeAccessKey();
                break;
            case 'stripe_secret_key':
                $this->updateSystemConfigForStripeSecretKey();
                break;
            case 'site_name':
                $this->updateSystemConfigForSiteName();
                break;
            case 'use_stripe':
                $this->updateSystemConfigForUseStripe();
                break;
            case 'search_bar':
                $this->updateSystemConfigForSearchBar();
                break;
            case 'uneisha':
                $this->updateSystemConfigForUneisha();
                break;
            case 'shozaichi':
                $this->updateSystemConfigForShozaichi();
                break;
            case 'company_name':
                $this->updateSystemConfigForCompanyName();
                break;

        }

    }

    protected function updateFreeInputItemForName() {
        $id = str_replace('free_items', '', $this->params->target);
        FreeInputItem::updateNameFromId($id, $this->params->value);
    }

    protected function updateFreeInputItemForEnabled() {
        $id = str_replace('free_item_enables', '', $this->params->target);
        FreeInputItem::updateEnabledFromId($id, $this->params->value);
    }

    protected function updateSystemConfigForCommission() {
        SystemConfig::updateCommissionRate($this->params->value);
    }

    protected function updateSystemConfigForEnabledCategory() {
        SystemConfig::updateEnabledCategory($this->params->value);
    }

    protected function updateSystemConfigForEnabledDeliveryType() {
        SystemConfig::updateEnabledDeliveryType($this->params->value);
    }

    protected function updateSystemConfigForEnabledCarriageType() {
        SystemConfig::updateEnabledCarriageType($this->params->value);
    }

    protected function updateSystemConfigForEnabledPostage() {
        SystemConfig::updateEnabledPostage($this->params->value);
    }

    protected function updateSystemConfigForEnabledPref() {
        SystemConfig::updateEnabledPref($this->params->value);
    }

    protected function updateSystemConfigForEnabledItemState() {
        SystemConfig::updateEnabledItemState($this->params->value);
    }

    protected function updateSystemConfigForItemName() {
        SystemConfig::updateItemName($this->params->value);
    }

    protected function updateSystemConfigForFinancialInstitutionName() {
        SystemConfig::updateSystemConfigForFinancialInstitutionName($this->params->value);
    }

    protected function updateSystemConfigForBranchName() {
        SystemConfig::updateSystemConfigForBranchName($this->params->value);
    }

    protected function updateSystemConfigForDepositType() {
        SystemConfig::updateSystemConfigForDepositType($this->params->value);
    }

    protected function updateSystemConfigForAccountNumber() {
        SystemConfig::updateSystemConfigForAccountNumber($this->params->value);
    }

    protected function updateSystemConfigForAccountHolder() {
        SystemConfig::updateSystemConfigForAccountHolder($this->params->value);
    }

    protected function updateSystemConfigForStripeAccessKey() {
        SystemConfig::updateSystemConfigForStripeAccessKey($this->params->value);
    }

    protected function updateSystemConfigForStripeSecretKey() {
        SystemConfig::updateSystemConfigForStripeSecretKey($this->params->value);
    }

    protected function updateSystemConfigForSiteName() {
        SystemConfig::updateSystemConfigForSiteName($this->params->value);
    }

    protected function updateSystemConfigForUseStripe() {
        SystemConfig::updateSystemConfigForUseStripe($this->params->value);
    }

    protected function updateSystemConfigForSearchBar() {
        SystemConfig::updateSystemConfigForSearchBar($this->params->value);
    }

    protected function updateSystemConfigForUneisha() {
        SystemConfig::updateSystemConfigForUneisha($this->params->value);
    }

    protected function updateSystemConfigForShozaichi() {
        SystemConfig::updateSystemConfigForShozaichi($this->params->value);
    }

    protected function updateSystemConfigForCompanyName() {
        SystemConfig::updateSystemConfigForCompanyName($this->params->value);
    }

}

$req = new SystemConfigApi();
$req->execute();
