<?php
class Identification {
	public $user_id;
	public $photo;
	public $photo2;
	public $regist_date;
	public $update_date;

	/**
	 * 登録
	 */
	public static function regist() {
		$db = new DB();
		$param = array(
				':user_id' => getUserId(),
		);

		//$db->beginTransaction();
		$db->execute('registIdentification', $param);
		static::sendUploadIdentificationMail();
	}

	/**
	 * 登録(管理者から）
	 */
	public static function registFromAdmin($user_id) {
		$db = new DB();
		$param = array(
		    ':user_id' => $user_id,
		);

		$db->execute('registIdentification', $param);
		static::sendUploadIdentificationMail($user_id);
	}

	/**
	 * メール送信
	 */
	public static function sendUploadIdentificationMail($user_id = 0) {
	    $system_config = SystemConfig::select();
		$user = new User();
		if ($user_id <= 0) {
		    $user_id = getUserId();
		}
		$user->select($user_id);

		$mail = new Mail();
		$mail->to = $system_config->to_email;
		$mail->from = $system_config->from_email;
		$mail->fromName = $system_config->from_email_name;

		$template = MailTemplate::get('sendUploadIdentificationMail');
		$mail->subject = $template->subject;
		$mail->body = $template->replaceMessage(
				array(
						MailTemplate::REPLACE_USER_NAME => $user->name
				)
				);
		$mail->sendMail();
	}

	/**
	 * 詳細検索
	 * @param unknown $id
	 * @return boolean
	 */
	public function select($user_id) {
		$db = new DB();
		$param = array(
				':user_id' => $user_id
		);

		$list = $db->query('selectIdentification', $param);
		if (is_array($list) && count($list) > 0) {
			foreach ($list as $item) {
				$this->user_id = $item['user_id'];
				$this->photo= $item['photo'];
				$this->photo2= $item['photo2'];
				$this->regist_date= $item['regist_date'];
				$this->update_date= $item['update_date'];

				return true;
			}
		}
		return false;
	}

	public function getPhoto($id) {
		$db = new DB();
		$param = array(
				':user_id' => $id
		);
		$list = $db->query('selectIdentification', $param);
		foreach ($list as $item) {
			return $item['photo'];
		}

		return null;
	}

	public function getPhoto2($id) {
		$db = new DB();
		$param = array(
				':user_id' => $id
		);
		$list = $db->query('selectIdentification', $param);
		foreach ($list as $item) {
			return $item['photo2'];
		}

		return null;
	}

	static public function hasPhoto($id = 0) {
	    return file_exists(static::getPhoto1Path($id));
	}

	static public function hasPhoto2($id = 0) {
	    return file_exists(static::getPhoto2Path($id));
	}

	static public function getImagePath($id = 0) {
	    if ($id <= 0) {
	        $id = getUserId();
	    }
	    return IDENTIFICATION_IMAGE_PATH.'/'.$id.'/';
	}

	static public function regist_tmp($id = 0) {
	    $dir = static::getImagePath($id);
	    $photo1_path = static::getPhoto1Path($id);
	    $photo2_path = static::getPhoto2Path($id);
	    if (!file_exists($dir)) {
            mkdir($dir, 0755, true);
	    }
	    if (file_exists($photo1_path)) {
	        unlink($photo1_path);
	    }
	    if (file_exists($photo2_path)) {
	        unlink($photo2_path);
	    }
	    if ($_FILES['photo']['error'] == 0) {
	        move_uploaded_file($_FILES['photo']['tmp_name'], $photo1_path);
	        if ($_FILES['photo2']['error'] == 0) {
	            move_uploaded_file($_FILES['photo2']['tmp_name'], $photo2_path);
	        }
	    }
	}

	static public function getPhoto1Path($id = 0) {
	    $dir = static::getImagePath($id);
	    $photo1_path = $dir.'photo1';
	    return $photo1_path;
	}

	static public function getPhoto2Path($id = 0) {
	    $dir = static::getImagePath($id);
	    $photo2_path = $dir.'photo2';
	    return $photo2_path;
	}

}
