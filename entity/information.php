<?php

class Information {
	public $id;
	public $title;
	public $text;
	public $enabled;

	public $regist_date;
	public $update_date;

	/**
	 * 登録
	 */
	public function regist() {
		$db = new DB();

		if (empty($this->id)) {
			$param = array(
					':title' => $this->title,
					':text' => $this->text
			);
			$db->execute('insertInformation', $param);
		} else {
			$param = array(
					':title' => $this->title,
					':text' => $this->text,
					':id' => $this->id
			);
			$db->execute('updateInformation', $param);
		}
		//$db->commit();
	}

	/**
	 * 全件検索
	 * @return Item[]
	 */
	public function selectAll() {
		$db = new DB();

		$result = array();
		$list = $db->query('selectInformationAll');
		if (is_array($list) && count($list) > 0) {
			foreach ($list as $item) {
				$entity = new Information();
				$entity->id = $item['id'];
				$entity->text= $item['text'];
				$entity->title= $item['title'];
				$entity->enabled= $item['enabled'];

				$entity->regist_date= $item['regist_date'];
				$entity->update_date= $item['update_date'];

				$result[] = $entity;
			}
		}
		return $result;
	}

	/**
	 * 全件検索
	 * @return Item[]
	 */
	public function select() {
		$db = new DB();

		$param = array(
				':id' => $this->id
		);
		$list = $db->query('selectInformation', $param);
		if (is_array($list) && count($list) > 0) {
			foreach ($list as $item) {
				$this->id = $item['id'];
				$this->text= $item['text'];
				$this->title= $item['title'];
				$this->enabled= $item['enabled'];

				$this->regist_date= $item['regist_date'];
				$this->update_date= $item['update_date'];

				return true;
			}
		}
		return false;
	}

}