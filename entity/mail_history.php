<?php
include_once(__DIR__.'/../common/db.php');
include_once(__DIR__.'/../entity/mail_history_target.php');

class MailHistory {

    public $id;
    public $subject;
    public $body;
    public $regist_date;
    public $targets = [];

    /**
     * 登録
     * @param unknown $subject
     * @param unknown $body
     * @return string
     */
    public static function regist($subject, $body) {
        $db = new DB();
        $param = array(
            ':subject' => $subject,
            ':body' => $body,
        );

        $db->execute('insertMailHistory', $param);
        return $db->lastInsertId();
    }

    /**
     * 検索（全件）
     * @param unknown $id
     * @return MailHistoryTarget[]
     */
    public static function selectAll() {
        $db = new DB();
        $param = array(
            ':mail_history_id' => $mail_history_id
        );
        $list = $db->query('selectMailHistory', $param);
        $result = [];
        if (is_array($list) && count($list) > 0) {
            foreach ($list as $item) {
                $entity = new MailHistory();
                $entity->id = $item['id'];
                $entity->subject = $item['subject'];
                $entity->body = $item['body'];
                $entity->regist_date = $item['regist_date'];

                $result[] = $entity;
            }
        }
        return $result;
    }


    /**
     * 検索（表示件数分）
     * @param unknown $id
     * @return MailHistoryTarget[]
     */
    public static function selectView() {
        $db = new DB();
        $list = $db->query('selectMailHistory');
        $result = [];
        if (is_array($list) && count($list) > 0) {
            foreach ($list as $item) {
                $entity = new MailHistory();
                $entity->id = $item['id'];
                $entity->subject = $item['subject'];
                $entity->body = $item['body'];
                $entity->regist_date = $item['regist_date'];

                $result[] = $entity;
                if (count($result) >= MAIL_HISTORY_VIEW_COUNT) {
                    break;
                }
            }
        }
        return $result;
    }

    /**
     * 検索（ID指定）
     * @param unknown $id
     * @return MailHistory
     */
    public static function select($id) {
        $db = new DB();
        $param = array(
            ':id' => $id
        );
        $list = $db->query('selectMailHistoryById', $param);
        $entity = new MailHistory();
        if (is_array($list) && count($list) > 0) {
            foreach ($list as $item) {
                $entity->id = $item['id'];
                $entity->subject = $item['subject'];
                $entity->body = $item['body'];
                $entity->regist_date = $item['regist_date'];

                $entity->targets = MailHistoryTarget::selectByMailHistoryId($id);

                break;
            }
        }
        return $entity;
    }
}
