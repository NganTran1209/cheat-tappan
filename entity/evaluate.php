<?php
include_once(__DIR__.'/../common/db.php');

class Evaluate {
	public $user_id;
	public $good_count = 0;
	public $normal_count = 0;
	public $bad_count = 0;
	public $point = 0;
	public $count = 0;

	public $buyerEvaluate;
	public $sellerEvaluate;

	/**
	 * 検索
	 * @param unknown $user_id
	 * @return unknown
	 */
	public function selectSummary($user_id) {
		$map = $this->selectSummaryMap();
		if (isset($map[$user_id])) {
			$entity = $map[$user_id];
		}
		else {
			$entity = new Evaluate();
			$entity->buyerEvaluate = new Evaluate();
			$entity->sellerEvaluate= new Evaluate();
			$entity->sellerEvaluate->user_id = $user_id;
			$entity->buyerEvaluate->user_id = $user_id;
		}
		$this->user_id = $user_id;
		$this->good_count = $entity->good_count;
		$this->normal_count = $entity->normal_count;
		$this->bad_count = $entity->bad_count;
		$this->point = $entity->point;
		$this->count = $entity->count;
		$this->sellerEvaluate = $entity->sellerEvaluate;
		$this->buyerEvaluate = $entity->buyerEvaluate;
	}

	/**
	 * 検索
	 */
	public function selectSummaryMap() {
		$db = new DB();

		$result = array();
		$list = $db->query('selectOrderEvaluate');
		if (is_array($list) && count($list) > 0) {
			foreach ($list as $item) {
				$user_id = $item['user_id'];
				$owner_id = $item['owner_id'];
				if (!isset($result[$user_id])) {
					$result[$user_id]= array();
					$result[$user_id] = new Evaluate();
					$result[$user_id]->user_id = $user_id;
					$result[$user_id]->buyerEvaluate = new Evaluate();
					$result[$user_id]->sellerEvaluate = new Evaluate();
				}
				if (!isset($result[$owner_id])) {
					$result[$owner_id]= array();
					$result[$owner_id] = new Evaluate();
					$result[$owner_id]->user_id = $owner_id;
					$result[$owner_id]->buyerEvaluate = new Evaluate();
					$result[$owner_id]->sellerEvaluate = new Evaluate();
				}

				switch ($item['seller_evaluate']) {
					case 0:
						break;
					case 1:
						$result[$owner_id]->sellerEvaluate->good_count++;
						$result[$owner_id]->good_count++;
						break;
					case 2:
						$result[$owner_id]->sellerEvaluate->normal_count++;
						$result[$owner_id]->normal_count++;
						break;
					case 3:
						$result[$owner_id]->sellerEvaluate->bad_count++;
						$result[$owner_id]->bad_count++;
						break;
				}
				$result[$owner_id]->sellerEvaluate->point += $item['seller_point'];
				$result[$owner_id]->point += $item['seller_point'];
				$result[$owner_id]->sellerEvaluate->count++;
				$result[$owner_id]->count++;

				switch ($item['buyer_evaluate']) {
					case 0:
						break;
					case 1:
						$result[$user_id]->buyerEvaluate->good_count++;
						$result[$user_id]->good_count++;
						break;
					case 2:
						$result[$user_id]->buyerEvaluate->normal_count++;
						$result[$user_id]->normal_count++;
						break;
					case 3:
						$result[$user_id]->buyerEvaluate->bad_count++;
						$result[$user_id]->bad_count++;
						break;
				}
				$result[$user_id]->buyerEvaluate->point += $item['buyer_point'];
				$result[$user_id]->point += $item['buyer_point'];
				$result[$user_id]->buyerEvaluate->count++;
				$result[$user_id]->count++;
			}
		}
		return $result;
	}

	/**
	 * 登録
	 */
	public function regist() {
		$db = new DB();
		$param = array(
				':order_id' => $this->order_id,
				':seller_evaluate' => $this->seller_evaluate,
				':buyer_evaluate' => $this->buyer_evaluate,
				':seller_comment' => $this->seller_comment,
				':buyer_comment' => $this->buyer_comment
		);

		$db->execute('insertOrder', $param);
	}

	/**
	 * 販売者評価登録
	 */
	public function registBuyer() {
		$db = new DB();
		$param = array(
				':order_id' => $this->order_id,
				':buyer_evaluate' => $this->buyer_evaluate,
				':buyer_comment' => $this->buyer_comment
		);

		$db->execute('registEvaluateByBuyer', $param);
	}

	/**
	 * 購入者評価登録
	 */
	public function registSeller() {
		$db = new DB();
		$param = array(
				':order_id' => $this->order_id,
				':seller_evaluate' => $this->seller_evaluate,
				':seller_comment' => $this->seller_comment,
		);

		$db->execute('registEvaluateBySeller', $param);
	}

	/**
	 * 文字列出力
	 */
	public function toString() {
		return '評価：'.$this->point.'（'. Evaluate::toEvaluateText(1).''.$this->good_count.' '. Evaluate::toEvaluateText(2).''.$this->normal_count.' '. Evaluate::toEvaluateText(3).''.$this->bad_count.'）';
	}

	/**
	 * 文字列出力（イメージ埋め込み）
	 */
	public function toStringOnImage() {
		return '評価：'.$this->point.'（'. Evaluate::toEvaluateImageTag(1) . ''.$this->good_count.' '. Evaluate::toEvaluateImageTag(2) . ''.$this->normal_count.' '. Evaluate::toEvaluateImageTag(3) . ''.$this->bad_count.'）';
	}

	/**
	 * 評価（良い）埋め込みテキスト
	 * @return string
	 */
	public function toGoodStringOnImage() {
		return Evaluate::toEvaluateImageTag(1).''.$this->good_count;
	}

	/**
	 * 評価（普通）埋め込みテキスト
	 * @return string
	 */
	public function toNormalStringOnImage() {
		return Evaluate::toEvaluateImageTag(2).''.$this->normal_count;
	}

	/**
	 * 評価（悪い）埋め込みテキスト
	 * @return string
	 */
	public function toBadStringOnImage() {
		return Evaluate::toEvaluateImageTag(3).''.$this->bad_count;
	}

	/**
	 * 状態名変換
	 * @param unknown $status
	 * @return string
	 */
	public static function toEvaluateText($value) {
		switch ($value) {
			case 1:
				return '良い';
			case 2:
				return '普通';
			case 3:
				return '悪い';
			default:
				break;
		}

		return '-';
	}

	/**
	 * 状態イメージパス変換
	 * @param unknown $status
	 * @return string
	 */
	public static function toEvaluateImage($value) {
		switch ($value) {
			case 1:
				return getContextRoot() . '/'. FACE_GOOD_IMAGE_PATH;
			case 2:
				return getContextRoot() . '/'. FACE_NORMAL_IMAGE_PATH;
			case 3:
				return getContextRoot() . '/'. FACE_BAD_IMAGE_PATH;
			default:
				return getContextRoot() . '/'. FACE_NORMAL_IMAGE_PATH;
		}

		//return '-';
	}

	/**
	 * 状態イメージタグ変換
	 * @param unknown $status
	 * @return string
	 */
	public static function toEvaluateImageTag($value) {
		return '<img src="' . Evaluate::toEvaluateImage($value) .'" width="16" height="16" alt="' . Evaluate::toEvaluateText($value) . '"/>';
	}
}