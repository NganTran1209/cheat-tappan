<?php
include_once(__DIR__.'/../common/db.php');

class OrderEvaluate {

	public $id;
	public $order_id;
	public $seller_evaluate;
	public $buyer_evaluate;
	public $seller_comment;
	public $buyer_comment;

	public $regist_date;
	public $update_date;

	/**
	 * 登録
	 */
	public function regist() {
		$db = new DB();
		$param = array(
				':item_id' => $this->item_id,
				':user_id' => $this->user_id,
				':state' => $this->state
		);

		//$db->beginTransaction();
		$db->execute('insertOrderEvaluate', $param);
		//$db->commit();
	}


}
