<?php
include_once(__DIR__.'/../common/db.php');

class BlockAccess {
	public $id;
	public $user_id;
	public $block_user_id;
	public $regist_date;
	public $update_date;

	/**
	 * 検索
	 */
	public function select() {
		$db = new DB();
		$param = array(
				':id' => $this->id
		);

		$result = array();
		$list = $db->query('selectBlockUser', $param);
		if (is_array($list) && count($list) > 0) {
			foreach ($list as $item) {
				$this->id = $item['id'];
				$this->user_id= $item['user_id'];
				$this->block_user_id= $item['block_user_id'];
				$this->regist_date= $item['regist_date'];
				$this->update_date= $item['update_date'];
			}
		}
	}

	/**
	 * 検索（ブロックしたユーザー一覧）
	 */
	public function selectFromUser() {
		$db = new DB();
		$param = array(
				':user_id' => $this->user_id
		);

		$result = array();
		$list = $db->query('selectBlockUserFromUser', $param);
		if (is_array($list) && count($list) > 0) {
			foreach ($list as $item) {
				$entity = new BlockAccess();
				$entity->id = $item['id'];
				$entity->user_id= $item['user_id'];
				$entity->block_user_id= $item['block_user_id'];
				$entity->regist_date= $item['regist_date'];
				$entity->update_date= $item['update_date'];
				$result[] = $entity;
			}
		}
		return $result;
	}

	/**
	 * 検索（ブロックされたユーザー一覧）
	 */
	public function selectFromBlockUser() {
		$db = new DB();
		$param = array(
				':block_user_id' => $this->block_user_id
		);

		$result = array();
		$list = $db->query('selectBlockUserFromBlockUser', $param);
		if (is_array($list) && count($list) > 0) {
			foreach ($list as $item) {
				$entity = new BlockAccess();
				$entity->id = $item['id'];
				$entity->user_id= $item['user_id'];
				$entity->block_user_id= $item['block_user_id'];
				$entity->regist_date= $item['regist_date'];
				$entity->update_date= $item['update_date'];
				$result[] = $entity;
			}
		}
		return $result;
	}

	/**
	 * 登録
	 */
	public function regist() {
		$db = new DB();
		$param = array(
				':user_id' => $this->user_id,
				':block_user_id' => $this->block_user_id
		);
		$db->execute('registBlockUser', $param);
	}

	/**
	 * 削除
	 */
	public function remove() {
		$db = new DB();
		$param = array(
				':id' => $this->id
		);
		$db->execute('deleteBlockUser', $param);
	}

	/**
	 * ブロックされているチェック（自分がブロックされた場合）
	 * @param unknown $user_id ブロック登録したユーザーID
	 * @return boolean
	 */
	static public function isBlockOwner($user_id) {
		$block = new BlockAccess();
		$block->user_id = $user_id;
		$list = $block->selectFromUser();

		foreach ($list as $item) {
			if ($item->block_user_id == getUserId()) {
				return true;
			}
			$user = new User();
			$user->select($item->block_user_id);
			if ($user->ipaddr == getRemoteAddress()) {
				return true;
			}
		}

		return false;
	}

	/**
	 * ブロックしているチェック（自分がブロックした場合）
	 * @param unknown $user_id ブロック登録したユーザーID
	 * @return boolean
	 */
	static public function isBlockOther($user_id) {
		$block = new BlockAccess();
		$block->user_id = getUserId();
		$list = $block->selectFromUser();

		foreach ($list as $item) {
			if ($item->block_user_id == $user_id) {
				return true;
			}
		}

		return false;
	}

	/**
	 * ブロックチェック
	 * @param unknown $user_id ブロックチェック対象ユーザーID
	 * @return boolean
	 */
	static public function isBlock($user_id) {
		return BlockAccess::isBlockOwner($user_id) || BlockAccess::isBlockOther($user_id);
	}

}
