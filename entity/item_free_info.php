<?php
include_once(__DIR__.'/../common/db.php');
include_once(__DIR__.'/free_input_item.php');
include_once(__DIR__.'/free_input_item_value.php');

class ItemFreeInfo {

    public $item_id;
    public $free_input_item_id;
    public $free_input_item_value_id;

    public function regist() {
        $db = new DB();
        $param = array(
            ':item_id' => $this->item_id,
            ':free_input_item_id' => $this->free_input_item_id,
            ':free_input_item_value_id' => $this->free_input_item_value_id,
        );

        $db->execute('registItemFreeInput', $param);
    }

    static public function selectFromItemId($item_id) {
        $db = new DB();

        $ret = [];
        foreach (FreeInputItem::selectAll() as $value1) {
            $param = [
                ':item_id' => $item_id,
                ':free_input_item_id' => $value1->id
            ];

            $entity = new ItemFreeInfo();
            $list = $db->query('selectItemFreeInputFromId', $param);
            if (is_array($list) && count($list) > 0) {
                foreach ($list as $item) {
                    $entity->item_id = $item['item_id'];
                    $entity->free_input_item_id = $item['free_input_item_id'];
                    $entity->free_input_item_value_id = $item['free_input_item_value_id'];
                }
            }
            $ret[$value1->id] = $entity;
        }
        return $ret;
    }
}