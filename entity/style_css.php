<?php
include_once(__DIR__.'/../common/util.php');

define('STYLE_CSS_FILEPATH', __DIR__.'/../override/common/style.css');
define('STYLE_CSS_FILEURL', '/override/common/style.css');

class StyleCss {

    public $css;

    /**
     * 読み込み
     */
    public function load() {
        $this->css = file_get_contents(STYLE_CSS_FILEPATH);
    }

    /**
     * 保存
     */
    public function save(){
        file_put_contents(STYLE_CSS_FILEPATH, $this->css);
    }

    /**
     * CSSのURL取得
     * @return string
     */
    static public function getFileName() {
        return getContextRoot().STYLE_CSS_FILEURL.'?'.filemtime(STYLE_CSS_FILEPATH);
    }

}