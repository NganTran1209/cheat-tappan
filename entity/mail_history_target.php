<?php
include_once(__DIR__.'/../common/db.php');

class MailHistoryTarget {
    public $mail_history_id;
    public $user_id;

    /**
     * 登録
     * @param unknown $mail_history_id
     * @param unknown $user_id
     */
    public static function regist($mail_history_id, $user_id) {
        $db = new DB();
        $param = array(
            ':mail_history_id' => $mail_history_id,
            ':user_id' => $user_id,
        );

        $db->execute('insertMailHistoryTarget', $param);
    }

    /**
     * 検索（メール送信履歴ID）
     * @param unknown $mail_history_id
     * @return MailHistoryTarget[]
     */
    static public function selectByMailHistoryId($mail_history_id) {
        $db = new DB();
        $param = array(
            ':mail_history_id' => $mail_history_id
        );
        $list = $db->query('selectMailHistoryTargetByMailHistoryId', $param);
        $result = [];
        if (is_array($list) && count($list) > 0) {
            foreach ($list as $item) {
                $entity = new MailHistoryTarget();
                $entity->mail_history_id = $item['mail_history_id'];
                $entity->user_id = $item['user_id'];

                $result[] = $entity;
            }
        }
        return $result;
    }


}
