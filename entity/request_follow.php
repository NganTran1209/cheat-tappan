<?php
include_once(__DIR__.'/../common/db.php');

class RequestFollow {

	public $id;
	public $request_id;
	public $user_id;
	public $item_id;
	public $title;

	public $user_name;

	public $regist_date;
	public $update_date;

	/**
	 * 登録
	 */
	public function regist() {
		$db = new DB();
		$param = array(
				':user_id' => $this->user_id,
				':request_id' => $this->reques_id,
				':item_id' => $this->item_id
		);

		//$db->beginTransaction();
		$db->execute('insertReauestFollow', $param);
		//$db->commit();

		$this->sendRequestFollowMail();
	}

	/**
	 * 全件検索
	 * @return Item[]
	 */
	public function selectRequest($request_id) {
		$db = new DB();

		$result = array();
		$param = array(
				':request_id' => $request_id
		);
		$list = $db->query('selectRequestFollowRequest', $param);
		if (is_array($list) && count($list) > 0) {
			foreach ($list as $item) {
				$entity = new RequestFollow();
				$entity->id = $item['id'];
				$entity->user_id= $item['user_id'];
				$entity->user_name= $item['user_name'];
				$entity->title= $item['title'];
				$entity->request_id= $item['request_id'];
				$entity->item_id= $item['item_id'];

				$entity->regist_date= $item['regist_date'];
				$entity->update_date= $item['update_date'];

				$result[] = $entity;
			}
		}
		return $result;
	}

	/**
	 * 詳細検索
	 * @param unknown $id
	 * @return boolean
	 */
	public function select($id) {
		$db = new DB();
		$param = array(
				':id' => $id
		);

		$list = $db->query('selectReauestFollow', $param);
		if (is_array($list) && count($list) > 0) {
			foreach ($list as $item) {
				$this->id = $item['id'];
				$this->user_id= $item['user_id'];
				$this->title= $item['title'];
				$this->request_id= $item['request_id'];
				$this->item_id= $item['item_id'];

				$this->regist_date= $item['regist_date'];
				$this->update_date= $item['update_date'];

				return true;
			}
		}
		return false;
	}

	/**
	 * リクエストありチェック
	 */
	static public function hasRequest($user_id, $item_id) {
		$db = new DB();
		$param = array(
				':user_id' => $user_id,
				':item_id' => $item_id,
		);

		$list = $db->query('selectReauestFollowFromOrder', $param);
		if (is_array($list) && count($list) > 0) {
			return true;
		}
		return false;
	}

	/**
	 * メール送信
	 */
	public function sendRequestFollowMail() {

	    $system_config = SystemConfig::select();
	    $item = new Item();
		$item->select($this->item_id);
		$owner = new User();
		$owner->select($item->owner_id);
		$req = new Request();
		$req->select($this->request_id);

		$user = new User();
		$user->select($req->user_id);

		$mail = new Mail();
		$mail->to = $user->email;
		$mail->from = $system_config->from_email;
		$mail->fromName = $system_config->from_email_name;

		$template = MailTemplate::get('sendRequestFollow');
		$mail->subject = $template->replaceTitle(
				array(
						MailTemplate::REPLACE_TITLE=> $item->title,
				)
				);
		$mail->body = $template->replaceMessage(
				array(
						MailTemplate::REPLACE_USER_NAME => $user->name,
						MailTemplate::REPLACE_TITLE=> $item->title,
						MailTemplate::REPLACE_OWNER_NAME=> $owner->name,
						MailTemplate::REPLACE_OWNER_HASH_ID=> $owner->hash_id,
						MailTemplate::REPLACE_CATEGORY_NAME=> $item->category_name,
				)
				);
		$mail->sendMail();
	}

}
