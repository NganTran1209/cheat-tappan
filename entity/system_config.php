<?php
include_once(__DIR__.'/../common/db.php');
include_once(__DIR__.'/../common/mail_template.php');

define('INITIAL_CATEGORY_NAME', 'カテゴリー１');
define('INITIAL_ITEM_NAME', '商品');
define('INITIAL_COMMISSION_RATE', 10.0);
define('INITIAL_FROM_EMAIL', '');
define('INITIAL_FROM_EMAIL_NAME', '');
define('INITIAL_TO_EMAIL', '');
define('FREE_INPUT_ITEM_COUNT', 3);
define('INITIAL_FREE_INPUT_ITEM_NAME_PREFIX', '自由項目');

define('INITIAL_SITE_NAME', 'てっぱんプレミアムストア');
define('INITIAL_UNEISHA', '');
define('INITIAL_SHOZAICHI', '');
define('INITIAL_COMPANY_NAME', '');
define('INITIAL_ENABLED_SEARCH_BAR', 0);
define('INITIAL_FINANCIAL_INSTITUTION_NAME', '');
define('INITIAL_BRANCH_NAME', '');
define('INITIAL_DEPOSIT_TYPE', 0);
define('INITIAL_ACCOUNT_NUMBER', '');
define('INITIAL_ACCOUNT_HOLDER', '');
define('INITIAL_USE_STRIPE', 0);
define('INITIAL_STRIPE_ACCESS_KEY', '');
define('INITIAL_STRIPE_SECRET_KEY', '');

define('DEFAULT_TOP_IMAGE_NAME', 'top_img.jpg');
define('DEFAULT_LOGO_NAME', 'logo.jpg');

class SystemConfig {

    private static $system_config = null;

    public $commission_rate;
    public $from_email;
    public $from_email_name;
    public $to_email;
    public $site_name;
    public $financial_institution_name;
    public $branch_name;
    public $deposit_type;
    public $account_number;
    public $account_holder;
    public $use_stripe;
    public $stripe_access_key;
    public $stripe_secret_key;
    public $enabled_category;
    public $enabled_delivery_type;
    public $enabled_carriage_type;
    public $enabled_postage;
    public $enabled_pref;
    public $enabled_item_state;
    public $enabled_search_bar;
    public $item_name;
    public $uneisha;
    public $shozaichi;
    public $company_name;

    static public function getItemName() {
        return static::select()->item_name;
    }

    static public function update() {
        $db = new DB();
        $param = array(
            ':commission_rate' => 10,
        );
        $db->execute('updateSystem', $param);
    }

    static public function updateCommissionRate($commission_rate) {
        $db = new DB();
        $param = array(
            ':commission_rate' => $commission_rate,
        );
        $db->execute('updateSystemConfigForCommission', $param);
    }

    static public function select() {

        if (empty(static::$system_config)) {
            $db = new DB();
            $list = $db->query('selectSystemConfig');
            if (is_array($list) && count($list) > 0) {
                foreach ($list as $item) {
                    static::$system_config = new SystemConfig();
                    static::$system_config->commission_rate = $item['commission_rate'];
                    static::$system_config->from_email = $item['from_email'];
                    static::$system_config->from_email_name = $item['from_email_name'];
                    static::$system_config->to_email = $item['to_email'];
                    static::$system_config->site_name = $item['site_name'];
                    static::$system_config->financial_institution_name = $item['financial_institution_name'];
                    static::$system_config->branch_name = $item['branch_name'];
                    static::$system_config->deposit_type = $item['deposit_type'];
                    static::$system_config->account_number = $item['account_number'];
                    static::$system_config->account_holder = $item['account_holder'];
                    static::$system_config->use_stripe = $item['use_stripe'];
                    static::$system_config->stripe_access_key = $item['stripe_access_key'];
                    static::$system_config->stripe_secret_key = $item['stripe_secret_key'];
                    static::$system_config->enabled_category = $item['enabled_category'];
                    static::$system_config->enabled_delivery_type = $item['enabled_delivery_type'];
                    static::$system_config->enabled_carriage_type = $item['enabled_carriage_type'];
                    static::$system_config->enabled_postage = $item['enabled_postage'];
                    static::$system_config->enabled_pref = $item['enabled_pref'];
                    static::$system_config->enabled_item_state = $item['enabled_item_state'];
                    static::$system_config->item_name = $item['item_name'];
                    static::$system_config->enabled_search_bar = $item['enabled_search_bar'];
                    static::$system_config->uneisha = $item['uneisha'];
                    static::$system_config->shozaichi = $item['shozaichi'];
                    static::$system_config->company_name = $item['company_name'];
                }
            }
        }

        return static::$system_config;
    }

    static public function updateEnabledCategory($enabled_category) {
        $db = new DB();
        $param = array(
            ':enabled_category' => $enabled_category,
        );
        $db->execute('updateSystemConfigForEnabledCategory', $param);
    }

    static public function updateEnabledDeliveryType($enabled_delivery_type) {
        $db = new DB();
        $param = array(
            ':enabled_delivery_type' => $enabled_delivery_type,
        );
        $db->execute('updateSystemConfigForEnabledDeliveryType', $param);
    }

    static public function updateEnabledCarriageType($enabled_carriage_type) {
        $db = new DB();
        $param = array(
            ':enabled_carriage_type' => $enabled_carriage_type,
        );
        $db->execute('updateSystemConfigForEnabledCarriageType', $param);
    }

    static public function updateEnabledPostage($enabled_postage) {
        $db = new DB();
        $param = array(
            ':enabled_postage' => $enabled_postage,
        );
        $db->execute('updateSystemConfigForEnabledPostage', $param);
    }

    static public function updateEnabledPref($enabled_pref) {
        $db = new DB();
        $param = array(
            ':enabled_pref' => $enabled_pref,
        );
        $db->execute('updateSystemConfigForEnabledPref', $param);
    }

    static public function updateEnabledItemState($enabled_item_state) {
        $db = new DB();
        $param = array(
            ':enabled_item_state' => $enabled_item_state,
        );
        $db->execute('updateSystemConfigForEnabledItemState', $param);
    }

    static public function updateForFromEmailName($from_email_name) {
        $db = new DB();
        $param = array(
            ':from_email_name' => $from_email_name,
        );
        $db->execute('updateSystemConfigForFromEmailName', $param);
    }

    static public function updateFromEmail($from_email) {
        $db = new DB();
        $param = array(
            ':from_email' => $from_email,
        );
        $db->execute('updateSystemConfigForFromEmail', $param);
    }

    static public function updateToEmail($to_email) {
        $db = new DB();
        $param = array(
            ':to_email' => $to_email,
        );
        $db->execute('updateSystemConfigForToEmail', $param);
    }

    static public function updateItemName($item_name) {
        $db = new DB();
        $param = array(
            ':item_name' => $item_name,
        );
        $db->execute('updateSystemConfigForItemName', $param);
    }

    static public function updateSystemConfigForFinancialInstitutionName($financial_institution_name) {
        $db = new DB();
        $param = array(
            ':financial_institution_name' => $financial_institution_name,
        );
        $db->execute('updateSystemConfigForFinancialInstitutionName', $param);
    }

    static public function updateSystemConfigForBranchName($branch_name) {
        $db = new DB();
        $param = array(
            ':branch_name' => $branch_name,
        );
        $db->execute('updateSystemConfigForBranchName', $param);
    }

    static public function updateSystemConfigForDepositType($deposit_type) {
        $db = new DB();
        $param = array(
            ':deposit_type' => $deposit_type,
        );
        $db->execute('updateSystemConfigForDepositType', $param);
    }

    static public function updateSystemConfigForAccountNumber($account_number) {
        $db = new DB();
        $param = array(
            ':account_number' => $account_number,
        );
        $db->execute('updateSystemConfigForAccountNumber', $param);
    }

    static public function updateSystemConfigForAccountHolder($account_holder) {
        $db = new DB();
        $param = array(
            ':account_holder' => $account_holder,
        );
        $db->execute('updateSystemConfigForAccountHolder', $param);
    }

    static public function updateSystemConfigForStripeAccessKey($stripe_access_key) {
        $db = new DB();
        $param = array(
            ':stripe_access_key' => $stripe_access_key,
        );
        $db->execute('updateSystemConfigForStripeAccessKey', $param);
    }

    static public function updateSystemConfigForStripeSecretKey($stripe_secret_key) {
        $db = new DB();
        $param = array(
            ':stripe_secret_key' => $stripe_secret_key,
        );
        $db->execute('updateSystemConfigForStripeSecretKey', $param);
    }

    static public function updateSystemConfigForSiteName($site_name) {
        $db = new DB();
        $param = array(
            ':site_name' => $site_name,
        );
        $db->execute('updateSystemConfigForSiteName', $param);
    }

    static public function updateSystemConfigForUseStripe($use_stripe) {
        $db = new DB();
        $param = array(
            ':use_stripe' => $use_stripe,
        );
        $db->execute('updateSystemConfigForUseStripe', $param);
    }

    static public function updateSystemConfigForSearchBar($search_bar) {
        $db = new DB();
        $param = array(
            ':enabled_search_bar' => $search_bar,
        );
        $db->execute('updateSystemConfigForSearchBar', $param);
    }

    static public function updateSystemConfigForUneisha($uneisha) {
        $db = new DB();
        $param = array(
            ':uneisha' => $uneisha,
        );
        $db->execute('updateSystemConfigForUneisha', $param);
    }

    static public function updateSystemConfigForShozaichi($shozaichi) {
        $db = new DB();
        $param = array(
            ':shozaichi' => $shozaichi,
        );
        $db->execute('updateSystemConfigForShozaichi', $param);
    }

    static public function updateSystemConfigForCompanyName($company_name) {
        $db = new DB();
        $param = array(
            ':company_name' => $company_name,
        );
        $db->execute('updateSystemConfigForCompanyName', $param);
    }

    static public function clearDb() {
        $db = new DB();
        $db->execute('deleteAllForFremaBlockAccess');
        $db->execute('deleteAllForFremaFavorites');
        $db->execute('deleteAllForFremaGood');
        $db->execute('deleteAllForFremaIdentification');
        $db->execute('deleteAllForFremaInformation');
        $db->execute('deleteAllForFremaInvoice');
        $db->execute('deleteAllForFremaInvoiceDetails');
        $db->execute('deleteAllForFremaItem');
        $db->execute('deleteAllForFremaItemFreeInput');
        $db->execute('deleteAllForFremaItemPhoto');
        $db->execute('deleteAllForFremaItemTmp');
        $db->execute('deleteAllForFremaLoginSession');
        $db->execute('deleteAllForFremaNegotiation');
        $db->execute('deleteAllForFremaOrder');
        $db->execute('deleteAllForFremaOrderNumber');
        $db->execute('deleteAllForFremaRequest');
        $db->execute('deleteAllForFremaRequestFollow');
        $db->execute('deleteAllForFremaUser');
        $db->execute('deleteAllForFremaMailHistory');
        $db->execute('deleteAllForFremaMailHistoryTarget');
        $db->execute('deleteAllForFremaFreeInputItem');
        $db->execute('deleteAllForFremaFreeInputItemValue');
        $db->execute('deleteAllForFremaCategory');
        $db->execute('deleteAllForCardInfo');
        $db->execute('deleteAllForCashFlow');
        $db->execute('deleteAllForPayoutHistory');

        static::initId();
        static::registIntialUser();
        static::registInitCategory();
        static::registInitSystemConfig();
        static::registFreeInputItem();
        static::repairCopyXml();

        static::init_logo_image_file_name();
        static::init_top_large_image_file_name();

        static::$system_config = null;
    }

    static public function initId() {
        $db = new DB();
        $db->execute('resetIdForFremaBlockAccess');
        $db->execute('resetIdForFremaInformation');
        $db->execute('resetIdForFremaInvoice');
        $db->execute('resetIdForFremaItem');
        $db->execute('resetIdForFremaItemTmp');
        $db->execute('resetIdForFremaNegotiation');
        $db->execute('resetIdForFremaOrder');
        $db->execute('resetIdForFremaRequest');
        $db->execute('resetIdForFremaRequestFollow');
        $db->execute('resetIdForFremaUser');
        $db->execute('resetIdForFremaCategory');
        $db->execute('resetIdForFremaMailHistory');
        $db->execute('resetIdForFremaInputItem');
        $db->execute('resetIdForFremaInputItemValue');
        $db->execute('resetIdForFremaCashFlow');
        $db->execute('resetIdForFremaPayoutHistory');
    }

    static public function registIntialUser() {
        $db = new DB();
        $param = array(
            ':hash_id' => hash('crc32', INITIAL_ADMIN_EMAIL. date('YmdHis')),
            ':password' => password_hash( INITIAL_ADMIN_PASSWORD, PASSWORD_DEFAULT),
            ':email' => INITIAL_ADMIN_EMAIL,
            ':ipaddr' => getRemoteAddress(),
            ':name' => INITIAL_ADMIN_NAME,
            ':name_kana' => INITIAL_ADMIN_NAME_KANA,
        );

        $db->execute('registAdminUser', $param);
    }

    static public function repairCopyXml() {
        MailTemplate::repairInitXml();
    }

    static public function registInitCategory() {

        $db = new DB();
        $param = array(
            ':name' => INITIAL_CATEGORY_NAME,
        );

        $db->execute('insertCategory', $param);
        $id = $db->lastInsertId();

        $param = array(
            ':id' => $id,
            ':name' => INITIAL_CATEGORY_NAME,
            ':enabled' => true,
        );
        $id = $db->execute('registCategory', $param);

    }

    static public function registInitSystemConfig() {

        $db = new DB();
        $param = array(
            ':item_name' => INITIAL_ITEM_NAME,
            ':commission_rate' => INITIAL_COMMISSION_RATE,
            ':from_email' => INITIAL_FROM_EMAIL,
            ':from_email_name' => INITIAL_FROM_EMAIL_NAME,
            ':to_email' => INITIAL_TO_EMAIL,
            ':enabled_category' => true,
            ':enabled_delivery_type' => true,
            ':enabled_carriage_type' => true,
            ':enabled_postage' => true,
            ':enabled_pref' => true,
            ':enabled_item_state' => true,
            ':site_name' => INITIAL_SITE_NAME,
            ':uneisha' => INITIAL_UNEISHA,
            ':shozaichi' => INITIAL_SHOZAICHI,
            ':company_name' => INITIAL_COMPANY_NAME,
            ':enabled_search_bar' => false,
            ':financial_institution_name' => INITIAL_FINANCIAL_INSTITUTION_NAME,
            ':branch_name' => INITIAL_BRANCH_NAME,
            ':deposit_type' => INITIAL_DEPOSIT_TYPE,
            ':account_number' => INITIAL_ACCOUNT_NUMBER,
            ':account_holder' => INITIAL_ACCOUNT_HOLDER,
            ':use_stripe' => INITIAL_USE_STRIPE,
            ':stripe_access_key' => INITIAL_STRIPE_ACCESS_KEY,
            ':stripe_secret_key' => INITIAL_STRIPE_SECRET_KEY,
        );
        $db->execute('updateSystemConfig', $param);

    }

    static public function registFreeInputItem() {
        for ($i = 0; $i < FREE_INPUT_ITEM_COUNT; $i++) {
            $id = $i + 1;
            FreeInputItem::regist($id, INITIAL_FREE_INPUT_ITEM_NAME_PREFIX.$id, 1);
        }
    }

    static private function get_log_image_file_name() {
        $dirpath = __DIR__.'/..'.TOP_IMAGE_DIR_PATH;
        foreach (glob($dirpath.'/*logo*') as $file) {
            if (endsWith($file, DEFAULT_LOGO_NAME)) {
                continue;
            }
            return $file;
        }
        return DEFAULT_LOGO_NAME;
    }

    static private function get_top_large_image_file_name() {
        $dirpath = __DIR__.'/..'.TOP_IMAGE_DIR_PATH;
        foreach (glob($dirpath.'/*top_img*') as $file) {
            if (endsWith($file, DEFAULT_TOP_IMAGE_NAME)) {
                continue;
            }
            return $file;
        }
        return DEFAULT_TOP_IMAGE_NAME;
    }

    static private function init_logo_image_file_name() {
        $dirpath = __DIR__.'/..'.TOP_IMAGE_DIR_PATH;
        foreach (glob($dirpath.'/*logo*') as $file) {
            if (endsWith($file, DEFAULT_LOGO_NAME)) {
                continue;
            }
            unlink($file);
        }
    }

    static private function init_top_large_image_file_name() {
        $dirpath = __DIR__.'/..'.TOP_IMAGE_DIR_PATH;
        foreach (glob($dirpath.'/*top_img*') as $file) {
            if (endsWith($file, DEFAULT_TOP_IMAGE_NAME)) {
                continue;
            }
            unlink($file);
        }
    }

    static private function create_log_image_file_name() {
        return 'logo'.date("Ymd-His").'.jpg';
    }

    static private function create_top_large_image_file_name() {
        return 'top_img'.date("Ymd-His").'.jpg';
    }

    static public function get_logo_image_url() {
        return getContextRoot().TOP_IMAGE_DIR_PATH.'/'.basename(static::get_log_image_file_name());
    }

    static public function get_top_large_image_url() {
        return getContextRoot().TOP_IMAGE_DIR_PATH.'/'.basename(static::get_top_large_image_file_name());
    }

    static public function regist_logo_image() {
        $dirpath = __DIR__.'/..'.TOP_IMAGE_DIR_PATH;
        if (!file_exists($dirpath)) {
            mkdir($dirpath, 0777, true);
        }

        $oldfile = static::get_log_image_file_name();

        if (!empty($_FILES['logoImg']) && $_FILES['logoImg']['error'] == 0) {
            $filepath = $dirpath.'/'.static::create_log_image_file_name();
            $image = rotateImage(file_get_contents($_FILES['logoImg']['tmp_name']));
            $handler = fopen($filepath, 'w');
            $result = fwrite($handler, $image);
            fclose($handler);
            if (!endsWith($oldfile, DEFAULT_LOGO_NAME)) {
                unlink($oldfile);
            }
        }
    }

    static public function regist_top_large_image() {
        $dirpath = __DIR__.'/..'.TOP_IMAGE_DIR_PATH;
        if (!file_exists($dirpath)) {
            mkdir($dirpath, 0777, true);
        }

        $oldfile = static::get_top_large_image_file_name();

        if (!empty($_FILES['topLargeImg']) && $_FILES['topLargeImg']['error'] == 0) {
            $filepath = $dirpath.'/'.static::create_top_large_image_file_name();
            $image = rotateImage(file_get_contents($_FILES['topLargeImg']['tmp_name']));
            $handler = fopen($filepath, 'w');
            $result = fwrite($handler, $image);
            fclose($handler);
            if (!endsWith($oldfile, DEFAULT_TOP_IMAGE_NAME)) {
                unlink($oldfile);
            }
        }
    }

    public function get_deposit_type_name() {
        switch ($this->deposit_type) {
            case 1:
                return '普通預金';
            case 2:
                return '当座預金';
            default:
                break;
        }
        return '';
    }


    /* 削除対象テーブル（データテーブル）
frema_block_access
frema_favorites
frema_good
frema_identification
frema_information
frema_invoice
frema_invoice_details
frema_item
frema_item_free_input
frema_item_photo
frema_item_tmp
frema_login_session
frema_negotiation
frema_order
frema_order_number
frema_request
frema_request_follow
frema_user
frema_category
frema_mail_history
frema_mail_history_target
frema_free_input_item_value
frema_free_input_item
     */

    /* 非削除テーブル（編集可能なマスタ）
frema_carriage_plan
frema_carriage_type
frema_delivery_type
frema_evaluate
frema_free_input_item_value
frema_item_state
frema_settle_plan
     */

    /* 削除不可テーブル（システムで初期値が必要なテーブル）
frema_system
frema_free_input_item
frema_pref
frema_type
frema_system_config
     */

}