<?php
include_once(__DIR__.'/../common/util.php');

class Sns {
	public $user_id;
	public $twitter_url = '';
	public $facebook_url = '';
	public $youtube_url = '';
	public $instagram_url = '';
	public $other1_url = '';
	public $other2_url = '';
	public $other3_url = '';
	public $regist_date;
	public $update_date;

	/**
	 * SNS登録
	 */
	public function regist() {
		$db = new DB();
		$param = array(
		    ':user_id' => $this->user_id,
		    ':twitter_url' => $this->twitter_url,
		    ':facebook_url' => $this->facebook_url,
		    ':youtube_url' => $this->youtube_url,
		    ':instagram_url' => $this->instagram_url,
		    ':other1_url' => $this->other1_url,
		    ':other2_url' => $this->other2_url,
		    ':other3_url' => $this->other3_url,
		);

		$db->execute('registSns', $param);
	}

	/**
	 * 検索
	 * @param unknown $id
	 * @return boolean
	 */
	public function select($user_id) {
		$db = new DB();
		$param = array(
		    ':user_id' => $user_id
		);

		$list = $db->query('selectSns', $param);
		if (is_array($list) && count($list) > 0) {
			foreach ($list as $item) {
			    $this->user_id = $item['user_id'];
			    $this->twitter_url = $item['twitter_url'];
			    $this->facebook_url = $item['facebook_url'];
			    $this->youtube_url = $item['youtube_url'];
			    $this->instagram_url = $item['instagram_url'];
			    $this->other1_url = $item['other1_url'];
			    $this->other2_url = $item['other2_url'];
			    $this->other3_url = $item['other3_url'];
				$this->regist_date= $item['regist_date'];
				$this->update_date= $item['update_date'];
			}
			return true;
		}
		return false;
	}
}
