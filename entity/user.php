<?php
include_once(__DIR__.'/../common/util.php');

class User {
	public $id;
	public $hash_id = '';
	public $password = '';
	public $password2 = '';
	public $email = '';
	public $email2 = '';
	public $ipaddr = '';
	public $nickname = '';
	public $name = '';
	public $name_kana = '';
	public $tel = '';
	public $zip = '';
	public $addr = '';
	public $company = '';
	public $pref;
	public $type;
	public $settle_plan = 1; //※販売プランをデフォルトとするため1を設定
	public $regist_date;
	public $update_date;
	public $identification;
	public $selling;
	public $buying;
	public $send_use;
	public $enabled;
	public $invalid;
	public $admin = false;

	public $pref_name = '';
	public $type_name = '';
	public $settle_plan_name = '';
	public $identification_user_id;

	public $financial_institution_name;
	public $branch_name;
	public $deposit_type;
	public $account_number;
	public $account_holder;

	public $self_pr;

	public $sex;
	public $age;
	public $item_interest;

	public function getViewNicknameOrId() {
	    $value = empty($this->nickname) ? PROFILE_NOT_NICKNAME : $this->nickname;
	    if ($this->invalid) {
	        return "$value[退会済み]";
	    }
	    return "$value";
	}

	public function getViewId() {
		if ($this->invalid) {
			return "$this->hash_id[退会済み]";
		}
		return $this->hash_id;
	}

    /**
	 * ユーザ登録
	 */
	public function regist() {
		$db = new DB();
		$this->hash_id = hash('crc32', $this->name. date('YmdHis'));
		$param = array(
				':hash_id' => $this->hash_id,
    		    ':password' => password_hash( $this->password, PASSWORD_DEFAULT),
				':email' => $this->email,
				':ipaddr' => $this->ipaddr,
// 				':tel' => $this->tel,
				':name' => $this->name,
				':name_kana' => $this->name_kana,
				':zip' => $this->zip,
				':addr' => $this->addr,
				':nickname' => $this->nickname,
				':age' => $this->age,
				':sex' => $this->sex,
				':item_interest' => $this->item_interest
// 				':company' => $this->company,
// 				':pref' => $this->pref,
// 				':type' => $this->type,
// 				':settle_plan' => $this->settle_plan
		);

		$db->execute('registUser', $param);
		$this->id = $db->lastInsertId();
		$this->sendRegistMail();

	}

	/**
	 * ユーザ更新
	 */
	

	/**
	 * IPアドレス更新
	 */
	static public function updateIp() {
		if (!isLogin()) {
			return false;
		}

		$db = new DB();
		$param = array(
				':id' => getUserId(),
				':ipaddr' => getRemoteAddress()
		);
		$db->execute('updateUserForIp', $param);
	}

	/**
	 * パスワード更新
	 */
	public function resetPassword() {
		$db = new DB();
		$param = array(
				':id' => $this->id,
                ':password' => password_hash( $this->password, PASSWORD_DEFAULT)
		);
 		$db->execute('resetPassword', $param);
		$this->sendResetPassword();
	}

	/**
	 * プラン変更
	 */
	public function updateSettlePlan() {
		$db = new DB();
		$param = array(
				':id' => $this->id,
				':settle_plan' => $this->settle_plan
		);

		$db->execute('updateSettlePlan', $param);
	}

	/**
	 * 販売許可切り替え
	 */
	public function updateSelling() {
		$db = new DB();
		$param = array(
				':id' => $this->id,
				':selling' => $this->selling ? 1 : 0,
				':send_use' => 1
		);

		$db->execute('updateUserSelling', $param);
		if (!$this->send_use && $this->selling) {
			$this->sendApprovaSellingMail();
		}
	}

	/**
	 * 購入許可切り替え
	 */
	public function updateBuying() {
		$db = new DB();
		$param = array(
				':id' => $this->id,
				':buying' => $this->buying ? 1: 0,
				':send_use' => 1
		);

 		$db->execute('updateUserBuying', $param);
// 		if ($this->buying) {
// 			$this->sendApprovaSellingMail();
// 		}
	}

	/**
	 * ログイン有効切り替え
	 */
	public function updateEnabled2() {
		$db = new DB();
		$param = array(
				':id' => $this->id,
				':enabled' => $this->enabled ? 1 : 0
		);

		$db->execute('updateUserEnabled2', $param);
	}

	/**
	 * 本登録
	 */
	public function updateEnabled() {
		$db = new DB();
		$param = array(
				':id' => $this->id
		);

		$db->execute('updateUserEnabled', $param);
		$this->sendNewUserMail();
		$this->sendCompleteUser();
	}

	/**
	 * ユーザー削除
	 */
	public function updateInvalid() {
		$db = new DB();
		$param = array(
				':id' => $this->id,
				':invalid' => true
		);

		$db->execute('updateUserInvalid', $param);
	}

	/**
	 * 口座情報更新
	 */
	public function updateAccountInfo() {
	    $db = new DB();
	    $param = array(
	        ':id' => $this->id,
	        ':financial_institution_name' => $this->financial_institution_name,
	        ':branch_name' => $this->branch_name,
	        ':deposit_type' => $this->deposit_type,
	        ':account_number' => $this->account_number,
	        ':account_holder' => $this->account_holder,
	    );
	    $db->execute('updateUserForAccountInfo', $param);
	}

	/**
	 * メール送信
	 */
	public function sendNewUserMail() {

	    $system_config = SystemConfig::select();
	    $user = new User();
		$user->select($this->id);

		$mail = new Mail();
		$mail->to = $system_config->to_email;
		$mail->from = $system_config->from_email;
		$mail->fromName = $system_config->from_email_name;

		$template = MailTemplate::get('sendNewUser');
		$mail->subject = $template->subject;
		$mail->body = $template->replaceMessage(
				array(
						MailTemplate::REPLACE_USER_NAME => $user->name
				)
				);
		$mail->sendMail();
	}

	/**
	 * 本登録完了メール送信
	 */
	public function sendCompleteUser() {

	    $system_config = SystemConfig::select();
	    $mail = new Mail();
		$mail->to = $this->email;
		$mail->from = $system_config->from_email;
		$mail->fromName = $system_config->from_email_name;

		$template = MailTemplate::get('sendCompleteUser');
		$mail->subject = $template->subject;
		$mail->body = $template->replaceMessage(
				array(
						MailTemplate::REPLACE_USER_NAME => $this->name
				)
			);
		$mail->sendMail();
	}

	/**
	 * メール送信（販売許可）
	 */
	public function sendApprovaSellingMail() {

	    $system_config = SystemConfig::select();
	    $user = new User();
		$user->select($this->id);

		$mail = new Mail();
		$mail->to = $user->email;
		$mail->from = $system_config->from_email;
		$mail->fromName = $system_config->from_email_name;

		$template = MailTemplate::get('sendApprovaSellingMail');
		$mail->subject = $template->subject;
		$mail->body = $template->replaceMessage(
				array(
						MailTemplate::REPLACE_USER_NAME => $user->name
				)
				);
		$mail->sendMail();
	}

	/**
	 * ログインチェック
	 * @return boolean
	 */
	public function login($is_autologon = false) {
		$db = new DB();
		$param = array(
				':email' => $this->email
		);
		$list = $db->query('login', $param);
		if (is_array($list) && count($list) > 0) {
			foreach ($list as $item) {
			    if (!$is_autologon && !password_verify($this->password, $item['password'])) {
			        continue;
			    }

				$this->id = $item['id'];
				$this->hash_id = $item['hash_id'];
				$this->password = $item['password'];
				$this->email = $item['email'];
				$this->ipaddr = $item['ipaddr'];
				$this->nickname = $item['nickname'];
				$this->name = $item['name'];
				$this->name_kana = $item['name_kana'];
				$this->tel = $item['tel'];
				$this->zip = $item['zip'];
				$this->addr = $item['addr'];
				$this->company = $item['company'];
				$this->pref = $item['pref'];
				$this->type = $item['type'];
				$this->buying = $item['buying'];
				$this->selling = $item['selling'];
				$this->send_use = $item['send_use'];
				$this->settle_plan= $item['settle_plan'];
				$this->identification= $item['identification'];
				$this->enabled = $item['enabled'];
				$this->invalid = $item['invalid'];
				$this->admin = $item['admin'];
				$this->regist_date= $item['regist_date'];
				$this->update_date= $item['update_date'];

				$this->pref_name= $item['pref_name'];
				$this->type_name= $item['type_name'];
				$this->settle_plan_name= $item['settle_plan_name'];

				$this->financial_institution_name= $item['financial_institution_name'];
				$this->branch_name= $item['branch_name'];
				$this->deposit_type= $item['deposit_type'];
				$this->account_number= $item['account_number'];
				$this->account_holder= $item['account_holder'];
				$this->self_pr= $item['self_pr'];
				$this->age= $item['age'];
				$this->sex= $item['sex'];
				$this->item_interest= $item['item_interest'];

				User::registLoginSession($this->id);
				return true;
			}
		}
		return false;

	}

	/**
	 * ユーザー仮取得
	 * @return boolean
	 */
	public function selectFromEmail() {
		$db = new DB();
		$param = array(
				':email' => $this->email
		);
		$list = $db->query('selectUserFromEmail2', $param);
		if (is_array($list) && count($list) > 0) {
			foreach ($list as $item) {
				$this->id = $item['id'];
				$this->hash_id = $item['hash_id'];
				$this->password = $item['password'];
				$this->email = $item['email'];
				$this->ipaddr = $item['ipaddr'];
				$this->nickname = $item['nickname'];
				$this->name = $item['name'];
				$this->name_kana = $item['name_kana'];
				$this->tel = $item['tel'];
				$this->zip = $item['zip'];
				$this->addr = $item['addr'];
				$this->company = $item['company'];
				$this->pref = $item['pref'];
				$this->type = $item['type'];
				$this->buying = $item['buying'];
				$this->selling = $item['selling'];
				$this->send_use = $item['send_use'];
				$this->settle_plan= $item['settle_plan'];
				$this->identification= $item['identification'];
				$this->enabled = $item['enabled'];
				$this->invalid = $item['invalid'];
				$this->regist_date= $item['regist_date'];
				$this->update_date= $item['update_date'];

				$this->pref_name= $item['pref_name'];
				$this->type_name= $item['type_name'];
				$this->settle_plan_name= $item['settle_plan_name'];

				$this->financial_institution_name= $item['financial_institution_name'];
				$this->branch_name= $item['branch_name'];
				$this->deposit_type= $item['deposit_type'];
				$this->account_number= $item['account_number'];
				$this->account_holder= $item['account_holder'];
				$this->self_pr= $item['self_pr'];
			}
			return true;
		}
		return false;

	}

	/**
	 * 登録済みチェック
	 * @return boolean
	 */
	public function has() {
		$db = new DB();
		$param = array(
				':email' => $this->email
		);
		$list = $db->query('selectUserEmail', $param);
		if (is_array($list) && count($list) > 0) {
			foreach ($list as $item) {
				$this->id = $item['id'];
				return true;
			}
		}
		return false;
	}

	/**
	 * 登録済みチェック（ハッシュキー）
	 * @return boolean
	 */
	public function hasHashId() {
		$db = new DB();
		$param = array(
				':hash_id' => $this->hash_id
		);
		$list = $db->query('selectUserHashId', $param);
		if (is_array($list) && count($list) > 0) {
			foreach ($list as $item) {
				$this->id = $item['id'];
				return true;
			}
		}
		return false;
	}

	/**
	 * 検索
	 * @param unknown $id
	 * @return boolean
	 */
	public function selectFromHashId($hash_id) {
		$db = new DB();
		$param = array(
		    ':hash_id' => $hash_id
		);

		$list = $db->query('selectUserFromHashId', $param);
		if (is_array($list) && count($list) > 0) {
			foreach ($list as $item) {
				$this->id = $item['id'];
				$this->hash_id = $item['hash_id'];
				$this->password = $item['password'];
				$this->email = $item['email'];
				$this->ipaddr = $item['ipaddr'];
				$this->nickname = $item['nickname'];
				$this->name = $item['name'];
				$this->name_kana = $item['name_kana'];
				$this->tel = $item['tel'];
				$this->zip = $item['zip'];
				$this->addr = $item['addr'];
				$this->company = $item['company'];
				$this->pref = $item['pref'];
				$this->type = $item['type'];
				$this->buying = $item['buying'];
				$this->selling = $item['selling'];
				$this->send_use = $item['send_use'];
				$this->settle_plan= $item['settle_plan'];
				$this->identification= $item['identification'];
				$this->invalid = $item['invalid'];
				$this->enabled = $item['enabled'];
				$this->regist_date= $item['regist_date'];
				$this->update_date= $item['update_date'];

				$this->pref_name= $item['pref_name'];
				$this->type_name= $item['type_name'];
				$this->settle_plan_name= $item['settle_plan_name'];

				$this->identification_user_id= $item['identification_user_id'];

				$this->financial_institution_name= $item['financial_institution_name'];
				$this->branch_name= $item['branch_name'];
				$this->deposit_type= $item['deposit_type'];
				$this->account_number= $item['account_number'];
				$this->account_holder= $item['account_holder'];
				$this->self_pr= $item['self_pr'];

			}
			return true;
		}
		return false;
	}

	public function select($id) {
	    $db = new DB();
	    if (empty($id)) {
	        $id = $this->id;
	    }
	    $param = array(
	        ':id' => $id
	    );

	    $list = $db->query('selectUser', $param);
	    if (is_array($list) && count($list) > 0) {
	        foreach ($list as $item) {
	            $this->id = $item['id'];
	            $this->hash_id = $item['hash_id'];
	            $this->password = $item['password'];
	            $this->email = $item['email'];
	            $this->ipaddr = $item['ipaddr'];
	            $this->nickname = $item['nickname'];
	            $this->name = $item['name'];
	            $this->name_kana = $item['name_kana'];
	            $this->tel = $item['tel'];
	            $this->zip = $item['zip'];
	            $this->addr = $item['addr'];
	            $this->company = $item['company'];
	            $this->pref = $item['pref'];
	            $this->type = $item['type'];
	            $this->buying = $item['buying'];
	            $this->selling = $item['selling'];
	            $this->send_use = $item['send_use'];
	            $this->settle_plan= $item['settle_plan'];
	            $this->identification= $item['identification'];
	            $this->invalid = $item['invalid'];
	            $this->enabled = $item['enabled'];
	            $this->regist_date= $item['regist_date'];
	            $this->update_date= $item['update_date'];

	            $this->pref_name= $item['pref_name'];
	            $this->type_name= $item['type_name'];
	            $this->settle_plan_name= $item['settle_plan_name'];

	            $this->identification_user_id= $item['identification_user_id'];

	            $this->financial_institution_name= $item['financial_institution_name'];
	            $this->branch_name= $item['branch_name'];
	            $this->deposit_type= $item['deposit_type'];
	            $this->account_number= $item['account_number'];
	            $this->account_holder= $item['account_holder'];
				$this->self_pr= $item['self_pr'];
				$this->age= $item['age'];
				$this->sex= $item['sex'];
				$this->item_interest= $item['item_interest'];

	        }
	        return true;
	    }
	    return false;
	}

	/**
	 * 口座情報があるか確認
	 * @return boolean
	 */
	public function hasAccountInfo() {
	    return !empty(trim($this->financial_institution_name))
    	    && !empty(trim($this->branch_name))
    	    && !empty(trim($this->deposit_type))
    	    && !empty(trim($this->account_number))
    	    && !empty(trim($this->account_holder));
	}

	/**
	 * 全検索
	 * @return User[]
	 */
	public function selectAll() {
		$db = new DB();

		$result = array();
		$list = $db->query('selectUserAll');
		if (is_array($list) && count($list) > 0) {
			foreach ($list as $item) {
				$entity = new User();
				$entity->id = $item['id'];
				$entity->hash_id = $item['hash_id'];
				$entity->password = $item['password'];
				$entity->email = $item['email'];
				$entity->ipaddr = $item['ipaddr'];
				$entity->nickname = $item['nickname'];
				$entity->name = $item['name'];
				$entity->name_kana = $item['name_kana'];
				$entity->tel = $item['tel'];
				$entity->zip = $item['zip'];
				$entity->addr = $item['addr'];
				$entity->company = $item['company'];
				$entity->pref = $item['pref'];
				$entity->type = $item['type'];
				$entity->buying = $item['buying'];
				$entity->selling = $item['selling'];
				$entity->send_use = $item['send_use'];
				$entity->settle_plan= $item['settle_plan'];
				$entity->identification= $item['identification'];
				$entity->invalid = $item['invalid'];
				$entity->enabled = $item['enabled'];
				$entity->regist_date= $item['regist_date'];
				$entity->update_date= $item['update_date'];

				$entity->pref_name= $item['pref_name'];
				$entity->type_name= $item['type_name'];
				$entity->settle_plan_name= $item['settle_plan_name'];
				$entity->identification_user_id= $item['identification_user_id'];

				$entity->enabled = $item['enabled'];

				$entity->financial_institution_name= $item['financial_institution_name'];
				$entity->branch_name= $item['branch_name'];
				$entity->deposit_type= $item['deposit_type'];
				$entity->account_number= $item['account_number'];
				$entity->account_holder= $item['account_holder'];
				$entity->self_pr= $item['self_pr'];

				$result[$entity->id] = $entity;
			}
		}
		return $result;
	}

	/**
	 * ユーザー退会確認
	 * @return boolean
	 */
	public static function isInvalid($id) {
		$user = new User();
		$user->select($id);
		return $user->invalid;
	}

	/**
	 * ハッシュID[表示用」
	 * @return boolean
	 */
	public static function getViewHashId($id) {
		$user = new User();
		$user->select($id);
		return $user->getViewNicknameOrId();
	}

	/**
	 * 承認
	 * @param unknown $id
	 */
	public function approval($id) {
		$db = new DB();
		$param = array(
				':id' => $id
		);

		$db->execute('updateUserIdentification', $param);
	}

	/**
	 * メール送信
	 */
	public function sendRegistMail() {

	    $system_config = SystemConfig::select();
		$mail = new Mail();
		$mail->to = $this->email;
		$mail->from = $system_config->from_email;
		$mail->fromName = $system_config->from_email_name;

		$template = MailTemplate::get('sendRegistUser');
		$mail->subject = $template->subject;
		$mail->body = $template->replaceMessage(
				array(
						MailTemplate::REPLACE_USER_NAME => $this->name,
						MailTemplate::REPLACE_REGIST_USER_URL=> getContextRoot()."/user/usercomplete.php?id=".$this->hash_id."&check=".$this->id
				)
		);
		$mail->sendMail();
	}


	/**
	 * メール送信
	 */
	public function sendResetPassword() {

	    $system_config = SystemConfig::select();
	    $mail = new Mail();
		$mail->to = $this->email;
		$mail->from = $system_config->from_email;
		$mail->fromName = $system_config->from_email_name;

// 		$this->select($this->id);

		$template = MailTemplate::get('sendResetPassword');
		$mail->subject = $template->subject;
		$mail->body = $template->replaceMessage(
				array(
						MailTemplate::REPLACE_USER_NAME => $this->name,
						MailTemplate::REPLACE_NEW_PASSWORD=> $this->password
				)
				);
		$mail->sendMail();
	}

	/**
	 * ログインセッション情報登録
	 */
	public static function registLoginSession($user_id) {
		$db = new DB();
		$hash_key = hash('ripemd160', getUserName(). date('YmdHis'));
		$param = array(
				':hash_key' => $hash_key,
				':user_id' => $user_id
		);

		$db->execute('registLoginSession', $param);
		setcookie('SESSION_KEY', $hash_key, time()+60*60*24*365);
	}

	/**
	 * ログインセッション情報取得
	 */
	public static function selectLoginSession($hash_key) {
		$result = new User();
		$db = new DB();
		$param = array(
				':hash_key' => $hash_key
		);

		$list = $db->query('selectLoginSession', $param);
		if (is_array($list) && count($list) > 0) {
			foreach ($list as $item) {
				$result->password = $item['password'];
				$result->email = $item['email'];
			}
		}
		return $result;
	}

	public static function removeLoginSession() {
		$db = new DB();
		$param = array(
				':user_id' => getUserId()
		);

		$db->execute('deleteLoginSession', $param);
		setcookie('SESSION_KEY', '', time() - 1800);
	}

	/**
	 * バリデーション
	 * @return unknown
	 */
	public function validate() {
		$msg = '';
		if (empty($this->email)) {
			$msg .= '[メールアドレス]';
		}
		if (empty($this->name)) {
			$msg .= '[名前]';
		}
		// if (empty($this->name_kana)) {
		// 	$msg .= '[フリガナ]';
		// }
// 		if (empty($this->zip)) {
// 			$msg .= '[郵便番号]';
// 		}
// 		if (empty($this->addr)) {
// 			$msg .= '[住所]';
// 		}
		if (empty($this->password)) {
			$msg .= '[パスワード]';
		}
// 		if (empty($this->tel)) {
// 			$msg .= '[電話番号]';
// 		}
// 		if (empty($this->pref)) {
// 			$msg .= '[地域]';
// 		}
// 		if (empty($this->type)) {
// 			$msg .= '[属性]';
// 		}

		if (!empty($msg)) {
			$msg .= 'は必須項目です。';
			setMessage($msg);
		}

		return empty($msg);
	}

	public function validate_update() {
	    $msg = '';
	    if (empty($this->email)) {
	        $msg .= '[メールアドレス]';
	    }
	    if (empty($this->name)) {
	        $msg .= '[名前]';
	    }
	    if (empty($this->name_kana)) {
	        $msg .= '[フリガナ]';
	    }
		if (empty($this->age)){
			$msg .= '[年齢]';
		}
	    if (empty($this->zip)) {
	        $msg .= '[郵便番号]';
	    }
	    if (empty($this->addr)) {
	        $msg .= '[住所]';
	    }
	    if (empty($this->tel)) {
	        $msg .= '[電話番号]';
	    }
	    if (empty($this->pref)) {
	        $msg .= '[地域]';
	    }
	    if (empty($this->type)) {
	        $msg .= '[属性]';
	    }

	    if (!empty($msg)) {
	        $msg .= 'は必須項目です。';
	    }

	    if (!empty($this->password)) {
	        if ($this->password != $this->password2) {
	            if (!empty($msg)) {
	                $msg .= '\r\n';
	            }
	            $msg .= '[パスワード]が一致しません。';
	        }
	    }

	    if (!empty($msg)) {
	        setMessage($msg);
	    }

	    return empty($msg);
	}

	public function get_icon_url() {
	    $filename = hash('sha256', $this->hash_id.'1');
	    if (!file_exists(__DIR__.'/../'.USER_ICON_DIR.$filename)) {
            return '';
	    }
	    return getContextRoot().'/'.USER_ICON_DIR.$filename;
	}

	public function get_header_image_url() {
	    $filename = hash('sha256', $this->hash_id.'1');
	    if (!file_exists(__DIR__.'/../'.USER_HEADER_IMAGE_DIR.$filename)) {
	        return '';
	    }
	    return getContextRoot().'/'.USER_HEADER_IMAGE_DIR.$filename;
	}

	public function regist_icon() {
	    $filename = hash('sha256', $this->hash_id.'1');
	    if (!file_exists(__DIR__.'/../'.USER_ICON_DIR)) {
	        mkdir(__DIR__.'/../'.USER_ICON_DIR, 0777, true);
	    }

	    if (!empty($_FILES['userIcon']) && $_FILES['userIcon']['error'] == 0) {
	        $image_path = __DIR__.'/../'.USER_ICON_DIR.$filename;
	        $image = rotateImage(file_get_contents($_FILES['userIcon']['tmp_name']));
	        $handler = fopen($image_path, 'w');
	        $result = fwrite($handler, $image);
	        fclose($handler);
	    }
	}

	public function regist_header_image() {
	    $filename = hash('sha256', $this->hash_id.'1');
	    if (!file_exists(__DIR__.'/../'.USER_HEADER_IMAGE_DIR)) {
	        mkdir(__DIR__.'/../'.USER_HEADER_IMAGE_DIR, 0777, true);
	    }

	    if (!empty($_FILES['headerImage']) && $_FILES['headerImage']['error'] == 0) {
	        $image_path = __DIR__.'/../'.USER_HEADER_IMAGE_DIR.$filename;
	        $image = rotateImage(file_get_contents($_FILES['headerImage']['tmp_name']));
	        $handler = fopen($image_path, 'w');
	        $result = fwrite($handler, $image);
	        fclose($handler);
	    }
	}

	public function delete_icon() {
	    $filename = hash('sha256', $this->hash_id.'1');
		$image_path = __DIR__.'/../'.USER_ICON_DIR.$filename;
		if(file_exists($image_path)){
			unlink($image_path);
		}
	}

	public function delete_header_image() {
		$filename = hash('sha256', $this->hash_id.'1');
	    $image_path = __DIR__.'/../'.USER_HEADER_IMAGE_DIR.$filename;
		if(file_exists($image_path)){
			unlink($image_path);
		}
	}

	public function get_deposit_type_name() {
	    switch ($this->deposit_type) {
	        case 1:
	            return '普通預金';
	        case 2:
	            return '当座預金';
	        default:
	            break;
	    }
	    return '';
	}

	public function selectUserFromEmail() {
		$result = new User();
		$db = new DB();
		$param = array(
				':email' => $this->email
		);
		$list = $db->query('selectUserFromEmail2', $param);
		if (is_array($list) && count($list) > 0) {
			foreach ($list as $item) {
				$result->nickname = $item['nickname'];
				$result->name = $item['name'];
			}
		}
		return $result;
	}

	public function validate_updateInfo() {
		//check conflict
	}
}
