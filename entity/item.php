<?php
include_once(__DIR__.'/../common/db.php');
include_once(__DIR__.'/free_input_item.php');
include_once(__DIR__.'/free_input_item_value.php');
include_once(__DIR__.'/item_free_info.php');
include_once(__DIR__.'/system_config.php');

/**
 * 商品
 * @author asagao
 *
 */
class Item {

	public $id;
	public $access_count;
	public $owner_id;
	public $title;
	public $category;
	public $item_state;
	public $remarks;
	public $pref;
	public $carriage_plan;
	public $carriage_type;
	public $delivery_type;
	public $delivery_price;
	public $price;
	public $stock = 0;
	public $enabled;
	public $regist_date;
	public $update_date;
	public $target_user_id;

	public $pref_name;
	public $item_state_name;
	public $carriage_plan_name;
	public $carriage_type_name;
	public $delivery_type_name;
	public $category_name;
	public $buyer_id;

	public $negotiation_count;
	public $last_date;

	public $photo1;
	public $photo2;
	public $photo3;
	public $photo4;
	public $photo5;
	public $photo6;
	public $photo7;
	public $photo8;
	public $photo9;
	public $photo10;

	public $owner_name;
	public $owner_hash_id;
	public $owner_selling;

	public $order_id;
	public $buyer_evaluate_name;
	public $seller_evaluate_name;
	public $buyer_evaluate;
	public $seller_evaluate;
	public $buyer_evaluate_hash_id;
	public $seller_evaluate_hash_id;

	public $evaluate_count;
	public $evaluate_point;

	public $order_date;

	public $show_evaluate;
	public $with_delivery;

	public $good_count;

	public $favorites_regist_date;
	public $favorites_update_date;

	public $free_input = [];

	public $owner;

	public $groupCode;
	public $groupObject;

	public $photoGaziru1;
	public $photoGaziru2;
	public $photoGaziru3;
	public $photoGaziru4;
	public $photoGaziru5;
	public $photoGaziru6;
	public $photoGaziru7;
	public $photoGaziru8;
	public $photoGaziru9;
	public $photoGaziru10;

	public $imageAuthen1;
	public $imageAuthen2;
	public $imageAuthen3;

	public $photoCheck1;
	public $photoCheck2;
	public $photoCheck3;
	public $photoCheck4;
	public $photoCheck5;
	public $photoCheck6;
	public $photoCheck7;
	public $photoCheck8;
	public $photoCheck9;
	public $photoCheck10;

	function __construct() {
	    $this->owner = new User();
	}
	/**
	 * 許可
	 */
	public function approval() {
		$db = new DB();
		$param = array(
				':id' => $this->id,
				':enabled' => 1
		);
		$db->execute('registItemEnabled', $param);
		$this->sendApprovalItemMail();
	}

	/**
	 * 却下
	 */
	public function regection() {
		$db = new DB();
		$param = array(
				':id' => $this->id,
				':enabled' => 2
		);
		$db->execute('registItemEnabled', $param);
	}

	/**
	 * キャンセル
	 */
	public function cancel() {
		$db = new DB();
		$param = array(
				':id' => $this->id,
				':enabled' => 9
		);
		$db->execute('registItemEnabled', $param);
		$this->sendCancelItemMail();
	}

	/**
	 * 登録
	 */
	public function regist() {
		$db = new DB();
		$id = empty($this->id) || $this->id == 0 ? 0 : $this->id;
		$this->groupCode = $this->createGroup();
		
		if($this->photoGaziru1 != null && $this->photoGaziru2!= null){
			$jsonObjectCode = array('1' => $this->photoGaziru1, '2'=>$this->photoGaziru2);
		}else{
			if($this->photoGaziru1 != null){
				$jsonObjectCode = array('1' => $this->photoGaziru1);
			}else{
				if($this->photoGaziru2 != null){
					$jsonObjectCode = array('2' => $this->photoGaziru2);
				}
			}
		}
		
		$param = array(
			':owner_id' => getUserId(),
			':title' => $this->title,
			':category' => $this->category,
			':item_state' => $this->item_state,
			':remarks' => $this->remarks,
			':pref' => $this->pref,
			':carriage_type' => $this->carriage_type,
			':carriage_plan' => $this->carriage_plan,
			':delivery_type' => $this->delivery_type,
			':delivery_price' => $this->delivery_price,
			':price' => $this->price,
			':stock' => $this->stock,
		    ':show_evaluate' => $this->show_evaluate ? 1 : 0,
		    ':with_delivery' => $this->with_delivery ? 1 : 0,
		    ':target_user_id' => $this->target_user_id,
			':groupCode' => $this->groupCode,
			':groupObject' => json_encode($jsonObjectCode)
		);
		if ($id == 0) {
			$db->execute('registItem', $param);
			if($this->photoGaziru1 != null){
				$this->addObjectToGroup($this->groupCode,$this->photoGaziru1);
			}
			if($this->photoGaziru2 != null){
				$this->addObjectToGroup($this->groupCode,$this->photoGaziru2);
			}
			if($this->photoGaziru3 != null){
				$this->addObjectToGroup($this->groupCode,$this->photoGaziru3);
			}
			if($this->photoGaziru4 != null){
				$this->addObjectToGroup($this->groupCode,$this->photoGaziru4);
			}
			if($this->photoGaziru5 != null){
				$this->addObjectToGroup($this->groupCode,$this->photoGaziru5);
			}
			if($this->photoGaziru6 != null){
				$this->addObjectToGroup($this->groupCode,$this->photoGaziru6);
			}
			if($this->photoGaziru7 != null){
				$this->addObjectToGroup($this->groupCode,$this->photoGaziru7);
			}
			if($this->photoGaziru8 != null){
				$this->addObjectToGroup($this->groupCode,$this->photoGaziru8);
			}
			if($this->photoGaziru9 != null){
				$this->addObjectToGroup($this->groupCode,$this->photoGaziru9);
			}
			if($this->photoGaziru10 != null){
				$this->addObjectToGroup($this->groupCode,$this->photoGaziru10);
			}

		}
		else {
			$param[':id'] = $id;
			$db->execute('registItem2', $param);
		}

		//$db->beginTransaction();
		//$db->commit();
		if ($id == 0) {
			$this->id = $db->lastInsertId();
		}

		foreach ($this->free_input as $item) {
		    $item->item_id = $this->id;
		    $item->regist();
		}

		$this->registPhoto();
		$this->registImgAuthen();
		$this->sendNewItemMail();
	}

	/**
	 * 更新
	 */
	public function modify() {
		$db = new DB();
		$param = array(
				':id' => $this->id,
				':price' => $this->price
		);
		$db->execute('modifyItem', $param);
	}

	public function createGroup(){
		if(authenticateRequestAsync()){
			$params = [
				'id' => ''
			];
			$base = "https://teppan.gaziru-service.jp/v2/web/groups";
			$curl = curl_init();
			curl_setopt($curl, CURLOPT_URL, $base);
			curl_setopt($curl, CURLOPT_POST, TRUE);
			curl_setopt($curl, CURLOPT_HTTPHEADER, [
				'Content-Type:application/json',
				'Host: teppan.gaziru-service.jp',
				'Authorization: Bearer '. json_decode(authenticateRequestAsync(),true)["access_token"]
			]);
			curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);
			curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
		
			$xml = curl_exec($curl);
			return json_decode($xml,true)["groupId"];
		}
	}

	public function addObjectToGroup($groupId, $objId){
		if($groupId != null){
			$base = "https://teppan.gaziru-service.jp/v2/web/groups/".$groupId."/objects/put";
			$curl = curl_init();
			$params = [
				'ids' => '['.$objId .']'
			];

			curl_setopt($curl, CURLOPT_POSTFIELDS, "{\"ids\":[\"".$objId."\"]}");
			curl_setopt($curl, CURLOPT_URL, $base);
			curl_setopt($curl, CURLOPT_POST, TRUE);
			curl_setopt($curl, CURLOPT_HTTPHEADER, [
				'Content-Type:application/json',
				'Host: teppan.gaziru-service.jp',
				'Authorization: Bearer '. json_decode(authenticateRequestAsync(),true)["access_token"]
			]);
			
			curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);
			curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
			$xml = curl_exec($curl);
			return $xml;
		}
	}

	/**
	 * 登録（下書き）
	 */
	public function registTmp() {
		$db = new DB();
		$param = array(
			':owner_id' => getUserId(),
			':title' => $this->title,
			':category' => $this->category,
			':item_state' => $this->item_state,
			':remarks' => $this->remarks,
			':pref' => $this->pref,
			':carriage_type' => $this->carriage_type,
			':carriage_plan' => $this->carriage_plan,
			':delivery_type' => $this->delivery_type,
			':delivery_price' => $this->delivery_price,
			':price' => $this->price,
			':stock' => $this->stock,
		    ':show_evaluate' => $this->show_evaluate ? 1 : 0,
		    ':with_delivery' => $this->with_delivery ? 1 : 0,
		);

		$db->execute('registItemTmp', $param);
	}

	/**
	 * 詳細（下書き）削除
	 * @param unknown $id
	 * @return boolean
	 */
	public function removeTmp() {
		$db = new DB();
		$param = array(
				':id' => $this->id
		);

		$db->execute('removeItemTmp', $param);
	}

	public function registPhotoOld() {
		$db = new DB();
		$param = array(
				':item_id' => $this->id,
				':number' => 1,
				':photo' => convertImage($this->photo1)
		);
		$db->execute('registItemPhoto', $param);
		$param = array(
				':item_id' => $this->id,
				':number' => 2,
				':photo' => $this->photo2
		);
		$db->execute('registItemPhoto', $param);
		$param = array(
				':item_id' => $this->id,
				':number' => 3,
				':photo' => $this->photo3
		);
		$db->execute('registItemPhoto', $param);
		$param = array(
				':item_id' => $this->id,
				':number' => 4,
				':photo' => $this->photo4
		);
		$db->execute('registItemPhoto', $param);

	}

	public function registPhoto() {
		$dirpath = __DIR__.'/../images/item/id' . $this->id;
		if (!file_exists($dirpath)) {
			mkdir($dirpath, 0777, true);
		}

		if ($this->photo1 != null) {
		    $image_path = $dirpath . '/'.hash('sha256', 'image'.$this->id.'1');
			$handler = fopen($image_path, 'w');
			$result = fwrite($handler, convertImage($this->photo1));
			fclose($handler);


			$thumbnail_path = $dirpath . '/'.hash('sha256', 'thumbnail'.$this->id.'1');
			$handler = fopen($thumbnail_path, 'w');
			$result = fwrite($handler, convertThumbnailImage($this->photo1));
			fclose($handler);
		} else {
		    $image_path = $dirpath . '/'.hash('sha256', 'image'.$this->id.'1');
		    if (file_exists($image_path)) {
				unlink($image_path);
			}
		}

		if ($this->photo2 != null) {
		    $image_path = $dirpath . '/'.hash('sha256', 'image'.$this->id.'2');
		    $handler = fopen($image_path, 'w');
			$result = fwrite($handler, convertImage($this->photo2));
			fclose($handler);
		} else {
		    $image_path = $dirpath . '/'.hash('sha256', 'image'.$this->id.'2');
		    if (file_exists($image_path)) {
				unlink($image_path);
			}
		}

		if ($this->photo3 != null) {
		    $image_path = $dirpath . '/'.hash('sha256', 'image'.$this->id.'3');
		    $handler = fopen($image_path, 'w');
			$result = fwrite($handler, convertImage($this->photo3));
			fclose($handler);
		} else {
		    $image_path = $dirpath . '/'.hash('sha256', 'image'.$this->id.'3');
		    if (file_exists($image_path)) {
				unlink($image_path);
			}
		}

		if ($this->photo4 != null) {
		    $image_path = $dirpath . '/'.hash('sha256', 'image'.$this->id.'4');
		    $handler = fopen($image_path, 'w');
			$result = fwrite($handler, convertImage($this->photo4));
			fclose($handler);
		} else {
		    $image_path = $dirpath . '/'.hash('sha256', 'image'.$this->id.'4');
		    if (file_exists($image_path)) {
				unlink($image_path);
			}
		}

		if ($this->photo5 != null) {
		    $image_path = $dirpath . '/'.hash('sha256', 'image'.$this->id.'5');
		    $handler = fopen($image_path, 'w');
			$result = fwrite($handler, convertImage($this->photo5));
			fclose($handler);
		} else {
		    $image_path = $dirpath . '/'.hash('sha256', 'image'.$this->id.'5');
		    if (file_exists($image_path)) {
				unlink($image_path);
			}
		}

		if ($this->photo6 != null) {
		    $image_path = $dirpath . '/'.hash('sha256', 'image'.$this->id.'6');
		    $handler = fopen($image_path, 'w');
			$result = fwrite($handler, convertImage($this->photo6));
			fclose($handler);
		} else {
		    $image_path = $dirpath . '/'.hash('sha256', 'image'.$this->id.'6');
		    if (file_exists($image_path)) {
				unlink($image_path);
			}
		}

		if ($this->photo7 != null) {
		    $image_path = $dirpath . '/'.hash('sha256', 'image'.$this->id.'7');
		    $handler = fopen($image_path, 'w');
			$result = fwrite($handler, convertImage($this->photo7));
			fclose($handler);
		} else {
		    $image_path = $dirpath . '/'.hash('sha256', 'image'.$this->id.'7');
		    if (file_exists($image_path)) {
				unlink($image_path);
			}
		}

		if ($this->photo8 != null) {
		    $image_path = $dirpath . '/'.hash('sha256', 'image'.$this->id.'8');
		    $handler = fopen($image_path, 'w');
			$result = fwrite($handler, convertImage($this->photo8));
			fclose($handler);
		} else {
		    $image_path = $dirpath . '/'.hash('sha256', 'image'.$this->id.'8');
		    if (file_exists($image_path)) {
				unlink($image_path);
			}
		}

		if ($this->photo9 != null) {
		    $image_path = $dirpath . '/'.hash('sha256', 'image'.$this->id.'9');
		    $handler = fopen($image_path, 'w');
			$result = fwrite($handler, convertImage($this->photo9));
			fclose($handler);
		} else {
		    $image_path = $dirpath . '/'.hash('sha256', 'image'.$this->id.'9');
		    if (file_exists($image_path)) {
				unlink($image_path);
			}
		}

		if ($this->photo10 != null) {
		    $image_path = $dirpath . '/'.hash('sha256', 'image'.$this->id.'10');
		    $handler = fopen($image_path, 'w');
			$result = fwrite($handler, convertImage($this->photo10));
			fclose($handler);
		} else {
		    $image_path = $dirpath . '/'.hash('sha256', 'image'.$this->id.'10');
		    if (file_exists($image_path)) {
				unlink($image_path);
			}
		}
	}

	public function registImgAuthen(){
		$dirpath = __DIR__.'/../images/item/authen/id' . $this->id;
		if (!file_exists($dirpath)) {
			mkdir($dirpath, 0777, true);
		}

		if ($this->imageAuthen1 != null) {
		    $image_path = $dirpath . '/'.hash('sha256', 'image'.$this->id.'1');
			$handler = fopen($image_path, 'w');
			$result = fwrite($handler, convertImage($this->imageAuthen1, 1024, 1024));
			fclose($handler);


			$thumbnail_path = $dirpath . '/'.hash('sha256', 'thumbnail'.$this->id.'1');
			$handler = fopen($thumbnail_path, 'w');
			$result = fwrite($handler, convertThumbnailImage($this->imageAuthen1));
			fclose($handler);
		} else {
		    $image_path = $dirpath . '/'.hash('sha256', 'image'.$this->id.'1');
		    if (file_exists($image_path)) {
				unlink($image_path);
			}
		}
		if ($this->imageAuthen2 != null) {
		    $image_path = $dirpath . '/'.hash('sha256', 'image'.$this->id.'2');
			$handler = fopen($image_path, 'w');
			$result = fwrite($handler, convertImage($this->imageAuthen2, 1024, 1024));
			fclose($handler);


			$thumbnail_path = $dirpath . '/'.hash('sha256', 'thumbnail'.$this->id.'2');
			$handler = fopen($thumbnail_path, 'w');
			$result = fwrite($handler, convertThumbnailImage($this->imageAuthen2));
			fclose($handler);
		} else {
		    $image_path = $dirpath . '/'.hash('sha256', 'image'.$this->id.'2');
		    if (file_exists($image_path)) {
				unlink($image_path);
			}
		}
		if ($this->imageAuthen3 != null) {
		    $image_path = $dirpath . '/'.hash('sha256', 'image'.$this->id.'3');
			$handler = fopen($image_path, 'w');
			$result = fwrite($handler, convertImage($this->imageAuthen3, 1024, 1024));
			fclose($handler);


			$thumbnail_path = $dirpath . '/'.hash('sha256', 'thumbnail'.$this->id.'3');
			$handler = fopen($thumbnail_path, 'w');
			$result = fwrite($handler, convertThumbnailImage($this->imageAuthen3));
			fclose($handler);
		} else {
		    $image_path = $dirpath . '/'.hash('sha256', 'image'.$this->id.'3');
		    if (file_exists($image_path)) {
				unlink($image_path);
			}
		}
	}

	public function registerRequestGaziru($imageFile){
		if(authenticateRequestAsync()){
			$params = [
				'id' => '',
				'image' => $imageFile
			];
			$base = "https://teppan.gaziru-service.jp/v2/web/objects";
			$curl = curl_init();
			curl_setopt($curl, CURLOPT_URL, $base);
			curl_setopt($curl, CURLOPT_POST, TRUE);
				
			curl_setopt($curl, CURLOPT_HTTPHEADER, [
				'Host: teppan.gaziru-service.jp',
				'Authorization: Bearer '. json_decode(authenticateRequestAsync(),true)["access_token"]
			]);
			curl_setopt($curl, CURLOPT_POSTFIELDS, $params);
			curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);
			curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
		
			$xml = curl_exec($curl);
			return $xml;
		}
	}

	/**
	 * メール送信
	 */
	public function sendNewItemMail() {

	    $system_config = SystemConfig::select();
	    $mail = new Mail();
	    $mail->to = $system_config->to_email;
	    $mail->from = $system_config->from_email;
	    $mail->fromName = $system_config->from_email_name;

		$owner = new User();
		$owner->select(getUserId());

		$template = MailTemplate::get('sendNewItem');
		$mail->subject = $template->replaceTitle(
				array(
						MailTemplate::REPLACE_TITLE=> $this->title,
				)
				);
		$mail->body = $template->replaceMessage(
				array(
						MailTemplate::REPLACE_TITLE => $this->title,
						MailTemplate::REPLACE_CATEGORY_NAME => $this->category_name,
						MailTemplate::REPLACE_OWNER_NAME => $owner->name,
						MailTemplate::REPLACE_OWNER_HASH_ID => $owner->hash_id
				)
				);
		$mail->sendMail();
	}

	/**
	 * 取り消しメール送信
	 */
	public function sendCancelItemMail() {

		$this->sendCancelItemAdminMail();
		$this->sendCancelItemOwnerMail();
	}

	/**
	 * 取り消しメール送信（管理者）
	 */
	public function sendCancelItemAdminMail() {

	    $system_config = SystemConfig::select();
	    $owner = new User();
		$owner->select($this->owner_id);
		$mail->to = $owner->email;

		$mail = new Mail();
		$mail->to = $system_config->to_email;
		$mail->from = $system_config->from_email;
		$mail->fromName = $system_config->from_email_name;

		$template = MailTemplate::get('sendCancelItemAdmin');
		$mail->subject = $template->replaceTitle(
				array(
						MailTemplate::REPLACE_TITLE=> $this->title,
				)
				);
		$mail->body = $template->replaceMessage(
				array(
						MailTemplate::REPLACE_TITLE => $this->title,
						MailTemplate::REPLACE_CATEGORY_NAME => $this->category_name,
						MailTemplate::REPLACE_OWNER_NAME => $owner->name,
						MailTemplate::REPLACE_OWNER_HASH_ID => $owner->hash_id
				)
				);
		$mail->sendMail();
	}

	/**
	 * 取り消しメール送信（出品者）
	 */
	public function sendCancelItemOwnerMail() {

	    $system_config = SystemConfig::select();
	    $owner = new User();
		$owner->select($this->owner_id);

		$mail = new Mail();
		$mail->to = $owner->email;
		$mail->from = $system_config->from_email;
		$mail->fromName = $system_config->from_email_name;

		$template = MailTemplate::get('sendCancelItemOwner');
		$mail->subject = $template->replaceTitle(
				array(
						MailTemplate::REPLACE_TITLE=> $this->title,
				)
				);
		$mail->body = $template->replaceMessage(
				array(
						MailTemplate::REPLACE_TITLE => $this->title,
						MailTemplate::REPLACE_CATEGORY_NAME => $this->category_name,
						MailTemplate::REPLACE_OWNER_NAME => $owner->name,
						MailTemplate::REPLACE_OWNER_HASH_ID => $owner->hash_id
				)
				);
		$mail->sendMail();
	}

	/**
	 * 取り消しメール送信（購入者）
	 */
	public function sendCancelItemBuyerMail() {

	    $system_config = SystemConfig::select();
	    $mail = new Mail();
	    $mail->to = $system_config->to_email;
	    $mail->from = $system_config->from_email;
	    $mail->fromName = $system_config->from_email_name;

		$owner = new User();
		$owner->select(getUserId());

		$template = MailTemplate::get('sendNewItem');
		$mail->subject = $template->replaceTitle(
				array(
						MailTemplate::REPLACE_TITLE=> $this->title,
				)
				);
		$mail->body = $template->replaceMessage(
				array(
						MailTemplate::REPLACE_TITLE => $this->title,
						MailTemplate::REPLACE_CATEGORY_NAME => $this->category_name,
						MailTemplate::REPLACE_OWNER_NAME => $owner->name,
						MailTemplate::REPLACE_OWNER_HASH_ID => $owner->hash_id
				)
				);
		$mail->sendMail();
	}

	/**
	 * 承認メール送信（出品者）
	 */
	public function sendApprovalItemMail() {

	    $system_config = SystemConfig::select();
	    $owner = new User();
		$owner->select($this->owner_id);

		$mail = new Mail();
		$mail->to = $owner->email;
		$mail->from = $system_config->from_email;
		$mail->fromName = $system_config->from_email_name;

		$url = getContextRoot() . '/itemdetail.php?id='.$this->id;

		$template = MailTemplate::get('sendApprovalItemMail');
		$mail->subject = $template->replaceTitle(
				array(
						MailTemplate::REPLACE_TITLE=> $this->title,
				)
				);
		$mail->body = $template->replaceMessage(
				array(
						MailTemplate::REPLACE_TITLE => $this->title,
						MailTemplate::REPLACE_CATEGORY_NAME => $this->category_name,
						MailTemplate::REPLACE_OWNER_NAME => $this->owner_name,
						MailTemplate::REPLACE_OWNER_HASH_ID => $this->owner_hash_id,
						MailTemplate::REPLACE_ITEM_URL=> $url
				)
				);
		$mail->sendMail();
	}



	/**
	 * 全件検索
	 * @return Item[]
	 */
	public function selectAll() {
		$db = new DB();

		$result = array();
		$list = $db->query('selectItemAll');
		if (is_array($list) && count($list) > 0) {
			foreach ($list as $item) {
				$entity = new Item();
				$entity->id = $item['id'];
				$entity->access_count= $item['access_count'];
				$entity->owner_id= $item['owner_id'];
				$entity->title= $item['title'];
				$entity->category= $item['category'];
				$entity->item_state= $item['item_state'];
				$entity->remarks= $item['remarks'];
				$entity->carriage_type= $item['carriage_type'];
				$entity->carriage_plan= $item['carriage_plan'];
				$entity->delivery_type= $item['delivery_type'];
				$entity->price= $item['price'];
				$entity->stock= $item['stock'];
				$entity->photo= $item['photo'];
				$entity->enabled= $item['enabled'];
				$entity->owner_name= $item['owner_name'];
				$entity->owner_hash_id= $item['owner_hash_id'];
				$entity->owner_selling= $item['owner_selling'];

				$entity->regist_date= $item['regist_date'];
				$entity->update_date= $item['update_date'];

				$entity->item_state_name= $item['item_state_name'];
				$entity->carriage_plan_name= $item['carriage_plan_name'];
				$entity->carriage_type_name= $item['carriage_type_name'];
				$entity->delivery_type_name= $item['delivery_type_name'];
				$entity->category_name= $item['category_name'];

				$entity->buyer_id= $item['buyer_id'];
				$entity->order_id= $item['order_id'];
				$entity->seller_evaluate= $item['seller_evaluate'];
				$entity->buyer_evaluate= $item['buyer_evaluate'];
				$entity->seller_evaluate_name= $item['seller_evaluate_name'];
				$entity->buyer_evaluate_name= $item['buyer_evaluate_name'];

				$entity->evaluate_count= $item['evaluate_count'];
				$entity->evaluate_point= $item['evaluate_point'];

				$entity->order_date= $item['order_date'];
				$entity->show_evaluate= $item['show_evaluate'];
				$entity->with_delivery= $item['with_delivery'];

				$entity->owner->select($entity->owner_id);

				$this->free_input = ItemFreeInfo::selectFromItemId($entity->id);
				$result[] = $entity;
			}
		}
		return $result;
	}

	/**
	 * 全件検索
	 * @return Item[]
	 */
	public function selectNewItemsLimited() {
		$db = new DB();

		$result = array();
		$list = $db->query('selectNewItemsLimted');
		if (is_array($list) && count($list) > 0) {
			foreach ($list as $item) {
				$entity = new Item();
				$entity->id = $item['id'];
				$entity->access_count= $item['access_count'];
				$entity->owner_id= $item['owner_id'];
				$entity->title= $item['title'];
				$entity->category= $item['category'];
				$entity->item_state= $item['item_state'];
				$entity->remarks= $item['remarks'];
				$entity->carriage_type= $item['carriage_type'];
				$entity->carriage_plan= $item['carriage_plan'];
				$entity->delivery_type= $item['delivery_type'];
				$entity->price= $item['price'];
				$entity->stock= $item['stock'];
				$entity->photo= $item['photo'];
				$entity->enabled= $item['enabled'];
				$entity->owner_name= $item['owner_name'];
				$entity->owner_hash_id= $item['owner_hash_id'];
				$entity->owner_selling= $item['owner_selling'];

				$entity->regist_date= $item['regist_date'];
				$entity->update_date= $item['update_date'];

				$entity->item_state_name= $item['item_state_name'];
				$entity->carriage_plan_name= $item['carriage_plan_name'];
				$entity->carriage_type_name= $item['carriage_type_name'];
				$entity->delivery_type_name= $item['delivery_type_name'];
				$entity->category_name= $item['category_name'];

				$entity->buyer_id= $item['buyer_id'];
				$entity->order_id= $item['order_id'];
				$entity->seller_evaluate= $item['seller_evaluate'];
				$entity->buyer_evaluate= $item['buyer_evaluate'];
				$entity->seller_evaluate_name= $item['seller_evaluate_name'];
				$entity->buyer_evaluate_name= $item['buyer_evaluate_name'];

				$entity->evaluate_count= $item['evaluate_count'];
				$entity->evaluate_point= $item['evaluate_point'];

				$entity->order_date= $item['order_date'];
				$entity->show_evaluate= $item['show_evaluate'];
				$entity->with_delivery= $item['with_delivery'];

				$entity->owner->select($entity->owner_id);

				$this->free_input = ItemFreeInfo::selectFromItemId($entity->id);
				$result[] = $entity;
			}
		}
		return $result;
	}

	/**
	 * 全件検索
	 * @return Item[]
	 */
	public function selectSearch($search) {
		$db = new DB();
		$param = array(
				':search' => '%'.$search.'%'
		);

		$result = array();
		$list = $db->query('selectItemSearch', $param);
		if (is_array($list) && count($list) > 0) {
			foreach ($list as $item) {
				$entity = new Item();
				$entity->id = $item['id'];
				$entity->access_count= $item['access_count'];
				$entity->owner_id= $item['owner_id'];
				$entity->title= $item['title'];
				$entity->category= $item['category'];
				$entity->item_state= $item['item_state'];
				$entity->remarks= $item['remarks'];
				$entity->carriage_type= $item['carriage_type'];
				$entity->carriage_plan= $item['carriage_plan'];
				$entity->delivery_type= $item['delivery_type'];
				$entity->price= $item['price'];
				$entity->stock= $item['stock'];
				$entity->photo= $item['photo'];
				$entity->enabled= $item['enabled'];
				$entity->owner_name= $item['owner_name'];
				$entity->owner_hash_id= $item['owner_hash_id'];
				$entity->owner_selling= $item['owner_selling'];

				$entity->regist_date= $item['regist_date'];
				$entity->update_date= $item['update_date'];

				$entity->item_state_name= $item['item_state_name'];
				$entity->carriage_plan_name= $item['carriage_plan_name'];
				$entity->carriage_type_name= $item['carriage_type_name'];
				$entity->delivery_type_name= $item['delivery_type_name'];
				$entity->category_name= $item['category_name'];

				$entity->buyer_id= $item['buyer_id'];
				$entity->order_id= $item['order_id'];
				$entity->seller_evaluate= $item['seller_evaluate'];
				$entity->buyer_evaluate= $item['buyer_evaluate'];
				$entity->seller_evaluate_name= $item['seller_evaluate_name'];
				$entity->buyer_evaluate_name= $item['buyer_evaluate_name'];

				$entity->evaluate_count= $item['evaluate_count'];
				$entity->evaluate_point= $item['evaluate_point'];

				$entity->order_date= $item['order_date'];
				$entity->show_evaluate= $item['show_evaluate'];
				$entity->with_delivery= $item['with_delivery'];

				$entity->owner->select($entity->owner_id);

				$this->free_input = ItemFreeInfo::selectFromItemId($entity->id);
				$result[] = $entity;
			}
		}
		return $result;
	}

	/**
	 * 全件検索
	 * @return Item[]
	 */
	public function selectCategory($category) {
		$db = new DB();
		$param = array(
				':category' => $category
		);

		$result = array();
		$list = $db->query('selectItemCategory', $param);
		if (is_array($list) && count($list) > 0) {
			foreach ($list as $item) {
				$entity = new Item();
				$entity->id = $item['id'];
				$entity->access_count= $item['access_count'];
				$entity->owner_id= $item['owner_id'];
				$entity->title= $item['title'];
				$entity->category= $item['category'];
				$entity->item_state= $item['item_state'];
				$entity->remarks= $item['remarks'];
				$entity->carriage_type= $item['carriage_type'];
				$entity->carriage_plan= $item['carriage_plan'];
				$entity->delivery_type= $item['delivery_type'];
				$entity->price= $item['price'];
				$entity->stock= $item['stock'];
				$entity->photo= $item['photo'];
				$entity->enabled= $item['enabled'];
				$entity->owner_name= $item['owner_name'];
				$entity->owner_hash_id= $item['owner_hash_id'];
				$entity->owner_selling= $item['owner_selling'];

				$entity->regist_date= $item['regist_date'];
				$entity->update_date= $item['update_date'];

				$entity->item_state_name= $item['item_state_name'];
				$entity->carriage_plan_name= $item['carriage_plan_name'];
				$entity->carriage_type_name= $item['carriage_type_name'];
				$entity->delivery_type_name= $item['delivery_type_name'];
				$entity->category_name= $item['category_name'];

				$entity->buyer_id= $item['buyer_id'];
				$entity->order_id= $item['order_id'];
				$entity->seller_evaluate= $item['seller_evaluate'];
				$entity->buyer_evaluate= $item['buyer_evaluate'];
				$entity->seller_evaluate_name= $item['seller_evaluate_name'];
				$entity->buyer_evaluate_name= $item['buyer_evaluate_name'];

				$entity->evaluate_count= $item['evaluate_count'];
				$entity->evaluate_point= $item['evaluate_point'];

				$entity->order_date= $item['order_date'];
				$entity->show_evaluate= $item['show_evaluate'];
				$entity->with_delivery= $item['with_delivery'];

				$entity->owner->select($entity->owner_id);

				$this->free_input = FreeInputItemValue::selectFromFreeInputItemId($entity->id);
				$result[] = $entity;
			}
		}
		return $result;
	}

	/**
	 * 詳細検索
	 * @param unknown $id
	 * @return boolean
	 */
	public function select($id) {
		$db = new DB();
		$param = array(
				':id' => $id
		);

		$list = $db->query('selectItem', $param);
		if (is_array($list) && count($list) > 0) {
			foreach ($list as $item) {
				$this->id = $item['id'];
				$this->access_count= $item['access_count'];
				$this->owner_id= $item['owner_id'];
				$this->title= $item['title'];
				$this->category= $item['category'];
				$this->item_state= $item['item_state'];
				$this->remarks= $item['remarks'];
				$this->pref= $item['pref'];
				$this->carriage_type= $item['carriage_type'];
				$this->carriage_plan= $item['carriage_plan'];
				$this->delivery_type= $item['delivery_type'];
				$this->price= $item['price'];
				$this->stock= $item['stock'];
				$this->photo= $item['photo'];
				$this->enabled= $item['enabled'];
				$this->owner_name= $item['owner_name'];
				$this->owner_hash_id= $item['owner_hash_id'];
				$this->owner_selling= $item['owner_selling'];

				$this->regist_date= $item['regist_date'];
				$this->update_date= $item['update_date'];

				$this->item_state_name= $item['item_state_name'];
				$this->carriage_plan_name= $item['carriage_plan_name'];
				$this->carriage_type_name= $item['carriage_type_name'];
				$this->delivery_type_name= $item['delivery_type_name'];
				$this->category_name= $item['category_name'];

				$this->buyer_id= $item['buyer_id'];
				$this->order_id= $item['order_id'];
				$this->seller_evaluate= $item['seller_evaluate'];
				$this->buyer_evaluate= $item['buyer_evaluate'];
				$this->seller_evaluate_name= $item['seller_evaluate_name'];
				$this->buyer_evaluate_name= $item['buyer_evaluate_name'];

				$this->evaluate_count= $item['evaluate_count'];
				$this->evaluate_point= $item['evaluate_point'];
				$this->show_evaluate= $item['show_evaluate'];
				$this->with_delivery= $item['with_delivery'];

				$this->groupCode= $item['groupCode'];
				$this->groupObject = $item['groupObject'];
				$this->free_input = ItemFreeInfo::selectFromItemId($this->id);

				$this->repairName();

				return true;
			}
		}
		return false;
	}

	/**
	 * 詳細（下書き）検索
	 * @param unknown $id
	 * @return boolean
	 */
	public function selectTmp($id) {
		$db = new DB();
		$param = array(
				':id' => $id
		);

		$list = $db->query('selectItemTmp', $param);
		if (is_array($list) && count($list) > 0) {
			foreach ($list as $item) {
				$this->id = $item['id'];
				$this->owner_id= $item['owner_id'];
				$this->title= $item['title'];
				$this->category= $item['category'];
				$this->item_state= $item['item_state'];
				$this->remarks= $item['remarks'];
				$this->pref= $item['pref'];
				$this->carriage_type= $item['carriage_type'];
				$this->carriage_plan= $item['carriage_plan'];
				$this->delivery_type= $item['delivery_type'];
				$this->price= $item['price'];
				$this->stock= $item['stock'];
				$this->photo= $item['photo'];
				$this->enabled= $item['enabled'];
				$this->owner_name= $item['owner_name'];
				$this->owner_hash_id= $item['owner_hash_id'];
				$this->owner_selling= $item['owner_selling'];

				$this->regist_date= $item['regist_date'];
				$this->update_date= $item['update_date'];

				$this->item_state_name= $item['item_state_name'];
				$this->carriage_plan_name= $item['carriage_plan_name'];
				$this->carriage_type_name= $item['carriage_type_name'];
				$this->delivery_type_name= $item['delivery_type_name'];
				$this->category_name= $item['category_name'];

				$this->buyer_id= $item['buyer_id'];
				$this->order_id= $item['order_id'];
				$this->seller_evaluate= $item['seller_evaluate'];
				$this->buyer_evaluate= $item['buyer_evaluate'];
				$this->seller_evaluate_name= $item['seller_evaluate_name'];
				$this->buyer_evaluate_name= $item['buyer_evaluate_name'];

				$this->evaluate_count= $item['evaluate_count'];
				$this->evaluate_point= $item['evaluate_point'];
				$this->show_evaluate= $item['show_evaluate'];
				$this->with_delivery= $item['with_delivery'];

				return true;
			}
		}
		return false;
	}

	/**
	 * 詳細（下書き）検索
	 * @param unknown $id
	 * @return boolean
	 */
	public function selectTmpFromUser($owner_id) {
		$db = new DB();
		$param = array(
				':owner_id' => $owner_id
		);

		$result = array();
		$list = $db->query('selectItemTmpFromUserId', $param);
		if (is_array($list) && count($list) > 0) {
			foreach ($list as $item) {
				$entity = new Item();
				$entity->id = $item['id'];
				$entity->owner_id= $item['owner_id'];
				$entity->title= $item['title'];
				$entity->category= $item['category'];
				$entity->item_state= $item['item_state'];
				$entity->remarks= $item['remarks'];
				$entity->carriage_type= $item['carriage_type'];
				$entity->carriage_plan= $item['carriage_plan'];
				$entity->delivery_type= $item['delivery_type'];
				$entity->price= $item['price'];
				$entity->stock= $item['stock'];
				$entity->photo= $item['photo'];
				$entity->enabled= $item['enabled'];
				$entity->owner_name= $item['owner_name'];
				$entity->owner_hash_id= $item['owner_hash_id'];
				$entity->owner_selling= $item['owner_selling'];

				$entity->regist_date= $item['regist_date'];
				$entity->update_date= $item['update_date'];

				$entity->item_state_name= $item['item_state_name'];
				$entity->carriage_plan_name= $item['carriage_plan_name'];
				$entity->carriage_type_name= $item['carriage_type_name'];
				$entity->delivery_type_name= $item['delivery_type_name'];
				$entity->category_name= $item['category_name'];

				$entity->buyer_id= $item['buyer_id'];
				$entity->order_id= $item['order_id'];
				$entity->seller_evaluate= $item['seller_evaluate'];
				$entity->buyer_evaluate= $item['buyer_evaluate'];
				$entity->seller_evaluate_name= $item['seller_evaluate_name'];
				$entity->buyer_evaluate_name= $item['buyer_evaluate_name'];

				$entity->evaluate_count= $item['evaluate_count'];
				$entity->evaluate_point= $item['evaluate_point'];
				$entity->show_evaluate= $item['show_evaluate'];
				$entity->with_delivery= $item['with_delivery'];

				$result[] = $entity;
			}
		}
		return $result;
	}

	/**
	 * 販売中商品一覧
	 * @param unknown $user_id
	 * @return Item[]
	 */
	public function selectSell($user_id) {
		$db = new DB();
		if (empty($user_id)) {
			$user_id = getUserId();
		}
		$param = array(
				':owner_id' => $user_id
		);

		$result = array();
		$list = $db->query('selectSellItem', $param);
		if (is_array($list) && count($list) > 0) {
			foreach ($list as $item) {
				$entity = new Item();
				$entity->id = $item['id'];
				$entity->access_count= $item['access_count'];
				$entity->owner_id= $item['owner_id'];
				$entity->title= $item['title'];
				$entity->category= $item['category'];
				$entity->item_state= $item['item_state'];
				$entity->remarks= $item['remarks'];
				$entity->carriage_type= $item['carriage_type'];
				$entity->carriage_plan= $item['carriage_plan'];
				$entity->delivery_type= $item['delivery_type'];
				$entity->price= $item['price'];
				$entity->stock= $item['stock'];
				$entity->photo= $item['photo'];
				$entity->enabled= $item['enabled'];

				$entity->regist_date= $item['regist_date'];
				$entity->update_date= $item['update_date'];

				$entity->item_state_name= $item['item_state_name'];
				$entity->carriage_plan_name= $item['carriage_plan_name'];
				$entity->carriage_type_name= $item['carriage_type_name'];
				$entity->delivery_type_name= $item['delivery_type_name'];
				$entity->category_name= $item['category_name'];

				$entity->negotiation_count = $item['negotiation_count'];
				$entity->last_date = $item['last_date'];

				$entity->buyer_id= $item['buyer_id'];
				$entity->order_id= $item['order_id'];
				$entity->seller_evaluate= $item['seller_evaluate'];
				$entity->buyer_evaluate= $item['buyer_evaluate'];
				$entity->seller_evaluate_name= $item['seller_evaluate_name'];
				$entity->buyer_evaluate_name= $item['buyer_evaluate_name'];
// 				$entity->seller_evaluate_hash_id= $item['seller_evaluate_hash_id'];
// 				$entity->buyer_evaluate_hash_id= $item['buyer_evaluate_hash_id'];

				$entity->evaluate_count= $item['evaluate_count'];
				$entity->evaluate_point= $item['evaluate_point'];

				$entity->order_date= $item['order_date'];
				$entity->show_evaluate= $item['show_evaluate'];
				$entity->with_delivery= $item['with_delivery'];

				$this->free_input = ItemFreeInfo::selectFromItemId($entity->id);
				$result[] = $entity;
			}
		}
		return $result;
	}

	public function selectSellByUser($user_id) {
		$db = new DB();
		if (empty($user_id)) {
			$user_id = getUserId();
		}
		$param = array(
				':owner_id' => $user_id
		);

		$result = array();
		$list = $db->query('selectSellItemUser', $param);
		if (is_array($list) && count($list) > 0) {
			foreach ($list as $item) {
				$entity = new Item();
				$entity->id = $item['id'];
				$entity->access_count= $item['access_count'];
				$entity->owner_id= $item['owner_id'];
				$entity->title= $item['title'];
				$entity->category= $item['category'];
				$entity->item_state= $item['item_state'];
				$entity->remarks= $item['remarks'];
				$entity->carriage_type= $item['carriage_type'];
				$entity->carriage_plan= $item['carriage_plan'];
				$entity->delivery_type= $item['delivery_type'];
				$entity->price= $item['price'];
				$entity->stock= $item['stock'];
				$entity->photo= $item['photo'];
				$entity->enabled= $item['enabled'];

				$entity->regist_date= $item['regist_date'];
				$entity->update_date= $item['update_date'];

				$entity->item_state_name= $item['item_state_name'];
				$entity->carriage_plan_name= $item['carriage_plan_name'];
				$entity->carriage_type_name= $item['carriage_type_name'];
				$entity->delivery_type_name= $item['delivery_type_name'];
				$entity->category_name= $item['category_name'];

				$entity->negotiation_count = $item['negotiation_count'];
				$entity->last_date = $item['last_date'];

				$entity->buyer_id= $item['buyer_id'];
				$entity->order_id= $item['order_id'];
				$entity->seller_evaluate= $item['seller_evaluate'];
				$entity->buyer_evaluate= $item['buyer_evaluate'];
				$entity->seller_evaluate_name= $item['seller_evaluate_name'];
				$entity->buyer_evaluate_name= $item['buyer_evaluate_name'];
// 				$entity->seller_evaluate_hash_id= $item['seller_evaluate_hash_id'];
// 				$entity->buyer_evaluate_hash_id= $item['buyer_evaluate_hash_id'];

				$entity->evaluate_count= $item['evaluate_count'];
				$entity->evaluate_point= $item['evaluate_point'];

				$entity->order_date= $item['order_date'];
				$entity->show_evaluate= $item['show_evaluate'];
				$entity->with_delivery= $item['with_delivery'];

				$this->free_input = ItemFreeInfo::selectFromItemId($entity->id);
				$result[] = $entity;
			}
		}
		return $result;
	}

	public function selectAllSellItemByUserID($user_id) {
		$db = new DB();
		if (empty($user_id)) {
			$user_id = getUserId();
		}
		$param = array(
				':owner_id' => $user_id
		);

		$result = array();
		$list = $db->query('selectAllSellItemByUserID', $param);
		if (is_array($list) && count($list) > 0) {
			foreach ($list as $item) {
				$entity = new Item();
				$entity->id = $item['id'];
				$entity->access_count= $item['access_count'];
				$entity->owner_id= $item['owner_id'];
				$entity->title= $item['title'];
				$entity->category= $item['category'];
				$entity->item_state= $item['item_state'];
				$entity->remarks= $item['remarks'];
				$entity->carriage_type= $item['carriage_type'];
				$entity->carriage_plan= $item['carriage_plan'];
				$entity->delivery_type= $item['delivery_type'];
				$entity->price= $item['price'];
				$entity->stock= $item['stock'];
				$entity->photo= $item['photo'];
				$entity->enabled= $item['enabled'];
				$entity->owner_name= $item['owner_name'];
				$entity->owner_hash_id= $item['owner_hash_id'];
				$entity->owner_selling= $item['owner_selling'];

				$entity->regist_date= $item['regist_date'];
				$entity->update_date= $item['update_date'];

				$entity->item_state_name= $item['item_state_name'];
				$entity->carriage_plan_name= $item['carriage_plan_name'];
				$entity->carriage_type_name= $item['carriage_type_name'];
				$entity->delivery_type_name= $item['delivery_type_name'];
				$entity->category_name= $item['category_name'];

				$entity->buyer_id= $item['buyer_id'];
				$entity->order_id= $item['order_id'];
				$entity->seller_evaluate= $item['seller_evaluate'];
				$entity->buyer_evaluate= $item['buyer_evaluate'];
				$entity->seller_evaluate_name= $item['seller_evaluate_name'];
				$entity->buyer_evaluate_name= $item['buyer_evaluate_name'];

				$entity->evaluate_count= $item['evaluate_count'];
				$entity->evaluate_point= $item['evaluate_point'];

				$entity->order_date= $item['order_date'];
				$entity->show_evaluate= $item['show_evaluate'];
				$entity->with_delivery= $item['with_delivery'];

				$entity->owner->select($entity->owner_id);

				$this->free_input = ItemFreeInfo::selectFromItemId($entity->id);
				$result[] = $entity;
			}
		}
		return $result;
	}

	public function selectAllSellItemsNotSoldByUserID($user_id) {
		$db = new DB();
		if (empty($user_id)) {
			$user_id = getUserId();
		}
		$param = array(
				':owner_id' => $user_id
		);

		$result = array();
		$list = $db->query('selectAllSellItemsNotSoldByUserID', $param);
		if (is_array($list) && count($list) > 0) {
			foreach ($list as $item) {
				$entity = new Item();
				$entity->id = $item['id'];
				$entity->access_count= $item['access_count'];
				$entity->owner_id= $item['owner_id'];
				$entity->title= $item['title'];
				$entity->category= $item['category'];
				$entity->item_state= $item['item_state'];
				$entity->remarks= $item['remarks'];
				$entity->carriage_type= $item['carriage_type'];
				$entity->carriage_plan= $item['carriage_plan'];
				$entity->delivery_type= $item['delivery_type'];
				$entity->price= $item['price'];
				$entity->stock= $item['stock'];
				$entity->photo= $item['photo'];
				$entity->enabled= $item['enabled'];

				$entity->regist_date= $item['regist_date'];
				$entity->update_date= $item['update_date'];

				$entity->owner->select($entity->owner_id);

				$this->free_input = ItemFreeInfo::selectFromItemId($entity->id);
				$result[] = $entity;
			}
		}
		return $result;
	}

	/**
	 * 交渉中商品一覧
	 * @param unknown $user_id
	 * @return Item[]
	 */
	public function selectNegotiation($user_id) {
		$db = new DB();
		if (empty($user_id)) {
			$user_id = getUserId();
		}
		$param = array(
				':user_id' => $user_id
		);

		$result = array();
		$list = $db->query('selectNegotiationItem', $param);
		if (is_array($list) && count($list) > 0) {
			foreach ($list as $item) {
				$entity = new Item();
				$entity->id = $item['id'];
				$entity->access_count= $item['access_count'];
				$entity->owner_id= $item['owner_id'];
				$entity->title= $item['title'];
				$entity->category= $item['category'];
				$entity->item_state= $item['item_state'];
				$entity->remarks= $item['remarks'];
				$entity->carriage_type= $item['carriage_type'];
				$entity->carriage_plan= $item['carriage_plan'];
				$entity->delivery_type= $item['delivery_type'];
				$entity->price= $item['price'];
				$entity->stock= $item['stock'];
				$entity->photo= $item['photo'];
				$entity->enabled= $item['enabled'];

				$entity->regist_date= $item['regist_date'];
				$entity->update_date= $item['update_date'];

				$entity->item_state_name= $item['item_state_name'];
				$entity->carriage_plan_name= $item['carriage_plan_name'];
				$entity->carriage_type_name= $item['carriage_type_name'];
				$entity->delivery_type_name= $item['delivery_type_name'];
				$entity->category_name= $item['category_name'];

				$entity->negotiation_count = $item['negotiation_count'];
				$entity->last_date = $item['last_date'];

				$entity->buyer_id= $item['buyer_id'];
				$entity->order_id= $item['order_id'];
				$entity->seller_evaluate= $item['seller_evaluate'];
				$entity->buyer_evaluate= $item['buyer_evaluate'];
				$entity->seller_evaluate_name= $item['seller_evaluate_name'];
				$entity->buyer_evaluate_name= $item['buyer_evaluate_name'];
				$entity->show_evaluate= $item['show_evaluate'];
				$entity->with_delivery= $item['with_delivery'];

				$this->free_input = ItemFreeInfo::selectFromItemId($entity->id);
				$result[] = $entity;
			}
		}
		return $result;
	}

	/**
	 * 売約済み商品検索
	 * @return Item[]
	 */
	public function selectBuy() {
		$db = new DB();
		$param = array(
				':user_id' => getUserId()
		);

		$result = array();
		$list = $db->query('selectBuyItem', $param);
		if (is_array($list) && count($list) > 0) {
			foreach ($list as $item) {
				$entity = new Item();
				$entity->id = $item['id'];
				$entity->access_count= $item['access_count'];
				$entity->owner_id= $item['owner_id'];
				$entity->title= $item['title'];
				$entity->category= $item['category'];
				$entity->item_state= $item['item_state'];
				$entity->remarks= $item['remarks'];
				$entity->carriage_type= $item['carriage_type'];
				$entity->carriage_plan= $item['carriage_plan'];
				$entity->delivery_type= $item['delivery_type'];
				$entity->price= $item['price'];
				$entity->stock= $item['stock'];
				$entity->photo= $item['photo'];
				$entity->enabled= $item['enabled'];

				$entity->regist_date= $item['regist_date'];
				$entity->update_date= $item['update_date'];

				$entity->item_state_name= $item['item_state_name'];
				$entity->carriage_plan_name= $item['carriage_plan_name'];
				$entity->carriage_type_name= $item['carriage_type_name'];
				$entity->delivery_type_name= $item['delivery_type_name'];
				$entity->category_name= $item['category_name'];

				$entity->negotiation_count = $item['negotiation_count'];
				$entity->last_date = $item['last_date'];

				$entity->buyer_id= $item['buyer_id'];
				$entity->order_id= $item['order_id'];
				$entity->seller_evaluate= $item['seller_evaluate'];
				$entity->buyer_evaluate= $item['buyer_evaluate'];
				$entity->seller_evaluate_name= $item['seller_evaluate_name'];
				$entity->buyer_evaluate_name= $item['buyer_evaluate_name'];

				$entity->order_date= $item['order_date'];
				$entity->show_evaluate= $item['show_evaluate'];
				$entity->with_delivery= $item['with_delivery'];

				$this->free_input = ItemFreeInfo::selectFromItemId($entity->id);
				if (empty($entity->last_date)) {
					$entity->last_date = $entity->order_date;
				}
				$result[] = $entity;
			}
		}
		return $result;
	}

	/**
	 * お気に入り検索
	 * @return Item[]
	 */
	public function selectFavorites() {
		$db = new DB();

		$result = array();
		$param = array(
				':user_id' => getUserId()
		);
		$list = $db->query('selectFavorites', $param);
		if (is_array($list) && count($list) > 0) {
			foreach ($list as $item) {
				$entity = new Item();
				$entity->id = $item['id'];
				$entity->owner_id= $item['owner_id'];
				$entity->title= $item['title'];
				$entity->category= $item['category'];
				$entity->item_state= $item['item_state'];
				$entity->remarks= $item['remarks'];
				$entity->carriage_type= $item['carriage_type'];
				$entity->carriage_plan= $item['carriage_plan'];
				$entity->delivery_type= $item['delivery_type'];
				$entity->price= $item['price'];
				$entity->stock= $item['stock'];
				$entity->photo= $item['photo'];
				$entity->enabled= $item['enabled'];
				$entity->owner_name= $item['owner_name'];
				$entity->owner_hash_id= $item['owner_hash_id'];
				$entity->owner_selling= $item['owner_selling'];

				$entity->regist_date= $item['regist_date'];
				$entity->update_date= $item['update_date'];

				$entity->item_state_name= $item['item_state_name'];
				$entity->carriage_plan_name= $item['carriage_plan_name'];
				$entity->carriage_type_name= $item['carriage_type_name'];
				$entity->delivery_type_name= $item['delivery_type_name'];
				$entity->category_name= $item['category_name'];

				$entity->buyer_id= $item['buyer_id'];
				$entity->order_id= $item['order_id'];
				$entity->seller_evaluate= $item['seller_evaluate'];
				$entity->buyer_evaluate= $item['buyer_evaluate'];
				$entity->seller_evaluate_name= $item['seller_evaluate_name'];
				$entity->buyer_evaluate_name= $item['buyer_evaluate_name'];

				$entity->evaluate_count= $item['evaluate_count'];
				$entity->evaluate_point= $item['evaluate_point'];

				$entity->order_date= $item['order_date'];
				$entity->show_evaluate= $item['show_evaluate'];
				$entity->with_delivery= $item['with_delivery'];

				$entity->favorites_regist_date= $item['favorites_regist_date'];
				$entity->favorites_update_date= $item['favorites_update_date'];

				$this->free_input = ItemFreeInfo::selectFromItemId($entity->id);

				$result[] = $entity;
			}
		}
		return $result;
	}

	/**
	 * アクセス数カウントアップ
	 * @param unknown $id
	 */
	public static function updateItemAccessCount($id) {
		$db = new DB();
		$param = array(
				':id' => $id
		);
		$db->execute('updateItemAccessCount', $param);
	}

	/**
	 * いいね件数更新
	 * @param unknown $item_id
	 * @return boolean
	 */
	public static function updateItemGoodCountUp($item_id) {

		$item = Item::selectItemGood($item_id);
		if ($item->good_count > 0 && diffDays(date("Y/m/d"), $item->update_date) <= 0) {
			return false;
		}

		$db = new DB();
		$param = array(
				':user_id' => getUserId(),
				':item_id' => $item_id
		);
		$db->execute('updateItemGoodCountUp', $param);
		return true;
	}

	/**
	 * いいね検索
	 * @param unknown $item_id
	 * @return Item
	 */
	public static function selectItemGood($item_id) {
		$db = new DB();

		$param = array(
				':user_id' => getUserId(),
				':item_id' => $item_id
		);
		$list = $db->query('selectItemGood', $param);
		$result = new Item();
		if (is_array($list) && count($list) > 0) {
			foreach ($list as $item) {
				$result->good_count= $item['good_count'];
				$result->regist_date= $item['regist_date'];
				$result->update_date= $item['update_date'];
				break;
			}
		}
		return $result;
	}

	/**
	 * いいね件数検索
	 * @param unknown $item_id
	 * @return unknown|number
	 */
	public static function selectItemGoodCount($item_id) {
		$db = new DB();

		$param = array(
				':user_id' => getUserId(),
				':item_id' => $item_id
		);
		$list = $db->query('selectItemGood', $param);
		if (is_array($list) && count($list) > 0) {
			foreach ($list as $item) {
				return $item['good_count'];
			}
		}
		return 0;
	}

	/**
	 * いいね件数合計検索
	 * @param unknown $item_id
	 * @return unknown|number
	 */
	public static function selectItemGoodCountSummary($item_id) {
		$db = new DB();

		$param = array(
				':item_id' => $item_id
		);
		$result = 0;
		$list = $db->query('selectItemGood2', $param);
		if (is_array($list) && count($list) > 0) {
			foreach ($list as $item) {
				$result += $item['good_count'];
			}
		}
		return $result;
	}



	/**
	 * お気に入り有無
	 */
	public function hasFavorites() {
		$db = new DB();

		$param = array(
				':user_id' => getUserId(),
				':item_id' => $this->id
		);
		$list = $db->query('hasFavorites', $param);
		return is_array($list) && count($list) > 0;
	}

	/**
	 * お気に入り登録
	 */
	public function registFavorites() {
		$db = new DB();
		$param = array(
				':user_id' => getUserId(),
				':item_id' => $this->id
		);
		$db->execute('registFavorites', $param);
	}

	/**
	 * お気に入り削除
	 */
	public function deleteFavorites() {
		$db = new DB();
		$param = array(
				':user_id' => getUserId(),
				':item_id' => $this->id
		);
		$db->execute('deleteFavorites', $param);
	}

	/**
	 * お気に入り件数
	 */
	public function selectCountFavorites() {
		$db = new DB();
		$param = array(
				':item_id' => $this->id
		);
		$list = $db->query('selectCountFavorites', $param);
		if (is_array($list) && count($list) > 0) {
			foreach ($list as $item) {
				return $item['count_favorites'];
			}
		}
		return 0;
	}

	/**
	 * ブロックチェック
	 */
	public function isBlock() {
		if (getUserId() == $this->owner_id) {
			return false;
		}

		return BlockAccess::isBlock($this->owner_id)
		|| !$this->isTargetUser();
	}

	public function isTargetUser() {
		if (!isLogin()) {
			return true;
		}
		if ($this->owner_id == getUserId()) {
			return true;
		}
		if (!$this->show_evaluate) {
			return true;
		}

		$negotiation = new Negotiation();
		$negotiation->item_id = $this->id;
		$negotiation->user_id = getUserId();
		if (count($negotiation->select()) > 0) {
			return true;
		}

		$evaluate = new Evaluate();
		$evaluate->selectSummary(getUserId());

		return $evaluate->point >= 0;
	}

	public function repairName() {
		$db = new DB();
		$param = array(
				':id' => $this->category
		);
		$list = $db->query('selectCategoryById', $param);
		foreach ($list as $item) {
			$this->category_name = $item['name'];
		}
		$param = array(
				':id' => $this->item_state
		);
		$list = $db->query('selectItemStateById', $param);
		foreach ($list as $item) {
			$this->item_state_name= $item['name'];
		}

		$param = array(
				':id' => $this->carriage_plan
		);
		$list = $db->query('selectCarriagePlanById', $param);
		foreach ($list as $item) {
			$this->carriage_plan_name= $item['name'];
		}

		$param = array(
				':id' => $this->carriage_type
		);
		$list = $db->query('selectCarriageTypeById', $param);
		foreach ($list as $item) {
			$this->carriage_type_name= $item['name'];
		}

		$param = array(
				':id' => $this->delivery_type
		);
		$list = $db->query('selectDeliveryTypeById', $param);
		foreach ($list as $item) {
			$this->delivery_type_name= $item['name'];
		}

		$param = array(
				':id' => $this->pref
		);
		$list = $db->query('selectPrefById', $param);
		foreach ($list as $item) {
			$this->pref_name= $item['name'];
		}
	}

	public function getPhoto($id, $number) {
		$db = new DB();
		$param = array(
				':item_id' => $id,
				':number' => $number
		);
		$list = $db->query('selectItemPhoto', $param);
		foreach ($list as $item) {
			if ($item['photo'] == null) {
				return file_get_contents(__DIR__.'/../'.NOIMAGE_PATH);
			}
			return $item['photo'];
		}

		return file_get_contents(__DIR__.'/../'.NOIMAGE_PATH);
	}

	/**
	 * バリデーション
	 * @return unknown
	 */
	public function validate() {
		$msg = '';
		$system_config = SystemConfig::select();
		if (empty($this->title)) {
			$msg .= '[タイトル]';
		}
		if ($system_config->enabled_category && empty($this->category)) {
			$msg .= '[カテゴリー]';
		}
		if ($system_config->enabled_item_state && empty($this->item_state)) {
			$msg .= '[商品の状態]';
		}
		if (empty($this->remarks)) {
			$msg .= '[説明文]';
		}
		if ($this->with_delivery && $system_config->enabled_postage && empty($this->carriage_plan)) {
			$msg .= '[配送料の負担]';
		}
		if ($this->with_delivery && $system_config->enabled_carriage_type && empty($this->carriage_type)) {
			$msg .= '[配送の目安]';
		}
		if ($this->with_delivery && $system_config->enabled_delivery_type && empty($this->delivery_type)) {
			$msg .= '[配送方法]';
		}
// 		if ($this->delivery_price == '') {
// 			$msg .= '[配送料金]';
// 		}
		if (empty($this->price)) {
			$msg .= '[商品価格]';
		}
		if (empty($this->photo1)) {
			$msg .= '[商品画像１]';
		}

		if (!empty($msg)) {
			$msg .= 'は必須項目です。';
			setMessage($msg);
		}

		return empty($msg);
	}

	/**
	 * バリデーション（編集）
	 * @return unknown
	 */
	public function validateModify() {
		$msg = '';
		if (empty($this->price)) {
			$msg .= '[商品価格]';
		}

		if (!empty($msg)) {
			$msg .= 'は必須項目です。';
			setMessage($msg);
		}

		return empty($msg);
	}

	/**
	 * ブロックメッセージスクリプト生成
	 * @return string|unknown
	 */
	public function createBlockScript($target_user_id = 0) {
		$msg = '';
		if (empty($target_user_id)) {
			$target_user_id = getUserId();
		}
		if ($this->enabled == 9) {
			$msg = '出品が取り消されているため閲覧できません。';
		}
		if (BlockAccess::isBlock($target_user_id)) {
			$msg = 'このユーザーと取引できません。';
		}

		if (empty($msg)) {
			return '';
		}

		return ' onclick="window.alert(\''.$msg.'\'); return false;"'
				. ' onkeypress="window.alert(\''.$msg.'\'); return false;"';
	}

	/**
	 * 状態名取得
	 */
	public function getStatusText() {
		switch ($this->enabled) {
			case 0:
				return '出品待ち';
			case 1:
				return '出品中';
			case 2:
				return '出品却下';
			case 3:
				return '出品取消';
			default:
				break;
		}

		return '';
	}

	public function getFreeInfoName($id) {
	    if (!isset($this->free_input[$id])) {
	        return '-';
	    }
	    return FreeInputItemValue::select($this->free_input[$id]->free_input_item_value_id)->name;
	}

	public function getThumbnailUrl($index = 1) {
	    $filename = hash('sha256', 'thumbnail'.$this->id.$index);
	    return getContextRoot().'/images/item/id'.$this->id.'/'.$filename;
	}

	public function getThumnailPath($index = 1) {
	    $filename = hash('sha256', 'thumbnail'.$this->id.$index);
	    return __DIR__.'/../images/item/id'.$this->id.'/'.$filename;
	}

	public function getImageUrl($index) {
	    $filename = hash('sha256', 'image'.$this->id.$index);
	    return getContextRoot().'/images/item/id'.$this->id.'/'.$filename;
	}

	public function getImagePath($index) {
	    $filename = hash('sha256', 'image'.$this->id.$index);
	    return __DIR__.'/../images/item/id'.$this->id.'/'.$filename;
	}

	public function getImageAuthenUrl($index) {
	    $filename = hash('sha256', 'image'.$this->id.$index);
	    return getContextRoot().'/images/item/authen/id'.$this->id.'/'.$filename;
	}

	public function getImageAuthenPath($index) {
	    $filename = hash('sha256', 'image'.$this->id.$index);
	    return __DIR__.'/../images/item/authen/id'.$this->id.'/'.$filename;
	}

	static public function repairImageName() {
	    foreach (glob(__DIR__.'/../images/item/id*') as $dir) {
	        echo basename($dir);
	        $id = str_replace('id', '', basename($dir));
	        $item = new Item();
	        $item->select($id);
	        for ($i = 1; $i <= 4; $i++) {
	            $old_path = $dir.'/image'.$i.'.jpg';
	            if (file_exists($old_path)) {
	                $new_path = $item->getImagePath($i);
	                rename($old_path, $new_path);
	            }
	        }
	        $old_path = $dir.'/thumbnail.jpg';
	        if (file_exists($old_path)) {
	            $new_path = $item->getThumnailPath();
	            rename($old_path, $new_path);
	        }
	    }
	}

}