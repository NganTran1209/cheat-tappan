<?php
include_once(__DIR__.'/../common/util.php');

class CardInfo {
    public $user_id;
    public $token = '';
    public $customer_id = '';
    public $last_receipt_url = '';
    public $email = '';
    public $enabled_customer = '';
    public $regist_key = '';
    public $subscription_id = '';
    public $canceled_subscription = '';
    public $regist_date;
	public $update_date;

	/**
	 * カード情報登録
	 */
	public function regist() {
	    $db = new DB();
	    $param = array(
	        ':user_id' => $this->user_id,
	        ':token' => $this->token,
	        ':customer_id' => $this->customer_id,
	        ':email' => $this->email,
	        ':regist_key' => $this->regist_key,
	        ':enabled_customer' => 1,
	        ':canceled_subscription' => 0,
	    );
	    $db->execute('registCardInfo', $param);
	}

	/**
	 * 検索
	 * @param unknown $id
	 * @return boolean
	 */
	public function select($user_id) {
		$db = new DB();
		$param = array(
		    ':user_id' => $user_id
		);

		$list = $db->query('selectCardInfo', $param);
		if (is_array($list) && count($list) > 0) {
			foreach ($list as $item) {
			    $this->user_id = $item['user_id'];
			    $this->token = $item['token'];
			    $this->customer_id = $item['customer_id'];
			    $this->last_receipt_url = $item['last_receipt_url'];
			    $this->email = $item['email'];
			    $this->enabled_customer = $item['enabled_customer'];
			    $this->regist_key = $item['regist_key'];
			    $this->subscription_id = $item['subscription_id'];
			    $this->canceled_subscription = $item['canceled_subscription'];
			    $this->regist_date= $item['regist_date'];
				$this->update_date= $item['update_date'];
			}
			return true;
		}
		return false;
	}

	/**
	 * カード情報無効
	 */
	public function remove() {
	    $db = new DB();
	    $param = array(
	        ':user_id' => $this->user_id,
	    );
	    $db->execute('updateCardInfoForRemove', $param);
	}
}
