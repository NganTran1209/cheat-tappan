<?php
include_once(__DIR__.'/../common/db.php');
include_once(__DIR__.'/../common/mail.php');

class Invoice {
	public $id;
	public $user_id;
	public $month;
	public $price;
	public $transaction_count;
	public $transaction_amount;
	public $commission_rate;
	public $commission;
	public $amount;
	public $status;

	public $user_name;
	public $email;
	public $type;
	public $type_name;
	public $settle_plan;
	public $settle_plan_name;

	public $regist_date;
	public $update_date;

	public $details;

	public function select($id) {
		$db = new DB();
		$param = array(
				':id' => $id
		);

		$list = $db->query('selectInvoice', $param);
		if (is_array($list) && count($list) > 0) {
			foreach ($list as $item) {
				$this->id = $item['id'];
				$this->user_id= $item['user_id'];
				$this->month= $item['month'];
				$this->status= $item['status'];
				$this->price= $item['price'];
				$this->transaction_count= $item['transaction_count'];
				$this->transaction_amount= $item['transaction_amount'];
				$this->commission_rate= $item['commission_rate'];
				$this->commission= $item['commission'];
				$this->amount= $item['amount'];

				$this->user_name= $item['user_name'];
				$this->email= $item['email'];
				$this->type= $item['type'];
				$this->type_name= $item['type_name'];
				$this->settle_plan= $item['settle_plan'];
				$this->settle_plan_name= $item['settle_plan_name'];

				$this->regist_date= $item['regist_date'];
				$this->update_date= $item['update_date'];
			}
		}
	}

	public function selectMonth($month) {
		$db = new DB();
		$param = array(
				':month' => $month
		);

		$result = array();
		$list = $db->query('selectInvoiceByMonth', $param);
		if (is_array($list) && count($list) > 0) {
			foreach ($list as $item) {
				$entity = new Invoice();
				$entity->id = $item['id'];
				$entity->user_id= $item['user_id'];
				$entity->month= $item['month'];
				$entity->status= $item['status'];
				$entity->price= $item['price'];
				$entity->transaction_count= $item['transaction_count'];
				$entity->transaction_amount= $item['transaction_amount'];
				$entity->commission_rate= $item['commission_rate'];
				$entity->commission= $item['commission'];
				$entity->amount= $item['amount'];

				$entity->user_name= $item['user_name'];
				$entity->email= $item['email'];
				$entity->type= $item['type'];
				$entity->type_name= $item['type_name'];
				$entity->settle_plan= $item['settle_plan'];
				$entity->settle_plan_name= $item['settle_plan_name'];

				$entity->regist_date= $item['regist_date'];
				$entity->update_date= $item['update_date'];

				$result[] = $entity;
			}
		}
		return $result;
	}

	/**
	 * ユーザー検索
	 * @param unknown $month
	 * @return Invoice[]
	 */
	public function selectUser($user_id) {
		$db = new DB();
		$param = array(
				':user_id' => $user_id
		);

		$result = array();
		$list = $db->query('selectInvoiceByUser', $param);
		if (is_array($list) && count($list) > 0) {
			foreach ($list as $item) {
				$entity = new Invoice();
				$entity->id = $item['id'];
				$entity->user_id= $item['user_id'];
				$entity->month= $item['month'];
				$entity->status= $item['status'];
				$entity->price= $item['price'];
				$entity->transaction_count= $item['transaction_count'];
				$entity->transaction_amount= $item['transaction_amount'];
				$entity->commission_rate= $item['commission_rate'];
				$entity->commission= $item['commission'];
				$entity->amount= $item['amount'];

				$entity->user_name= $item['user_name'];
				$entity->email= $item['email'];
				$entity->type= $item['type'];
				$entity->type_name= $item['type_name'];
				$entity->settle_plan= $item['settle_plan'];
				$entity->settle_plan_name= $item['settle_plan_name'];

				$entity->regist_date= $item['regist_date'];
				$entity->update_date= $item['update_date'];

				$result[] = $entity;
			}
		}
		return $result;
	}

/**
	 * 請求情報生成
	 * @param unknown $month
	 * @return Invoice[]
	 */
	public function create($month) {
		$db = new DB();
		$invoiceDetails = new InvoiceDetails();
		$list = $db->query('selectInvoiceUser');
		if (is_array($list) && count($list) > 0) {
			foreach ($list as $item) {
				$entity = new Invoice();
				$entity->month = $month;
				$entity->user_id= $item['user_id'];

				$entity->user_name= $item['user_name'];
				$entity->email= $item['email'];
				$entity->type= $item['type'];
				$entity->type_name= $item['type_name'];
				$entity->settle_plan= $item['settle_plan'];
				$entity->settle_plan_name= $item['settle_plan_name'];

				if ($entity->settle_plan != 0) {
					$entity->price = $item['price'];
				} else {
					$entity->price = 0;
				}
				$entity->commission_rate = SystemConfig::select()->commission_rate;

				//$entity->details = $invoiceDetails->selectTransaction($month, $entity->user_id);
				$entity->details = $invoiceDetails->selectTransaction2($entity->user_id);
				$entity->transaction_count = count($entity->details);
				$amount = 0;
				$commission = 0;
				foreach ($entity->details as $details) {
					$amount += $details->price;
					$commission += floor($details->price * ($entity->commission_rate / 100));
				}

				$entity->transaction_amount = $amount;
				$entity->commission = $commission;
				$entity->amount = $entity->price + $entity->commission;

				if ($entity->amount == 0) {
					continue;
				}

				$entity->regist();
				$invoiceId = $db->lastInsertId();
				foreach ($entity->details as $details) {
					$details->invoice_id = $invoiceId;
					$details->regist();
				}

			}
		}
	}

	/**
	 * 登録
	 */
	public function regist() {
		$db = new DB();
		$param = array(
				':user_id' => $this->user_id,
				':month' => $this->month,
				':price' => $this->price,
				':transaction_count' => $this->transaction_count,
				':transaction_amount' => $this->transaction_amount,
				':commission_rate' => $this->commission_rate,
				':commission' => $this->commission,
				':amount' => $this->amount
		);
		$db->execute('insertInvoice', $param);
	}

	/**
	 * 一括メール送信
	 * @param unknown $month
	 */
	public function sendMailFromMonth($month) {
		$list = $this->selectMonth($month);
		foreach ($list as $item) {
			$item->sendMail();
		}
	}

	/**
	 * メール送信
	 */
	public function sendMail() {
	    $system_config = SystemConfig::select();
	    $mail = new Mail();
		$mail->to = $this->email;
		$mail->from = $system_config->from_email;
		$mail->fromName = $system_config->from_email_name;

		$template = MailTemplate::get('sendInvoice');
		$mail->subject = $template->subject;
		$mail->body = $template->replaceMessage(
				array(
						MailTemplate::REPLACE_USER_NAME => $this->user_name,
						MailTemplate::REPLACE_INVOICE_MONTH => toMonthJp($this->month),
						MailTemplate::REPLACE_AMOUNT => number_format($this->amount),
						MailTemplate::REPLACE_MONTHLY_FEE => number_format($this->price),
						MailTemplate::REPLACE_COMMISSION_FEE => number_format($this->commission),
						MailTemplate::REPLACE_SALES_AMOUNT => number_format($this->transaction_amount),
						MailTemplate::REPLACE_COMMISSION_RATE => $this->commission_rate,
						MailTemplate::REPLACE_SALES_COUNT=> $this->transaction_count
				)
				);
		$mail->sendMail();
		$this->status = 1;
		$this->updateStatus();
	}

	/**
	 * ステータス更新
	 */
	public function updateStatus() {
		$db = new DB();
		$param = array(
				':id' => $this->id,
				':status' => $this->status
		);
		$db->execute('updateInvoiceStatus', $param);
	}

	/**
	 * 状態名変換
	 * @param unknown $status
	 * @return string
	 */
	public static function toStatusText($status) {
		switch ($status) {
			case 0:
				return '請求準備中';
			case 1:
				return '請求中';
			case 2:
				return '入金済';
			default:
				break;
		}

		return '-';
	}

	/**
	 * 状態名変換（ユーザー向け）
	 * @param unknown $status
	 * @return string
	 */
	public static function toStatusUserText($status) {
		switch ($status) {
			case 1:
				return '未払';
			case 2:
				return '支払済';
			default:
				break;
		}

		return '-';
	}

	/**
	 * 請求金額取得
	 */
	public static function selectAmount($user_id) {
		$invoice = new Invoice();
		$list = $invoice->selectUser($user_id);
		$amount = 0;
		foreach ($list as $item) {
			if ($item->status == 1) {
				$amount += $item->amount;
			}
		}

		return $amount;
	}

	/**
	 * 請求情報削除
	 * @param unknown $month
	 */
	public static function delete($month) {
		$db = new DB();
		$param = array(
				':month' => $month
		);
		$db->execute('deleteInvoiceDetailsMonth', $param);
		$db->execute('deleteInvoiceMonth', $param);

	}

	/**
	 * 決済番号取得
	 * @return unknown
	 */
	private static function selectOrderNumber() {
		$db = new DB();
		$list = $db->query('selectOrderNumber');
		if (is_array($list) && count($list) > 0) {
			foreach ($list as $item) {
				$result = $item['order_number'];
				break;
			}
		}
		return $result;
	}

	/**
	 * 決済番号更新
	 * @param unknown $order_number
	 */
	private static function updateOrderNumber($order_number) {
		$db = new DB();
		$param = array(
				':order_number' => $order_number
		);
		$db->execute('updateOrderNumber', $param);
	}

	/**
	 * 決済番号生成
	 * @return unknown
	 */
	public static function createOrderNumber() {
		$order_number = Invoice::selectOrderNumber();
		$order_number++;
		Invoice::updateOrderNumber($order_number);
		return $order_number;
	}

	/**
	 * 決済番号登録
	 * @param unknown $order_number
	 */
	public static function registOrderNumber($order_number) {
		$db = new DB();
		$param = array(
				':user_id' => getUserId(),
				':order_number' => $order_number
		);
		$db->execute('registOrderNumber', $param);

	}

}
