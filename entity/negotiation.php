<?php
class Negotiation {
	public $id;
	public $item_id;
	public $user_id;
	public $comment;
	public $kbn;
	public $regist_date;
	public $update_date;
	public $order_date;

	public $start_date;
	public $last_date;
	public $negotiation_count;
	public $user_name;
	public $order_user_id;

	public $photo;
	public $pdf;

	public $isOffer = false;

	private $image_dir = '/images/negotiation/';
	private $image_file_name = '/image.jpg';

	/**
	 * 登録
	 */
	public function regist() {
		$db = new DB();
		$id = empty($this->id) || $id == 0 ? 0 : $this->id;
		$param = array(
				':user_id' => $this->user_id,
				':item_id' => $this->item_id,
				':comment' => $this->comment,
				':kbn' => $this->kbn,
				':id' => $id,
				':regist_date'=>$this->regist_date
		);

		$db->execute('registNegotiation', $param);

		if ($id == 0) {
			$this->id = $db->lastInsertId();
		}
		$this->registPhoto();
		$this->registPdf();

		if ($this->kbn == 1) {
			$this->sendInquiryMail();
		} else {
			$this->sendReplyMail();
		}
	}

	/**
	 * 画像登録
	 */
	private function registPhoto() {
		if ($this->photo == null) {
			return;
		}

		$dirpath = __DIR__.'/..'.$this->image_dir. 'id' . $this->id;
		if (!file_exists($dirpath)) {
			mkdir($dirpath, 0777, true);
		}

		$image_path = $this->getImagePath();
		$handler = fopen($image_path, 'w');
		$result = fwrite($handler, convertImage($this->photo));
		fclose($handler);

		if (!isImage($image_path)) {
			unlink($image_path);
		}

		return;
	}

	private function registPdf() {
	    if ($this->pdf == null) {
	        return;
	    }

	    $dirpath = __DIR__.'/..'.$this->image_dir. 'id' . $this->id;
	    if (!file_exists($dirpath)) {
	        mkdir($dirpath, 0777, true);
	    }

	    $pdf_path = $this->getPdfPath();
	    $handler = fopen($pdf_path, 'w');
	    $result = fwrite($handler, convertImage($this->pdf));
	    fclose($handler);

	    return;
	}

	/**
	 * 画像ファイル有無チェック
	 * @return boolean
	 */
	public function hasPhoto() {
	    $filename = hash('sha256', 'image'.$this->id);
	    $filepath = __DIR__.'/..'.$this->image_dir. 'id' . $this->id . '/' . $filename;
	    return file_exists($filepath);
	}

	public function hasPdf() {
	    $filename = hash('sha256', 'image'.$this->id) . '.pdf';
	    $filepath = __DIR__.'/..'.$this->image_dir. 'id' . $this->id . '/' . $filename;
		return file_exists($filepath);
	}

    /**
	 * 画像URL取得
	 * @return string
	 */
	public function getImageUrl() {
	    $filename = hash('sha256', 'image'.$this->id);
	    return getContextRoot().$this->image_dir. 'id' . $this->id . '/' . $filename;
	}

	public function getImagePath() {
	    $filename = hash('sha256', 'image'.$this->id);
	    return __DIR__.'/..'.$this->image_dir. 'id' . $this->id . '/' . $filename;
	}

	public function getPdfUrl() {
	    $filename = hash('sha256', 'image'.$this->id) . '.pdf';
	    return getContextRoot().$this->image_dir. 'id' . $this->id . '/' . $filename;
	}

	public function getPdfPath() {
	    $filename = hash('sha256', 'image'.$this->id) . '.pdf';
	    return __DIR__.'/..'.$this->image_dir. 'id' . $this->id . '/' . $filename;
	}

    /**
	 * 交渉検索
	 */
	public function select() {
		$db = new DB();
		$param = array(
				':item_id' => $this->item_id,
				':user_id' => $this->user_id
		);

		$result = array();
		$list = $db->query('selectNegotiation', $param);
		if (is_array($list) && count($list) > 0) {
			foreach ($list as $item) {
				$entity = new Negotiation();
				$entity->id = $item['id'];
				$entity->item_id= $item['item_id'];
				$entity->user_id= $item['user_id'];
				$entity->user_name= $item['user_name'];
				$entity->order_user_id= $item['order_user_id'];
				$entity->comment= $item['comment'];
				$entity->kbn= $item['kbn'];
				$entity->regist_date= $item['regist_date'];
				$entity->update_date= $item['update_date'];
				$entity->order_date= $item['order_date'];

				$result[] = $entity;
			}
		}
		return $result;

	}

	/**
	 * 交渉検索
	 */

	public function select2() {
		$db = new DB();
		$param = array(
				':item_id' => $this->item_id,
				':user_id' => $this->user_id
		);

		$before = array();
		$after = array();
		$list = $db->query('selectNegotiation', $param);
		if (is_array($list) && count($list) > 0) {
			foreach ($list as $item) {
				$entity = new Negotiation();
				$entity->id = $item['id'];
				$entity->item_id= $item['item_id'];
				$entity->user_id= $item['user_id'];
				$entity->user_name= $item['user_name'];
				$entity->order_user_id= $item['order_user_id'];
				$entity->comment= $item['comment'];
				$entity->kbn= $item['kbn'];
				$entity->regist_date= $item['regist_date'];
				$entity->update_date= $item['update_date'];
				$entity->order_date= $item['order_date'];

				if ($entity->hasOrder()) {
					$after[] = $entity;

				} else {
					$before[] = $entity;
				}
			}
		}
		return array($before, $after);

	}
	/**
	 * 販売交渉一覧
	 */
	public function selectSell() {
		$db = new DB();
		$param = array(
				':item_id' => $this->item_id
		);

		$result = array();
		$list = $db->query('selectSellNegotiation', $param);
		if (is_array($list) && count($list) > 0) {
			foreach ($list as $item) {
				$entity = new Negotiation();
				$entity->item_id= $item['item_id'];
				$entity->user_id= $item['user_id'];
				$entity->order_user_id= $item['order_user_id'];
				$entity->start_date= $item['start_date'];
				$entity->last_date= $item['last_date'];
				$entity->negotiation_count = $item['negotiation_count'];
				$entity->user_name= $item['user_name'];

				$result[] = $entity;
			}
		}
		return $result;

	}

	/**
	 * 注文後チェック
	 * @return boolean
	 */
	public function hasOrder() {
		if (!isset($this->order_date)) {
			return false;
		}

		return $this->order_date < $this->regist_date;
	}

	/**
	 * お問い合わせメール送信（未来の購入者？→出品者）
	 */
	public function sendInquiryMail() {

	    $system_config = SystemConfig::select();
	    $item = new Item();
		$item->select($this->item_id);
		$owner = new User();
		$owner->select($item->owner_id);
		$user = new User();
		$user->select($this->user_id);

		$mail = new Mail();
		$mail->to = $owner->email;
		$mail->from = $system_config->from_email;
		$mail->fromName = $system_config->from_email_name;

		$url = getContextRoot() . '/user/negotiation.php?item_id='.$this->item_id.'&user_id='.$this->user_id;

		$template = MailTemplate::get('sendInquiry');
//		if (RequestFollow::hasRequest(getUserId(), $this->item_id)) {
//			$template = MailTemplate::get('sendRequestInquiry');
//		}
		if (Order::selectFromItem($this->item_id)) {
			$template = MailTemplate::get('sendInquiryAfter');
		}
		$mail->subject = $template->replaceTitle(
				array(
						MailTemplate::REPLACE_TITLE=> $item->title,
				)
				);
		$mail->body = $template->replaceMessage(
				array(
						MailTemplate::REPLACE_USER_NAME => $user->name,
						MailTemplate::REPLACE_TITLE=> $item->title,
						MailTemplate::REPLACE_OWNER_NAME=> $owner->name,
						MailTemplate::REPLACE_CATEGORY_NAME=> $item->category_name,
						MailTemplate::REPLACE_NEGOTIATION_URL=> $url,
				)
				);
		$mail->sendMail();
	}

	/**
	 * 返信メール送信（出品者→購入者）
	 */
	public function sendReplyMail() {

	    $system_config = SystemConfig::select();
	    $item = new Item();
		$item->select($this->item_id);
		$owner = new User();
		$owner->select($item->owner_id);
		$user = new User();
		$user->select($this->user_id);

		$mail = new Mail();
		$mail->to = $user->email;
		$mail->from = $system_config->from_email;
		$mail->fromName = $system_config->from_email_name;

		$url = getContextRoot() . '/user/negotiation.php?item_id='.$this->item_id.'&user_id='.$this->user_id;

		$template = MailTemplate::get('sendReply');
		if ($this->isOffer && RequestFollow::hasRequest($this->user_id, $this->item_id)) {
			$template = MailTemplate::get('sendRequestReply');
		}
		if (Order::selectFromItem($this->item_id)) {
			$template = MailTemplate::get('sendReplyAfter');
		}

		$mail->subject = $template->replaceTitle(
				array(
						MailTemplate::REPLACE_TITLE=> $item->title,
				)
				);
		$mail->body = $template->replaceMessage(
				array(
						MailTemplate::REPLACE_USER_NAME => $user->name,
						MailTemplate::REPLACE_TITLE=> $item->title,
						MailTemplate::REPLACE_OWNER_NAME=> $owner->name,
						MailTemplate::REPLACE_CATEGORY_NAME=> $item->category_name,
						MailTemplate::REPLACE_NEGOTIATION_URL=> $url,
						MailTemplate::REPLACE_OWNER_HASH_ID=> $owner->hash_id,
				)
				);
		$mail->sendMail();
	}

	/**
	 * 交渉開始メール送信（購入者→購入者）
	 */
	public function sendBeginNegotiationMail($item_id) {

	    $system_config = SystemConfig::select();
	    $item = new Item();
		$item->select($item_id);
		$user = new User();
		$user->select(getUserId());

		$mail = new Mail();
		$mail->to = $user->email;
		$mail->from = $system_config->from_email;
		$mail->fromName = $system_config->from_email_name;

		$url = getContextRoot() . '/user/negotiation.php?item_id='.$item->id.'&user_id='.$user->id;

		$template = MailTemplate::get('sendBeginNegotiationMail');
		$mail->subject = $template->replaceTitle(
				array(
						MailTemplate::REPLACE_TITLE=> $item->title,
				)
				);
		$mail->body = $template->replaceMessage(
				array(
						MailTemplate::REPLACE_USER_NAME => $user->name,
						MailTemplate::REPLACE_TITLE=> $item->title,
						MailTemplate::REPLACE_CATEGORY_NAME=> $item->category_name,
						MailTemplate::REPLACE_NEGOTIATION_URL=> $url,
				)
				);
		$mail->sendMail();

		return [
		    'status' => true,
		    'url' => $url,
		];
	}

	public function select3() {
		$db = new DB();
		$param = array(
				':item_id' => $this->item_id
		);

		$before = array();
		$after = array();
		$list = $db->query('selectNegotiationAllItem', $param);
		if (is_array($list) && count($list) > 0) {
			foreach ($list as $item) {
				$entity = new Negotiation();
				$entity->id = $item['id'];
				$entity->item_id= $item['item_id'];
				$entity->user_id= $item['user_id'];
				$entity->user_name= $item['user_name'];
				$entity->order_user_id= $item['order_user_id'];
				$entity->comment= $item['comment'];
				$entity->kbn= $item['kbn'];
				$entity->regist_date= $item['regist_date'];
				$entity->update_date= $item['update_date'];
				$entity->order_date= $item['order_date'];

				if ($entity->hasOrder()) {
					$after[] = $entity;

				} else {
					$before[] = $entity;
				}
			}
		}
		return array($before, $after);

	}

}
