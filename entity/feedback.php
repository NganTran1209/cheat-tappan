<?php
class Feedback {
    public $id;
	public $item_id;
    public $user_id;
    public $order_id;
    public $score;
    public $regist_date;
    public $update_date;
	public $photo;
	public $photo2;
    private $image_dir = '/images/feedback/';
	private $image_file_name = '/image.jpg';
	public $index;

	public $item;
	public $user;
	public $state;

    function __construct() {
		$this->item = new Item();
		$this->user = new User();
		
	}
    /**
	 * 登録
	 */
	public function regist() {
		$db = new DB();
		$id = empty($this->id) || $id == 0 ? 0 : $this->id;
		$param = array(
				':user_id' => $this->user_id,
				':item_id' => $this->item_id,
				':order_id' => $this->order_id,
				':score' => $this->score,
				':index' => $this->index,
				':id' => $id
		);

		$db->execute('registFeedback', $param);

		if ($id == 0) {
			$this->id = $db->lastInsertId();
		}
		$this->registPhoto();
		//$this->sendFeedbackMail();
	}

	/**
	 * 画像登録
	 */
	private function registPhoto() {
		$dirpath = __DIR__.'/..'.$this->image_dir. 'id' . $this->id;
		if (!file_exists($dirpath)) {
			mkdir($dirpath, 0777, true);
		}

		if ($this->photo != null) {
		    $image_path = $dirpath . '/'.hash('sha256', 'image'.$this->id);
			$handler = fopen($image_path, 'w');
			$result = fwrite($handler, convertImage($this->photo, 1024, 1024));
			fclose($handler);


			$thumbnail_path = $dirpath . '/'.hash('sha256', 'thumbnail'.$this->id);
			$handler = fopen($thumbnail_path, 'w');
			$result = fwrite($handler, convertThumbnailImage($this->photo));
			fclose($handler);
		} else {
		    $image_path = $dirpath . '/'.hash('sha256', 'image'.$this->id);
		    if (file_exists($image_path)) {
				unlink($image_path);
			}
		}
    }

    public function hasPhoto($index) {
		$filename = hash('sha256', 'image'.$this->id.$index);
	    $filepath = getContextRoot().'/images/feedback/id'.$this->id.'/'.$filename;
	    return file_exists($filepath);
	}
    public function getImageUrl() {
	    $filename = hash('sha256', 'image'.$this->id);
	    return getContextRoot().'/images/feedback/id'.$this->id.'/'.$filename;
	}
    public function getImagePath() {
	    $filename = hash('sha256', 'image'.$this->id);
	    return __DIR__.'/../images/feedback/id'.$this->id.'/'.$filename;
	}
    public function compareRequestGaziru($groupCode, $imageFile){
		if(authenticateRequestAsync()){
			$params = [
				'ids' => $groupCode,
				'image' => $imageFile
			];
			$base = "https://teppan.gaziru-service.jp/v2/web/groups/objects/query";
			$curl = curl_init();
			curl_setopt($curl, CURLOPT_URL, $base);
			curl_setopt($curl, CURLOPT_POST, TRUE);
				
			curl_setopt($curl, CURLOPT_HTTPHEADER, [
				'Host: teppan.gaziru-service.jp',
				'Authorization: Bearer '. json_decode(authenticateRequestAsync(),true)["access_token"]
			]);
			curl_setopt($curl, CURLOPT_POSTFIELDS, $params);
			curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);
			curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
		
			$xml = curl_exec($curl);
			return $xml;
		}
    }
	
	public function compareRequestGaziruWithObject($objectCode, $imageFile){
		if(authenticateRequestAsync()){
			$params = [
				'ids' => $objectCode,
				'image' => $imageFile
			];
			$base = "https://teppan.gaziru-service.jp/v2/web/objects/query";
			$curl = curl_init();
			curl_setopt($curl, CURLOPT_URL, $base);
			curl_setopt($curl, CURLOPT_POST, TRUE);
				
			curl_setopt($curl, CURLOPT_HTTPHEADER, [
				'Host: teppan.gaziru-service.jp',
				'Authorization: Bearer '. json_decode(authenticateRequestAsync(),true)["access_token"]
			]);
			curl_setopt($curl, CURLOPT_POSTFIELDS, $params);
			curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);
			curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
		
			$xml = curl_exec($curl);
			return $xml;
		}
	}
	
    public function select($item_id, $user_id) {
		$db = new DB();
		$param = array(
				':item_id' => $item_id,
				':user_id' => $user_id
		);
        $result = array();
		$list = $db->query('selectFeedback', $param);
		if (is_array($list) && count($list) > 0) {
			foreach ($list as $item) {
				$this->id = $item['id'];
				$this->item_id= $item['item_id'];
				$this->user_id= $item['user_id'];
                $this->score= $item['score'];
                $this->order_id = $item['order_id'];
                $this->regist_date = $item['regist_date'];
                $this->update_date = $item['update_date'];

            }
            return true;
		}
		return false;

	}

	public function selectByOrderId($order_id) {
		$db = new DB();
		$param = array(
				':order_id' => $order_id
		);
        $result = array();
		$list = $db->query('selectFeedbackByOrderId', $param);
		if (is_array($list) && count($list) > 0) {
			foreach ($list as $item) {
				$this->id = $item['id'];
				$this->item_id= $item['item_id'];
				$this->user_id= $item['user_id'];
                $this->score= $item['score'];
                $this->order_id = $item['order_id'];
                $this->regist_date = $item['regist_date'];
                $this->update_date = $item['update_date'];

            }
		}
	}

	public function selectAll() {
		$db = new DB();
        $result = [];
		$list = $db->query('selectAllFeedback');
		if (is_array($list) && count($list) > 0) {
			foreach ($list as $item) {
				$entity = new Feedback();
				$entity->id = $item['id'];
				$entity->item_id= $item['item_id'];
				$entity->user_id= $item['user_id'];
                $entity->score= $item['score'];
                $entity->order_id = $item['order_id'];
                $entity->regist_date = $item['regist_date'];
                $entity->update_date = $item['update_date'];
				$entity->item->select($entity->item_id);
				$entity->user->select($entity->user_id);
				
				$result[] = $entity;
            }
		}
		return $result;
	}

	public function selectSearch($search) {
		$db = new DB();
		$result = [];
		$param = array(
			':search' => '%'.$search.'%'
		);
		$list = $db->query('selectSearchFeedback', $param);
		if (is_array($list) && count($list) > 0) {
			foreach ($list as $item) {
				$entity = new Feedback();
				$entity->id = $item['id'];
				$entity->item_id= $item['item_id'];
				$entity->user_id= $item['user_id'];
                $entity->score= $item['score'];
                $entity->order_id = $item['order_id'];
                $entity->regist_date = $item['regist_date'];
                $entity->update_date = $item['update_date'];
				$entity->item->select($entity->item_id);
				$entity->user->select($entity->user_id);
				
				$result[] = $entity;
            }
		}
		return $result;
	}

	public function selectForIndex($item_id, $user_id, $index) {
		$db = new DB();
		$param = array(
				':item_id' => $item_id,
				':user_id' => $user_id,
				':index' => $index
		);
        $result = array();
		$list = $db->query('selectFeedbackIndex', $param);
		if (is_array($list) && count($list) > 0) {
			foreach ($list as $item) {
				$this->id = $item['id'];
				$this->item_id= $item['item_id'];
				$this->user_id= $item['user_id'];
                $this->score= $item['score'];
                $this->order_id = $item['order_id'];
                $this->regist_date = $item['regist_date'];
				$this->update_date = $item['update_date'];
				$this->index = $item['indexImg'];
				$this->state = $item['state'];
            }
            return true;
		}
		return false;

	}

	public function selectByOrderIdGroupIndex($order_id) {
		$db = new DB();
		$param = array(
				':order_id' => $order_id
		);
        $result = array();
		$list = $db->query('selectByOrderIdGroupIndex', $param);
		if (is_array($list) && count($list) > 0) {
			foreach ($list as $item) {
				$this->id = $item['id'];
				$this->item_id= $item['item_id'];
				$this->user_id= $item['user_id'];
                $this->score= $item['score'];
                $this->order_id = $item['order_id'];
                $this->regist_date = $item['regist_date'];
                $this->update_date = $item['update_date'];

            }
		}
	}

	public function updateSate() {
		$db = new DB();
		$param = array(
				':id' => $this->id,
				':state' => $this->state
		);

		$db->execute('updateFeedbackState', $param);
		
	}
}