<?php
include_once(__DIR__.'/../common/db.php');

class InvoiceDetails {
	public $invoice_id;
	public $order_id;

	public $item_id;
	public $order_state;
	public $title;
	public $price;
	public $commission;
	public $delivery_price;
	public $buy_user_id;
	public $buy_count;
	public $buy_user_name;
	public $owner_id;
	public $owner_name;
	public $transaction_date;

	public $status;

	public $regist_date;
	public $update_date;

	public function selectTransaction($month, $owner_id) {
		$db = new DB();
		$target = strtotime($month . '01');
		$param = array(
				':target' => date('Y-m-d', $target),
				':next' => date('Y-m-d', strtotime(date('Y-m-d', $target) . '+1 month')),
				':owner_id' => $owner_id
		);

		$result = array();
		$list = $db->query('selectInvoiceTransaction', $param);
		if (is_array($list) && count($list) > 0) {
			foreach ($list as $item) {
				$entity = new InvoiceDetails();
				$entity->order_id = $item['order_id'];

				$entity->order_state = $item['order_state'];
				$entity->item_id = $item['item_id'];
				$entity->title= $item['title'];
				$entity->price= $item['price'];
				$entity->delivery_price= $item['delivery_price'];
				$entity->buy_user_id= $item['buy_user_id'];
				$entity->buy_count= $item['buy_count'];
				$entity->owner_id= $item['owner_id'];
				$entity->transaction_date= $item['regist_date'];

				$entity->regist_date = $item['regist_date'];
				$entity->update_date = $item['update_date'];

				$result[] = $entity;
			}
		}
		return $result;
	}

	public function selectTransaction2($owner_id) {
		$db = new DB();
		$param = array(
				':owner_id' => $owner_id
		);

		$result = array();
		$list = $db->query('selectInvoiceTransaction2', $param);
		if (is_array($list) && count($list) > 0) {
			foreach ($list as $item) {
				$entity = new InvoiceDetails();
				$entity->order_id = $item['order_id'];

				$entity->order_state = $item['order_state'];
				$entity->item_id = $item['item_id'];
				$entity->title= $item['title'];
				$entity->price= $item['price'];
				$entity->delivery_price= $item['delivery_price'];
				$entity->buy_user_id= $item['buy_user_id'];
				$entity->buy_count= $item['buy_count'];
				$entity->owner_id= $item['owner_id'];
				$entity->transaction_date= $item['regist_date'];

				$entity->regist_date = $item['regist_date'];
				$entity->update_date = $item['update_date'];

				switch ($entity->order_state) {
					case 2:
						if (diffDays(date("Y/m/d"), $entity->update_date) < FORCED_LIQUIDATION_DAYS) {
							continue 2;
						}
						$order = new Order();
						$order->id = $entity->order_id;
						$order->state = 3;
						$order->updateProcedure();
						break;
					case 3:
						break;
					default:
						continue 2;
				}

				$result[] = $entity;

			}
		}
		return $result;
	}

	/**
	 * 検索
	 * @param unknown $invoice_id
	 * @return InvoiceDetails[]
	 */
	public function select($invoice_id, $month) {
		$db = new DB();
		$param = array(
				':invoice_id' => $invoice_id,
		);
		$result = array();
		$list = $db->query('selectInvoiceDetails', $param);
		if (is_array($list) && count($list) > 0) {
			foreach ($list as $item) {
				$entity = new InvoiceDetails();
				$entity->invoice_id= $item['invoice_id'];
				$entity->order_id= $item['order_id'];
				$entity->item_id = $item['item_id'];
				$entity->order_state = $item['order_state'];
				$entity->buy_user_id= $item['buy_user_id'];
				$entity->buy_user_name= $item['buy_user_name'];
				$entity->buy_count= $item['buy_count'];

				$entity->title= $item['title'];
				$entity->price= $item['price'];
				$entity->commission= $item['commission'];
				$entity->delivery_price= $item['delivery_price'];
				$entity->buy_user_id= $item['buy_user_id'];
				$entity->buy_count= $item['buy_count'];
				$entity->owner_id= $item['owner_id'];
				$entity->owner_name= $item['owner_name'];
				$entity->transaction_date= $item['transaction_date'];

				$result[] = $entity;
			}
		}
		return $result;

	}

	/**
	 * 登録
	 */
	public function regist() {
		$db = new DB();
		$param = array(
				':invoice_id' => $this->invoice_id,
				':order_id' => $this->order_id
		);
		$db->execute('insertInvoiceDetails', $param);

	}


}