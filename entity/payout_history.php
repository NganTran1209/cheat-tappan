<?php
include_once(__DIR__.'/../common/util.php');

class PayoutHistory {

    public $id;
    public $user_id;
    public $amount;
    public $status;
    public $regist_date;
    public $update_date;

    public $user;

    function __construct() {
        $this->user = new User();
    }

    /**
     * 出金履歴登録
     */
    public function regist() {
        $db = new DB();
        $param = array(
            ':user_id' => $this->user_id,
            ':amount' => $this->amount,
        );
        $db->execute('insertPayoutHistory', $param);
        $this->sendRquestPayoutAdmin();
    }

    /**
     * 振込完了登録
     */
    public function updateComplete() {
        $db = new DB();
        $param = array(
            ':id' => $this->id,
        );
        $db->execute('updatePayoutHistoryForComplete', $param);
        $this->sendPayoutBankTransfer();
    }


    /**
     * 検索
     * @param unknown $user_id
     * @return CashFlow[]
     */
    public function select() {
        $db = new DB();
        $param = array(
            ':id' => $this->id,
        );

        $list = $db->query('selectPayoutHistory', $param);
        if (is_array($list) && count($list) > 0) {
            foreach ($list as $item) {
                $this->id = $item['id'];
                $this->user_id= $item['user_id'];
                $this->amount= $item['amount'];
                $this->status= $item['status'];
                $this->update_date= $item['update_date'];
                $this->regist_date= $item['regist_date'];

                $this->user->select($this->user_id);
            }
        }
    }

    /**
     * 検索（ユーザーID）
     * @param unknown $user_id
     * @return CashFlow[]
     */
    public function selectByUserId($user_id) {
        $db = new DB();
        $param = array(
            ':user_id' => $user_id
        );

        $result = [];
        $list = $db->query('selectPayoutHistoryByUserId', $param);
        if (is_array($list) && count($list) > 0) {
            foreach ($list as $item) {
                $entity = new PayoutHistory();
                $entity->id = $item['id'];
                $entity->user_id= $item['user_id'];
                $entity->amount= $item['amount'];
                $entity->status= $item['status'];
                $entity->update_date= $item['update_date'];
                $entity->regist_date= $item['regist_date'];

                $entity->user->select($entity->user_id);

                $result[] = $entity;
            }
        }

        return $result;
    }

    /**
     * 検索（出金全件）
     * @return CashFlow[]
     */
    public function selectForAll() {
        $db = new DB();
        $result = [];
        $list = $db->query('selectPayoutHistoryForAll');
        if (is_array($list) && count($list) > 0) {
            foreach ($list as $item) {
                $entity = new PayoutHistory();
                $entity->id = $item['id'];
                $entity->user_id= $item['user_id'];
                $entity->amount= $item['amount'];
                $entity->status= $item['status'];
                $entity->update_date= $item['update_date'];
                $entity->regist_date= $item['regist_date'];

                $entity->user->select($entity->user_id);

                $result[] = $entity;
            }
        }

        return $result;
    }

    /**
     * 振込依頼（管理者へ）
     */
    private function sendRquestPayoutAdmin() {
        $system_config = SystemConfig::select();
        $user = new User();
        $user->select($this->user_id);

        $mail = new Mail();
        $mail->to = $system_config->to_email;
        $mail->from = $system_config->from_email;
        $mail->fromName = $system_config->from_email_name;

        $template = MailTemplate::get('sendRquestPayoutAdmin');
        $mail->subject = $template->replaceTitle(
            array(
                MailTemplate::REPLACE_USER_HASH_ID=> $user->hash_id,
                MailTemplate::REPLACE_USER_NAME=> $user->name,
            )
            );
        $mail->body = $template->replaceMessage(
            array(
                MailTemplate::REPLACE_USER_NAME => $user->name,
                MailTemplate::REPLACE_USER_HASH_ID=> $user->hash_id,

                MailTemplate::REPLACE_FINANCIAL_INSTITUTION_NAME=> $user->financial_institution_name,
                MailTemplate::REPLACE_BRANCH_NAME=> $user->branch_name,
                MailTemplate::REPLACE_DEPOSIT_TYPE=> $user->get_deposit_type_name(),
                MailTemplate::REPLACE_ACCOUNT_NUMBER=> $user->account_number,
                MailTemplate::REPLACE_ACCOUNT_HOLDER=> $user->account_holder,

                MailTemplate::REPLACE_AMOUNT=>number_format($this->amount),
            )
            );
        $mail->sendMail();

    }

    /**
     * 振込完了（ユーザーへ）
     */
    private function sendPayoutBankTransfer() {
        $system_config = SystemConfig::select();
        $user = new User();
        $user->select($this->user_id);

        $mail = new Mail();
        $mail->to = $user->email;
        $mail->from = $system_config->from_email;
        $mail->fromName = $system_config->from_email_name;

        $template = MailTemplate::get('sendPayoutBankTransfer');
        $mail->subject = $template->replaceTitle(
            array(
                MailTemplate::REPLACE_USER_HASH_ID=> $user->hash_id,
                MailTemplate::REPLACE_USER_NAME=> $user->name,
            )
            );
        $mail->body = $template->replaceMessage(
            array(
                MailTemplate::REPLACE_USER_NAME => $user->name,
                MailTemplate::REPLACE_USER_HASH_ID=> $user->hash_id,

                MailTemplate::REPLACE_FINANCIAL_INSTITUTION_NAME=> $user->financial_institution_name,
                MailTemplate::REPLACE_BRANCH_NAME=> $user->branch_name,
                MailTemplate::REPLACE_DEPOSIT_TYPE=> $user->get_deposit_type_name(),
                MailTemplate::REPLACE_ACCOUNT_NUMBER=> $user->account_number,
                MailTemplate::REPLACE_ACCOUNT_HOLDER=> $user->account_holder,

                MailTemplate::REPLACE_AMOUNT=>number_format($this->amount),
            )
            );
        $mail->sendMail();

    }

}
