<?php
include_once(__DIR__.'/../common/util.php');

define('MAX_FREE_INPUT_ITEM_ID', 3);
class FreeInputItem {
    public $id;
    public $name;
    public $field_type;
    public $enabled;

    /**
     * 全選択
     * @return
     */
    static public function selectAll() {
        $db = new DB();

        $result = array();
        $list = $db->query('selectFreeInputItem');
        if (is_array($list) && count($list) > 0) {
            foreach ($list as $item) {
                $entity = new FreeInputItem();
                $entity->id = $item['id'];
                $entity->name = $item['name'];
                $entity->field_type = $item['field_type'];
                $entity->enabled = $item['enabled'];

                $result[] = $entity;
            }
        }
        return $result;
    }

    static public function select($id) {
        $db = new DB();
        $param = array(
            ':id' => $id
        );

        $list = $db->query('selectFreeInputItemFromId', $param);
        if (is_array($list) && count($list) > 0) {
            foreach ($list as $item) {
                $entity = new FreeInputItem();
                $entity->id = $item['id'];
                $entity->name = $item['name'];
                $entity->field_type = $item['field_type'];
                $entity->enabled = $item['enabled'];

                return $entity;
            }
        }
        return false;
    }

    static public function regist($id, $name, $field_type) {
        $db = new DB();
        $param = [
            ':id' => $id,
            ':name' => $name,
            ':field_type' => $field_type,
        ];
        $db->execute('insertFreeInputItem', $param);
    }

    /**
     * 更新
     */
    static public function update() {
        for ($i = 1; $i <= MAX_FREE_INPUT_ITEM_ID; $i++) {
            static::updateFromId($i);
        }
    }

    static public function updateFromId($id) {
        $db = new DB();
        $param = array(
            ':id' => $id,
            ':email' => $_POST['free_input_item_name_'.$id],
            ':field_type' => $_POST['free_input_item_field_type_'.$id],
        );
        $db->execute('updateFreeInputItem', $param);

    }

    static public function updateNameFromId($id, $name) {
        $db = new DB();
        $param = array(
            ':id' => $id,
            ':name' => $name,
        );
        $db->execute('updateFreeInputItemForName', $param);
    }

    static public function updateEnabledFromId($id, $enabled) {
        $db = new DB();
        $param = array(
            ':id' => $id,
            ':enabled' => $enabled,
        );
        $db->execute('updateFreeInputItemForEnabled', $param);
    }
}
