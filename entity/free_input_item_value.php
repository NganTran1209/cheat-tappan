<?php
include_once(__DIR__.'/../common/util.php');

class FreeInputItemValue {
    public $id;
    public $free_input_item_id;
    public $name;
    public $order_number;
    public $enabled;

    static public function select($id) {
        $db = new DB();
        $param = array(
            ':id' => $id
        );

        $list = $db->query('selectFreeInputItemValue', $param);
        if (is_array($list) && count($list) > 0) {
            foreach ($list as $item) {
                $entity = new FreeInputItemValue();
                $entity->id = $item['id'];
                $entity->free_input_item_id = $item['free_input_item_id'];
                $entity->name = $item['name'];
                $entity->order_number = $item['order_number'];
                $entity->enabled = $item['enabled'];

                return $entity;
            }
        }

        return new FreeInputItemValue();


    }

    static public function selectFromFreeInputItemId($free_input_item_id) {
        $db = new DB();
        $param = array(
            ':free_input_item_id' => $free_input_item_id
        );

        $result = [];
        $list = $db->query('selectFreeInputItemValueFromFreeInputItemId', $param);
        if (is_array($list) && count($list) > 0) {
            foreach ($list as $item) {
                $entity = new FreeInputItemValue();
                $entity->id = $item['id'];
                $entity->free_input_item_id = $item['free_input_item_id'];
                $entity->name = $item['name'];
                $entity->order_number = $item['order_number'];
                $entity->enabled = $item['enabled'];

                $result[] = $entity;
            }
            return $result;
        }
        return [];

    }

    static public function regist() {
        $db = new DB();
        $param = array(
            ':free_input_item_id' => $_POST['free_input_item_id'],
            ':name' => $_POST['name'],
        );

        $db->execute('registFreeInputItemValue', $param);
    }

    static public function update() {
        $db = new DB();
        $param = array(
            ':id' => $_POST['id'],
            ':name' => $_POST['name'],
            ':enabled' => isset($_POST['enabled']),
        );

        $db->execute('updateFreeInputItemValue', $param);
    }

}
