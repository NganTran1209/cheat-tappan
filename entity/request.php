<?php
include_once(__DIR__.'/../common/db.php');

class Request {

	public $id;
	public $user_id;
	public $title;
	public $text;
	public $price;
	public $enabled;

	public $follow_count;
	public $last_date;

	public $regist_date;
	public $update_date;

	public $isNewEntry = false;

	/**
	 * 登録
	 */
	public function regist() {
		$db = new DB();
		$param = array(
				':user_id' => $this->user_id,
				':title' => $this->title,
				':text' => $this->text,
				':price' => $this->price
		);

		$db->execute('insertReauest', $param);
		$this->sendNewRequest();
	}

	/**
	 * 全件検索
	 * @return Item[]
	 */
	public function selectAll() {
		$db = new DB();

		$result = array();
		$list = $db->query('selectRequestAll');
		if (is_array($list) && count($list) > 0) {
			foreach ($list as $item) {
				$entity = new Request();
				$entity->id = $item['id'];
				$entity->user_id= $item['user_id'];
				$entity->title= $item['title'];
				$entity->text= $item['text'];
				$entity->price= $item['price'];
				$entity->enabled= $item['enabled'];

				$entity->regist_date= $item['regist_date'];
				$entity->update_date= $item['update_date'];

				$result[] = $entity;
			}
		}
		return $result;
	}

	/**
	 * 全件検索(該当ユーザ)
	 * @return Item[]
	 */
	public function selectUser($user_id) {
		$db = new DB();

		$result = array();
		$param = array(
				':user_id' => $user_id
		);

		$list = $db->query('selectRequestUser', $param);
		if (is_array($list) && count($list) > 0) {
			foreach ($list as $item) {
				$entity = new Request();
				$entity->id = $item['id'];
				$entity->user_id= $item['user_id'];
				$entity->title= $item['title'];
				$entity->text= $item['text'];
				$entity->price= $item['price'];
				$entity->enabled= $item['enabled'];
				$entity->follow_count= $item['follow_count'];
				$entity->last_date= $item['last_date'];

				$entity->regist_date= $item['regist_date'];
				$entity->update_date= $item['update_date'];

				$result[] = $entity;
			}
		}
		return $result;
	}

/**
	 * 詳細検索
	 * @param unknown $id
	 * @return boolean
	 */
	public function select($id) {
		$db = new DB();
		$param = array(
				':id' => $id
		);

		$list = $db->query('selectRequest', $param);
		if (is_array($list) && count($list) > 0) {
			foreach ($list as $item) {
				$this->id = $item['id'];
				$this->user_id= $item['user_id'];
				$this->title= $item['title'];
				$this->text= $item['text'];
				$this->price= $item['price'];
				$this->enabled= $item['enabled'];

				$this->regist_date= $item['regist_date'];
				$this->update_date= $item['update_date'];

				return true;
			}
		}
		return false;
	}

	/**
	 * 許可
	 */
	public function approval() {
		$db = new DB();
		$param = array(
				':id' => $this->id,
				':enabled' => 1
		);
		$db->execute('registRequestEnabled', $param);
		$this->sendApprovaRequestMail();
	}

	/**
	 * 却下
	 */
	public function regection() {
		$db = new DB();
		$param = array(
				':id' => $this->id,
				':enabled' => 2
		);
		$db->execute('registRequestEnabled', $param);
	}

	/**
	 * 承認メール送信（購入希望者）
	 */
	public function sendApprovaRequestMail() {

	    $system_config = SystemConfig::select();
	    $owner = new User();
		$owner->select($this->user_id);

		$mail = new Mail();
		$mail->to = $owner->email;
		$mail->from = $system_config->from_email;
		$mail->fromName = $system_config->from_email_name;

		$url = getContextRoot() . '/user/request_details.php?id='.$this->id;

		$template = MailTemplate::get('sendApprovaRequestMail');
		$mail->subject = $template->replaceTitle(
				array(
						MailTemplate::REPLACE_TITLE=> $this->title,
				)
				);
		$mail->body = $template->replaceMessage(
				array(
						MailTemplate::REPLACE_TITLE => $this->title,
						MailTemplate::REPLACE_USER_NAME => $owner->name,
						MailTemplate::REPLACE_TEXT=> $this->text,
						MailTemplate::REPLACE_REQUEST_URL=> $url,
						MailTemplate::REPLACE_PRICE=> number_format($this->price),
				)
				);
		$mail->sendMail();
	}

	/**
	 * メール送信
	 */
	public function sendNewRequest() {

	    $system_config = SystemConfig::select();
	    $mail = new Mail();
	    $mail->to = $system_config->to_email;
	    $mail->from = $system_config->from_email;
	    $mail->fromName = $system_config->from_email_name;

		$owner = new User();
		$owner->select(getUserId());

		$template = MailTemplate::get('sendNewRequest');
		$mail->subject = $template->replaceTitle(
				array(
						MailTemplate::REPLACE_TITLE=> $this->title,
				)
				);
		$mail->body = $template->replaceMessage(
				array(
						MailTemplate::REPLACE_TITLE => $this->title,
						MailTemplate::REPLACE_USER_NAME => $owner->name,
						MailTemplate::REPLACE_USER_HASH_ID => $owner->hash_id
				)
				);
		$mail->sendMail();
	}

	/**
	 * バリデーション
	 * @return unknown
	 */
	public function validate() {
		$msg = '';
		if (empty($this->title)) {
			$msg .= '[タイトル]';
		}
		if (empty($this->text)) {
			$msg .= '[説明文]';
		}
		if (empty($this->price)) {
			if ($this->price != 0 || $this->price != '0')
				$msg .= '[予算]';
		}

		if (!empty($msg)) {
			$msg .= 'は必須項目です。';
			setMessage($msg);
		}

		if ($this->price < 0) {
			$msg = '最小予算は0です';
			setMessage($msg);
		}

		return empty($msg);
	}

	/**
	 * 状態名変換（ユーザー向け）
	 * @param unknown $status
	 * @return string
	 */
	public static function toStatusUserText($status) {
		switch ($status) {
			case 0:
				return '承認待ち';
			case 1:
				return '掲載中';
			case 2:
				return '掲載却下';
			default:
				break;
		}

		return '-';
	}


}
