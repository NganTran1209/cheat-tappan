<?php
include_once(__DIR__.'/../common/util.php');

class CashFlow {

    public $user_id;
    public $order_id;
    public $in_out;
    public $amount;
    public $commission;
    public $regist_date;
    public $update_date;

    public $order;

    public $feedback;
    function __construct() {
        $this->order = new Order();
        $this->feedback = new Feedback();
    }

    /**
     * キャッシュフロー登録
     */
    public function regist() {
        $db = new DB();

        if ($this->amount >= 0) {
            $system_config = SystemConfig::select();
            $this->commission = ceil($this->amount * ($system_config->commission_rate / 100));
            $this->in_out = 1;
        } else {
            $this->commission = 0;
            $this->in_out = 2;
            $this->order_id = 0;
        }

        $param = array(
            ':user_id' => $this->user_id,
            ':order_id' => $this->order_id,
            ':in_out' => $this->in_out,
            ':amount' => $this->amount,
            ':commission' => $this->commission,
        );
        $db->execute('insertCashFlow', $param);
    }

    /**
     * 検索（ユーザーID）
     * @param unknown $user_id
     * @return CashFlow[]
     */
    public function selectByUserId($user_id) {
        $db = new DB();
        $param = array(
            ':user_id' => $user_id
        );

        $result = [];
        $list = $db->query('selectCashFlowByUsesrId', $param);
        if (is_array($list) && count($list) > 0) {
            foreach ($list as $item) {
                $entity = new CashFlow();
                $entity->id = $item['id'];
                $entity->user_id= $item['user_id'];
                $entity->order_id= $item['order_id'];
                $entity->in_out= $item['in_out'];
                $entity->amount= $item['amount'];
                $entity->commission= $item['commission'];
                $entity->update_date= $item['update_date'];
                $entity->regist_date= $item['regist_date'];

                $entity->order->select($entity->order_id);
                $entity->feedback->selectByOrderId($entity->order_id);
                $result[] = $entity;
            }
        }

        return $result;
    }

    /**
     * 検索（ユーザーID）入金完了のみ
     * @param unknown $user_id
     * @return CashFlow[]
     */
    public function selectByUserIdForPaymentComplete($user_id) {
        $result = [];
        foreach ($this->selectByUserId($user_id) as $item) {
            if ($item->in_out == 1 && $item->order->cash_flow < 2) {
                continue;
            }
            $result[] = $item;
        }

        return $result;
    }

    /**
     * 検索（入金情報）
     * @return CashFlow[]
     */
    public function selectByIn() {
        $db = new DB();
        $result = [];
        $list = $db->query('selectCashFlowForIn');
        if (is_array($list) && count($list) > 0) {
            foreach ($list as $item) {
                $entity = new CashFlow();
                $entity->id = $item['id'];
                $entity->user_id= $item['user_id'];
                $entity->order_id= $item['order_id'];
                $entity->in_out= $item['in_out'];
                $entity->amount= $item['amount'];
                $entity->commission= $item['commission'];
                $entity->update_date= $item['update_date'];
                $entity->regist_date= $item['regist_date'];

                $entity->order->select($entity->order_id);
                $entity->feedback->selectByOrderId($entity->order_id);

                $result[] = $entity;
            }
        }

        return $result;
    }

    /**
     * 検索（ユーザーID）
     * @param unknown $order_id
     * @return CashFlow[]
     */
    public function selectByOrderId($order_id) {
        $db = new DB();
        $param = array(
            ':order_id' => $order_id
        );

        $result = [];
        $list = $db->query('selectCashFlowByOrderId', $param);
        if (is_array($list) && count($list) > 0) {
            foreach ($list as $item) {
                $this->id = $item['id'];
                $this->user_id= $item['user_id'];
                $this->order_id= $item['order_id'];
                $this->in_out= $item['in_out'];
                $this->amount= $item['amount'];
                $this->commission= $item['commission'];
                $this->update_date= $item['update_date'];
                $this->regist_date= $item['regist_date'];

            }
        }

    }
}
