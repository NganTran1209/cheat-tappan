<?php 
$title_page = 'すべての商品';
$description_page = '欲しい商品を画像で確認・届いた商品を画像で照合！気持ち良い取引が叶うオンラインマーケット。';
 ?>
<?php include_once(__DIR__ . '/common/util.php'); ?>
<?php
$uri = $_SERVER["REQUEST_URI"];
$evaluate = new Evaluate();
$evaluateMap = $evaluate->selectSummaryMap();
$items = array();
$items_tmp = array();
$entity = new Item();
$category_title = 'すべての商品';
$page = 1;

$search = empty($_POST['search']) ? '' : $_POST['search'];
$category = empty($_POST['category']) ? '' : $_POST['category'];
$sort = empty($_POST['sort']) ? '' : $_POST['sort'];
$owner_id = empty($_POST['owner_id']) ? '' : $_POST['owner_id'];
$page = empty($_POST['page']) ? '1' : $_POST['page'];

if (!empty($_GET['category'])) {
    $category = $_GET['category'];
}
if (!empty($_GET['search'])) {
    $search = $_GET['search'];
}
if (!empty($_GET['owner_id'])) {
    $owner_id = $_GET['owner_id'];
}
if (!empty($_GET['page'])) {
    $page = $_GET['page'];
}

if (!empty($category)) {
    $items_tmp = $entity->selectCategory($category);
    $category_title = selectCodeName('selectCategoryById', $category);
} elseif (!empty($search)) {
    $items_tmp = $entity->selectSearch($search);
    $category_title = '検索文字「' . $search . '」';
} else {
    $items_tmp = $entity->selectAll();
}
foreach ($items_tmp as $item) {
    if ($item->enabled != 1) {
        continue;
    }
    if (!$item->owner_selling) {
        continue;
    }
    if (!empty($owner_id) && $item->owner_id != $owner_id) {
        continue;
    }
    //if ($item->order_id != null) { continue; }
    $items[] = $item;
}

// ページ数制御 商品増幅＠テスト用
//{
//	for ($i = 0; $i < 500; $i++) {
//		$items[] = $items[0];
//	}
//}


// ソート
if (!empty($sort)) {
    if ($sort == '2') {
        usort($items, function ($a, $b) {
// 			if (empty($a->buyer_id) && !empty($b->buyer_id)) {
// 				return -1;
// 			} elseif (!empty($a->buyer_id) && empty($b->buyer_id)) {
// 				return 1;
// 			}
            return $b->price - $a->price;
        });
    } elseif ($sort == '3') {
        usort($items, function ($a, $b) {
// 			if (empty($a->buyer_id) && !empty($b->buyer_id)) {
// 				return -1;
// 			} elseif (!empty($a->buyer_id) && empty($b->buyer_id)) {
// 				return 1;
// 			}
            return $a->price - $b->price;
        });
    } else {
        usort($items, function ($a, $b) {
// 			if (empty($a->buyer_id) && !empty($b->buyer_id)) {
// 				return -1;
// 			} elseif (!empty($a->buyer_id) && empty($b->buyer_id)) {
// 				return 1;
// 			} elseif (!empty($a->buyer_id) && !empty($b->buyer_id)) {
// 				return $a->order_date >= $b->order_date ? -1 : 1 ;
// 			}
            return $a->regist_date >= $b->regist_date ? -1 : 1;
        });
    }
} else {
    usort($items, function ($a, $b) {
// 		if (empty($a->buyer_id) && !empty($b->buyer_id)) {
// 			return -1;
// 		} elseif (!empty($a->buyer_id) && empty($b->buyer_id)) {
// 			return 1;
// 		} elseif (!empty($a->buyer_id) && !empty($b->buyer_id)) {
// 			return $a->regist_date >= $b->regist_date ? -1 : 1;
// 		}
        return $a->regist_date >= $b->regist_date ? -1 : 1;
    });
}

if (!empty($owner_id)) {
    $owner = new User();
    $owner->select($owner_id);
    $category_title = $owner->getViewId();
}

function get_url_status($url) 
{
    $curl = curl_init($url);
    $ch = curl_init();
    curl_setopt($ch,CURLOPT_BINARYTRANSFER,1);
    curl_setopt($ch, CURLOPT_VERBOSE, 0);
    curl_setopt($ch, CURLINFO_HEADER_OUT, true);
    curl_setopt($ch, CURLOPT_HEADER, true);
    curl_setopt($ch, CURLOPT_URL, $url);
    echo "<span style='display:none'>";
        $result = curl_exec($curl);
    echo"</span>";
    if ($result !== false)
    {
        $statusCode = curl_getinfo($curl, CURLINFO_HTTP_CODE);  
        if ($statusCode == 404)
        {
            return false;
        }
        else
        {
            return true;
        }
    }
    else
    {
        return false;
    }
}
if (!empty($_GET['new_title'])) {
    $category_title = $_GET['new_title'];
}
$title_page = $category_title;

?>
<?php include('other_header.php'); ?>
<form id="fm-param" method="post">
    <input type="hidden" id="id-page" name="page" value="<?= $page ?>" />
</form>
<div class="com-header-top">
    <div class="com-header-top__img  wow animate__animated animate__fadeInUp">
        <img src="<?php echo HOME_URL; ?>/common/assets/img/common/other-com-header.png" alt="">
    </div>
    <div class="com-header-top__path bg-other-01  wow animate__animated animate__fadeInUp">
        <p><span><a href="<?php echo HOME_URL; ?>/" class="clr-yel">トップページ</a></span><span> > </span><span><?= $category_title ?></span></p>
    </div>
<!--    <div class="com-header-top__txt">
        <p><?= $category_title ?></p>
    </div>-->
</div>
<div class="customer-container">
<div class="category-title  wow animate__animated animate__fadeInUp">
                <h3><span><?= $category_title ?></span></h3>
            </div>
    <div class="ladies-new-term  wow animate__animated animate__fadeInUp">
        <div class="row">
            <?php
                $count = 0;
                $index = 0;
                foreach ($items as $item) {
                    if (User::isInvalid($item->owner_id)) {
                        continue;
                    }
                    if ($item->order_id != null && diffDays(date("Y/m/d"), $item->order_date) > MAX_VIEW_DAYS) {
                        continue;
                    }

                    $count++;
                    if (ceil(++$index / MAX_PAGE_COUNT) != $page) {
                        continue;
                    }
            ?>
            <?php
                $evaluate = new Evaluate();
                $evaluate->selectSummary($item->owner_id);
            ?>

                <a href="<?php echo HOME_URL; ?>/itemdetail.php?id=<?= $item->id ?>" class="col-sp-3 col-md-3 float-left mb-5">
                    <div class="ladies-group-column" style="width:100%">
                        <div class="ladies-group-column__img">
                            <?php 
                                // if(@file_get_contents($item->getThumbnailUrl()) ): 
                            ?>
                                <img src="<?= $item->getThumbnailUrl() ?>" alt="" width="255" height="255">
                            <?php 
                                // else: 
                            ?>
                                <!-- <img src="<?php echo HOME_URL; ?>/common/assets/img/no-image.png" alt="" > -->
                            <?php 
                                // endif; 
                            ?>
                            <div class="ladies-group-column__img-bottom"><?= $item->title ?></div>
                        </div>
                        <div class="ladies-group-column__price">
                            <p><span>¥<?= number_format($item->price) ?></span></p>
                        </div>
                    </div>
                </a>
            <?php } ?>  
        </div>
        <?php if ($count == 0) : ?>
            <div class="card mb-3">
                <div class="card-body" style="font-size: 1.5rem;">
                    <div class="card-text font20 fontBold">
                         検索結果 0件
                    </div>
                    <p>
                        該当する商品が見つかりませんでした。商品は増えていますので、これからの出品にご期待ください。
                        また、購入希望情報への投稿もご利用ください。
                    </p>
                </div>
            </div>
        <?php else: ?>
        <?php
            $prev_page = $page - 1;
            $max_page = ceil($count / MAX_PAGE_COUNT);
            $next_page = min($max_page, $page + 1);
            $view_start_page = max(1, $page - ceil(VIEW_PAGE_SELECT_COUNT / 2));
            $view_last_page = min($view_start_page + VIEW_PAGE_SELECT_COUNT - 1, $max_page);
            if ($view_last_page - $view_start_page < VIEW_PAGE_SELECT_COUNT) {
                $view_start_page = max(1, $view_last_page - VIEW_PAGE_SELECT_COUNT + 1);
            }
        ?>
        <?php if($max_page >1): ?>
            <div class="content-group-pagination">
        <?php else: ?>
            <div class="content-group-pagination content-one-page">
        <?php endif; ?>
        <?php if($max_page > 1 ) { ?>
            <?php if ($prev_page > 0): ?>
                <a id="pagination__preve" class="pagination__preve" href=""
                        onclick="submitForm('fm-param', 'page', '<?= $prev_page ?>'); return false;"><i class="bi bi-chevron-left"></i></a>
            <?php else: ?>
                <a id="pagination__preve" class="pagination__preve" href="" onclick="return false;"><i class="bi bi-chevron-left"></i></a>
            <?php endif; ?>
        <?php } ?>
        <?php for ($i = $view_start_page; $i <= $view_last_page; $i++): ?>
            <?php if ($page != $i): ?>
                <a class="pagination__num" href=""
                    onclick="submitForm('fm-param', 'page', '<?= $i ?>'); return false;"><span><?= $i ?></span></a>
            <?php else: ?>
                <a class="pagination__num pag_active" href="" onclick="return false;"><span><?= $i ?></span></a>
            <?php endif; ?>
        <?php endfor; ?>
        <?php if($max_page > 1 ) { ?>
            <?php if ($page < $max_page): ?>
                <a id="pagination__next" class="pagination__preve" href="" onclick="submitForm('fm-param', 'page', '<?= $next_page ?>'); return false;">
                    <i class="bi bi-chevron-right"></i>
                </a>
            <?php else: ?>
                <a id="pagination__next" class="pagination__preve" href="" onclick="return false;"><i class="bi bi-chevron-right"></i></a>
            <?php endif; ?>
        <?php } ?>    
        </div>
        <?php endif; ?>
    </div>
</div>
<?php include('footer.php'); ?>