<?php 
$title_page = 'ログイン画面';
$description_page = '欲しい商品を画像で確認・届いた商品を画像で照合！気持ち良い取引が叶うオンラインマーケット。';
 ?>
<?php include_once(__DIR__ . '/common/util.php'); ?>
<?php include_once(__DIR__ . '/entity/user.php'); ?>
<?php
if (logout()) {
    setMessage('ログアウトしました。');
}
unset($_SESSION['login']);
if (isset($_POST['email'])) {
    $entity = new User();
    $entity->email = $_POST['email'];
    $entity->password = $_POST['password'];

    if ($entity->login()) {
        $_SESSION['login'] = $entity;
        $entity->updateIp();
        $entity->select($entity->id);
        header('Location: '.getContextRoot().'/user/mypage.php');
        exit;
    } else {
        setMessage('IDまたはパスワードが違います。');
    }
}
?>
<?php include('other_header.php'); ?>
<div class="container mb-5">
    <div class="row">
        <div class="mainContents login_page">
            <div class="bg-inner wow animate__animated animate__fadeInUp">
                <?php if (!isLogin()): ?>
                    <aside id="formlogin" class="login__form">
                        <form action="<?php echo HOME_URL; ?>/login.php" method="post" name="Login_Form" class="form-signin">
                            <h3 class="form-signin-heading"><i class="fa fa-lock mr-2"></i>ログイン</h3>
                            <hr class="colorgraph">
                            <input type="text" class="form-control mb-2" name="email" placeholder="ログインID"/>
                            <input type="password" class="form-control" name="password" placeholder="パスワード"/>
                            <input class="btn btn-danger btn-block" type="submit" value="ログイン"/>
                        </form>
                    </aside>
                <?php endif; ?>
                <div class="lostpassword">
                    <a href="reset_password.php">パスワードを忘れた場合</a>
                </div>
            </div>
        </div>
    </div>
</div>

<?php include('footer.php'); ?>