<?php 
$title_page = 'ご利用の流れ（売る）';
$description_page = '欲しい商品を画像で確認・届いた商品を画像で照合！気持ち良い取引が叶うオンラインマーケット。';
 ?>
<?php include('../user_header.php'); ?>

    <div class="com-header-top">
        <div class="com-header-top__img wow animate__animated animate__fadeInUp">
            <img src="<?php echo HOME_URL; ?>/common/assets/img/common/other-com-header.png" alt="">
        </div>
        <div class="com-header-top__path bg-other-01 wow animate__animated animate__fadeInUp">
            <p><span><a href="<?php echo HOME_URL; ?>/" class="clr-yel">トップページ</a></span><span> > </span><span>ご利用の流れ（売る）</span></p>
        </div>
 <!--       <div class="com-header-top__txt">
            <p>ご利用の流れ（売る）</p>
        </div>-->
    </div>
    <div class="customer-container">
    <div class="category-title wow animate__animated animate__fadeInUp">
                <h3><span>ご利用の流れ（売る）</span></h3>
            </div>
        <div class="customer-contact-form">
            <div class="flow-content">
                <div class="flow-content__txt wow animate__animated animate__fadeInUp">
                    <p>出品を希望される方は、以下の手順でお願いいたします。 </p>
                    <p>※会員登録すると「売る」「買う」両方ご利用できます。</p>
                </div>
                <div class="flow-content-step wow animate__animated animate__fadeInUp">
                    <div class="flow-content-step__left">
                        <p>Step1</p>
                        <p>(会員登録)</p>
                    </div>
                    <div class="flow-content-step__right">
                        <p>
                            会員登録をお願いいたします。 <br>
                            承認されると出品できるようになります。 <br>
                            登録時に身分証明書の添付が必須となります。
                        </p>
                    </div>
                </div>
                <div class="flow-icon wow animate__animated animate__zoomIn"></div>
                <div class="flow-content-step wow animate__animated animate__fadeInUp">
                    <div class="flow-content-step__left">
                        <p>Step2</p>
                        <p>(商品登録・公開)</p>
                    </div>
                    <div class="flow-content-step__right">
                        <p>
                            あなたの商品を登録しましょう。承認されると公開されます。<br>
                            簡単な操作であなたの商品を販売することができます。<br>
                            SNSなどでも商品公開を告知すれば、より多くの人の目にとまります。
                        </p>
                    </div>
                </div>
                <div class="flow-icon wow animate__animated animate__zoomIn"></div>
                <div class="flow-content-step wow animate__animated animate__fadeInUp">
                    <div class="flow-content-step__left">
                        <p>Step3</p>
                        <p>(納品)</p>
                    </div>
                    <div class="flow-content-step__right">
                        <p>
                            購入者があなたの商品を購入すると通知が来ます。<br>
                            支払いが行われたら、できるだけすぐに納品をお願いします。<br>
                            素早い対応が信用や高い評価につながります。
                        </p>
                    </div>
                </div>
                <div class="flow-icon wow animate__animated animate__zoomIn"></div>
                <div class="flow-content-step wow animate__animated animate__fadeInUp">
                    <div class="flow-content-step__left">
                        <p>Step4</p>
                        <p>(評価)</p>
                    </div>
                    <div class="flow-content-step__right">
                        <p>
                            購入者が出品者を評価します。<br>
                            購入者の評価をすれば取引終了です。
                        </p>
                    </div>
                </div>
                <div class="notice-show__btn wow animate__animated animate__fadeInUp">
                    <?php if (!isLogin()): ?>
                        <button onclick="location.href= '<?php echo HOME_URL; ?>/user/userentry.php'" >新規会員登録</button>
                    <?php else: ?>
                        <button onclick="location.href= '<?php echo HOME_URL; ?>/user/sell.php'" >出品する</button>
                    <?php endif; ?>
                    
                </div>
            </div>
        </div>
    </div>
<!--    <section class="footer-login">
        <div class="footer-login-content">
            <div class="footer-login-content__img">
                <img class="pc-show" src="<?php echo HOME_URL; ?>/common/assets/img/top/footer-bg.png" alt="">
                <img class="sp-show" src="<?php echo HOME_URL; ?>/common/assets/img/top/sp-footer-bg.png" alt="">
            </div>
            <div class="footer-login-content-item" >
                <div class="footer-login-content-item__txt"><h4>出品/購入するなら</h4></div>
                <div class="footer-login-content-item__txt"><h3>今すぐ会員登録！</h3></div>
                <div class="footer-login-content-item__btn" onclick="location.href= '<?php echo HOME_URL; ?>/user/userentry.php'">
                    <span>
                    </span>
                    <span>
                        新規会員登録
                    </span>
                </div>
            </div>
        </div>
    </section>-->

<?php include('../footer.php'); ?>
