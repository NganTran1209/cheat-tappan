<?php include('../header.php'); ?>
    <div id="faq" class="container">
        <div class="row">
            <div class="col-md-9 mainContents">
                <div class="bg-inner">
                    <h1>よくあるご質問</h1>
                    <div class="border-bottom py-4">
                        <h3 class="fontBold my-2">販売手数料は、売上があった時にのみ、かかるだけですか？</h3>
                        <p>
                            はい。その通りです。売上が発生した時のみ、売上に対して販売手数料がかかります。<br>
                            それ以外はかかりません。（出品者）<br>
                            <a href="<?php echo HOME_URL; ?>/pages/priceinfo.php">手数料の詳細はこちらから</a>ご確認できます。<br>
                            <strong>※売上げがなければ料金は一切かかりません。無料です。</strong><br>
                        </p>
                    </div>
                    <div class="border-bottom py-4">
                        <h3 class="fontBold my-2">販売手数料が発生した場合、いつ支払えばいいですか？</h3>
                        <p>
                            ユーザー側(出品者側)でなにか特別な操作をする必要は御座いません。
                        </p>
                        <p>
                            出金申請時に売上から手数料を引いた金額をお振込み致します。<br>
                            <a href="<?php echo HOME_URL; ?>/pages/priceinfo.php">手数料の詳細はこちらから</a>ご確認できます。<br>
                        </p>
                    </div>
                    <div class="border-bottom py-4">
                        <h3 class="fontBold my-2">購入の支払方法はなにがありますか？</h3>
                        <p>購入のお支払方法は銀行振込、クレジットカード決済となります。</p>
                    </div>
                    <div class="border-bottom py-4">
                        <h3 class="fontBold my-2">出品者もしくは購入者から連絡がない場合どうしたらいいですか？</h3>
                        <p>
                            まずは、相手の方に何度か連絡をされてください。お忘れになられているということもございます。
                            基本的に、商品を購入した日より7日連絡がない場合、再度、取引されている相手に連絡をお願いします。
                        </p>
                        <p>
                            どうしても連絡がつかない場合は、<a href="<?php echo HOME_URL; ?>/mail/">こちら</a>から運営者へご連絡ください。
                        </p>
                    </div>
                    <div class="border-bottom py-4">
                        <h3 class="fontBold my-2">購入する前に出品者に質問などできますか？</h3>
                        <p>ログインすると<strong>「交渉・質問する」「購入する」</strong>のボタンが表示されますので<strong>「交渉・質問する」</strong>より質問することができます。
                        </p>
                    </div>
                    <div class="border-bottom py-4">
                        <h3 class="fontBold my-2">登録後に会員情報の変更は出来ますか？</h3>
                        <p>はい。出来ます。</p>
                    </div>
                    <div class="border-bottom py-4">
                        <h3 class="fontBold my-2">購入後にキャンセルするにはどうすればよいですか？</h3>
                        <p><strong>購入後のキャンセルはできません
                                （<a href="<?php echo HOME_URL; ?>/pages/tokusho.php">特定商取引法に基づく表示</a>に記載）</strong><br>
                            特別な場合を除いてキャンセルした場合、悪戯による購入とみなし、IDの利用を制限することがあります。</p>
                    </div>
                </div>
            </div>
            <div class="col-md-3 sideContents">
                <?php include('../sidebar.php'); ?>
            </div>
        </div>
    </div>

<?php include('../footer.php'); ?>