<?php 
$title_page = '料金について';
$description_page = '欲しい商品を画像で確認・届いた商品を画像で照合！気持ち良い取引が叶うオンラインマーケット。';
 ?>
<?php include('../user_header.php'); ?>
    <div class="com-header-top">
        <div class="com-header-top__img  wow animate__animated animate__fadeInUp">
            <img src="<?php echo HOME_URL; ?>/common/assets/img/common/other-com-header.png" alt="">
        </div>
        <div class="com-header-top__path bg-other-01  wow animate__animated animate__fadeInUp">
            <p><span><a href="<?php echo HOME_URL; ?>/" class="clr-yel">トップページ</a></span><span> > </span><span>料金について</span></p>
        </div>
<!--      <div class="com-header-top__txt">
            <p>料金について</p>
        </div>-->
    </div>
    <div class="customer-container">
    <div class="category-title  wow animate__animated animate__fadeInUp">
                <h3><span>料金について</span></h3>
            </div>
            <div class="customer-contact-form p-priceinfo__1">
                <div class="priceinfo-inner">
                    <div class="contents  wow animate__animated animate__fadeInUp">
                        <h3>サイト利用料金（出品）</h3>
                        <table class="priceinfo-table">
                            <tr>
                                <th>個人</th>
                                <th>法人</th>
                            </tr>
                            <tr>
                                <td colspan="2">無料</td>
                            </tr>
                        </table>
                        <p class="font_note">
                            ※出品して商品が売れた場合のみ、売上総額に対して一律<?= round($system_config->commission_rate) ?>％の販売手数料がかかります。<br>
                            ※カード決済利用時はカード会社へ3.6％の手数料が事前に引かれます。<br>
                        </p>
                    </div>
                    <div class="contents  wow animate__animated animate__fadeInUp">
                        <h3>サイト利用料金（購入）</h3>
                        <table class="priceinfo-table">
                            <tr>
                                <th>個人</th>
                                <th>法人</th>
                            </tr>
                            <tr>
                                <td colspan="2">無料</td>
                            </tr>
                        </table>
                    </div>
                </div>
        </div>
    </div>
<?php include('../footer.php'); ?>