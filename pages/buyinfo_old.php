<?php include('../header.php'); ?>
<div class="container">
    <div class="row">
        <div class="col-md-9 mainContents">
            <div class="bg-inner">
                <h1>買いたい方</h1>
                <p class="mb-4">
                    購入を希望される方は、以下の手順でお願いいたします。<br>
                    <span class="fontRed font10">※会員登録すると「売る」「買う」両方できます。</span>
                </p>
                <div class="card">
                    <div class="card-body">
                        <div class="card-text">
                            <span class="fontBold">Step1（会員登録）</span>
                        </div>
                        <div class="row">
                            <div class="col-md-12"> 会員登録をお願いいたします。<br>
                                承認されると商品を購入できるようになります。<br>
                                登録時に身分証明書の添付が必須となります。
                            </div>
                        </div>
                    </div>
                </div>
                <div class="arrowSmall my-3"></div>

                <div class="card">
                    <div class="card-body">
                        <div class="card-text">
                            <span class="fontBold">Step2（購入・お振り込み）</span>
                        </div>
                        <div class="row">
                            <div class="col-md-12"> 欲しい商品を見つけたら「購入する」を押し、購入手続きに進んでください。<br>
                                購入手続きを完了すると、出品者に購入者情報が送信されます。
                            </div>
                        </div>
                    </div>
                </div>
                <div class="arrowSmall my-3"></div>

                <div class="card">
                    <div class="card-body">
                        <div class="card-text">
                            <span class="fontBold">Step3（評価）</span>
                        </div>
                        <div class="row">
                            <div class="col-md-12"> 商品を受け取った後に出品者の評価をお願いいたします。<br>
                                これで完了です。
                            </div>
                        </div>
                    </div>
                </div>
                <?php /*?>
                <?php if(isLogin()) { ?>
                <div class="row">
                    <div class="col-xs-8 col-xs-offset-2"> <a class="btn btn-block btn-info" href="itemlist.php">現在掲載されている商品一覧</a> </div>
                </div>
                <?php } ?>
				<?php */ ?>
                <div class="my-4">
                    <?php if (!isLogin()): ?>
                        <a class="btn btn-block btn-info btn-sm"
                           href="<?php echo HOME_URL; ?>/user/userentry.php">新規会員登録</a>
                    <?php else: ?>
                        <a class="btn btn-block btn-info btn-sm" href="<?php echo HOME_URL; ?>/itemlist.php">商品一覧へ</a>
                    <?php endif; ?>
                </div>
            </div>
        </div>
        <div class="col-md-3 sideContents">
            <?php include('../sidebar.php'); ?>
        </div>
    </div>
</div>
<?php include('../footer.php'); ?>
