<?php //include_once(__DIR__.'/../common/util.php'); ?>
<?php // /* サイトタイトル用　パーセント表示など */ //$system_config = SystemConfig::select(); ?>

<?php include('../header.php'); ?>
<div class="container ">
    <div class="row">
        <div class="col-md-9 mainContents wow animate__animated animate__fadeInUp">
            <div class="bg-inner">
                <h1 class=" u-mb20"><span>特定商取引法に基づく表示</span></h1>
                <table class="table table-bordered">
                    <tr>
                        <th>サイト名</th>
                        <td><?= $system_config->site_name ?></td>
                    </tr>
                    <?php if (!empty($system_config->uneisha)): ?>
                        <tr>
                            <th>運営者</th>
                            <td><?= $system_config->uneisha ?></td>
                        </tr>
                    <?php endif;?>
                    <?php if (!empty($system_config->company_name)): ?>
                        <tr>
                            <th>運営会社</th>
                            <td><?= $system_config->company_name ?></td>
                        </tr>
                    <?php endif;?>
                    <tr>
                        <th>所在地</th>
                        <?php if (!empty($system_config->shozaichi)): ?>
                            <td><?= $system_config->shozaichi ?></td>
                        <?php else: ?>
                            <td>ご請求があり次第、遅滞なく提供致します。<br>必要な場合はお問い合わせ先までご連絡下さい。</td>
                        <?php endif;?>
                    </tr>
                    <tr>
                        <th>お問い合わせ</th>
                        <td>お電話でのお問い合わせは受け付けておりません。<br>
                            お問い合わせの際は「<a href="<?php echo HOME_URL; ?>/mail">お問い合わせ</a>」フォームよりご連絡をお願いいたします。
                        </td>
                    </tr>
                    <tr>
                        <th>サイト利用料金等<br>（出品者）</th>
                        <td>出品している商品が売れた場合のみ、売上総額に対して販売手数料がかかります。
                            それ以外に料金はかかりません。<a href="<?php echo HOME_URL; ?>/pages/priceinfo.php">手数料の詳細はこちらから</a>ご確認できます。</td>
                    </tr>
                    <tr>
                        <th>サイト利用料金等<br>（購入者）</th>
                        <td>サイト利用料金はかかりません。</td>
                    </tr>
                    <tr>
                        <th>販売価格</th>
                        <td>各出品商品の販売価格は各ページに記載しております。<br>各ページをご参照下さい。</td>
                    </tr>
                    <tr>
                        <th>支払方法</th>
                        <td>銀行振込・クレジットカード決済</td>
                    </tr>
                    <tr>
                        <th>支払時期</th>
                        <td>基本、先払い（※購入日より7日以内）</td>
                    </tr>
                    <tr>
                        <th>商品の納品</th>
                        <td>出品者が入金確認後となります。<br>詳しくは出品者よりご案内いたします。</td>
                    </tr>
                    <tr>
                        <th>商品価格以外に<br>必要な料金</th>
                        <td>インターネット接続に必要な通信回線等の諸費用。</td>
                    </tr>
                    <tr>
                        <th>返品について</th>
                        <td>購入後のキャンセルや返品はできません。</td>
                    </tr>
                </table>
            </div>
        </div>
        <div class="col-md-3 sideContents wow animate__animated animate__fadeInUp">
            <?php include('../sidebar.php'); ?>
        </div>
    </div>
</div>
<?php include('../footer.php'); ?>
