<?php include('../header.php'); ?>
<div class="container">
    <div class="row">
        <div class="col-md-9 mainContents">
            <div class="bg-inner">
                <h1>売りたい方</h1>
                <p class="margin-bottom-30">出品を希望される方は、以下の手順でお願いいたします。<br>
                    <span class="fontRed font10">※会員登録すると「売る」「買う」両方できます。</span>
                </p>
                <div class="card">
                    <div class="card-body">
                        <div class="card-text">
                            <span class="fontBold">Step1（会員登録）</span>
                        </div>
                        <div class="row">
                            <div class="col-md-12"> 会員登録をお願いいたします。<br>
                                承認されると、あなたの商品を販売することができるようになります。<br>
                                登録時に身分証明書の添付が必須となります。
                            </div>
                        </div>
                    </div>
                </div>
                <div class="arrowSmall my-3"></div>

                <div class="card">
                    <div class="card-body">
                        <div class="card-text">
                            <span class="fontBold">Step2（商品登録・公開）</span>
                        </div>
                        <div class="row">
                            <div class="col-md-12"> あなたの商品を登録しましょう。<br>
                                承認されると公開されます。<br>
                                簡単な操作であなたの商品を販売することができます。<br>
                                SNSなどでも商品公開を告知すれば、より多くの人の目にとまります。
                            </div>
                        </div>
                    </div>
                </div>
                <div class="arrowSmall my-3"></div>

                <!--            <div class="card">-->
                <!--                <div class="card-body">-->
                <!--                    <div class="card-text">-->
                <!--                        <span class="fontBold">Step3（取引連絡）</span>-->
                <!--                    </div>-->
                <!--                    <div class="row">-->
                <!--                        <div class="col-md-12">-->
                <!--                            購入者があなたの商品を購入すると通知が来ます。<br>-->
                <!--                            購入者に連絡し、振込先などの情報をお間違いのないように伝えてください。<br>-->
                <!--                            連絡が遅いと購入者は不安になることがあるので出来るだけ早めにお願いいたします。-->
                <!--                        </div>-->
                <!--                    </div>-->
                <!--                </div>-->
                <!--            </div>-->
                <!--            <div class="arrowSmall my-3"></div>-->

                <div class="card">
                    <div class="card-body">
                        <div class="card-text">
                            <span class="fontBold">Step3（納品）</span>
                        </div>
                        <div class="row">
                            <div class="col-md-12"> 購入者があなたの商品を購入すると通知が来ます。<br>
                                支払いが行われたら、できるだけすぐに納品をお願いします。<br>
                                素早い対応が信用になります。
                            </div>
                        </div>
                    </div>
                </div>

                <div class="arrowSmall my-3"></div>

                <div class="card">
                    <div class="card-body">
                        <div class="card-text">
                            <span class="fontBold">Step4（評価）</span>
                        </div>
                        <div class="row">
                            <div class="col-md-12"> 購入者が出品者を評価します。<br>
                                購入者の評価をすれば取引終了です。
                            </div>
                        </div>
                    </div>
                </div>
                <div class="my-4">
                    <?php if (!isLogin()): ?>
                        <a class="btn btn-block btn-info btn-sm" href="<?php echo HOME_URL; ?>/user/userentry.php">新規会員登録</a>
                    <?php else: ?>
                        <a class="btn btn-block btn-info btn-sm" href="<?php echo HOME_URL; ?>/user/sell.php">出品する</a>
                    <?php endif; ?>
                </div>
            </div>
        </div>
        <div class="col-md-3 sideContents">
            <?php include('../sidebar.php'); ?>
        </div>
    </div>

</div>

<?php include('../footer.php'); ?>
