<?php 
$title_page = 'てっぱんプレミアムストアとは';
$description_page = '欲しい商品を画像で確認・届いた商品を画像で照合！気持ち良い取引が叶うオンラインマーケット。';
 ?>
<?php include('../user_header.php'); ?>
    <div class="com-header-top">
        <div class="com-header-top__img wow animate__animated animate__fadeInUp">
            <img src="<?php echo HOME_URL; ?>/common/assets/img/common/other-com-header.png" alt="">
        </div>
        <div class="com-header-top__path bg-other-01 wow animate__animated animate__fadeInUp">
            <p><span><a href="<?php echo HOME_URL; ?>/" class="clr-yel">トップページ</a></span><span> > </span><span>てっぱんプレミアムストアとは</span></p>
        </div>
       <!-- <div class="com-header-top__txt">
            <p>てっぱんプレミアムストアとは</p>
        </div> -->
    </div>
    <div class="customer-container">
    <div class="category-title wow animate__animated animate__fadeInUp">
                <h3><span>てっぱんプレミアムストアとは</span></h3>
            </div>
        <div class="customer-contact-form">
            <div class="consept">
                <div class="contents wow animate__animated animate__fadeInUp">
                    <h3>
                        欲しい商品を画像で確認・届いた商品を画像で照合<br>
                        気持ち良い取引が叶うオンラインマーケット。
                    </h3>
                    <p>
                    <span class="font_b">※只今、技術対応中。近日バージョンアップ致します！</span>
                    </p>
                </div>
                <div class="contents wow animate__animated animate__fadeInUp">
                    <p>
                        <img src="https://teppan.store/common/assets/img/common/teppan_consept.jpg" alt="てっぱんプレミアムストアとは"/>                    
                    </p>
                </div>
                <div class="contents wow animate__animated animate__fadeInUp">
                    <h4>
                        てっぱんプレミアムストアとは？
                    </h4>
                    <p>
                        てっぱんプレミアムストアは<br>
                        あなたの大切な一品や市場価値の高い商品を安全・安心して取引してもらえる為に<br>
                        工夫を凝らした今までにないオンラインマーケットです。
                    </p>
                    <p>
                        工夫その１　「<span class="font_b">チーム関所</span>」の設置<br>
                        ・商品の<span class="font_b">すり替え</span>を防止するための「画像認証システム」の導入。<br>
                        ・<span class="font_b">ホンモノ</span>を確実に手にする為、各種専門家による肉眼真贋鑑定経由での購入。
                    </p>
                    <p>
                        工夫その２　「<span class="font_b">チーム宿直6班窓口</span>」の設置<br>
                        ・購入後の各種相談・トラブルを真正面から解決するために「マーケット利用者の身分証明書の二段階確認」「郵便物到着確認」「弁護士費用少額短期保険加入」その他24時間トラブル解決に向けて幅広いご意見・ご提案をメールにて募集し、被害者を置き去りにせず、解決に向けて迅速対応。
                    </p>
                </div>
                <div class="contents wow animate__animated animate__fadeInUp">
                    <h4>
                        お約束
                    </h4>
                    <p>
                        1.コピー品・模倣品・盗品・破損品・不良品は断固防衛！<br>
                        受け取った人が悲しむ事が無いように独自の仕組みを採用しております。
                    </p>
                    <p>
                        2.配送時のすり替え・不正取引を防御！<br>
                        画像を見て購入した商品とお手元に届いた商品が同一かどうか確認できるシステムを導入しております！
                    </p>
                    <p>
                        3.万が一のトラブルには運営会社が真摯に対応！<br>
                        てっぱんプレミアムストアのモットーは「電子商取引の健全化」です。<br>
                        運営会社としてご利用者様の声に真摯に耳を傾けてトラブルを解決し、<br>
                        不正無き売買・譲り合いを安全・安心して円滑に行えるマーケット運用をお約束いたします。
                    </p>
                </div>
            </div>
        </div>
    </div>
<?php include('../footer.php'); ?>