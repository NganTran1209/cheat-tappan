<?php 
$title_page = 'よくあるご質問';
$description_page = '欲しい商品を画像で確認・届いた商品を画像で照合！気持ち良い取引が叶うオンラインマーケット。';
 ?>

<?php include('../user_header.php'); ?>
    <div class="com-header-top">
        <div class="com-header-top__img wow animate__animated animate__fadeInUp">
            <img src="<?php echo HOME_URL; ?>/common/assets/img/common/other-com-header.png" alt="">
        </div>
        <div class="com-header-top__path bg-other-01 wow animate__animated animate__fadeInUp">
            <p><span><a href="<?php echo HOME_URL; ?>/" class="clr-yel">トップページ</a></span><span> > </span><span>よくあるご質問</span></p>
        </div>
  <!--      <div class="com-header-top__txt">
            <p>よくあるご質問</p>
        </div>-->
    </div>
    <div class="customer-container">
    <div class="category-title wow animate__animated animate__fadeInUp">
                <h3><span>よくある質問</span></h3>
            </div>
        <div class="customer-contact-form">
            <div class="faq-content wow animate__animated animate__fadeInUp">
                <div class="faq-content-item" data-toggle="collapse" data-target="#faq-1" aria-expanded="false">
                    <div class="faq-content-item__txt">
                        <p>ご質問1</p>
                    </div>
                    <div class="faq-content-item__txt">
                        <p>ログインができない。
                           ログイン方法がわからない。  </p>
                    </div>
                    <div class="faq-content-item__icon">
                        <i class="bi bi-chevron-down"></i>
                        <i class="bi bi-chevron-up"></i>
                    </div>
                </div>
                <div id="faq-1" class="faq-content-item__show collapse">
                    <p>回答あいうえお回答あいうえお回答あいうえお回答あいうえお回答あいうえお回答あいうえお回答あいうえお回答あいうえお。</p>
                </div>

                <div class="faq-content-item" data-toggle="collapse" data-target="#faq-2" aria-expanded="false">
                    <div class="faq-content-item__txt">
                        <p>ご質問2</p>
                    </div>
                    <div class="faq-content-item__txt">
                        <p>取引をキャンセルしたいが方法がわからない</p>
                    </div>
                    <div class="faq-content-item__icon">
                        <i class="bi bi-chevron-down"></i>
                        <i class="bi bi-chevron-up"></i>
                    </div>
                </div>
                <div id="faq-2" class="faq-content-item__show collapse">
                    <p>回答あいうえお回答あいうえお回答あいうえお回答あいうえお回答あいうえお回答あいうえお回答あいうえお回答あいうえお。</p>
                </div>

                <div class="faq-content-item" data-toggle="collapse" data-target="#faq-3" aria-expanded="false">
                    <div class="faq-content-item__txt">
                        <p>ご質問3</p>
                    </div>
                    <div class="faq-content-item__txt">
                        <p>本人確認はなぜ必要なのか</p>
                    </div>
                    <div class="faq-content-item__icon">
                        <i class="bi bi-chevron-down"></i>
                        <i class="bi bi-chevron-up"></i>
                    </div>
                </div>
                <div id="faq-3" class="faq-content-item__show collapse">
                    <p>回答あいうえお回答あいうえお回答あいうえお回答あいうえお回答あいうえお回答あいうえお回答あいうえお回答あいうえお。</p>
                </div>

                <div class="faq-content-item" data-toggle="collapse" data-target="#faq-4" aria-expanded="false">
                    <div class="faq-content-item__txt">
                        <p>ご質問4</p>
                    </div>
                    <div class="faq-content-item__txt">
                        <p>商品販売の流れはどうなっているのか</p>
                    </div>
                    <div class="faq-content-item__icon">
                        <i class="bi bi-chevron-down"></i>
                        <i class="bi bi-chevron-up"></i>
                    </div>
                </div>
                <div id="faq-4" class="faq-content-item__show collapse">
                    <p>回答あいうえお回答あいうえお回答あいうえお回答あいうえお回答あいうえお回答あいうえお回答あいうえお回答あいうえお。</p>
                </div>

                <div class="faq-content-item" data-toggle="collapse" data-target="#faq-5" aria-expanded="false">
                    <div class="faq-content-item__txt">
                        <p>ご質問5</p>
                    </div>
                    <div class="faq-content-item__txt">
                        <p>返金方法について知りたい</p>
                    </div>
                    <div class="faq-content-item__icon">
                        <i class="bi bi-chevron-down"></i>
                        <i class="bi bi-chevron-up"></i>
                    </div>
                </div>
                <div id="faq-5" class="faq-content-item__show collapse">
                    <p>回答あいうえお回答あいうえお回答あいうえお回答あいうえお回答あいうえお回答あいうえお回答あいうえお回答あいうえお。</p>
                </div>

                <div class="faq-content-item" data-toggle="collapse" data-target="#faq-6" aria-expanded="false">
                    <div class="faq-content-item__txt">
                        <p>ご質問6</p>
                    </div>
                    <div class="faq-content-item__txt">
                        <p>入金時期などについて知りたい</p>
                    </div>
                    <div class="faq-content-item__icon">
                        <i class="bi bi-chevron-down"></i>
                        <i class="bi bi-chevron-up"></i>
                    </div>
                </div>
                <div id="faq-6" class="faq-content-item__show collapse">
                    <p>回答あいうえお回答あいうえお回答あいうえお回答あいうえお回答あいうえお回答あいうえお回答あいうえお回答あいうえお。</p>
                </div>
            </div>
        </div>
    </div>

<?php include('../footer.php'); ?>