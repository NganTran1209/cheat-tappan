<?php include('../header.php'); ?>
<div class="container">
    <div class="row">
        <div class="col-md-9 mainContents">
            <div class="bg-inner">
                <h1><img class="img-fluid" src="images/lapias/A001.png" width="850" height="150" alt="ご利用ガイド"/></h1>
                <div class="row">
                    <div class="col-xs-10 col-xs-offset-1"><a class="btn btn-block btn-info btn-usersguide"
                                                              href="itemlist.php">掲載されている商品一覧</a></div>
                </div>
            </div>
        </div>
        <div class="col-sm-4 sideContents">
            <?php include('../sidebar.php'); ?>
        </div>
    </div>
</div>
<?php include('../footer.php'); ?>
