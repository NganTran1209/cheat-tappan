<?php 
$title_page = 'ご利用の流れ（買う）';
$description_page = '欲しい商品を画像で確認・届いた商品を画像で照合！気持ち良い取引が叶うオンラインマーケット。';
 ?>
<?php include_once(__DIR__ . '/../common/util.php'); ?>

<?php include('../user_header.php'); ?>
<script>
    function pageProduct(){
        header('Location: ' . getContextRoot() . '/pages/list_item_category.php');
        exit();
    }
</script>
    <div class="com-header-top">
        <div class="com-header-top__img wow animate__animated animate__fadeInUp">
            <img src="<?php echo HOME_URL; ?>/common/assets/img/common/other-com-header.png" alt="">
        </div>
        <div class="com-header-top__path bg-other-01 wow animate__animated animate__fadeInUp">
            <p><span><a href="<?php echo HOME_URL; ?>/" class="clr-yel">トップページ</a></span><span> > </span><span>ご利用の流れ（買う）</span></p>
        </div>
<!--        <div class="com-header-top__txt">
            <p>ご利用の流れ（買う）</p>
        </div>-->
    </div>
    <div class="customer-container">
    <div class="category-title wow animate__animated animate__fadeInUp">
                <h3><span>ご利用の流れ（買う）</span></h3>
            </div>
        <div class="customer-contact-form">
            <div class="flow-content">
                <div class="flow-content__txt wow animate__animated animate__fadeInUp">
                    <p>購入を希望される方は、以下の手順でお願いいたします。 </p>                    
                    <p>※会員登録すると「売る」「買う」両方ご利用できます。</p>
                </div>
                <div class="flow-content-step wow animate__animated animate__fadeInUp">
                    <div class="flow-content-step__left">
                        <p>Step1</p>
                        <p>(会員登録)</p>
                    </div> 
                    <div class="flow-content-step__right">
                        <p>
                            会員登録をお願いいたします。 <br>
                            承認されると商品を購入できるようになります。 <br>
                            登録時に身分証明書の添付が必須となります。
                        </p>
                    </div>
                </div>
                <div class="flow-icon wow animate__animated animate__zoomIn"></div>
                <div class="flow-content-step wow animate__animated animate__fadeInUp">
                    <div class="flow-content-step__left">
                        <p>Step2</p>
                        <p>(購入・お振り込み)</p>
                    </div>                     
                    <div class="flow-content-step__right">
                        <p>
                            欲しい商品を見つけたら「購入する」を押し、購入手続きに進んでください。<br>
                            購入手続きを完了すると、出品者に購入者情報が送信されます。
                        </p>
                    </div>
                </div>
                <div class="flow-icon wow animate__animated animate__zoomIn"></div>
                <div class="flow-content-step wow animate__animated animate__fadeInUp">
                    <div class="flow-content-step__left">
                        <p>Step3</p>
                        <p>(評価)</p>
                    </div>
                    <div class="flow-content-step__right">
                        <p>
                            商品を受け取った後に出品者の評価をお願いいたします。<br>
                            これで完了です。
                        </p>
                    </div>
                </div>
                <div class="notice-show__btn wow animate__animated animate__fadeInUp">
                    <?php if (!isLogin()): ?>
                        <button  onclick="location.href= '<?php echo HOME_URL; ?>/user/userentry.php'">新規会員登録</button>
                    <?php else: ?>
                        <button  onclick="location.href= '<?php echo HOME_URL; ?>/itemlist.php'">商品一覧へ</button>
                    <?php endif; ?>
                    
                </div>
            </div>
        </div>
    </div>
    <!-- 
    <section class="footer-login">
        <div class="footer-login-content">
            <div class="footer-login-content__img">
                <img class="pc-show" src="<?php echo HOME_URL; ?>/common/assets/img/top/footer-bg.png" alt="">
                <img class="sp-show" src="<?php echo HOME_URL; ?>/common/assets/img/top/sp-footer-bg.png" alt="">
            </div>
           <div class="footer-login-content-item" >
                <div class="footer-login-content-item__txt"><h4>出品/購入するなら</h4></div>
                <div class="footer-login-content-item__txt"><h3>今すぐ会員登録！</h3></div>
                <div class="footer-login-content-item__btn" onclick="location.href= '<?php echo HOME_URL; ?>/user/userentry.php'">
                    <span>
                    </span>
                    <span>
                        新規会員登録
                    </span>
                </div>
            </div>
        </div>
    </section>-->
<?php include('../footer.php'); ?>